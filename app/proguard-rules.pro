# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in D:\Tools\AndroidStudioInstall\sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}
-keep class android.content.pm.** {*;}
-keep class * extends android.content.pm.PackageManager {*;}
-keepclassmembers class * extends android.content.pm.PackageManager {
    public void get*(java.lang.String, android.content.pm.IPackageStatsObserver);
}
-keep class com.fbon.android.app.basiccleaner.feature.CleanLdrManager
-keep class com.startapp.** {*;}
-keepattributes Exceptions, InnerClasses, Signature, Deprecated, SourceFile,LineNumberTable, *Annotation*, EnclosingMethod
-keep class com.google.android.gms.** { *; }
-dontwarn com.google.android.gms.**

package com.samsung.android.sdk.slinkcloud;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

/**
 * Utility class for obtaining remote Samsung Link device info and sending specific commands to remote devices.
 */
public class CloudGatewayDeviceInfoUtils {
    public static final String READY_TO_LOGIN = "READY_TO_LOGIN";
    public static final String GET_LAST_SYNC_TIME = "GET_LAST_SYNC_TIME";
    public static final String GET_STORAGE_USED = "GET_STORAGE_USED";
    public static final String UPDATE_QUOTA = "UPDATE_QUOTA";

    private static final String TAG = "mfl_ApiLib_" + CloudGatewayDeviceInfoUtils.class.getSimpleName();

    private static CloudGatewayDeviceInfoUtils sInstance;

    private final Context context;

    /**
     * Gets the singleton CloudGatewayDeviceInfoUtils instance.
     *
     * @param context Context
     * @return the singleton CloudGatewayDeviceInfoUtils instance
     * @throws NullPointerException if context is null
     */
    public static synchronized CloudGatewayDeviceInfoUtils getInstance(Context context) {
        if (context == null) {
            throw new NullPointerException("context is null");
        }
        if (sInstance == null) {
            sInstance = new CloudGatewayDeviceInfoUtils(context);
        }
        return sInstance;
    }

    /**
     * Private constructor.
     *
     * @param context Context
     */
    private CloudGatewayDeviceInfoUtils(Context context) {
        this.context = context.getApplicationContext();
    }

    /**
     * Get versitile information using bundle from slink platform
     * synchronous call, caller should use a thread or AsyncTask to handle the response.
     *
     * @return Bundle.
     */
    public Bundle getVersatileInformation(String strInfoType, int nDeviceId) {
        Bundle extras = new Bundle();
        extras.putString(
                CloudGatewayMediaStore.CallMethods.GetVersatileInformation.INTENT_ARG_INFO_TYPE,
                strInfoType);
        extras.putLong(
                CloudGatewayMediaStore.CallMethods.GetVersatileInformation.INTENT_ARG_DEVICEID,
                nDeviceId);

        Bundle result = null;
        try {
            result = this.context.getContentResolver().call(
                    CloudGatewayMediaStore.CallMethods.CONTENT_URI,
                    CloudGatewayMediaStore.CallMethods.GetVersatileInformation.NAME,
                    null,
                    extras);
        } catch (Exception e) {
            ////tskim fix stress test issue when called after platform disabling
            Log.e(TAG, "Exception ::maybe platform disabled ");
        }
        ////prevent FC
        if (result == null) {
            Log.e(TAG, "::getDeviceInfo result is null");
            return null;
        }
        return result;
    }
}


package com.samsung.android.sdk.slinkcloud;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Random;

/**
 * Utility class for performing file transfer operations such as downloading
 * files from remote devices and uploading files to remote devices.
 */
public class CloudGatewayFileTransferUtils {

    private static final String TAG = "mfl_ApiLib_"
            + CloudGatewayFileTransferUtils.class.getSimpleName();

    /**
     * Intent action for the file transfer list activity
     */
    public static final String ACTION_FILE_TRANSFER_LIST = "com.samsung.android.sdk.slinkcloud.filetransfer.FileTransferList";

    /**
     * Intent action to start a remote device to remote device file transfer (3box)
     */
    public static final String ACTION_TRANSFER = "com.samsung.android.sdk.slinkcloud.filetransfer.Transfer";

    /**
     * Intent action to start a cancel
     */
    public static final String ACTION_CANCEL = "com.samsung.android.sdk.slinkcloud.filetransfer.Cancel";

    public static final String ACTION_RENAME_REQ = "com.samsung.android.sdk.slinkcloud.filetransfer.RenameReq";
    public static final String ACTION_RENAME_RSP = "com.samsung.android.sdk.slinkcloud.filetransfer.RenameRsp";

    /**
     * Intent extra used to store a device id (long)
     */
    public static final String EXTRA_DEVICE_ID = "deviceId";

    /**
     * Intent extra used to store the row ids (long[])
     */
    public static final String EXTRA_ROW_IDS = "rowIds";

    /**
     * Intent extra used to store {@link TransferOptions}
     */
    public static final String EXTRA_TRANSFER_OPTIONS = "transferOptions";

    /**
     * Intent extra used to check whether the cancel is intended from auto upload
     */
    public static final String EXTRA_IS_CANCEL_FOR_AUTO_UPLOAD = "isCancelForAutoUpload";

    /**
     * Intent extra used to check file transfer session info
     */
    public static final String EXTRA_SESSION_ID = "sessionId";

    public static final String EXTRA_SESSION_STATUS_CODE = "sessionStatus";
    public static final String EXTRA_SESSION_STATUS_PERCENTAGE = "sessionPercent";
    public static final String EXTRA_SESSION_STATUS_TOTALBYTES = "sessionTotalBytes";
    public static final String EXTRA_SESSION_STATUS_SENTBYTES = "sessionCurBytes";
    public static final String EXTRA_SESSION_STATUS_SENTFILENUM = "sessionCurSentFileNum";
    public static final String EXTRA_SESSION_STATUS_SRCDEV_ID = "sessionSourceID";
    public static final String EXTRA_SESSION_STATUS_TARGETDEV_ID = "sessionTargetID";
    public static final String EXTRA_SESSION_STATUS_FIRSTFILENAME = "sessionFirstFileName";
    public static final String EXTRA_SESSION_STATUS_FILENUM = "sessionFileNum";
    public static final String EXTRA_SESSION_STATUS_FILELIST = "sessionFileList";
    public static final String EXTRA_SESSION_STATUS_ERROR_TYPE = "sessionErrorType";

    public static final int SESSION_STATUS_STARTED = 0;
    public static final int SESSION_STATUS_CANCELED = 1;
    public static final int SESSION_STATUS_INPROGRESS = 2;
    public static final int SESSION_STATUS_ERROR = -1;
    public static final int SESSION_STATUS_COMPLETED = 3;
    public static final int SESSION_STATUS_PREPARE = 4;


    public static final String EXTRA_RENAME_OLDNAME = "renameOldName";
    public static final String EXTRA_RENAME_NEWNAME = "renameNewName";
    public static final String EXTRA_RENAME_TOKEN = "renameToken";
    public static final String EXTRA_RENAME_STATUS = "renameStatus";
    public static final String EXTRA_RENAME_ISDIRECTORY = "renameIsDirectory";
    public static final String EXTRA_RENAME_APPLYALL = "renameAplyAll";

    public static final int RENAME_STATUS_REQ_INUSE = 1;
    public static final int RENAME_STATUS_REQ_STRANGE_CHARACTER = 2;


    public static final int RENAME_STATUS_RSP_START = 3;
    public static final int RENAME_STATUS_RSP_RENAME = 4;
    public static final int RENAME_STATUS_RSP_REPLACE = 5;
    public static final int RENAME_STATUS_RSP_SKIP = 6;
    public static final int RENAME_STATUS_RSP_CANCEL = 7;

    public static int m_nSessionIdCount = 0;

    /**
     * The action of the intent broadcasted by fileTransfer task which includes the created session id
     */
    public static final String FILE_TRANSFER_SESSION_ID_BROADCAST_ACTION = "com.samsung.android.sdk.slinkcloud.FILE_TRANSFER_SESSION_ID_BROADCAST_ACTION";

    /**
     * This broadcast is sent when the auto upload in progress state has changed
     */
    public static final String BROADCAST_AUTO_UPLOAD_STATE_CHANGED = "com.samsung.android.sdk.slinkcloud.AUTO_UPLOAD_STATE_CHANGED";

    private static CloudGatewayFileTransferUtils sInstance;

    private final Context context;

    /**
     * Gets the singleton CloudGatewayFileTransferUtils instance.
     *
     * @param context Context
     * @return the singleton CloudGatewayFileTransferUtils instance
     * @throws NullPointerException if context is null
     */
    public static synchronized CloudGatewayFileTransferUtils getInstance(Context context) {
        if (context == null) {
            throw new NullPointerException("context is null");
        }
        if (sInstance == null) {
            sInstance = new CloudGatewayFileTransferUtils(context);
        }
        return sInstance;
    }

    /**
     * Private constructor.
     *
     * @param context Context
     */
    private CloudGatewayFileTransferUtils(Context context) {
        this.context = context.getApplicationContext();
    }

    public void transferFiles(
            CloudGatewayMediaSet mediaSet,
            long remoteDeviceId,
            TransferOptions options,
            String strPremadeSession) {
        Log.v(TAG, "Enter ::transferFiles(mediaSet, remoteDeviceId:"
                + remoteDeviceId
                + ", TransferOptions)");
        Intent intent = new Intent(ACTION_TRANSFER);
        //// android L accept only explicit Intent
        intent.setComponent(new ComponentName(
                CloudGatewayConstants.TARGET_APK_PACKAGE,
                "platform.com.mfluent.asp.filetransfer.FileTransferStarterService"));
        if (mediaSet != null)
            mediaSet.writeToIntent(intent);
        intent.putExtra(EXTRA_DEVICE_ID, remoteDeviceId);
        if (options != null) {
            intent.putExtra(EXTRA_TRANSFER_OPTIONS, options);
        }
        if (strPremadeSession != null) {
            intent.putExtra(EXTRA_SESSION_ID, strPremadeSession);
        }
//        CloudGatewayCommonUtils.getInstance(this.context).addAppIdToIntent(intent);

        this.context.startService(intent);
    }

    /**
     * Checks to see if a 3box transfer is supported between the given devices.
     * This method performs remote procedure call and should not be invoked from the main thread.
     *
     * @param sourceDeviceId The device that has the content.
     * @param targetDeviceId The device that will receive the content.
     * @return true if the transfer is supported, false otherwise
     */
    public boolean is3boxSupported(long sourceDeviceId, long targetDeviceId) {

        Log.v(TAG, "Enter ::is3boxSupported( sourceDeviceId:"
                + sourceDeviceId
                + ", targetDeviceId:"
                + targetDeviceId
                + ")");

        Bundle extras = new Bundle();
        extras.putLong(
                CloudGatewayMediaStore.CallMethods.Is3boxSupported.EXTRA_SOURCE_DEVICE_ID,
                sourceDeviceId);
        extras.putLong(
                CloudGatewayMediaStore.CallMethods.Is3boxSupported.EXTRA_TARGET_DEVICE_ID,
                targetDeviceId);

        Bundle result = null;
        try {
            result = this.context.getContentResolver().call(
                    CloudGatewayMediaStore.CallMethods.CONTENT_URI,
                    CloudGatewayMediaStore.CallMethods.Is3boxSupported.NAME,
                    null,
                    extras);
        } catch (IllegalArgumentException e) {
            ////tskim fix stress test issue when called after platform disabling
            Log.e("slinklib", "IllegalArgumentException ::maybe platform disabled is3boxSupported");
        }
        if (result == null) {
            return false;
        }
        return result.getBoolean(CloudGatewayMediaStore.CallMethods.KEY_RESULT);
    }

    public String createPremadeUniqueSessionID(String strAppName, int nTargetDevID) {
        long nCurTime = System.currentTimeMillis();
        int nSeed = 0;
        int nNextID = 0;
        Random random = new Random();
        nSeed = random.nextInt(1000000);
        nNextID = m_nSessionIdCount++;
        String strResult = "DEV-" + nTargetDevID + "-" + strAppName + "-" + Long.toHexString(nCurTime) + "-" + Integer.toHexString(nSeed) + "-" + nNextID;
        return strResult;
    }

    public static final class CloudGatewayFileTransferSessionInfo {

        public enum CloudGatewayFileTransferState {
            PENDING,
            IN_PROGRESS,
            COMPLETE,
            ERROR,
            CANCELLED
        }

        private String sessionId;
        private CloudGatewayFileTransferState state;
        private long totalBytes;
        private long sentBytes;
        private int percent;
        private String controlDeviceName;
        private String sourceDeviceName;
        private String targetDeviceName;
        private String currentFileName;
        private int totalNumberOfFiles;
        private int currentFileIndex;

        private CloudGatewayFileTransferSessionInfo(String sessionInfo) {
            if (TextUtils.isEmpty(sessionInfo)) {
                throw new IllegalArgumentException("sessionInfo must be non-empty");
            }
            try {
                JSONObject sessionJson = new JSONObject(sessionInfo);
                this.sessionId = sessionJson.getString("id");
                this.sentBytes = sessionJson.optLong("currentBytesSent", -100L);
                this.totalBytes = sessionJson.optLong("totalBytesSent", -100L);
                this.controlDeviceName = sessionJson.optString("controllerName", "NULL");
                this.sourceDeviceName = sessionJson.optString("sourceName", "NULL");
                this.targetDeviceName = sessionJson.optString("targetName", "NULL");
                this.state = CloudGatewayFileTransferState.valueOf(sessionJson.optString(
                        "status",
                        "ERROR"));
                this.currentFileName = sessionJson.optString("currentFileName", "NULL");
                this.currentFileIndex = sessionJson.optInt("currentFileIndex", -1);
                this.totalNumberOfFiles = sessionJson.optInt("numberOfFiles", -1);

                int percent;
                if (this.totalBytes <= 0) {
                    percent = 0;
                } else if (this.sentBytes > this.totalBytes) {
                    percent = 100;
                } else {
                    percent = (int) (this.sentBytes * 100 / this.totalBytes);
                }

                this.percent = percent;

            } catch (JSONException e) {
                e.printStackTrace();
                throw new IllegalArgumentException("sessionInfo must be json format");
            }

        }

        public String getSessionId() {
            return this.sessionId;
        }

        public CloudGatewayFileTransferState getFileTransferState() {
            return this.state;
        }

        public long getTotalBytes() {
            return this.totalBytes;
        }

        public long getSentBytes() {
            return this.sentBytes;
        }

        public int getPercent() {
            return this.percent;
        }

        public String getControlDeviceName() {
            return this.controlDeviceName;
        }

        public String getSourceDeviceName() {
            return this.sourceDeviceName;
        }

        public String getTargetDeviceName() {
            return this.targetDeviceName;
        }

        public String getCurrentFileName() {
            return this.currentFileName;
        }

        public int getCurrentFileIndex() {
            return this.currentFileIndex;
        }

        public int getTotalNumberOfFiles() {
            return this.totalNumberOfFiles;
        }
    }

    /**
     * Cancel file transfers
     * This method performs it's work in the background and is safe to call from the main thread.
     */
    public void cancelFileTransferBySessionId(String sessionId) {
        Log.v(TAG, "Enter ::cancelFileTransferBySessionId() : " + sessionId);
        if (sessionId == null || sessionId.isEmpty()) {
            throw new NullPointerException("sessionId is null!");
        }
        Intent intent = new Intent(ACTION_CANCEL);
        //// android L accept only explicit Intent
        intent.setComponent(new ComponentName(
                CloudGatewayConstants.TARGET_APK_PACKAGE,
                "platform.com.mfluent.asp.filetransfer.FileTransferStarterService"));
        intent.putExtra(EXTRA_IS_CANCEL_FOR_AUTO_UPLOAD, false);
        intent.putExtra(EXTRA_SESSION_ID, sessionId);
//        CloudGatewayCommonUtils.getInstance(this.context).addAppIdToIntent(intent);

        this.context.startService(intent);
    }

    public boolean cancelFileTransferWithResult(String sessionId) {
        Bundle extras = new Bundle();
        extras.putString(
                CloudGatewayMediaStore.CallMethods.CancelFileTransfer.INTENT_ARG_SESSION_ID,
                sessionId);
        Bundle result = null;
        try {
            result = this.context.getContentResolver().call(
                    CloudGatewayMediaStore.CallMethods.CONTENT_URI,
                    CloudGatewayMediaStore.CallMethods.CancelFileTransfer.NAME,
                    null,
                    extras);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (result == null) {
            return false;
        }
        return result.getBoolean(CloudGatewayMediaStore.CallMethods.KEY_RESULT);
    }

    /**
     * Options that affect file transfers
     */
    public static final class TransferOptions implements Parcelable {

        /**
         * if true then the media files will be deleted from the source device when the file transfer is complete.
         * <p>
         * Default is false
         * </p>
         */
        public boolean deleteSourceFilesWhenTransferIsComplete;

        /**
         * if true then the transfer will skip files that already have a duplicate on the target device. Set to false to force the file transfer regardless of
         * the presence of duplicates.
         * <p>
         * Default is true
         * </p>
         */
        public boolean skipIfDuplicate = true;

        /**
         * The target directory where transferred files should be placed. If null, the transferred files will be placed in an appropriate directory based on
         * their type.
         * <p>
         * Default is null
         * </p>
         */
        public String targetDirectoryPath;

        public boolean transferImmediately;
        public boolean useTrackingSession;
        public boolean waitForRename;

        /**
         * Default constructor
         */
        public TransferOptions() {
        }

        @Override
        public int describeContents() {
            return 0;
        }

        private TransferOptions(Parcel in) {
            boolean[] bools = in.createBooleanArray();
            this.deleteSourceFilesWhenTransferIsComplete = bools[0];
            this.skipIfDuplicate = bools[1];
            this.transferImmediately = bools[2];
            this.useTrackingSession = bools[3];
            this.waitForRename = bools[4];

            this.targetDirectoryPath = in.readString();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeBooleanArray(new boolean[]{
                    this.deleteSourceFilesWhenTransferIsComplete,
                    this.skipIfDuplicate,
                    this.transferImmediately,
                    this.useTrackingSession,
                    this.waitForRename});

            dest.writeString(this.targetDirectoryPath);
        }

        public static final Parcelable.Creator<TransferOptions> CREATOR = new Parcelable.Creator<TransferOptions>() {

            @Override
            public TransferOptions createFromParcel(Parcel in) {
                return new TransferOptions(in);
            }

            @Override
            public TransferOptions[] newArray(int size) {
                return new TransferOptions[size];
            }
        };
    }
}

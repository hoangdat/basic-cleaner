
package com.samsung.android.sdk.slinkcloud;

import java.util.Arrays;

import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.text.TextUtils;

/**
 * Represents a collection of media files from either {@link MediaStore} or {@link CloudGatewayMediaStore} or consisting of a list of local file paths.
 */
public final class CloudGatewayMediaSet implements Parcelable {

    private static final String EXTRA_URI = "com.samsung.android.sdk.slinkcloud.MediaSet.uri";
    private static final String EXTRA_SELECTION = "com.samsung.android.sdk.slinkcloud.MediaSet.selection";
    private static final String EXTRA_SELECTION_ARGS = "com.samsung.android.sdk.slinkcloud.MediaSet.selectionArgs";
    private static final String EXTRA_IDS = "com.samsung.android.sdk.slinkcloud.MediaSet.ids";
    private static final String EXTRA_INCLUDE = "com.samsung.android.sdk.slinkcloud.MediaSet.include";
    private static final String EXTRA_ID_COLUMN_NAME = "com.samsung.android.sdk.slinkcloud.MediaSet.idColumnName";
    private static final String EXTRA_ID_COLUMN_TYPE = "com.samsung.android.sdk.slinkcloud.MediaSet.idColumnType";
    private static final String EXTRA_SORT_ORDER = "com.samsung.android.sdk.slinkcloud.MediaSet.sortOrder";
    private static final String EXTRA_LOCAL_FILE_PATHS = "com.samsung.android.sdk.slinkcloud.MediaSet.localFilePaths";

    private Uri uri;

    private String selection;

    private String[] selectionArgs;

    private String[] ids;

    private String[] localFilePaths;

    private boolean include;

    private String idColumnName;

    private int idColumnType = Cursor.FIELD_TYPE_INTEGER;

    private String sortOrder;

    /**
     * Creates a media set from a list of {@link MediaStore} records.
     * 
     * @param mediaStoreIds
     *            ids of rows in {@link MediaStore}
     * @return
     *         the media set
     */
    public static CloudGatewayMediaSet createFromMediaStoreIds(long[] mediaStoreIds) {
        CloudGatewayMediaSet mediaSet = new CloudGatewayMediaSet();
        mediaSet.uri = CloudGatewayMediaStore.Files.CONTENT_URI;
        mediaSet.selection = CloudGatewayMediaStore.DeviceColumns.TRANSPORT_TYPE + "=?";
        mediaSet.selectionArgs = new String[] {CloudGatewayDeviceTransportType.LOCAL.name()};
        mediaSet.ids = CloudGatewayMediaSet.convertToStringArray(mediaStoreIds);
        mediaSet.include = true;
        mediaSet.idColumnName = "source_media_id";

        return mediaSet;
    }

    /**
     * Creates a media set from a list of local file paths.
     * 
     * @param localFilePaths
     *            - an array of local file paths that make up this set.
     * @return the media set
     */
    public static CloudGatewayMediaSet createFromLocalFilePaths(String[] localFilePaths) {
        CloudGatewayMediaSet mediaSet = new CloudGatewayMediaSet();
        mediaSet.uri = CloudGatewayMediaStore.buildFileUri();
        mediaSet.localFilePaths = localFilePaths.clone();
        return mediaSet;
    }

    /**
     * Creates a media set compatible with the {@link CloudGatewayMediaStore.FileBrowser.FileList} contract class.
     * 
     * @param deviceId
     *            The row id of the remote device where the file set is located.
     * @param directoryId
     *            The {@link CloudGatewayMediaStore.FileBrowser.FileBrowserColumns#FILE_ID} for directory that contains this set of files.
     * @param fileSelections
     *            An array of {@link CloudGatewayMediaStore.FileBrowser.FileBrowserColumns#FILE_ID} values to determine this set.
     * @param include
     *            Boolean value to determine whether this set is equal to the {@code fileSelections} array or the inverse selection. false means it is the
     *            inverse selection.
     * @param sortOrder
     *            The sort order to retrieve files in from the remote device.
     * @return the media set
     */
    public static CloudGatewayMediaSet createFromFileBrowserIds(
            long deviceId,
            String directoryId,
            String[] fileSelections,
            boolean include,
            String sortOrder) {
        CloudGatewayMediaSet mediaSet = new CloudGatewayMediaSet();
        if (TextUtils.isEmpty(directoryId)) {
            mediaSet.uri = CloudGatewayMediaStore.FileBrowser.FileList
                    .getDefaultFileListUri(deviceId);
        } else {
            mediaSet.uri = CloudGatewayMediaStore.FileBrowser.FileList.getFileListUri(
                    deviceId,
                    directoryId);
        }
        mediaSet.idColumnName = CloudGatewayMediaStore.FileBrowser.FileBrowserColumns.FILE_ID;
        mediaSet.idColumnType = Cursor.FIELD_TYPE_STRING;
        mediaSet.include = include;
        if (fileSelections != null) {
            mediaSet.ids = fileSelections.clone();
        } else {
            mediaSet.ids = new String[0];
        }
        mediaSet.sortOrder = sortOrder;

        return mediaSet;
    }

    /**
     * Creates a media set from a list of {@link CloudGatewayMediaStore} records.
     * 
     * @param slinkMediaStoreIds
     *            ids of rows in {@link CloudGatewayMediaStore}
     * @return
     *         the media set
     */
    public static CloudGatewayMediaSet createFromCloudGatewayMediaStoreIds(long[] slinkMediaStoreIds) {
        CloudGatewayMediaSet mediaSet = new CloudGatewayMediaSet();
        mediaSet.uri = CloudGatewayMediaStore.Files.CONTENT_URI;
        mediaSet.idColumnName = CloudGatewayMediaStore.MediaColumns._ID;
        mediaSet.selection = null;
        mediaSet.selectionArgs = null;
        mediaSet.ids = CloudGatewayMediaSet.convertToStringArray(slinkMediaStoreIds);
        mediaSet.include = true;

        return mediaSet;
    }

    /**
     * Creates a media set that contains all media in the given query except those specified.
     * 
     * @param uri
     *            the uri of the query
     * @param selection
     *            the selection of the query
     * @param selectionArgs
     *            the selection args of the query
     * @param idsToExclude
     *            the ids of the records to exclude
     * @return
     *         the media set
     */
    public static CloudGatewayMediaSet createExcludeSet(
            Uri uri,
            String selection,
            String[] selectionArgs,
            long[] idsToExclude) {

        CloudGatewayMediaSet mediaSet = new CloudGatewayMediaSet();
        mediaSet.idColumnName = CloudGatewayMediaStore.MediaColumns._ID;
        mediaSet.uri = uri;
        mediaSet.selection = selection;
        mediaSet.selectionArgs = selectionArgs;
        mediaSet.ids = CloudGatewayMediaSet.convertToStringArray(idsToExclude);
        mediaSet.include = false;

        return mediaSet;
    }

    /**
     * Creates a media set from an Intent
     * 
     * @param intent
     *            the Intent where the media set was written
     * @return
     *         the media set or null of no media set was written to the given intent
     */
    public static CloudGatewayMediaSet createFromIntent(Intent intent) {
        //backwards compatibility version 1
        CloudGatewayMediaSet result = intent.getParcelableExtra("mediaSet");
        if (result != null) {
            return result;
        }

        //backwards compatibility version 2
        result = intent.getParcelableExtra("com.samsung.android.sdk.slinkcloud.extraMediaSet");
        if (result != null) {
            return result;
        }

        if (intent.getParcelableExtra(EXTRA_URI) == null) {
            return null;
        }

        result = new CloudGatewayMediaSet();
        result.uri = intent.getParcelableExtra(EXTRA_URI);
        result.idColumnName = intent.getStringExtra(EXTRA_ID_COLUMN_NAME);
        if (TextUtils.isEmpty(result.idColumnName)) {
            result.idColumnName = CloudGatewayMediaStore.BaseSamsungLinkColumns._ID;
        }
        result.idColumnType = intent.getIntExtra(EXTRA_ID_COLUMN_TYPE, Cursor.FIELD_TYPE_INTEGER);
        result.selection = intent.getStringExtra(EXTRA_SELECTION);
        result.selectionArgs = intent.getStringArrayExtra(EXTRA_SELECTION_ARGS);
        result.ids = intent.getStringArrayExtra(EXTRA_IDS);
        result.include = intent.getBooleanExtra(EXTRA_INCLUDE, true);
        result.sortOrder = intent.getStringExtra(EXTRA_SORT_ORDER);
        result.localFilePaths = intent.getStringArrayExtra(EXTRA_LOCAL_FILE_PATHS);
        // Add new field here

        return result;
    }

    /**
     * Method to write this CloudGatewayMediaSet to the given Intent's extras.
     * 
     * @param intent
     *            The Intent to write to.
     */
    public void writeToIntent(Intent intent) {
        intent.putExtra(EXTRA_URI, this.uri);
        intent.putExtra(EXTRA_ID_COLUMN_NAME, this.idColumnName);
        intent.putExtra(EXTRA_ID_COLUMN_TYPE, this.idColumnType);
        intent.putExtra(EXTRA_SELECTION, this.selection);
        intent.putExtra(EXTRA_SELECTION_ARGS, this.selectionArgs);
        intent.putExtra(EXTRA_IDS, this.ids);
        intent.putExtra(EXTRA_INCLUDE, this.include);
        intent.putExtra(EXTRA_SORT_ORDER, this.sortOrder);
        intent.putExtra(EXTRA_LOCAL_FILE_PATHS, this.localFilePaths);

        // Add new field here
    }

    private CloudGatewayMediaSet() {
    }

    /**
     * Getter for the Uri to be used to retrieve the set of media records.
     * 
     * @return the Uri
     */
    public Uri getUri() {
        return this.uri;
    }

    /**
     * Getter for the selection statement used to retrieve the set of media records.
     * 
     * @return the selection statement
     */
    public String getSelection() {
        return this.selection;
    }

    /**
     * Getter for the selection statement bound arguments used to retrieve the set of media records.
     * 
     * @return the array of bound arguments
     */
    public String[] getSelectionArgs() {
        return this.selectionArgs;
    }

    /**
     * Getter for an array of "id" values. If {@link #isInclude()} is true, then these id's should be considered the absolute set of selected records. If
     * {@link #isInclude()} is false, the selected set should be considered to be the inverse of this array.
     * 
     * @return the array of "id" values.
     */
    public String[] getIds() {
        return this.ids;
    }

    /**
     * Getter for an array of local file paths that represent an absolute set of media files.
     * 
     * @return the array of file paths
     */
    public String[] getLocalFilePaths() {
        return this.localFilePaths;
    }

    /**
     * Getter for the sort order parameter to use when using a Cursor-based media set.
     * 
     * @return the sort order parameter
     */
    public String getSortOrder() {
        return this.sortOrder;
    }

    /**
     * Getter for the column name of the "id" field. This column name is used in conjunction with {@link #getIds()} to determine the selection set. The default
     * value of this field, if not supplied is {@link android.provider.BaseColumns#_ID}.
     * 
     * @return the id field column name.
     */
    public String getIdColumnName() {
        return this.idColumnName;
    }

    /**
     * Getter for the data type of the "id" field. The default type, if not supplied is {@link android.database.Cursor#FIELD_TYPE_INTEGER}. Currently the
     * supported types are:
     * <ul>
     * <li>{@link android.database.Cursor#FIELD_TYPE_INTEGER}</li>
     * <li>{@link android.database.Cursor#FIELD_TYPE_STRING}</li>
     * </ul>
     * 
     * @return the data type of the "id" field.
     */
    public int getIdColumnType() {
        return this.idColumnType;
    }

    /**
     * Getter to determine whether or not the supplied "ids" array should be used as the set or inverse set.
     * 
     * @return true if the "ids" array is the actual set. false if the "ids" array is the inverse set.
     */
    public boolean isInclude() {
        return this.include;
    }

    /**
     * Utility method used to classify this CloudGatewayMediaSet.
     * 
     * @return true if this CloudGatewayMediaSet is a set of row ids to be used with the Samsung Link ContentProvider.
     */
    public boolean isCloudGatewayUri() {
        return getUri().getAuthority().equals(CloudGatewayMediaStore.AUTHORITY);
    }

    /**
     * Utility method used to classify this CloudGatewayMediaSet.
     * 
     * @return true if this CloudGatewayMediaSet is a set of local file paths.
     * @see #createFromLocalFilePaths(String[])
     */
    public boolean isLocalFilePathsMediaSet() {
        return getUri().getScheme().equals(ContentResolver.SCHEME_FILE);
    }

    /**
     * @deprecated This class' Parcelable interface should not be used anymore for new code. It is still parcelled to support legacy code. Please use
     *             {@link #writeToIntent(Intent)} and {@link #createFromIntent(Intent)} instead.
     */
    @Override
    @Deprecated
    public int describeContents() {
        return 0;
    }

    /**
     * @deprecated This class' Parcelable interface should not be used anymore for new code. It is still parcelled to support legacy code. Please use
     *             {@link #writeToIntent(Intent)} and {@link #createFromIntent(Intent)} instead.
     */
    @Deprecated
    /*
     * !!!!!!!!!!! DO NOT MODIFY - THIS MUST REMAIN HERE UNCHANGED FOR BACKWARDS COMPATABILITY ONLY !!!!!!!!!!!!!!!!!
     */
    private CloudGatewayMediaSet(Parcel in) {

        this.uri = in.readParcelable(null);
        this.selection = in.readString();
        this.selectionArgs = in.createStringArray();
        this.ids = in.createStringArray();
        this.idColumnName = in.readString();
        this.include = in.readInt() == 1 ? true : false;
        if (TextUtils.isEmpty(this.idColumnName)) {
            this.idColumnName = CloudGatewayMediaStore.BaseSamsungLinkColumns._ID;
        }
    }

    /**
     * @deprecated This class' Parcelable interface should not be used anymore for new code. It is still parcelled to support legacy code. Please use
     *             {@link #writeToIntent(Intent)} and {@link #createFromIntent(Intent)} instead.
     */
    @Override
    @Deprecated
    /*
     * !!!!!!!!!!! DO NOT MODIFY - THIS MUST REMAIN HERE UNCHANGED FOR BACKWARDS COMPATABILITY ONLY !!!!!!!!!!!!!!!!!
     */
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeParcelable(this.uri, 0);
        dest.writeString(this.selection);
        dest.writeStringArray(this.selectionArgs);
        dest.writeStringArray(this.ids);
        dest.writeString(this.idColumnName);
        dest.writeInt(this.include ? 1 : 0);
    }

    /**
     * @deprecated This class' Parcelable interface should not be used anymore for new code. It is still parcelled to support legacy code. Please use
     *             {@link #writeToIntent(Intent)} and {@link #createFromIntent(Intent)} instead.
     */
    @Deprecated
    public static final Parcelable.Creator<CloudGatewayMediaSet> CREATOR = new Parcelable.Creator<CloudGatewayMediaSet>() {

        @Override
        public CloudGatewayMediaSet createFromParcel(Parcel in) {
            return new CloudGatewayMediaSet(in);
        }

        @Override
        public CloudGatewayMediaSet[] newArray(int size) {
            return new CloudGatewayMediaSet[size];
        }
    };

    private static String[] convertToStringArray(long[] vals) {
        if (vals == null) {
            return new String[] {};
        }

        String[] result = new String[vals.length];
        for (int i = 0; i < vals.length; i++) {
            result[i] = String.valueOf(vals[i]);
        }
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(128);
        sb.append("CloudGatewayMediaSet:{");
        sb.append("uri:").append(this.uri).append(',');
        sb.append("selection:").append(this.selection).append(',');
        sb.append("selectionArgs:").append(Arrays.toString(this.selectionArgs)).append(',');
        sb.append("sortOrder:").append(this.sortOrder).append(',');
        sb.append("include:").append(this.include).append(',');
        sb.append("ids:").append(Arrays.toString(this.ids)).append(',');
        sb.append("idColumnName:").append(this.idColumnName).append(',');
        sb.append("idColumnType:").append(this.idColumnType).append(',');
        sb.append("localFilePaths:").append(Arrays.toString(this.localFilePaths));
        sb.append('}');

        return sb.toString();
    }
}

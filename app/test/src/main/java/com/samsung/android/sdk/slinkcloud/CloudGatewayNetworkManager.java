
package com.samsung.android.sdk.slinkcloud;

import android.content.Context;
import android.util.Log;

/**
 * This class provides the primary API for managing the Cloud Manager network.
 */
public final class CloudGatewayNetworkManager {
    private static final String TAG = "mfl_ApiLib_" + CloudGatewayNetworkManager.class.getSimpleName();

    private static CloudGatewayNetworkManager sInstance;
    private final Context mContext;

    /**
     * declared to be private to hide it from the outside world
     */
    private CloudGatewayNetworkManager(Context context) {
        this.mContext = context.getApplicationContext();
    }

    /**
     * Return a handle to Cloud Manager's network management facilities
     *
     * @param context
     *            Context
     * @return a handle to Cloud Manager's network management facilities
     * @throws NullPointerException
     *             if context is null
     */
    public static synchronized CloudGatewayNetworkManager getInstance(Context context) {
        if (context == null) {
            throw new NullPointerException("context is null");
        }
        if (sInstance == null) {
            sInstance = new CloudGatewayNetworkManager(context);
        }
        return sInstance;
    }

    /**
     * Requests a refresh of all devices
     */
    public void requestRefresh() {
        try {
            this.mContext.getContentResolver().call(
                    CloudGatewayMediaStore.CallMethods.CONTENT_URI,
                    CloudGatewayMediaStore.CallMethods.RequestNetworkRefresh.NAME,
                    null,
                    null);
        } catch (IllegalArgumentException e) {
            ////tskim fix stress test issue when called after platform disabling
            Log.e(TAG, "::requestRefresh maybe platform is disabled");
        }
    }
}

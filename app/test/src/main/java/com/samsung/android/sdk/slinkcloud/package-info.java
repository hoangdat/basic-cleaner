/**
 * Provides convenience classes to access content supplied by the Samsung Link application.
 */

package com.samsung.android.sdk.slinkcloud;

package com.samsung.android.sdk.slinkcloud;

import android.content.ContentValues;
import android.database.Cursor;
import android.util.Log;

import com.samsung.android.sdk.slinkcloud.CloudGatewayMediaStore.DeviceColumns;

/**
 * Represent all possible transport types
 */
public enum CloudGatewayDeviceTransportType {
    /**
     * The local device
     */
    LOCAL,

    /**
     * A web storage device (i.e. SugarSync)
     */
    WEB_STORAGE,

    /**
     * An unknown transport type
     */
    UNKNOWN;

    private static final String TAG = CloudGatewayDeviceTransportType.class.getSimpleName();

    /**
     * Gets the CloudGatewayDeviceTransportType for the given Device record.
     *
     * @param cursor a Cursor that is currently positioned to a Device record
     * @return the CloudGatewayDeviceTransportType for the given Device record
     * @throws IllegalArgumentException if the given Cursor record does not contain the {@link DeviceColumns#TRANSPORT_TYPE} column.
     */
    public static CloudGatewayDeviceTransportType getDeviceTransportType(Cursor cursor) {
        int columnIndex = cursor.getColumnIndexOrThrow(DeviceColumns.TRANSPORT_TYPE);
        String value = cursor.getString(columnIndex);
        try {
            return CloudGatewayDeviceTransportType.valueOf(value);
        } catch (IllegalArgumentException e) {
            Log.w(TAG, "Unrecognized value for deviceTransportType: " + value, e);
            return UNKNOWN;
        }
    }

    /**
     * Populates the given {@link ContentValues} with the {@link DeviceColumns#TRANSPORT_TYPE} column for this enum value.
     *
     * @param values the {@link ContentValues} to populate
     */
    public void toContentValues(ContentValues values) {
        values.put(DeviceColumns.TRANSPORT_TYPE, name());
    }
}


package com.samsung.android.sdk.slinkcloud;

import android.content.ContentValues;
import android.database.Cursor;
import android.text.TextUtils;
import android.util.Log;

import com.samsung.android.sdk.slinkcloud.CloudGatewayMediaStore.DeviceColumns;

/**
 * Represent all possible network modes
 */
public enum CloudGatewayNetworkMode {
    /**
     * No network available (offline)
     */
    OFF,

    /**
     * WIFI connection
     */
    WIFI,

    /**
     * 2G connection
     */
    MOBILE_2G,

    /**
     * 3G connection
     */
    MOBILE_3G,

    /**
     * LTE connection
     */
    MOBILE_LTE,

    /**
     * Unknown network mode
     */
    UNKNOWN,

    /**
     * Bluetooth connection
     */
    BLUETOOTH;

    private static final String TAG = CloudGatewayNetworkMode.class.getSimpleName();

    /**
     * Gets the CloudGatewayNetworkMode for the given Device record.
     * 
     * @param cursor
     *            a Cursor that is currently positioned to a Device record
     * @return
     *         the CloudGatewayNetworkMode for the given Device record
     * @throws IllegalArgumentException
     *             if the given Cursor record does not contain the {@link DeviceColumns#NETWORK_MODE} column.
     */
    public static CloudGatewayNetworkMode getNetworkMode(Cursor cursor) {
        try {
            int columnIndex = cursor.getColumnIndexOrThrow(DeviceColumns.NETWORK_MODE);
            String value = cursor.getString(columnIndex);
            if (TextUtils.isEmpty(value)) {
                return CloudGatewayNetworkMode.OFF;
            }
            return CloudGatewayNetworkMode.valueOf(value);
        } catch (Exception e) {
            e.printStackTrace();
            Log.w(TAG, "Unrecognized value for network mode: ", e);
            return UNKNOWN;
        }
    }

    /**
     * Populates the given {@link ContentValues} with the {@link DeviceColumns#NETWORK_MODE} column for this enum value.
     * 
     * @param values
     *            the {@link ContentValues} to populate
     */
    public void toContentValues(ContentValues values) {
        values.put(DeviceColumns.NETWORK_MODE, name());
    }
}

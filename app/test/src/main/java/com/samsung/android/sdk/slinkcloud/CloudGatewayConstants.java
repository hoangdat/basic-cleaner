
package com.samsung.android.sdk.slinkcloud;

/**
 * Commonly used constants for Samsung Link Platform
 */
public class CloudGatewayConstants {
    /**
     * Intent action to start a short-lived activity to display an error message after failing to authenticate against a web storage service.
     */
    public static final String ACTION_CLOUD_AUTHENTICATION_FAILURE = "com.samsung.android.sdk.slinkcloud.CloudGatewayConstants.ACTION_CLOUD_AUTHENTICATION_FAILURE";

    /**
     * Theme definition - intent extra parameter key
     */
    public static final String SLINK_UI_APP_THEME = "com.samsung.android.sdk.slinkcloud.SLINK_UI_APP_THEME";

    public static final String SIGN_IN_ACCOUNT = "com.samsung.android.sdk.slinkcloud.SIGN_IN_ACCOUNT";

    public static final String ADD_ACCOUNT = "com.samsung.android.sdk.slinkcloud.ADD_ACCOUNT";

    /**
     * Intent extra to pass along a device id.
     */
    public static final String DEVICE_ID_EXTRA_KEY = "com.samsung.android.sdk.slinkcloud.DEVICE_ID_EXTRA_KEY";

    public static final String APPLICATION_ID = "com.samsung.android.sdk.slinkcloud.APPLICATION_ID";
    public static final String SLINKCLOUD_PLATFORM_PACKAGE_NAME = "com.samsung.android.slinkcloud";
    public static final String SLINK_PLATFORM_PROVIDER = "com.samsung.android.slinkcloud.provider.cloudgateway";
    ////one apk pcw, merge platform jar into pcw
    public static String TARGET_APK_PACKAGE = SLINKCLOUD_PLATFORM_PACKAGE_NAME;

    public enum ClientApp {
        //        SLINK_UI_APP(200),
//        GALLERY_APP(201),
//        MUSIC_APP(202),
//        VIDEO_APP(203),
        SLINK_PLATFORM_SAMPLE_APP(2050),
        NONE(-1);

        private final int value;

        ClientApp(int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }
    }

    public enum OperationType {
        NONE,
        DOWNLOAD,
        UPLOAD,
        COPY,
        MOVE,
        DELETE
    }
}


package com.samsung.android.sdk.slinkcloud;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

public class CloudGatewayFileBrowserUtils {

    private static final String TAG = "mfl_ApiLib_" + CloudGatewayFileBrowserUtils.class.getSimpleName();
    private static CloudGatewayFileBrowserUtils sInstance;
    private final Context context;

    public static final int REMOTE_FOLDER_MKDIR = 1;
    public static final int REMOTE_FOLDER_RMDIR = 2;
    public static final int REMOTE_FOLDER_RENAME = 3;
    public static final int REMOTE_FOLDER_MOVE = 4;
    public static final int REMOTE_FOLDER_COPY = 5;
    public static final int REMOTE_FOLDER_SYNC = 6;
    public static final int REMOTE_TRASH_EMPTY = 7;
    public static final int REMOTE_TRASH_RESTORE = 8;
    public static final int REMOTE_TRASH_DELETE = 9;

    public static final int REMOTE_FOLDER_MKDIR_BATCH = 11;
    public static final int REMOTE_FOLDER_RMDIR_BATCH = 12;
    public static final int REMOTE_FOLDER_MOVE_BATCH = 14;
    public static final int REMOTE_FOLDER_COPY_BATCH = 15;

    public static final int REMOTE_TRASH_DELETE_BATCH = 16;
    public static final int REMOTE_TRASH_RESTORE_BATCH = 17;
    public static final int REMOTE_FOLDER_RMDIR_PERMANENTLY_BATCH = 18;

    public static final int REMOTE_FOLDER_RMDIR_BATCH_CANCEL = 21;

    public static final int ERROR_NONE = -1;
    public static final int ERROR_OUT_OF_STORAGE = 1000;
    public static final int ERROR_REACH_MAX_ITEM = 1001;

    public static final String SYNC_START = "sync_start";
    public static final String SYNC_FINISHED = "sync_finished";

    public enum SyncResult {
        SYNC_SUCCESS,
        NO_DELTA_METADATA,
        WIFI_NETWORK_ERROR,
        NO_NETWORK_ERROR,
        SYNC_FAIL
    }

    public interface QuotaError {
        void onQuotaError(int error, int cmd);
    }

    /**
     * Gets the singleton CloudGatewayUserSettings instance.
     *
     * @param context Context
     * @return the singleton CloudGatewayFileBrowserUtils instance
     * @throws NullPointerException if context is null
     */
    public static synchronized CloudGatewayFileBrowserUtils getInstance(Context context) {
        if (context == null) {
            throw new NullPointerException("context is null");
        }
        if (sInstance == null) {
            sInstance = new CloudGatewayFileBrowserUtils(context);
        }
        return sInstance;
    }

    /**
     * Private constructor.
     *
     * @param context Context
     */
    private CloudGatewayFileBrowserUtils(Context context) {
        this.context = context.getApplicationContext();
    }

    /**
     * Mkdir/Rename/RmDir: control remote device's folder
     * synchronous call, caller should use a thread or AsyncTask to handle the response.
     *
     * @return boolean
     */
    public boolean mrrControlCommand(
            int deviceId,
            int nCmd,
            String parentDirectoryId,
            String sourceId,
            String newItem,
            QuotaError errorCallback) {

        Bundle extras = new Bundle();
        extras.putLong(CloudGatewayMediaStore.CallMethods.MRRControl.INTENT_ARG_DEVICEID, deviceId);
        extras.putInt(CloudGatewayMediaStore.CallMethods.MRRControl.INTENT_ARG_CMD, nCmd);

        extras.putString(CloudGatewayMediaStore.CallMethods.MRRControl.INTENT_ARG_PARENTDIRECTORYID, parentDirectoryId);
        extras.putString(CloudGatewayMediaStore.CallMethods.MRRControl.INTENT_ARG_SOURCEID, sourceId);
        extras.putString(CloudGatewayMediaStore.CallMethods.MRRControl.INTENT_ARG_NEWITEM, newItem);

        Bundle result = null;
        try {
            result = context.getContentResolver().call(
                    CloudGatewayMediaStore.CallMethods.CONTENT_URI,
                    CloudGatewayMediaStore.CallMethods.MRRControl.NAME,
                    null,
                    extras);
        } catch (Exception e) {
            ////tskim fix stress test issue when called after platform disabling
            Log.e(TAG, "Exception ::maybe platform disabled ");
            e.printStackTrace();
        }
        ////prevent FC
        if (result == null) {
            Log.e(TAG, "::mrrControlCommand result is null");
            return false;
        }

        int errType = result.getInt(CloudGatewayMediaStore.CallMethods.KEY_RESULT_QUOTA_ERROR, ERROR_NONE);
        if (errType != ERROR_NONE && errorCallback != null) {
            errorCallback.onQuotaError(errType, nCmd);
        }

        return result.getBoolean(CloudGatewayMediaStore.CallMethods.KEY_RESULT);
    }

    public boolean mrrControlBatchCommand(
            int deviceId,
            int nCmd,
            String[] sourceIds,
            QuotaError errorCallback) {
        Bundle extras = new Bundle();
        extras.putLong(CloudGatewayMediaStore.CallMethods.MRRControlBatch.INTENT_ARG_BATCH_DEVICEID, deviceId);
        extras.putInt(CloudGatewayMediaStore.CallMethods.MRRControlBatch.INTENT_ARG_BATCH_CMD, nCmd);

        extras.putStringArray(CloudGatewayMediaStore.CallMethods.MRRControlBatch.INTENT_ARG_BATCH_SOURCEID, sourceIds);

        Bundle result = null;
        try {
            result = context.getContentResolver().call(
                    CloudGatewayMediaStore.CallMethods.CONTENT_URI,
                    CloudGatewayMediaStore.CallMethods.MRRControlBatch.NAME,
                    null,
                    extras);
        } catch (Exception e) {
            ////tskim fix stress test issue when called after platform disabling
            Log.e(TAG, "Exception ::maybe platform disabled ");
            e.printStackTrace();
        }
        ////prevent FC
        if (result == null) {
            Log.e(TAG, "::mrrControlBatchCommand result is null");
            return false;
        }

        int errType = result.getInt(CloudGatewayMediaStore.CallMethods.KEY_RESULT_QUOTA_ERROR, ERROR_NONE);
        if (errType != ERROR_NONE && errorCallback != null) {
            errorCallback.onQuotaError(errType, nCmd);
        }

        return result.getBoolean(CloudGatewayMediaStore.CallMethods.KEY_RESULT);
    }

    /**
     * Mkdir/Rename/RmDir: control remote device's folder
     * synchronous call, caller should use a thread or AsyncTask to handle the response.
     *
     * @return String
     */
    public String mrrControlCommandWithStringReturn(
            int deviceId,
            int nCmd,
            String parentDirectoryId,
            String sourceId,
            String newItem,
            QuotaError errorCallback) {
        Bundle extras = new Bundle();
        extras.putLong(CloudGatewayMediaStore.CallMethods.MRRControl.INTENT_ARG_DEVICEID, deviceId);
        extras.putInt(CloudGatewayMediaStore.CallMethods.MRRControl.INTENT_ARG_CMD, nCmd);

        extras.putString(CloudGatewayMediaStore.CallMethods.MRRControl.INTENT_ARG_PARENTDIRECTORYID, parentDirectoryId);
        extras.putString(CloudGatewayMediaStore.CallMethods.MRRControl.INTENT_ARG_SOURCEID, sourceId);
        extras.putString(CloudGatewayMediaStore.CallMethods.MRRControl.INTENT_ARG_NEWITEM, newItem);

        Bundle result = null;
        try {
            result = this.context.getContentResolver().call(
                    CloudGatewayMediaStore.CallMethods.CONTENT_URI,
                    CloudGatewayMediaStore.CallMethods.MRRControl.NAME,
                    null,
                    extras);
        } catch (Exception e) {
            ////tskim fix stress test issue when called after platform disabling
            Log.e(TAG, "Exception ::maybe platform disabled ");
            e.printStackTrace();
        }
        ////prevent FC
        if (result == null) {
            Log.e(TAG, "::mrrControlCommandWithStringReturn result is null");
            return null;
        }

        int errType = result.getInt(CloudGatewayMediaStore.CallMethods.KEY_RESULT_QUOTA_ERROR, ERROR_NONE);
        if (errType != ERROR_NONE && errorCallback != null) {
            errorCallback.onQuotaError(errType, nCmd);
        }

        String strRet = result.getString(CloudGatewayMediaStore.CallMethods.KEY_RESULT_STR);
        Log.d(TAG, "::mrrControlCommandWithStringReturn mkdir result : " + strRet);
        return strRet;
    }

}

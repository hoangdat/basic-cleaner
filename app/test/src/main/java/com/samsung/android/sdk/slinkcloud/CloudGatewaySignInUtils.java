
package com.samsung.android.sdk.slinkcloud;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.os.Bundle;
import android.util.Log;

import com.samsung.android.sdk.slinkcloud.CloudGatewayMediaStore.CallMethods;

import java.util.ArrayList;

/**
 * Utility class for providing a way to launch SamsungLink Intro/Login screen
 */
public class CloudGatewaySignInUtils {
    private static final String TAG = "mfl_ApiLib_" + CloudGatewaySignInUtils.class.getSimpleName();
    /**
     * Intent action to launch the Samsung Link Platform upgrade Activity. This Activity will check for updates and download the latest APK if needed and
     * install it.
     */
    public static final String ACTION_SAMSUNG_LINK_PLATFORM_UPGARDE = "com.samsung.android.sdk.slinkcloud.CloudGatewaySignInUtils.ACTION_SAMSUNG_LINK_PLATFORM_UPGARDE";

    public static final String ACTIVITY_CLOUD_ACCESS_SETTING = "com.samsung.android.sdk.slinkcloud.CloudGatewaySignInUtils.CLOUDSETTING";

    private static CloudGatewaySignInUtils sInstance;

    private final Context context;

    /**
     * Gets the singleton CloudGatewaySignInUtils instance.
     *
     * @param context Context
     * @return the singleton CloudGatewaySignInUtils instance
     * @throws NullPointerException if context is null
     */
    public static synchronized CloudGatewaySignInUtils getInstance(Context context) {
        if (context == null) {
            throw new NullPointerException("context is null");
        }
        if (sInstance == null) {
            sInstance = new CloudGatewaySignInUtils(context);
        }
        return sInstance;
    }

    /**
     * Private constructor.
     *
     * @param context Context
     */
    private CloudGatewaySignInUtils(Context context) {
        this.context = context.getApplicationContext();
    }

    /**
     * Returns true if user is already signed into Samsung Link
     *
     * @return a boolean status whether user is already signed in
     */
    public boolean isSignedIn() {
        Log.v(TAG, "::isSignedIn()");
        Bundle result = null;
        ////check the null pointer to prevent FC error
        if (this.context == null || this.context.getContentResolver() == null) {
            Log.e(TAG, "::isSignedIn context is null");
            return false;
        }
        try {
            result = this.context.getContentResolver().call(
                    CallMethods.CONTENT_URI,
                    CallMethods.GetSignInStatus.NAME,
                    null,
                    null);
        } catch (IllegalArgumentException e) {
            Log.e(TAG, "::isSignedIn IllegalArgumentException", e);
            return false;
        }
        ////check the null pointer to prevent FC error
        if (result == null) {
            Log.e(TAG, "::isSignedIn result is null");
            return false;
        }
        return result.getBoolean(CallMethods.KEY_RESULT);
    }

    public boolean checkProviderInit() {
        Log.v(TAG, "::checkProviderInit()");

        ////check the null pointer to prevent FC error
        if (this.context == null || this.context.getContentResolver() == null) {
            Log.e(TAG, "::isSignedIn context is null");
            return false;
        }
        try {
            final String packageName = context.getPackageName();
            this.context.getContentResolver().call(
                    CallMethods.CONTENT_URI,
                    CallMethods.ProviderReservation.NAME,
                    packageName,
                    null);
        } catch (Exception e) {
            Log.e(TAG, "::checkProviderInit Exception=", e);
            return false;
        }
        return true;
    }

    /**
     * Creates a new Intent to start the Samsung Link Platform's upgrade check process. This launches a new Activity.
     *
     * @return the newly created Intent
     * @see #ACTION_SAMSUNG_LINK_PLATFORM_UPGARDE
     */
    public Intent getPlatformUpgradeIntent() {
        Intent intent = new Intent(ACTION_SAMSUNG_LINK_PLATFORM_UPGARDE);
        //// android L accept only explicit Intent
        intent.setComponent(new ComponentName(
                CloudGatewayConstants.TARGET_APK_PACKAGE,
                "platform.com.mfluent.asp.ui.UpgradeActivity"));
//        CloudGatewayCommonUtils.getInstance(this.context).addAppIdToIntent(intent);
        return intent;
    }

    public Intent getCloudSettingIntent(String storageTypeList) {
        Intent intent = new Intent(ACTIVITY_CLOUD_ACCESS_SETTING);
        //// android L accept only explicit Intent
        intent.setComponent(new ComponentName(
                CloudGatewayConstants.TARGET_APK_PACKAGE,
                "platform.com.mfluent.asp.ui.CloudAccessSettingActivity"));
        intent.putExtra("STORAGE_TYPE_LIST", storageTypeList);
//        CloudGatewayCommonUtils.getInstance(this.context).addAppIdToIntent(intent);
        return intent;
    }

    /**
     * Returns whether or not the Samsung Link Platform is installed and enabled on the device.
     *
     * @return true if the application is installed and not disabled
     */
    public boolean isPlatformEnabled() {
        boolean bRet = false;
        ApplicationInfo ai;

        try {
            ai = this.context.getPackageManager().getApplicationInfo(
                    CloudGatewayConstants.SLINKCLOUD_PLATFORM_PACKAGE_NAME,
                    0);
            if (ai != null && ai.enabled == true) {
                bRet = true;
            }
        } catch (Exception e) {
            Log.d(TAG, "CloudGateway Not Support Model : " + e.getMessage());
        }

        return bRet;
    }

    /**
     * Fetch remote devices which has completed initial sync
     *
     * @return ArrayList of device IDs
     */
    public ArrayList<Integer> getInitialSyncDoneDevices() {
        Log.v(TAG, "::Enter getInitialSyncDoneDevices()");

        if (this.context == null) {
            Log.e(TAG, "::getInitialSyncDoneDevices context is null");
            return null;
        }
        Bundle result = null;
        try {
            result = this.context.getContentResolver().call(
                    CallMethods.CONTENT_URI,
                    CallMethods.GetInitialSyncDoneDevices.NAME,
                    null,
                    null);
        } catch (Exception e) {
            Log.e(TAG, "::getInitialSyncDoneDevices failed with exception.", e);
        }
        if (result == null) {
            Log.e(TAG, "::getInitialSyncDoneDevices result is null");
            return null;
        }
        return result.getIntegerArrayList(CallMethods.KEY_RESULT);
    }

    public void setWifiOnlyMode(boolean bEnable) {
        Bundle extras = new Bundle(1);
        extras.putBoolean(
                CallMethods.SetWifiOnly.INTENT_ARG_ENABLE_BOOLEAN,
                bEnable);
        try {
            this.context.getContentResolver().call(
                    CallMethods.CONTENT_URI,
                    CallMethods.SetWifiOnly.NAME,
                    null,
                    extras);
        } catch (Exception e) {
            ////tskim fix stress test issue when called after platform disabling
            Log.e(TAG, "::setWifiOnlyMode error=" + e);
        }
    }

    public boolean getWifiOnlyMode() {
        boolean bRet = true;
        try {
            Bundle result = this.context.getContentResolver().call(
                    CallMethods.CONTENT_URI,
                    CallMethods.SetWifiOnly.NAME,
                    null,
                    null);
            if (result != null)
                bRet = result.getBoolean("RET_ENABLE_BOOLEAN", bRet);
        } catch (Exception e) {
            ////tskim fix stress test issue when called after platform disabling
            Log.e(TAG, "::getWifiOnlyMode error=" + e);
        }
        return bRet;
    }

    public void keepServiceAlive(boolean bEnable) {
        Bundle extras = new Bundle(1);
        extras.putBoolean(
                CallMethods.KeepServiceAlive.INTENT_ARG_ENABLE_BOOLEAN,
                bEnable);
        extras.putString(CallMethods.KeepServiceAlive.INTENT_ARG_PKGNAME, context.getPackageName());
        try {
            context.getContentResolver().call(
                    CallMethods.CONTENT_URI,
                    CallMethods.KeepServiceAlive.NAME,
                    null,
                    extras);
        } catch (Exception e) {
            ////tskim fix stress test issue when called after platform disabling
            Log.e(TAG, "::keepServiceAlive error=" + e);
        }
    }

}

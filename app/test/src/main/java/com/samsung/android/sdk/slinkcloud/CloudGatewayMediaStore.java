
package com.samsung.android.sdk.slinkcloud;

import android.content.ContentResolver;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;

import com.samsung.android.sdk.slinkcloud.CloudGatewayMediaStore.Files.FileColumns;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Provides access to meta data for all available media for devices that are
 * associated with the user's Samsung account and have enabled access to Samsung
 * Link. This includes cloud storage accounts that the user has associated with
 * Samsung Link, such as Dropbox and SkyDrive.
 */
public class CloudGatewayMediaStore {

    public static boolean ENABLE_LOGGING = false;

    private static final String TAG = "mfl_ApiLib_" + CloudGatewayMediaStore.class.getSimpleName();
    /**
     * The Samsung Link content provider's authority value.
     */
    public static String AUTHORITY = CloudGatewayConstants.SLINK_PLATFORM_PROVIDER;

    /**
     * For use with the {@link ContentResolver#query(Uri, String[], String, String[], String)} method. Append to the Uri as a query string parameter to put a
     * hard upper
     * limit on the number of rows returned in the cursor. The value should be
     * an integer.
     */
    public static final String QUERY_STR_LIMIT = "limit";

    /**
     * Builds a content type String.
     */
    protected static final String buildContentType(String path) {
        return ContentResolver.CURSOR_DIR_BASE_TYPE + "/vnd.samsunglink." + path;
    }

    /**
     * Builds an entry content type String.
     */
    protected static final String buildEntryContentType(String path) {
        return ContentResolver.CURSOR_ITEM_BASE_TYPE + "/vnd.samsunglink." + path;
    }

    /**
     * Builds a content uri for Samsung Link content.
     *
     * @param path the path of the content
     * @return the content uri
     */
    public static final Uri buildContentUri(String path) {
        return Uri.parse(ContentResolver.SCHEME_CONTENT + "://" + AUTHORITY + "/" + path);
    }

    /**
     * Builds a file uri.
     *
     * @return the file uri
     */
    public static final Uri buildFileUri() {
        return Uri.parse(ContentResolver.SCHEME_FILE + "://" + AUTHORITY);
    }

    /**
     * Builds an entry uri for a single entry of each device
     *
     * @param deviceId the id of the device
     * @param entryId  the id of the media item
     * @param path     the path of the content
     * @return the entry uri
     */

    protected static final Uri buildEntryIdUriForDevice(long deviceId, long entryId, String path) {
        return Uri.parse(ContentResolver.SCHEME_CONTENT
                + "://"
                + AUTHORITY
                + "/"
                + path
                + "/device/"
                + +deviceId
                + "/entry/"
                + entryId);
    }

    /**
     * Builds an entry uri for a single Samsung Link media item.
     *
     * @param entryId the id of the media item
     * @param path    the path of the content
     * @return the entry uri
     */
    protected static final Uri buildEntryIdUri(long entryId, String path) {
        return Uri.parse(ContentResolver.SCHEME_CONTENT
                + "://"
                + AUTHORITY
                + "/"
                + path
                + "/entry/"
                + entryId);
    }

    /**
     * Builds a content uri for a particular device.
     *
     * @param deviceId the id of the device
     * @param path     the path of the content
     * @return the content uri
     */
    protected static final Uri buildDeviceContentUri(long deviceId, String path) {
        return Uri.parse(ContentResolver.SCHEME_CONTENT
                + "://"
                + AUTHORITY
                + "/"
                + path
                + "/device/"
                + deviceId);
    }

    /**
     * Fields for device table.
     */
    public interface DeviceColumns extends BaseColumns {

        /**
         * The alias name of the device
         * <p/>
         * Type: TEXT
         * </P>
         */
        String ALIAS_NAME = "alias_name";

        /**
         * The transport type of the device. See {@link CloudGatewayDeviceTransportType} for possible values.
         * <p/>
         * Type: TEXT
         * </P>
         */
        String TRANSPORT_TYPE = "transport_type";

        /**
         * The current type of network connection that the device is using. See {@link CloudGatewayNetworkMode} for possible values. This value is only
         * valid for devices where TRANSPORT_TYPE is SLINK.
         * <p/>
         * Type: TEXT
         * </P>
         */
        String NETWORK_MODE = "network_mode";

        /**
         * The order of priority for the device, based on the transport-type. 0
         * is the highest priority and lower priorities have a higher integer.
         * <p/>
         * Type: INTEGER
         * </P>
         */
        String DEVICE_PRIORITY = "device_priority";

        /**
         * Boolean value indicating if the device's metadata is currently being
         * synced
         * <p/>
         * Type: INTEGER (boolean)
         * </P>
         */
        String IS_SYNCING = "is_syncing";

        /**
         * The web storage type of the device
         * <p/>
         * Type: TEXT
         * </P>
         */
        String WEB_STORAGE_TYPE = "web_storage_type";
        /**
         * The storage service isSignedIn flag
         * <p/>
         * Type: BOOLEAN (stored as Integer in db)
         * </P>
         */
        String WEB_STORAGE_IS_SIGNED_IN = "web_storage_is_signed_in_id";

        /**
         * The web storage userid
         * <p/>
         * Type: TEXTsss
         * </P>
         */
        String WEB_STORAGE_USER_ID = "web_storage_user_id";

        /**
         * The web storage total storage capacity
         * <p/>
         * Type: TEXT
         * </P>
         */
        String WEB_STORAGE_TOTAL_CAPACITY = "web_storage_total_capacity_id";

        /**
         * The web storage used storage capacity
         * <p/>
         * Type: TEXT
         * </P>
         */
        String WEB_STORAGE_USED_CAPACITY = "web_storage_used_capacity_id";

        /**
         * The storage sync enable flag
         * <p/>
         * Type: BOOLEAN (stored as Integer in db)
         * </P>
         */
        String WEB_STORAGE_ENABLE_SYNC = "web_storage_enable_sync";
    }

    /**
     * Table containing an index of all devices that are registered with Samsung
     * Link for the user's Samsung account.
     */
    public static class Device implements DeviceColumns {

        /**
         * Path for the Device table
         */
        public static final String PATH = "device";

        /**
         * Conent type for the Device table
         */
        public static final String CONTENT_TYPE = CloudGatewayMediaStore.buildContentType(PATH);

        /**
         * Entry content type for the Device table
         */
        public static final String ENTRY_CONTENT_TYPE = CloudGatewayMediaStore
                .buildEntryContentType(PATH);

        /**
         * Content uri for the Device table
         */
        public static final Uri CONTENT_URI = CloudGatewayMediaStore.buildContentUri(PATH);

        /**
         * Intent action for the device deleted broadcast
         */
        public static final String ACTION_DEVICE_DELETED_BROADCAST = "com.samsung.android.sdk.slinkcloud.device.DeviceDeleted";

        /**
         * Builds an entry uri for a specific device
         *
         * @param deviceId the id of the device
         * @return the entry uri
         */
        public static Uri getDeviceEntryUri(long deviceId) {
            return CloudGatewayMediaStore.buildEntryIdUri(deviceId, PATH);
        }

        /**
         * Get the name of a Device that is suitable for display to the user.
         *
         * @param cursor a Cursor that is currently positioned to a Device record
         * @return The display name of the device or null if no display name
         * could be determined.
         */
        public static String getDisplayName(Cursor cursor) {
            String aliasName = cursor.getString(cursor
                    .getColumnIndexOrThrow(CloudGatewayMediaStore.DeviceColumns.ALIAS_NAME));
            if (!TextUtils.isEmpty(aliasName)) {
                return aliasName;
            }

            return null;
        }
    }


    /**
     * Common fields for Samsung Link tables.
     */
    public interface BaseSamsungLinkColumns extends android.provider.BaseColumns {

        /**
         * The id (foreign key to device table) of the source device for this
         * media.
         * <p/>
         * Type: INTEGER (long)
         * </P>
         */
        String DEVICE_ID = "device_id";
    }

    /**
     * Common fields for media related tables.
     */
    public interface MediaColumns
            extends android.provider.MediaStore.MediaColumns, BaseSamsungLinkColumns {

        /**
         * The media type of the file. See {@link FileColumns} for valid values.
         * <p/>
         * Type: INTEGER
         * </P>
         */
        String MEDIA_TYPE = "media_type";

        /**
         * status of file duplication of other cloud storage.
         * This value is matched storage names (ex "OneDrive" or "OneDrive, GoogleDrive")
         * <p/>
         * Type: TEXT (String)
         * </P>
         */
        String EXTERNAL_AUTO_UPLOAD_STATUS = "external_autoupload_status";

        /**
         * An id to group duplicate media by. This will be the _id of a record
         * that is duplicate media.
         * <p/>
         * Type: INTEGER (long)
         * </P>
         */
        String DUP_ID = "dup_id";

        /**
         * An id to group duplicate media and burst shots by. This will be the
         * _id of a record that is duplicate media or the burst_id field.
         * <p/>
         * Type: INTEGER (long)
         * </P>
         */
        String BURST_DUP_ID = "burst_dup_id";

        /**
         * An id to group burst shot photos by. This will be the _id of a photo
         * having the same group_id. If this photo is not part of a burst shot
         * group, then it will simply be the _id field.
         * <p/>
         * Type: INTEGER (long)
         * </P>
         */
        String BURST_ID = "burst_id";

        /**
         * The path to the locally generated thumbnail file for the media, if it exists yet.
         * <p/>
         * Type: TEXT
         * </P>
         */
        String THUMB_DATA = "thumb_data";

        /**
         * The width of a thumbnail file for the media. This applies for both remote and local files.
         * <p/>
         * Type: INTEGER
         * </P>
         */
        String THUMB_WIDTH = "thumb_width";

        /**
         * The height of a thumbnail file for the media. This applies for both remote and local files.
         * <p/>
         * Type: INTEGER
         * </P>
         */
        String THUMB_HEIGHT = "thumb_height";

        /**
         * The displayable character provided to the indexable sidebar to
         * quickly jump from one section to the next.
         * <p/>
         * Type: TEXT
         * </P>
         */
        String INDEX_CHAR = "index_char";

        /**
         * The order of priority for the device, based on the transport-type. 0
         * is the highest priority and lower priorities have a higher integer.
         * <p/>
         * Type: INTEGER
         * </P>
         */
        String DEVICE_PRIORITY = DeviceColumns.DEVICE_PRIORITY;

        /**
         * The transport type of the device. See {@link CloudGatewayDeviceTransportType} for possible values.
         * <p/>
         * Type: TEXT
         * </P>
         */
        String TRANSPORT_TYPE = DeviceColumns.TRANSPORT_TYPE;

        String ASP_ID = "asp_id";

        String FULL_URI = "full_uri";
    }

    /**
     * Fields for thumbnails
     */
    public interface ThumbnailColumns extends BaseSamsungLinkColumns {

        /**
         * The file size of the cached thumbnail image.
         * <p/>
         * Type: INTEGER (long)
         * </P>
         */
        String SIZE = MediaColumns.SIZE;

        /**
         * The width requested for the cached thumbnail. This is not the actual
         * width of the cached image. It may be larger or smaller based on what
         * the source device was able to provide.
         * <p/>
         * Type: INTEGER
         * </P>
         */
        String WIDTH = MediaColumns.WIDTH;

        /**
         * The height requested for the cached thumbnail. This is not the actual
         * height of the cached image. It may be larger or smaller based on what
         * the source device was able to provide.
         * <p/>
         * Type: INTEGER (long)
         * </P>
         */
        String HEIGHT = MediaColumns.HEIGHT;

        /**
         * The orientation value of the original media that this thumbnail
         * represents.
         * <p/>
         * Type: INTEGER
         * </P>
         */
        String ORIENTATION = MediaStore.Images.ImageColumns.ORIENTATION;

        /**
         * The full width of the original media that this thumbnail represents.
         * This value will always be null for {@link CloudGatewayMediaStore.Audio.AlbumArt}.
         * <p/>
         * Type: INTEGER (long)
         * </P>
         */
        String FULL_WIDTH = "full_width";

        /**
         * The full height of the original media that this thumbnail represents.
         * This value will always be null for {@link CloudGatewayMediaStore.Audio.AlbumArt}.
         * <p/>
         * Type: INTEGER (long)
         * </P>
         */
        String FULL_HEIGHT = "full_height";

        /**
         * The actual width of the downloaded thumbnail image in the file cache.
         * <p/>
         * Type: INTEGER (long)
         * </P>
         */
        String THUMB_WIDTH = "thumb_width";

        /**
         * The actual height of the downloaded thumbnail image in the file
         * cache.
         * <p/>
         * Type: INTEGER (long)
         * </P>
         */
        String THUMB_HEIGHT = "thumb_height";

        /**
         * Query string parameter to set the requested width of a thumbnail.
         * <P>
         * <table border=1>
         * <tr>
         * <td>TYPE:</td>
         * <td>Integer</td>
         * </tr>
         * <tr>
         * <td>USE WITH:</td>
         * <td>{@link ContentResolver#openInputStream(Uri)}</td>
         * </tr>
         * <tr>
         * <td>DEFAULT VALUE:</td>
         * <td>512</td>
         * </tr>
         * </table>
         * </P>
         * <p>
         * The desired width when retrieving a thumbnail image from a remote device. (Using {@link ContentResolver#openInputStream(Uri)} as an appended query
         * string value). The requested width will be passed to the host device and it will provide a thumbnail of this size to its best abilities. If the
         * resulting thumbnail file is placed in the cache (using {@link #QUERY_STR_SKIP_CACHE_PUT}=false), future queries will return this in the WIDTH field,
         * as long as the file remains in the cache.
         * </p>
         */
        String QUERY_STR_WIDTH = "width";

        /**
         * Query string parameter to set the requested height of thumbnail.
         * <P>
         * <table border=1>
         * <tr>
         * <td>TYPE:</td>
         * <td>Integer</td>
         * </tr>
         * <tr>
         * <td>USE WITH:</td>
         * <td>{@link ContentResolver#openInputStream(Uri)}</td>
         * </tr>
         * <tr>
         * <td>DEFAULT VALUE:</td>
         * <td>512</td>
         * </tr>
         * </table>
         * </P>
         * <p>
         * The desired height when retrieving a thumbnail image from a remote device. The requested height will be passed to the host device and it will provide
         * a thumbnail of this size to its best abilities. If the resulting thumbnail file is placed in the cache (using {@link #QUERY_STR_SKIP_CACHE_PUT}
         * =false), future queries will return this in the HEIGHT field, as long as the file remains in the cache.
         * </p>
         */
        String QUERY_STR_HEIGHT = "height";

        /**
         * Query string parameter to control whether or not a thumbnail will be retrieved from the file cache.
         * <P>
         * <table border=1>
         * <tr>
         * <td>TYPE:</td>
         * <td>Boolean</td>
         * </tr>
         * <tr>
         * <td>USE WITH:</td>
         * <td>{@link ContentResolver#openInputStream(Uri)}</td>
         * </tr>
         * <tr>
         * <td>DEFAULT VALUE:</td>
         * <td>TRUE</td>
         * </tr>
         * </table>
         * </P>
         * <p>
         * Controls whether a read of a thumbnail will attempt to read from a cached thumbnail file. The default value (if omitted) is true. If false, then the
         * cached file will be returned if it exists (regardless of the requested width and height). If true or if there is no cached file, then a far call will
         * be made to the host device using the requested width and height.
         * </p>
         */
        String QUERY_STR_SKIP_CACHE_GET = "skip_cache_get";

        /**
         * Query string parameter to control whether or not a thumbnail retrieved from a far call will be placed in the file cache.
         * <P>
         * <table border=1>
         * <tr>
         * <td>TYPE:</td>
         * <td>Boolean</td>
         * </tr>
         * <tr>
         * <td>USE WITH:</td>
         * <td>{@link ContentResolver#openInputStream(Uri)}</td>
         * </tr>
         * <tr>
         * <td>DEFAULT VALUE:</td>
         * <td>TRUE</td>
         * </tr>
         * </table>
         * </P>
         * <p>
         * Controls whether a thumbnail file will be placed in the cache file after a far call to the hosting device is made. The default value (if omitted) is
         * true. If false, then the thumbnail file will be placed in the cache as long as the requested width and height are larger than the previously request
         * made for the current file in the cache.
         * </p>
         */
        String QUERY_STR_SKIP_CACHE_PUT = "skip_cache_put";

        /**
         * Query string parameter to append to a thumbnail Uri to cancel a pending request.
         * <P>
         * <table border=1>
         * <tr>
         * <td>TYPE:</td>
         * <td>Boolean</td>
         * </tr>
         * <tr>
         * <td>USE WITH:</td>
         * <td>{@link ContentResolver#query(Uri, String[], String, String[], String)}</td>
         * </tr>
         * <tr>
         * <td>DEFAULT VALUE:</td>
         * <td>FALSE</td>
         * </tr>
         * </table>
         * </P>
         * <p>
         * When this query string value is passed in as true, no Cursor object will be generated. A thread that is blocked trying to either get a thumbnail or
         * information on a thumbnail will return and be unblocked if possible.
         * </p>
         */
        String QUERY_STR_CANCEL = "cancel";

        /**
         * Query string parameter to append to a thumbnail Uri to read from the local thumbnail cache only.
         * <P>
         * <table border=1>
         * <tr>
         * <td>TYPE:</td>
         * <td>Boolean</td>
         * </tr>
         * <tr>
         * <td>USE WITH:</td>
         * <td>{@link ContentResolver#openInputStream(Uri)}</td>
         * </tr>
         * <tr>
         * <td>DEFAULT VALUE:</td>
         * <td>FALSE</td>
         * </tr>
         * </table>
         * </P>
         * <p>
         * When this query string value is passed as true, no far call will be made if the thumbnail is not in the cache.
         * </p>
         */
        String QUERY_STR_CACHE_ONLY = "cache_only";

        /**
         * Query string parameter to key your thumbnail requests off of (along with the media id).
         * <P>
         * <table border=1>
         * <tr>
         * <td>TYPE:</td>
         * <td>Long</td>
         * </tr>
         * <tr>
         * <td>USE WITH:</td>
         * <td>{@link ContentResolver#openInputStream(Uri)}</td>
         * </tr>
         * <tr>
         * <td>DEFAULT VALUE:</td>
         * <td>0</td>
         * </tr>
         * </table>
         * </P>
         * <p>
         * Using the group_id will allow you to cancel your pending request.
         * </p>
         */
        String QUERY_STR_GROUP_ID = "group_id";
    }

    /**
     * Helper class for generating thumbnails
     */
    private static final class ThumbnailUtility {

        private static class ThumbnailTask {

            private final String mHashKey;
            private boolean mCancelled = false;
            public int mWidth;
            public int mHeight;
            public int mActualWidth = 0;
            public int mActualHeight = 0;
            public int mFullWidth = 0;
            public int mFullHeight = 0;

            public ThumbnailTask(long origId, long groupId, int width, int height) {
                this.mHashKey = Long.toString(origId) + "_" + Long.toString(groupId);
                this.mWidth = width;
                this.mHeight = width;
            }
        }

        private static final ConcurrentHashMap<String, ThumbnailTask> sTaskMap = new ConcurrentHashMap<String, ThumbnailTask>();

        private static final String[] THUMB_PROJECTION = {
                ThumbnailColumns.WIDTH,
                ThumbnailColumns.HEIGHT,
                ThumbnailColumns.THUMB_WIDTH,
                ThumbnailColumns.THUMB_HEIGHT,
                ThumbnailColumns.FULL_WIDTH,
                ThumbnailColumns.FULL_HEIGHT};

        public static InputStream getThumbnailInputStream(
                ContentResolver cr,
                final String path,
                final long origId,
                final long groupId,
                int width,
                int height,
                boolean skipCacheGet,
                boolean skipCachePut) throws FileNotFoundException {
            final ThumbnailTask task = new ThumbnailTask(origId, groupId, width, height);
            sTaskMap.put(task.mHashKey, task);
            try {
                Uri uri = ThumbnailUtility.getThumbnailUri(
                        task,
                        cr,
                        path,
                        origId,
                        groupId,
                        width,
                        height,
                        skipCacheGet,
                        skipCachePut);
                InputStream is = null;
                if (!task.mCancelled) {
                    is = cr.openInputStream(uri);
                }
                return is;
            } catch (Exception exc) {
                if (CloudGatewayMediaStore.ENABLE_LOGGING) {
                    Log.e(TAG, "ThumbnailUtility.getThumbnailUri failed:", exc);
                }
                return null;
            } finally {
                sTaskMap.remove(task.mHashKey, task);
            }
        }

        private static Uri getThumbnailUri(
                ThumbnailTask task,
                ContentResolver cr,
                final String path,
                final long origId,
                final long groupId,
                final int width,
                final int height,
                final boolean skipCacheGet,
                final boolean skipCachePut) {

            Uri uri = CloudGatewayMediaStore.buildEntryIdUri(origId, path);

            int requestWidth = -1;
            int requestHeight = -1;

            if (!skipCacheGet) {
                Cursor cursor = cr.query(uri, THUMB_PROJECTION, null, null, null);
                if (cursor != null) {
                    if (cursor.moveToFirst()) {
                        requestWidth = cursor.getInt(0);
                        requestHeight = cursor.getInt(1);
                        task.mActualWidth = cursor.getInt(2);
                        task.mActualHeight = cursor.getInt(3);
                        task.mFullWidth = cursor.getInt(4);
                        task.mFullHeight = cursor.getInt(5);

                        if (task.mFullWidth > 0 && task.mFullHeight > 0) {
                            long requestArea = ((long) task.mWidth) * ((long) task.mHeight);
                            long fullArea = ((long) task.mFullWidth) * ((long) task.mFullHeight);
                            if (requestArea > fullArea) {
                                task.mWidth = task.mFullWidth;
                                task.mHeight = task.mFullHeight;
                            }
                        }
                    }

                    cursor.close();
                }
            }

            if (!task.mCancelled) {
                Uri.Builder builder = uri.buildUpon();
                builder.appendQueryParameter(
                        ThumbnailColumns.QUERY_STR_WIDTH,
                        Integer.toString(width));
                builder.appendQueryParameter(
                        ThumbnailColumns.QUERY_STR_HEIGHT,
                        Integer.toString(height));
                builder.appendQueryParameter(
                        ThumbnailColumns.QUERY_STR_SKIP_CACHE_GET,
                        Boolean.toString(skipCacheGet
                                || (task.mWidth > requestWidth)
                                || (task.mHeight > requestHeight)));
                builder.appendQueryParameter(
                        ThumbnailColumns.QUERY_STR_SKIP_CACHE_PUT,
                        Boolean.toString(skipCachePut));
                uri = builder.build();
            }

            return uri;
        }

        public static Bitmap getThumbnailImage(
                ContentResolver cr,
                final String path,
                final long origId,
                final long groupId,
                int width,
                int height,
                boolean skipCacheGet,
                boolean skipCachePut,
                BitmapFactory.Options bitmapOptions) {
            Bitmap result = null;

            final ThumbnailTask task = new ThumbnailTask(origId, groupId, width, height);
            sTaskMap.put(task.mHashKey, task);
            try {
                Uri uri = ThumbnailUtility.getThumbnailUri(
                        task,
                        cr,
                        path,
                        origId,
                        groupId,
                        width,
                        height,
                        skipCacheGet,
                        skipCachePut);

                int largestRequestDim = Math.max(task.mWidth, task.mHeight);

                InputStream is = null;
                try {
                    if (!task.mCancelled && !bitmapOptions.inJustDecodeBounds) {
                        if ((task.mActualWidth <= 0 || task.mActualHeight <= 0)) {
                            is = cr.openInputStream(uri);
                            if (is == null) {
                                if (CloudGatewayMediaStore.ENABLE_LOGGING) {
                                    Log.e(TAG, "Unable to open inputStream for bitmap. origId:"
                                            + origId);
                                }
                                return null;
                            }
                            bitmapOptions.inJustDecodeBounds = true;
                            BitmapFactory.decodeStream(is, null, bitmapOptions);
                            task.mActualWidth = bitmapOptions.outWidth;
                            task.mActualHeight = bitmapOptions.outHeight;

                            is.close();

                            Uri.Builder builder = CloudGatewayMediaStore.buildEntryIdUri(
                                    origId,
                                    path).buildUpon();
                            builder.appendQueryParameter(
                                    ThumbnailColumns.QUERY_STR_WIDTH,
                                    Integer.toString(task.mWidth));
                            builder.appendQueryParameter(
                                    ThumbnailColumns.QUERY_STR_HEIGHT,
                                    Integer.toString(task.mHeight));
                            builder.appendQueryParameter(
                                    ThumbnailColumns.QUERY_STR_SKIP_CACHE_GET,
                                    Boolean.toString(false));
                            builder.appendQueryParameter(
                                    ThumbnailColumns.QUERY_STR_SKIP_CACHE_PUT,
                                    Boolean.toString(skipCachePut));
                            uri = builder.build();

                        }

                        int scaleFactor = 1;
                        int largestActualDim = Math.max(task.mActualHeight, task.mActualWidth);

                        if (largestActualDim > largestRequestDim) {
                            scaleFactor = Math.round((float) largestActualDim
                                    / (float) largestRequestDim);
                        }
                        scaleFactor = Math.max(scaleFactor, 1);

                        bitmapOptions.inJustDecodeBounds = false;
                        bitmapOptions.inSampleSize = scaleFactor;

                        if (CloudGatewayMediaStore.ENABLE_LOGGING) {
                            Log.v(TAG, "::getThumbnailImage scaleFactor:"
                                    + scaleFactor
                                    + " requested:"
                                    + task.mWidth
                                    + 'x'
                                    + task.mHeight
                                    + " bitmap:"
                                    + task.mActualWidth
                                    + 'x'
                                    + task.mActualHeight);
                        }
                    }
                    if (!task.mCancelled) {

                        is = cr.openInputStream(uri);
                        try {
                            if (is == null) {
                                if (CloudGatewayMediaStore.ENABLE_LOGGING) {
                                    Log.e(TAG, "Unable to open inputStream for bitmap. origId:"
                                            + origId);
                                }
                                return null;
                            }

                            result = BitmapFactory.decodeStream(is, null, bitmapOptions);

                            is.close();
                            if (result != null) {

                                int largestActualDim = Math.max(
                                        result.getWidth(),
                                        result.getHeight());
                                if (largestActualDim > largestRequestDim) {
                                    float ratio = ((float) largestRequestDim)
                                            / ((float) largestActualDim);

                                    int newWidth;
                                    int newHeight;
                                    if (largestActualDim == result.getWidth()) {
                                        newWidth = largestRequestDim;
                                        newHeight = (int) (result.getHeight() * ratio);
                                    } else {
                                        newWidth = (int) (result.getWidth() * ratio);
                                        newHeight = largestRequestDim;
                                    }

                                    if (newWidth > 0 && newHeight > 0) {
                                        Bitmap scaledBitmap = Bitmap.createScaledBitmap(
                                                result,
                                                newWidth,
                                                newHeight,
                                                false);
                                        result.recycle();
                                        result = scaledBitmap;
                                    }
                                }
                            }
                        } catch (OutOfMemoryError oome) {
                            if (CloudGatewayMediaStore.ENABLE_LOGGING) {
                                if (result != null) {
                                    Log.e(TAG, "Unable to rescale bitmap. origId:" + origId, oome);
                                } else {
                                    Log.e(TAG, "Unable to decode bitmap. origId:" + origId, oome);
                                }
                            }
                        }
                    }
                } catch (IOException ioe) {
                    if (CloudGatewayMediaStore.ENABLE_LOGGING) {
                        Log.e(TAG, "Unable to decode bitmap. origId:" + origId, ioe);
                    }
                } finally {
                    if (is != null) {
                        try {
                            is.close();
                        } catch (IOException e) {

                        }
                    }
                }
            } finally {
                sTaskMap.remove(task.mHashKey, task);
            }

            return result;
        }

        public static void cancelThumbnailRequest(
                ContentResolver cr,
                String path,
                long origId,
                long groupId) {
            ThumbnailTask task = new ThumbnailTask(origId, groupId, 0, 0);

            task = sTaskMap.get(task.mHashKey);
            if (task != null) {
                sTaskMap.remove(task.mHashKey);
                task.mCancelled = true;

                Uri.Builder builder = CloudGatewayMediaStore
                        .buildEntryIdUri(origId, path)
                        .buildUpon();
                builder.appendQueryParameter(
                        ThumbnailColumns.QUERY_STR_CANCEL,
                        Boolean.TRUE.toString());
                builder.appendQueryParameter(
                        ThumbnailColumns.QUERY_STR_GROUP_ID,
                        Long.toString(groupId));
                Cursor cursor = cr.query(builder.build(), null, null, null, null);
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
    }

    /**
     * Contains meta data for all available images
     */
    public static class Images {

        /**
         * Fields for images
         */
        public interface ImageColumns
                extends android.provider.MediaStore.Images.ImageColumns, MediaColumns {

            /**
             * The group id of the image. A group of photos taken in burst shot
             * mode will all have the same group_id value. If this value is '0'
             * or null, the photo is not part of a burst shot.
             * <p/>
             * Type: TEXT
             * </P>
             */
            String GROUP_ID = "group_id";
        }

        /**
         * Contains meta data for all available images
         */
        public static class Media implements ImageColumns {

            /**
             * Path for the Images table
             */
            public static final String PATH = "image";

            /**
             * Path for the Images table that will group by burst shot and
             * duplicate ids
             */
            public static final String CROSS_DEVICE_PATH = "image_cross_device";

            /**
             * Path for the Images table that will group by burst shot
             */
            public static final String BURST_SHOT_PATH = "image_burst_shot";

            /**
             * The content type for the Images table
             */
            public static final String CONTENT_TYPE = CloudGatewayMediaStore.buildContentType(PATH);

            /**
             * The entry content type for the Images table
             */
            public static final String ENTRY_CONTENT_TYPE = CloudGatewayMediaStore
                    .buildEntryContentType(PATH);

            /**
             * The content uri for the Images table
             */
            public static final Uri CONTENT_URI = CloudGatewayMediaStore.buildContentUri(PATH);

            /**
             * Builds a content uri for all images for a specific device
             *
             * @param deviceId the id of the device
             * @return the content uri
             */
            public static Uri getContentUriForDevice(long deviceId) {
                return CloudGatewayMediaStore.buildDeviceContentUri(deviceId, PATH);
            }

            /**
             * Builds an entry uri for a specific image
             *
             * @param rowId the id of the image
             * @return the entry uri
             */
            public static Uri getEntryUri(long rowId) {
                return CloudGatewayMediaStore.buildEntryIdUri(rowId, PATH);
            }
        }

        /**
         * Provides access to thumbnails for images
         */
        public static class Thumbnails implements ThumbnailColumns {

            /**
             * Path for the image thumbnails virtual table
             */
            public static final String PATH = "image_thumbs";

            /**
             * Content type for image thumbnails
             */
            public static final String CONTENT_TYPE = CloudGatewayMediaStore
                    .buildEntryContentType(PATH);

            /**
             * Entry content type for image thumbnails
             */
            public static final String ENTRY_CONTENT_TYPE = CloudGatewayMediaStore
                    .buildEntryContentType(PATH);

            /**
             * The content uri for image thumbnails
             */
            public static final Uri CONTENT_URI = CloudGatewayMediaStore.buildContentUri(PATH);

            /**
             * Builds an entry uri for a specific image thumbnail
             *
             * @param rowId the id of the image
             * @return the entry uri
             */
            public static Uri getEntryUri(long rowId) {
                return CloudGatewayMediaStore.buildEntryIdUri(rowId, PATH);
            }

            /**
             * This utility method will provide a thumbnail of an Image media
             * file. This method will block until the thumbnail is retrieved and
             * decoded. You can specify various options as specified below:
             *
             * @param cr            your ContentResolver
             * @param origId        {@link android.provider.BaseColumns#_ID} of the
             *                      Image requested
             * @param groupId       a key used to cancel a thumbnail request. It is
             *                      recommended that you use the current thread's id.
             * @param width         the requested width of the thumbnail to be produced.
             *                      The image's original aspect ratio will be maintained.
             * @param height        the requested height of the thumbnail to be
             *                      produced. The image's original aspect ration will be
             *                      maintained.
             * @param skipCacheGet  True: force the ContentProvider to perform a long
             *                      fetch to the hosting device. The hosting device will
             *                      attempt to honor the requested width and height. -
             *                      False: will cause a lookup in the file cache first. If
             *                      the file is not in the cache, a far call will be made.
             *                      If the cached file was made with a smaller width and
             *                      height that this request's width and height, then the
             *                      cached file will be skipped and a far call will be
             *                      made. If the cached file was requested with a width
             *                      and height larger or equal to this request, the cached
             *                      thumbnail image will be provided.
             * @param skipCachePut  If a far call is made using the above rules, this
             *                      controls whether or not the resulting far call will be
             *                      placed in ContentProvider's cache. True will cause it
             *                      to not be placed in the cache.
             * @param bitmapOptions The BitmapFactory.Options for decoding the image.
             * @return The decoded Bitmap if successful or null.
             */
            public static Bitmap getThumbnail(
                    ContentResolver cr,
                    long origId,
                    long groupId,
                    int width,
                    int height,
                    boolean skipCacheGet,
                    boolean skipCachePut,
                    BitmapFactory.Options bitmapOptions) {
                return ThumbnailUtility.getThumbnailImage(
                        cr,
                        PATH,
                        origId,
                        groupId,
                        width,
                        height,
                        skipCacheGet,
                        skipCachePut,
                        bitmapOptions);
            }


            /**
             * This utility method will provide an InputStream to a thumbnail
             * file. This method will block until the InputStream is created or
             * fails. This allows the application to do its own Bitmap decoding
             * using the InputStream. The InputStream can be encoded as a JPEG,
             * PNG, GIF.
             *
             * @param cr           your ContentResolver
             * @param origId       {@link android.provider.BaseColumns#_ID} of the
             *                     Image requested
             * @param groupId      a key used to cancel a thumbnail request. It is
             *                     recommended that you use the current thread's id.
             * @param width        the requested width of the thumbnail to be produced.
             *                     The image's original aspect ratio will be maintained.
             * @param height       the requested height of the thumbnail to be
             *                     produced. The image's original aspect ration will be
             *                     maintained.
             * @param skipCacheGet True: force the ContentProvider to perform a long
             *                     fetch to the hosting device. The hosting device will
             *                     attempt to honor the requested width and height. -
             *                     False: will cause a lookup in the file cache first. If
             *                     the file is not in the cache, a far call will be made.
             *                     If the cached file was made with a smaller width and
             *                     height that this request's width and height, then the
             *                     cached file will be skipped and a far call will be
             *                     made. If the cached file was requested with a width
             *                     and height larger or equal to this request, the cached
             *                     thumbnail image will be provided.
             * @param skipCachePut If a far call is made using the above rules, this
             *                     controls whether or not the resulting far call will be
             *                     placed in ContentProvider's cache. True will cause it
             *                     to not be placed in the cache.
             * @return The encoded InputStream for the thumbnail image, if
             * successful.
             */
            public static InputStream openThumbnailInputStream(
                    ContentResolver cr,
                    long origId,
                    long groupId,
                    int width,
                    int height,
                    boolean skipCacheGet,
                    boolean skipCachePut) throws FileNotFoundException {

                return ThumbnailUtility.getThumbnailInputStream(
                        cr,
                        PATH,
                        origId,
                        groupId,
                        width,
                        height,
                        skipCacheGet,
                        skipCachePut);
            }

            /**
             * This utility method will attempt to cancel a pending image
             * request made using {@link #getThumbnail(ContentResolver, long, long, int, int, boolean, boolean, android.graphics.BitmapFactory.Options)} or
             * using {@link #openThumbnailInputStream(ContentResolver, long, long, int, int, boolean, boolean)} and unblock the thread used for that call.
             * Currently only cloud
             * storage and local devices support cancelling a thumbnail transfer
             * mid-stream.
             *
             * @param cr      your ContentResolver
             * @param origId  {@link android.provider.BaseColumns#_ID} of the
             *                Video requested
             * @param groupId The groupId value passed into getThumbnail.
             */
            public static void cancelThumbnailRequest(ContentResolver cr, long origId, long groupId) {
                ThumbnailUtility.cancelThumbnailRequest(cr, PATH, origId, groupId);
            }
        }
    }

    /**
     * Contains meta data for all available videos
     */
    public static class Video {

        /**
         * Fields for videos
         */
        public interface VideoColumns
                extends android.provider.MediaStore.Video.VideoColumns, MediaColumns {

            /**
             * The type of caption file associated with the video, if any.
             * <p/>
             * Possible types are:
             * <ul>
             * <li>"SMI" - SAMI caption file</li>
             * <li>"SRT" - SubRip caption file</li>
             * <li>"SUB" - VobSub caption file (must have an accompanying .idx file)</li>
             * <li>"TXT" - Simple text file.</li>
             * <li>"TTXT" - A timed text file.</li>
             * </ul>
             * </P>
             * <p/>
             * Type: TEXT
             * <p/>
             */
            String CAPTION_TYPE = "caption_type";

            /**
             * The orientation for the video expressed as degrees. Only degrees
             * 0, 90, 180, 270 will work.
             * <p/>
             * Type: INTEGER
             * <p/>
             */
            String ORIENTATION = Images.ImageColumns.ORIENTATION;

            /**
             * Used as a timestamp to mark recently played video media. The
             * value should be in seconds since Jan 1, 1970 UTC.
             * <p/>
             * Type: INTEGER
             * </P>
             */
            String IS_PLAYED = "isPlayed";

        }

        /**
         * Contains meta data for all available videos
         */
        public static class Media implements VideoColumns {

            /**
             * Path for the videos table
             */
            public static final String PATH = "video";

            /**
             * The content type for the videos table
             */
            public static final String CONTENT_TYPE = CloudGatewayMediaStore.buildContentType(PATH);

            /**
             * The entry content type for the videos table
             */
            public static final String ENTRY_CONTENT_TYPE = CloudGatewayMediaStore
                    .buildEntryContentType(PATH);
            /**
             * The content uri for the videos table
             */
            public static final Uri CONTENT_URI = CloudGatewayMediaStore.buildContentUri(PATH);

            /**
             * Builds a content uri for all videos for a specific device
             *
             * @param deviceId the id of the device
             * @return the content uri
             */
            public static Uri getContentUriForDevice(long deviceId) {
                return CloudGatewayMediaStore.buildDeviceContentUri(deviceId, PATH);
            }
        }

        /**
         * Provides access to thumbnails for videos
         */
        public static class Thumbnails implements ThumbnailColumns {

            /**
             * Path for the video thumbnails virtual table
             */
            public static final String PATH = "video_thumbs";

            /**
             * Content type for video thumbnails
             */
            public static final String CONTENT_TYPE = CloudGatewayMediaStore.buildEntryContentType(PATH);

            /**
             * Entry content type for video thumbnails
             */
            public static final String ENTRY_CONTENT_TYPE = CloudGatewayMediaStore.buildEntryContentType(PATH);

            /**
             * The content uri for video thumbnails
             */
            public static final Uri CONTENT_URI = CloudGatewayMediaStore.buildContentUri(PATH);

            /**
             * Builds an entry uri for a specific video thumbnail
             *
             * @param rowId the id of the video
             * @return the entry uri
             */
            public static Uri getEntryUri(long rowId) {
                return CloudGatewayMediaStore.buildEntryIdUri(rowId, PATH);
            }

            /**
             * This utility method will provide a thumbnail of an Video media
             * file. This method will block until the thumbnail is retrieved and
             * decoded. You can specify various options as specified below:
             *
             * @param cr            your ContentResolver
             * @param origId        {@link android.provider.BaseColumns#_ID} of the
             *                      Video requested
             * @param groupId       an key used to cancel a thumbnail request. It is
             *                      recommended that you use the current thread's id.
             * @param width         the requested width of the thumbnail to be produced.
             *                      The video's original aspect ratio will be maintained.
             * @param height        the requested height of the thumbnail to be
             *                      produced. The video's original aspect ration will be
             *                      maintained.
             * @param skipCacheGet  True: force the ContentProvider to perform a long
             *                      fetch to the hosting device. The hosting device will
             *                      attempt to honor the requested width and height. -
             *                      False: will cause a lookup in the file cache first. If
             *                      the file is not in the cache, a far call will be made.
             *                      If the cached file was made with a smaller width and
             *                      height that this request's width and height, then the
             *                      cached file will be skipped and a far call will be
             *                      made. If the cached file was requested with a width
             *                      and height larger or equal to this request, the cached
             *                      thumbnail image will be provided.
             * @param skipCachePut  If a far call is made using the above rules, this
             *                      controls whether or not the resulting far call will be
             *                      placed in ContentProvider's cache. True will cause it
             *                      to not be placed in the cache.
             * @param bitmapOptions The BitmapFactory.Options for decoding the image.
             * @return The decoded Bitmap if successful or null.
             */
            public static Bitmap getThumbnail(
                    ContentResolver cr,
                    long origId,
                    long groupId,
                    int width,
                    int height,
                    boolean skipCacheGet,
                    boolean skipCachePut,
                    BitmapFactory.Options bitmapOptions) {
                return ThumbnailUtility.getThumbnailImage(
                        cr,
                        PATH,
                        origId,
                        groupId,
                        width,
                        height,
                        skipCacheGet,
                        skipCachePut,
                        bitmapOptions);
            }

            /**
             * This utility method will provide an InputStream to a thumbnail
             * file. This method will block until the InputStream is created or
             * fails. This allows the application to do its own Bitmap decoding
             * using the InputStream. The InputStream can be encoded as a JPEG.
             *
             * @param cr           your ContentResolver
             * @param origId       {@link android.provider.BaseColumns#_ID} of the
             *                     Video requested
             * @param groupId      a key used to cancel a thumbnail request. It is
             *                     recommended that you use the current thread's id.
             * @param width        the requested width of the thumbnail to be produced.
             *                     The video's original aspect ratio will be maintained.
             * @param height       the requested height of the thumbnail to be
             *                     produced. The video's original aspect ration will be
             *                     maintained.
             * @param skipCacheGet True: force the ContentProvider to perform a long
             *                     fetch to the hosting device. The hosting device will
             *                     attempt to honor the requested width and height. -
             *                     False: will cause a lookup in the file cache first. If
             *                     the file is not in the cache, a far call will be made.
             *                     If the cached file was made with a smaller width and
             *                     height that this request's width and height, then the
             *                     cached file will be skipped and a far call will be
             *                     made. If the cached file was requested with a width
             *                     and height larger or equal to this request, the cached
             *                     thumbnail image will be provided.
             * @param skipCachePut If a far call is made using the above rules, this
             *                     controls whether or not the resulting far call will be
             *                     placed in ContentProvider's cache. True will cause it
             *                     to not be placed in the cache.
             * @return The encoded InputStream for the thumbnail image, if
             * successful.
             */
            public static InputStream openThumbnailInputStream(
                    ContentResolver cr,
                    long origId,
                    long groupId,
                    int width,
                    int height,
                    boolean skipCacheGet,
                    boolean skipCachePut) throws FileNotFoundException {

                return ThumbnailUtility.getThumbnailInputStream(
                        cr,
                        PATH,
                        origId,
                        groupId,
                        width,
                        height,
                        skipCacheGet,
                        skipCachePut);
            }

            /**
             * This utility method will attempt to cancel a pending image
             * request made using {@link #getThumbnail(ContentResolver, long, long, int, int, boolean, boolean, android.graphics.BitmapFactory.Options)} or
             * using {@link #openThumbnailInputStream(ContentResolver, long, long, int, int, boolean, boolean)} and unblock the thread used for that call.
             * Currently only cloud
             * storage and local devices support cancelling a thumbnail transfer
             * mid-stream.
             *
             * @param cr      your ContentResolver
             * @param origId  {@link android.provider.BaseColumns#_ID} of the
             *                Video requested
             * @param groupId The groupId value passed into getThumbnail.
             */
            public static void cancelThumbnailRequest(ContentResolver cr, long origId, long groupId) {
                ThumbnailUtility.cancelThumbnailRequest(cr, PATH, origId, groupId);
            }
        }
    }

    /**
     * Container for all audio content
     */
    public static class Audio {

        /**
         * Columns representing an album
         */
        public interface AlbumColumns
                extends android.provider.MediaStore.Audio.AlbumColumns, BaseSamsungLinkColumns {

            /**
             * The id of the first audio record for this album
             * <p/>
             * Type: INTEGER (long)
             * </P>
             */
            String AUDIO_ID = "audio_id";

            /**
             * The id of the first artist record for this album
             * <p/>
             * Type: INTEGER (long)
             * </P>
             */
            String ARTIST_ID = "artist_id";

            /**
             * The artist for the entire album.
             * <p/>
             * Type: TEXT
             * </P>
             */
            String ALBUM_ARTIST = "album_artist";

            /**
             * The a collation key for the ALBUM_ARTIST field. Similar to the
             * ALBUM_KEY.
             * <p/>
             * Type: TEXT
             * </P>
             */
            String ALBUM_ARTIST_KEY = "album_artist_key";

            /**
             * The number of duplicate-reduced tracks for this album.
             * <p/>
             * Type: INTEGER
             * </P>
             */
            String NUMBER_OF_DUP_REDUCED_SONGS = "num_dup_reduced_songs";

            /**
             * The displayable character provided to the indexable sidebar to
             * quickly jump from one section to the next.
             * <p/>
             * Type: TEXT
             * </P>
             */
            String ALBUM_INDEX_CHAR = "album_index_char";

            /**
             * The transport type of the device. See {@link CloudGatewayDeviceTransportType} for possible values.
             * <p/>
             * Type: TEXT
             * </P>
             */
            String TRANSPORT_TYPE = DeviceColumns.TRANSPORT_TYPE;

            /**
             * The path to the locally generated thumbnail file for the media, if it exists yet. This is only for local media.
             */
            String THUMB_DATA = "thumb_data";

        }

        /**
         * Contains album information
         */
        public static class Albums implements AlbumColumns {

            /**
             * Path for the album table
             */
            public static final String PATH = "album";

            /**
             * The content type for the album table
             */
            public static final String CONTENT_TYPE = CloudGatewayMediaStore.buildContentType(PATH);

            /**
             * The entry content type for the album table
             */
            public static final String ENTRY_CONTENT_TYPE = CloudGatewayMediaStore
                    .buildEntryContentType(PATH);

            /**
             * The content uri for the album table
             */
            public static final Uri CONTENT_URI = CloudGatewayMediaStore.buildContentUri(PATH);

            /**
             * The default album name used for unknown albums. This is inserted
             * into the {@link MediaStore.Audio.AudioColumns#ALBUM} field in the
             * database. For internationalization purposes, please compare the
             * value retrieved from the the database to this and substitute a
             * localized string for "unknown album".
             */
            public static final String DEFAULT_ALBUM_NAME = MediaStore.UNKNOWN_STRING;

            /**
             * Builds a content uri for all albums for a specific device
             *
             * @param deviceId the id of the device
             * @return the content uri
             */
            public static Uri getContentUriForDevice(long deviceId) {
                return CloudGatewayMediaStore.buildDeviceContentUri(deviceId, PATH);
            }
        }

        /**
         * Provides access to album art thumbnails
         */
        public static class AlbumArt implements ThumbnailColumns {

            /**
             * Path for the album art virtual table
             */
            public static final String PATH = "album_art";

            /**
             * Content type for album art
             */
            public static final String CONTENT_TYPE = CloudGatewayMediaStore
                    .buildEntryContentType(PATH);

            /**
             * Entry content type for album art
             */
            public static final String ENTRY_CONTENT_TYPE = CloudGatewayMediaStore
                    .buildEntryContentType(PATH);

            /**
             * Content uri for album art
             */
            public static final Uri CONTENT_URI = CloudGatewayMediaStore.buildContentUri(PATH);

            /**
             * Builds an entry uri for a specific album art thumbnail
             *
             * @param rowId the id of the album
             * @return the entry uri
             */
            public static Uri getEntryUri(long rowId) {
                return CloudGatewayMediaStore.buildEntryIdUri(rowId, PATH);
            }

            /**
             * This utility method will provide a thumbnail of an Album's
             * artwork. This method will block until the thumbnail is retrieved
             * and decoded. You can specify various options as specified below:
             *
             * @param cr            your ContentResolver
             * @param origId        {@link android.provider.BaseColumns#_ID} of the
             *                      Album requested
             * @param groupId       a key used to cancel a thumbnail request. It is
             *                      recommended that you use the current thread's id.
             * @param width         the requested width of the thumbnail to be produced.
             *                      The image's original aspect ratio will be maintained.
             * @param height        the requested height of the thumbnail to be
             *                      produced. The image's original aspect ration will be
             *                      maintained.
             * @param skipCacheGet  True: force the ContentProvider to perform a long
             *                      fetch to the hosting device. The hosting device will
             *                      attempt to honor the requested width and height. -
             *                      False: will cause a lookup in the file cache first. If
             *                      the file is not in the cache, a far call will be made.
             *                      If the cached file was made with a smaller width and
             *                      height that this request's width and height, then the
             *                      cached file will be skipped and a far call will be
             *                      made. If the cached file was requested with a width
             *                      and height larger or equal to this request, the cached
             *                      thumbnail image will be provided.
             * @param skipCachePut  If a far call is made using the above rules, this
             *                      controls whether or not the resulting far call will be
             *                      placed in ContentProvider's cache. True will cause it
             *                      to not be placed in the cache.
             * @param bitmapOptions The BitmapFactory.Options for decoding the image.
             * @return The decoded Bitmap if successful or null.
             */
            public static Bitmap getThumbnail(
                    ContentResolver cr,
                    long origId,
                    long groupId,
                    int width,
                    int height,
                    boolean skipCacheGet,
                    boolean skipCachePut,
                    BitmapFactory.Options bitmapOptions) {
                return ThumbnailUtility.getThumbnailImage(
                        cr,
                        PATH,
                        origId,
                        groupId,
                        width,
                        height,
                        skipCacheGet,
                        skipCachePut,
                        bitmapOptions);
            }

            /**
             * This utility method will provide an InputStream to a thumbnail
             * file. This method will block until the InputStream is created or
             * fails. This allows the application to do its own Bitmap decoding
             * using the InputStream. The InputStream can be encoded as a JPEG,
             * PNG, GIF.
             *
             * @param cr           your ContentResolver
             * @param origId       {@link android.provider.BaseColumns#_ID} of the
             *                     Album requested
             * @param groupId      a key used to cancel a thumbnail request. It is
             *                     recommended that you use the current thread's id.
             * @param width        the requested width of the thumbnail to be produced.
             *                     The image's original aspect ratio will be maintained.
             * @param height       the requested height of the thumbnail to be
             *                     produced. The image's original aspect ration will be
             *                     maintained.
             * @param skipCacheGet True: force the ContentProvider to perform a long
             *                     fetch to the hosting device. The hosting device will
             *                     attempt to honor the requested width and height. -
             *                     False: will cause a lookup in the file cache first. If
             *                     the file is not in the cache, a far call will be made.
             *                     If the cached file was made with a smaller width and
             *                     height that this request's width and height, then the
             *                     cached file will be skipped and a far call will be
             *                     made. If the cached file was requested with a width
             *                     and height larger or equal to this request, the cached
             *                     thumbnail image will be provided.
             * @param skipCachePut If a far call is made using the above rules, this
             *                     controls whether or not the resulting far call will be
             *                     placed in ContentProvider's cache. True will cause it
             *                     to not be placed in the cache.
             * @return The encoded InputStream for the thumbnail image, if
             * successful.
             */
            public static InputStream openThumbnailInputStream(
                    ContentResolver cr,
                    long origId,
                    long groupId,
                    int width,
                    int height,
                    boolean skipCacheGet,
                    boolean skipCachePut) throws FileNotFoundException {

                return ThumbnailUtility.getThumbnailInputStream(
                        cr,
                        PATH,
                        origId,
                        groupId,
                        width,
                        height,
                        skipCacheGet,
                        skipCachePut);
            }

            /**
             * This utility method will attempt to cancel a pending image
             * request made using {@link #getThumbnail(ContentResolver, long, long, int, int, boolean, boolean, android.graphics.BitmapFactory.Options)} or
             * using {@link #openThumbnailInputStream(ContentResolver, long, long, int, int, boolean, boolean)} and unblock the thread used for that call.
             * Currently only cloud
             * storage and local devices support cancelling a thumbnail transfer
             * mid-stream.
             *
             * @param cr      your ContentResolver
             * @param origId  {@link android.provider.BaseColumns#_ID} of the
             *                Video requested
             * @param groupId The groupId value passed into getThumbnail.
             */
            public static void cancelThumbnailRequest(ContentResolver cr, long origId, long groupId) {
                ThumbnailUtility.cancelThumbnailRequest(cr, PATH, origId, groupId);
            }
        }

        /**
         * Columns representing an artist
         */
        public interface ArtistColumns
                extends android.provider.MediaStore.Audio.ArtistColumns, BaseSamsungLinkColumns {

            /**
             * The album id of the first track in this grouping.
             * <p/>
             * Type: INTEGER (long)
             * </P>
             */
            String ALBUM_ID = AudioColumns.ALBUM_ID;

            /**
             * The number of duplicate-reduced audio tracks.
             * <p/>
             * TYPE: INTEGER
             * </P>
             */
            String NUMBER_OF_DUP_REDUCED_TRACKS = "num_dup_reduced_tracks";

            /**
             * The path to the locally generated thumbnail file for the media, if it exists yet. This is only for local media.
             */
            String THUMB_DATA = "thumb_data";

        }

        /**
         * Provides access to artist information
         */
        public static class Artists implements ArtistColumns {

            /**
             * Path for the artist table
             */
            public static final String PATH = "artist";

            /**
             * The content type for the artist table
             */
            public static final String CONTENT_TYPE = CloudGatewayMediaStore.buildContentType(PATH);

            /**
             * The entry content type for the artist table
             */
            public static final String ENTRY_CONTENT_TYPE = CloudGatewayMediaStore
                    .buildEntryContentType(PATH);

            /**
             * The content uri for the artist table
             */
            public static final Uri CONTENT_URI = CloudGatewayMediaStore.buildContentUri(PATH);

            /**
             * The default artist name used for unknown artists. This is
             * inserted into the {@link MediaStore.Audio.AudioColumns#ARTIST} field in the database. For internationalization purposes, please
             * compare the value retrieved from the the database to this and
             * substitute a localized string for "unknown artist".
             */
            public static final String DEFAULT_ARTIST_NAME = MediaStore.UNKNOWN_STRING;

            /**
             * Builds a content uri for all artists for a specific device
             *
             * @param deviceId the id of the device
             * @return the content uri
             */
            public static Uri getContentUriForDevice(long deviceId) {
                return CloudGatewayMediaStore.buildDeviceContentUri(deviceId, PATH);
            }
        }

        /**
         * Columns representing a genre
         */
        public interface GenresColumns
                extends android.provider.MediaStore.Audio.GenresColumns, BaseSamsungLinkColumns {

            /**
             * A non human readable key calculated from the GENRE, used for
             * searching, sorting and grouping
             * <p/>
             * Type: TEXT
             * </P>
             */
            String GENRE_KEY = "genre_key";

            /**
             * The id of the first album record for this genre
             * <p/>
             * Type: INTEGER (long)
             * </P>
             */
            String ALBUM_ID = AudioColumns.ALBUM_ID;

            /**
             * The id of the first artist record for this genre
             * <p/>
             * Type: INTEGER (long)
             * </P>
             */
            String ARTIST_ID = AudioColumns.ARTIST_ID;

            /**
             * The title of the first audio record for this genre.
             * <p/>
             * Type: TEXT
             * </P>
             */
            String TITLE = AudioColumns.TITLE;

            /**
             * The number of duplicate-reduced audio tracks in this genre.
             * <p/>
             * Type: INTEGER
             * </P>
             */
            String DUP_REDUCED_COUNT = "dup_reduced_count";

            /**
             * The path to the locally generated thumbnail file for the media, if it exists yet. This is only for local media.
             */
            String THUMB_DATA = "thumb_data";
        }

        /**
         * Columns for audio files
         */
        public interface AudioColumns
                extends android.provider.MediaStore.Audio.AudioColumns, MediaColumns {

            /**
             * The containing directory for the audio file.
             * <p/>
             * Type: TEXT
             * </P>
             */
            String BUCKET_DISPLAY_NAME = Images.ImageColumns.BUCKET_DISPLAY_NAME;

            /**
             * The displayable character provided to the indexable sidebar to
             * quickly jump from one section to the next.
             * <p/>
             * Type: TEXT
             * </P>
             */
            String ALBUM_INDEX_CHAR = "album_index_char";

            /**
             * The last time this audio media was played. Timestamp should be in
             * seconds since Jan 01, 1970 UTC.
             * <p/>
             * Type: INTEGER
             * </P>
             */
            String RECENTLY_PLAYED = "recently_played";

            /**
             * The count of how many times this audio track has been played on
             * this device.
             * <p/>
             * Type: INTEGER
             * </P>
             */
            String MOST_PLAYED = "most_played";

            /**
             * The bitrate on the device's database.
             * <p/>
             * Type: INTEGER(long)
             * </P>
             */
            String BITRATE = "bitrate";

            /**
             * The copyright on the device's database.
             * <p/>
             * Type: TEXT
             * </P>
             */
            String COPYRIGHT = "copyright";

            /**
             * The genre on the device's database.
             * <p/>
             * Type: TEXT
             * </P>
             */
            String GENRE = "genre";

            /**
             * Has DRM?
             * <p/>
             * Type: INTEGER(boolean)
             * </P>
             */
            String HAS_DRM = "hasDrm";

            /**
             * Is VariableBitrate?
             * <p/>
             * Type: INTEGER(boolean)
             * </P>
             */
            String IS_VBR = "isVBR";
        }

        /**
         * Provides access to audio files
         */
        public static class Media implements AudioColumns {

            /**
             * Path for the audio table
             */
            public static final String PATH = "audio";

            /**
             * Content uri for the audio table
             */
            public static final Uri CONTENT_URI = CloudGatewayMediaStore.buildContentUri(PATH);

            /**
             * Builds a content uri for all audio files for a specific device
             *
             * @param deviceId the id of the device
             * @return the content uri
             */
            public static Uri getContentUriForDevice(long deviceId) {
                return CloudGatewayMediaStore.buildDeviceContentUri(deviceId, PATH);
            }
        }
    }

    /**
     * Contains meta data for all available documents
     */
    public static class Documents {

        /**
         * Columns for documents
         */
        public interface DocumentColumns extends MediaColumns {

            String BUCKET_DISPLAY_NAME = Images.ImageColumns.BUCKET_DISPLAY_NAME;
            String IS_PERSONAL = "is_personal";
        }

        /**
         * Provides access to document files
         */
        public static class Media implements DocumentColumns {

            /**
             * Path for the document table
             */
            public static final String PATH = "document";

            /**
             * Special path for the documents table that duplicate reduces
             * document files across devices
             */
            public static final String CROSS_DEVICE_PATH = "document_cross_device";

            /**
             * Content type for the document table
             */
            public static final String CONTENT_TYPE = CloudGatewayMediaStore.buildContentType(PATH);

            /**
             * Entry content type for the document table
             */
            public static final String ENTRY_CONTENT_TYPE = CloudGatewayMediaStore
                    .buildEntryContentType(PATH);

            /**
             * Content uri for the document table
             */
            public static final Uri CONTENT_URI = CloudGatewayMediaStore.buildContentUri(PATH);

            /**
             * Builds a content uri for all documents for a specific device
             *
             * @param deviceId the id of the device
             * @return the content uri
             */
            public static Uri getContentUriForDevice(long deviceId) {
                return CloudGatewayMediaStore.buildDeviceContentUri(deviceId, PATH);
            }

            /**
             * Builds an entry uri for a specific document
             *
             * @param rowId the id of the video
             * @return the entry uri
             */
            public static Uri getEntryUri(long rowId) {
                return CloudGatewayMediaStore.buildEntryIdUri(rowId, PATH);
            }
        }
    }

    public static class Trash {
        public static final String PATH = "trash";
        public static final String Trash_ID = "^^^trash@$%#@";

        public static final String RESTORE_ONGOING_STATUS = "Restore";
        public static final String TRASH_ONGOING_STATUS = "Trash";

        public static Uri getTrashListUri(long deviceId) {
            return FileBrowser.createFileBrowserUri(PATH, deviceId);
        }

        public static long getDeviceIdFromUri(Uri uri) {
            return CloudGatewayMediaStore.FileBrowser.getDeviceIdFromUri(uri, PATH);
        }
    }

    /**
     * Contains meta data for all available directories
     */
    public static class Directory {

        /**
         * Columns for directory
         */
        public interface DirectoryColumns extends MediaColumns {

            String BUCKET_DISPLAY_NAME = Images.ImageColumns.BUCKET_DISPLAY_NAME;
            String IS_PERSONAL = "is_personal";
        }

        /**
         * Provides access to directory
         */
        public static class Media implements DirectoryColumns {

            /**
             * Path for the directory table
             */
            public static final String PATH = "directory";

            /**
             * Special path for the directory table that duplicate reduces
             * directories across devices
             */
            public static final String CROSS_DEVICE_PATH = "directory_cross_device";

            /**
             * Content type for the directory table
             */
            public static final String CONTENT_TYPE = CloudGatewayMediaStore.buildContentType(PATH);

            /**
             * Entry content type for the directory table
             */
            public static final String ENTRY_CONTENT_TYPE = CloudGatewayMediaStore
                    .buildEntryContentType(PATH);

            /**
             * Content uri for the directory table
             */
            public static final Uri CONTENT_URI = CloudGatewayMediaStore.buildContentUri(PATH);

            /**
             * Builds a content uri for all directory for a specific device
             *
             * @param deviceId the id of the device
             * @return the content uri
             */
            public static Uri getContentUriForDevice(long deviceId) {
                return CloudGatewayMediaStore.buildDeviceContentUri(deviceId, PATH);
            }

            /**
             * Builds an entry uri for a specific directory
             *
             * @param rowId the id of the directory
             * @return the entry uri
             */
            public static Uri getEntryUri(long rowId) {
                return CloudGatewayMediaStore.buildEntryIdUri(rowId, PATH);
            }
        }
    }

    /**
     * Media provider file table for particular cloud storage connector
     * example samsungdrive.db -> files table
     */
    public static class CloudFiles {
        public static final String PATH = "files";

        public CloudFiles() {
        }

        public static Uri createCloudFilesContentUri(String uriPath, long deviceId) {
            return Uri.parse(ContentResolver.SCHEME_CONTENT
                    + "://"
                    + AUTHORITY
                    + "/"
                    + uriPath
                    + "/"
                    + deviceId);
        }

        /**
         * Create a new "cloud file" URI for the given device id.
         *
         * @param deviceId The row id of the device.
         * @return the newly constructed Uri.
         */
        public static Uri getCloudFileUri(long deviceId) {
            return createCloudFilesContentUri(PATH, deviceId);
        }

        /**
         * Parses out the device id field from the given URI.
         *
         * @param uri The Uri to process
         * @return The device id from within the URI.
         */
        public static long getDeviceIdFromUri(Uri uri) {
            return CloudGatewayMediaStore.FileBrowser.getDeviceIdFromUri(uri, PATH);
        }

    }

    /**
     * Media provider table containing an index of all files in the media
     * storage, including non-media files. This should be used by applications
     * that work with non-media file types (text, HTML, PDF, etc) as well as
     * applications that need to work with multiple media file types in a single
     * query.
     */
    public static class Files {

        /**
         * Columns for files
         */
        public interface FileColumns extends MediaColumns {

            /**
             * @see android.provider.MediaStore.Images.ImageColumns#DATE_TAKEN
             */
            String DATE_TAKEN = Images.ImageColumns.DATE_TAKEN;

            /**
             * @see android.provider.MediaStore.Images.ImageColumns#ORIENTATION
             */
            String ORIENTATION = Images.ImageColumns.ORIENTATION;

            /**
             * @see android.provider.MediaStore.Audio.AudioColumns#ALBUM_ID
             */
            String ALBUM_ID = Audio.AudioColumns.ALBUM_ID;

            /**
             * @see android.provider.MediaStore.Files.FileColumns#MEDIA_TYPE_IMAGE
             */
            int MEDIA_TYPE_IMAGE = MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE;

            /**
             * @see android.provider.MediaStore.Files.FileColumns#MEDIA_TYPE_VIDEO
             */
            int MEDIA_TYPE_VIDEO = MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO;

            /**
             * @see android.provider.MediaStore.Files.FileColumns#MEDIA_TYPE_AUDIO
             */
            int MEDIA_TYPE_AUDIO = MediaStore.Files.FileColumns.MEDIA_TYPE_AUDIO;

            /**
             * Constant for the {@link android.provider.MediaStore.Files.FileColumns#MEDIA_TYPE} column indicating that record is an album
             */
            int MEDIA_TYPE_ALBUM = MEDIA_TYPE_AUDIO + 10;

            /**
             * Constant for the {@link android.provider.MediaStore.Files.FileColumns#MEDIA_TYPE} column indicating that record is an artist
             */
            int MEDIA_TYPE_ARTIST = MEDIA_TYPE_AUDIO + 11;

            /**
             * Constant for the {@link android.provider.MediaStore.Files.FileColumns#MEDIA_TYPE} column indicating that record is a genre
             */
            int MEDIA_TYPE_GENRE = MEDIA_TYPE_AUDIO + 12;

            /**
             * Constant for the {@link android.provider.MediaStore.Files.FileColumns#MEDIA_TYPE} column indicating that record is a document
             */
            int MEDIA_TYPE_DOCUMENT = MEDIA_TYPE_AUDIO + 13;

            /**
             * Constant for the {@link android.provider.MediaStore.Files.FileColumns#MEDIA_TYPE} column indicating that record is a document
             */
            int MEDIA_TYPE_DIRECTORY = MEDIA_TYPE_AUDIO + 14; //jsub12.lee_150717

            /**
             * @see android.provider.MediaStore.Files.FileColumns#MEDIA_TYPE_NONE
             */
            int MEDIA_TYPE_NONE = MediaStore.Files.FileColumns.MEDIA_TYPE_NONE;

            /**
             * The path to the locally generated thumbnail file for the media, if it exists yet. This is only for local media.
             */
            String THUMB_DATA = "thumb_data";

            String CHILD_CNT = "childCnt";
            String CHILD_DIR_CNT = "childDirCnt";

            String DESCENDANTS_CNT = "descendantsCnt";
            String DESCENDANTS_DIR_CNT = "descendantsDirCnt";

            String TRASH_PROCESSING = "processing";

            String HASH = "hash";
        }

        /**
         * Path for the files table
         */
        public static final String PATH = "files";

        /**
         * Content type for the files table
         */
        public static final String CONTENT_TYPE = CloudGatewayMediaStore.buildContentType(PATH);

        /**
         * Entry content type for the files table
         */
        public static final String ENTRY_CONTENT_TYPE = CloudGatewayMediaStore
                .buildEntryContentType(PATH);

        /**
         * Content uri for the files table
         */
        public static final Uri CONTENT_URI = CloudGatewayMediaStore.buildContentUri(PATH);

        /**
         * Builds a content uri for all files for a specific device
         *
         * @param deviceId the id of the device
         * @return the content uri
         */
        public static Uri getContentUriForDevice(int deviceId) {
            return CloudGatewayMediaStore.buildDeviceContentUri(deviceId, PATH);
        }

        /**
         * Builds an entry uri for a specific file
         *
         * @param rowId the id of the file
         * @return the entry uri
         */
        public static Uri getEntryUri(long rowId) {
            return CloudGatewayMediaStore.buildEntryIdUri(rowId, PATH);
        }
    }

    /**
     * Use the FileBrowser contract class to traverse the directory structure of remote devices and to get lists of files in those directories.
     * <ul>
     * <li>Use the DirectoryInfo contract class to get information about a given directory, such as the directory's name, path, and parent directory.</li>
     * <li>Use the FileList contract class to get a list of files and sub-directories within a given directory.</li>
     * </ul>
     * Both the DirectoryInfo and FileList classes
     * have a concept of a default directory where you can start file browsing. Once you start, you can traverse up from the DirectoryInfo Cursor's
     * parent directory, or you can traverse down using one of the sub-directories in the FileList cursor.<br>
     * <br>
     * Neither the FileList or DirectoryInfo Cursors support any selection or selectionArgs filtering.
     */
    public static class FileBrowser {

        private static Uri createFileBrowserUri(
                String uriPath,
                long deviceId,
                String directoryDocumentId) {

            try {
                directoryDocumentId = URLEncoder.encode(directoryDocumentId, "UTF-8");
            } catch (Exception uee) {
                return null;
            }
            return Uri.parse(ContentResolver.SCHEME_CONTENT
                    + "://"
                    + AUTHORITY
                    + "/"
                    + uriPath
                    + "/"
                    + deviceId
                    + "/"
                    + directoryDocumentId);
        }

        private static Uri createFileBrowserUri(String uriPath, long deviceId) {
            return Uri.parse(ContentResolver.SCHEME_CONTENT
                    + "://"
                    + AUTHORITY
                    + "/"
                    + uriPath
                    + "/"
                    + deviceId);
        }

        /**
         * All directory rows returned in a FileList Cursor will have this special {@link FileBrowserColumns#MIME_TYPE}.
         */
        public static final String MIME_TYPE_DIR = DocumentsContract.Document.MIME_TYPE_DIR;

        /**
         * Use this constant for the "Directory ID" when making your URIs to force the cursor to use the root directory.
         *
         * @see com.samsung.android.sdk.slinkcloud.FileList#createFileBrowserUri
         * @see com.samsung.android.sdk.slinkcloud.DirectoryInfo#getDirectoryInfoUri
         */
        public static final String ROOT_DIRECTORY_ID = "ROOT";

        /**
         * List of available fields for getting a list of files and directories.
         */
        public interface FileBrowserColumns {

            /**
             * This _ID field DOES NOT identify a file. It is NOT unique for any given file. It is simply
             * present for compatibility with the Android CursorAdapter class, which requires the _ID field.
             * The value of this field will simply be the row index within the cursor.
             * <p>
             * Type: INTEGER (long)
             * </p>
             */
            String _ID = BaseColumns._ID;

            /**
             * The human-readable name of the file or directory.
             * <p>
             * Type: TEXT
             * </p>
             */
            String DISPLAY_NAME = DocumentsContract.Document.COLUMN_DISPLAY_NAME;

            /**
             * The size of the file in bytes.
             * <p>
             * Type: INTEGER (long)
             * </p>
             */
            String SIZE = DocumentsContract.Document.COLUMN_SIZE;

            /**
             * The unique identifier of a file or directory on a device. This should not be considered to be human-readable.
             * <p>
             * Type: TEXT
             * </p>
             */
            String FILE_ID = DocumentsContract.Document.COLUMN_DOCUMENT_ID;

            /**
             * The mime-type of the file. If the record represents a directory, it will be equal to the this constant.
             * <p>
             * Type: TEXT
             * </p>
             */
            String MIME_TYPE = DocumentsContract.Document.COLUMN_MIME_TYPE;

            /**
             * The last modified date of the file or directory. This value is in milliseconds since Jan 1, 1970 UTC.
             * <p>
             * Type: INTEGER (long)
             * </p>
             */
            String LAST_MODIFIED = DocumentsContract.Document.COLUMN_LAST_MODIFIED;

            /**
             * If the Samsung Link Platform has an icon that can be used to represent this file or directory, this field will be filled out with the resource
             * id. It can be acquired by opening a resource Uri in your application's ContentResolver.
             * <p>
             * Type: INTEGER
             * </p>
             */
            String ICON_ID = DocumentsContract.Document.COLUMN_ICON;

            /**
             * If you are browsing the local device's file system with this API, then this field will be the path to that file. Non-local devices will always be
             * null.
             * <p>
             * Type: TEXT
             * </p>
             */
            String LOCAL_DATA = "local_data";
            ////zero project myfiles
            String MEDIA_ID = "asp_media_id";
        }

        /**
         * Available fields when opening a Cursor to get information about a directory.
         */
        public interface DirectoryInfoColumns {

            /**
             * The human-readable name of this directory.
             * <p>
             * Type: TEXT
             * </p>
             */
            String DISPLAY_NAME = DocumentsContract.Document.COLUMN_DISPLAY_NAME;

            /**
             * The unique id of this directory on this device. This value should not be considered human-readable.
             * <p>
             * Type: TEXT
             * </p>
             */
            String FILE_ID = DocumentsContract.Document.COLUMN_DOCUMENT_ID;

            /**
             * If the Samsung Link Platform has an icon that can be used to represent this directory, this field will be filled out with the resource id. It can
             * be acquired by opening a resource Uri in your application's ContentResolver.
             * <p>
             * Type: INTEGER
             * </p>
             */
            String ICON_ID = DocumentsContract.Document.COLUMN_ICON;

            /**
             * The number of files contained this directory.
             * <p>
             * Type: INTEGER
             * </p>
             */
            String FILE_COUNT = "file_count";

            /**
             * The human-readable path describing this directory.
             * <p>
             * Type: TEXT
             * </p>
             */
            String PATH = "path";

            /**
             * The human-readable display name of this directory's parent directory (if it exists).
             * <p>
             * Type: TEXT
             * </p>
             */
            String PARENT_DISPLAY_NAME = "parent_display_name";

            /**
             * The unique id of this directory's parent directory (if it exists). This should not be considered human-readable.
             * <p>
             * Type: TEXT
             * </p>
             */
            String PARENT_FILE_ID = "parent_id";

            /**
             * If the Samsung Link Platform has an icon that can be used to represent this directory's parent directory, this field will be filled out with the
             * resource id. It can be acquired by opening a resource Uri in your application's ContentResolver.
             * <p>
             * Type: INTEGER
             * </p>
             */
            String PARENT_ICON_ID = "parent_"
                    + DocumentsContract.Document.COLUMN_ICON;
        }

        private static final boolean isFileBrowserUri(Uri uri, String path) {
            boolean result = false;

            if (uri != null) {
                if (CloudGatewayMediaStore.AUTHORITY.equals(uri.getAuthority())) {
                    List<String> pathSegments = uri.getPathSegments();
                    if (pathSegments != null && pathSegments.size() > 1) {
                        result = pathSegments.get(0).equals(path);
                    }
                }
            }

            return result;
        }

        private static final long getDeviceIdFromUri(Uri uri, String path) {
            long result = 0;

            if (uri != null) {
                if (CloudGatewayMediaStore.AUTHORITY.equals(uri.getAuthority())) {
                    List<String> pathSegments = uri.getPathSegments();
                    if (pathSegments != null
                            && pathSegments.size() > 1
                            && pathSegments.get(0).equals(path)) {
                        result = Long.parseLong(pathSegments.get(1));
                    }
                }
            }

            return result;
        }

        private static final String getDirectoryIdFromUri(Uri uri, String path) {
            String result = null;

            if (uri != null) {
                if (CloudGatewayMediaStore.AUTHORITY.equals(uri.getAuthority())) {
                    List<String> pathSegments = uri.getPathSegments();
                    if (pathSegments != null
                            && pathSegments.size() > 2
                            && pathSegments.get(0).equals(path)) {

                        try {
                            result = URLDecoder.decode(pathSegments.get(2), "UTF-8");
                        } catch (UnsupportedEncodingException uee) {

                        } catch (IllegalArgumentException err) {
                            ////if file name has %, Urldecoder return IllegalArgumentException
                            try {
                                String data = pathSegments.get(2);
                                data = data.replaceAll("%(?![0-9a-fA-F]{2})", "%25");
                                data = data.replaceAll("\\+", "%2B");
                                result = URLDecoder.decode(data, "UTF-8");
                            } catch (Exception e) {

                            }
                        }
                    }
                }
            }

            return result;
        }

        /**
         * Use the DirectoryInfo contract class to obtain a Uri to get information on a directory from the directory's {@link DirectoryInfoColumns#FILE_ID}.
         * <p>
         * A Cursor returned from {@link ContentResolver#query(Uri, String[], String, String[], String)} using the Uri will only have a single row with all the
         * requested fields in it. Any selection or selectionArgs parameters passed are ignored.
         * </p>
         */
        public static class DirectoryInfo implements DirectoryInfoColumns {

            public static final String PATH = "directory_info";

            /**
             * Utility method to determine whether or not a given URI is for retrieving a directory info Cursor.
             *
             * @param uri The Uri to process.
             * @return true if the URI is for getting directory info
             */
            public static boolean isDirectoryInfoUri(Uri uri) {
                return FileBrowser.isFileBrowserUri(uri, PATH);
            }

            /**
             * Parses out the device id field from the given URI.
             *
             * @param uri The Uri to process.
             * @return The device id from within the URI.
             */
            public static long getDeviceIdFromUri(Uri uri) {
                return FileBrowser.getDeviceIdFromUri(uri, PATH);
            }

            /**
             * Parses out the directory's {@link DirectoryInfoColumns#FILE_ID} from the given URI.
             *
             * @param uri The Uri to process.
             * @return the FILE_ID embedded in the URI
             */
            public static String getDirectoryIdFromUri(Uri uri) {
                return FileBrowser.getDirectoryIdFromUri(uri, PATH);
            }

            /**
             * Create a "directory info" URI from the given device id and {@link FileBrowserColumns#FILE_ID}.
             *
             * @param deviceId            The row id of the device.
             * @param directoryDocumentId The value of the FILE_ID field for the directory.
             * @return the newly constructed Uri
             */
            public static Uri getDirectoryInfoUri(long deviceId, String directoryDocumentId) {
                return FileBrowser.createFileBrowserUri(PATH, deviceId, directoryDocumentId);
            }

            /**
             * Create a "directory info" URI that will provide info for the default file browsing location for the given device id.
             *
             * @param deviceId The row id of the device.
             * @return the newly constructed Uri
             */
            public static Uri getDefaultDirectoryInfoUri(long deviceId) {
                return FileBrowser.createFileBrowserUri(PATH, deviceId);
            }
        }

        /**
         * Use the FileList contract class to obtain a Uri in order to get a file list Cursor for a given directory's {@link FileBrowserColumns#FILE_ID}.
         * <p>
         * A Cursor returned from {@link ContentResolver#query(Uri, String[], String, String[], String)} using the Uri will contain a listing of all files and
         * sub-directories in the given directory. The selection and selectionArgs parameters are ignored. The Cursor only supports limited sorting support.
         * Only a single field is supported as either ASC or DESC:
         * <ul>
         * <li>{@link FileBrowserColumns#DISPLAY_NAME}</li>
         * <li>{@link FileBrowserColumns#LAST_MODIFIED}</li>
         * <li>{@link FileBrowserColumns#SIZE}</li>
         * </ul>
         * </p>
         */
        public static class FileList implements FileBrowserColumns {

            public static final String PATH = "file_browser";

            /**
             * Utility method to determine whether or not a given URI is for retrieving a file list Cursor.
             *
             * @param uri The Uri to process
             * @return true if the URI is for getting directory info
             */
            public static boolean isFileListUri(Uri uri) {
                return FileBrowser.isFileBrowserUri(uri, PATH);
            }

            /**
             * Parses out the device id field from the given URI.
             *
             * @param uri The Uri to process
             * @return The device id from within the URI.
             */
            public static long getDeviceIdFromUri(Uri uri) {
                return FileBrowser.getDeviceIdFromUri(uri, PATH);
            }

            /**
             * Parses out the directory's {@link DirectoryInfoColumns#FILE_ID} from the given URI.
             *
             * @param uri The Uri to process
             * @return the FILE_ID embedded in the URI
             */
            public static String getDirectoryIdFromUri(Uri uri) {
                return FileBrowser.getDirectoryIdFromUri(uri, PATH);
            }

            /**
             * Create a new "file list" URI for the given device id and directory's {@link FileBrowserColumns#FILE_ID}.
             *
             * @param deviceId            The row id of the device.
             * @param directoryDocumentId The FILE_ID of the directory you want the file list for.
             * @return the newly constructed Uri.
             */
            public static Uri getFileListUri(long deviceId, String directoryDocumentId) {
                return FileBrowser.createFileBrowserUri(PATH, deviceId, directoryDocumentId);
            }

            /**
             * Create a new "file list" URI for the given device id starting at the devices default directory.
             *
             * @param deviceId The row id of the device.
             * @return the newly constructed Uri.
             */
            public static Uri getDefaultFileListUri(long deviceId) {
                return FileBrowser.createFileBrowserUri(PATH, deviceId);
            }
        }

        ////zero project myfiles, new cursor for myfiles

        /**
         * Use the FileList2 contract class to obtain a Uri in order to get a file list Cursor for a given directory's {@link FileBrowserColumns#FILE_ID}.
         * <p>
         * A Cursor returned from {@link ContentResolver#query(Uri, String[], String, String[], String)} using the Uri will contain a listing of all files and
         * sub-directories in the given directory. The selection and selectionArgs parameters are ignored. The Cursor only supports limited sorting support.
         * Only a single field is supported as either ASC or DESC:
         * <ul>
         * <li>{@link FileBrowserColumns#DISPLAY_NAME}</li>
         * <li>{@link FileBrowserColumns#LAST_MODIFIED}</li>
         * <li>{@link FileBrowserColumns#SIZE}</li>
         * </ul>
         * </p>
         */
        public static class FileList2 implements FileBrowserColumns {

            public static final String PATH = "file_browser2";

            /**
             * Utility method to determine whether or not a given URI is for retrieving a file list Cursor.
             *
             * @param uri The Uri to process
             * @return true if the URI is for getting directory info
             */
            public static boolean isFileListUri(Uri uri) {
                return FileBrowser.isFileBrowserUri(uri, PATH);
            }

            /**
             * Parses out the device id field from the given URI.
             *
             * @param uri The Uri to process
             * @return The device id from within the URI.
             */
            public static long getDeviceIdFromUri(Uri uri) {
                return FileBrowser.getDeviceIdFromUri(uri, PATH);
            }

            /**
             * Parses out the directory's {@link DirectoryInfoColumns#FILE_ID} from the given URI.
             *
             * @param uri The Uri to process
             * @return the FILE_ID embedded in the URI
             */
            public static String getDirectoryIdFromUri(Uri uri) {
                return FileBrowser.getDirectoryIdFromUri(uri, PATH);
            }

            /**
             * Create a new "file list" URI for the given device id and directory's {@link FileBrowserColumns#FILE_ID}.
             *
             * @param deviceId            The row id of the device.
             * @param directoryDocumentId The FILE_ID of the directory you want the file list for.
             * @return the newly constructed Uri.
             */
            public static Uri getFileListUri(long deviceId, String directoryDocumentId) {
                return FileBrowser.createFileBrowserUri(PATH, deviceId, directoryDocumentId);
            }

            /**
             * Create a new "file list" URI for the given device id starting at the devices default directory.
             *
             * @param deviceId The row id of the device.
             * @return the newly constructed Uri.
             */
            public static Uri getDefaultFileListUri(long deviceId) {
                //return FileBrowser.createFileBrowserUri(PATH, deviceId);
                return FileBrowser.createFileBrowserUri(PATH, deviceId, ROOT_DIRECTORY_ID);
            }

            /**
             * Create a new "file list" URI for the given device id starting at the devices default directory (legacy).
             *
             * @param deviceId The row id of the device.
             * @return the newly constructed Uri.
             */
            public static Uri getDefaultFileListUriLegacy(long deviceId) {
                return FileBrowser.createFileBrowserUri(PATH, deviceId);
            }
        }

    }

    /**
     * Defined methods to be used with {@link ContentResolver#call(Uri, String, String, Bundle)}
     */
    public static class CallMethods {

        /**
         * Path to use for call methods
         */
        public static final String PATH = "call_method";

        /**
         * Content uri for call methods
         */
        public static final Uri CONTENT_URI = CloudGatewayMediaStore.buildContentUri(PATH);

        /**
         * Key for an integer result code returned in the result bundle
         */
        public static final String KEY_RESULT = "method_result";

        /**
         * Key for a string result returned in the result bundle
         */
        public static final String KEY_RESULT_STR = "method_result_str";

        /**
         * Key for an integer quota error in the result bundle
         */
        public static final String KEY_RESULT_QUOTA_ERROR = "method_result_quota_error";

        /**
         * constant value for an ok result
         */
        public static final int RESULT_OK = 0;

        /**
         * constant value for an error result
         */
        public static final int RESULT_ERROR = -1;

        /**
         * Call method to return whether the user is signed into Samsung Link or not. Being signed in means that the
         * user has signed in with a valid Samsung Account and the local device is registered as a peer device.
         * <p>
         * Input: None<br>
         * Output: <br>
         * KEY_RESULT - boolean indicating whether user is signed it or not
         * </p>
         */
        public static class GetSignInStatus {

            /**
             * Name of this call method
             */
            public static final String NAME = "com.sec.samsunglink.api.SamsungLinkMediaStore.CallMethods.GetSignInStatus.NAME";
        }

        /**
         * Call method used to check if 3-box file transfer is supported between
         * two devices. Use {@link CloudGatewayFileTransferUtils#is3boxSupported(long, long)} to utilize this call method.
         * <p>
         * Input:<br>
         * args - null<br>
         * extras
         * <p>
         * <table border="1">
         * <tr>
         * <td>EXTRA_SOURCE_DEVICE_ID</td>
         * <td>_ID (long) of the source device for the transfer</td>
         * </tr>
         * <tr>
         * <td>EXTRA_TARGET_DEVICE_ID</td>
         * <td>_ID (long) of the target device for the transfer</td>
         * </tr>
         * </table>
         * </p>
         * Output:<br>
         * KEY_RESULT = boolean indicating both the source and target devices support 3-box file transfers
         * </p>
         */
        public static class Is3boxSupported {

            /**
             * Name of this call method
             */
            public static final String NAME = "com.sec.samsunglink.api.SamsungLinkMediaStore.CallMethods.Is3boxSupported.NAME";

            /**
             * key for source device id extra
             */
            public static final String EXTRA_SOURCE_DEVICE_ID = "com.samsung.android.sdk.slinkcloud.SamsungLinkMediaStore.CallMethods.Is3boxSupported.EXTRA_SOURCE_DEVICE_ID";

            /**
             * key for target device id extra
             */
            public static final String EXTRA_TARGET_DEVICE_ID = "com.samsung.android.sdk.slinkcloud.SamsungLinkMediaStore.CallMethods.Is3boxSupported.EXTRA_TARGET_DEVICE_ID";
        }

        /**
         * Call method used to check current SLP initialized status.
         * <p>
         * Input: none<br>
         * Output:<br>
         * KEY_RESULT - boolean indicating whether SLP has been initialized.
         * </p>
         */
        public static class IsInitialized {

            /**
             * Name of this call method
             */
            public static final String NAME = "com.samsung.android.sdk.slinkcloud.CloudGatewayMediaStore.CallMethods.IsInitialized.NAME";
        }

        /**
         * Private Access: Call method to sign out of storage service
         */
        public static class SignOutOfStorageService {

            /**
             * Name of this call method
             */
            public static final String NAME = "com.samsung.android.sdk.slinkcloud.CloudGatewayMediaStore.CallMethods.SignOutOfStorageService.NAME";
        }

        /**
         * Private Access: Call method to get auth info (encrypted)
         */
        public static class GetAuthInfo {

            /**
             * Name of this call method
             */
            public static final String NAME = "com.samsung.android.sdk.slinkcloud.CloudGatewayMediaStore.CallMethods.GetAuthInfo.NAME";
        }

        /**
         * Call method to request a meta-data sync with an online peer device.
         * <p>
         * Input:<br>
         * <p>
         * <table border="1">
         * <tr>
         * <td>args</td>
         * <td>null</td>
         * </tr>
         * <tr>
         * <td>extras.INTENT_ARG_DEVICE_ID</td>
         * <td>Device ID of the device to be refreshed</td>
         * </tr>
         * <tr>
         * <td>extras.INTENT_ARG_MEDIA_TYPE</td>
         * <td>Optional media type to have highest priority</td>
         * </tr>
         * </table>
         * </p>
         * </p>
         **/
        public static class RequestNetworkRefresh {

            /**
             * Name of this call method
             */
            public static final String NAME = "com.samsung.android.sdk.slinkcloud.CloudGatewayMediaStore.CallMethods.RequestNetworkRefresh.NAME";

            /**
             * key for id of device to refresh value should be a long
             */
            public static final String INTENT_ARG_DEVICE_ID = "com.samsung.android.sdk.slinkcloud.CloudGatewayMediaStore.CallMethods.RequestNetworkRefresh.INTENT_ARG_DEVICE_ID";

            /**
             * key for specific media type to refresh (Optional). A given device MAY honor this media type and give it precendence over other types. The values
             * should be one of
             * the following: {@link FileColumns#MEDIA_TYPE_IMAGE}, {@link FileColumns#MEDIA_TYPE_AUDIO}, {@link FileColumns#MEDIA_TYPE_VIDEO},
             * {@link FileColumns#MEDIA_TYPE_DOCUMENT}, or {@link FileColumns#MEDIA_TYPE_NONE}.
             */
            public static final String INTENT_ARG_MEDIA_TYPE = "com.samsung.android.sdk.slinkcloud.CloudGatewayMediaStore.CallMethods.RequestNetworkRefresh.INTENT_ARG_MEDIA_TYPE";
        }

        /**
         * Call method to control remote folder (mkdir, Rename, Rmdir)
         * <p>
         * Input: none<br>
         * Output: KEY_RESULT - boolean value to check the result.
         * </p>
         */
        public static class MRRControl {
            public static final String NAME = "com.samsung.android.sdk.slinkcloud.CloudGatewayMediaStore.CallMethods.MRRControl.NAME";
            public static final String INTENT_ARG_CMD = "INTENT_ARG_CMD";
            public static final String INTENT_ARG_DEVICEID = "INTENT_ARG_DEVICEID";
            public static final String INTENT_ARG_PARENTDIRECTORYID = "INTENT_ARG_PARENTDIRECTORYID";
            public static final String INTENT_ARG_SOURCEID = "INTENT_ARG_SOURCEID";

            // newItem (ex newDirectoryName, newName, newParentPath ..)
            public static final String INTENT_ARG_NEWITEM = "INTENT_ARG_NEWITEM";
        }

        public static class MRRControlBatch {
            public static final String NAME = "com.samsung.android.sdk.slinkcloud.CloudGatewayMediaStore.CallMethods.MRRControlBatch.NAME";
            public static final String INTENT_ARG_BATCH_CMD = "INTENT_ARG_BATCH_CMD";
            public static final String INTENT_ARG_BATCH_DEVICEID = "INTENT_ARG_BATCH_DEVICEID";
            public static final String INTENT_ARG_BATCH_PARENTDIRECTORYID = "INTENT_ARG_BATCH_PARENTDIRECTORYID";
            public static final String INTENT_ARG_BATCH_SOURCEID = "INTENT_ARG_BATCH_SOURCEID";

            // newItem (ex newDirectoryName, newName, newParentPath ..)
            public static final String INTENT_ARG_BATCH_NEWITEM = "INTENT_ARG_BATCH_NEWITEM";
        }

        /**
         * Call method for app to get the versatile information from the platform, which can't be queried.
         * <p>
         * Input: none<br>
         * Output: Bundle which includes various results
         * </p>
         */
        public static class GetVersatileInformation {

            /**
             * Name of this call method
             */
            public static final String NAME = "com.samsung.android.sdk.slinkcloud.CloudGatewayMediaStore.CallMethods.GetVersatileInformation.NAME";
            /**
             * information type
             */
            public static final String INTENT_ARG_INFO_TYPE = "INTENT_ARG_INFO_TYPE";
            /**
             * device id
             */
            public static final String INTENT_ARG_DEVICEID = "INTENT_ARG_DEVICEID";
        }

        /**
         * Call method to get remote devices which has completed initial sync.
         */
        public static class GetInitialSyncDoneDevices {

            /**
             * Name of this call method
             */
            public static final String NAME = "com.samsung.android.sdk.slinkcloud.CloudGatewayMediaStore.CallMethods.GetInitialSyncDoneDevices.NAME";
        }

        /**
         * Call method to get remote devices which has completed initial sync.
         */
        public static class ProviderReservation {

            /**
             * Name of this call method
             */
            public static final String NAME = "com.samsung.android.sdk.slinkcloud.CloudGatewayMediaStore.CallMethods.ProviderReservation.NAME";
        }

        /**
         * Call method to keep alive for service
         */
        public static class KeepServiceAlive {

            /**
             * Name of this call method
             */
            public static final String NAME = "com.samsung.android.sdk.slinkcloud.CloudGatewayMediaStore.CallMethods.KeepServiceAlive.NAME";
            public static final String INTENT_ARG_ENABLE_BOOLEAN = "INTENT_ARG_ENABLE_BOOLEAN";
            public static final String INTENT_ARG_PKGNAME = "INTENT_ARG_PKGNAME";
        }

        /**
         * Call method to keep alive for service
         */
        public static class SetWifiOnly {

            /**
             * Name of this call method
             */
            public static final String NAME = "com.samsung.android.sdk.slinkcloud.CloudGatewayMediaStore.CallMethods.SetWifiOnly.NAME";
            public static final String INTENT_ARG_ENABLE_BOOLEAN = "INTENT_ARG_ENABLE_BOOLEAN";
            public static final String RET_ENABLE_BOOLEAN = "RET_ENABLE_BOOLEAN";
        }


        public static class CloudStorageListInfo {

            /**
             * Name of this call method
             */
            public static final String NAME = "com.samsung.android.sdk.slinkcloud.CloudGatewayMediaStore.CallMethods.CloudStorageListInfo.NAME";

            /**
             * String identifying the provider type you are requesting information about.
             */
            public static final String INTENT_ARG_NEED_JSON = "com.samsung.android.sdk.slinkcloud.CloudGatewayMediaStore.CallMethods.CloudStorageListInfo.INTENT_ARG_NEED_JSON";
        }

        public static class CancelFileTransfer {

            /**
             * Name of this call method
             */
            public static final String NAME = "com.samsung.android.sdk.slinkcloud.CloudGatewayMediaStore.CallMethods.CancelFileTransfer.NAME";
            public static final String INTENT_ARG_SESSION_ID = "INTENT_ARG_SESSION_ID";
        }
    }

    /**
     * Columns for the ArchivedMedia table.
     */
    public interface ArchivedMediaColumns extends BaseSamsungLinkColumns {

        /**
         * @see com.samsung.android.sdk.slinkcloud.CloudGatewayMediaStore.Files.FileColumns#SIZE
         */
        String SIZE = com.samsung.android.sdk.slinkcloud.CloudGatewayMediaStore.Files.FileColumns.SIZE;

        /**
         * @see com.samsung.android.sdk.slinkcloud.CloudGatewayMediaStore.Files.FileColumns#DATE_TAKEN
         */
        String DATE_TAKEN = com.samsung.android.sdk.slinkcloud.CloudGatewayMediaStore.Files.FileColumns.DATE_TAKEN;

        /**
         * @see com.samsung.android.sdk.slinkcloud.CloudGatewayMediaStore.Files.FileColumns#DISPLAY_NAME
         */
        String DISPLAY_NAME = com.samsung.android.sdk.slinkcloud.CloudGatewayMediaStore.Files.FileColumns.DISPLAY_NAME;
    }

    /**
     * Table containing information related to media that has been archived. An entry is written into this table when a media item is archived, or auto-uploaded
     * successfully.
     */
    public static class ArchivedMedia {

        public static final String TABLE_NAME = "ARCHIVED_MEDIA";

        public static final String PATH = "archivedMedia";

        public static final Uri CONTENT_URI = CloudGatewayMediaStore.buildContentUri(PATH);

        public static final String ENTRY_CONTENT_TYPE = CloudGatewayMediaStore
                .buildEntryContentType(PATH);

        public static final String CONTENT_TYPE = CloudGatewayMediaStore.buildContentType(PATH);

        /**
         * Builds an entry uri for a specific archived media record
         *
         * @param rowId the id of the record
         * @return the entry uri
         */
        public static Uri getEntryUri(long rowId) {
            return CloudGatewayMediaStore.buildEntryIdUri(rowId, PATH);
        }

        /**
         * Builds a content uri for all archived media records for a specific device
         *
         * @param deviceId the id of the device
         * @return the content uri
         */
        public static Uri getContentUriForDevice(long deviceId) {
            return CloudGatewayMediaStore.buildDeviceContentUri(deviceId, PATH);
        }
    }

    public static class StorageProvider {

        public static final String TABLE_NAME = "STORAGE_PROVIDER";

        public static final String PATH = "storageProvider";

        public static final Uri CONTENT_URI = CloudGatewayMediaStore.buildContentUri(PATH);

        public static final String ENTRY_CONTENT_TYPE = CloudGatewayMediaStore
                .buildEntryContentType(PATH);

        public static final String CONTENT_TYPE = CloudGatewayMediaStore.buildContentType(PATH);

        public static Uri getEntryUri(long rowId) {
            return CloudGatewayMediaStore.buildEntryIdUri(rowId, PATH);
        }

        public static Uri getContentUriForDevice(long deviceId) {
            return CloudGatewayMediaStore.buildDeviceContentUri(deviceId, PATH);
        }

    }
}

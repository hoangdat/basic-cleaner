package com.samsung.android.sdk.slinkcloud;

import android.content.ContentResolver;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by sec on 2015-11-11.
 */
public class CloudGatewayThumbnailUtils {
    private static CloudGatewayThumbnailUtils sInstance = null;

    public static synchronized CloudGatewayThumbnailUtils getInstance(Context context) {
        if (context == null) {
            throw new NullPointerException("context is null");
        }
        if (sInstance == null) {
            sInstance = new CloudGatewayThumbnailUtils();
        }
        return sInstance;
    }

    private static InputStream getThumbnailInput(ContentResolver cr, int nMediaType, long deviceId, long _dbId, boolean bNonBlock, int nMinReqSize) {
        InputStream is = null;
        String path;

        if (nMediaType == CloudGatewayMediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE) {
            path = CloudGatewayMediaStore.Images.Thumbnails.PATH;
        } else if (nMediaType == CloudGatewayMediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO) {
            path = CloudGatewayMediaStore.Video.Thumbnails.PATH;
        } else {
            Log.e("ERR", "can't find thumbnail media type");
            return null;
        }
        Uri uri = CloudGatewayMediaStore.buildEntryIdUriForDevice(deviceId, _dbId, path);
        Uri.Builder builder = uri.buildUpon();
        builder.appendQueryParameter(
                CloudGatewayMediaStore.ThumbnailColumns.QUERY_STR_WIDTH,
                Integer.toString(nMinReqSize));
        builder.appendQueryParameter(
                CloudGatewayMediaStore.ThumbnailColumns.QUERY_STR_HEIGHT,
                Integer.toString(nMinReqSize));
        builder.appendQueryParameter(
                CloudGatewayMediaStore.ThumbnailColumns.QUERY_STR_SKIP_CACHE_GET, "false");
        builder.appendQueryParameter(
                CloudGatewayMediaStore.ThumbnailColumns.QUERY_STR_SKIP_CACHE_PUT, "false");
        if (bNonBlock) {
            builder.appendQueryParameter(
                    CloudGatewayMediaStore.ThumbnailColumns.QUERY_STR_CACHE_ONLY, "true");
        }
        uri = builder.build();
        try {
            is = cr.openInputStream(uri);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return is;
    }

    private static void closeThumbnailInput(InputStream is) {
        if (is != null) {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static Bitmap getThumbnailBitmap(ContentResolver cr, int nMediaType, long deviceId, long _dbId, int nMinReqSize, int nScaledWidth, int nScaledHeight, boolean bNonBlock, BitmapFactory.Options bitmapOptions) {
        Bitmap bmpResult = null;
        InputStream is = null;

        try {
            is = getThumbnailInput(cr, nMediaType, deviceId, _dbId, bNonBlock, nMinReqSize);
            if (bitmapOptions == null) {
                bitmapOptions = new BitmapFactory.Options();
                bitmapOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;
            }
            bmpResult = BitmapFactory.decodeStream(is, null, bitmapOptions);
            if (nScaledWidth > 0 && nScaledHeight > 0) {
                Bitmap bmpNew = ThumbnailUtils.extractThumbnail(bmpResult, nScaledWidth, nScaledHeight);
                if (bmpNew != null && bmpNew != bmpResult) {
                    bmpResult.recycle();
                    bmpResult = bmpNew;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeThumbnailInput(is);
        }
        return bmpResult;
    }
}
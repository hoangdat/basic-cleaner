
package com.samsung.android.sdk.slinkcloud;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.database.ContentObserver;
import android.util.Log;

import com.samsung.android.sdk.slinkcloud.CloudGatewayMediaStore.CallMethods;

/**
 * Utility class for providing a way to launch SamsungLink Intro/Login screen
 */
public class CloudGatewayStorageUtils {

    public static final String WELLKNOWN_STORAGE_TYPE_GOOGLEDRIVE = "google";
    public static final String WELLKNOWN_STORAGE_TYPE_ALIBABA = "alibaba";


    private static final String TAG = "mfl_ApiLib_"
            + CloudGatewayStorageUtils.class.getSimpleName();

    /**
     * Intent action to start Samsung Link's "add web storage" Activity. This will guide the user through picking and signing into a supported web storage
     * service.
     *
     * @see #createStorageSignInIntent(String)
     */
    public static final String ACTION_ADD_WEB_STORAGE = "com.samsung.android.sdk.slinkcloud.CloudGatewayStorageUtils.ACTION_ADD_WEB_STORAGE";

    /**
     * Intent extra for the type of web storage to sign in with using {@link #ACTION_ADD_WEB_STORAGE}.
     * <p>
     * TYPE: String
     * </p>
     */
    public static final String EXTRA_STORAGE_TYPE = "com.samsung.android.sdk.slinkcloud.CloudGatewayStorageUtils.EXTRA_STORAGE_TYPE";

    private static CloudGatewayStorageUtils sInstance;

    private final Context context;

    private ContentObserver contentObserver = null;

    /**
     * Gets the singleton CloudGatewaySignInUtils instance.
     *
     * @param context Context
     * @return the singleton CloudGatewaySignInUtils instance
     * @throws NullPointerException if context is null
     */
    public static synchronized CloudGatewayStorageUtils getInstance(Context context) {
        if (context == null) {
            throw new NullPointerException("context is null");
        }
        if (sInstance == null) {
            sInstance = new CloudGatewayStorageUtils(context);
        }
        return sInstance;
    }

    /**
     * Private constructor.
     *
     * @param context Context
     */
    private CloudGatewayStorageUtils(Context context) {
        this.context = context.getApplicationContext();
    }

    /**
     * Sign out of a web storage service.
     *
     * @param storageDeviceId The row id for the device record which represents a web storage service.
     */
    public void signOutOfStorageService(int storageDeviceId) {
        try {
            this.context.getContentResolver().call(
                    CallMethods.CONTENT_URI,
                    CallMethods.SignOutOfStorageService.NAME,
                    String.valueOf(storageDeviceId),
                    null);
        } catch (IllegalArgumentException e) {
            ////tskim fix stress test issue when called after platform disabling
            Log.e(TAG, "::signOutOfStorageService maybe platform is disabled");
        }
    }

    /**
     * Create a new Intent to launch the "add web storage" Activity.
     *
     * @param storageType The type of storage to be added or null if you want the user to pick from a list of available storages.
     * @return The newly created Intent.
     * @see #ACTION_ADD_WEB_STORAGE
     */
    public Intent createStorageSignInIntent(String storageType) {
        Intent intent = new Intent(ACTION_ADD_WEB_STORAGE);
        //// android L accept only explicit Intent
        intent.setComponent(new ComponentName(
                CloudGatewayConstants.TARGET_APK_PACKAGE,
                "platform.com.mfluent.asp.ui.AddWebStorageActivity"));
        if (storageType != null) {
            intent.putExtra(EXTRA_STORAGE_TYPE, storageType);
        }
//        CloudGatewayCommonUtils.getInstance(this.context).addAppIdToIntent(intent);
        return intent;
    }

    @Override
    protected void finalize() throws Throwable {
        if (this.contentObserver != null) {
            this.context.getContentResolver().unregisterContentObserver(this.contentObserver);
        }
    }
}
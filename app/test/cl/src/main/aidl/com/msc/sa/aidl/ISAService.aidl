package com.msc.sa.aidl;

import com.msc.sa.aidl.ISACallback;

/**
* AIDL Interface Class.
* Provide Register Callback, unRegisterCallback, requestAccesToken requestChecklistValidation, requestDisclaimerAgreement, requestAuthCode, requestSCloudAccessToken Function.
*/  
interface ISAService {

	/**
	* register Service Appicaion Callback and Client info.
	*/ 
	String		registerCallback(String clientID, String clientSecret, String packageName, ISACallback callback);
	
	/**
	* unregister Service Appicaion Callback.
	*/ 
	boolean		unregisterCallback(String registrationCode);
	
	/**
	* request AccessToken.
	*/ 	
	boolean		requestAccessToken(int requestID, String registrationCode, in Bundle data);
	
	/**
	* request Checklist Validation.
	*/ 	
	boolean		requestChecklistValidation(int requestID, String registrationCode, in Bundle data);
	
	/**
	* request Disclaimer Agreement.
	*/ 	
	boolean	    requestDisclaimerAgreement(int requestID, String registrationCode, in Bundle data);
	
	
	/**
	* request AuthCode.
	*/ 	
	boolean		requestAuthCode(int requestID, String registrationCode, in Bundle data);
	
	/**
	* request SCloud AccessToken.
	*/ 		
	boolean		requestSCloudAccessToken(int requestID, String registrationCode, in Bundle data);	
}
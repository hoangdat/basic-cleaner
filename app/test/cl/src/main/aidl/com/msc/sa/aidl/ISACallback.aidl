package com.msc.sa.aidl;

/**
* AIDL Interface Class.
* Provide callback functions.
*/  
 interface ISACallback {
 
	 /**
	*  receive requestAccessToken callback data.
	*/  
	void onReceiveAccessToken(int requestID, boolean isSuccess,  in Bundle resultData);
	
	/**
	*  receive requestChecklistValidation callback data.
	*/ 
	void onReceiveChecklistValidation(int requestID, boolean isSuccess, in Bundle resultData);
	
	/**
	*  receive requestDisclaimerAgreement callback data.
	*/ 
	void onReceiveDisclaimerAgreement(int requestID, boolean isSuccess, in Bundle resultData);
		
		
	/**
	*  receive requestAuthCode callback data.
	*/ 
	void onReceiveAuthCode(int requestID, boolean isSuccess,  in Bundle resultData);
	
	/**
	*  receive requestSCloudAccessToken callback data.
	*/ 
	void onReceiveSCloudAccessToken(int requestID, boolean isSuccess,  in Bundle resultData);
}


package uicommon.com.mfluent.asp.util;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;

import com.mfluent.log.Log;

public class AccountUtil {

    /**
     * PCloudHandler Log Tag
     */
    private static final String TAG = "mfl_AccountUtil";

    /**
     * Google Account Type
     */
    private static final String GOOGLE_ACCOUNT_TYPE = "com.google";

    /**
     * check Google Account
     *
     * @param context Application Context
     * @return If Google Account exist, true
     */
    public static boolean hasGoogleAccount(Context context) {
        Account[] accounts = AccountUtil.getAccountsByType(context, GOOGLE_ACCOUNT_TYPE);

        // check Google Account
        if (accounts != null && accounts.length > 0) {
            Log.d(TAG, "[hasGoogleAccount] - Google Account exist");
            return true;
        } else {
            Log.w(TAG, "[hasGoogleAccount] - No Google Account");
            return false;
        }
    }

    /**
     * Lists all accounts of a particular type.
     *
     * @param context Application Context
     * @param type    The type of accounts to return, null to retrieve all accounts
     * @return An array of Account, one per matching account. Empty (never null)
     * if no accounts of the specified type have been added.
     */
    private static Account[] getAccountsByType(Context context, String type) {
        AccountManager manager = AccountManager.get(context);
        return manager.getAccountsByType(type);
    }
}

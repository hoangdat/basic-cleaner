
package uicommon.com.mfluent.asp.util;

import android.os.Bundle;

import java.lang.reflect.Method;

/**
 * Created by sec on 2015-05-12.
 */
public class FrameworkReflector {

    public static void AbstractWindowedCursor_setExtras(Object childObj, Bundle bundle) {
        try {
            ClassLoader myClassLoader = ClassLoader.getSystemClassLoader();
            Class aClass = myClassLoader.loadClass("android.database.AbstractWindowedCursor");
            Method method = aClass.getMethod("setExtras", Bundle.class);
            method.invoke(childObj, bundle);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static int DatabaseUtils_cursorPickFillWindowStartPosition(Object childObj, int position, int size) {
        int nRet = 0;
        try {
            ClassLoader myClassLoader = ClassLoader.getSystemClassLoader();
            Class aClass = myClassLoader.loadClass("android.database.DatabaseUtils");
            Method method = aClass.getMethod("cursorPickFillWindowStartPosition", int.class, int.class);
            nRet = (Integer) method.invoke(childObj, position, size);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return nRet;
    }

    public static <T> boolean contains(T[] array, T value) {
        return FrameworkReflector.indexOf(array, value) != -1;
    }

    /**
     * Return first index of {@code value} in {@code array}, or {@code -1} if
     * not found.
     */
    public static <T> int indexOf(T[] array, T value) {
        if (array == null) {
            return -1;
        }
        for (int i = 0; i < array.length; i++) {
            if (array[i] == null) {
                if (value == null) {
                    return i;
                }
            } else {
                if (value != null && array[i].equals(value)) {
                    return i;
                }
            }
        }
        return -1;
    }

    public static boolean ArrayUtils_contains(Object childObj, String[] projections, String field) {
        boolean bRet = false;
        try {
            bRet = FrameworkReflector.contains(projections, field);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bRet;
    }

}

/**
 *
 */

package uicommon.com.mfluent.asp.util;

import com.mfluent.asp.common.media.thumbnails.ImageInfo;
import com.mfluent.log.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;

import platform.com.mfluent.asp.util.IOUtils;

/**
 * @author Ilan Klinghofer
 */
public class FileImageInfo {

    private ImageInfo mImageInfo;

    private final File mFile;

    private final File mImageInfoFile;

    /**
     * @param imageInfo
     * @param file
     */
    public FileImageInfo(ImageInfo imageInfo, File file) {
        super();
        mImageInfo = new ImageInfo(imageInfo);
        mFile = file;
        mImageInfoFile = null;
    }

    public FileImageInfo(File infoFile, File file) {
        super();
        mFile = file;
        mImageInfoFile = infoFile;
    }

    /**
     * @return the imageInfo
     */
    public ImageInfo getImageInfo() {
        if (mImageInfo == null) {
            FileInputStream fis = null;
            ObjectInputStream objectInputStream = null;
            try {
                fis = new FileInputStream(mImageInfoFile);
                objectInputStream = new ObjectInputStream(fis);
                mImageInfo = (ImageInfo) objectInputStream.readObject();
            } catch (Exception e) {
                Log.e(this, "getImageInfo() - Exception : " + e);
            } finally {
                IOUtils.closeQuietly(objectInputStream);
                IOUtils.closeQuietly(fis);
            }
        }
        return mImageInfo;
    }

    /**
     * @return the file
     */
    public File getFile() {
        return mFile;
    }
}

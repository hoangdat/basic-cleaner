package uicommon.com.mfluent.asp.util;

import android.os.Handler;

import com.mfluent.log.Log;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Callable;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ScheduledHandlerBasedTaskWithoutIdleThread {

    public Runnable mMyRun = null;

    private Lock tasksLock = new ReentrantLock();

    private Timer mCheckTimer = null;
    private MyTimerTask mTimerTask = null;

    public void setRunnable(Runnable runner) {
        mMyRun = runner;
    }

    public void triggerEvent(long nDelayTime) {
        if (nDelayTime>0) {
            Handler h = new Handler();
            h.postDelayed(new Runnable() {
                @Override
                public void run() {
                    CachedExecutorService.getInstance().submit(new TriggeredEventTask());
                }
            },nDelayTime);

        } else {
            CachedExecutorService.getInstance().submit(new TriggeredEventTask());
        }
    }

    public void setRepeatTimer(long nDelayTime, long nPeriodicTime) {
        mCheckTimer = new Timer();
        mTimerTask = new MyTimerTask();
        mCheckTimer.schedule(mTimerTask, nDelayTime, nPeriodicTime);
    }

    public void cancelRepeatTimer() {
        if(mCheckTimer != null) {
            mCheckTimer.cancel();
        }
    }

    public void singleRunImmediate() {
        tasksLock.lock();

        try {
            if(mMyRun != null) {
                mMyRun.run();
            }
        } catch (Exception e) {
            Log.e(this, "singleRunImmediate() - Exception : " + e.getMessage());
        } finally {
            tasksLock.unlock();
        }
    }

    private class MyTimerTask extends TimerTask {
        @Override
        public void run() {
            try {
                singleRunImmediate();
            } catch (Exception e) {
                Log.e(this, "run() - Exception : " + e.getMessage());
            }
        }
    }

    private class TriggeredEventTask implements Callable<Void> {
        @Override
        public Void call() throws Exception {
            try {
                singleRunImmediate();
            } catch (Exception e) {
                Log.e(this, "call() - Exception : " + e.getMessage());
            }
            return null;
        }
    }
}

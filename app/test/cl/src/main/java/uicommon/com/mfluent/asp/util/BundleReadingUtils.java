
package uicommon.com.mfluent.asp.util;

import android.os.Bundle;

public class BundleReadingUtils {

	private BundleReadingUtils() {
	}

	public static boolean getBoolean(String key, Bundle... bundles) {
		for (Bundle temp : bundles) {
			if (temp != null && temp.containsKey(key)) {
				return temp.getBoolean(key);
			}
		}

		return false;
	}

	public static boolean getBoolean(String key, boolean defaultValue, Bundle... bundles) {
		for (Bundle temp : bundles) {
			if (temp != null && temp.containsKey(key)) {
				return temp.getBoolean(key);
			}
		}

		return defaultValue;
	}

	public static int getInt(String key, int defaultValue, Bundle... bundles) {
		for (Bundle temp : bundles) {
			if (temp != null && temp.containsKey(key)) {
				return temp.getInt(key);
			}
		}

		return defaultValue;
	}

	public static int getInt(String key, Bundle... bundles) {
		return BundleReadingUtils.getInt(key, 0, bundles);
	}
}

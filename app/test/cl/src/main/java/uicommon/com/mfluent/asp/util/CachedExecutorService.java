
package uicommon.com.mfluent.asp.util;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class CachedExecutorService {

	private static final ExecutorService sCachedExecutorService = Executors.newCachedThreadPool();

	public synchronized static ExecutorService getInstance() {
		return sCachedExecutorService;
	}
}

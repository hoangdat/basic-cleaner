
package uicommon.com.mfluent.asp;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Helper class for managing singletons.
 * 
 * @author Ilan Klinghofer
 */
public class ServiceLocator {

	private static final Map<Class<?>, Object> SINGLETONS = new ConcurrentHashMap<Class<?>, Object>();

	// private static Context mContext;

	@SuppressWarnings("unchecked")
	public static <T>T get(Class<T> c) {
		return (T) SINGLETONS.get(c);
	}

	@SuppressWarnings("unchecked")
	public static <T>T getWithClassName(Class<T> c, String className) {
		Set<Class<?>> keyset = SINGLETONS.keySet();
		T object = null;

		for (Class<?> key : keyset) {
			if (className.equals(key.getSimpleName())) {
				object = (T) SINGLETONS.get(key);
				break;
			}
		}
		if (object == null && className.equals("ASPApplication")) {
			object = ServiceLocator.get(c);
		}
		return object;
	}

	public static <T>void bind(T obj, Class<? extends T> c) {
		if (obj == null) {
			SINGLETONS.remove(c);
		} else {
			SINGLETONS.put(c, obj);
		}
	}

	// public static void reset() {
	// SINGLETONS.clear();
	// }

	/**
	 * get the application context (same as
	 * ServiceLocator.get(ASPApplication.class).getApplicationContext())
	 */
	// public static Context getContext() { return mContext; }

	// public static void setContext(Context context) { mContext = context; }
}

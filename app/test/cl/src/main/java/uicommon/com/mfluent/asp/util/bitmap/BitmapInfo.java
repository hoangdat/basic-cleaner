/**
 * 
 */

package uicommon.com.mfluent.asp.util.bitmap;

import android.graphics.Bitmap;

import com.mfluent.asp.common.media.thumbnails.ImageInfo;
import com.mfluent.log.Log;

public class BitmapInfo {

	private final ImageInfo mImageInfo;

	private final Bitmap mBitmap;

	private int mRefCount;

	public BitmapInfo(ImageInfo imageInfo, Bitmap bitmap) {
		mImageInfo = new ImageInfo(imageInfo);
		mBitmap = bitmap;
		mRefCount = 0;
	}

	public ImageInfo getImageInfo() {
		return mImageInfo;
	}

	public synchronized Bitmap getBitmap() {
		return mBitmap;
	}

	public final synchronized void retain() {
		if (!mBitmap.isRecycled()) {
			mRefCount++;
		}
	}

	public final synchronized void release() {
		if (mRefCount > 0) {
			mRefCount--;
			if (mRefCount == 0 && !mBitmap.isRecycled()) {
				Log.d(this, "Recycling bitmap. ImageInfo: " + mImageInfo + ", size: " + mBitmap.getByteCount() + " bytes.");
				mBitmap.recycle();
			}
		}
	}
}

/**
 * 
 */

package com.mfluent.asp.common.io.util;

/**
 * @author Ilan Klinghofer
 */
public class NullStreamProgressListener implements StreamProgressListener {

	/*
	 * (non-Javadoc)
	 * @see com.mfluent.asp.io.util.StreamProgressListener#bytesTransferred(int)
	 */
	@Override
	public void bytesTransferred(long numberOfBytes) {
	}
}

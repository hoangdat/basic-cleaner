
package com.mfluent.cloud.samsungdrive.common;

import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.text.TextUtils;
import android.util.Base64;
import android.widget.Toast;

import com.mfluent.asp.cloudstorage.api.sync.CloudStorageAppDelegate;
import com.mfluent.asp.cloudstorage.api.sync.CloudStorageSync;
import com.mfluent.asp.cloudstorage.api.sync.CloudStreamInfo;
import com.mfluent.asp.common.clouddatamodel.AbsCloudContext.AccessTokenListener;
import com.mfluent.asp.common.clouddatamodel.AbsCloudContext.AuthorizationState;
import com.mfluent.asp.common.datamodel.ASPFile;
import com.mfluent.asp.common.datamodel.ASPFileBrowser;
import com.mfluent.asp.common.datamodel.ASPFileSortType;
import com.mfluent.asp.common.datamodel.ASPMediaStore;
import com.mfluent.asp.common.datamodel.CloudDataModel;
import com.mfluent.asp.common.datamodel.CloudDevice;
import com.mfluent.asp.common.datasource.AspMediaId;
import com.mfluent.asp.common.io.util.StreamProgressListener;
import com.mfluent.asp.common.media.thumbnails.ImageInfo;
import com.mfluent.asp.common.util.CloudStorageConstants;
import com.mfluent.asp.common.util.CloudStorageError;
import com.mfluent.asp.common.util.FileTypeHelper;
import com.mfluent.asp.common.util.MimeType;
import com.mfluent.cloud.samsungdrive.Drive;
import com.mfluent.cloud.samsungdrive.Drive.DriveRequest;
import com.mfluent.cloud.samsungdrive.SamsungDriveContext;
import com.mfluent.cloud.samsungdrive.common.Metadata.Id;
import com.mfluent.cloud.samsungdrive.exception.CloudAuthorizationException;
import com.mfluent.cloud.samsungdrive.samsungpushplatform.SppManager;
import com.mfluent.log.Log;
import com.samsung.android.sdk.SsdkUnsupportedException;
import com.samsung.android.sdk.scloud.api.drive.batch.BatchParam;
import com.samsung.android.sdk.scloud.api.drive.batch.BatchRequest;
import com.samsung.android.sdk.scloud.decorator.drive.Batch;
import com.samsung.android.sdk.scloud.decorator.drive.DriveFile;
import com.samsung.android.sdk.scloud.decorator.drive.DriveFileList;
import com.samsung.android.sdk.scloud.decorator.drive.Files;
import com.samsung.android.sdk.scloud.decorator.quota.QuotaInfo;
import com.samsung.android.sdk.scloud.decorator.quota.SamsungCloudQuota;
import com.samsung.android.sdk.scloud.exception.SamsungCloudException;
import com.samsung.android.sdk.scloud.listeners.NetworkStatusListener;
import com.samsung.android.sdk.scloud.listeners.ProgressListener;
import com.samsung.android.sdk.scloud.listeners.ResponseListener;
import com.samsung.android.sdk.slinkcloud.CloudGatewayConstants.OperationType;
import com.samsung.android.sdk.slinkcloud.CloudGatewayMediaStore;
import com.samsung.android.slinkcloud.R;

import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;

import platform.com.mfluent.asp.datamodel.filebrowser.CachedFileBrowser;
import platform.com.mfluent.asp.util.AspFileUtilsSLPF;
import platform.com.mfluent.asp.util.FileUtils;
import platform.com.mfluent.asp.util.MFLStorageManagerSLPF;
import platform.com.mfluent.asp.util.NetworkUtilSLPF;

/**
 * <p>
 * Base class for a cloud storage connector implementation.
 * </p>
 * <p>
 * It contains definitions, information, and procedures that are:
 * <ul>
 * <li>independent of a specific cloud storage connector, but</li>
 * <li>specific to the All Share - Play application's use of a cloud storage, via implementation of the CloudStorageSync interface.</li>
 * </ul>
 * </p>
 * <p>
 * Implementations of specific cloud storages:
 * <ul>
 * <li>extend this base class,</li>
 * <li>implement abstract methods that are necessary to the procedures in this class,</li>
 * <li>override methods that are not appropriate to the specific cloud storage, and</li>
 * <li>provide an application independent but cloud storage specific "context" which directly or indirectly extends CloudContext.</li>
 * </ul>
 * </p>
 *
 * @param <T> the "context" used by this cloud storage connector
 */
public abstract class CloudStorageBase<T extends CloudContext> implements CloudStorageSync, AccessTokenListener {

    private static final String ACCESS_SECRET_ID = "accessSecret";
    private static final String ACCESS_TOKEN_ID = "accessToken";
    private static final String METADATA_USER_ID = "metadataUserId";
    private static final String LARGEST_CHANGE_ID = "largestChangeId";
    private static final String SIGNIN_STATE = "signin_state";
    private static final String ACCOUNT_NAME = "account_name";

    public static final int BULK_OPERATION_LIMIT = 250;
    private static final int API_CALL_BATCH_LIMIT = 50;
    private static final int TOKEN_MAX_WAITING_COUNT = 10;
    private static final int RETRY_MAX_COUNT = 1;

    private static final long ERROR_REACH_MAX_ITEM = 400110571L;
    private static final long ERROR_FILE_DOES_NOT_EXIST = 400110311L;
    private static final long ERROR_SUPPORT_ONLY_TRASHED_FILE = 400110532L;

    private static final String MESSAGE_EXCEEDED_STORAGE_QUOTA = "Exceeded Storage quota";
    private static final String MESSAGE_REACH_MAX_ITEMS = "Reach the max items of storage";


    protected boolean isSignIn = false;

    private boolean mDuringFullSync = false;

    //jsub12.lee_150721
    private CancellationSignal mCancellationSignal = null;
    private String mCancellationFileId = null;

    public enum PreferenceType {
        LAST_SYNC_TIME,
        SPP_REG_ID,
    }

    /**
     * The media types supported by procedures in this class, listed in the order in which they will sync, if they will sync as indicated by the presence of the
     * metadata columns attribute. <br>
     * <br>
     * The attributes of each media type are:
     * <ul>
     * <li>the prefix of MIME types for this type of media</li>
     * <li>the vector of metadata columns supported for this type of media (not null => this metadata type will be sync'ed)</li>
     * <li>the integer value that identifies this type of media in the All Share - Play application</li>
     * <li>the name of the directory in the cloud storage where files of this media type are found</li>
     * <li>an indication of whether or not artist and album sub-directories are used in the cloud storage for files of this media type</li>
     * <li>an indication of whether or not files of this media type can be captioned</li>
     * <li>an indication of whether or not the metadata for files of this type include a date taken value</li>
     * </ul>
     */
    public enum MediaType {
        /* @formatter:off */
        image(new MimeTypeMatcher("image/"), Metadata.imageColumns, AspMediaId.MEDIA_TYPE_IMAGE, CloudStorageConstants.PHOTO_FOLDER, ASPMediaStore.Images.Media.CONTENT_URI, true),
        audio(new MimeTypeMatcher("audio/"), Metadata.audioColumns, AspMediaId.MEDIA_TYPE_AUDIO, CloudStorageConstants.MUSIC_FOLDER, ASPMediaStore.Audio.Media.CONTENT_URI, true),
        video(new MimeTypeMatcher("video/"), Metadata.videoColumns, AspMediaId.MEDIA_TYPE_VIDEO, CloudStorageConstants.VIDEO_FOLDER, ASPMediaStore.Video.Media.CONTENT_URI, true),
        document(new DocumentMatcher(), Metadata.documentColumns, AspMediaId.MEDIA_TYPE_DOCUMENT, CloudStorageConstants.DOCUMENTS_FOLDER, ASPMediaStore.Documents.Media.CONTENT_URI, false),
        directory(new MimeTypeMatcher(MimeType.DIR_MIMETYPE), Metadata.directoryColumns, AspMediaId.MEDIA_TYPE_DIRECTORY, null, ASPMediaStore.Directory.Media.CONTENT_URI, false), //jsub12.lee_150721
        none(null, Metadata.defaultColumns, AspMediaId.MEDIA_TYPE_NONE, CloudStorageConstants.FILES_FOLDER, null, false), //jsub12_150922
        caption(null, null, AspMediaId.MEDIA_TYPE_CAPTION, CloudStorageConstants.VIDEO_FOLDER, null, false),
        captionIndex(null, null, AspMediaId.MEDIA_TYPE_CAPTION_INDEX, CloudStorageConstants.VIDEO_FOLDER, null, false),
        /* @formatter:on */;

        private interface CloudFileMediaTypeMatcher {

            boolean matches(CloudFile file);
        }

        public final CloudFileMediaTypeMatcher matcher;

        public final String[] columnNames;

        public final int aspMediaType;

        public final String rootSubdirectoryName;

        public final Uri aspContentUri;

        public final boolean hasThumbnails;

        /* @formatter:off */
        MediaType(
                CloudFileMediaTypeMatcher matcher,
                String[] columnNames,
                int aspMediaType,
                String rootSubdirectoryName,
                Uri aspContentUri,
                boolean hasThumbnails) {
            /* @formatter:on */
            this.matcher = matcher;
            this.columnNames = columnNames;
            this.aspMediaType = aspMediaType;
            this.rootSubdirectoryName = rootSubdirectoryName;
            this.aspContentUri = aspContentUri;
            this.hasThumbnails = hasThumbnails;
        }

        /**
         * get the media type for the specified MIME type
         *
         * @param file the CloudFile for which to get the media type
         * @return the media type, if any, for the specified MIME type
         */
        public static MediaType forFile(CloudFile file) {
            MediaType ret = null;
            if (file != null) {
                for (MediaType mediaType : MediaType.values()) {
                    if (mediaType.matcher != null && mediaType.matcher.matches(file)) {
                        ret = mediaType;
                        break;
                    }
                }
            }

            return ret;
        }

        private static class MimeTypeMatcher implements CloudFileMediaTypeMatcher {
            private final String mimeType;

            public MimeTypeMatcher(String mimeType) {
                this.mimeType = mimeType;
            }

            @Override
            public boolean matches(CloudFile file) {
                String fileMimeType = file.getMimeType();
                return fileMimeType != null && fileMimeType.startsWith(mimeType);
            }
        }

        private static class DocumentMatcher implements CloudFileMediaTypeMatcher {
            @Override
            public boolean matches(CloudFile file) {
                return checkMimeType(file.getMimeType()) ? true : FileTypeHelper.isAspDocument(file.getName());
            }

            private boolean checkMimeType(String fileMimeType) {
                boolean bRet = false;
                if (fileMimeType != null) {
                    if (!fileMimeType.contains("image")
                            && !fileMimeType.contains("audio")
                            && !fileMimeType.contains("video")
                            && !fileMimeType.contains("vnd.asp.dir/dir")) {
                        bRet = true;
                    }

                }
                return bRet;
            }
        }
    }

    /**
     * <p>
     * The "types" of cloud storage connectors.
     * </p>
     * <p>
     * Each cloud storage connector implemented as an extension of this class must be in this list.
     * </p>
     * <p>
     * The attributes of each cloud storage connector "type" are:
     * <ul>
     * <li>the "id" of this cloud storage type in the All Share - Play application, and</li>
     * <li>the "name" of this cloud storage type in the Samsung Link application and, more importantly, <b>to the storage gateway.</b></li>
     * </ul>
     * </p>
     * Note: do NOT reference the names in CloudStorageConstants!!! This creates an undesirable "link" between the plugins and the APK!!! <br/>
     * E.g., to add a new connector or to change the name of an existing connector, the APK would have to be modified; this is NOT good.
     */
    public enum Type {
        SAMSUNG_DRIVE("15", "samsung" + "drive");

        final String id;
        final String name;

        Type(String id, String name) {
            this.id = id;
            this.name = name;
        }
    }

    protected final Type type;

    protected final AuthType authType;

    protected final boolean supportsSignUp;

    protected final T context;

    protected CloudDirectory rootDirectory;

    CloudDevice device = null;

    CloudStorageAppDelegate appDelegate = null;

    private final AtomicBoolean syncRunning = new AtomicBoolean(false);

    ContentResolver contentResolver = null;

    Context appContext = null;

    CloudDataModel dataModel = null;

    protected int preferredThumbnailWidth = -1;

    protected int preferredThumbnailHeight = -1;

    private String metadataUserId; // the userId whose metadata is stored

    private IntentFilter[] intentFilters;

    private boolean syncMetadata = true;

    private long usedCapacity;
    private long numberOfAuthorizeRequests = 0l;
    private long numberOfGetTokenRequests = 0l;
    private String mCurChangeId = null;

    private BroadcastReceiver mSppResultReceiver = null;

    private HashMap<OperationType, ArrayList> mInProgressOpMap = new HashMap<>();

    /**
     * constructor for the CloudStorageBase class subclasses *MUST* invoke this constructor (actually, java will force this)
     *
     * @param type           the type of cloud storage that the subclass implements
     * @param authType       the type of authorization this cloud storage type uses
     * @param supportsSignUp true <=> this cloud storage type supports sign-up from the device
     * @param context        the context of this cloud storage connector
     */
    protected CloudStorageBase(Type type, AuthType authType, boolean supportsSignUp, T context) {
        this.type = type;
        this.authType = authType;
        this.supportsSignUp = supportsSignUp;
        this.context = context;
    }

	/*
     * inversion of control setters from interface CloudStorageSync
	 */

    @Override
    public void setCloudDevice(CloudDevice device) {
        this.device = device;
    }

    @Override
    public void setStorageAppDelegate(CloudStorageAppDelegate appDelegate) {
        this.appDelegate = appDelegate;
    }

    @Override
    public void setContentProvider(ContentResolver contentResolver) {
        this.contentResolver = contentResolver;
    }

    protected CloudDirectory instantiateRootNotFoundInDatabase() {
        CloudDirectory rootDirectory = new CloudDirectory(context, null, "");
        rootDirectory.setRoot(true);
        rootDirectory.setCloudId(CloudStorageConstants.CLOUD_ROOT);

        return rootDirectory;
    }

    @Override
    public void setApplicationContext(Context appContext) {
        this.appContext = appContext;
        DatabaseHelper databaseHelper = new DatabaseHelper(this);
        context.setDatabaseHelper(databaseHelper);

//        registerSppReceiver();

        CloudDirectory rootDirectory = databaseHelper.getRootDirectory(context);
        if (rootDirectory == null) {
            rootDirectory = instantiateRootNotFoundInDatabase();
            databaseHelper.saveOrUpdate(rootDirectory);
        }
        rootDirectory.setPath("/");
        rootDirectory.setDescription("user root directory");
        rootDirectory.setRoot(true);
        rootDirectory.setCloudId(Files.getRootFolder().fileId);

        Log.d(this, "setApplicationContext() - rootDirectory : " + rootDirectory);
        this.rootDirectory = rootDirectory;
    }

    public void registerSppReceiver() {
        if (getPreferenceString(PreferenceType.SPP_REG_ID) == null) {
            mSppResultReceiver = SppManager.registerSppReceiver(appContext, type.name, appContext);
        }
    }

    @Override
    public void setDataModel(CloudDataModel dataModel) {
        this.dataModel = dataModel;
    }

    /*
     * inversion of control initialization from interface CloudStorageSync
     */
    @Override
    public void init() {
        SharedPreferences preferences = getSharedPreferences();

		/*
         * initialize the metadata user ID
		 */
        metadataUserId = preferences.getString(METADATA_USER_ID, null);
        String accessToken = preferences.getString(ACCESS_TOKEN_ID, null);

        isSignIn = preferences.getBoolean(SIGNIN_STATE, false);

        // In case SignIn state is true, get Drive object.
        if (isSignIn) {
            context.setUserId(preferences.getString(ACCOUNT_NAME, null));
        }
        context.setCloudStorage(this);
        context.setAndCheckToken(accessToken);

        String pushToken = getPreferenceString(CloudStorageBase.PreferenceType.SPP_REG_ID);
        if (pushToken != null) {
            DriveConstants.apiClient.pushToken = pushToken;
        }

        DriveConstants.apiClient.pushName = "SPP";
        DriveConstants.apiClient.pushAppId = SppManager.MY_APPID;

        mCurChangeId = getCurChangeIdFromPref();
        Log.i(this, "init():: metadataUserId : [" + metadataUserId + "], isSignIn : ["
                + isSignIn + "], account name : [" + context.getUserId() + "], mCurChangeId : [" + mCurChangeId + "]");
    }

    protected void saveAccountInfoToPref(String _accountName, boolean signInState) {
        Editor editor = getSharedPreferences().edit();

        if (_accountName == null) {
            Log.i(this, "saveAccountInfoToPref() ; ACCOUNT_NAME & SIGNIN_STATE removed");
            editor.remove(ACCOUNT_NAME);
            editor.remove(SIGNIN_STATE);
        } else {
            editor.putString(ACCOUNT_NAME, _accountName);
            editor.putBoolean(SIGNIN_STATE, signInState);
            Log.i(this, "saveAccountInfoToPref() ; ACCOUNT_NAME : " + _accountName + ", SIGNIN_STATE : " + signInState);
        }
        editor.apply();
    }

    @Override
    public IntentFilter[] getIntentFilters() {
        if (intentFilters == null) {
            initIntentFilters();
        }
        return intentFilters;
    }

    private void initIntentFilters() {
        IntentFilter intentFilters[] = null;
        switch (authType) {
            case OAUTH10:
            case OAUTH20: {
                /* @formatter:off */
                intentFilters = new IntentFilter[]{
                        new IntentFilter(CloudStorageSync.CLOUD_OAUTH1_RESPONSE),
                        new IntentFilter(CLOUD_SIGNIN),
                        device.buildDeviceIntentFilterForAction(CloudDevice.BROADCAST_DEVICE_REFRESH),
                };
                /* @formatter:on */
                break;
            }
            case NONE:
            case USER_PASSWORD:
            default:
                /* @formatter:off */
                intentFilters = new IntentFilter[]{
                        new IntentFilter(CLOUD_SIGNIN),
                        this.device.buildDeviceIntentFilterForAction(CloudDevice.BROADCAST_DEVICE_REFRESH),
                };
                /* @formatter:on */
                break;
        }
        this.intentFilters = intentFilters;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (StringUtils.equals(action, CloudDevice.BROADCAST_DEVICE_REFRESH)) {
            boolean syncMetadata = intent.hasExtra(CloudDevice.REFRESH_FROM_KEY);
            // someday we might set syncThisFirst from an extra in this intent
            requestSync(syncMetadata);
//        } else if (StringUtils.equals(action, CloudStorageSync.CLOUD_APP_STARTED)) {
//            registerSppReceiver();
        }
    }

    @Override
    public void reset() {
        long startTime = System.currentTimeMillis();
        try {
            Log.i(this, "reset() starting");
            setMetadataUserId(null);
            resetAccessTokenAndSecret();
            saveAccountInfoToPref(null, false);
            saveCurChangeIdToPref(null);
            context.getDatabaseHelper().reset();
            isSignIn = false;
        } finally {
            long endTime = System.currentTimeMillis();
            Log.i(this, "reset() took " + (endTime - startTime) + " millisecond(s)");
        }
    }

    @Override
    public File downloadThumbnail(ImageInfo imageInfo, String pathToDownload, String fileName) throws IOException {
        Log.i(this, "downloadThumbnail() - " + fileName);
        File file = null;

        try {
            String mediaId = imageInfo.getSourceMediaId();
            if (mediaId == null) {
                throw new FileNotFoundException("no mediaId in downloadThumbnail() request");
            }

            Drive drive = ((SamsungDriveContext) context).getDrive(appContext);
            DriveFile sourceFile;
            try (Cursor cursor = context.getDatabaseHelper().query(DatabaseHelper.FILE_TABLE,
                    new String[]{ASPMediaStore.MediaColumns.SOURCE_MEDIA_ID},
                    ASPMediaStore.MediaColumns.SOURCE_MEDIA_ID + "=?", new String[]{mediaId}, null)) {

                if (cursor != null && cursor.moveToFirst()) {
                    Log.i(this, "downloadThumbnail() - make srcDFile from DB information");
                    sourceFile = new DriveFile();
                    sourceFile.fileId = mediaId;
                    sourceFile.type = DriveFile.FILE;
                } else {
                    Log.e(this, "downloadThumbnail() - there is no info in DB");
                    return null;
                }
            }

            Drive.Files.DownloadThumbnail thumbnailRequest = drive.files().downloadThumbnail(sourceFile,
                    pathToDownload, fileName, com.samsung.android.sdk.scloud.decorator.drive.Drive.Thumbnail.SIZE_240);
            if (retryRequest(thumbnailRequest, RETRY_MAX_COUNT)) {
                file = new File(pathToDownload, fileName);
            }
        } catch (IOException ioe) {
            throw ioe;
        }

        return file;
    }

    @Override
    public void setPreferredThumbnailSize(int width, int height) {
        preferredThumbnailWidth = width;
        preferredThumbnailHeight = height;
    }

    public void setCancellationSignal(CancellationSignal cancelSignal, String fileId) {
        Log.d(this, "setCancellationSignal : " + fileId);
        mCancellationSignal = cancelSignal;
        mCancellationFileId = fileId;
    }

    @Override
    public final ASPFileBrowser<?> getCloudStorageFileBrowser(String storageGatewayId, ASPFileSortType sort, boolean forceReload, boolean isBrowserForView)
            throws InterruptedException, IOException {

        Log.i(this, "getCloudStorageFileBrowser() called, storageGatewayId = " + storageGatewayId
                + ", mCurChangeId = " + mCurChangeId + ", isSignIn = " + isSignIn + ", syncRunning = " + syncRunning.get());

        long oldTime = 0;
        boolean isVisited = false;

        if (isSignIn && !syncRunning.get() && !mDuringFullSync && (mCurChangeId == null)) {
            requestSync(true);
        }

        while (isSignIn && (syncRunning.get() || mDuringFullSync || (mCurChangeId == null))) {
            if (!isVisited) {
                isVisited = true;
                if (storageGatewayId != null
                        && !storageGatewayId.equals(CloudStorageConstants.CLOUD_ROOT)
                        && isEmptyDirectory(storageGatewayId)) {
                    Log.i(this, "getCloudStorageFileBrowser() - " + storageGatewayId + "is empty directory");
                    return null;
                }
            }

            if (mDuringFullSync) {  // in the process of initial full sync
                if (storageGatewayId == null)
                    storageGatewayId = CloudStorageConstants.CLOUD_ROOT;//Files.getRootFolder().fileId
                if (((SamsungDriveContext) context).cloudFilesMap_cache.containsKey(storageGatewayId)) {
                    // the directory has been already  synched.
                    break;
                }
            }

            long curTime = System.currentTimeMillis();
            if (curTime > oldTime + 5000) {
                oldTime = curTime;
                Log.d(this, "getCloudStorageFileBrowser() - sync is running.." + storageGatewayId
                        + " " + mDuringFullSync + " " + syncRunning.get() + " " + mCurChangeId);
            }

            if (mCancellationSignal != null && mCancellationSignal.isCanceled()) {
                Log.i(this, "getCloudStorageFileBrowser() - canceled signal");
                return null;
            }
            //tskim because of cancel signal
            Thread.sleep(100);
        }

        Log.d(this, "getCloudStorageFileBrowser() - mDuringFullSync = " + mDuringFullSync);
        CloudDirectory directory = null;
        if (StringUtils.isNotEmpty(storageGatewayId)) {
            if (mDuringFullSync && ((SamsungDriveContext) context).cloudFilesMap_cache.containsKey(storageGatewayId)) {
                directory = (CloudDirectory) ((SamsungDriveContext) context).cloudFilesMap_cache.get(storageGatewayId);
            } else {
                if (CloudGatewayMediaStore.Trash.Trash_ID.equals(storageGatewayId)) {
                    directory = getTrashDirectory();
                } else {
                    directory = context.getDatabaseHelper().getDirectoryByCloudId(context, storageGatewayId);
                }
            }
        }
        Log.i(this, "getCloudStorageFileBrowser() - directory = " + directory);

        boolean bRet = false;
        CloudFileBrowser fileBrowser = (CloudFileBrowser) getCloudStorageFileBrowser();
        if (fileBrowser != null) {
            synchronized (fileBrowser) {
                bRet = fileBrowser.init(directory, sort, true);
            }
        }
        return bRet ? fileBrowser : null;
    }

    @Override
    public int getCountOfChild(ASPFile file_param) {
        return !(file_param instanceof CloudFile) ? 0 : ((CloudFile) file_param).getChildCount();
    }

    @Override
    public int getCountOfChildDir(ASPFile file_param) {
        return !(file_param instanceof CloudFile) ? 0 : ((CloudFile) file_param).getChildDirCount();
    }

    @Override
    public int getCountOfDescendants(ASPFile file_param) {
        return !(file_param instanceof CloudFile) ? 0 : ((CloudFile) file_param).getDescendantsCount();
    }

    @Override
    public int getCountOfDescendantDir(ASPFile file_param) {
        return !(file_param instanceof CloudFile) ? 0 : ((CloudFile) file_param).getDescendantDirCount();
    }

    protected abstract boolean idsUseBase64();

    protected CloudDirectory getDirectoryNotInDatabase(String storageGatewayId) throws InterruptedException, IOException {

        Log.d(this, "getDirectoryNotInDatabase() - storageGatewayId : " + storageGatewayId);

        storageGatewayId = decodeStorageGatewayFileId(storageGatewayId);
        Log.d(this, "getDirectoryNotInDatabase() - decode storageGatewayId : " + storageGatewayId);
        CloudDirectory directory = getRootDirectory();
        String path = Metadata.Id.getFilePathFromSourceMediaId(storageGatewayId);
        Log.d(this, "getDirectoryNotInDatabase() - path : " + path);

        String[] pathComponents = path.split("\\" + CloudContext.PATH_SEPARATOR);
        for (int i = 0; i < pathComponents.length; i++) {
            String pathComponent = pathComponents[i];
            Log.d(this, "getDirectoryNotInDatabase() - pathComponent : " + pathComponent);

            if (StringUtils.isEmpty(pathComponent)) {
                continue;
            }
            CloudFile file = directory.getFile(pathComponent);

            if (!(file instanceof CloudDirectory)) {
                throw new IllegalArgumentException("invalid storageGatewayId: " + storageGatewayId);
            }
            directory = (CloudDirectory) file;
        }

        return directory;
    }

    @Override
    public ASPFileBrowser<?> getCloudStorageFileBrowser() {
        CloudFileBrowser cloudFileBrowser = null;
        if (getAuthorization()) {
            cloudFileBrowser = new CloudFileBrowser(this);
        }
        broadcastAuthorizationState();
        return cloudFileBrowser;
    }

    private boolean isTokenError(long type) {
        return (SamsungCloudException.Code.BAD_ACCESS_TOKEN == type || SamsungCloudException.Code.BAD_ACCESS_TOKEN2 == type || SamsungCloudException.Code.BAD_FORMAT == type);
    }

    private QuotaInfo getQuota() {
        QuotaInfo ret = null;
        try {
            Log.d(this, "getQuota() : CountryCode : " + DriveConstants.sCountryCode + ", WebStorageUserId : " + context.getUserId() + ", user id : " + DriveConstants.apiClient.uid
                    + ", cid : " + DriveConstants.apiClient.cid + ", Token : " + DriveConstants.apiClient.accessToken + ", Push " + DriveConstants.apiClient.pushAppId + " " + DriveConstants.apiClient.pushToken + " " + DriveConstants.apiClient.pushName);
            SamsungCloudQuota samsungCloudQuota = new SamsungCloudQuota(appContext, DriveConstants.APPID, appContext.getPackageName(), DriveConstants.sCountryCode, DriveConstants.apiClient);
            ret = samsungCloudQuota.usage.get();
        } catch (SamsungCloudException e) {
            Log.e(this, "getQuota() : " + e.getMessage());

            long type = e.getType();
            if (isTokenError(type)) {
                handleTokenException();
            }
        } catch (SsdkUnsupportedException e) {
            Log.e(this, "getQuota() : " + e.getMessage());
        } finally {
            return ret;
        }
    }

    @Override
    public void sync() {
        if ((device == null) || !isSignIn) {
            Log.i(this, "sync() - sync fail device : " + (device == null) + " ,signIn : " + isSignIn);
            return;
        }

        Log.i(this, "sync() - sync syncRunning : " + syncRunning);
        if (!syncRunning.compareAndSet(false, true)) {
            Log.i(this, "sync() - sync is already running");
            return;
        }
        Log.i(this, "sync() - sync is called " + syncMetadata);

        try {
            if (device != null) {
                device.setWebStorageUserId(context.getUserId());
                dataModel.updateDevice(device);
            }

            if (syncMetadata) {
                if (mCurChangeId == null) {
                    mDuringFullSync = true;
                    setCurChangeId();
                    if (doFullSync()) {
                        getTrashSync();
                    } else {
                        handleSyncFail();
                    }
                    mDuringFullSync = false;
                } else {
                    doDeltaSync();
                }
            }
        } catch (Exception e) {
            Log.e(this, "sync() - " + e.getMessage());
            e.printStackTrace();
            mDuringFullSync = false;
            handleSyncFail();
        } finally {
            syncRunning.set(false);
            Log.i(this, "sync() - sync syncRunning set false");
        }
    }

    private void handleSyncFail() {
        setLargestChangeID(null);
        if (NetworkUtilSLPF.isNetworkAvailable())
            requestSync(true);
    }

    private void doDeltaSync() {
        Drive drive = ((SamsungDriveContext) context).getDrive(appContext);
        Drive.Changes.ListWithoutPaging changedListRequest;
        ArrayList<DriveFile> changedFiles = new ArrayList<>();
        DriveFileList changedList;
        String nextChangePoint;
        try {
            do {
                changedListRequest = drive.changes().listWithoutPaging(mCurChangeId, true);
                changedList = retryRequest(changedListRequest, RETRY_MAX_COUNT);
                changedFiles.addAll(changedList.getItems());

                nextChangePoint = changedList.getNextChangePoint();
                if ((nextChangePoint != null) && !nextChangePoint.equals("")) {
                    setLargestChangeID(nextChangePoint);
                }
                if (changedFiles.size() > CloudStorageBase.BULK_OPERATION_LIMIT) {
                    ((SamsungDriveContext) context).putDBforDeltaSync(changedFiles);
                }
            } while ((changedList != null) && changedList.hasNext());

            if (changedFiles.size() > 0) {
                ((SamsungDriveContext) context).putDBforDeltaSync(changedFiles);
                //CachedFileBrowser.deltaFileLayerAdd();
            }
        } catch (IOException e) {
            Log.d(this, "doDeltaSync() - IOException: " + e.getMessage());
            handleSyncFail();
        }
    }

    private void setCurChangeId() throws IOException {
        Drive drive = ((SamsungDriveContext) context).getDrive(appContext);
        Drive.Changes.GetChangePoint changePointRequest = drive.changes().getChangePoint();
        String curChangeId = retryRequest(changePointRequest, RETRY_MAX_COUNT);
        setLargestChangeID(curChangeId);
    }

    private CloudDirectory getTrashDirectory() {
        CloudDirectory trashDirectory = new CloudDirectory(context, null, CloudGatewayMediaStore.Trash.Trash_ID);
        trashDirectory.setCloudId(CloudGatewayMediaStore.Trash.Trash_ID);
        return trashDirectory;
    }

    private void getTrashSync() throws IOException {
        CloudDirectory trashDirectory = getTrashDirectory();

        Drive drive = ((SamsungDriveContext) context).getDrive(appContext);
        Drive.Trash.List trashListRequest = drive.trash().list();
        DriveFileList trashList = retryRequest(trashListRequest, RETRY_MAX_COUNT);

        if (trashList != null) {
            ((SamsungDriveContext) context).putDBforFullSync(trashDirectory, trashList.getItems());
        }
    }

    private static int sPrevLevel = 0;
    private static int sLevelDirCnt = 0;
    private final static int sMAX_LEVEL_CNT = 13; // # of items at list view

    private boolean doFullSync() {
        boolean bRet = true;
        Log.d(this, "doFullSync() - Start");
        try {
            deleteAllFileInfoFromDB();
            CloudDirectory root = getRootDirectory();
            root.setLevel(0);

            ((SamsungDriveContext) context).cloudDirListQToSync = new LinkedList<>();
            ((SamsungDriveContext) context).cloudDirListQToSync.add(root);

            CloudDirectory targetDirectory;
            while (isSignedIn() && bRet && ((targetDirectory = ((SamsungDriveContext) context).getDirectoryFromQtoSync()) != null)) {
                int parentLevel = targetDirectory.getLevel();

                if (parentLevel != sPrevLevel) {
                    sPrevLevel = parentLevel;
                    sLevelDirCnt = 0;
                    if (parentLevel >= 2) {
                        // TODO : check only samsung cache
                        Log.d(this, "doFullSync() level - " + parentLevel);
                        CachedFileBrowser.checkAllInstances();
                    }
                } else {
                    sLevelDirCnt++;
                    if (sLevelDirCnt > sMAX_LEVEL_CNT) {
                        // TODO : check only samsung cache
                        CachedFileBrowser.checkAllInstances();
                    }
                }

                DriveFile driveDir = new DriveFile();
                driveDir.fileId = targetDirectory.getCloudId();
                driveDir.type = DriveFile.FOLDER;

                Log.d(this, "doFullSync() - " + targetDirectory.getCloudId());

                Drive drive = ((SamsungDriveContext) context).getDrive(appContext);
                Drive.Files.ListChildren listChildrenRequest = drive.files().listChildren(driveDir);
                DriveFileList driveFilesList = retryRequest(listChildrenRequest, RETRY_MAX_COUNT);

                if (driveFilesList != null) {
                    for (DriveFile file : driveFilesList.getItems()) {
                        if (!isSignedIn()) {
                            Log.d(this, "doFullSync() - signout 1");
                            return false;
                        }
                        if (file.isFolder()) {
                            CloudDirectory cd = new CloudDirectory(context, targetDirectory, file.name);
                            cd.setCloudId(file.fileId);
                            cd.setParentCloudId(file.parent);
                            cd.setLevel(parentLevel + 1);

                            ((SamsungDriveContext) context).cloudDirListQToSync.add(cd);
                            context.getDatabaseHelper().findOrSaveDirectory(cd);
                        }
                    }
                    bRet = ((SamsungDriveContext) context).putDBforFullSync(targetDirectory, driveFilesList.getItems());
                }
            }
            if (!isSignedIn() || !bRet) {
                Log.d(this, "doFullSync() - signout 2 " + isSignedIn() + " or putDB fail " + bRet);
                return false;
            }
            savePreferenceLong(PreferenceType.LAST_SYNC_TIME, System.currentTimeMillis());
        } catch (Exception e) {
            Log.d(this, "doFullSync() - " + e.getMessage());
            bRet = false;
        }
        Log.d(this, "doFullSync() - End");
        return bRet;
    }

    public void deleteAllFileInfoFromDB() {
        context.getDatabaseHelper().delete(DatabaseHelper.FILE_TABLE, null, null);
    }

    @Override
    public UploadResult uploadFile(String targetDirId, File fileToUpload, String mimeType, final StreamProgressListener streamProgressListener) {
        UploadResult result = new UploadResult();
        result.mFileId = null;
        result.mStatus = UploadResult.Status.OTHER;

        long startTime = System.currentTimeMillis();
        String canonicalPath = "???";
        try {
            canonicalPath = fileToUpload.getCanonicalPath();
        } catch (IOException e) {
            Log.e(this, "uploadFile() - IOException : " + e.getMessage());
        }
        Log.i(this, "uploadFile(\"" + canonicalPath + "\") starting - targetDirID param = " + targetDirId);

        CloudContext context = getContext();
        try {
            long length = fileToUpload.length();

            long sizeLimit = getMaximumFileSize();
            if (sizeLimit > 0 && length > sizeLimit) {
                result.mStatus = UploadResult.Status.TOO_LARGE;
            } else {
                DriveFile dstFolder = new DriveFile();
                dstFolder.fileId = targetDirId;
                dstFolder.type = DriveFile.FOLDER;

                ProgressListener progressListener = new ProgressListener() {
                    long byteTransfer = 0;

                    @Override
                    public void onProgress(long transfer, long total) {
                        streamProgressListener.bytesTransferred(transfer - byteTransfer);
                        byteTransfer = transfer;
                    }
                };

                Drive drive = ((SamsungDriveContext) context).getDrive(appContext);
                Drive.Files.Upload upload = drive.files().upload(dstFolder, fileToUpload.getAbsolutePath(), progressListener, setNetworkStatusListener(OperationType.UPLOAD));
                DriveFile ret = retryRequest(upload, RETRY_MAX_COUNT);

                if (ret != null && ret.isValid()) {
                    result.mStatus = UploadResult.Status.OK;
                    result.mFileId = ret.fileId;
                    ((SamsungDriveContext) context).insertDB(DatabaseHelper.TABLE_TYPE.FILE, dstFolder, ret);
                    usedCapacity += length;
                    device.setUsedCapacityInBytes(usedCapacity);
                }
            }
        } catch (IOException e) {
            Log.e(this, "uploadFile() - IOException : " + e.getMessage());
            if (e.getMessage().contains(CloudStorageError.RETRY_MAX_ERROR)) {
                result.mStatus = UploadResult.Status.MAX_RETRY_FAIL;
            } else if (e.getMessage().contains(CloudStorageError.NO_NEED_RETRY_ERROR)) {
                result.mStatus = UploadResult.Status.NO_NEED_RETRY;
            } else if (e.getMessage().contains(CloudStorageError.OUT_OF_STORAGE_ERROR)) {
                result.mStatus = UploadResult.Status.OUT_OF_SPACE;
            } else if (e.getMessage().contains(CloudStorageError.REACH_MAX_ITEM_ERROR)) {
                result.mStatus = UploadResult.Status.REACHED_MAX_ITEMS;
            }
        } finally {
            long endTime = System.currentTimeMillis();
            Log.i(this, "uploadFile() took " + (endTime - startTime) + " millisecond(s)");
            return result;
        }
    }

    @Override
    public boolean isRangeDownloadSupported() {
        return true;
    }

    @Override
    public CloudStreamInfo getFile(ASPFile file, String contentRange) throws IOException {
        if (!(file instanceof CloudFile)) {
            throw new FileNotFoundException("not a CloudFile!");
        }
        CloudFile cloudFile = (CloudFile) file;
        Id sourceMediaId = getSourceMediaId(cloudFile);
        if (sourceMediaId == null) {
            throw new FileNotFoundException("file does not have a sourceMediaId");
        }
        CloudStreamInfo streamInfo = getFile(sourceMediaId.toString(), sourceMediaId.toString(), contentRange);
        return streamInfo;
    }

    @Override
    public CloudStreamInfo getFile(String sourceUrl, String sourceId, String contentRange) throws IOException {
        return null;
    }

    @Override
    public String downloadFile(final StreamProgressListener listener, String fileId, String targetPath, String fileName, boolean bWaitForRename) {
        String ret = null;

        File cacheDir = new File(MFLStorageManagerSLPF.getCacheDir(appContext), "filetransfer_cache");
        cacheDir.mkdirs();

        String tmpName = "DownloadTask.tmp." + UUID.randomUUID().toString();
        File tmpFile = new File(cacheDir, tmpName);

        DriveFile srcDriveFile = new DriveFile();
        srcDriveFile.fileId = fileId;
        srcDriveFile.name = fileName;
        srcDriveFile.type = DriveFile.FILE;

        try {
            Drive drive = ((SamsungDriveContext) context).getDrive(appContext);
            ProgressListener progressListener = new ProgressListener() {
                long byteTransfer = 0;

                @Override
                public void onProgress(long transfer, long total) {
                    listener.bytesTransferred(transfer - byteTransfer);
                    byteTransfer = transfer;
                }
            };

            Drive.Files.Download downloadRequest = drive.files().download(srcDriveFile, cacheDir.getAbsolutePath(), tmpName, progressListener, setNetworkStatusListener(OperationType.DOWNLOAD));
            if (retryRequest(downloadRequest, RETRY_MAX_COUNT)) {
                if (targetPath != null) {
                    File targetDirectory = new File(targetPath);
                    if (targetDirectory != null && !targetDirectory.exists()) {
                        targetDirectory.mkdirs();
                    }
                }

                File targetFile = new File(targetPath, fileName);
                targetFile = AspFileUtilsSLPF.moveFile(tmpFile, targetFile, bWaitForRename);
                if (targetFile != null && targetFile.exists()) {
                    ret = targetFile.getName();
                }
            }
        } catch (IOException e) {
            Log.e(this, "downloadFile() - IOException : " + e.getMessage());
            File downloadFile = new File(targetPath, fileName);
            if (downloadFile.exists()) {
                FileUtils.deleteQuietly(downloadFile);
            }
        } finally {
            FileUtils.deleteQuietly(tmpFile);
        }

        return ret;
    }

    // TODO : Remove check
    @Override
    public int deleteFiles(String directoryGatewayFileId, ASPFileSortType sortType, String... storageGatewayFilesToDelete)
            throws InterruptedException, IOException {
        Log.i(this, "deleteFiles(), directoryGatewayFileId = " + directoryGatewayFileId);

        int numFilesDeleted = 0;
        if (StringUtils.isNotEmpty(directoryGatewayFileId)) {
            CloudDirectory directory = context.getDatabaseHelper().getDirectoryByCloudId(context, directoryGatewayFileId);

            if (directory == null) {
                directory = getDirectoryNotInDatabase(directoryGatewayFileId);
            }

            if (directory != null) {
                directory.holdFiles();

                try {
                    for (int i = 0; i < storageGatewayFilesToDelete.length; ++i) {
                        boolean result = deleteFileFromDirectory(directory, storageGatewayFilesToDelete[i]);
                        if (!result) {
                            break;
                        }
                        numFilesDeleted++;
                    }
                } catch (Exception e) {
                    Log.e(this, "Failed to delete files : "
                            + Arrays.toString(storageGatewayFilesToDelete)
                            + " in directory:"
                            + directoryGatewayFileId);
                    Log.e(this, "deleteFiles() - Exception : " + e.getMessage());
                    if (e instanceof InterruptedException) {
                        throw (InterruptedException) e;
                    } else if (e instanceof IOException) {
                        throw (IOException) e;
                    }
                } finally {
                    directory.releaseFiles();
                }

                if (numFilesDeleted > 0) {
                    ContentResolver cr = getApplicationContext().getContentResolver();
                    cr.notifyChange(CloudGatewayMediaStore.FileBrowser.FileList.getFileListUri(getCloudDevice().getId(), directoryGatewayFileId), null);
                    cr.notifyChange(
                            CloudGatewayMediaStore.FileBrowser.DirectoryInfo.getDirectoryInfoUri(getCloudDevice().getId(), directoryGatewayFileId),
                            null);
                }
            }
        }

        return numFilesDeleted;
    }

    // TODO : Remove check
    protected boolean deleteFileFromDirectory(CloudDirectory directory, String storageGatewayId) throws Exception {
        Log.i(this, "deleteFileFromDirectory() called");
        storageGatewayId = decodeStorageGatewayFileId(storageGatewayId);

        String path = Metadata.Id.getFilePathFromSourceMediaId(storageGatewayId);
        String[] pathComponents = path.split("\\" + CloudContext.PATH_SEPARATOR);
        boolean success = false;
        if (pathComponents.length > 0) {
            String name = pathComponents[pathComponents.length - 1];
            CloudFile file = directory.getFile(name);
            success = (file != null) ? deleteFile(file) : true;
        }

        return success;
    }

    private class APIBatchRequest {
        public boolean bBatchSuccess;
        public int mBatchRetryCnt;

        public APIBatchRequest() {
            mBatchRetryCnt = 0;
        }

        public void doBatchExecute(BatchRequest batch, OperationType opType) throws IOException {
            Log.d(this, "doBatchExecute() called - request size : " + batch.size());
            try {
                if (batch.hasNext()) {
                    if (opType != null) {
                        batch.next(setNetworkStatusListener(opType));
                    } else {
                        batch.next();
                    }
                }
            } catch (SamsungCloudException e) {
                Log.e(this, "doBatchExecute() - SamsungCloudException : " + e.getMessage());
                long exceptionType = e.getType();
                if (isTokenError(exceptionType)) {
                    try {
                        Thread.sleep(1000);
                        handleTokenException();
                    } catch (InterruptedException e1) {
                        e1.printStackTrace();
                    }
                } else if (isQuotaException(exceptionType)) {
                    Log.d(this, "doBatchExecute() - quota exception : " + e.getMessage());
                    throwQuotaException(e);
                } else {
                    Log.d(this, "doBatchExecute() - no_need_retry exception : " + e.getMessage());
                    throw new IOException(CloudStorageError.NO_NEED_RETRY_ERROR + e.getMessage());
                }
            }
        }
    }

    @Override
    public boolean deleteFileBatch(final String[] sourceIds) {
        final APIBatchRequest request = new APIBatchRequest();
        final ArrayList<DriveFile> successList = new ArrayList<>();
        request.bBatchSuccess = true;

        try {
            Drive drive = ((SamsungDriveContext) context).getDrive(appContext);
            BatchRequest deleteBatch = null;

            ResponseListener<DriveFile> responseListener = new ResponseListener<DriveFile>() {
                @Override
                public void onResponse(DriveFile driveFile) throws SamsungCloudException {
                    if (driveFile.isValid()) {
                        Log.d(this, "deleteFileBatch onResponse success id = " + driveFile.fileId + "- process = " + driveFile.processing);
                        successList.add(driveFile);
                    } else {
                        Log.e(this, "deleteFileBatch onResponse fail id = " + driveFile.fileId);
                        request.bBatchSuccess = false;
                    }
                }

                @Override
                public void onError(long l, ContentValues contentValues) throws SamsungCloudException {
                    Log.e(this, "deleteFileBatch onError fail");
                    request.bBatchSuccess = false;
                }
            };

            for (int index = 0; index < sourceIds.length; index++) {
                DriveFile targetDriveFile = new DriveFile();
                targetDriveFile.fileId = sourceIds[index];

                if (deleteBatch == null) {
                    Drive.Batch.Create createRequest = drive.batch().create(Batch.DELETE_TO_TRASH_IGNORE_CONFLICT);
                    deleteBatch = retryRequest(createRequest, RETRY_MAX_COUNT);
                }

                BatchParam batchParam = new BatchParam.BatchParamBuilder().targetDriveFile(targetDriveFile).build();
                deleteBatch.add(batchParam, responseListener);

                if ((deleteBatch.size() >= API_CALL_BATCH_LIMIT) || (index == sourceIds.length - 1)) {
                    request.doBatchExecute(deleteBatch, OperationType.DELETE);
                    deleteBatch = null;
                }
            }
        } catch (SamsungCloudException e) {
            Log.d(this, "deleteFileBatch() - SamsungCloudException : " + e.getMessage());
            e.printStackTrace();
        } catch (IOException ioe) {
            Log.d(this, "deleteFileBatch() - no_need_retry exception : " + ioe.getMessage());
        }

        for (DriveFile deleteFile : successList) {
            ContentValues values = new ContentValues();
            values.put(ASPMediaStore.TrashColumns.LIST_SHOWN, deleteFile.restoreAllowed);
            values.put(ASPMediaStore.TrashColumns.TRASHED, deleteFile.trashed);
            values.put(ASPMediaStore.TrashColumns.PROCESSING, deleteFile.processing);

            ((SamsungDriveContext) getContext()).updateDB(DatabaseHelper.TABLE_TYPE.FILE, deleteFile.fileId, values);
        }
        return request.bBatchSuccess;
    }

    @Override
    public boolean deletePermanentlyBatch(final String[] sourceIds) {
        final APIBatchRequest request = new APIBatchRequest();
        final ArrayList<DriveFile> successList = new ArrayList<>();
        request.bBatchSuccess = true;

        try {
            Drive drive = ((SamsungDriveContext) context).getDrive(appContext);
            BatchRequest deleteBatch = null;

            ResponseListener<DriveFile> responseListener = new ResponseListener<DriveFile>() {
                @Override
                public void onResponse(DriveFile driveFile) throws SamsungCloudException {
                    if (driveFile.isValid()) {
                        Log.d(this, "deletePermanentlyBatch onResponse success id = " + driveFile.fileId + "- process = " + driveFile.processing);
                        successList.add(driveFile);
                    } else {
                        Log.e(this, "deletePermanentlyBatch onResponse fail id = " + driveFile.fileId);
                        request.bBatchSuccess = false;
                    }
                }

                @Override
                public void onError(long l, ContentValues contentValues) throws SamsungCloudException {
                    Log.e(this, "deletePermanentlyBatch onError fail");
                    request.bBatchSuccess = false;
                }
            };

            for (int index = 0; index < sourceIds.length; index++) {
                DriveFile targetDriveFile = new DriveFile();
                targetDriveFile.fileId = sourceIds[index];

                if (deleteBatch == null) {
                    Drive.Batch.Create createRequest = drive.batch().create(Batch.DELETE_PERMANENTLY_IGNORE_CONFLICT);
                    deleteBatch = retryRequest(createRequest, RETRY_MAX_COUNT);
                }

                BatchParam batchParam = new BatchParam.BatchParamBuilder().targetDriveFile(targetDriveFile).build();
                deleteBatch.add(batchParam, responseListener);

                if ((deleteBatch.size() >= API_CALL_BATCH_LIMIT) || (index == sourceIds.length - 1)) {
                    request.doBatchExecute(deleteBatch, OperationType.DELETE);
                    deleteBatch = null;
                }
            }
        } catch (SamsungCloudException e) {
            Log.d(this, "deletePermanentlyBatch() - SamsungCloudException : " + e.getMessage());
            e.printStackTrace();
        } catch (IOException ioe) {
            Log.d(this, "deletePermanentlyBatch() - no_need_retry exception : " + ioe.getMessage());
        }

        for (DriveFile deleteFile : successList) {
            ((SamsungDriveContext) getContext()).deleteDB(deleteFile.type, deleteFile.fileId);
        }
        return request.bBatchSuccess;
    }


    @Override
    public boolean deleteFile(String sourceUrl, String sourceId) {
        Log.i(this, "deleteFile() - sourceUrl = " + sourceUrl + ", sourceId = " + sourceId);

        String fileId = Metadata.Id.getFileIdFromSourceMediaId(sourceId);
        CloudFile file = new CloudFile(getContext(), null, null);
        file.setCloudId(fileId);
        boolean success = false;
        try {
            success = deleteFile(file);
        } catch (Exception e) {
            Log.e(this, "deleteFile() - Exception deleting file : " + e);
        }
        return success;
    }

    public boolean deleteFile(CloudFile file) throws Exception {
        long startTime = System.currentTimeMillis();
        boolean success = false;
        try {
            DriveFile targetDriveFile = new DriveFile();
            targetDriveFile.fileId = file.getCloudId();

            Drive drive = ((SamsungDriveContext) context).getDrive(appContext);
            Drive.Files.Delete deleteRequest = drive.files().delete(targetDriveFile, true);
            DriveFile ret = retryRequest(deleteRequest, RETRY_MAX_COUNT);
            success = (ret != null) && ret.isValid() && ret.trashed;
        } catch (IOException e) {
            Log.e(this, "deleteFile() - IOException : " + e.getMessage());
        } finally {
            long endTime = System.currentTimeMillis();
            Log.i(this, "deleteFile() took " + (endTime - startTime) + " millisecond(s) / return " + success);
        }

        return success;
    }

    /*
     * methods that complement methods in interface CloudStorageSync
     */
    @Override
    public CloudDevice getCloudDevice() {
        return device;
    }

    public ContentResolver getContentResolver() {
        return contentResolver;
    }

    @Override
    public Context getApplicationContext() {
        return appContext;
    }

    protected CloudDataModel getDataModel() {
        return dataModel;
    }

    /**
     * get authorization for the connector to access the cloud storage, without using the storage gateway.
     *
     * @return true iff the connector is authorized to access the cloud storage
     */
    protected abstract AuthorizationState getAuthorizationInDevice();

    /**
     * get the limit on the size of files in this cloud storage
     *
     * @return the limit on the size of files in this cloud storage, 0 if no limit
     */
    @Override
    public abstract long getMaximumFileSize();

    protected Metadata.Id getSourceMediaId(CloudFile file) {
        String metadataId = getMetadataId(file);
        if (metadataId == null) {
            throw new RuntimeException("metadataId is null!!!");
        }
        String directoryId = "";

        return new Metadata.Id(directoryId, metadataId);
    }

    protected String getMetadataId(CloudFile file) {
        return (file != null) ? file.getCloudId() : "";
    }

    @Override
    public String getStorageGatewayFileId(ASPFile file) throws FileNotFoundException {
        if (!(file instanceof CloudFile)) {
            throw new FileNotFoundException("not a CloudFile!");
        }
        CloudFile cloudFile = (CloudFile) file;
        Id sourceMediaId = getSourceMediaId(cloudFile);
        if (sourceMediaId == null) {
            throw new FileNotFoundException("file does not have a sourceMediaId");
        }
        String fileMetadataPath = sourceMediaId.toString();
        Log.i(this, "getStorageGatewayFileId(), sourceMediaId = " + sourceMediaId + ", fileMetadataPath = " + fileMetadataPath);
        MediaType mediaType = MediaType.forFile(cloudFile);
        int aspMediaType = AspMediaId.MEDIA_TYPE_NONE;
        if (mediaType != null) {
            aspMediaType = mediaType.aspMediaType;
        }
        String storageGatewayFileId = getStorageGatewayFileId(aspMediaType, fileMetadataPath, null);
        Log.i(this, "getStorageGatewayFileId(), returned storageGatewayFileId = " + storageGatewayFileId);
        return storageGatewayFileId;
    }

    @Override
    public String getStorageGatewayFileId(int aspMediaType, String sourceUrl, String sourceId) {
        String storageGatewayFileId = Metadata.Id.getFilePathFromSourceMediaId(sourceUrl);
        if (idsUseBase64()) {
            storageGatewayFileId = Base64.encodeToString(storageGatewayFileId.getBytes(), Base64.NO_WRAP | Base64.URL_SAFE);
        }
        return storageGatewayFileId;
    }

    protected String decodeStorageGatewayFileId(String fileId) {
        if (idsUseBase64()) {
            fileId = new String(Base64.decode(fileId, Base64.NO_WRAP | Base64.URL_SAFE));
        }
        return fileId;
    }

    @Override
    public CloudStreamInfo getFile(String fileId, String contentRange) throws IOException, IllegalStateException {
        return null;
    }

    /**
     * get authorization to access the cloud storage.
     *
     * @return true iff now authorized to access the cloud storage
     * @throws CloudAuthorizationException if authorization state could not be determined
     */
    protected boolean getAuthorization() {
        try {
            numberOfAuthorizeRequests += 1;
            boolean authorized = checkAuthorization();

            if (!authorized) {
                authorized = setAndCheckAuthorizationState(getAuthorizationInDevice());
            }
            broadcastAuthorizationState();

            return authorized;
        } finally {
            Log.d(this, numberOfAuthorizeRequests + " getAuthorization() request(s); " + numberOfGetTokenRequests + " getToken() request(s)");
        }
    }

    protected void broadcastAuthorizationState() {
        AuthorizationState authorizationState = getAuthorizationState();
        setIsSignedIn(authorizationState == AuthorizationState.YES);
        broadcastMessage(authorizationState.broadcast);
    }

    protected final String getName() {
        return type.name;
    }

    protected final T getContext() {
        return context;
    }

    protected void resetAccessTokenAndSecret() {
        setAccessTokenAndSecret(null, null);
    }

    /**
     * set the authorization token and secret that the connector is to use to access the cloud storage.
     * </br></br>
     * these values are very much connector dependent, and both may not necessarily be used by all connectors.
     *
     * @param token       the token that the connector is to use to access the cloud storage
     * @param tokenSecret the token secret that the connector is to use to access the cloud storage
     */
    protected void setAccessTokenAndSecret(String token, String tokenSecret) {
        context.setAndCheckAccessTokenAndSecret(token, tokenSecret);
    }

    protected AuthorizationState getAuthorizationState() {
        return context.getAuthorizationState();
    }

    public boolean setAndCheckAuthorizationState(AuthorizationState authorizationState) {
        return context.setAndCheckAuthorizationState(authorizationState);
    }

    public boolean checkAuthorization() {
        return context.checkAuthorization();
    }

    protected final void setIsSignedIn(boolean isSignedIn) {
        CloudDevice device = getCloudDevice();
        if (device != null && device.isWebStorageSignedIn() != isSignedIn) {
            device.setWebStorageSignedIn(isSignedIn);
            getDataModel().updateDevice(device);
        } else {
            Log.e(this, "setIsSignedIn() - isSignedIn : " + isSignedIn + ", device : " + device);
        }
    }

    public boolean isSignedIn() {
        Log.i(this, "isSignedIn(), return = " + getCloudDevice().isWebStorageSignedIn());
        return getCloudDevice().isWebStorageSignedIn();
    }

    public final CloudDirectory getRootDirectory() {
        return rootDirectory;
    }

    public final void requestSync(boolean syncMetadata) {
        this.syncMetadata |= syncMetadata;    // only the sync() method can rest this!!!
        if (appDelegate != null) {
            appDelegate.requestSync();
        }
    }

    @Override
    public void newToken(String token, String secret) {
        SharedPreferences preferences = getSharedPreferences();
        String accessToken = preferences.getString(ACCESS_TOKEN_ID, null);
        String accessSecret = preferences.getString(ACCESS_SECRET_ID, null);
        if (!StringUtils.equals(token, accessToken) || !StringUtils.equals(secret, accessSecret)) {
            Log.d(this, "persisting token : " + token + "; secret : " + secret);
            Editor editor = preferences.edit();
            if (accessToken != null) {
                editor.remove(ACCESS_TOKEN_ID);
            }
            if (token != null) {
                editor.putString(ACCESS_TOKEN_ID, token);
            }
            if (accessSecret != null) {
                editor.remove(ACCESS_SECRET_ID);
            }
            if (secret != null) {
                editor.putString(ACCESS_SECRET_ID, secret);
            }
            editor.apply();
        }
    }

    protected final void setMetadataUserId(String userId) {
        if (!StringUtils.equals(metadataUserId, userId)) {
            Editor editor = getSharedPreferences().edit();
            if (metadataUserId != null) {
                try {
                    device.deleteAllMetaData(contentResolver);
                } catch (Exception e) {
                    Log.e(this, "setMetadataUserId() - Exception deleting all metadata : " + e);
                }
                editor.remove(METADATA_USER_ID);
            }
            metadataUserId = userId;
            if (metadataUserId != null) {
                editor.putString(METADATA_USER_ID, metadataUserId);
            }
            editor.apply();
        }
    }

    //jsub12.lee_150721
    protected final ArrayList<Metadata> getMetadataOfChildren(MediaType mediaType, String parentCloudId) {
        Log.i(this, "getMetadataOfChildren(), mediaType = " + mediaType + ", parentCloudId = " + parentCloudId);

        ArrayList<Metadata> metaList = new ArrayList<>();
        String[] columnNames = mediaType.columnNames;
        if (columnNames != null) {
            String selection = ASPMediaStore.BaseASPColumns.PARENT_CLOUD_ID + "=? and trashed=0";
            String[] selectionArgs = new String[]{parentCloudId};

            if (parentCloudId.equals(CloudGatewayMediaStore.Trash.Trash_ID)) {
                selection = ASPMediaStore.TrashColumns.LIST_SHOWN + "=1";
                selectionArgs = null;
            }

            Cursor cursor = context.getDatabaseHelper().query(
                    DatabaseHelper.FILE_TABLE,
                    columnNames,
                    selection,
                    selectionArgs,
                    null);

            if (cursor != null) {
                Metadata metadata;
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    metadata = metadataFromCursor(cursor, Metadata.State.UNSEEN);
                    metaList.add(metadata);
                    cursor.moveToNext();
                }
                cursor.close();
            }
        } else {
            Log.i(this, "getMetadataOfChildren(), columnNames is null");
        }

        return metaList;
    }

    protected final void broadcastMessage(String intentAction) {
        Log.d(this, "broadcasting message : " + intentAction);
        if (appDelegate != null) {
            Intent intent = new Intent(intentAction);
            if (device != null) {
                intent.putExtra(CloudDevice.DEVICE_ID_EXTRA_KEY, device.getId());
            }
            appDelegate.sendBroadcastMessage(intent);
        } else {
            Log.d(this, "no appDelegate!");
        }
    }

    private SharedPreferences getSharedPreferences() {
        // int id = device.getId();
        String instanceName = type.name/* + "-" + Integer.toString(id) */;
        SharedPreferences preferences = appContext.getSharedPreferences(instanceName, Context.MODE_PRIVATE);
        return preferences;
    }

    private boolean isEmptyDirectory(String dirId) throws IOException {
        Drive drive = ((SamsungDriveContext) context).getDrive(appContext);
        DriveFile file = new DriveFile();
        file.type = DriveFile.FOLDER;
        file.fileId = dirId;

        Drive.Files.ListChildrenWithoutPaging list = drive.files().listChildrenWithoutPaging(file);
        DriveFileList fileList = retryRequest(list, RETRY_MAX_COUNT);
        return (fileList.getItems().size() == 0);  /// TODO : current page could be 0 during paging
    }

    public void deleteChildrenFromDB(String parentDirId) {
        String where = ASPMediaStore.BaseASPColumns.PARENT_CLOUD_ID + "=? and trashed=0";
        String[] selectionArgs = {parentDirId};

        context.getDatabaseHelper().delete(DatabaseHelper.FILE_TABLE, where, selectionArgs);
    }

    private Metadata metadataFromCursor(Cursor cursor, Metadata.State state) {
        String id = getCursorString(cursor, ASPMediaStore.BaseASPColumns.SOURCE_MEDIA_ID);
        if (id == null) {
            throw new RuntimeException("sourceMediaId is null");
        }
        long length = getCursorLong(cursor, ASPMediaStore.Audio.AudioColumns.SIZE);
        String parentCloudId = getCursorString(cursor, ASPMediaStore.BaseASPColumns.PARENT_CLOUD_ID); //jsub12.lee_150715_2
        String parentName = getCursorString(cursor, ASPMediaStore.Images.ImageColumns.BUCKET_DISPLAY_NAME); //jsub12_150924_1
        long dateModified = getCursorLong(cursor, ASPMediaStore.Audio.AudioColumns.DATE_MODIFIED);
        String displayName = getCursorString(cursor, ASPMediaStore.Audio.AudioColumns.DISPLAY_NAME);
        String mimeType = getCursorString(cursor, ASPMediaStore.Audio.AudioColumns.MIME_TYPE);
        String thumbnailURI = getCursorString(cursor, ASPMediaStore.Audio.AudioColumns.THUMBNAIL_URI);
        String fullPath = getCursorString(cursor, ASPMediaStore.Audio.AudioColumns.DATA);  //jsublee_150907
        String trashProcessing = getCursorString(cursor, ASPMediaStore.TrashColumns.PROCESSING);

        /* @formatter:off */
        Metadata fileMetadata = new Metadata(
                id,
                state,
                length,
                parentCloudId, //jsub12.lee_150715_2
                parentName,
                dateModified,
                displayName,
                mimeType,
                thumbnailURI,
                fullPath,
                trashProcessing);
        /* @formatter:on */
        return fileMetadata;
    }

    private long getCursorLong(Cursor cursor, String name) {
        long result = 0;
        int columnIndex = cursor.getColumnIndex(name);
        if (columnIndex >= 0) {
            result = cursor.getLong(columnIndex);
        }
        return result;
    }

    private String getCursorString(Cursor cursor, String name) {
        String result = null;
        int columnIndex = cursor.getColumnIndex(name);
        if (columnIndex >= 0) {
            result = cursor.getString(columnIndex);
        }
        return result;
    }

    public Integer getDeviceId() {
        Integer deviceId = null;
        CloudDevice cloudDevice = getCloudDevice();
        if (cloudDevice != null) {
            deviceId = cloudDevice.getId();
        }
        return deviceId;
    }

    public void setFileStorageGatewayId(CloudFile result, String asString) {
        // TODO Auto-generated method stub
        result.setCloudId(asString);
    }

    public void setLargestChangeID(String _changeId) {
        mCurChangeId = _changeId;
        saveCurChangeIdToPref(mCurChangeId);
    }

    private void saveCurChangeIdToPref(String _largestChangeID) {
        Log.i(this, "saveCurChangeIdToPref() called");

        Editor editor = getSharedPreferences().edit();
        editor.remove(LARGEST_CHANGE_ID);
        editor.putString(LARGEST_CHANGE_ID, _largestChangeID);
        editor.apply();
    }

    private String getCurChangeIdFromPref() {
        Log.i(this, "getCurChangeIdFromPref() called");
        SharedPreferences preferences = getSharedPreferences();

        return preferences.getString(LARGEST_CHANGE_ID, null);
    }

    private void savePreferenceLong(PreferenceType type, long value) {
        Log.i(this, "savePreferenceLong() " + type.name() + " " + value);

        Editor editor = getSharedPreferences().edit();
        editor.remove(type.name());
        editor.putLong(type.name(), value);
        editor.apply();
    }

    private long getPreferenceLong(PreferenceType type) {
        SharedPreferences preferences = getSharedPreferences();
        Log.i(this, "getPreferenceLong() " + type.name() + " " + preferences.getLong(type.name(), 0));
        return preferences.getLong(type.name(), 0);
    }

    private void savePreferenceString(PreferenceType type, String value) {
        Log.i(this, "savePreferenceString() " + type.name() + " " + value);

        Editor editor = getSharedPreferences().edit();
        editor.remove(type.name());
        editor.putString(type.name(), value);
        editor.apply();
    }

    private String getPreferenceString(PreferenceType type) {
        SharedPreferences preferences = getSharedPreferences();
        Log.i(this, "getPreferenceLong() " + type.name() + " " + preferences.getString(type.name(), null));
        return preferences.getString(type.name(), null);
    }

    @Override
    public long getLastSync() {
        return getPreferenceLong(PreferenceType.LAST_SYNC_TIME);
    }

    @Override
    public long[] getDetailedUsed() {
        return null;
    }

    @Override
    public long[] updateQuota() {
        long[] bRet = null;
        if (device != null) {
            QuotaInfo info = getQuota();
            long quota = (info != null) ? info.totalSize.longValue() : 0;
            long used = (info != null) ? info.currentUsage.longValue() : 0;

            Log.d(this, "Quota info - Total : " + quota + ", Usage : " + used);
            if ((quota != 0) && ((device.getCapacityInBytes() != quota) || (device.getUsedCapacityInBytes() != used))) {
                Log.i(this, "updateQuota(), capacity and / or used changed, will sync metadata");
                device.setCapacityInBytes(quota);
                device.setUsedCapacityInBytes(used);
                dataModel.updateDevice(device);
            }
            bRet = new long[2];
            bRet[0] = quota;
            bRet[1] = used;
        } else {
            Log.e(this, "updateQuota(), device is null");
        }

        requestSync(true);
        return bRet;
    }

    @Override
    public boolean moveFile(String curDirId, String sourceId, String targetDirId) {
        long startTime = System.currentTimeMillis();
        Log.i(this, "moveFile(), curDirId : " + curDirId + ", sourceId : " + sourceId + ", targetDirId : " + targetDirId);
        boolean success = false;

        try {
            Drive drive = ((SamsungDriveContext) context).getDrive(appContext);
            DriveFile srcDFile = new DriveFile();
            srcDFile.fileId = sourceId;
            DriveFile dstDFile = new DriveFile();
            dstDFile.fileId = targetDirId;
            dstDFile.type = DriveFile.FOLDER;

            Drive.Files.Move moveRequest = drive.files().move(srcDFile, dstDFile);
            DriveFile ret = retryRequest(moveRequest, RETRY_MAX_COUNT);
            if (ret != null && ret.isValid()) {
                success = true;
            }
        } catch (IOException e) {
            Log.e(this, "moveFile() - IOException : " + e.getMessage());
        } finally {
            long endTime = System.currentTimeMillis();
            Log.i(this, "moveFile() took " + (endTime - startTime) + " millisecond(s) / return " + success);
        }

        return success;
    }

    @Override
    public Result moveFileBatch(ArrayList<String[]> moveList, final StreamProgressListener listener) throws IOException {
        long startTime = System.currentTimeMillis();
        Log.d(this, "moveFileBatch() called - sourceIds.size() : " + moveList.size());
        final ArrayList<DriveFile> successList = new ArrayList<DriveFile>();
        final Result result = new Result(true);
        final APIBatchRequest request = new APIBatchRequest();
        request.bBatchSuccess = true;
        try {
            Drive drive = ((SamsungDriveContext) context).getDrive(appContext);
            BatchRequest moveBatch = null;

            ResponseListener<DriveFile> responseListener = new ResponseListener<DriveFile>() {
                @Override
                public void onResponse(DriveFile driveFile) throws SamsungCloudException {
                    Log.d(this, "moveFileBatch()/responseListener - onResponse : fileId = " + driveFile.fileId + ", name : " + driveFile.name);
                    if (driveFile.isValid()) {
                        successList.add(driveFile);
                        listener.bytesTransferred(1);
                    } else {
                        request.bBatchSuccess = false;
                    }
                }

                @Override
                public void onError(long l, ContentValues contentValues) throws SamsungCloudException {
                    Log.d(this, "moveFileBatch()/responseListener - onError");
                    request.bBatchSuccess = false;
                }
            };

            for (String[] file : moveList) {
                String srcId = file[0];
                String fileName = file[1];
                String dstId = file[2];


                DriveFile srcDFile = new DriveFile();
                srcDFile.fileId = srcId;
                DriveFile dstDFile = new DriveFile();
                dstDFile.fileId = dstId;
                dstDFile.type = DriveFile.FOLDER;

                if (moveBatch == null) {
                    Drive.Batch.Create createRequest = drive.batch().create(Batch.MOVE_IGNORE_CONFLICT);
                    moveBatch = retryRequest(createRequest, RETRY_MAX_COUNT);
                }

                BatchParam batchParam = new BatchParam.BatchParamBuilder().sourceDriveFile(srcDFile).destinationDriveFile(dstDFile).fileName(fileName).build();
                moveBatch.add(batchParam, responseListener);

                if ((moveBatch.size() >= API_CALL_BATCH_LIMIT) || (moveList.size() == moveList.indexOf(file) + 1)) {
                    request.doBatchExecute(moveBatch, OperationType.MOVE);
                    moveBatch = null;
                }
            }
        } catch (SamsungCloudException e) {
            Log.e(this, "moveFileBatch() - SamsungCloudException : " + e.getMessage());
            e.printStackTrace();
        } catch (IOException ioe) {
            Log.e(this, "moveFileBatch() - IOException : " + ioe.getMessage());
            throw ioe;
        } finally {
            long endTime = System.currentTimeMillis();
            Log.i(this, "moveFileBatch() took " + (endTime - startTime) + " millisecond(s) / return " + request.bBatchSuccess);
        }

        if (successList != null && !successList.isEmpty()) {
            for (DriveFile driveFile : successList) {
                result.mFileIdList.add(driveFile.fileId);
                result.mFileNameList.add(driveFile.name);
                result.mFileDstIdList.add(driveFile.parent);
            }

            ((SamsungDriveContext) context).putDBforDeltaSync(successList);
        }
        result.mRet = request.bBatchSuccess;

        Log.d(this, "moveFileBatch() - return : " + request.bBatchSuccess);
        return result;
    }

    @Override
    public String createDirectory(String parentId, String directoryName) throws IOException {
        DriveFile dstFolder = new DriveFile();
        dstFolder.fileId = parentId;
        dstFolder.type = DriveFile.FOLDER;

        DriveFile createdFolder = null;
        try {
            Drive.Files.CreateFolder createFolderRequest = ((SamsungDriveContext) context).getDrive(appContext).files().createFolder(directoryName, dstFolder);
            createdFolder = retryRequest(createFolderRequest, RETRY_MAX_COUNT);
        } catch (IOException e) {
            Log.d(this, "createDirectory : " + e.getMessage());
            throw e;
        }
        String ret = null;
        if (createdFolder != null && createdFolder.isValid()) {
            ret = createdFolder.fileId;
            ((SamsungDriveContext) context).insertDB(DatabaseHelper.TABLE_TYPE.BOTH, dstFolder, createdFolder);
        }
        return ret;
    }

    @Override
    public boolean renameFile(String directoryId, String sourceId, String newName) {
        long startTime = System.currentTimeMillis();
        Log.i(this, "renameFile(), directoryId = " + directoryId + ", sourceId = " + sourceId + ", newName = " + newName);

        boolean success = false;
        try {
            DriveFile targetDriveFile = new DriveFile();
            targetDriveFile.fileId = sourceId;

            Drive drive = ((SamsungDriveContext) context).getDrive(appContext);
            Drive.Files.Rename renameRequest = drive.files().rename(targetDriveFile, newName, true);
            DriveFile ret = retryRequest(renameRequest, RETRY_MAX_COUNT);
            if ((ret != null) && ret.isValid()) {
                ArrayList<DriveFile> changes = new ArrayList<>();
                changes.add(ret);
                ((SamsungDriveContext) getContext()).putDBforDeltaSync(changes);
                if (TextUtils.equals(ret.type, DriveFile.FOLDER)) {
                    ((SamsungDriveContext) getContext()).updatePathRenamed(ret);
                }

                success = true;
            }
        } catch (IOException e) {
            Log.e(this, "renameFile() - IOException : " + e.getMessage());
        } finally {
            long endTime = System.currentTimeMillis();
            Log.i(this, "renameFile() took " + (endTime - startTime) + " millisecond(s) / return " + success);
        }

        return success;
    }

    @Override
    public boolean copyFile(String parentDirectoryId, String sourceId, String targetDirId, String newName) {
        long startTime = System.currentTimeMillis();
        Log.i(this, "copyFile(), parentDirectoryId = " + parentDirectoryId + ", sourceId = " + sourceId + ", targetDirId = " + targetDirId + ", newName = " + newName);

        boolean success = false;
        Cursor cursor = null;
        try {
            Drive drive = ((SamsungDriveContext) context).getDrive(appContext);

            DriveFile srcDFile;
            cursor = context.getDatabaseHelper().query(DatabaseHelper.FILE_TABLE,
                    new String[]{ASPMediaStore.MediaColumns.SOURCE_MEDIA_ID, DatabaseHelper.HASH}, ASPMediaStore.MediaColumns.SOURCE_MEDIA_ID + "=?", new String[]{sourceId}, null);

            if (cursor != null && cursor.moveToFirst()) {
                Log.i(this, "copyFile() - make srcDFile from DB information");
                srcDFile = new DriveFile();
                srcDFile.fileId = sourceId;
                srcDFile.type = DriveFile.FILE;
                srcDFile.hash = cursor.getString(cursor.getColumnIndex(DatabaseHelper.HASH));
            } else {
                Log.i(this, "copyFile() - make srcDFile from Samsung Drive server information");
                srcDFile = getDriveFile(drive, sourceId);
            }

            DriveFile dstDFile = new DriveFile();
            dstDFile.fileId = targetDirId;
            dstDFile.type = DriveFile.FOLDER;

            Drive.Files.CreateFile copyRequest = drive.files().createFile(newName, srcDFile, dstDFile);
            DriveFile ret = retryRequest(copyRequest, RETRY_MAX_COUNT);
            if (ret != null && ret.isValid()) {
                success = true;
            }
        } catch (IOException e) {
            Log.e(this, "copyFile() - IOException : " + e.getMessage());
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
            long endTime = System.currentTimeMillis();
            Log.i(this, "copyFile() took " + (endTime - startTime) + " millisecond(s) / return " + success);
        }

        return success;
    }

    @Override
    public Result copyFileBatch(ArrayList<String[]> copyList, final StreamProgressListener listener) throws IOException {
        Log.d(this, "copyFileBatch() called copyList.size() : " + copyList.size());
        final ArrayList<DriveFile> successList = new ArrayList<>();
        final Result result = new Result(true);
        final APIBatchRequest request = new APIBatchRequest();
        request.bBatchSuccess = true;
        Cursor cursor = null;
        try {
            Drive drive = ((SamsungDriveContext) context).getDrive(appContext);
            BatchRequest copyBatch = null;

            ResponseListener<DriveFile> responseListener = new ResponseListener<DriveFile>() {
                @Override
                public void onResponse(DriveFile driveFile) throws SamsungCloudException {
                    Log.d(this, "copyFileBatch() - onResponse : fileId = " + driveFile.fileId + ", name : " + driveFile.name);
                    if (driveFile.isValid()) {
                        successList.add(driveFile);
                        listener.bytesTransferred(1);
                    } else {
                        request.bBatchSuccess = false;
                    }
                }

                @Override
                public void onError(long l, ContentValues contentValues) throws SamsungCloudException {
                    Log.d(this, "copyFileBatch() - onError : " + l + " ," + contentValues);
                    request.bBatchSuccess = false;
                    throwQuotaException(l);
                }
            };

            for (String[] file : copyList) {
                String srcId = file[0];
                String fileName = file[1];
                String dstId = file[2];

                DriveFile srcDFile;
                DriveFile dstDFile = new DriveFile();
                dstDFile.fileId = dstId;
                dstDFile.type = DriveFile.FOLDER;

                cursor = context.getDatabaseHelper().query(DatabaseHelper.FILE_TABLE,
                        new String[]{ASPMediaStore.MediaColumns.SOURCE_MEDIA_ID, DatabaseHelper.HASH}, ASPMediaStore.MediaColumns.SOURCE_MEDIA_ID + "=?", new String[]{srcId}, null);

                if (cursor != null && cursor.moveToFirst()) {
                    Log.i(this, "copyFileBatch() - make srcDFile from DB information");
                    srcDFile = new DriveFile();
                    srcDFile.fileId = srcId;
                    srcDFile.type = DriveFile.FILE;
                    srcDFile.hash = cursor.getString(cursor.getColumnIndex(DatabaseHelper.HASH));
                } else {
                    Log.i(this, "copyFileBatch() - make srcDFile from Samsung Drive server information");
                    srcDFile = getDriveFile(drive, srcId);
                }

                Log.d(this, "copyFileBatch() - srcDFile : " + srcDFile.fileId + ", dstFile : " + dstDFile.fileId);
                if (copyBatch == null) {
                    Drive.Batch.Create createRequest = drive.batch().create(Batch.CREATE_FILE);
                    copyBatch = retryRequest(createRequest, RETRY_MAX_COUNT);
                }

                BatchParam batchParam = new BatchParam.BatchParamBuilder().sourceDriveFile(srcDFile).destinationDriveFile(dstDFile).fileName(fileName).build();
                copyBatch.add(batchParam, responseListener);

                if ((copyBatch.size() >= API_CALL_BATCH_LIMIT) || (copyList.size() == copyList.indexOf(file) + 1)) {
                    request.doBatchExecute(copyBatch, OperationType.COPY);
                    copyBatch = null;
                }
            }
        } catch (SamsungCloudException e) {
            Log.e(this, "copyFileBatch() - SamsungCloudException : " + e.getMessage());
            throwQuotaException(e);
        } catch (IOException ioe) {
            Log.e(this, "copyFileBatch() - IOException : " + ioe.getMessage());
            throw ioe;
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        if (successList != null && !successList.isEmpty()) {
            for (DriveFile driveFile : successList) {
                result.mFileIdList.add(driveFile.fileId);
                result.mFileNameList.add(driveFile.name);
                result.mFileDstIdList.add(driveFile.parent);
            }

            ((SamsungDriveContext) context).putDBforDeltaSync(successList);
        }
        result.mRet = request.bBatchSuccess;

        Log.d(this, "copyFileBatch() - return : " + request.bBatchSuccess);
        return result;
    }

    public ArrayList<CloudFile> getChildren(String targetDirId, boolean bNeedDetailInfo, Result result) {
        Log.i(this, "getChildren(), targetDirId = " + targetDirId);
        ArrayList<CloudFile> files = new ArrayList<>();
        ArrayList<Metadata> childrenMeta = getMetadataOfChildren(MediaType.directory, targetDirId);

        Log.i(this, "getChildren(), count of child meta = " + childrenMeta.size());
        for (Metadata childMeta : childrenMeta) {
            if (mCancellationSignal != null && mCancellationSignal.isCanceled() && mCancellationFileId.equals(targetDirId)) {
                Log.i(this, "getChildren(), cancelled " + targetDirId); // to prevent memory leak issue
                result.mRet = false;
                break;
            }
            files.add(getFileFromFileMeta(childMeta, bNeedDetailInfo));
        }

        return files;
    }

    //jsub12.lee_150721
    private CloudFile getFileFromFileMeta(Metadata fileMeta, boolean bNeedDetailInfo) {
        CloudFile file;
        Log.d(this, "getFileFromFileMeta(), bNeedDetailInfo = " + bNeedDetailInfo
                + ", --------- Added -----------------------------------------------");

        boolean bIsDir = (fileMeta.mimeType != null) && fileMeta.mimeType.equals(MimeType.DIR_MIMETYPE);

        if (bIsDir) {
            file = new CloudDirectory(this.context, null, fileMeta.displayName);
        } else {
            file = new CloudFile(this.context, null, fileMeta.displayName);
        }

        file.setCloudId(Metadata.Id.getFileIdFromSourceMediaId(fileMeta.id.toString()));
        file.setMimeType(fileMeta.mimeType);
        Log.i(this, "getFileFromFileMeta(), #cloudId = " + file.getCloudId()
                + ", #dateTaken = " + file.getCreated() + ", #mimeType = " + file.getMimeType());

        if (bNeedDetailInfo && bIsDir) {
            //jsub12_151006
            ArrayList<Metadata> childrenMetadata = getMetadataOfChildren(MediaType.directory, file.getCloudId());
            file.setChildCount(childrenMetadata.size());
            file.setChildDirCount(getCountOfDirFromMetadataList(childrenMetadata));
            //file.setLength(getTotalSizeOfChildFile(childrenMetadata));

            ArrayList<Metadata> metadataOfDescendants = getMetadataOfDescendants(file.getCloudId());
            file.setDescendantsCount(metadataOfDescendants.size());
            file.setDescendantDirCount(getCountOfDirFromMetadataList(metadataOfDescendants));
            file.setLength(getTotalSizeOfDescendants(metadataOfDescendants));
            file.setTrashProcessing(fileMeta.trashProcessing);

            Log.i(this, "getFileFromFileMeta(), #DIR child count = "
                    + "# DIR child count = " + file.getChildCount()
                    + "# DIR child dir count = " + file.getChildDirCount()
                    + "# DIR descendants count = " + file.getDescendantsCount()
                    + "# DIR descendant dir count = " + file.getDescendantDirCount()
                    + "# DIR length = " + file.length()
                    + "# DIR TrashProcessing = " + file.getTrashProcessing());
        } else {
            file.setLength(fileMeta.length);
            file.setTrashProcessing(fileMeta.trashProcessing);
            Log.i(this, "getFileFromFileMeta(), # length = " + file.length());
        }

        file.setName(fileMeta.displayName);
        file.setLastModified(fileMeta.dateModified);
        Log.i(this, "getFileFromFileMeta(), # name = " + file.getName() + ", # dateModified = " + fileMeta.dateModified);

        return file;
    }

    //jsub12_151006
    private int getCountOfDirFromMetadataList(ArrayList<Metadata> metadataList) {
        int retVal = 0;
        for (Metadata metadata : metadataList) {
            if (metadata.mimeType.equals(MimeType.DIR_MIMETYPE)) {
                retVal++;
            }
        }

        return retVal;
    }

    //jsub12_151006
    private long getTotalSizeOfDescendants(ArrayList<Metadata> metadataList) {
        long retVal = 0;
        for (Metadata metadata : metadataList) {
            if (!metadata.mimeType.equals(MimeType.DIR_MIMETYPE)) {
                retVal = retVal + metadata.length;
            }
        }

        return retVal;
    }

    private ArrayList<Metadata> getMetadataOfDescendants(String ancestorCloudId) {
        Log.i(this, "getMetadataOfDescendants(), ancestorCloudId = " + ancestorCloudId);

        ArrayList<Metadata> metadataOfDescendants = new ArrayList<Metadata>();
        ArrayList<Metadata> metadataOfChildren = getMetadataOfChildren(MediaType.none, ancestorCloudId);

        for (Metadata metadata : metadataOfChildren) {
            if (metadata.mimeType.equals(MimeType.DIR_MIMETYPE)) {
                metadataOfDescendants.addAll(getMetadataOfDescendants(metadata.id.toString()));
            }
        }

        metadataOfDescendants.addAll(metadataOfChildren);

        return metadataOfDescendants;
    }

    private DriveFile getDriveFile(Drive drive, String fileId) {
        Log.i(this, "getDriveFile() - drive : " + drive + ", fileId : " + fileId);
        DriveFile ret = null;
        try {
            DriveFile targetDriveFile = new DriveFile();
            targetDriveFile.fileId = fileId;
            Drive.Files.GetMeta getMetaRequest = drive.files().getMeta(targetDriveFile);
            ret = retryRequest(getMetaRequest, RETRY_MAX_COUNT);
        } catch (IOException e) {
            Log.e(this, "getDriveFile() - IOException : " + e.getMessage());
            e.printStackTrace();
        }

        return ret;
    }

    public <T> T retryRequest(DriveRequest<T> request, int retryTimes) throws IOException {
        int retryCnt = 0;

        do {
            try {
                Log.d(this, "retryRequest : " + request.getClass().getName() + " " + request + "---retry = " + retryCnt);
                return request.execute();
            } catch (SamsungCloudException e) {
                Log.d(this, "retryRequest() - SamsungCloudException : " + e.getMessage());
                long type = e.getType();

                if (isTokenError(type)) {
                    handleTokenException();
                    ++retryCnt;
                } else if (isQuotaException(type)) {
                    Log.e(this, "retryRequest() - quota exception : " + e.getMessage() + "-type = " + type);
                    throwQuotaException(e);
                } else {
                    Log.e(this, "retryRequest() - no_need_retry exception : " + e.getMessage());
                    e.printStackTrace();
                    if (SamsungCloudException.Code.NETWORK_IO_ERROR == type) {
                        Toast.makeText(appContext, R.string.cloud_not_conncet_samsung_drive, Toast.LENGTH_LONG).show();
                    }
                    throw new IOException(CloudStorageError.NO_NEED_RETRY_ERROR + e.getMessage());
                }
            }
        } while (retryCnt > 0 && retryCnt <= retryTimes);
        throw new IOException(CloudStorageError.RETRY_MAX_ERROR);
    }

    public void handleTokenException() {
        int tokenWaiting = 0;
        Log.d(this, "handleTokenException() " + DriveConstants.sIsTokenExpired);
        synchronized (this) {
            if (!DriveConstants.sIsTokenExpired) {
                DriveConstants.sIsTokenExpired = true;
                getNewToken(appContext);
            }
        }
        while (DriveConstants.sIsTokenExpired && (tokenWaiting < TOKEN_MAX_WAITING_COUNT)) {
            try {
                ++tokenWaiting;
                Log.d(this, "handleTokenException() waiting : " + tokenWaiting);
                Thread.sleep(1000);
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }
        }
    }

    private void getNewToken(final Context context) {
        final SamsungAccountImp samsungAccountImp = new SamsungAccountImp(DriveConstants.APPID, DriveConstants.APP_SECRET);
        Log.d(this, "getNewToken() - tokenExpired : " + DriveConstants.sIsTokenExpired + " accessToken : " + DriveConstants.apiClient.accessToken);
        samsungAccountImp.request(context, DriveConstants.sIsTokenExpired || DriveConstants.apiClient.accessToken == null || DriveConstants.apiClient.accessToken.equals(""), DriveConstants.apiClient.accessToken, new SamsungAccountImp.IResultListener() {
            @Override
            public void onResult(Bundle result) {
                if (result.getInt(DriveConstants.RCODE) == DriveConstants.ResultCode.SUCCESS) {
                    DriveConstants.setConstants(result, false);

                    Log.d(this, "getNewToken() - onResult : " + DriveConstants.apiClient.pushAppId + " " + DriveConstants.apiClient.pushToken + " " + DriveConstants.apiClient.pushName);
                    ((SamsungDriveContext) CloudStorageBase.this.context).getDrive(appContext).setApiClient(DriveConstants.apiClient);
                } else if (result.getInt(DriveConstants.RCODE) == DriveConstants.ResultCode.FAIL && SamsungAccountImp.ErrorCode.AUTH_TOKEN_EXPIRED.equals(result.getString(SamsungAccountImp.ERROR_CODE))) {
                    CloudAuthorizationException.showAuthActivity(context, device);
                } else {
                    Log.d(this, "getSamsungAccount()/onReceive() - onResult : " + result.getInt(DriveConstants.RCODE));
                }
            }
        });
    }

    @Override
    public boolean emptyTrash() {
        Drive drive = ((SamsungDriveContext) context).getDrive(appContext);
        Drive.Trash.Empty emptyRequest = drive.trash().empty();
        boolean bResult = false;
        try {
            bResult = retryRequest(emptyRequest, RETRY_MAX_COUNT);
        } catch (IOException e) {
            Log.e(this, "emptyTrash : " + e.getMessage());
        }
        return bResult;
    }

    @Override
    public boolean deleteTrash(String sourceId) {
        boolean success = false;
        try {
            Drive drive = ((SamsungDriveContext) context).getDrive(appContext);
            DriveFile targetDriveFile = new DriveFile();
            targetDriveFile.fileId = sourceId;
            Drive.Trash.Delete deleteRequest = drive.trash().delete(targetDriveFile, true);
            DriveFile ret = retryRequest(deleteRequest, RETRY_MAX_COUNT);
            if (ret != null && ret.isValid()) {
                success = true;
            }
        } catch (IOException e) {
            Log.e(this, "deleteTrash : " + e.getMessage());
        }
        return success;
    }

    @Override
    public boolean deleteTrashBatch(String[] sourceIds) {
        final APIBatchRequest request = new APIBatchRequest();
        final ArrayList<DriveFile> successList = new ArrayList<>();
        request.bBatchSuccess = true;

        try {
            final Drive drive = ((SamsungDriveContext) context).getDrive(appContext);
            BatchRequest deleteTrashBatch = null;
            ResponseListener<DriveFile> responseListener = new ResponseListener<DriveFile>() {
                @Override
                public void onResponse(DriveFile driveFile) throws SamsungCloudException {
                    if (driveFile.isValid()) {
                        Log.d(this, "deleteTrashBatch success id = " + driveFile.fileId);
                        successList.add(driveFile);
                    } else {
                        request.bBatchSuccess = false;
                    }
                }

                @Override
                public void onError(long errType, ContentValues contentValues) throws SamsungCloudException {
                    Log.e(this, "deleteTrashBatch() onError errorType = " + errType);
                    if (!ignoreDeleteTrashError(errType)) {
                        request.bBatchSuccess = false;
                    }
                }
            };

            for (int index = 0; index < sourceIds.length; index++) {
                DriveFile targetDriveFile = new DriveFile();
                targetDriveFile.fileId = sourceIds[index];

                if (deleteTrashBatch == null) {
                    Drive.Batch.Create createRequest = drive.batch().create(Batch.DELETE_FROM_TRASH_IGNORE_CONFLICT);
                    deleteTrashBatch = retryRequest(createRequest, RETRY_MAX_COUNT);
                }

                BatchParam batchParam = new BatchParam.BatchParamBuilder().targetDriveFile(targetDriveFile).build();
                deleteTrashBatch.add(batchParam, responseListener);

                if ((deleteTrashBatch.size() >= API_CALL_BATCH_LIMIT) || (index == sourceIds.length - 1)) {
                    request.doBatchExecute(deleteTrashBatch, OperationType.DELETE);
                    deleteTrashBatch = null;
                }
            }
        } catch (SamsungCloudException e) {
            Log.d(this, "deleteTrashBatch() - SamsungCloudException : " + e.getMessage());
            e.printStackTrace();
        } catch (IOException ioe) {
            Log.d(this, "deleteTrashBatch() - no_need_retry exception : " + ioe.getMessage());
        } finally {
            int deviceId = 0;
            CloudDevice device = getCloudDevice();
            if (device != null) {
                deviceId = device.getId();
            }
            for (DriveFile trashFile : successList) {
                if (deviceId > 0) {
                    CachedFileBrowser.deltaFileLayerRemoveTrash(deviceId, trashFile.fileId, true);
                }
                ((SamsungDriveContext) getContext()).deleteDB(trashFile.type, trashFile.fileId);
            }
        }

        return request.bBatchSuccess;
    }

    @Override
    public String restoreTrash(String sourceId) throws IOException {
        String retParentId = null;
        try {
            Drive drive = ((SamsungDriveContext) context).getDrive(appContext);
            DriveFile targetDriveFile = new DriveFile();
            targetDriveFile.fileId = sourceId;
            Drive.Trash.Restore restoreRequest = drive.trash().restore(targetDriveFile, true);
            DriveFile ret = retryRequest(restoreRequest, RETRY_MAX_COUNT);
            if (ret != null && ret.isValid()) {
                retParentId = ret.parent;

                ContentValues values = new ContentValues();
                values.put(ASPMediaStore.TrashColumns.LIST_SHOWN, ret.restoreAllowed);
                values.put(ASPMediaStore.TrashColumns.TRASHED, ret.trashed);
                values.put(ASPMediaStore.TrashColumns.PROCESSING, ret.processing);
                values.put(ASPMediaStore.Files.FileColumns.PARENT_CLOUD_ID, retParentId);

                ((SamsungDriveContext) getContext()).updateDB(DatabaseHelper.TABLE_TYPE.FILE, ret.fileId, values);
            }
        } catch (IOException e) {
            Log.e(this, "restoreTrash : " + e.getMessage());
            throw e;
        }
        return retParentId;
    }

    @Override
    public ArrayList<String> restoreTrashBatch(String[] sourceIds) throws IOException {
        ArrayList<String> parentIdSuccess = new ArrayList<>();
        final APIBatchRequest request = new APIBatchRequest();
        final ArrayList<DriveFile> successList = new ArrayList<>();
        request.bBatchSuccess = true;

        try {
            final Drive drive = ((SamsungDriveContext) context).getDrive(appContext);
            BatchRequest restoreBatch = null;
            ResponseListener<DriveFile> responseListener = new ResponseListener<DriveFile>() {
                @Override
                public void onResponse(DriveFile driveFile) throws SamsungCloudException {
                    if (driveFile != null && driveFile.isValid()) {
                        Log.d(this, "restoreTrashBatch success id = " + driveFile.fileId + "--parent = " + driveFile.parent);
                        successList.add(driveFile);
                    } else {
                        Log.d(this, "restoreTrashBatch not success id = " + ((driveFile != null) ? driveFile.fileId : null));
                        request.bBatchSuccess = false;
                    }
                }

                @Override
                public void onError(long l, ContentValues contentValues) throws SamsungCloudException {
                    Log.d(this, "restoreTrashBatch onError l = " + l);
                    request.bBatchSuccess = false;
                    throwQuotaException(l);
                }
            };

            for (int index = 0; index < sourceIds.length; index++) {
                DriveFile targetDriveFile = new DriveFile();
                targetDriveFile.fileId = sourceIds[index];

                if (restoreBatch == null) {
                    Drive.Batch.Create createRequest = drive.batch().create(Batch.RESTORE);
                    restoreBatch = retryRequest(createRequest, RETRY_MAX_COUNT);
                }

                BatchParam batchParam = new BatchParam.BatchParamBuilder().targetDriveFile(targetDriveFile).build();
                restoreBatch.add(batchParam, responseListener);

                if ((restoreBatch.size() >= API_CALL_BATCH_LIMIT) || (index == sourceIds.length - 1)) {
                    request.doBatchExecute(restoreBatch, null);
                    restoreBatch = null;
                }
            }
        } catch (SamsungCloudException e) {
            Log.d(this, "restoreTrashBatch() - SamsungCloudException : " + e.getMessage());
            throwQuotaException(e);
        } catch (IOException ioe) {
            Log.d(this, "restoreTrashBatch() - IOException : " + ioe.getMessage());
            throw ioe;
        } finally {
            int deviceId = 0;
            CloudDevice device = getCloudDevice();
            if (device != null) {
                deviceId = device.getId();
            }

            if (deviceId > 0) {
                for (DriveFile file : successList) {
                    parentIdSuccess.add(file.parent);
                    Log.d(this, "restoreTrashBatch() delta call id = " + file.fileId);
                    CachedFileBrowser.deltaFileLayerRemoveTrash(deviceId, file.fileId, true);
                }
            }

            if (successList != null && !successList.isEmpty()) {
                ((SamsungDriveContext) context).putDBforDeltaSync(successList);
            }
        }

        return parentIdSuccess;
    }

    @Override
    public SQLiteDatabase getReadableCloudDatabase() {
        return context.getDatabaseHelper().getReadableDatabase();
    }

    @Override
    public String getTrashProcessingDir(ASPFile file_param) {
        return !(file_param instanceof CloudFile) ? null : ((CloudFile) file_param).getTrashProcessing();
    }

    public void throwQuotaException(long type) throws SamsungCloudException {
        if (type == SamsungCloudException.Code.QUOTA_FULL) {
            throw new SamsungCloudException(MESSAGE_EXCEEDED_STORAGE_QUOTA, type);
        } else if (ERROR_REACH_MAX_ITEM == type) {
            throw new SamsungCloudException(MESSAGE_REACH_MAX_ITEMS, type);
        }
    }

    public void throwQuotaException(SamsungCloudException e) throws IOException {
        long type = e.getType();
        if (SamsungCloudException.Code.QUOTA_FULL == type || SamsungCloudException.Code.QUOTA_FAIL == type) {
            Log.d(this, "throwQuotaException() quota full");
            throw new IOException(CloudStorageError.OUT_OF_STORAGE_ERROR + ": " + e.getMessage());
        } else if (ERROR_REACH_MAX_ITEM == type) {
            Log.d(this, "throwQuotaException() retryRequest reach max");
            throw new IOException(CloudStorageError.REACH_MAX_ITEM_ERROR + ": " + e.getMessage());
        }
    }

    private boolean isQuotaException(long type) {
        Log.d(this, "isQuotaException type =" + type);
        return (type == SamsungCloudException.Code.QUOTA_FULL || type == SamsungCloudException.Code.QUOTA_FAIL || type == ERROR_REACH_MAX_ITEM);
    }

    private boolean ignoreDeleteTrashError(long type) {
        Log.d(this, "ignoreDeleteTrashError() type = " + type);
        return (type == ERROR_FILE_DOES_NOT_EXIST || type == ERROR_SUPPORT_ONLY_TRASHED_FILE);
    }

    @Override
    public void cancel(OperationType type) {
        synchronized (mInProgressOpMap) {
            ArrayList<Integer> cancelConnectionList = mInProgressOpMap.get(type);
            if (cancelConnectionList != null) {
                Drive drive = ((SamsungDriveContext) context).getDrive(appContext);
                for (Integer connection : cancelConnectionList) {
                    if (connection != null) {
                        drive.close(connection.intValue());
                    }
                }
                mInProgressOpMap.remove(type);
            }
        }
    }

    private NetworkStatusListener setNetworkStatusListener(final OperationType type) {
        return new NetworkStatusListener() {
            @Override
            public void onStarted(int connection) {
                Log.d(this, "setNetworkStatusListener type : " + type + ", onStarted : " + connection);
                ArrayList<Integer> connectionList = mInProgressOpMap.get(type);
                if (connectionList == null) {
                    connectionList = new ArrayList<>();
                }

                connectionList.add(connection);
                mInProgressOpMap.put(type, connectionList);
            }

            @Override
            public void onClosed(int connection) {
                Log.d(this, "setNetworkStatusListener type : " + type + ", onClosed : " + connection);
                synchronized (mInProgressOpMap) {
                    ArrayList<Integer> connectionList = mInProgressOpMap.get(type);
                    if (connectionList != null) {
                        connectionList.remove(new Integer(connection));
                        if (!connectionList.isEmpty()) {
                            mInProgressOpMap.put(type, connectionList);
                        } else {
                            mInProgressOpMap.remove(type);
                        }
                    }
                }
            }
        };
    }
}

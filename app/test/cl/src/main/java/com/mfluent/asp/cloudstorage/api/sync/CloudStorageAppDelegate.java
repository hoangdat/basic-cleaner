
package com.mfluent.asp.cloudstorage.api.sync;

import android.content.Intent;

/**
 * The Interface CloudStorageAppDelegate.
 * 
 * @author MFluent
 * @version 1.5
 */
public interface CloudStorageAppDelegate {

	/**
	 * This is used by plugins to send broadcast messages.
	 * These are some typical broadcast sent by plugins
	 * <table border="1">
	 * <tr>
	 * <th>Intent</th>
	 * <th>Description</th>
	 * </tr>
	 * <tr>
	 * <td>CloudStorageSync.CLOUD_AUTHENTICATION_SUCCESS</td>
	 * <td>Sent when authentication is successful</td>
	 * </tr>
	 * <tr>
	 * <td>CloudStorageSync.CLOUD_AUTHENTICATION_FAILURE</td>
	 * <td>Sent when authentication is fails</td>
	 * </tr>
	 * <tr>
	 * <td>CloudStorageSync.CLOUD_AUTHENTICATION_UNKNOWN</td>
	 * <td>Sent when authentication state is unknown</td>
	 * </tr>
	 * <tr>
	 * <td>CloudStorageSync.CLOUD_LAUNCH_OAUTH1_BROWSER</td>
	 * <td>Sent by the plugin when OAUTH is needed. The UI may or may not start OAUTH when received.</td>
	 * </tr>
	 * </table>
	 * 
	 * @param intent
	 *            the intent
	 */
	void sendBroadcastMessage(Intent intent);

	/**
	 * Request sync.
	 */
	void requestSync();

	/**
	 * returns boolean if the sync is blocked by wifi only option.
	 */
}

package com.mfluent.cloud.googledrive.common;

import com.mfluent.asp.common.clouddatamodel.AbsCloudFile;

public class CloudFile extends AbsCloudFile {
    protected final CloudDirectory mParent;
    protected final CloudContext mContext;

    public CloudFile(CloudContext context, CloudDirectory parent, String name) {
        super(name);
        mParent = parent;
        mContext = context;
    }

    public CloudFile(CloudFile file) {
        super(file);
        mContext = file.mContext;
        mParent = file.mParent;
    }

    public void copyFromFile(CloudFile file) {
        super.copyFromFile(file);
        mDownloadURL = file.mDownloadURL;
    }

    public CloudDirectory getParent() {
        return mParent;
    }

    public String getDownloadURL() {
        return mDownloadURL;
    }

    public void setDownloadURL(String downloadURL) {
        mDownloadURL = downloadURL;
    }

    public CloudContext getContext() {
        return mContext;
    }

    @Override
    public boolean isDirectory() {
        return this instanceof CloudDirectory;
    }
}

package com.mfluent.cloud.samsungdrive.samsungpushplatform;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.mfluent.asp.common.datamodel.ASPMediaStore;
import com.mfluent.cloud.samsungdrive.common.DriveConstants;
import com.mfluent.log.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import platform.com.mfluent.asp.datamodel.DataModelSLPF;
import platform.com.mfluent.asp.datamodel.DeviceSLPF;

public class PushMsgReceiver extends BroadcastReceiver {
    private static final String KEY_APP_ID = "appId";
    private static final String KEY_MESSAGE = "msg";
    private static final String KEY_DATA = "appData";

    private static final int SAMSUNG_DEVICE_ID = 3; // TODO : find other way.. to get deviceId

    @Override
    public void onReceive(Context context, Intent intent) {
        String appId = intent.getStringExtra(KEY_APP_ID);
        String msg = intent.getStringExtra(KEY_MESSAGE);
        String appData = intent.getStringExtra(KEY_DATA);

        if (TextUtils.isEmpty(appId) || !appId.equals(SppManager.MY_APPID)) {
            Log.d(this, "This isn't my appID : " + appId);
            return;
        }

        Log.d(this, "HJ SPP Receive Msg : " + msg + " ,appData : " + appData);
        try {
            JSONObject jmsg = new JSONObject(msg);
            String category = jmsg.getString("category");
            String type = jmsg.getString("type");

            JSONObject jdata = new JSONObject(appData);
            String uid = jdata.getString("uid");
            JSONArray arry = jdata.getJSONArray("cid");

            if (category.equals("drive") && type.equals("changed") && uid.equals(DriveConstants.apiClient.uid)) {
                int len = arry.length();
                for (int i = 0; i < len; ++i) {
                    String cid = arry.getString(i);
                    if (cid != null && cid.equals(DriveConstants.apiClient.cid)) {
                        Intent refreshIntent = new Intent(DeviceSLPF.BROADCAST_DEVICE_REFRESH);
                        refreshIntent.setDataAndType(ASPMediaStore.Device.getDeviceEntryUri(SAMSUNG_DEVICE_ID), ASPMediaStore.Device.CONTENT_TYPE);
                        refreshIntent.putExtra(DeviceSLPF.DEVICE_ID_EXTRA_KEY, SAMSUNG_DEVICE_ID);
                        DataModelSLPF dataModel = DataModelSLPF.getInstance();
                        if (dataModel != null) {
                            dataModel.sendLocalBroadcast(refreshIntent);
                        }
                        return;
                    }
                }
                Log.d(this, "There wasn't msg for my app");
            }
        } catch (JSONException e) {
            Log.e(this, e.getMessage());
        }
    }

}

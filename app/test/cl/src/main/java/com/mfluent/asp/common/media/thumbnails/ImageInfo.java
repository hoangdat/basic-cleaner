/**
 *
 */

package com.mfluent.asp.common.media.thumbnails;

import android.database.Cursor;

import com.mfluent.asp.common.datamodel.ASPMediaStore;
import com.mfluent.asp.common.datasource.AspMediaId;
import com.mfluent.asp.common.util.CursorUtils;
import com.samsung.android.sdk.slinkcloud.CloudGatewayDeviceTransportType;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;

/**
 * @author Ilan Klinghofer
 */
public class ImageInfo implements Serializable {

    private static final long serialVersionUID = -6386850848880613471L;

    public enum ThumbnailSize {
        MICRO,
        MINI,
        FULL_SCREEN,
    }

    private int desiredWidth;

    private int desiredHeight;

    private int desiredBitmapWidth;

    private int desiredBitmapHeight;

    private int fullWidth;

    private int fullHeight;

    private int thumbWidth;

    private int thumbHeight;

    private int mediaType;

    private int contentId;

    private int orientation;

    private int deviceId;

    private CloudGatewayDeviceTransportType transportType = CloudGatewayDeviceTransportType.UNKNOWN;

    private String sourceMediaId;

    private String sourceAlbumId;

    private String remoteUri;

    private String data;

    private String thumbUri;

    private ThumbnailSize thumbnailSize = ThumbnailSize.MINI;

    private String fileName;

    public static ImageInfo fromCursor(Cursor cursor) {
        ImageInfo imageInfo = new ImageInfo();

        imageInfo.setMediaType(CursorUtils.getInt(cursor, ASPMediaStore.MediaColumns.MEDIA_TYPE));
        ImageInfo.initFromCursor(imageInfo, cursor);

        return imageInfo;
    }

    private static void initFromCursor(ImageInfo imageInfo, Cursor cursor) {
        imageInfo.setContentId(CursorUtils.getInt(cursor, ASPMediaStore.BaseASPColumns._ID));
        imageInfo.setDeviceId(CursorUtils.getInt(cursor, ASPMediaStore.BaseASPColumns.DEVICE_ID));
        imageInfo.setSourceMediaId(CursorUtils.getString(cursor, ASPMediaStore.BaseASPColumns.SOURCE_MEDIA_ID));
        imageInfo.setRemoteUri(CursorUtils.getString(cursor, ASPMediaStore.MediaColumns.FULL_URI));
        imageInfo.setData(CursorUtils.getString(cursor, ASPMediaStore.MediaColumns.DATA));
        imageInfo.setFileName(CursorUtils.getString(cursor, ASPMediaStore.MediaColumns.DISPLAY_NAME));

        boolean getWidthHeight = false;
        if (imageInfo.getMediaType() == AspMediaId.MEDIA_TYPE_ALBUM) {
            imageInfo.setContentId(CursorUtils.getInt(cursor, ASPMediaStore.Audio.AlbumColumns._ID));
            imageInfo.setRemoteUri(CursorUtils.getString(cursor, ASPMediaStore.MediaColumns.THUMBNAIL_URI));
            imageInfo.setSourceAlbumId(CursorUtils.getString(cursor, ASPMediaStore.Audio.AudioColumns.SOURCE_ALBUM_ID));
        } else if (imageInfo.getMediaType() == AspMediaId.MEDIA_TYPE_AUDIO) {
            imageInfo.setContentId(CursorUtils.getInt(cursor, ASPMediaStore.Audio.AudioColumns.ALBUM_ID));
            imageInfo.setMediaType(AspMediaId.MEDIA_TYPE_ALBUM);
            imageInfo.setRemoteUri(CursorUtils.getString(cursor, ASPMediaStore.MediaColumns.THUMBNAIL_URI));
            imageInfo.setSourceAlbumId(CursorUtils.getString(cursor, ASPMediaStore.Audio.AudioColumns.SOURCE_ALBUM_ID));
        } else if (imageInfo.getMediaType() == AspMediaId.MEDIA_TYPE_VIDEO) {
            imageInfo.setRemoteUri(CursorUtils.getString(cursor, ASPMediaStore.MediaColumns.THUMBNAIL_URI));
            getWidthHeight = true;
        } else if (imageInfo.getMediaType() == AspMediaId.MEDIA_TYPE_IMAGE) {
            imageInfo.setThumbUri(CursorUtils.getString(cursor, ASPMediaStore.MediaColumns.THUMBNAIL_URI));
            imageInfo.setOrientation((CursorUtils.getInt(cursor, ASPMediaStore.Images.Media.ORIENTATION)));
            getWidthHeight = true;
        }

        if (getWidthHeight) {
            imageInfo.setFullWidth(CursorUtils.getInt(cursor, ASPMediaStore.Images.Media.WIDTH));
            imageInfo.setFullHeight(CursorUtils.getInt(cursor, ASPMediaStore.Images.Media.HEIGHT));
        }
    }

    /**
     * use ImageInfo.fromCursor() when possible
     */
    public ImageInfo() {
    }

    public ImageInfo(ImageInfo copyFrom) {
        this.contentId = copyFrom.contentId;
        this.data = copyFrom.data;
        this.desiredBitmapHeight = copyFrom.desiredBitmapHeight;
        this.desiredBitmapWidth = copyFrom.desiredBitmapWidth;
        this.desiredHeight = copyFrom.desiredHeight;
        this.desiredWidth = copyFrom.desiredWidth;
        this.fullWidth = copyFrom.fullWidth;
        this.fullHeight = copyFrom.fullHeight;
        this.thumbWidth = copyFrom.thumbWidth;
        this.thumbHeight = copyFrom.thumbHeight;
        this.deviceId = copyFrom.deviceId;
        this.fileName = copyFrom.fileName;
        this.mediaType = copyFrom.mediaType;
        this.orientation = copyFrom.orientation;
        this.remoteUri = copyFrom.remoteUri;
        this.sourceAlbumId = copyFrom.sourceAlbumId;
        this.sourceMediaId = copyFrom.sourceMediaId;
        this.thumbnailSize = copyFrom.thumbnailSize;
        this.thumbUri = copyFrom.thumbUri;
    }

    /**
     * @return the desiredWidth
     */
    public int getDesiredWidth() {
        return desiredWidth;
    }

    /**
     * @param desiredWidth the desiredWidth to set
     */
    public void setDesiredWidth(int desiredWidth) {
        if (fullWidth > 0 && fullWidth < desiredWidth) {
            desiredWidth = fullWidth;
        }
        this.desiredWidth = desiredWidth;
    }

    /**
     * @return the desiredHeight
     */
    public int getDesiredHeight() {
        return this.desiredHeight;
    }

    public CloudGatewayDeviceTransportType getDeviceTransportType() {
        return this.transportType;
    }

    /**
     * @param desiredHeight the desiredHeight to set
     */
    public void setDesiredHeight(int desiredHeight) {
        if (fullHeight > 0 && fullHeight < desiredHeight) {
            desiredHeight = fullHeight;
        }
        this.desiredHeight = desiredHeight;
    }

    /**
     * @return the desiredBitmapWidth
     */
    public int getDesiredBitmapWidth() {
        if (desiredBitmapWidth == 0) {
            return getDesiredWidth();
        }
        return desiredBitmapWidth;
    }

    /**
     * @param desiredBitmapWidth the desiredBitmapWidth to set
     */
    public void setDesiredBitmapWidth(int desiredBitmapWidth) {
        if (fullWidth > 0 && fullWidth < desiredBitmapWidth) {
            desiredBitmapWidth = fullWidth;
        }
        this.desiredBitmapWidth = desiredBitmapWidth;
    }

    /**
     * @return the desiredBitmapHeight
     */
    public int getDesiredBitmapHeight() {
        if (desiredBitmapHeight == 0) {
            return getDesiredHeight();
        }
        return desiredBitmapHeight;
    }

    /**
     * @param desiredBitmapHeight the desiredBitmapHeight to set
     */
    public void setDesiredBitmapHeight(int desiredBitmapHeight) {
        if (fullHeight > 0 && fullHeight < desiredBitmapHeight) {
            desiredBitmapHeight = fullHeight;
        }
        this.desiredBitmapHeight = desiredBitmapHeight;
    }

    /**
     * @return the mediaType
     */
    public int getMediaType() {
        return mediaType;
    }

    /**
     * @param mediaType the mediaType to set
     */
    public void setMediaType(int mediaType) {
        this.mediaType = mediaType;
    }

    /**
     * @return the contentId
     */
    public int getContentId() {
        return contentId;
    }

    /**
     * @param contentId the contentId to set
     */
    public void setContentId(int contentId) {
        this.contentId = contentId;
    }

    public void setFullWidth(int fullWidth) {
        this.fullWidth = fullWidth;
    }

    public void setFullHeight(int fullHeight) {
        this.fullHeight = fullHeight;
    }

    public int getFullWidth() {
        return fullWidth;
    }

    public int getFullHeight() {
        return fullHeight;
    }

    public void setThumbWidth(int thumbWidth) {
        this.thumbWidth = thumbWidth;
    }

    public void setThumbHeight(int thumbHeight) {
        this.thumbHeight = thumbHeight;
    }

    public int getThumbWidth() {
        return thumbWidth;
    }

    public int getThumbHeight() {
        return thumbHeight;
    }

    /**
     * @return the desiredWidth
     */
    public int getOrientation() {
        return orientation;
    }

    public void setOrientation(int orientation) {
        this.orientation = orientation;
    }

    /**
     * @return the deviceId
     */
    public int getDeviceId() {
        return deviceId;
    }

    /**
     * @param deviceId the deviceId to set
     */
    public void setDeviceId(int deviceId) {
        this.deviceId = deviceId;
    }

    /**
     * @return the sourceMediaId
     */
    public String getSourceMediaId() {
        return sourceMediaId;
    }

    /**
     * @param sourceMediaId the sourceMediaId to set
     */
    public void setSourceMediaId(String sourceMediaId) {
        this.sourceMediaId = sourceMediaId;
    }

    /**
     * @return the fullUri
     */
    public String getRemoteUri() {
        return remoteUri;
    }

    /**
     * @param fullUri the fullUri to set
     */
    public void setRemoteUri(String fullUri) {
        this.remoteUri = fullUri;
    }

    /**
     * @return the thumbnailSize
     */
    public ThumbnailSize getThumbnailSize() {
        return thumbnailSize;
    }

    /**
     * @param thumbnailSize the thumbnailSize to set
     */
    public void setThumbnailSize(ThumbnailSize thumbnailSize) {
        this.thumbnailSize = thumbnailSize;
    }

    /**
     * @return the data
     */
    public String getData() {
        return data;
    }

    public void setThumbUri(String thumbUri) {
        this.thumbUri = thumbUri;
    }

    /**
     * @param data the data to set
     */
    public void setData(String data) {
        this.data = data;
    }

    /**
     * @param sourceAlbumId - the album id on the source device, if it exists
     */
    public void setSourceAlbumId(String sourceAlbumId) {
        this.sourceAlbumId = sourceAlbumId;
    }

    /**
     * @param fileName the fileName to set
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        ToStringBuilder sb = new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE);
        sb.append("type", mediaType).append("id", contentId).append("source", sourceMediaId);

        if (desiredWidth != 0) {
            sb.append("fetch_w", desiredWidth).append("fetch_h", desiredHeight);
        }
        if (desiredBitmapWidth != 0) {
            sb.append("render_w", desiredBitmapWidth).append("render_h", desiredBitmapHeight);
        }

        if (orientation > 0) {
            sb.append("r", orientation);
        }
        // only load data *or* name (if 1 exists) - typically these are similar enough for debugging purposes
        if (data != null && data.length() > 0) {
            sb.append("data", data);
        } else if (fileName != null && fileName.length() > 0) {
            sb.append("name", fileName);
        }

        return sb.toString();
    }
}

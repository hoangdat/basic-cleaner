
package com.mfluent.asp.common.util;

import android.webkit.MimeTypeMap;

import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

import platform.com.mfluent.asp.util.AspFileUtilsSLPF;

public class FileTypeHelper {

    private static final String APPLICATION_OCTET_STREAM = "application/octet-stream";

    public static final String getMimeTypeForFile(File file) {
        return FileTypeHelper.getMimeTypeForFile(file.getName(), APPLICATION_OCTET_STREAM);
    }

    public static final String getMimeTypeForFile(String filename, String defaultValue) {

        String type = null;

        if (StringUtils.isNotEmpty(filename)) {
            String extension = MimeTypeMap.getFileExtensionFromUrl(filename);
            if (StringUtils.isBlank(extension)) {
                // Something went wrong so manually try and parse extension for example this
                // filename fails but is a test case run by Samsung testers
                // Filename "&#47693;&#47693;&#51060;(1).JPG" return an empty extension
                int index = filename.lastIndexOf('.');
                if (index >= 0 && index < filename.length()) {
                    extension = filename.substring(index + 1);
                }
            }

            if (extension != null) {
                MimeTypeMap mime = MimeTypeMap.getSingleton();
                type = mime.getMimeTypeFromExtension(extension.toLowerCase(Locale.US));

                if (StringUtils.isEmpty(type)) {
                    type = MimeType.EXTENSION_TO_MIME_TYPE_MAP.get(extension.toUpperCase(Locale.US));
                }
            }
        }

        if (StringUtils.isEmpty(type)) {
            type = defaultValue;
        }

        return type;
    }

    public enum DocumentType {
        MS_WORD,
        HWP,
        PDF,
        MS_POWERPOINT,
        MS_EXCEL,
        TXT,
        ZIP,
        JAR,
        ETC
    }

    private static final Map<String, DocumentType> extensionsToDocumentTypeMapping;

    static {
        // NOTE: If an extension is removed from this list (or the document type is changed), make sure to increment the database version (com.mfluent.asp.datamodel.ASPMediaStoreProvider.DatabaseHelper.DATABASE_VERSION)
        // and then wipe the database in onUpgrade. This is needed so that files that are no longer considered documents are removed from the database. If you add to this list the regular
        // sync logic should pick up the new files automatically
        //@formatter:off
        extensionsToDocumentTypeMapping = new LinkedHashMap<String, DocumentType>();
        extensionsToDocumentTypeMapping.put("DOC", DocumentType.MS_WORD);
        extensionsToDocumentTypeMapping.put("DOCX", DocumentType.MS_WORD);
        extensionsToDocumentTypeMapping.put("DOT", DocumentType.MS_WORD);
        extensionsToDocumentTypeMapping.put("DOTX", DocumentType.MS_WORD);

        extensionsToDocumentTypeMapping.put("HWP", DocumentType.HWP);

        extensionsToDocumentTypeMapping.put("PDF", DocumentType.PDF);

        extensionsToDocumentTypeMapping.put("PPT", DocumentType.MS_POWERPOINT);
        extensionsToDocumentTypeMapping.put("PPTX", DocumentType.MS_POWERPOINT);
        extensionsToDocumentTypeMapping.put("PPS", DocumentType.MS_POWERPOINT);
        extensionsToDocumentTypeMapping.put("PPSX", DocumentType.MS_POWERPOINT);
        extensionsToDocumentTypeMapping.put("POT", DocumentType.MS_POWERPOINT);
        extensionsToDocumentTypeMapping.put("POTX", DocumentType.MS_POWERPOINT);

        extensionsToDocumentTypeMapping.put("XLS", DocumentType.MS_EXCEL);
        extensionsToDocumentTypeMapping.put("XLSX", DocumentType.MS_EXCEL);
        extensionsToDocumentTypeMapping.put("XLT", DocumentType.MS_EXCEL);
        extensionsToDocumentTypeMapping.put("XLTX", DocumentType.MS_EXCEL);
        extensionsToDocumentTypeMapping.put("CSV", DocumentType.MS_EXCEL);

        extensionsToDocumentTypeMapping.put("TXT", DocumentType.TXT);

        extensionsToDocumentTypeMapping.put("ZIP", DocumentType.ZIP);
        extensionsToDocumentTypeMapping.put("JAR", DocumentType.JAR);

        extensionsToDocumentTypeMapping.put("SCC", DocumentType.ETC); //Samsung Scrapbook
        extensionsToDocumentTypeMapping.put("VCF", DocumentType.ETC); //contact file
        extensionsToDocumentTypeMapping.put("ICS", DocumentType.ETC); //calendar file

        //@formatter:on
    }

    // We use extensions to recognize documents, because that's what the HomeSync team does, and we want to be consistent
    public static boolean isAspDocument(String filename) {
        String extension = StringUtils.upperCase(AspFileUtilsSLPF.getExtension(filename));
        if (extension == null) {
            return false;
        }

        return extensionsToDocumentTypeMapping.containsKey(extension);
    }
}

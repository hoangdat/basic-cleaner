/**
 * 
 */

package com.mfluent.asp.common.io.util;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.io.OutputStream;

/**
 * @author Ilan Klinghofer
 */
public class InterruptibleOutputStream extends OutputStream {

	private final OutputStream mOutstream;

	public InterruptibleOutputStream(OutputStream outstream) {
		mOutstream = outstream;
	}

	@Override
	public void write(int b) throws IOException {
		checkForInterrupt();
		mOutstream.write(b);
	}

	@Override
	public void write(byte[] b) throws IOException {
		checkForInterrupt();
		mOutstream.write(b);
	}

	@Override
	public void write(byte[] b, int off, int len) throws IOException {
		checkForInterrupt();
		mOutstream.write(b, off, len);
	}

	@Override
	public void flush() throws IOException {
		checkForInterrupt();
		mOutstream.flush();
	}

	private void checkForInterrupt() throws InterruptedIOException {
		if (Thread.currentThread().isInterrupted()) {
			throw new InterruptedIOException();
		}
	}

	@Override
	public void close() throws IOException {
		mOutstream.close();
	}
}

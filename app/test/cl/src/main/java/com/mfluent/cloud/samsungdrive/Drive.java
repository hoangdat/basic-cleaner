package com.mfluent.cloud.samsungdrive;


import android.content.Context;

import com.mfluent.log.Log;
import com.samsung.android.sdk.SsdkUnsupportedException;
import com.samsung.android.sdk.scloud.api.drive.batch.BatchRequest;
import com.samsung.android.sdk.scloud.client.ApiClient;
import com.samsung.android.sdk.scloud.decorator.drive.DriveFile;
import com.samsung.android.sdk.scloud.decorator.drive.DriveFileList;
import com.samsung.android.sdk.scloud.decorator.drive.SamsungCloudDrive;
import com.samsung.android.sdk.scloud.exception.SamsungCloudException;
import com.samsung.android.sdk.scloud.listeners.NetworkStatusListener;
import com.samsung.android.sdk.scloud.listeners.ProgressListener;


public class Drive {
    SamsungCloudDrive drive;
    com.samsung.android.sdk.scloud.decorator.drive.Files files;
    com.samsung.android.sdk.scloud.decorator.drive.Trash trash;
    com.samsung.android.sdk.scloud.decorator.drive.Changes changes;
    com.samsung.android.sdk.scloud.decorator.drive.Batch batch;

    public Drive(Context context, String appId, String appName, String countryCode, ApiClient apiClient) throws SamsungCloudException, SsdkUnsupportedException {
        try {
            Log.d(this, "Drive : " + apiClient.pushAppId + " " + apiClient.pushToken + " " + apiClient.pushName);
            drive = new SamsungCloudDrive(context, appId, appName, countryCode, apiClient);
            this.files = drive.files;
            this.trash = drive.trash;
            this.changes = drive.changes;
            this.batch = drive.batch;
        } catch (SsdkUnsupportedException e) {
            e.printStackTrace();
        } catch (SamsungCloudException e) {
            Log.d(this, "Drive() - SamsungCloudException : " + e.getMessage());
            e.printStackTrace();
            throw e;
        }
    }

    public void setApiClient(ApiClient apiClient) {
        try {
            drive.setApiClient(apiClient);
        } catch (SamsungCloudException e) {
            Log.e(this, "setApiClient : " + e.getMessage());
        }
    }

    public abstract class DriveRequest<T> {
        public abstract T execute() throws SamsungCloudException;
    }

    public void close() {
        if (drive != null) {
            drive.close();
        }
    }

    public void close(int connection) {
        if (drive != null) {
            drive.close(connection);
        }
    }

    public Drive.Files files() {
        return new Drive.Files();
    }

    public Drive.Trash trash() {
        return new Drive.Trash();
    }

    public Drive.Changes changes() {
        return new Drive.Changes();
    }

    public Drive.Batch batch() {
        return new Drive.Batch();
    }

    public class Files {
        public Files() {
        }

        public Drive.Files.Upload upload(DriveFile destinationFolder, String pathToUpload, ProgressListener progressListener) {
            return upload(destinationFolder, pathToUpload, null, progressListener);
        }

        public Drive.Files.Upload upload(DriveFile destinationFolder, String pathToUpload, ProgressListener progressListener, NetworkStatusListener networkStatusListener) {
            return upload(destinationFolder, pathToUpload, null, progressListener, networkStatusListener);
        }

        public Drive.Files.Upload upload(DriveFile destinationFolder, String pathToUpload, java.util.List<String> tags, ProgressListener progressListener) {
            return upload(destinationFolder, pathToUpload, tags, progressListener, null);
        }

        public Drive.Files.Upload upload(DriveFile destinationFolder, String pathToUpload, java.util.List<String> tags, ProgressListener progressListener, NetworkStatusListener networkStatusListener) {
            return new Drive.Files.Upload(destinationFolder, pathToUpload, tags, progressListener, networkStatusListener);
        }

        public Drive.Files.List list() {
            return list(null);
        }

        public Drive.Files.List list(NetworkStatusListener networkStatusListener) {
            return new Drive.Files.List(networkStatusListener);
        }

        public Drive.Files.ListChildren listChildren(DriveFile targetFolder) {
            return listChildren(targetFolder, null, null);
        }

        public Drive.Files.ListChildren listChildren(DriveFile targetFolder, String column, String attribute) {
            return listChildren(targetFolder, column, attribute, null);
        }

        public Drive.Files.ListChildren listChildren(DriveFile targetFolder, String column, String attribute, NetworkStatusListener networkStatusListener) {
            return new Drive.Files.ListChildren(targetFolder, column, attribute, networkStatusListener);
        }

        public Drive.Files.ListChildrenWithoutPaging listChildrenWithoutPaging(DriveFile targetFolder) {
            return listChildrenWithoutPaging(targetFolder, null, null);
        }

        public Drive.Files.ListChildrenWithoutPaging listChildrenWithoutPaging(DriveFile targetFolder, String column, String attribute) {
            return listChildrenWithoutPaging(targetFolder, column, attribute, null);
        }

        public Drive.Files.ListChildrenWithoutPaging listChildrenWithoutPaging(DriveFile targetFolder, String column, String attribute, NetworkStatusListener networkStatusListener) {
            return new Drive.Files.ListChildrenWithoutPaging(targetFolder, column, attribute, networkStatusListener);
        }

        public Drive.Files.Download download(DriveFile sourceDriveFile, String pathToDownload, String fileName, ProgressListener progressListener) {
            return download(sourceDriveFile, pathToDownload, fileName, progressListener, null);
        }

        public Drive.Files.Download download(DriveFile sourceDriveFile, String pathToDownload, String fileName, ProgressListener progressListener, NetworkStatusListener networkStatusListener) {
            return new Drive.Files.Download(sourceDriveFile, pathToDownload, fileName, progressListener, networkStatusListener);
        }

        public Drive.Files.DownloadThumbnail downloadThumbnail(DriveFile sourceDriveFile, String pathToDownload, String fileName, String size) {
            return downloadThumbnail(sourceDriveFile, pathToDownload, fileName, size, null);
        }

        public Drive.Files.DownloadThumbnail downloadThumbnail(DriveFile sourceDriveFile, String pathToDownload, String fileName, String size, NetworkStatusListener networkStatusListener) {
            return new Drive.Files.DownloadThumbnail(sourceDriveFile, pathToDownload, fileName, size, networkStatusListener);
        }

        public Drive.Files.CreateFolder createFolder(String name, DriveFile destinationDriveFolder) {
            return createFolder(name, destinationDriveFolder, null);
        }

        public Drive.Files.CreateFolder createFolder(String name, DriveFile destinationDriveFolder, java.util.List<String> tags) {
            return createFolder(name, destinationDriveFolder, tags, null);
        }

        public Drive.Files.CreateFolder createFolder(String name, DriveFile destinationDriveFolder, java.util.List<String> tags, NetworkStatusListener networkStatusListener) {
            return new Drive.Files.CreateFolder(name, destinationDriveFolder, tags, networkStatusListener);
        }

        public Drive.Files.CreateFile createFile(String fileName, DriveFile sourceDriveFile, DriveFile destinationDriveFolder) {
            return createFile(fileName, sourceDriveFile, destinationDriveFolder, (java.util.List<String>)null);
        }

        public Drive.Files.CreateFile createFile(String fileName, DriveFile sourceDriveFile, DriveFile destinationDriveFolder, NetworkStatusListener networkStatusListener) {
            return createFile(fileName, sourceDriveFile, destinationDriveFolder, null, networkStatusListener);
        }

        public Drive.Files.CreateFile createFile(String fileName, DriveFile sourceDriveFile, DriveFile destinationDriveFolder, java.util.List<String> tags) {
            return createFile(fileName, sourceDriveFile, destinationDriveFolder, tags, null);
        }

        public Drive.Files.CreateFile createFile(String fileName, DriveFile sourceDriveFile, DriveFile destinationDriveFolder, java.util.List<String> tags, NetworkStatusListener networkStatusListener) {
            return new Drive.Files.CreateFile(fileName, sourceDriveFile, destinationDriveFolder, tags, networkStatusListener);
        }

        public Drive.Files.Move move(DriveFile sourceDriveFile, DriveFile destinationDriveFolder) {
            return move(sourceDriveFile, destinationDriveFolder, false);
        }

        public Drive.Files.Move move(DriveFile sourceDriveFile, DriveFile destinationDriveFolder, NetworkStatusListener networkStatusListener) {
            return move(sourceDriveFile, destinationDriveFolder, false, networkStatusListener);
        }

        public Drive.Files.Move move(DriveFile sourceDriveFile, DriveFile destinationDriveFolder, boolean ignoreConflict) {
            return move(sourceDriveFile, destinationDriveFolder, ignoreConflict, null);
        }

        public Drive.Files.Move move(DriveFile sourceDriveFile, DriveFile destinationDriveFolder, boolean ignoreConflict, NetworkStatusListener networkStatusListener) {
            return new Drive.Files.Move(sourceDriveFile, destinationDriveFolder, ignoreConflict, networkStatusListener);
        }

        public Drive.Files.Rename rename(DriveFile targetDriveFile, String newName) {
            return rename(targetDriveFile, newName, false);
        }

        public Drive.Files.Rename rename(DriveFile targetDriveFile, String newName, boolean ignoreConflict) {
            return rename(targetDriveFile, newName, ignoreConflict, null);
        }

        public Drive.Files.Rename rename(DriveFile targetDriveFile, String newName, boolean ignoreConflict, NetworkStatusListener networkStatusListener) {
            return new Drive.Files.Rename(targetDriveFile, newName, ignoreConflict, networkStatusListener);
        }

        public Drive.Files.UpdateTags updateTags(DriveFile targetDriveFile, java.util.List<String> tags) {
            return updateTags(targetDriveFile, tags, false);
        }

        public Drive.Files.UpdateTags updateTags(DriveFile targetDriveFile, java.util.List<String> tags, boolean ignoreConflict) {
            return updateTags(targetDriveFile, tags, ignoreConflict, null);
        }

        public Drive.Files.UpdateTags updateTags(DriveFile targetDriveFile, java.util.List<String> tags, boolean ignoreConflict, NetworkStatusListener networkStatusListener) {
            return new Drive.Files.UpdateTags(targetDriveFile, tags, ignoreConflict, networkStatusListener);
        }

        public Drive.Files.Delete delete(DriveFile targetDriveFile) {
            return delete(targetDriveFile, false);
        }

        public Drive.Files.Delete delete(DriveFile targetDriveFile, NetworkStatusListener networkStatusListener) {
            return delete(targetDriveFile, false, networkStatusListener);
        }

        public Drive.Files.Delete delete(DriveFile targetDriveFile, boolean ignoreConflict) {
            return delete(targetDriveFile, ignoreConflict, null);
        }

        public Drive.Files.Delete delete(DriveFile targetDriveFile, boolean ignoreConflict, NetworkStatusListener networkStatusListener) {
            return new Drive.Files.Delete(targetDriveFile, ignoreConflict, networkStatusListener);
        }

        public Drive.Files.DeletePermanently deletePermanently(DriveFile targetDriveFile) {
            return deletePermanently(targetDriveFile, false);
        }

        public Drive.Files.DeletePermanently deletePermanently(DriveFile targetDriveFile, NetworkStatusListener networkStatusListener) {
            return deletePermanently(targetDriveFile, false, networkStatusListener);
        }

        public Drive.Files.DeletePermanently deletePermanently(DriveFile targetDriveFile, boolean ignoreConflict) {
            return deletePermanently(targetDriveFile, ignoreConflict, null);
        }

        public Drive.Files.DeletePermanently deletePermanently(DriveFile targetDriveFile, boolean ignoreConflict, NetworkStatusListener networkStatusListener) {
            return new Drive.Files.DeletePermanently(targetDriveFile, ignoreConflict, networkStatusListener);
        }

        public Drive.Files.GetMeta getMeta(DriveFile targetDriveFile) {
            return getMeta(targetDriveFile, null);
        }

        public Drive.Files.GetMeta getMeta(DriveFile targetDriveFile, NetworkStatusListener networkStatusListener) {
            return new Drive.Files.GetMeta(targetDriveFile, networkStatusListener);
        }


        public class Upload extends DriveRequest<DriveFile> {
            DriveFile destinationFolder;
            String pathToUpload;
            java.util.List<String> tags;
            ProgressListener progressListener;
            NetworkStatusListener networkStatusListener;

            public Upload(DriveFile destinationFolder, String pathToUpload, java.util.List<String> tags, ProgressListener progressListener, NetworkStatusListener networkStatusListener) {
                this.destinationFolder = destinationFolder;
                this.pathToUpload = pathToUpload;
                this.tags = tags;
                this.progressListener = progressListener;
                this.networkStatusListener = networkStatusListener;
            }

            @Override
            public DriveFile execute() throws SamsungCloudException {
                return files.upload(destinationFolder, pathToUpload, tags, progressListener, networkStatusListener);
            }
        }

        public class List extends DriveRequest<DriveFileList> {
            NetworkStatusListener networkStatusListener;

            public List(NetworkStatusListener networkStatusListener) {
                this.networkStatusListener = networkStatusListener;
            }

            @Override
            public DriveFileList execute() throws SamsungCloudException {
                return files.list(networkStatusListener);
            }
        }

        public class ListChildren extends DriveRequest<DriveFileList> {
            DriveFile targetFolder;
            String column;
            String attribute;
            NetworkStatusListener networkStatusListener;

            public ListChildren(DriveFile targetFolder, String column, String attribute, NetworkStatusListener networkStatusListener) {
                this.targetFolder = targetFolder;
                this.column = column;
                this.attribute = attribute;
                this.networkStatusListener = networkStatusListener;
            }

            @Override
            public DriveFileList execute() throws SamsungCloudException {
                return files.listChildren(targetFolder, column, attribute, networkStatusListener);
            }
        }

        public class ListChildrenWithoutPaging extends DriveRequest<DriveFileList> {
            DriveFile targetFolder;
            String column;
            String attribute;
            NetworkStatusListener networkStatusListener;

            public ListChildrenWithoutPaging(DriveFile targetFolder, String column, String attribute, NetworkStatusListener networkStatusListener) {
                this.targetFolder = targetFolder;
                this.column = column;
                this.attribute = attribute;
                this.networkStatusListener = networkStatusListener;
            }

            @Override
            public DriveFileList execute() throws SamsungCloudException {
                return files.listChildrenWithoutPaging(targetFolder, column, attribute, networkStatusListener);
            }
        }

        public class Download extends DriveRequest<Boolean> {
            DriveFile sourceDriveFile;
            String pathToDownload;
            String fileName;
            ProgressListener progressListener;
            NetworkStatusListener networkStatusListener;

            public Download(DriveFile sourceDriveFile, String pathToDownload, String fileName, ProgressListener progressListener, NetworkStatusListener networkStatusListener) {
                this.sourceDriveFile = sourceDriveFile;
                this.pathToDownload = pathToDownload;
                this.fileName = fileName;
                this.progressListener = progressListener;
                this.networkStatusListener = networkStatusListener;
            }

            @Override
            public Boolean execute() throws SamsungCloudException {
                files.download(sourceDriveFile, pathToDownload, fileName, progressListener, networkStatusListener);
                return true;
            }
        }

        public class DownloadThumbnail extends DriveRequest<Boolean> {
            DriveFile sourceDriveFile;
            String pathToDownload;
            String fileName;
            String size;
            NetworkStatusListener networkStatusListener;

            public DownloadThumbnail(DriveFile sourceDriveFile, String pathToDownload, String fileName, String size, NetworkStatusListener networkStatusListener) {
                this.sourceDriveFile = sourceDriveFile;
                this.pathToDownload = pathToDownload;
                this.fileName = fileName;
                this.size = size;
                this.networkStatusListener = networkStatusListener;
            }

            @Override
            public Boolean execute() throws SamsungCloudException {
                files.downloadThumbnail(sourceDriveFile, pathToDownload, fileName, size, networkStatusListener);
                return true;
            }
        }

        public class CreateFolder extends DriveRequest<DriveFile> {
            String name;
            DriveFile destinationDriveFolder;
            java.util.List<String> tags;
            NetworkStatusListener networkStatusListener;

            public CreateFolder(String name, DriveFile destinationDriveFolder, java.util.List<String> tags, NetworkStatusListener networkStatusListener) {
                this.name = name;
                this.destinationDriveFolder = destinationDriveFolder;
                this.tags = tags;
                this.networkStatusListener = networkStatusListener;
            }

            @Override
            public DriveFile execute() throws SamsungCloudException {
                return files.createFolder(name, destinationDriveFolder, tags, networkStatusListener);
            }
        }

        public class CreateFile extends DriveRequest<DriveFile> {
            String fileName;
            DriveFile sourceDriveFile;
            DriveFile destinationDriveFolder;
            java.util.List<String> tags;
            NetworkStatusListener networkStatusListener;

            public CreateFile(String fileName, DriveFile sourceDriveFile, DriveFile destinationDriveFolder, java.util.List<String> tags, NetworkStatusListener networkStatusListener) {
                this.fileName = fileName;
                this.sourceDriveFile = sourceDriveFile;
                this.destinationDriveFolder = destinationDriveFolder;
                this.tags = tags;
                this.networkStatusListener = networkStatusListener;
            }

            @Override
            public DriveFile execute() throws SamsungCloudException {
                return files.createFile(fileName, sourceDriveFile, destinationDriveFolder, tags, networkStatusListener);
            }
        }

        public class Move extends DriveRequest<DriveFile> {
            DriveFile sourceDriveFile;
            DriveFile destinationDriveFolder;
            boolean ignoreConflict;
            NetworkStatusListener networkStatusListener;

            public Move(DriveFile sourceDriveFile, DriveFile destinationDriveFolder, boolean ignoreConflict, NetworkStatusListener networkStatusListener) {
                this.sourceDriveFile = sourceDriveFile;
                this.destinationDriveFolder = destinationDriveFolder;
                this.ignoreConflict = ignoreConflict;
                this.networkStatusListener = networkStatusListener;
            }

            @Override
            public DriveFile execute() throws SamsungCloudException {
                return files.move(sourceDriveFile, destinationDriveFolder, ignoreConflict, networkStatusListener);
            }
        }

        public class Rename extends DriveRequest<DriveFile> {
            DriveFile targetDriveFile;
            String newName;
            boolean ignoreConflict;
            NetworkStatusListener networkStatusListener;

            public Rename(DriveFile targetDriveFile, String newName, boolean ignoreConflict, NetworkStatusListener networkStatusListener) {
                this.targetDriveFile = targetDriveFile;
                this.newName = newName;
                this.ignoreConflict = ignoreConflict;
                this.networkStatusListener = networkStatusListener;
            }

            @Override
            public DriveFile execute() throws SamsungCloudException {
                return files.rename(targetDriveFile, newName, ignoreConflict, networkStatusListener);
            }
        }

        public class UpdateTags extends DriveRequest<DriveFile> {
            DriveFile targetDriveFile;
            java.util.List<String> tags;
            boolean ignoreConflict;
            NetworkStatusListener networkStatusListener;

            public UpdateTags(DriveFile targetDriveFile, java.util.List<String> tags, boolean ignoreConflict, NetworkStatusListener networkStatusListener) {
                this.targetDriveFile = targetDriveFile;
                this.tags = tags;
                this.ignoreConflict = ignoreConflict;
                this.networkStatusListener = networkStatusListener;
            }

            @Override
            public DriveFile execute() throws SamsungCloudException {
                return files.updateTags(targetDriveFile, tags, ignoreConflict, networkStatusListener);
            }
        }

        public class Delete extends DriveRequest<DriveFile> {
            DriveFile targetDriveFile;
            boolean ignoreConflict = false;
            NetworkStatusListener networkStatusListener;

            public Delete(DriveFile targetDriveFile, boolean ignoreConflict, NetworkStatusListener networkStatusListener) {
                this.targetDriveFile = targetDriveFile;
                this.ignoreConflict = ignoreConflict;
                this.networkStatusListener = networkStatusListener;
            }

            @Override
            public DriveFile execute() throws SamsungCloudException {
                return files.delete(targetDriveFile, false);
            }
        }

        public class DeletePermanently extends DriveRequest<DriveFile> {
            DriveFile targetDriveFile;
            boolean igmoreConflict = false;
            NetworkStatusListener networkStatusListener;

            public DeletePermanently(DriveFile targetDriveFile, boolean ignoreConflict, NetworkStatusListener networkStatusListener) {
                this.targetDriveFile = targetDriveFile;
                this.igmoreConflict = ignoreConflict;
                this.networkStatusListener = networkStatusListener;
            }

            @Override
            public DriveFile execute() throws SamsungCloudException {
                return files.deletePermanently(targetDriveFile, igmoreConflict, networkStatusListener);
            }
        }

        public class GetMeta extends DriveRequest<DriveFile> {
            DriveFile targetDriveFile;
            NetworkStatusListener networkStatusListener;

            public GetMeta(DriveFile targetDriveFile, NetworkStatusListener networkStatusListener) {
                this.targetDriveFile = targetDriveFile;
                this.networkStatusListener = networkStatusListener;
            }

            @Override
            public DriveFile execute() throws SamsungCloudException {
                return files.getMeta(targetDriveFile, networkStatusListener);
            }
        }
    }

    public class Trash {
        public Trash() {
        }

        public Drive.Trash.List list() {
            return list(null);
        }

        public Drive.Trash.List list(NetworkStatusListener networkStatusListener) {
            return new Drive.Trash.List(networkStatusListener);
        }

        public Drive.Trash.Delete delete(DriveFile targetDriveFile) {
            return delete(targetDriveFile, false);
        }

        public Drive.Trash.Delete delete(DriveFile targetDriveFile, boolean ignoreConflict) {
            return delete(targetDriveFile, ignoreConflict, null);
        }

        public Drive.Trash.Delete delete(DriveFile targetDriveFile, boolean ignoreConflict, NetworkStatusListener networkStatusListener) {
            return new Drive.Trash.Delete(targetDriveFile, ignoreConflict, networkStatusListener);
        }

        public Drive.Trash.Restore restore(DriveFile targetDriveFile, boolean ignoreConflict) {
            return restore(targetDriveFile, ignoreConflict, null);
        }

        public Drive.Trash.Restore restore(DriveFile targetDriveFile, boolean ignoreConflict, NetworkStatusListener networkStatusListener) {
            return new Drive.Trash.Restore(targetDriveFile, ignoreConflict, networkStatusListener);
        }

        public Drive.Trash.Empty empty() {
            return empty(null);
        }

        public Drive.Trash.Empty empty(NetworkStatusListener networkStatusListener) {
            return new Drive.Trash.Empty(networkStatusListener);
        }

        public class List extends DriveRequest<DriveFileList> {
            NetworkStatusListener networkStatusListener;

            public List(NetworkStatusListener networkStatusListener) {
                this.networkStatusListener = networkStatusListener;
            }

            @Override
            public DriveFileList execute() throws SamsungCloudException {
                return trash.list(networkStatusListener);
            }
        }

        public class Delete extends DriveRequest<DriveFile> {
            DriveFile targetDriveFile;
            boolean ignoreConflict;
            NetworkStatusListener networkStatusListener;

            public Delete(DriveFile targetDriveFile, boolean ignoreConflict, NetworkStatusListener networkStatusListener) {
                this.targetDriveFile = targetDriveFile;
                this.ignoreConflict = ignoreConflict;
                this.networkStatusListener = networkStatusListener;
            }

            @Override
            public DriveFile execute() throws SamsungCloudException {
                return trash.delete(targetDriveFile, ignoreConflict, networkStatusListener);
            }
        }

        public class Restore extends DriveRequest<DriveFile> {
            DriveFile targetDriveFile;
            boolean ignoreConflict;
            NetworkStatusListener networkStatusListener;

            public Restore(DriveFile targetDriveFile, boolean ignoreConflict, NetworkStatusListener networkStatusListener) {
                this.targetDriveFile = targetDriveFile;
                this.ignoreConflict = ignoreConflict;
                this.networkStatusListener = networkStatusListener;
            }

            @Override
            public DriveFile execute() throws SamsungCloudException {
                return trash.restore(targetDriveFile, ignoreConflict, networkStatusListener);
            }
        }

        public class Empty extends DriveRequest<Boolean> {
            NetworkStatusListener networkStatusListener;

            public Empty(NetworkStatusListener networkStatusListener) {
                this.networkStatusListener = networkStatusListener;
            }

            @Override
            public Boolean execute() throws SamsungCloudException {
                return trash.empty(networkStatusListener);
            }
        }
    }

    public class Changes {
        public Changes() {
        }

        public Changes.GetChangePoint getChangePoint() {
            return getChangePoint(null);
        }

        public Changes.GetChangePoint getChangePoint(NetworkStatusListener networkStatusListener) {
            return new Changes.GetChangePoint(networkStatusListener);
        }

        public Changes.List list(String startChangePoint) {
            return list(startChangePoint, false);
        }

        public Changes.List list(String startChangePoint, boolean excludeChangedByThisDevice) {
            return list(startChangePoint, excludeChangedByThisDevice, null);
        }

        public Changes.List list(String startChangePoint, boolean excludeChangedByThisDevice, NetworkStatusListener networkStatusListener) {
            return new Changes.List(startChangePoint, excludeChangedByThisDevice, networkStatusListener);
        }

        public Changes.ListWithoutPaging listWithoutPaging(String startChangePoint) {
            return listWithoutPaging(startChangePoint, false);
        }

        public Changes.ListWithoutPaging listWithoutPaging(String startChangePoint, boolean excludeChangedByThisDevice) {
            return listWithoutPaging(startChangePoint, excludeChangedByThisDevice, null);
        }

        public Changes.ListWithoutPaging listWithoutPaging(String startChangePoint, boolean excludeChangedByThisDevice, NetworkStatusListener networkStatusListener) {
            return new Changes.ListWithoutPaging(startChangePoint, excludeChangedByThisDevice, networkStatusListener);
        }

        public class GetChangePoint extends DriveRequest<String> {
            NetworkStatusListener networkStatusListener;

            public GetChangePoint(NetworkStatusListener networkStatusListener) {
                this.networkStatusListener = networkStatusListener;
            }

            @Override
            public String execute() throws SamsungCloudException {
                return changes.getChangePoint(networkStatusListener);
            }
        }

        public class List extends DriveRequest<DriveFileList> {
            String startChangePoint;
            boolean excludeChangedByThisDevice;
            NetworkStatusListener networkStatusListener;

            public List(String startChangePoint, boolean excludeChangedByThisDevice, NetworkStatusListener networkStatusListener) {
                this.startChangePoint = startChangePoint;
                this.excludeChangedByThisDevice = excludeChangedByThisDevice;
                this.networkStatusListener = networkStatusListener;
            }

            @Override
            public DriveFileList execute() throws SamsungCloudException {
                return changes.list(startChangePoint, excludeChangedByThisDevice, networkStatusListener);
            }
        }

        public class ListWithoutPaging extends DriveRequest<DriveFileList> {
            String startChangePoint;
            boolean excludeChangedByThisDevice;
            NetworkStatusListener networkStatusListener;

            public ListWithoutPaging(String startChangePoint, boolean excludeChangedByThisDevice, NetworkStatusListener networkStatusListener) {
                this.startChangePoint = startChangePoint;
                this.excludeChangedByThisDevice = excludeChangedByThisDevice;
                this.networkStatusListener = networkStatusListener;
            }

            @Override
            public DriveFileList execute() throws SamsungCloudException {
                return changes.listWithoutPaging(startChangePoint, excludeChangedByThisDevice, networkStatusListener);
            }
        }
    }

    public class Batch {
        public Batch() {
        }

        public Batch.Create create(String type) {
            return new Batch.Create(type);
        }

        public class Create extends DriveRequest<BatchRequest> {
            String type;

            public Create(String type) {
                this.type = type;
            }

            @Override
            public BatchRequest execute() throws SamsungCloudException {
                return batch.create(type);
            }
        }
    }
}

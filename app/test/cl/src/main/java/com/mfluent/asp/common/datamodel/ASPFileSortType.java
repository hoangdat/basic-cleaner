/**
 * 
 */

package com.mfluent.asp.common.datamodel;

import com.samsung.android.sdk.slinkcloud.CloudGatewayMediaStore.FileBrowser.FileBrowserColumns;

/**
 * @author Ilan Klinghofer
 */
public enum ASPFileSortType {
	DATE_MODIFIED_ASC(FileBrowserColumns.LAST_MODIFIED),
	DATE_MODIFIED_DESC(FileBrowserColumns.LAST_MODIFIED + " DESC"),
	SIZE_ASC(FileBrowserColumns.SIZE),
	SIZE_DESC(FileBrowserColumns.SIZE + " DESC"),
	NAME_ASC(FileBrowserColumns.DISPLAY_NAME),
	NAME_DESC(FileBrowserColumns.DISPLAY_NAME + " DESC"),
	TYPE_ASC(FileBrowserColumns.MIME_TYPE),
	TYPE_DESC(FileBrowserColumns.MIME_TYPE + " DESC"),
	DURATION_ASC(null),
	DURATION_DESC(null);

	ASPFileSortType(String cursorSortOrder) {
		this.cursorSortOrder = cursorSortOrder;
	}

	private final String cursorSortOrder;

	public static ASPFileSortType getSortTypeFromCursorSortOrder(String cursorSortOrder) {
		if (cursorSortOrder == null) {
			return null;
		}

		for (ASPFileSortType type : ASPFileSortType.values()) {
			if (cursorSortOrder.equalsIgnoreCase(type.cursorSortOrder)) {
				return type;
			}
		}

		return null;
	}

	public static ASPFileSortType getDefaultSortType() {
		return DATE_MODIFIED_DESC;
	}
}

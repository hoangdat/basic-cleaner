/*
 * Copyright (c) 2009-2011 Dropbox, Inc.
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.mfluent.cloud.samsungdrive.exception;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpUriRequest;

/**
 * A base for all exceptions from using the API. Catch this instead of specific
 * subclasses when you want to deal with those issues in a generic way.
 *
 * @author MFluent
 * @version 1.5
 */
public class CloudException extends Exception {

	private static final long serialVersionUID = 7364165275206332134L;

	public HttpUriRequest request;

	public HttpResponse response;

	public CloudException(HttpUriRequest request, HttpResponse response) {
		super();
		this.request = request;
		this.response = response;
	}

	/**
	 * Instantiates a new cloud exception.
	 */
	protected CloudException() {
		super();
	}

	/**
	 * Instantiates a new cloud exception.
	 *
	 * @param detailMessage
	 *            the detail message
	 */
	public CloudException(String detailMessage) {
		super(detailMessage);
	}

	/**
	 * Instantiates a new cloud exception.
	 *
	 * @param detailMessage
	 *            the detail message
	 * @param throwable
	 *            the throwable
	 */
	public CloudException(String detailMessage, Throwable throwable) {
		super(detailMessage, throwable);
	}

	/**
	 * Instantiates a new cloud exception.
	 *
	 * @param throwable
	 *            the throwable
	 */
	public CloudException(Throwable throwable) {
		super(throwable);
	}
}

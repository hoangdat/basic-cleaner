
package com.mfluent.asp.cloudstorage.api.sync;

import java.io.InputStream;
import java.util.List;

import org.apache.http.Header;

/**
 * This class is for relaying a cloud storage file response to a proxy server.
 * 
 * @author MFluent
 * @version 1.5
 */
public interface CloudStreamInfo {

	/**
	 * The raw stream from the remote. This is expected to be the file data.
	 * 
	 * @return the input stream
	 */
	InputStream getInputStream();

	/**
	 * the length of the response.
	 * 
	 * @return the content length
	 */
	long getContentLength();

	/**
	 * The content type received from the remote.
	 * 
	 * @return the content type
	 */
	String getContentType();

	/**
	 * Optional headers to return to requester.
	 * 
	 * @return the extra headers
	 */
	List<Header> getExtraHeaders();

	/**
	 * HTTP status response received from remote.
	 * Normal responses are:
	 * 200 OK
	 * 206 Partial (in the case of a range response)
	 * 416 Requested Range Not Satisfiable
	 * 
	 * @return the response code
	 */
	int getResponseCode();

	/**
	 * The response message received from remote.
	 * 
	 * @return the response message
	 */
	String getResponseMessage();

}


package com.mfluent.asp.common.util;

/**
 * Constants can be added to this class but they cannot be removed otherwise previously deployed cloud plugins may fail.
 *
 * @author MFluent
 * @version 1.5
 */
public class CloudStorageConstants {

    public static final String CLOUD_ROOT = "root";

    public static final String PHOTO_FOLDER = "Photos";

    public static final String MUSIC_FOLDER = "Music";

    public static final String VIDEO_FOLDER = "Videos";

    public static final String FILES_FOLDER = "Files";

    public static final String DOCUMENTS_FOLDER = "Documents";
}

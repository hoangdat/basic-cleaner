
package com.mfluent.cloud.googledrive.common;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpHeaders;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpResponse;
import com.mfluent.asp.cloudstorage.api.sync.CloudStreamInfo;
import com.mfluent.log.Log;

import org.apache.http.Header;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class CloudStream implements CloudStreamInfo {

    private final HttpResponse response;

    private final String mimeType;

    private final InputStream contentStream;

    private final List<Header> headerList;

    private final HttpHeaders httpHeaders;

    private CloudFile file;

    public CloudStream(CloudContext context, CloudFile file, String contentRange)
            throws FileNotFoundException, IllegalStateException{
        /* @formatter:on */
        this(context, file.getDownloadURL(), file.getMimeType(), contentRange);
        this.file = file;
    }

    /* @formatter:off */
    public CloudStream(
            CloudContext context,
            String uri,
            String mimeType,
            String contentRange)
            throws FileNotFoundException,
            IllegalStateException{
        /* @formatter:on */
        if (uri == null) {
            throw new FileNotFoundException("uri is null");
        }

        //deleted by shirley.kim
        /*
        HttpGet getRequest = new HttpGet(uri);
		if (contentRange != null) {
			getRequest.addHeader("Range", contentRange);
		}
		*/

        GenericUrl downloadUrl = new GenericUrl(uri);

        Log.i(this, "CloudStream()_1, uri = " + downloadUrl.toURI().toString() + ", contentRange = " + contentRange);

        InputStream contentStream = null;
        HttpResponse httpResponse = null;
        try {

            //modified by shirley.kim
            //httpResponse = context.mDrive.getRequestFactory().buildGetRequest(downloadUrl).execute();
            HttpRequest getRequest = context.mDrive.getRequestFactory().buildGetRequest(downloadUrl);

            if (contentRange != null) {
                HttpHeaders httpHeader = getRequest.getHeaders();
                httpHeader.setRange(contentRange);
                getRequest.setHeaders(httpHeader);
            }

            httpResponse = (getRequest != null) ? getRequest.execute() : null;
            contentStream = httpResponse.getContent();
        } catch (IOException e) {
            Log.e(this, "Exception getting file contents : " + e);
            throw new FileNotFoundException(e.getMessage());
        }

        this.response = httpResponse;
        this.contentStream = contentStream;

        this.headerList = new ArrayList<Header>();
        this.httpHeaders = this.response.getHeaders();

        this.mimeType = mimeType;
    }

    @Override
    public InputStream getInputStream() {
        return this.contentStream;
    }

    @Override
    public long getContentLength() {
        Long length = httpHeaders.getContentLength();
        long contentLength = (length != null) ? length.longValue() : file.length();
        Log.i(this, "contentLength : " + contentLength);

        return contentLength;
    }

    @Override
    public String getContentType() {
        return this.mimeType;
    }

    @Override
    public List<Header> getExtraHeaders() {
        return this.headerList;
    }

    @Override
    public int getResponseCode() {
        // int statusCode = this.response.getStatusLine().getStatusCode();
        int statusCode = this.response.getStatusCode();
        return statusCode;
    }

    @Override
    public String getResponseMessage() {
        // String reasonPhrase = this.response.getStatusLine().getReasonPhrase();
        String reasonPhrase = this.response.getStatusMessage();
        return reasonPhrase;
    }
}

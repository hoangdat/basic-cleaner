
package com.mfluent.cloud.samsungdrive.common;

import android.content.ContentValues;

import com.mfluent.asp.common.datamodel.ASPMediaStore;

import org.apache.commons.lang3.StringUtils;

/**
 * <p>
 * The metadata for a single cloud storage file.
 * </p>
 * <p>
 * This metadata is maintained:
 * <ul>
 * <li>in a uniform, cloud connector independent way by the All Share - Play application, and</li>
 * <li>in a cloud connector dependent way in the cloud storage to which the connector connects.</li>
 * </ul>
 * </p
 * <p>
 * As such, this is the connection between metadata in the All Share - Play application and the cloud storage.
 * </p>
 */
public class Metadata {

    /**
     * <p>
     * The identifier of the (file described by the) metadata.
     * </p>
     * <p>
     * It is composed of:
     * <ul>
     * <li>the identifier (a String), in the cloud storage, of the directory in which the file is contained, and</li>
     * <li>the identifier (a String), in the cloud storage, of the file, not necessarily globally, but certainly within its containing directory.</li>
     * </ul>
     * </p>
     * <p>
     * As a string, the two parts are separated by "//", and is generally referred to as a "sourceMediaId".
     * </p>
     * <p>
     * Each cloud storage connector implementation, i.e., each extension of CloudStorageBase, MUST provide methods that return:
     * <ul>
     * <li>its identifier of a specified CloudDirectory (getMetadataId(CloudDirectory directory)), and</li>
     * <li>its identifier of a specified CloudFile (getMetadataId(CloudFile file)).</li>
     * </ul>
     * </p>
     */
    public static class Id {

        private static final String DIRECTORY_SEPARATOR = CloudContext.PATH_SEPARATOR + CloudContext.PATH_SEPARATOR;

        public final String directoryId;

        public final String fileId;

        public Id(String directoryId, String fileId) {
            this.directoryId = directoryId;
            this.fileId = fileId;
        }

        public Id(String sourceMediaId) {
//jsub12.lee_150721
//			int indexOfSeparator = sourceMediaId.indexOf(DIRECTORY_SEPARATOR);
//			this.directoryId = sourceMediaId.substring(0, indexOfSeparator);
//			this.fileId = sourceMediaId.substring(indexOfSeparator + DIRECTORY_SEPARATOR.length());
            this.directoryId = "";
            this.fileId = sourceMediaId;
        }

        public boolean equalTo(Id that) {
            /* @formatter:off */
            return (StringUtils.equals(this.directoryId, that.directoryId)
                    && StringUtils.equals(this.fileId, that.fileId));
            /* @formatter:on */
        }

        @Override
        public String toString() {
            //return Id.getDirectoryPrefix(this.directoryId) + this.fileId;
            return this.fileId; //jsub12.lee_150721
        }

        public static String getFileIdFromSourceMediaId(String sourceMediaId) {
            int lastIndexOfSlashSlash = sourceMediaId.lastIndexOf(DIRECTORY_SEPARATOR);
            int directorySeparatorLength = DIRECTORY_SEPARATOR.length();
            if (lastIndexOfSlashSlash < 0) {
                lastIndexOfSlashSlash = -directorySeparatorLength;
            }
            String fileId = sourceMediaId.substring(lastIndexOfSlashSlash + directorySeparatorLength);
            return fileId;
        }

        public static String getFilePathFromSourceMediaId(String sourceMediaId) {
            String filePath = sourceMediaId.replace(DIRECTORY_SEPARATOR, CloudContext.PATH_SEPARATOR);
            return filePath;
        }

        public static boolean equals(Id x, Id y) {
            if (x == null && y == null) {
                return true;
            }
            if (x == null || y == null) {
                return false;
            }
            /*
             * at this point x and y are both not null, but a stupid static analyzer can't figure that out, and may complain!!!
			 */
            return x.equalTo(y);

        }

    }

    /**
     * the state of a file and its metadata in the metadata database and the cloud storage
     */
    public enum State {
        /* @formatter:off */
        ADDED,      // this file is in the cloud storage but not the metadata database
        UNSEEN,     // this file is in the metadata database, but not (yet) seen in the cloud
        UNCHANGED,  // this file is in the metadata database and the cloud, and the metadata is the same
        CHANGED,    // this file is in the metadata database and the cloud, and the metadata is NOT the same
        ;
    }
    /* @formatter:on */

    public Metadata.Id id;

    public Metadata.State state;

    public CloudStorageBase.MediaType mediaType;

    public long length;

    public String parentId; //jsub12.lee_150715_2

    public String parentName; //jsub12_150924_1

    public long dateAdded; // this is in MILLISECONDS since the start of the epoch, but stored in the metadata as SECONDS

    public long dateModified; // this is in MILLISECONDS since the start of the epoch, but stored in the metadata as SECONDS

    public String displayName;

    public String mimeType;

    public String thumbnailURI;

    public String fullURI;

    public String fullPath; //jsublee_150907

    public String trashProcessing;

	/*
     * these are the All Share - Play application metadata database media-type dependent columns that are maintained by procedures in this class
	 */

    /* @formatter:off */
    static final String[] audioColumns = {
            ASPMediaStore.BaseASPColumns.SOURCE_MEDIA_ID,
            ASPMediaStore.BaseASPColumns.PARENT_CLOUD_ID, //jsub12.lee_150715_2
            ASPMediaStore.Audio.AudioColumns.DATE_ADDED,
            ASPMediaStore.Audio.AudioColumns.DATE_MODIFIED,
            ASPMediaStore.Audio.AudioColumns.DISPLAY_NAME,
            ASPMediaStore.Audio.AudioColumns.MIME_TYPE,
            ASPMediaStore.Audio.AudioColumns.SIZE,
            ASPMediaStore.Audio.AudioColumns.THUMBNAIL_URI,
            ASPMediaStore.Audio.AudioColumns.FULL_URI,
            ASPMediaStore.Audio.AudioColumns.DATA  //jsublee_150907
    };

    static final String[] imageColumns = {
            ASPMediaStore.BaseASPColumns.SOURCE_MEDIA_ID,
            ASPMediaStore.BaseASPColumns.PARENT_CLOUD_ID, //jsub12.lee_150715_2
            ASPMediaStore.Images.ImageColumns.DATE_ADDED,
            ASPMediaStore.Images.ImageColumns.DATE_MODIFIED,
            ASPMediaStore.Images.ImageColumns.DISPLAY_NAME,
            // ASPMediaStore.Images.ImageColumns.IS_PRIVATE,
//            ASPMediaStore.Images.ImageColumns.LATITUDE,
//            ASPMediaStore.Images.ImageColumns.LONGITUDE,
            ASPMediaStore.Images.ImageColumns.MIME_TYPE,
            // ASPMediaStore.Images.ImageColumns.MINI_THUMB_MAGIC,
            // ASPMediaStore.Images.ImageColumns.ORIENTATION,
            // ASPMediaStore.Images.ImageColumns.PICASA_ID,
            ASPMediaStore.Images.ImageColumns.SIZE,
            // ASPMediaStore.Images.ImageColumns.WIDTH,
            // ASPMediaStore.Images.ImageColumns.HEIGHT,
            ASPMediaStore.Images.ImageColumns.THUMBNAIL_URI,
            ASPMediaStore.Images.ImageColumns.FULL_URI,
            ASPMediaStore.Images.ImageColumns.DATA
    };

    static final String[] videoColumns = {
            ASPMediaStore.BaseASPColumns.SOURCE_MEDIA_ID,
            ASPMediaStore.BaseASPColumns.PARENT_CLOUD_ID, //jsub12.lee_150715_2
            // ASPMediaStore.Video.VideoColumns.BOOKMARK,
            // ASPMediaStore.Video.VideoColumns.CATEGORY,
            ASPMediaStore.Video.VideoColumns.DATE_ADDED,
            ASPMediaStore.Video.VideoColumns.DATE_MODIFIED,
            ASPMediaStore.Video.VideoColumns.DISPLAY_NAME,
            ASPMediaStore.Video.VideoColumns.MIME_TYPE,
            // ASPMediaStore.Video.VideoColumns.MINI_THUMB_MAGIC,
            // ASPMediaStore.Video.VideoColumns.RESOLUTION,
            ASPMediaStore.Video.VideoColumns.SIZE,
            // ASPMediaStore.Video.VideoColumns.TAGS,
            // ASPMediaStore.Video.VideoColumns.WIDTH,
            // ASPMediaStore.Video.VideoColumns.HEIGHT,
            ASPMediaStore.Video.VideoColumns.THUMBNAIL_URI,
            ASPMediaStore.Video.VideoColumns.FULL_URI,
            ASPMediaStore.Video.VideoColumns.DATA
    };

    static final String[] documentColumns = {
            ASPMediaStore.Documents.DocumentColumns.SOURCE_MEDIA_ID,
            ASPMediaStore.BaseASPColumns.PARENT_CLOUD_ID, //jsub12.lee_150715_2
            ASPMediaStore.Documents.DocumentColumns.DATE_ADDED,
            ASPMediaStore.Documents.DocumentColumns.DATE_MODIFIED,
            ASPMediaStore.Documents.DocumentColumns.DISPLAY_NAME,
            ASPMediaStore.Documents.DocumentColumns.MIME_TYPE,
            ASPMediaStore.Documents.DocumentColumns.SIZE,
            // ASPMediaStore.Documents.DocumentColumns.WIDTH,
            // ASPMediaStore.Documents.DocumentColumns.HEIGHT,
            ASPMediaStore.Documents.DocumentColumns.THUMBNAIL_URI,
            ASPMediaStore.Documents.DocumentColumns.FULL_URI,
            ASPMediaStore.Documents.DocumentColumns.DATA //jsublee_150907
    };
    //jsub12.lee_150721
    static final String[] directoryColumns = {
            ASPMediaStore.Documents.DocumentColumns.SOURCE_MEDIA_ID,
            ASPMediaStore.BaseASPColumns.PARENT_CLOUD_ID,
            ASPMediaStore.Documents.DocumentColumns.DATE_ADDED,
            ASPMediaStore.Documents.DocumentColumns.DATE_MODIFIED,
            ASPMediaStore.Documents.DocumentColumns.DISPLAY_NAME,
            ASPMediaStore.Documents.DocumentColumns.MIME_TYPE,
            ASPMediaStore.Documents.DocumentColumns.SIZE,
            //ASPMediaStore.Documents.DocumentColumns.WIDTH,
            //ASPMediaStore.Documents.DocumentColumns.HEIGHT,
            ASPMediaStore.Documents.DocumentColumns.THUMBNAIL_URI,
            ASPMediaStore.Documents.DocumentColumns.FULL_URI,
            ASPMediaStore.Documents.DocumentColumns.DATA,  //jsublee_150907
            ASPMediaStore.TrashColumns.PROCESSING
    };

    //jsub12_150922
    static final String[] defaultColumns = {
            ASPMediaStore.BaseASPColumns.SOURCE_MEDIA_ID,
            ASPMediaStore.BaseASPColumns.PARENT_CLOUD_ID,
            ASPMediaStore.Documents.DocumentColumns.DATE_ADDED,
            ASPMediaStore.Documents.DocumentColumns.DATE_MODIFIED,
            ASPMediaStore.Documents.DocumentColumns.DISPLAY_NAME,
            ASPMediaStore.Documents.DocumentColumns.MIME_TYPE,
            ASPMediaStore.Documents.DocumentColumns.SIZE,
            //ASPMediaStore.Documents.DocumentColumns.WIDTH,
            //ASPMediaStore.Documents.DocumentColumns.HEIGHT,
            ASPMediaStore.Documents.DocumentColumns.THUMBNAIL_URI,
            ASPMediaStore.Documents.DocumentColumns.FULL_URI,
            ASPMediaStore.Documents.DocumentColumns.DATA,
            ASPMediaStore.TrashColumns.PROCESSING
    };
    /* @formatter:on */

    /* @formatter:off */
    public Metadata(
            String id,
            Metadata.State state,
            long length,
            String parentId, //jsub12.lee_150715_2
            String parentName, //jsub12_150924_1
            long dateModified,
            String displayName,
            String mimeType,
            String thumbnailURI,
            String fullPath,
            String trashProcessing) {
        this(new Id(id),
                state,
                length,
                parentId, //jsub12.lee_150715_2
                parentName, //jsub12_150924_1
                dateModified,
                displayName,
                mimeType,
                thumbnailURI,
                fullPath, trashProcessing);
        /* @formatter:on */
    }

    /* @formatter:off */
    public Metadata(
            Metadata.Id id,
            Metadata.State state,
            long length,
            String parentId, //jsub12.lee_150715_2
            String parentName, //jsub12_150924_1
            long dateModified,
            String displayName,
            String mimeType,
            String thumbnailURI,
            String fullPath,
            String trashProcessing) {
        /* @formatter:on */
        if (id == null) {
            throw new RuntimeException("no id specified");
        }
        if (state == null) {
            throw new RuntimeException("no state specified");
        }

        this.state = state;
        this.id = id;
        this.length = length;
        this.parentId = parentId; //jsub12.lee_150715_2
        this.parentName = parentName; //jsub12_150924_1
        this.dateModified = dateModified;
        this.displayName = displayName;
        this.mimeType = mimeType;
        this.thumbnailURI = thumbnailURI;
        this.fullPath = fullPath; //jsublee_150907
        this.trashProcessing = trashProcessing;
    }

    public boolean equalTo(Metadata that) {
        if (this.mediaType != that.mediaType) {
            return false;
        }
        if (this.length != that.length) {
            return false;
        }
        if (!StringUtils.equals(this.parentId, that.parentId)) { //jsub12.lee_150715_2
            return false;
        }

        if (this.dateAdded != that.dateAdded) {
            return false;
        }
        if (this.dateModified != that.dateModified) {
            return false;
        }

        if (!StringUtils.equals(this.displayName, that.displayName)) {
            return false;
        }
        if (!StringUtils.equals(this.mimeType, that.mimeType)) {
            return false;
        }
        if (!StringUtils.equals(this.thumbnailURI, that.thumbnailURI)) {
            return false;
        }
        if (!StringUtils.equals(this.fullURI, that.fullURI)) {
            return false;
        }
        if (!StringUtils.equals(this.fullPath, that.fullPath)) {
            return false;
        }
        return true;
    }

    public ContentValues toContentValues(Integer deviceId) {
        ContentValues values = new ContentValues();
        values.put(ASPMediaStore.BaseASPColumns.DEVICE_ID, deviceId);
        String[] columnNames = CloudStorageBase.MediaType.none.columnNames;
        if (columnNames == null) {
            return null;
        }
        for (int i = 0; i < columnNames.length; i++) {
            String columnName = columnNames[i];
            if (columnName.contentEquals(ASPMediaStore.BaseASPColumns.SOURCE_MEDIA_ID)) {
                values.put(ASPMediaStore.BaseASPColumns.SOURCE_MEDIA_ID, this.id.toString());
            } else if (columnName.contentEquals(ASPMediaStore.BaseASPColumns.PARENT_CLOUD_ID)) {  //jsub12.lee_150715_2
                values.put(ASPMediaStore.BaseASPColumns.PARENT_CLOUD_ID, this.parentId);
            } else if (columnName.contentEquals(ASPMediaStore.Audio.AudioColumns.DATE_ADDED)) {
                // also ASPMediaStore.Images.ImageColumns.DATE_ADDED and
                // ASPMediaStore.Video.VideoColumns.DATE_ADDED
                values.put(ASPMediaStore.Audio.AudioColumns.DATE_ADDED, this.dateAdded);
            } else if (columnName.contentEquals(ASPMediaStore.Audio.AudioColumns.DATE_MODIFIED)) {
                // also ASPMediaStore.Images.ImageColumns.DATE_MODIFIED and
                // ASPMediaStore.Video.VideoColumns.DATE_MODIFIED
                values.put(ASPMediaStore.Audio.AudioColumns.DATE_MODIFIED, this.dateModified);
            } else if (columnName.contentEquals(ASPMediaStore.Audio.AudioColumns.DISPLAY_NAME)) {
                // also ASPMediaStore.Images.ImageColumns.DISPLAY_NAME and
                // ASPMediaStore.Video.VideoColumns.DISPLAY_NAME
                values.put(ASPMediaStore.Audio.AudioColumns.DISPLAY_NAME, this.displayName);
            } else if (columnName.contentEquals(ASPMediaStore.Audio.AudioColumns.MIME_TYPE)) {
                // also ASPMediaStore.Images.ImageColumns.MIME_TYPE and
                // ASPMediaStore.Video.VideoColumns.MIME_TYPE
                values.put(ASPMediaStore.Audio.AudioColumns.MIME_TYPE, this.mimeType);
            } else if (columnName.contentEquals(ASPMediaStore.Audio.AudioColumns.SIZE)) {
                // also ASPMediaStore.Images.ImageColumns.SIZE and
                // ASPMediaStore.Video.VideoColumns.SIZE
                values.put(ASPMediaStore.Audio.AudioColumns.SIZE, this.length);
            } else if (columnName.contentEquals(ASPMediaStore.Audio.AudioColumns.THUMBNAIL_URI)) {
                // also ASPMediaStore.Images.ImageColumns.THUMBNAIL_URI and
                // ASPMediaStore.Video.VideoColumns.THUMBNAIL_URI
                values.put(ASPMediaStore.Audio.AudioColumns.THUMBNAIL_URI, this.thumbnailURI);
            } else if (columnName.contentEquals(ASPMediaStore.Audio.AudioColumns.DATA)) {  //jsublee_150907
                values.put(ASPMediaStore.Audio.AudioColumns.DATA, this.fullPath);
            }
        }
        return values;
    }
}

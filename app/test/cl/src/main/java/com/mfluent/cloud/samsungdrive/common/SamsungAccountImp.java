package com.mfluent.cloud.samsungdrive.common;

import com.mfluent.log.Log;
import com.msc.sa.aidl.ISAService;
import com.msc.sa.aidl.ISACallback;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;

import platform.com.mfluent.asp.ui.SignInOutUtils;


/**
 * Created by seongsu.yoon on 2016-09-30.
 */

public class SamsungAccountImp {

    private static final String TAG = SamsungAccountImp.class.getSimpleName();
    private static final String EXPIRED_ACCESS_TOKEN = "expired_access_token";
    private static final String ACCESS_TOKEN = "access_token";
    public static final String USER_ID = "user_id";
    public static final String COUNTRY_CODE = "cc";
    public static final String ERROR_CODE = "error_code";
    private final String appid;
    private final String appSecret;
    private ServiceConnection serviceConnection;
    private ISAService service = null;
    private String registrationCode = null;

    public static final class ErrorCode {
        public static final String AUTH_TOKEN_EXPIRED = "SAC_0402";
    }

    public interface IResultListener {
        void onResult(final Bundle result);
    }

    public SamsungAccountImp(final String appid, final String appSecret) {
        this.appid = appid;
        this.appSecret = appSecret;
    }

    public static String getAccountToken(final Bundle accountBundle) {
        return accountBundle.getString(ACCESS_TOKEN, "");
    }

    public static String getUserId(final Bundle accountBundle) {
        return accountBundle.getString(USER_ID, "");
    }

    public static String getCountryCode(final Bundle accountBundle) {
        return accountBundle.getString(COUNTRY_CODE, "");
    }

    public static String getErrorCode(final Bundle accountBundle) {
        return accountBundle.getString(ERROR_CODE, "");
    }

    private void unRegistCallback() throws RemoteException {
        if (service != null && service.unregisterCallback(registrationCode)) {
            Log.i(TAG, "===== UNREGISTER CALLBACK SUCCESS =====");
        } else {
            Log.i(TAG, "===== UNREGISTER CALLBACK FAIL =====");
        }
        service = null;
    }

    public boolean request(final Context context, final boolean isNewToken, final String currentToken, final IResultListener listener) {
        Log.i(TAG, "bindToSA");

        final Intent intent = new Intent("com.msc.action.samsungaccount.REQUEST_SERVICE");
        intent.setPackage(SignInOutUtils.AccountType_SamsungAccount);

        serviceConnection = new ServiceConnection() {

            @Override
            public void onServiceConnected(ComponentName name, IBinder binder) {
                service = ISAService.Stub.asInterface(binder);

                if (service == null) {
                    Log.i(TAG, "===== CANNOT BIND TO SAMSUNG ACCOUNT =====");
                    Bundle result = new Bundle();
                    result.putInt(DriveConstants.RCODE, DriveConstants.ResultCode.FAIL);
                    listener.onResult(result);
                } else {
                    Log.i(TAG, "===== SUCCESS BIND TO SAMSUNG ACCOUNT =====");
                    AccountManager am = AccountManager.get(context);
                    if (am != null) {
                        try {
                            final Account[] accounts = am.getAccountsByType(SignInOutUtils.AccountType_SamsungAccount);
                            if (accounts == null || accounts.length < 1) {
                                Log.i(TAG, "NO SAMSUNG ACCOUNT");
                                Bundle result = new Bundle();
                                result.putInt(DriveConstants.RCODE, DriveConstants.ResultCode.FAIL);
                                listener.onResult(result);
                                return;
                            }
                        } catch (SecurityException e) {
                            Log.e(TAG, "onServiceConnected() - SecurityException : " + e);
                            return;
                        }
                    }
                }

                Log.i(TAG, "===== REGISTER CALLBACK TO SAMSUNG ACCOUNT =====");
                try {
                    if (service != null) {
                        String pakcageName = context.getPackageName();
                        registrationCode = service.registerCallback(appid, appSecret, pakcageName, new SACallback(context, listener));

                        if (registrationCode == null) {
                            Log.e(TAG, "===== REGISTER CALLBACK TO SAMSUNG ACCOUNT FAIL =====");
                            Bundle result = new Bundle();
                            result.putInt(DriveConstants.RCODE, DriveConstants.ResultCode.FAIL);
                            listener.onResult(result);
                        } else {
                            Bundle data = new Bundle();
                            final String[] additionalData = {USER_ID, COUNTRY_CODE};

                            if (isNewToken) {
                                Log.i(TAG, "===== REQUEST NEW ACCESS TOKEN =====");
                                data.putString(EXPIRED_ACCESS_TOKEN, currentToken);
                                Log.d(TAG, "Expired token inf : " + currentToken);
                            } else {
                                Log.i(TAG, "===== REQUEST ACCESS TOKEN =====");
                            }
                            data.putStringArray("additional", additionalData);
                            service.requestAccessToken(this.hashCode(), registrationCode, data);
                        }
                    }
                } catch (RemoteException e) {
                    Log.e(TAG, "request() - RemoteException : " + e);
                    Bundle result = new Bundle();
                    result.putInt(DriveConstants.RCODE, DriveConstants.ResultCode.FAIL);
                    listener.onResult(result);
                }
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                Log.i(TAG, "===== onServiceDisconnected =====");
                service = null;
            }
        };

        return context.getApplicationContext().bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
    }

    private class SACallback extends ISACallback.Stub {
        final IResultListener listener;

        public SACallback(Context context, final IResultListener listener) {
            this.listener = listener;
        }

        @Override
        public void onReceiveAccessToken(int requestID, boolean isSuccess, Bundle resultData) throws RemoteException {
            if (isSuccess) {
                Log.i(TAG, "===== RECEIVED ACCESSTOKEN, USERID, COUNTRY CODE =====");
                resultData.putInt(DriveConstants.RCODE, DriveConstants.ResultCode.SUCCESS);
            } else {
                resultData.putInt(DriveConstants.RCODE, DriveConstants.ResultCode.FAIL);
                String mErrorCode = resultData.getString("error_code");
                String mErrorMessage = resultData.getString("error_message");
                Log.e(TAG, "===== RECEIVED ACCSSTOKEN FAIL : " + mErrorCode + " - " + mErrorMessage);
            }
            unRegistCallback();
            listener.onResult(resultData);
        }

        @Override
        public void onReceiveChecklistValidation(int requestID, boolean isSuccess, Bundle resultData) throws RemoteException {
            unRegistCallback();
            listener.onResult(resultData);
        }

        @Override
        public void onReceiveDisclaimerAgreement(int requestID, boolean isSuccess, Bundle resultData) throws RemoteException {
            unRegistCallback();
            listener.onResult(resultData);
        }

        @Override
        public void onReceiveAuthCode(int requestID, boolean isSuccess, Bundle resultData) throws RemoteException {
            unRegistCallback();
            listener.onResult(resultData);
        }

        @Override
        public void onReceiveSCloudAccessToken(int requestID, boolean isSuccess, Bundle resultData) throws RemoteException {
        }
    }

}

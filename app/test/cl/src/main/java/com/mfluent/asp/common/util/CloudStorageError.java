package com.mfluent.asp.common.util;

import com.samsung.android.sdk.slinkcloud.CloudGatewayFileBrowserUtils;

import org.apache.commons.lang3.StringUtils;

/**
 * Created by sev_user on 2/24/2017.
 */
public class CloudStorageError {

    public static final String RETRY_MAX_ERROR = "retry to the max";

    public static final String NO_NEED_RETRY_ERROR = "no need retry";

    public static final String OUT_OF_STORAGE_ERROR = "out_of_storage";

    public static final String REACH_MAX_ITEM_ERROR = "reach_max_item";

    public static int getExceptionError(Exception e) {
        int retType = CloudGatewayFileBrowserUtils.ERROR_NONE;
        String exceptionName = e.getClass().toString();
        String errorMsg = e.getMessage();
        if (exceptionName.contains("IOException")) {
            if (StringUtils.contains(errorMsg, CloudStorageError.OUT_OF_STORAGE_ERROR)) {
                retType = CloudGatewayFileBrowserUtils.ERROR_OUT_OF_STORAGE;
            } else if (StringUtils.contains(errorMsg, CloudStorageError.REACH_MAX_ITEM_ERROR)) {
                retType = CloudGatewayFileBrowserUtils.ERROR_REACH_MAX_ITEM;
            }
        }

        return retType;
    }
}

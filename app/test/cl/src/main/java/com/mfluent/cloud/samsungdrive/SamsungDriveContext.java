package com.mfluent.cloud.samsungdrive;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.text.TextUtils;

import com.mfluent.asp.cloudstorage.api.sync.CloudStreamInfo;
import com.mfluent.asp.common.datamodel.ASPMediaStore;
import com.mfluent.asp.common.util.CloudStorageConstants;
import com.mfluent.asp.common.util.MimeType;
import com.mfluent.cloud.samsungdrive.common.CloudContext;
import com.mfluent.cloud.samsungdrive.common.CloudDirectory;
import com.mfluent.cloud.samsungdrive.common.CloudFile;
import com.mfluent.cloud.samsungdrive.common.CloudStorageBase;
import com.mfluent.cloud.samsungdrive.common.DatabaseHelper;
import com.mfluent.cloud.samsungdrive.common.DriveConstants;
import com.mfluent.log.Log;
import com.samsung.android.sdk.SsdkUnsupportedException;
import com.samsung.android.sdk.scloud.decorator.drive.DriveFile;
import com.samsung.android.sdk.scloud.exception.SamsungCloudException;
import com.samsung.android.sdk.slinkcloud.CloudGatewayMediaStore;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.regex.Pattern;

public class SamsungDriveContext extends CloudContext {
    public HashMap<String, CloudFile> cloudFilesMap_cache = null; //jsublee_150907
    public Queue<CloudDirectory> cloudDirListQToSync = null;
    Drive mDrive = null;

    public SamsungDriveContext() {
        cloudFilesMap_cache = new HashMap<>();
    }

    public Drive getDrive(Context appContext) {
        Log.d(this, "getSamsungDrive() " + mDrive + " " + DriveConstants.apiClient.pushAppId + " " + DriveConstants.apiClient.pushName + " " + DriveConstants.apiClient.pushToken + " " + DriveConstants.apiClient.cid);

        if (mDrive == null) {
            try {
                mDrive = new Drive(appContext, DriveConstants.APPID, appContext.getPackageName(), DriveConstants.sCountryCode, DriveConstants.apiClient);
            } catch (SsdkUnsupportedException e) {
                Log.d(this, e.getMessage());
            } catch (SamsungCloudException e) {
                Log.d(this, "getDrvie() - SamsungCloudException : " + e.getMessage());
                cloudStorage.handleTokenException();
            }
        }
        return mDrive;
    }

    private CloudDirectory makeParent(DriveFile file) {
        Log.d(this, "makeParent()");
        if (file == null || file.parent == null) {
            return null;
        }
        CloudDirectory parentDir = new CloudDirectory(this, null, null);
        parentDir.setCloudId(file.parent);
        return parentDir;
    }

    public ContentValues toContentValues(CloudDirectory parentDir, DriveFile file) {
        ContentValues values = new ContentValues();

        String path = "/" + file.name;
        if ((parentDir != null) && !parentDir.getPath().equals("/") &&
                !parentDir.getPath().contains(CloudGatewayMediaStore.Trash.Trash_ID)) {
            path = parentDir.getPath() + path;
        }

        values.put(ASPMediaStore.BaseASPColumns.DEVICE_ID, cloudStorage.getDeviceId());
        values.put(ASPMediaStore.BaseASPColumns.SOURCE_MEDIA_ID, file.fileId);
        values.put(ASPMediaStore.BaseASPColumns.PARENT_CLOUD_ID, file.parent);
        values.put(ASPMediaStore.Audio.AudioColumns.DATE_MODIFIED, (file.modifiedTime / 1000));
        values.put(ASPMediaStore.Audio.AudioColumns.DISPLAY_NAME, file.name);
        values.put(ASPMediaStore.Audio.AudioColumns.MIME_TYPE, file.isFolder() ? MimeType.DIR_MIMETYPE : file.mimeType);
        values.put(ASPMediaStore.Audio.AudioColumns.SIZE, file.size);
        values.put(ASPMediaStore.Audio.AudioColumns.DATA, path);
        values.put(ASPMediaStore.TrashColumns.LIST_SHOWN, file.restoreAllowed);
        values.put(ASPMediaStore.TrashColumns.TRASHED, file.trashed);
        values.put(ASPMediaStore.TrashColumns.PROCESSING, file.processing);
        values.put(DatabaseHelper.HASH, file.hash);

        return values;
    }

    private void applyDB(ArrayList<ContentValues> contentValuesList) {
        getDatabaseHelper().newInsert(DatabaseHelper.FILE_TABLE, contentValuesList);
        contentValuesList.clear();
    }

    public boolean putDBforFullSync(CloudDirectory parentDir, List<DriveFile> fileList) {
        // TODO : when pagination support, first time check is needed
        if (parentDir != null)
            cloudStorage.deleteChildrenFromDB(parentDir.getCloudId());
        ArrayList<ContentValues> contentValuesList = new ArrayList<>();

        for (DriveFile dFile : fileList) {
            if (!cloudStorage.isSignedIn()) {
                return false;
            }
            ContentValues values = toContentValues(parentDir, dFile);
            contentValuesList.add(values);

            if (contentValuesList.size() >= CloudStorageBase.BULK_OPERATION_LIMIT) {
                applyDB(contentValuesList);
            }
        }
        if (!cloudStorage.isSignedIn()) {
            return false;
        }

        if (contentValuesList.size() > 0) {
            applyDB(contentValuesList);
        }

        // In the case of tree traversal,
        // I'll use the cloudFilesMap_cache as the collection of the sync completed directories.
        if (parentDir != null)
            cloudFilesMap_cache.put(parentDir.getCloudId(), parentDir);

        return true;
    }

    public void putDBforDeltaSync(ArrayList<DriveFile> changedFiles) {
        ArrayList<ContentValues> contentValuesList = new ArrayList<>();
        final String where = ASPMediaStore.MediaColumns.SOURCE_MEDIA_ID + "=?";

        for (DriveFile file : changedFiles) {
            if (file.permanentlyDeleted) {
                deleteDB(file.type, file.fileId);
            } else {
                if (TextUtils.equals(file.type, DriveFile.FOLDER)) {
                    CloudDirectory pDir = new CloudDirectory(this, null, null);
                    pDir.setCloudId(file.parent);
                    CloudDirectory curDir = new CloudDirectory(this, pDir, file.name);
                    curDir.setCloudId(file.fileId);
                    curDir.setParentCloudId(file.parent);
                    curDir.setRoot(file.isRoot());
                    getDatabaseHelper().findOrSaveDirectory(curDir);
                }
                try (Cursor cursor = getDatabaseHelper().query(DatabaseHelper.FILE_TABLE,
                        new String[]{ASPMediaStore.MediaColumns.SOURCE_MEDIA_ID}, where, new String[]{file.fileId}, null)) {
                    if ((cursor != null) && (cursor.moveToFirst())) {
                        getDatabaseHelper().update(DatabaseHelper.FILE_TABLE, where, new String[]{file.fileId},
                                toContentValues(makeParent(file), file));
                    } else {
                        contentValuesList.add(toContentValues(makeParent(file), file));
                    }
                }
            }
        }
        if (contentValuesList.size() > 0) {
            applyDB(contentValuesList);
        }
        changedFiles.clear();
    }

    /**
     * class for transfer the path item to update after rename
     */
    private static class PathItem {
        String fileId;
        String filePath;

        PathItem(String id, String path) {
            fileId = id;
            filePath = path;
        }
    }

    /**
     * call for update path of all children of file after rename
     *
     * @param file a file need to update
     */
    public void updatePathRenamed(DriveFile file) {
        if (file == null || file.fileId == null || TextUtils.equals(file.type, DriveFile.FILE)) {
            return;
        }

        CloudDirectory parentDir = new CloudDirectory(this, null, null);
        parentDir.setCloudId(file.fileId);
        String parentPath = null;
        if ((parentDir != null) && !parentDir.getPath().equals("/") &&
                !parentDir.getPath().contains(CloudGatewayMediaStore.Trash.Trash_ID)) {
            parentPath = parentDir.getPath();
        }

        Queue<PathItem> cloudIdQToUpdate = new LinkedList<>();
        PathItem pathItem = new PathItem(file.fileId, parentPath);
        cloudIdQToUpdate.add(pathItem);

        PathItem tmpPathItem;
        while (cloudIdQToUpdate != null && !cloudIdQToUpdate.isEmpty() && (tmpPathItem = cloudIdQToUpdate.remove()) != null) {
            if (tmpPathItem.fileId == null || tmpPathItem.filePath == null) {
                continue;
            }

            String where = ASPMediaStore.BaseASPColumns.PARENT_CLOUD_ID + " =?";
            try (Cursor cursor = getDatabaseHelper().query(DatabaseHelper.FILE_TABLE, null, where, new String[]{tmpPathItem.fileId}, null)) {
                if (cursor != null && cursor.moveToFirst()) {
                    while (!cursor.isAfterLast()) {
                        String name = cursor.getString(cursor.getColumnIndex(ASPMediaStore.Audio.AudioColumns.DISPLAY_NAME));
                        String sourceMediaId = cursor.getString(cursor.getColumnIndex(ASPMediaStore.BaseASPColumns.SOURCE_MEDIA_ID));
                        String mimeType = cursor.getString(cursor.getColumnIndex(ASPMediaStore.Audio.AudioColumns.MIME_TYPE));
                        if (name != null && sourceMediaId != null) {
                            ContentValues values = new ContentValues();
                            String updatePath = tmpPathItem.filePath + "/" + name;
                            values.put(ASPMediaStore.Audio.AudioColumns.DATA, updatePath);
                            updateDB(DatabaseHelper.TABLE_TYPE.FILE, sourceMediaId, values);
                            cursor.moveToNext();

                            boolean bIsDir = (mimeType != null) && mimeType.equals(MimeType.DIR_MIMETYPE);
                            if (bIsDir) {
                                PathItem newPath = new PathItem(sourceMediaId, updatePath);
                                cloudIdQToUpdate.add(newPath);
                            }
                        }
                    }
                }
            }
        }
    }

    // To list all files in the root folder, use the alias "root" as the value for folderId.
    public List<DriveFile> getChildFiles(String folderId) throws IOException {
        return null;
    }

    public CloudDirectory getDirectoryFromQtoSync() {
        synchronized (cloudDirListQToSync) {
            Log.i(this, "getDirectoryFromQtoSync()");
            if (!cloudDirListQToSync.isEmpty()) {
                return cloudDirListQToSync.remove();
            } else {
                Log.i(this, "getDirectoryFromQtoSync(), Q has no item.");
                return null;
            }
        }
    }

    @Override
    public boolean loadDirectoryFiles(CloudFile file, AddFileListener listener) throws Exception {
        Log.d(this, "loadDirectoryFiles() called");

        if (file == null) {
            Log.d(this, "loadDirectoryFiles() - file = null, return false");
            return false;
        }

        CloudDirectory directory = (CloudDirectory) file;
        Log.d(this, "loadDirectoryFiles() - file path = " + file.getPath() + ", file name = : " + file.getName());

        List<DriveFile> filelist = getChildFiles(directory.isRoot() ? CloudStorageConstants.CLOUD_ROOT : directory.getCloudId());
        Log.d(this, "loadDirectoryFiles() - filelist is : " + filelist);

        return (filelist != null);
    }

    public void deleteDB(String fileType, String fileId) {
        final String where = ASPMediaStore.MediaColumns.SOURCE_MEDIA_ID + "=?";

        getDatabaseHelper().delete(DatabaseHelper.FILE_TABLE, where, new String[]{fileId});
        if (TextUtils.equals(fileType, DriveFile.FOLDER)) {
            getDatabaseHelper().delete(DatabaseHelper.FOLDER_TABLE, where, new String[]{fileId});
        }
    }

    public void updateDB(DatabaseHelper.TABLE_TYPE type, String fileId, ContentValues values) {
        String selection = ASPMediaStore.MediaColumns.SOURCE_MEDIA_ID + "=?";
        switch (type) {
            case FILE:
                getDatabaseHelper().update(DatabaseHelper.FILE_TABLE, selection, new String[]{fileId}, values);
                break;
            case FOLDER:
                getDatabaseHelper().update(DatabaseHelper.FOLDER_TABLE, selection, new String[]{fileId}, values);
                break;
            case BOTH:
                getDatabaseHelper().update(DatabaseHelper.FILE_TABLE, selection, new String[]{fileId}, values);
                getDatabaseHelper().update(DatabaseHelper.FOLDER_TABLE, selection, new String[]{fileId}, values);
                break;
        }
    }

    public void insertDB(DatabaseHelper.TABLE_TYPE type, DriveFile parentDir, DriveFile file) {
        Log.d(this, "insertDB() - file : " + file.fileId + " " + file.name);

        ArrayList<ContentValues> fileList = new ArrayList<>();
        fileList.add(toContentValues(makeParent(file), file));

        CloudDirectory pDir = new CloudDirectory(this, null, parentDir.name);
        pDir.setCloudId(parentDir.fileId);
        CloudDirectory curDir = new CloudDirectory(this, pDir, file.name);
        curDir.setCloudId(file.fileId);
        curDir.setParentCloudId(parentDir.fileId);
        curDir.setRoot(file.isRoot());

        switch (type) {
            case FILE:
                getDatabaseHelper().newInsert(DatabaseHelper.FILE_TABLE, fileList);
                break;
            case FOLDER:
                getDatabaseHelper().addDirectory(curDir);
                break;
            case BOTH:
                getDatabaseHelper().addDirectory(curDir);
                getDatabaseHelper().newInsert(DatabaseHelper.FILE_TABLE, fileList);
                break;
        }
    }

    @Override
    public CloudStreamInfo getFile(String fileId, String contentRange) throws FileNotFoundException/*, CloudUnauthorizedException*/ {
        return null;
    }

    // file or directory name can't contain these characters. If so, they'll be replaced with INVALID_REPLACE_WITH
    private static Pattern INVALID_CHARS_PATTERN = Pattern.compile("[\\Q/\\<>:\"*?|\\E]");

    // file or directory name can't equal any of these strings
    private static List<String> INVALID_LIST = Arrays.asList("AUX",
            "CON",
            "COM0",
            "COM1",
            "COM2",
            "COM3",
            "COM4",
            "COM5",
            "COM6",
            "COM7",
            "COM8",
            "COM9",
            "LPT0",
            "LPT1",
            "LPT2",
            "LPT3",
            "LPT4",
            "LPT5",
            "LPT6",
            "LPT7",
            "LPT8",
            "LPT9",
            "NUL",
            "PRN");

    /**
     * santize filename according to Cloud Storage restrictions.
     * eg: http://windows.microsoft.com/en-GB/skydrive/upload-file-cant-faq#
     * prevent any files that:
     * - begin or end with a space, end with a period, or include any of these characters: / \ < > : * " ? |
     * - or equal: AUX, PRN, NUL, CON, COM0, COM1, COM2, COM3, COM4, COM5, COM6, COM7, COM8, COM9, LPT0, LPT1, LPT2, LPT3, LPT4, LPT5, LPT6, LPT7, LPT8, LPT9
     */
    @Override
    public String sanitizeFileName(String filename) {
        if (filename == null) {
            return null;
        }
        // replace any invalid characters with INVALID_REPLACE_WITH
        String clean = INVALID_CHARS_PATTERN.matcher(filename.trim()).replaceAll(CloudContext.INVALID_REPLACE_WITH);

        // make sure no period at the end
        if (clean.endsWith(".")) {
            // find last non-period and cut string up to that point
            int i = clean.length() - 2; // NOTE: -2 because we already know last character is "."
            for (; i >= 0; i--) {
                if (clean.charAt(i) != '.') {
                    break;
                }
            }
            if (i >= 0) {
                clean = clean.substring(0, i + 1);
            } else {
                // filename is all periods! ie: "...." - just create file/directory with INVALID_REPLACE_WITH
                clean = CloudContext.INVALID_REPLACE_WITH;
            }
        }

        // check for invalid file/dir names
        if (INVALID_LIST.contains(clean)) {
            // NOTE: we could fail here or rename file/directory.
            // Since user can't rename files in ASP, I'm renaming to allow this to succeed
            clean += CloudContext.INVALID_REPLACE_WITH;
        }

        // log only if filename has changed (don't even do string compare if logging not enabled)
        if (!clean.equals(filename)) {
            Log.d(this, "sanitizeFileName() - \"" + filename + "\" -> \"" + clean + "\"");
        }
        return clean;
    }
}

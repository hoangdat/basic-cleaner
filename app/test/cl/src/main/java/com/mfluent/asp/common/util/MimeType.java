
package com.mfluent.asp.common.util;

import java.util.HashMap;

public class MimeType {

	public static final HashMap<String, String> EXTENSION_TO_MIME_TYPE_MAP = new HashMap<String, String>();

	static {
		EXTENSION_TO_MIME_TYPE_MAP.put("EML", "message/rfc822");
		EXTENSION_TO_MIME_TYPE_MAP.put("MP3", "audio/mpeg");
		EXTENSION_TO_MIME_TYPE_MAP.put("M4A", "audio/mp4");
		EXTENSION_TO_MIME_TYPE_MAP.put("WAV", "audio/x-wav");
		EXTENSION_TO_MIME_TYPE_MAP.put("AMR", "audio/amr");
		EXTENSION_TO_MIME_TYPE_MAP.put("AWB", "audio/amr-wb");
		EXTENSION_TO_MIME_TYPE_MAP.put("WMA", "audio/x-ms-wma");
		EXTENSION_TO_MIME_TYPE_MAP.put("OGG", "audio/ogg");
		EXTENSION_TO_MIME_TYPE_MAP.put("OGA", "audio/ogg");
		EXTENSION_TO_MIME_TYPE_MAP.put("AAC", "audio/aac");
		EXTENSION_TO_MIME_TYPE_MAP.put("3GA", "audio/3gpp");
		EXTENSION_TO_MIME_TYPE_MAP.put("FLAC", "audio/flac");
		EXTENSION_TO_MIME_TYPE_MAP.put("MPGA", "audio/mpeg");
		EXTENSION_TO_MIME_TYPE_MAP.put("MP4_A", "audio/mp4");
		EXTENSION_TO_MIME_TYPE_MAP.put("3GP_A", "audio/3gpp");
		EXTENSION_TO_MIME_TYPE_MAP.put("3G2_A", "audio/3gpp2");
		EXTENSION_TO_MIME_TYPE_MAP.put("ASF_A", "audio/x-ms-asf");
		EXTENSION_TO_MIME_TYPE_MAP.put("3GPP_A", "audio/3gpp");
		EXTENSION_TO_MIME_TYPE_MAP.put("MID", "audio/midi");
		EXTENSION_TO_MIME_TYPE_MAP.put("XMF", "audio/midi");
		EXTENSION_TO_MIME_TYPE_MAP.put("MXMF", "audio/midi");
		EXTENSION_TO_MIME_TYPE_MAP.put("RTTTL", "audio/midi");
		EXTENSION_TO_MIME_TYPE_MAP.put("SMF", "audio/sp-midi");
		EXTENSION_TO_MIME_TYPE_MAP.put("IMY", "audio/imelody");
		EXTENSION_TO_MIME_TYPE_MAP.put("MIDI", "audio/midi");
		EXTENSION_TO_MIME_TYPE_MAP.put("RTX", "audio/midi");
		EXTENSION_TO_MIME_TYPE_MAP.put("OTA", "audio/midi");
		EXTENSION_TO_MIME_TYPE_MAP.put("PYA", "audio/vnd.ms-playready.media.pya");
		EXTENSION_TO_MIME_TYPE_MAP.put("M4B", "audio/mp4");
		EXTENSION_TO_MIME_TYPE_MAP.put("QCP", "audio/qcelp");
		EXTENSION_TO_MIME_TYPE_MAP.put("MPEG", "video/mpeg");
		EXTENSION_TO_MIME_TYPE_MAP.put("MPG", "video/mpeg");
		EXTENSION_TO_MIME_TYPE_MAP.put("MP4", "video/mp4");
		EXTENSION_TO_MIME_TYPE_MAP.put("M4V", "video/mp4");
		EXTENSION_TO_MIME_TYPE_MAP.put("3GP", "video/3gpp");
		EXTENSION_TO_MIME_TYPE_MAP.put("3GPP", "video/3gpp");
		EXTENSION_TO_MIME_TYPE_MAP.put("3G2", "video/3gpp2");
		EXTENSION_TO_MIME_TYPE_MAP.put("3GPP2", "video/3gpp2");
		EXTENSION_TO_MIME_TYPE_MAP.put("WMV", "video/x-ms-wmv");
		EXTENSION_TO_MIME_TYPE_MAP.put("ASF", "video/x-ms-asf");
		EXTENSION_TO_MIME_TYPE_MAP.put("AVI", "video/avi");
		EXTENSION_TO_MIME_TYPE_MAP.put("DIVX", "video/divx");
		EXTENSION_TO_MIME_TYPE_MAP.put("FLV", "video/flv");
		EXTENSION_TO_MIME_TYPE_MAP.put("MKV", "video/mkv");
		EXTENSION_TO_MIME_TYPE_MAP.put("SDP", "application/sdp");
		EXTENSION_TO_MIME_TYPE_MAP.put("RM", "video/mp4");
		EXTENSION_TO_MIME_TYPE_MAP.put("RMVB", "video/mp4");
		EXTENSION_TO_MIME_TYPE_MAP.put("MOV", "video/quicktime");
		EXTENSION_TO_MIME_TYPE_MAP.put("PYV", "video/vnd.ms-playready.media.pyv");
		EXTENSION_TO_MIME_TYPE_MAP.put("ISMV", "video/ismv");
		EXTENSION_TO_MIME_TYPE_MAP.put("SKM", "video/skm");
		EXTENSION_TO_MIME_TYPE_MAP.put("K3G", "video/k3g");
		EXTENSION_TO_MIME_TYPE_MAP.put("AK3G", "video/ak3g");
		EXTENSION_TO_MIME_TYPE_MAP.put("WEBM", "video/webm");
		EXTENSION_TO_MIME_TYPE_MAP.put("JPG", "image/jpeg");
		EXTENSION_TO_MIME_TYPE_MAP.put("JPEG", "image/jpeg");
		EXTENSION_TO_MIME_TYPE_MAP.put("MY5", "image/vnd.tmo.my5");
		EXTENSION_TO_MIME_TYPE_MAP.put("GIF", "image/gif");
		EXTENSION_TO_MIME_TYPE_MAP.put("PNG", "image/png");
		EXTENSION_TO_MIME_TYPE_MAP.put("BMP", "image/x-ms-bmp");
		EXTENSION_TO_MIME_TYPE_MAP.put("WBMP", "image/vnd.wap.wbmp");
		EXTENSION_TO_MIME_TYPE_MAP.put("MPO", "image/mpo");
		EXTENSION_TO_MIME_TYPE_MAP.put("M3U", "audio/x-mpegurl");
		EXTENSION_TO_MIME_TYPE_MAP.put("PLS", "audio/x-scpls");
		EXTENSION_TO_MIME_TYPE_MAP.put("WPL", "application/vnd.ms-wpl");
		EXTENSION_TO_MIME_TYPE_MAP.put("PDF", "application/pdf");
		EXTENSION_TO_MIME_TYPE_MAP.put("RTF", "application/msword");
		EXTENSION_TO_MIME_TYPE_MAP.put("DOC", "application/msword");
		EXTENSION_TO_MIME_TYPE_MAP.put("DOCX", "application/vnd.openxmlformats-officedocument.wordprocessingml.document");
		EXTENSION_TO_MIME_TYPE_MAP.put("DOT", "application/msword");
		EXTENSION_TO_MIME_TYPE_MAP.put("DOTX", "application/vnd.openxmlformats-officedocument.wordprocessingml.template");
		EXTENSION_TO_MIME_TYPE_MAP.put("XLS", "application/vnd.ms-excel");
		EXTENSION_TO_MIME_TYPE_MAP.put("XLSX", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		EXTENSION_TO_MIME_TYPE_MAP.put("XLT", "application/vnd.ms-excel");
		EXTENSION_TO_MIME_TYPE_MAP.put("XLTX", "application/vnd.openxmlformats-officedocument.spreadsheetml.template");
		EXTENSION_TO_MIME_TYPE_MAP.put("PPT", "application/vnd.ms-powerpoint");
		EXTENSION_TO_MIME_TYPE_MAP.put("PPTX", "application/vnd.openxmlformats-officedocument.presentationml.presentation");
		EXTENSION_TO_MIME_TYPE_MAP.put("POT", "application/vnd.ms-powerpoint");
		EXTENSION_TO_MIME_TYPE_MAP.put("POTX", "application/vnd.openxmlformats-officedocument.presentationml.template");
		EXTENSION_TO_MIME_TYPE_MAP.put("TXT", "text/plain");
		EXTENSION_TO_MIME_TYPE_MAP.put("GUL", "application/jungumword");
		EXTENSION_TO_MIME_TYPE_MAP.put("EPUB", "application/epub+zip");
		EXTENSION_TO_MIME_TYPE_MAP.put("ACSM", "application/vnd.adobe.adept+xml");
		EXTENSION_TO_MIME_TYPE_MAP.put("SWF", "application/x-shockwave-flash");
		EXTENSION_TO_MIME_TYPE_MAP.put("SVG", "image/svg+xml");
		EXTENSION_TO_MIME_TYPE_MAP.put("DCF", "application/vnd.oma.drm.content");
		EXTENSION_TO_MIME_TYPE_MAP.put("ODF", "application/vnd.oma.drm.content");
		EXTENSION_TO_MIME_TYPE_MAP.put("APK", "application/vnd.android.package-archive");
		EXTENSION_TO_MIME_TYPE_MAP.put("JAD", "text/vnd.sun.j2me.app-descriptor");
		EXTENSION_TO_MIME_TYPE_MAP.put("JAR", "application/java-archive ");
		EXTENSION_TO_MIME_TYPE_MAP.put("VCS", "text/x-vCalendar");
		EXTENSION_TO_MIME_TYPE_MAP.put("ICS", "text/x-vCalendar");
		EXTENSION_TO_MIME_TYPE_MAP.put("VTS", "text/x-vtodo");
		EXTENSION_TO_MIME_TYPE_MAP.put("VCF", "text/x-vcard");
		EXTENSION_TO_MIME_TYPE_MAP.put("VNT", "text/x-vnote");
		EXTENSION_TO_MIME_TYPE_MAP.put("HTML", "text/html");
		EXTENSION_TO_MIME_TYPE_MAP.put("HTM", "text/html");
		EXTENSION_TO_MIME_TYPE_MAP.put("XML", "application/xhtml+xml");
		EXTENSION_TO_MIME_TYPE_MAP.put("WGT", "application/vnd.samsung.widget");
		EXTENSION_TO_MIME_TYPE_MAP.put("HWP", "application/x-hwp");
		EXTENSION_TO_MIME_TYPE_MAP.put("SNB", "application/snb");
		EXTENSION_TO_MIME_TYPE_MAP.put("GOLF", "image/golf");

	}

	public static final String DIR_MIMETYPE = "vnd.asp.dir/dir";
}

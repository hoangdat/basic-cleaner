package com.mfluent.cloud.samsungdrive.common;

import android.database.Cursor;
import android.text.TextUtils;

import com.mfluent.asp.common.clouddatamodel.AbsCloudFile;
import com.mfluent.asp.common.datamodel.ASPMediaStore;
import com.mfluent.asp.common.util.CloudStorageConstants;
import com.mfluent.log.Log;

public class CloudFile extends AbsCloudFile {
    protected final CloudDirectory mParent;
    protected boolean mTrashed;
    protected boolean mListShown;
    protected String mTrashProcessing;

    protected final CloudContext mContext;

    public CloudFile(CloudContext context, CloudDirectory parent, String name) {
        super(name);
        mParent = parent;
        mContext = context;
    }

    public CloudFile(CloudFile file) {
        super(file);
        mContext = file.mContext;
        mParent = file.mParent;
    }

    public void copyFromFile(CloudFile file) {
        super.copyFromFile(file);

        mTrashed = file.mTrashed;
        mListShown = file.mListShown;
        mTrashProcessing = file.mTrashProcessing;
    }

    public CloudDirectory getParent() {
        return mParent;
    }

    public CloudContext getContext() {
        return mContext;
    }

    @Override
    public boolean isDirectory() {
        return this instanceof CloudDirectory;
    }

    public boolean isListShown() {
        return mListShown;
    }

    public void setListShown(boolean listShown) {
        mListShown = listShown;
    }

    public boolean isTrashed() {
        return mTrashed;
    }

    public void setTrashed(boolean trashed) {
        mTrashed = trashed;
    }

    public String getTrashProcessing() {
        return mTrashProcessing;
    }

    public void setTrashProcessing(String trashProcessing) {
        mTrashProcessing = trashProcessing;
    }

    @Override
    public String getPath() {
        Log.d(this, "getPath() start ");
        String path;
        StringBuilder pathBuilder = new StringBuilder();
        String nameFromDb = null;
        String parentPath = null;
        if (mParentCloudId != null) {
            parentPath = getParentPath(mParentCloudId);
        } else if (mCloudId != null){
            String parentId = null;
            Cursor cursor = null;
            try {
                cursor = getCursorRow(mCloudId);
                if (cursor != null && cursor.moveToFirst()) {
                    parentId = cursor.getString(cursor.getColumnIndex(ASPMediaStore.BaseASPColumns.PARENT_CLOUD_ID));
                    if (mName == null) {
                        nameFromDb = cursor.getString(cursor.getColumnIndex(ASPMediaStore.Audio.AudioColumns.DISPLAY_NAME));
                    }
                }
            } catch (Exception e) {
                Log.d(this, "getPath exception: " + e.getMessage());
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }

            if (parentId != null) {
                parentPath = getParentPath(parentId);
            }
        }

        if (parentPath != null) {
            pathBuilder = pathBuilder.append(parentPath);
        }

        pathBuilder = pathBuilder.append(CloudContext.PATH_SEPARATOR);

        if (mName != null) {
            pathBuilder = pathBuilder.append(mName);
        } else if (nameFromDb != null) {
            pathBuilder = pathBuilder.append(nameFromDb);
        }

        path = pathBuilder.toString();

        return path;
    }

    private Cursor getCursorRow(String cloudId) {
        Log.d(this, "getCursorRow()");
        if (cloudId == null) {
            return null;
        }
        Cursor cursor;

        DatabaseHelper db = mContext.getDatabaseHelper();
        String[] columns = {ASPMediaStore.Audio.AudioColumns.DISPLAY_NAME,
                            ASPMediaStore.BaseASPColumns.PARENT_CLOUD_ID};
        String selection = ASPMediaStore.BaseASPColumns.SOURCE_MEDIA_ID + "=?";
        cursor = db.query(DatabaseHelper.FILE_TABLE, columns, selection, new String[] {cloudId}, null);

        return cursor;
    }

    private String getParentPath(String parentId) {
        Log.d(this, "getParentPath");
        if (parentId == null && TextUtils.equals(parentId, CloudStorageConstants.CLOUD_ROOT)) {
            return null;
        }

        String parentPath = null;
        DatabaseHelper db = mContext.getDatabaseHelper();
        String[] columns = {ASPMediaStore.Audio.AudioColumns.DATA};
        String selection = ASPMediaStore.BaseASPColumns.SOURCE_MEDIA_ID + "=?";
        try (Cursor pathCursor = db.query(DatabaseHelper.FILE_TABLE, columns, selection, new String[]{parentId}, null)) {
            if (pathCursor != null && pathCursor.moveToFirst()) {
                parentPath = pathCursor.getString(pathCursor.getColumnIndex(ASPMediaStore.Audio.AudioColumns.DATA));
            }
        }
        return parentPath;
    }
}
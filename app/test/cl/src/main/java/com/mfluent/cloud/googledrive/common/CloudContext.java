package com.mfluent.cloud.googledrive.common;

import com.google.api.services.drive.Drive;
import com.mfluent.asp.common.clouddatamodel.AbsCloudContext;

/**
 * base class for contexts of cloud storage.
 *
 * @author jrenkel
 */
public abstract class CloudContext extends AbsCloudContext {
    private DatabaseHelper mDatabaseHelper;
    protected CloudStorageBase<? extends CloudContext> cloudStorage;
    public Drive mDrive = null;

	/*
     * abstract methods that must be implemented by subclasses
	 */

    public void setCloudStorage(CloudStorageBase<? extends CloudContext> _cloudStorage) {
        cloudStorage = _cloudStorage;
    }

	public interface AddFileListener {

		/**
		 * add a file to the directory. <br>
		 * <br>
		 * this method is called for each file in the directory specified to the loadDIrectoryFiles() method.
		 * It is possible that the call will return a substitute for the given file with an instance
		 * that is already in memory. The returned value should only be used after this call.
		 *
		 * @param file
		 *            the file to be added to the directory
		 * @param directory
		 *            the directory to which the file is to be added
		 */
		CloudFile addFile(CloudFile file, CloudDirectory directory);

		void removeFile(CloudFile file, CloudDirectory directory);

		void removeFile(CloudFile file);

	}

    public boolean loadDirectoryFiles(CloudDirectory directory) throws Exception {
        return loadDirectoryFiles(directory, directory);
    }

    public abstract boolean loadDirectoryFiles(CloudFile file, AddFileListener listener) throws Exception;

    public DatabaseHelper getDatabaseHelper() {
        return mDatabaseHelper;
    }

    public void setDatabaseHelper(DatabaseHelper databaseHelper) {
        mDatabaseHelper = databaseHelper;
    }
}

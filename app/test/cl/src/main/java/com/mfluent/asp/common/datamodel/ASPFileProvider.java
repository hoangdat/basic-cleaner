
package com.mfluent.asp.common.datamodel;

import android.content.Context;

import java.io.FileNotFoundException;
import java.io.IOException;

public interface ASPFileProvider {

    Context getApplicationContext();

    CloudDevice getCloudDevice();

    /**
     * get the storage gateway file ID for a file stored in a cloud storage.
     *
     * @param file the file whose storage gateway file id is to be gotten
     * @return the storage gateway file ID for the file
     * @throws FileNotFoundException
     */
    String getStorageGatewayFileId(ASPFile file) throws FileNotFoundException;

    /**
     * Delete file.
     *
     * @param sortType                  TODO
     * @param directoryStorageGatewayId TODO
     * @return true if the file was deleted successfully
     */
    int deleteFiles(String directoryStorageGatewayId, ASPFileSortType sortType, String... storageGatewayFileIdsToDelete)
            throws InterruptedException, IOException;

    /**
     * gets a cloud storage file browser for the directory specified by a storage gateway ID. <br/>
     *
     * @param storageGatewayID the storage gateway ID for which to get a file browser
     * @return the cloud storage file browser
     * @throws IOException
     * @throws InterruptedException
     */
    ASPFileBrowser<?> getCloudStorageFileBrowser(String storageGatewayID, ASPFileSortType sort, boolean forceReload, boolean isBrowserForView)
            throws InterruptedException, IOException;

    /**
     * gets the count of child included in the file. <br/>
     *
     * @param file the file
     * @return the count of child
     */
    int getCountOfChild(ASPFile file);


    /**
     * gets the count of child directory included in the file. <br/>
     *
     * @param file the file
     * @return the count of child directory
     */
    int getCountOfChildDir(ASPFile file);

    /**
     * gets the count of descendants included in the file. <br/>
     *
     * @param file the file
     * @return the count of child
     */
    int getCountOfDescendants(ASPFile file);


    /**
     * gets the count of descendant directories included in the file. <br/>
     *
     * @param file the file
     * @return the count of child directory
     */
    int getCountOfDescendantDir(ASPFile file);

    // gets the processing status
    String getTrashProcessingDir(ASPFile file);
}

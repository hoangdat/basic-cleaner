/**
 * 
 */

package com.mfluent.asp.common.io.util;

import java.io.IOException;
import java.io.OutputStream;

/**
 * @author Ilan Klinghofer
 */
public class ProgressUpdatingOutputStream extends OutputStream {

	private final OutputStream mOutstream;
	private final StreamProgressListener mStreamProgressListener;

	public ProgressUpdatingOutputStream(OutputStream outstream, StreamProgressListener streamProgressListener) {
		mOutstream = outstream;

		if (streamProgressListener == null) {
			streamProgressListener = new NullStreamProgressListener();
		}
		mStreamProgressListener = streamProgressListener;
	}

	@Override
	public void write(int b) throws IOException {
		mOutstream.write(b);
		mStreamProgressListener.bytesTransferred(1);
	}

	@Override
	public void write(byte[] b) throws IOException {
		mOutstream.write(b);
		mStreamProgressListener.bytesTransferred(b.length);
	}

	@Override
	public void write(byte[] b, int off, int len) throws IOException {
		mOutstream.write(b, off, len);
		mStreamProgressListener.bytesTransferred(len);
	}

	@Override
	public void flush() throws IOException {
		mOutstream.flush();
	}

	@Override
	public void close() throws IOException {
		mOutstream.close();
	}
}

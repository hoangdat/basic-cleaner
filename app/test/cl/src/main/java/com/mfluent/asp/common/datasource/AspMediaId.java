/**
 *
 */

package com.mfluent.asp.common.datasource;

import com.mfluent.asp.common.datamodel.ASPMediaStore;

/**
 * @author michaelgierach
 */
public class AspMediaId {
    public static final int MEDIA_TYPE_IMAGE = ASPMediaStore.MediaTypes.IMAGE;
    public static final int MEDIA_TYPE_VIDEO = ASPMediaStore.MediaTypes.VIDEO;
    public static final int MEDIA_TYPE_AUDIO = ASPMediaStore.MediaTypes.AUDIO;
    public static final int MEDIA_TYPE_ALBUM = ASPMediaStore.MediaTypes.ALBUM;
    public static final int MEDIA_TYPE_CAPTION = ASPMediaStore.MediaTypes.CAPTION;
    public static final int MEDIA_TYPE_CAPTION_INDEX = ASPMediaStore.MediaTypes.CAPTION_INDEX;
    public static final int MEDIA_TYPE_DOCUMENT = ASPMediaStore.MediaTypes.DOCUMENT;
    public static final int MEDIA_TYPE_DIRECTORY = ASPMediaStore.MediaTypes.DIRECTORY; //jsub12.lee_150717
    public static final int MEDIA_TYPE_NONE = ASPMediaStore.MediaTypes.NONE; // other filetype

    private AspMediaId() {
    }
}

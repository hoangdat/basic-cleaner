/**
 * 
 */

package com.mfluent.asp.common.io.util;

/**
 * @author Ilan Klinghofer
 */
public interface StreamProgressListener {

	/**
	 * Notifies this listener that more bytes have been transferred.
	 * 
	 * @param numberOfBytes
	 *            The (incremental) number of bytes that were transferred since the last call to bytesTransferred
	 */
	void bytesTransferred(long numberOfBytes);
}

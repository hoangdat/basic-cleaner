package com.mfluent.cloud.samsungdrive.samsungpushplatform;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.mfluent.log.Log;

import platform.com.mfluent.asp.util.DeviceUtilSLPF;

/**
 * Data of Samsung Push Service could be deleted by user.  In that case, registration information of apps is deleted also and they no more get push message from server.
 * So, whenever receiving data clear event of Samsung Push Service, then try registration again.
 */
public class SppDataClearReceiver extends BroadcastReceiver {
    public static final String SPP_PKG_NAME = "com.sec.spp.push";

    @Override
    public void onReceive(Context context, Intent intent) {
        String pkgName = intent.getData().getSchemeSpecificPart();
        if (!SPP_PKG_NAME.equals(pkgName)) {
            return;
        }

        boolean dataRemoved = intent.getBooleanExtra(Intent.EXTRA_DATA_REMOVED, false);
        if (dataRemoved) {
            Log.d(this, "SPP data is removed");
            SppManager.registerSppReceiver(context, DeviceUtilSLPF.SAMSUNGDRIVE_WEBSTORAGE_NAME, context);
        }
    }
}

package com.mfluent.cloud.samsungdrive.common;

import android.os.Bundle;

import com.samsung.android.sdk.scloud.client.ApiClient;

/**
 * Created by seongsu.yoon on 2016-10-04.
 */

public class DriveConstants {
    public static final String APPID = "gc4z299bi4";
    public static final String APP_SECRET = "A9DFBE5A1BF6BE4955486E9B36074C7C";
    public static final String UID = "U8W1YDSu2y";

    public static final String RCODE = "rcode";
    public static final String CLIENT_ID = "client_id";
    public static final String CLIENT_SECRET = "client_secret";
    public static final String MYPACKAGE = "mypackage";
    public static final String OSP_VER = "OSP_VER";
    public static final String MODE = "MODE";
    public static final String ADDITIONAL = "additional";


    public interface ResultCode {
        int SUCCESS = 1;
        int FAIL = 0;
        int CANCEL = 2;
    }

    public static boolean sIsTokenExpired = false;
    public static String sCountryCode = "XX";

    public static final ApiClient apiClient = new ApiClient();

    public static void setConstants(Bundle bundle, boolean isTokenExpired) {
        apiClient.accessToken = SamsungAccountImp.getAccountToken(bundle);
        apiClient.uid = SamsungAccountImp.getUserId(bundle);
        apiClient.cid = UID;
        sCountryCode = SamsungAccountImp.getCountryCode(bundle);
        sIsTokenExpired = isTokenExpired;
    }
}

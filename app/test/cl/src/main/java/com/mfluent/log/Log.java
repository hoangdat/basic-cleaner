package com.mfluent.log;

import java.io.File;

/**
 * Created by seongsu.yoon on 2016-04-24.
 */
final public class Log {
    public static final String SL_DEBUG_PATH = "/mnt/sdcard/SL_DEBUG";

    public static final int VERBOSE = android.util.Log.VERBOSE; /* 2 */
    public static final int DEBUG = android.util.Log.DEBUG; /* 3 */
    public static final int INFO = android.util.Log.INFO; /* 4 */
    public static final int WARN = android.util.Log.WARN; /* 5 */
    public static final int ERROR = android.util.Log.ERROR; /* 6 */
    public static final int PRIVATE_INFO = 99;

    private static boolean IS_LOGGABLE = true;
    private static final int MAX_INDEX = 9999;
    private static final String CLOUD_GATEWAY_TAG = "CloudGateway";
    private static int mLogIndex = 0;
    private static final boolean DEBUGGABLE = android.os.Debug.semIsProductDev();

    public enum CloudType {
        GoogleDrive,
        SamsungDrive
    }

    public static boolean isLoggable() {
        boolean bRet = false;

        try {
            File f = new File(SL_DEBUG_PATH);
            android.util.Log.d("CloudGateway", "isLoggable() - SL_DEBUG_EXIST = " + f.exists());
            if (f.exists() || DEBUGGABLE) {
                bRet = true;
            }
        } catch (Exception e) {
            android.util.Log.w("CloudGateway", "isLoggable() - Exception occurred : " + e.getMessage());
        }

        return bRet;
    }

    private static int getLogIndex() {
        mLogIndex++;

        if (mLogIndex > MAX_INDEX) {
            mLogIndex = 0;
        }

        return mLogIndex;
    }

    private static void log(String tag, String msg, int priority) {
        if (IS_LOGGABLE) {
            switch (priority) {
                case VERBOSE:
                    android.util.Log.v(CLOUD_GATEWAY_TAG, getMsg(tag, msg, null));
                    break;
                case DEBUG:
                    android.util.Log.d(CLOUD_GATEWAY_TAG, getMsg(tag, msg, null));
                    break;
                case INFO:
                    android.util.Log.i(CLOUD_GATEWAY_TAG, getMsg(tag, msg, null));
                    break;
                case WARN:
                    android.util.Log.w(CLOUD_GATEWAY_TAG, getMsg(tag, msg, null));
                    break;
                case ERROR:
                    android.util.Log.e(CLOUD_GATEWAY_TAG, getMsg(tag, msg, null));
                    break;
            }
        }
    }

    private static void log(Object obj, String msg, int priority) {
        if (IS_LOGGABLE) {
            switch (priority) {
                case VERBOSE:
                    android.util.Log.v(CLOUD_GATEWAY_TAG, getMsg(obj, msg));
                    break;
                case DEBUG:
                    android.util.Log.d(CLOUD_GATEWAY_TAG, getMsg(obj, msg));
                    break;
                case INFO:
                    android.util.Log.i(CLOUD_GATEWAY_TAG, getMsg(obj, msg));
                    break;
                case WARN:
                    android.util.Log.w(CLOUD_GATEWAY_TAG, getMsg(obj, msg));
                    break;
                case ERROR:
                    android.util.Log.e(CLOUD_GATEWAY_TAG, getMsg(obj, msg));
                    break;
            }
        }
    }

    private static String getMsg(Object obj, String msg) {
        String tag = null;
        CloudType cloudType = null;

        if (obj != null) {
            tag = obj.getClass().getSimpleName();

            cloudType = getCloudType(obj);
        }

        return getMsg(tag, msg, cloudType);
    }

    private static String getMsg(String tag, String msg, CloudType cloudType) {
        StringBuffer sb = new StringBuffer();

        if (tag != null) {
            if (cloudType != null) {
                String cloudTag = setCloudTag(cloudType);
                sb.append(String.format("[%04d/%-5s/%-20s] ", getLogIndex(), cloudTag, tag));
            } else {
                sb.append(String.format("[%04d/%-20s] ", getLogIndex(), tag));
            }

        } else {
            sb.append(String.format("[%04d] ", getLogIndex()));
        }

        sb.append(msg);
        return sb.toString();
    }

    public static void v(String tag, String msg) {
        log(tag, msg, VERBOSE);
    }

    public static void d(String tag, String msg) {
        log(tag, msg, DEBUG);
    }

    public static void i(String tag, String msg) {
        log(tag, msg, INFO);
    }

    public static void w(String tag, String msg) {
        log(tag, msg, WARN);
    }

    public static void e(String tag, String msg) {
        log(tag, msg, ERROR);
    }

    public static void privateLog(String tag, String msg) {
        log(tag, msg, PRIVATE_INFO);
    }

    public static void v(Object obj, String msg) {
        log(obj, msg, VERBOSE);
    }

    public static void d(Object obj, String msg) {
        log(obj, msg, DEBUG);
    }

    public static void i(Object obj, String msg) {
        log(obj, msg, INFO);
    }

    public static void w(Object obj, String msg) {
        log(obj, msg, WARN);
    }

    public static void e(Object obj, String msg) {
        log(obj, msg, ERROR);
    }

    public static String filter(String privateInfo) {
        String ret = "##HIDE LOG##";
        if (!DEBUGGABLE) {
            ret = privateInfo;
        }

        return ret;
    }

    private static CloudType getCloudType(Object obj) {
        CloudType cloudType = null;

        if (obj != null) {
            String packageName = obj.getClass().getName();
            if (packageName != null) {
                if (packageName.indexOf("oogle") > 0) {
                    cloudType = CloudType.GoogleDrive;
                } else if (packageName.indexOf("amsung") > 0) {
                    cloudType = CloudType.SamsungDrive;
                }
            }
        }

        return cloudType;
    }

    private static String setCloudTag(CloudType cloudType) {
        String tag = "";

        if (cloudType != null) {
            switch (cloudType) {
                case GoogleDrive:
                    tag = "/GoogleDrive";
                    break;
                case SamsungDrive:
                    tag = "/SamsungDrive";
                    break;
                default:
                    tag = "";
                    break;
            }
        }

        return tag;
    }
}

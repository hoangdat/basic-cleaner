package com.mfluent.asp.common.clouddatamodel;

import com.mfluent.asp.cloudstorage.api.sync.CloudStorageSync;
import com.mfluent.asp.cloudstorage.api.sync.CloudStreamInfo;
import com.mfluent.cloud.googledrive.exception.CloudUnauthorizedException;
import com.mfluent.log.Log;

import java.io.FileNotFoundException;

public abstract class AbsCloudContext {
    public static final String PATH_SEPARATOR = "/";

    public static final String INVALID_REPLACE_WITH = "~";

    private String userId = null;

    private String email;

    protected String mAccessToken;

    protected String accessSecret;

    AccessTokenListener accessTokenListener = null;

    public abstract CloudStreamInfo getFile(String filePath, String contentRange)
            throws FileNotFoundException, IllegalStateException, CloudUnauthorizedException;

    public void reset() {
        userId = null;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return this.userId;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    /**
     * Santize filename according to any specific Cloud Storage restrictions
     * By default, only checks for PATH_SEPARATOR and replaces it with INVALID_REPLACE_WITH
     */
    public String sanitizeFileName(String filename) {
        if (filename == null) {
            return null;
        }
        String clean = filename.replace(PATH_SEPARATOR, INVALID_REPLACE_WITH);

        // log only if filename has changed (don't even do string compare if logging not enabled)
        if (clean.equals(filename) == false) {
            Log.d(this, "santizeFilename : \"" + filename + "\" -> \"" + clean + "\"");
        }
        return clean;
    }

    public interface AccessTokenListener {

        void newToken(String token, String secret);
    }

    /**
     * Sets the access token and secret.
     *
     * @param token  the token
     * @param secret the secret
     * @return TODO
     */
    public boolean setAndCheckAccessTokenAndSecret(String token, String secret) {
        Log.v(this, "access token : " + token + "; secret : " + secret);
        mAccessToken = token;
        accessSecret = secret;
        if (accessTokenListener != null) {
            accessTokenListener.newToken(token, secret);
        }
        boolean authorized = checkAccessToken();
        setAndCheckAuthorizationState(authorized ? AuthorizationState.YES : AuthorizationState.NO);
        return authorized;
    }

    public boolean checkAccessToken() {
        return mAccessToken != null;
    }

    public enum AuthorizationState {
        DUNNO(CloudStorageSync.CLOUD_AUTHENTICATION_UNKNOWN),
        NO(CloudStorageSync.CLOUD_AUTHENTICATION_FAILURE),
        YES(CloudStorageSync.CLOUD_AUTHENTICATION_SUCCESS);

        public final String broadcast;

        AuthorizationState(String broadcast) {
            this.broadcast = broadcast;
        }

    }

    private AuthorizationState authorizationState = AuthorizationState.DUNNO;

    /**
     * set and check the authorization state of this connector
     *
     * @param authorizationState the authorization state of this connector
     * @return true if this connector is authorized; else false
     */
    public boolean setAndCheckAuthorizationState(AuthorizationState authorizationState) {
        this.authorizationState = authorizationState;
        return checkAuthorization();
    }

    /**
     * get the authorization state of this connector
     *
     * @return the authorization state of this connector
     */
    public AuthorizationState getAuthorizationState() {
        return authorizationState;
    }

    /**
     * check that this connector is authorized
     *
     * @return true if this connector is authorized; else false
     */
    public boolean checkAuthorization() {
        return authorizationState == AuthorizationState.YES;
    }
}

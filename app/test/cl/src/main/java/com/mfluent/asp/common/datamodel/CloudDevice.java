
package com.mfluent.asp.common.datamodel;

import android.content.ContentResolver;
import android.content.IntentFilter;

public interface CloudDevice {

    String BROADCAST_DEVICE_STATE_CHANGE = "cloudmanager.com.mfluent.asp.datamodel.Device.BROADCAST_DEVICE_STATE_CHANGE.CLOUDLINK";

    String BROADCAST_DEVICE_NOW_ONLINE = "cloudmanager.com.mfluent.asp.datamodel.Device.BROADCAST_DEVICE_NOW_ONLINE.CLOUDLINK";

    String BROADCAST_DEVICE_REFRESH = "cloudmanager.com.mfluent.asp.datamodel.Device.BROADCAST_DEVICE_REFRESH.CLOUDLINK";

    String BROADCAST_DEVICE_SIGNOUT_RESPONSE = "cloudmanager.com.mfluent.asp.datamodel.Device.BROADCAST_DEVICE_SIGNOUT_RESPONSE.CLOUDLINK";

    String DEVICE_ID_EXTRA_KEY = "DEVICE_ID_EXTRA_KEY";

    // extra details in BROADCAST_DEVICE_REFRESH to indicate where user is in app (optional)
    String REFRESH_FROM_KEY = "REFRESH_FROM_KEY";
    int REFRESH_FROM_UNKNOWN = -1;
    //	int REFRESH_FROM_FILES = 4; // files tab -> refresh
    // sent with ContentAggregatorService.ACTION_REFRESH_DEVICES message
    int REFRESH_FROM_HOME = 5; // home/devices -> refresh
    int REFRESH_FROM_RECENT = 6; // recent content -> refresh
    int REFRESH_FROM_LAUNCH = 7; // app launch (from login activity)
    int REFRESH_NEW_ACCOUNT = 8; // app has new SS account (from AccessManager)

    int getId();

    void setWebStorageUserId(String webStorageUserId);

    String getWebStorageUserId();

    void setWebStorageEncryptedUserId(String encryptedId);

    String getWebStorageEncryptedUserId();

    void setWebStorageSignedIn(boolean isWebStorageSignedIn);

    boolean isWebStorageSignedIn();

    void deleteAllMetaData(ContentResolver cr);

    /**
     * Total storage capacity of the device in bytes
     *
     * @return total capacity of the storage device
     */
    long getCapacityInBytes();

    /**
     * Total capacity of the device in bytes
     *
     */
    void setCapacityInBytes(long capacityInBytes);

    /**
     * Currently used storage capacity of the device in bytes
     *
     * @return size in bytes
     */
    long getUsedCapacityInBytes();

    /**
     * Used capacity of the device in bytes
     *
     */
    void setUsedCapacityInBytes(long usedCapacityInBytes);

    IntentFilter buildDeviceIntentFilterForAction(String action);

    boolean getIsInUserRecovering();

    void setIsInUserRecovering(boolean bFlag);
}

/*******************************************************************************
 * mFLuent LLC. CONFIDENTIAL PROPRIETARY
 * Copyright (c) 2010, mFLuent LLC. All rights reserved.
 * Use, distribution, or disclosure of this file or its contents in full or in
 * part is prohibited without prior written authorization from mFluent LLC.
 ******************************************************************************/

package com.mfluent.asp.common.util;

import android.os.BadParcelableException;
import android.os.Parcel;
import android.os.Parcelable;

public final class IntVector implements Parcelable {

    private final static int INT_VECTOR_PARCEL_VERSION = 1;

    public static final Parcelable.Creator<IntVector> CREATOR = new Parcelable.Creator<IntVector>() {

        @Override
        public IntVector createFromParcel(Parcel source) {
            int version = source.readInt();

            if (version > INT_VECTOR_PARCEL_VERSION) {
                throw new BadParcelableException("Invalid version in parcel: " + version);
            }
            IntVector result = new IntVector();

            result.m_sorted = source.readByte() == 1;
            result.m_increment = source.readInt();
            int size = source.readInt();
            result.m_unique = source.readByte() == 1;
            result.m_sorted = source.readByte() == 1;
            result.ensureCapacity(size);
            for (int i = 0; i < size; i++) {
                result.m_array[i] = source.readInt();
            }
            result.m_index = size;

            return result;
        }

        @Override
        public IntVector[] newArray(int size) {
            return new IntVector[size];
        }

    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(INT_VECTOR_PARCEL_VERSION);

        dest.writeByte((byte) (this.m_sorted ? 1 : 0));

        dest.writeInt(this.m_increment);
        dest.writeInt(this.m_index);
        dest.writeByte((byte) (this.m_unique ? 1 : 0));
        dest.writeByte((byte) (this.m_sorted ? 1 : 0));
        for (int i = 0; i < this.m_index; i++) {
            dest.writeInt(this.m_array[i]);
        }
    }

    private int m_increment;
    private int[] m_array;
    private int m_index;
    private boolean m_sorted;
    private boolean m_unique = true;

    /**
     * @param initialCapacity
     * @param growthIncrement
     * @param sorted
     * @param unique
     */
    public IntVector(int initialCapacity, int growthIncrement, boolean sorted, boolean unique) {
        this.m_increment = growthIncrement;
        this.m_array = new int[initialCapacity];
        this.m_sorted = sorted;
        this.m_unique = unique;
    }

    /**
     * Creates a new IntVector with the given int[] and growthIncrement and
     * sorting disabled.
     *
     * @param ints            the int[] to use when creating the vector. null is allowed.
     * @param growthIncrement How much the vector will grom when it runs out of space
     */
    public IntVector(int[] ints, int growthIncrement) {
        if (ints != null) {
            this.m_array = ints;
        } else {
            this.m_array = new int[0];
        }
        this.m_increment = growthIncrement;
        this.m_sorted = false;
        this.m_index = this.m_array.length;
    }

    private IntVector() {
    }

    public static int size(IntVector vector) {
        int size = 0;
        if (vector != null) {
            size = vector.size();
        }

        return size;
    }

    public static boolean contains(IntVector vector, int value) {
        boolean result = false;
        if (vector != null) {
            result = vector.contains(value);
        }

        return result;
    }

    @Override
    public IntVector clone() {
        int[] newArray = null;
        if (this.m_array != null && this.m_index > 0) {
            newArray = new int[this.m_index];
            System.arraycopy(this.m_array, 0, newArray, 0, this.m_index);
        }

        IntVector newIntVector = new IntVector(newArray, this.m_increment);
        newIntVector.m_sorted = this.m_sorted;
        newIntVector.m_unique = this.m_unique;

        return newIntVector;
    }

    /**
     * Adds this int to the end of the vector.
     * WARNING: This can easily break sorting!
     *
     * @param newInt The int to append
     */
    public void append(int newInt) {

        if (this.m_index == this.m_array.length) {
            grow();
        }
        this.m_array[this.m_index] = newInt;
        this.m_index++;
    }

    public void ensureCapacity(int capacity) {
        int arrayLength = 0;
        if (this.m_array != null) {
            arrayLength = this.m_array.length;
        }
        if ((this.m_array == null) || (arrayLength < capacity)) {
            int diff = capacity - arrayLength;
            int newSize = arrayLength + ((diff / this.m_increment) * this.m_increment) + this.m_increment;

            int[] newArray = new int[newSize];
            if ((this.m_index > 0) && (this.m_array != null)) {
                System.arraycopy(this.m_array, 0, newArray, 0, this.m_index);
            }
            this.m_array = newArray;
        }
    }

    /**
     * Inserts this int into the correct position in the vector.
     *
     * @param newInt
     * @return position where the item was inserted, otherwise -1 if unsuccessful
     */
    public int insert(int newInt) {
        int pos;
        if (this.m_sorted) {
            pos = search(newInt);
            if (this.m_unique && pos > -1 && this.m_array[pos] == newInt) {
                return -1;
            }
            pos++;
        } else {
            pos = this.m_index;
        }

        if (insertElementAt(newInt, pos)) {
            return pos;
        } else {
            return -1;
        }
    }

    public boolean insertElementAt(int element, int pos) {
        if (this.m_index == this.m_array.length) {
            grow();
        }

        if (pos < this.m_index) {
            System.arraycopy(this.m_array, pos, this.m_array, pos + 1, this.m_index - pos);
        }
        this.m_array[pos] = element;
        this.m_index++;
        return true;
    }

    /**
     * Check if the vector contains this int.
     *
     * @param target The int to look for
     * @return Whether the int was found
     */
    public boolean contains(int target) {
        int pos = search(target);
        return (pos > -1) && (this.m_array[pos] == target);
    }

    /**
     * Functions similar to contains, but will return
     * the index where the item occurs.
     *
     * @param target
     * @return -1 if target not found. Otherwise the index
     * where it is.
     */
    public int find(int target) {
        int pos = search(target);

        if ((pos > -1) && (this.m_array[pos] == target)) {
            return pos;
        } else {
            return -1;
        }
    }

    /**
     * Removes the int from this vector. If this is an unsorted vector, only one
     * instance of the given integer will be removed.
     *
     * @return Whether an element was actually removed.
     */
    public boolean remove(int target) {
        int pos = search(target);
        if (pos < 0 || this.m_array[pos] != target) {
            return false;
        }

        return removeElementAt(pos);
    }

    public int remove(IntVector other) {
        int numRemoved = 0;

        if (other == this) {
            numRemoved = this.m_index;
            removeAll();
        } else {
            for (int i = 0; i < other.m_index; i++) {
                if (this.remove(other.m_array[i])) {
                    numRemoved++;
                }
            }
        }

        return numRemoved;
    }

    public boolean removeElementAt(int index) {
        boolean result = false;

        if ((index >= 0) && (index < this.m_index)) {
            if (index < this.m_index) {
                System.arraycopy(this.m_array, index + 1, this.m_array, index, this.m_index - index - 1);
            }
            this.m_index--;
            result = true;
        }

        return result;
    }

    /**
     * Empties the vector.
     */
    public void removeAll() {
        this.m_index = 0;
    }

    /**
     * The number of ints the vector currently holds
     */
    public int size() {
        return this.m_index;
    }

    /**
     * Gets the element at a specified index.
     *
     * @param index
     * @return
     */
    public int elementAt(int index) {
        if (index < 0 || index >= this.m_index) {
            throw new ArrayIndexOutOfBoundsException("Can't read element at index " + index + ". size=" + this.m_index);
        }

        return this.m_array[index];
    }

    private void grow() {
        int[] tmp = new int[this.m_array.length + this.m_increment];
        System.arraycopy(this.m_array, 0, tmp, 0, this.m_array.length);
        this.m_array = tmp;
    }

    /**
     * Finds a given int's postion in the array, or one less if not found.
     *
     * @param target
     * @return
     */
    private int search(int target) {
        if (this.m_sorted) {
            if (this.m_index == 0) {
                return -1;
            }

            int high = this.m_index - 1, low = 0, probe;

            if (this.m_array[high] == target) {
                return high;
            } else if (this.m_array[high] < target) {
                return high;
            } else if (this.m_array[low] == target) {
                return low;
            } else if (this.m_array[low] > target) {
                return -1;
            }
            while (high - low > 1) {
                probe = (high + low) >>> 1;
                if (this.m_array[probe] == target) {
                    return probe;
                } else if (this.m_array[probe] > target) {
                    high = probe;
                } else {
                    low = probe;
                }
            }
            return low;
        } else {
            for (int i = 0; i < this.m_index; i++) {
                if (this.m_array[i] == target) {
                    return i;
                }
            }

            return -1;
        }
    }

    /**
     * Puts all members of other IntVector into this one.
     *
     * @param other
     */
    /*
     * public void addAll(IntVector other)
	 * {
	 * if (other == null || other.m_index == 0) return;
	 * if (m_sorted && !other.m_sorted) throw new IllegalArgumentException("Cannot merge unsorted vector into sorted vector");
	 * int[] newVec = new int[m_index + other.m_index + m_increment];
	 * if (m_sorted)
	 * {
	 * int idx1 = 0;
	 * int idx2 = 0;
	 * int idx3 = 0;
	 * int tmp1, tmp2;
	 * while (idx1 < m_index && idx2 < other.m_index)
	 * {
	 * tmp1 = m_array[idx1];
	 * tmp2 = other.m_array[idx2];
	 * if (tmp1 > tmp2)
	 * {
	 * newVec[idx3] = tmp1;
	 * idx1++;
	 * }
	 * else if (tmp2 > tmp1)
	 * {
	 * newVec[idx3] = tmp2;
	 * idx2++;
	 * }
	 * else
	 * {
	 * //don't want dupes
	 * newVec[idx3] = tmp1;
	 * idx1++;
	 * idx2++;
	 * }
	 * }
	 * if (m_index > idx1)
	 * {
	 * System.arraycopy(m_array, idx1, newVec, idx3, m_index - idx1);
	 * m_index = idx3 + m_index - idx1;
	 * }
	 * else if (other.m_index > idx2)
	 * {
	 * System.arraycopy(other.m_array, idx2, newVec, idx3, other.m_index - idx2);
	 * m_index = idx3 + other.m_index - idx2;
	 * }
	 * else m_index = idx3;
	 * m_array = newVec;
	 * }
	 * else
	 * {
	 * System.arraycopy(m_array, 0, newVec, 0, m_index);
	 * System.arraycopy(other.m_array, 0, newVec, m_index, other.m_index);
	 * m_array = newVec;
	 * m_index += other.m_index;
	 * }
	 * }
	 */

    /**
     * Returns a String containing the elements of this IntVector
     * separated by commas.
     *
     * @return
     */
    public String toDelimSeparatedString(char delim) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < size(); i++) {
            sb.append(elementAt(i));
            if (i < size() - 1) {
                sb.append(delim);
            }
        }
        return sb.toString();
    }

    public int[] toArray() {
        int[] result = new int[this.m_index];

        if (this.m_index > 0) {
            System.arraycopy(this.m_array, 0, result, 0, result.length);
        }

        return result;
    }

    @Override
    public String toString() {
        return size() > 0 ? toDelimSeparatedString(',') : "!!empty!!";
    }
}

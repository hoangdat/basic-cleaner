/**
 *
 */

package com.mfluent.cloud.googledrive.common;

import com.mfluent.asp.cloudstorage.api.sync.CloudStorageSync;
import com.mfluent.cloud.googledrive.common.CloudContext.AddFileListener;
import com.mfluent.log.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * each instance represents a single directory (not a file on the edge of the folder tree) in cloud storage
 *
 * @author jrenkel
 */
public class CloudDirectory extends CloudFile implements AddFileListener {
    List<CloudFile> files;
    AtomicInteger holdCount = new AtomicInteger(0);
    private long databaseId = 0;
    private boolean isRoot = false;

    private int mLevel = 0;

    private List<CloudDirectory> syncingSubDirectories = null;

    public CloudDirectory(CloudContext context, CloudDirectory parent, String name) {
        super(context, parent, name);
    }

    public void copyFromDirectory(CloudDirectory directory) {
        copyFromFile(directory);
        this.isRoot = directory.isRoot;
    }

    public boolean filesLoaded() {
        Log.i(this, "filesLoaded() called, files value : " + this.files);
        return files != null;
    }

    public boolean loadFiles(CloudStorageBase<?> cloudStorage) {
        CloudStorageSync.Result ret = new CloudStorageSync.Result(true);
        files = cloudStorage.getChildren(mCloudId, true, ret);
        return ret.mRet;
    }

    public int getCount() {
        return (files != null) ? files.size() : 0;
    }

    public CloudFile getFile(String name) {
        Log.e(this, "getFile(" + name + ")");

        if (name == null) {
            return null;
        }

        //		if (loadFiles() && filesLoaded()) {
        if (files != null) {
            for (CloudFile file : files) {
                Log.d(this, "getFile - file : " + file.getName() + " ;; " + name.equalsIgnoreCase(file.getName()));
                // if (name.contentEquals(file.getName())) {
                if (name.equalsIgnoreCase(file.getName())) {
                    return file;
                }
            }
        } else {
            Log.e(this, "getFile() - this.files == null");
        }
        return null;
    }

    public CloudFile getFilePath(String path) {
        Log.e(this, "getFilePath(" + path + ")");

        if (path == null || files == null) {
            return null;
        }

        for (CloudFile file : files) {
            if (path.equalsIgnoreCase(file.getPath())) {
                Log.e(this, "getFilePath() - returned file : " + file);
                return file;
            }
        }

        return null;
    }

    public CloudFile getFile(int index) {
        //return loadFiles() && filesLoaded() && index < this.files.size() ? this.files.get(index) : null;
        Log.i(this, "getFile() : index = " + index + ", files.size() = " + files.size() + ", returns = " + files.get(index));
        return (index < this.files.size()) ? this.files.get(index) : null;
    }

    public CloudFile getFileByCloudId(String cloudId) {
        if (cloudId == null) {
            return null;
        }
        if (loadFiles() && filesLoaded()) {
            for (CloudFile file : files) {
                if (cloudId.contentEquals(file.getCloudId())) {
                    return file;
                }
            }
        }
        return null;
    }

    public void holdFiles() {
        Log.i(this, "holdFiles() called");
        this.holdCount.incrementAndGet();
        CloudDirectory myParent = getParent();
        if (myParent != null) {
            myParent.holdFiles();
        }
    }

    public void releaseFiles() {
        Log.i(this, "releaseFiles() called, target dir = " + this);

        int holdCount = this.holdCount.decrementAndGet();
        Log.i(this, "releaseFiles(), holdCount = " + holdCount);
        if (holdCount == 0) {
            unloadFiles();
        }
        CloudDirectory myParent = getParent();
        if (myParent != null) {
            myParent.releaseFiles();
        }
    }

    public void unloadFiles() {
        Log.i(this, "unloadFiles() called, target directory = " + this);
        this.files = null;
    }

    public void doneSyncing(boolean success) {
        if (this.syncingSubDirectories != null) {
//jsub12.lee_150721
//				if (success) {
//					for (CloudDirectory notFound : this.syncingSubDirectories) {
//						this.context.getDatabaseHelper().deleteDirectory(notFound);
//					}
//				}
            this.syncingSubDirectories = null;
        }
        this.holdCount.notifyAll();
    }

    private boolean loadFiles() {
        Log.i(this, "loadFiles() called, TargetDir = " + this);
        boolean success = false;
        boolean needToLoadFiles;

        Log.d(this, "loadFiles() - syncingSubDirectories : " + syncingSubDirectories + ", holdCount : " + holdCount);
        while (this.syncingSubDirectories != null) {

            try {
                this.holdCount.wait();
            } catch (InterruptedException ie) {

            }
        }
        needToLoadFiles = this.files == null;
        Log.i(this, "loadFiles(), needToLoadFiles = " + needToLoadFiles);
        if (needToLoadFiles) {
            this.holdCount.incrementAndGet();    // hold the files just in this directory, not ancestors
            this.syncingSubDirectories = mContext.getDatabaseHelper().getSubDirectories(mContext, this);
        }

        if (needToLoadFiles) {
            try {
                this.files = new ArrayList<CloudFile>();
                try {
                    success = mContext.loadDirectoryFiles(this);
                    if (!success) {
                        this.files = null;
                    }
                } catch (Exception e) {
                    Log.e(this, "exception loading files for " + getPath() + " : " + e);
                    this.files = null;
                }
            } finally {
                doneSyncing(success);
                this.holdCount.decrementAndGet();    // now safe to release files
            }
        } else {
            success = true;
        }
        return success;
    }

    @Override
    public CloudFile addFile(CloudFile file, CloudDirectory directory) {

        if (this.files == null) {
            Log.i(this, "CloudDirectory::addFile():: - this.files = new ArrayList<CloudFile>()");
            this.files = new ArrayList<CloudFile>();
        }

        if (this.files != null) {
            Log.i(this, "addFile():: Current dir is " + this + ", Target file is " + file);
            this.files.add(file);
        }

        if (file instanceof CloudDirectory) {
            Log.d(this, "addFile():: file is directory");
            CloudDirectory subDirectory = (CloudDirectory) file;
            file = subDirectory = getContext().getDatabaseHelper().findOrSaveDirectory(subDirectory);
            if (this.syncingSubDirectories != null) {
                Log.d(this, "syncingSubDirectories is not null");
                this.syncingSubDirectories.remove(subDirectory);
            }
        }

        return file;
    }

    @Override
    public void removeFile(CloudFile file) {
        int index = getIndex(file);
        Log.i(this, "removeFile(), file index : " + index);
        if (index >= 0) {
            this.files.remove(index);
        }
    }

    private int getIndex(CloudFile file) {
        if (!filesLoaded()) {
            return -1;
        }
        return this.files.indexOf(file);
    }

    @Override
    public String toString() {
        return " [CloudDirectory:" + " path: " + getPath() + ", id = " + mCloudId
                + ", files: " + (this.files != null ? this.files.size() : "not loaded, level : " + mLevel + "] ");
    }

    @Override
    public void removeFile(CloudFile file, CloudDirectory directory) {
        // TODO Auto-generated method stub
        removeFile(file);
    }

    public void setDatabaseId(long databaseId) {
        this.databaseId = databaseId;
    }

    public long getDatabaseId() {
        return this.databaseId;
    }

    public boolean isRoot() {
        return this.isRoot;
    }

    public void setRoot(boolean isRoot) {
        this.isRoot = isRoot;

    }

    public int getLevel() {
        return mLevel;
    }

    public void setLevel(int level) {
        mLevel = level;
    }
}

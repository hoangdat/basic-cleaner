/**
 * 
 */

package com.mfluent.asp.common.datamodel;

/**
 * @author Ilan Klinghofer
 */
public class ASPFileSelectionHelper {
	private final boolean[] mSelectedIndexes;
	private final boolean[] mDirectoryIndexes;

	public ASPFileSelectionHelper(int totalCount) {
		mSelectedIndexes = new boolean[totalCount];
		mDirectoryIndexes = new boolean[totalCount];
	}

	public void setSelected(int index, boolean selected) {
		mSelectedIndexes[index] = selected;
	}

	public void setIsDirectory(int index) {
		boolean wasDirectory = mDirectoryIndexes[index];
		if (!wasDirectory) {
			mDirectoryIndexes[index] = true;

			//treat all directories as implicitly selected
			setSelected(index, true);
		}
	}
}

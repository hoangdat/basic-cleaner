
package com.mfluent.asp.cloudstorage.api.sync;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.sqlite.SQLiteDatabase;

import com.mfluent.asp.common.datamodel.ASPFile;
import com.mfluent.asp.common.datamodel.ASPFileBrowser;
import com.mfluent.asp.common.datamodel.ASPFileProvider;
import com.mfluent.asp.common.datamodel.CloudDataModel;
import com.mfluent.asp.common.datamodel.CloudDevice;
import com.mfluent.asp.common.io.util.StreamProgressListener;
import com.mfluent.asp.common.media.thumbnails.ImageInfo;
import com.samsung.android.sdk.slinkcloud.CloudGatewayConstants.OperationType;
import com.google.api.services.drive.model.About;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

/**
 * The interface CloudStorageSync. <br>
 * <br>
 * This interface is the "root" of the Cloud Storage Plugin API, which consists of:
 * <ul>
 * <li>the CloudStorageSync interface;</li>
 * <li>all types that are reachable from this API, except "standard" java Types (void, int, String, List&lt;String&gt;, etc.);</li>
 * <li>final values that are reachable from this API without a getter() in the chain from definition to use (in that case, the getter() is part of the API, the
 * value is not.).</li>
 * </ul>
 * Whenever this API changes the CLOUD_INTERFACE_VERSION, below, must be changed so that plugin versions can be downloaded that match the application version. <br>
 * <br>
 * The following is the changelog for CLOUD_INTERFACE_VERSION values: <br>
 * <br>
 * <table border="1">
 * <tr>
 * <td>Version</td>
 * <td>Date</td>
 * <td>Description of change(s) to this version</td>
 * </tr>
 * <tr>
 * <td>10</td>
 * <td>Feb 17 2013</td>
 * <td>[10 is the first released version of the interface]</td>
 * </tr>
 * </table>
 *
 * @author MFluent
 * @version 30
 */
public interface CloudStorageSync extends ASPFileProvider {

    // MUST be all lower case!!! you have been warned!!!
    String ASP_OAUTH_CALLBACK_KEYWORD = "aspoauth";

    // public static final String DEVICE_ID_KEY = "device_id";

    // OAUTH Broadcast extras
    String OAUTH1_URI = "OAUTH1_URI";

    String OAUTH1_RESPONSE_URI = "OAUTH1_RESPONSE_URI";

    String OAUTH1_CALLBACK_FILTER = "OAUTH1_CALLBACK_FILTER";

    /**
     * The plugin receives an Intent with this action as part of the IOC initialization sequence.
     */
    String PROVIDER_CONFIGURATION = "cloudmanager.com.mfluent.asp.syn.PROVIDER_CONFIGURATION";

    /**
     * UI sends this broadcast to a cloud device if users selects the "SignIn"
     * buttons. This should contain the extra "DEVICE_ID_KEY" with the id of the
     * device to sign into
     **/
    String CLOUD_SIGNIN = "cloudmanager.com.mfluent.asp.sync.CLOUD_SIGNIN";

    String CLOUD_APP_STARTED = "com.samsung.android.slinkcloud.CLOUD_APP_STARTED";
    String CLOUD_APP_STOPPED = "com.samsung.android.slinkcloud.CLOUD_APP_STOPPED";

    /**
     * These broadcasts contain extra CloudDevice.DEVICE_ID_EXTRA_KEY (int) to indicate which device
     * sent the broadcast
     **/
    String CLOUD_AUTHENTICATION_FAILURE = "cloudmanager.com.mfluent.asp.sync.CLOUD_AUTHENTICATION_FAILURE";

    String CLOUD_AUTHENTICATION_SUCCESS = "cloudmanager.com.mfluent.asp.sync.CLOUD_AUTHENTICATION_SUCCESS";

    String CLOUD_AUTHENTICATION_UNKNOWN = "cloudmanager.com.mfluent.asp.sync.CLOUD_AUTHENTICATION_UNKNOWN";

    String CLOUD_STORAGE_CAPACITY_CHANGED = "cloudmanager.com.mfluent.asp.sync.CLOUD_STORAGE_CAPACITY_CHANGED";

    /*
     * CLOUD_LAUNCH_OAUTH1_BROWSER has the following Extras: DEVICE_ID_KEY,
     * OAUTH1_URI,
     */
    String CLOUD_LAUNCH_OAUTH1_BROWSER = "cloudmanager.com.mfluent.asp.sync.CLOUD_LAUNCH_OAUTH1_BROWSER";

    String CLOUD_OAUTH1_RESPONSE = "cloudmanager.com.mfluent.asp.sync.CLOUD_OAUTH1_RESPONSE";

    /**
     * The Enum AuthType.
     *
     * @author MFluent
     * @version 1.5
     */
    enum AuthType {
        NONE,
        USER_PASSWORD,
        OAUTH10, // http://tools.ietf.org/html/rfc5849
        OAUTH20, // http://tools.ietf.org/html/draft-ietf-oauth-v2-31
    }

	/*
     * inversion of control methods:
	 * these are called before init(), which is called before any other methods
	 */

    /**
     * Sets the cloud device.
     *
     * @param device the new cloud device
     */
    void setCloudDevice(CloudDevice device);

    /**
     * Sets the storage app delegate.
     *
     * @param appDelegate the new storage app delegate
     */
    void setStorageAppDelegate(CloudStorageAppDelegate appDelegate);

    /**
     * Sets the content provider.
     *
     * @param contentResolver the new content provider
     */
    void setContentProvider(ContentResolver contentResolver);

    /**
     * Sets the application context.
     *
     * @param appContext the new application context
     */
    void setApplicationContext(Context appContext);

    /**
     * Sets the data model.
     *
     * @param dataModel the new data model
     */
    void setDataModel(CloudDataModel dataModel);

    /**
     * This is called after all the setters have been called and right before the first Sync() invocation.
     * Inits the plugin.
     */
    void init();

    /**
     * Sync data from remote CloudStorage into local meta store.
     */
    void sync();

    /**
     * Clear all internal storage for the CloudDevice. Once called no more
     * operations can be performed on this instance of CloudStorageSync.
     */
    void reset();


    /**
     * Download the thumbnail
     *
     * @param imageInfo      the image info
     * @param pathToDownload the directory to download
     * @param fileName       the file name of download
     * @return the thumbnail
     * @throws IOException
     */
    File downloadThumbnail(ImageInfo imageInfo, String pathToDownload, String fileName) throws IOException;

    /**
     * This is for support content range.
     *
     * @param sourceUrl    the URI of the media
     * @param sourceId     the source id
     * @param contentRange the HTTP range specification for the request
     * @return the CloudStreamInfo for the stream being returned
     * @throws FileNotFoundException the file not found exception
     */
    CloudStreamInfo getFile(String sourceUrl, String sourceId, String contentRange) throws IOException;

    /**
     * See: http://www.freesoft.org/CIE/RFC/2068/160.htm
     *
     * @return true if plugin can support returning partial files
     */
    boolean isRangeDownloadSupported();

    /**
     * get the storage gateway file ID for a file stored in a cloud storage.
     * </br></br>
     * the parameter values used to identify the file have been stored in the metadata for the file by the cloud connector.
     *
     * @param aspMediaType the media type of the file whose storage gateway file id is to be gotten
     * @param sourceUrl    the source URL of the file whose storage gateway file id is to be gotten
     * @param sourceId     the source ID of the file whose storage gateway file id is to be gotten
     * @return the storage gateway file ID for the file
     */
    String getStorageGatewayFileId(int aspMediaType, String sourceUrl, String sourceId) throws FileNotFoundException;

    /**
     * get the a portion of a file given a storage gateway file ID.
     *
     * @param storageGatewayFileId the storage gateway file ID of the file to get
     * @param contentRange         the portion of the file to get specified as an HTTP ContentRange header value <br/>
     *                             <br/>
     *                             if null, the entire file will be gotten
     * @return a CloudStreamInfo that can be used to read the file and monitor it's progress
     * @throws FileNotFoundException if the specified file can be gotten
     */
    CloudStreamInfo getFile(String storageGatewayFileId, String contentRange) throws IOException;

    String downloadFile(StreamProgressListener listener, String fileId, String targetPath, String fileName, boolean bWaitForRename) throws IOException;

    /**
     * Delete file.
     *
     * @param sourceUrl TODO
     * @param sourceId  the source id
     * @return true if the file was deleted successfully
     */
    boolean deleteFile(String sourceUrl, String sourceId);

    boolean deleteFileBatch(String[] sourceIds);

    boolean deletePermanentlyBatch(String[] sourceIds);

    /**
     * If Width or Height are less than 10 the default values will be used;.
     *
     * @param width  the width
     * @param height the height
     */
    void setPreferredThumbnailSize(int width, int height);

    /**
     * Gets the cloud storage file browser for the root directory.
     *
     * @return the cloud storage file browser
     */
    ASPFileBrowser<?> getCloudStorageFileBrowser();

    /**
     * On receive.<br/>
     * <br/>
     * Among other times, OnRecieve() will be called during the IOC initialization sequence
     * with an Intent having the PROVIDER_CONFIGURATION action and appropriate extras.
     *
     * @param context The Context in which the receiver is running.
     * @param intent  The Intent being received.
     */
    void onReceive(Context context, Intent intent);

    /**
     * Plugins that are interested in receiving intents must register the intents they expect.
     * Some common intents are:
     * <table border="1">
     * <tr>
     * <th>Intent</th>
     * <th>Description</th>
     * </tr>
     * <tr>
     * <td>CloudDevice.BROADCAST_DEVICE_REFRESH</td>
     * <td>This happens when the user presses the refresh menu option. You can look at the extra CloudDevice.REFRESH_FROM_KEY to see what screen the refresh
     * occurred in.</td>
     * </tr>
     * <tr>
     * <td>CloudStorageSync.CLOUD_OAUTH1_RESPONSE</td>
     * <td>This is sent after the user finishes with the OAuth Web View.</td>
     * </tr>
     * <tr>
     * <td>CloudStorageSync.CLOUD_SIGNIN</td>
     * <td>This is sent when user presses the SignIn button in the UI.</td>
     * </tr>
     * </table>
     *
     * @return the Intent filters this device is interested in
     */
    IntentFilter[] getIntentFilters();

    class Result {
        public boolean mRet = false;
        public ArrayList<String> mFileIdList = new ArrayList<>();
        public ArrayList<String> mFileNameList = new ArrayList<>();
        public ArrayList<String> mFileDstIdList = new ArrayList<>();

        public Result(boolean bRet) {
            mRet = bRet;
        }
    }

    /**
     * The Enum UploadResult.
     *
     * @author MFluent
     * @version 1.5
     */
    class UploadResult {
        public String mFileId;
        public Status mStatus;

        public enum Status {
            /**
             * the upload was successful
             */
            OK,
            /**
             * don't (currently) have authorization for the cloud storage account
             */
            NOT_AUTHORIZED,
            /**
             * the size of the upload exceeds the available space in the cloud
             * storage account
             */
            OUT_OF_SPACE,
            /**
             * the size of the upload exceeds the cloud storage's limit on file
             * sizes
             */
            TOO_LARGE,
            /**
             * what it says
             */
            CANT_CREATE_ARTIST_DIRECTORY,
            /**
             * what it says
             */
            CANT_CREATE_ALBUM_DIRECTORY,
            /**
             * max retry fail
             */
            MAX_RETRY_FAIL,
            /**
             * no need retry
             */
            NO_NEED_RETRY,
            /**
             * reach max item error
             */
            REACHED_MAX_ITEMS,
            /**
             * some other error occurred
             */
            OTHER,
        }
    }

    /**
     * get the maximum (upload) file size that is supported by this cloud storage (connector)
     *
     * @return the maximum file size that is supported by this cloud storage; 0 => no limit
     */
    long getMaximumFileSize();

    /**
     * Upload.
     *
     * @param targetDirId            the media Id of directory in which the uploaded file will be
     * @param fileToUpload           the File to upload
     * @param mimeType               the mime type
     * @param streamProgressListener the stream progress listener
     * @return an enumeration value giving the result of the upload request
     */
    UploadResult uploadFile(String targetDirId, File fileToUpload, String mimeType, StreamProgressListener streamProgressListener);

    /**
     * Gets the file.
     *
     * @param file         the file
     * @param contentRange the HTTP range specification for the request
     * @return the file
     * @throws FileNotFoundException the file not found exception
     */
    CloudStreamInfo getFile(ASPFile file, String contentRange) throws IOException;

    /**
     * rename content.
     *
     * @param parentDirectoryId directory ID of source content.
     * @param sourceId          source content ID.
     * @param newName           new content name.
     * @return true if the file was renamed successfully
     */
    boolean renameFile(String parentDirectoryId, String sourceId, String newName);

    /**
     * move content position.
     *
     * @param parentDirectoryId parent directory ID of source content.
     * @param sourceId          source content ID.
     * @param targetDirId       new parent directory Path.
     *                          in case of GoogleDrive, this param should be cloud id of the new parent directoy *
     * @return true if the file was moved successfully
     */
    boolean moveFile(String parentDirectoryId, String sourceId, String targetDirId);

    Result moveFileBatch(ArrayList<String[]> moveList, StreamProgressListener listener) throws IOException;


    /**
     * create a folder
     *
     * @param parentDirectoryId parent directory ID of new position.
     * @param directoryName     name of new folder.
     * @return the created directory's id if created successfully
     */
    String createDirectory(String parentDirectoryId, String directoryName) throws IOException;

    /**
     * copy a file to another position
     *
     * @param parentDirectoryId parent directory ID of source content.
     * @param sourceId          source content ID.
     * @param newParentPath     new parent directory Path or directory id.
     * @param newName           new content name
     * @return true if the file was deleted successfully
     */
    boolean copyFile(String parentDirectoryId, String sourceId, String newParentPath, String newName);

    Result copyFileBatch(ArrayList<String[]> copyList, StreamProgressListener listener) throws IOException;

    boolean emptyTrash();

    boolean deleteTrash(String sourceId);

    boolean deleteTrashBatch(String[] sourceIds);

    /**
     * restore a Trash
     *
     * @param sourceId source content ID
     * @return parent Id if the file restore successfully, null if not
     */
    String restoreTrash(String sourceId) throws IOException;

    ArrayList<String> restoreTrashBatch(String[] sourceIds) throws IOException;

    /**
     * get the database instance hold by this cloud storage (connector)
     *
     * @return the SQLiteDatabase object used by this cloud storage;
     */
    SQLiteDatabase getReadableCloudDatabase();

    long getLastSync();

    void cancel(OperationType type);

    long[] getDetailedUsed();

    long[] updateQuota();
}

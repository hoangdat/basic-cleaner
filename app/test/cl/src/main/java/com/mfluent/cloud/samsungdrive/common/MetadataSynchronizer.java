
package com.mfluent.cloud.samsungdrive.common;

import android.util.Log;

import com.mfluent.cloud.samsungdrive.common.CloudContext.AddFileListener;
import com.mfluent.cloud.samsungdrive.common.CloudStorageBase.MediaType;
import com.mfluent.cloud.samsungdrive.common.Metadata.State;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

/**
 * instances of this class are used to synchronize the metadata of a single media type in a single cloud storage.
 */
public class MetadataSynchronizer implements AddFileListener {

    private static String TAG = "MetadataSynchronizer";

    private final CloudStorageBase<? extends CloudContext> storage;

    public final Map<String, Metadata> aspMetadata; // metadata for this mediaType, //jsub12_150923

    protected Map<String, Metadata.Id> captionAndIndexFiles = new HashMap<String, Metadata.Id>();

    private final Stack<CloudDirectory> syncStack = new Stack<CloudDirectory>();

    private boolean aborted = false;

    /**
     * Create a media type synchronizer.
     *
     * @param storage                the cloud storage whose metadata is to be synchronized
     * @param aspMetadata            the existing metadata for this mediaType in this cloud storage
     * @param mediaTypeRootDirectory the root directory for files of this mediaType in this cloud storage
     */
    /* @formatter:off */
    public MetadataSynchronizer(
            CloudStorageBase<? extends CloudContext> storage,
            Map<String, Metadata> aspMetadata,
            CloudDirectory mediaTypeRootDirectory) {
        /* @formatter:on */
        this.storage = storage;
        this.aspMetadata = aspMetadata;
        Log.i(TAG, "MetadataSynchronizer::creator:: syncStack.push :" + mediaTypeRootDirectory);
        this.syncStack.push(mediaTypeRootDirectory);
    }

    /**
     * abort the synchronization of metadata for the cloud storage
     */
    public void abort() {
        this.aborted = true;
    }

    /*
     * methods from AddFileListener interface
     */
    @Override
    public CloudFile addFile(CloudFile file, CloudDirectory directory) {
        //		file = directory.addFile(file, directory);
        Log.d(TAG, "MetadataSynchronizer:: addFile(), target file = " + file);
        if (file.isDirectory() && !file.isDeleted()) {
            Log.v(TAG, "metasync addFile(), directory " + file.getPath());
            Log.i(TAG, "MetadataSynchronizer::addFile:: syncStack.push :" + file);
            syncStack.push((CloudDirectory) file);
        }

        String sourceMediaId = storage.getSourceMediaId(file).toString();
        Log.i(TAG, "MetadataSynchronizer:: addFile(), sourceMediaId : " + sourceMediaId);
        Metadata oldMetadata = aspMetadata.get(sourceMediaId);
        Metadata newMetadata = metadataFromFile(file, Metadata.State.ADDED);
        Metadata.State state;
        if (oldMetadata == null) {
            Log.i(TAG, "MetadataSynchronizer:: addFile(), oldMetadata is null");
            aspMetadata.put(sourceMediaId, newMetadata);
            state = newMetadata.state;
        } else {
            boolean metadataEqual = oldMetadata.equalTo(newMetadata);
            if (!metadataEqual) {
                newMetadata.state = State.CHANGED;
                aspMetadata.put(sourceMediaId, newMetadata);
                state = newMetadata.state;
            } else {
                oldMetadata.state = State.UNCHANGED;
                state = oldMetadata.state;
            }

            Log.d("ProcessMetadata", "MetadataSynchronizer - file.isDeleted() : " + file.isDeleted());

            if (file.isDeleted()) {
                newMetadata.state = State.UNSEEN;
                aspMetadata.put(sourceMediaId, newMetadata);
                state = newMetadata.state;
            }
        }
        Log.v(TAG, "addFile: file " + file.getName() + "; metadata " + state.name());

        return file;
    }

	/*
     * end of methods from AddFileListener interface
	 */

    private Metadata metadataFromFile(CloudFile file, Metadata.State state) {
        Metadata.Id id = this.storage.getSourceMediaId(file);
        long length = file.length();
        String parentCloudId = file.getParentCloudId(); //jsub12.lee_150715_2
        String parentName = file.getParentName(); //jsub12_150924_1
        long dateModified = file.getLastModified() / 1000;
        String displayName = file.getName();
        String mimeType = file.getMimeType();
        String thumbnailURI = file.getThumbnailURL();
        String fullPath = file.getPath(); //jsublee_150907
        String trashProcessing = file.getTrashProcessing();

        /* @formatter:off */
        Metadata fileMetadata = new Metadata(
                id,
                state,
                length,
                parentCloudId, //jsub12.lee_150715_2
                parentName, //jsub12_150924_1
                dateModified,
                displayName,
                mimeType,
                thumbnailURI,
                fullPath,
                trashProcessing);
        /* @formatter:on */

        return fileMetadata;
    }

    @Override
    public void removeFile(CloudFile file) {
        Log.d(TAG, "MetadataSynchronizer::removeFile() 1 called");

        String sourceMediaId = this.storage.getSourceMediaId(file).toString();
        Log.i(TAG, "MetadataSynchronizer::removeFile(), sourceMediaId = " + sourceMediaId);

        Metadata oldMetadata = this.aspMetadata.get(sourceMediaId);
        if (oldMetadata == null) {
            Log.i(TAG, "MetadataSynchronizer:: removeFile(), oldMetadata is null ");
        } else {
            oldMetadata.state = State.UNSEEN;
            this.aspMetadata.put(sourceMediaId, oldMetadata);
            Log.i(TAG, "MetadataSynchronizer::removeFile(), sourceMediaId is set to UNSEEN");
        }
    }

    @Override
    public void removeFile(CloudFile file, CloudDirectory directory) {

        Log.d(TAG, "MetadataSynchronizer::removeFile() 2 called");

        // TODO Auto-generated method stub
        CloudStorageBase.MediaType mediaType = MediaType.forFile(file);

        Log.d("ProcessMetadata", "MetadataSynchronizer - mediaType : " + mediaType);

        if (mediaType != null) {
            String sourceMediaId = storage.getSourceMediaId(file).toString();
            Metadata newMetadata = metadataFromFile(file, Metadata.State.UNSEEN);

            Log.d("ProcessMetadata", "MetadataSynchronizer - file.isDeleted() : " + file.isDeleted());

            aspMetadata.put(sourceMediaId, newMetadata);

            Log.d("ProcessMetadata", "removeFile: file " + file.getName() + "; metadata " + newMetadata.state.name());

        }
    }

}

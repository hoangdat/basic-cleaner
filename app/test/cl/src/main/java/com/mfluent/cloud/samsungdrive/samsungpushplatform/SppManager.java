package com.mfluent.cloud.samsungdrive.samsungpushplatform;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import com.mfluent.cloud.samsungdrive.common.DriveConstants;
import com.mfluent.log.Log;

public class SppManager {

    public static final String EXTRA_KEY_APPID = "appId";
    public static final String MY_APPID = "247383401ae9a3ba";

    public static final String ACTION_START_SPP = "com.sec.spp.push.PUSH_CLIENT_SERVICE_ACTION";
    public static final String SPP_PACKAGE_NAME = "com.sec.spp.push";

    public static final String EXTRA_REQTYPE = "reqType";
    public static final int PUSH_REQ_TYPE_REGISTRATION = 1;
    public static final int PUSH_REQ_TYPE_DEREGISTRATION = 2;

    public static final String EXTRA_KEY_USERDATA = "userdata";

    public static BroadcastReceiver registerSppReceiver(Context context, String preferenceFileName, Context appContext) {
        requestRegistration(context);
        return registerRegResultReceiver(context, preferenceFileName, appContext);
    }

    public static void requestRegistration(Context context) {
        Log.d("SppManager", "SPP requestRegistration");
        Intent i = new Intent(ACTION_START_SPP);
        i.setPackage(SPP_PACKAGE_NAME);
        i.putExtra(EXTRA_REQTYPE, PUSH_REQ_TYPE_REGISTRATION);
        i.putExtra(EXTRA_KEY_APPID, MY_APPID);
        i.putExtra(EXTRA_KEY_USERDATA, context.getPackageName());
        context.startService(i);
    }

    public static void requestDeregistration(Context context) {
        Log.d("SppManager", "SPP requestDeregistration");
        Intent i = new Intent(ACTION_START_SPP);
        i.setPackage(SPP_PACKAGE_NAME);
        i.putExtra(EXTRA_REQTYPE, PUSH_REQ_TYPE_DEREGISTRATION);
        i.putExtra(EXTRA_KEY_APPID, MY_APPID);
        context.startService(i);
    }

    public static BroadcastReceiver registerRegResultReceiver(Context context, String preferenceFileName, Context appContext) {
        SppRegResultReceiver receiver = new SppRegResultReceiver(preferenceFileName, appContext);
        IntentFilter filter = new IntentFilter();
        filter.addAction(SppRegResultReceiver.PUSH_REGISTRATION_CHANGED_ACTION);
        context.registerReceiver(receiver, filter);

        return receiver;
    }

    public static void deregisterRegResultReceiver(Context context, BroadcastReceiver receiver) {
        context.unregisterReceiver(receiver);
    }
}

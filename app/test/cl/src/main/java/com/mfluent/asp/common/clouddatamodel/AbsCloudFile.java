package com.mfluent.asp.common.clouddatamodel;

import com.mfluent.asp.common.datamodel.ASPFile;

import java.text.CollationKey;
import java.text.Collator;

abstract public class AbsCloudFile implements ASPFile {

    public static class ImageInfo {

        private int mHeight;

        private int mWidth;

        private String mType; // "full", "normal", "album", "small"

        public void setHeight(int height) {
            mHeight = height;
        }

        public int getHeight() {
            return mHeight;
        }

        public void setWidth(int width) {
            mWidth = width;
        }

        public int getWidth() {
            return mWidth;
        }

        public void setType(String type) {
            mType = type;
        }

        public String getType() {
            return mType;
        }

    }

    protected boolean mDeleted = false; // the file has been deleted in the cloud storage
    protected String mCloudId; // the id of the file in cloud storage

    protected String mParentCloudId; //bucket_id

    protected String mParentName; //bucket_display_name ,jsub12_150924_1

    protected String mName;

    protected String mPath = "";
    private CollationKey mNameCollationKey;

    protected long mLength;

    protected int mChildCount; //jsub12_151006
    protected int mChildDirCount; //jsub12_151006

    protected int mDescendantsCount;
    protected int mDescendantDirCount;

    protected long mCreated;
    protected long mLastModified;

    protected String mRevision;

    protected boolean mHasThumbnail;
    private String mThumbnailURL;

    protected String mType;

    protected String description;

    protected boolean isReallyDirectory = false;

    protected String mMimeType;

    protected String mDownloadURL;

    public AbsCloudFile(String name) {
        mName = name;
    }

    public AbsCloudFile(AbsCloudFile file) {
        copyFromFile(file);
    }

    public void copyFromFile(AbsCloudFile file) {
        mDeleted = file.mDeleted;
        mCloudId = file.mCloudId;
        mParentCloudId = file.mParentCloudId; //jsub12.lee_150715_2
        mParentName = file.mParentName; //jsub12_150924_1
        mName = file.mName;
        mNameCollationKey = file.mNameCollationKey;
        mLength = file.mLength;
        mCreated = file.mCreated;
        mLastModified = file.mLastModified;
        mRevision = file.mRevision;
        mHasThumbnail = file.mHasThumbnail;
        mThumbnailURL = file.mThumbnailURL;
        mType = file.mType;
        mMimeType = file.mMimeType;
        mDownloadURL = file.mDownloadURL;
        mPath = file.mPath;
    }

    @Override
    public String getName() {
        return mName;
    }

    @Override
    public long length() {
        return mLength;
    }

    /**
     * last modified time (in milliseconds)
     */
    @Override
    public long lastModified() {
        return mLastModified;
    }

    public boolean isDeleted() {
        return mDeleted;
    }

    public void setDeleted(boolean deleted) {
        mDeleted = deleted;
    }

    public String getCloudId() {
        return mCloudId;
    }

    public void setCloudId(String cloudId) {
        mCloudId = cloudId;
    }

    public void setName(String name) {
        mName = name;
        // reset collation key
        mNameCollationKey = null;
    }

    public void setPath(String path) {
        mPath = path;
    }

    public CollationKey getNameCollationKey() {
        if ((mNameCollationKey == null) && (mName != null)) {
            mNameCollationKey = Collator.getInstance().getCollationKey(mName.toLowerCase());
        }
        return mNameCollationKey;
    }

    public String getPath() {
        //		String name = "";
        //		if (this.parent != null) {
        //			name = this.parent.getPath() + CloudContext.PATH_SEPARATOR + this.name;
        //		}
        //		return name;

        return mPath;
    }

    public void setLength(long length) {
        mLength = length;
    }

    public long getCreated() {
        return mCreated;
    }

    public void setCreated(long created) {
        mCreated = created;
    }

    public long getLastModified() {
        return mLastModified;
    }

    public void setLastModified(long lastModified) {
        mLastModified = lastModified;
    }

    public void setRevision(String revision) {
        mRevision = revision;
    }

    public void setHasThumbnail(boolean hasThumbnail) {
        mHasThumbnail = hasThumbnail;
    }

    public String getThumbnailURL() {
        return mThumbnailURL;
    }

    public void setThumbnailURL(String thumbnailURL) {
        mThumbnailURL = thumbnailURL;
    }

    public String getType() {
        return mType;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setType(String type) {
        mType = type;
    }

    public boolean isReallyDirectory() {
        return this.isReallyDirectory;
    }

    public void setReallyDirectory(boolean isReallyDirectory) {
        this.isReallyDirectory = isReallyDirectory;
    }

    public String getMimeType() {
        return mMimeType;
    }

    public void setMimeType(String mimeType) {
        mMimeType = mimeType;
    }

    public String getDownloadURL() {
        return mDownloadURL;
    }

    public void setDownloadURL(String downloadURL) {
        mDownloadURL = downloadURL;
    }

    @Override
    public String toString() {
        return "CloudFile:" + " path: " + getPath()
                + ", id = " + mCloudId + " , size: " + length()
                + ", modified: " + lastModified();
    }

    @Override
    public String getSpecialDirectoryType() {
        return null;
    }

    public String getParentCloudId() {
        return mParentCloudId;
    }

    public void setParentCloudId(String parentCloudId) {
        mParentCloudId = parentCloudId;
    }

    public void setParentName(String parentNameParam) {
        mParentName = parentNameParam;
    }

    public String getParentName() {
        return mParentName;
    }

    public int getChildCount() {
        return mChildCount;
    }

    public void setChildCount(int childCount) {
        mChildCount = childCount;
    }

    public int getChildDirCount() {
        return mChildDirCount;
    }

    public void setChildDirCount(int childDirCount) {
        mChildDirCount = childDirCount;
    }

    public int getDescendantsCount() {
        return mDescendantsCount;
    }

    public void setDescendantsCount(int descendantsCount) {
        mDescendantsCount = descendantsCount;
    }

    public int getDescendantDirCount() {
        return mDescendantDirCount;
    }

    public void setDescendantDirCount(int descendantDirCount) {
        mDescendantDirCount = descendantDirCount;
    }

    @Override
    public boolean equals(Object obj) {
        boolean bRet = false;
        if (obj instanceof AbsCloudFile) {
            bRet = ((AbsCloudFile) obj).getCloudId().equals(mCloudId);
        }
        return bRet;
    }

}

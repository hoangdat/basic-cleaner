/**
 * Copyright, 2015, SEC. All rights reserved.
 */

package com.mfluent.cloud.googledrive;

import android.content.Context;
import android.content.Intent;

import com.mfluent.asp.common.datamodel.ASPFile;
import com.mfluent.asp.common.clouddatamodel.AbsCloudContext.AuthorizationState;
import com.mfluent.cloud.googledrive.common.CloudDirectory;
import com.mfluent.cloud.googledrive.common.CloudFile;
import com.mfluent.cloud.googledrive.common.CloudStorageBase;
import com.mfluent.log.Log;

import java.io.FileNotFoundException;

/**
 * An implementation of cloud storage for the Google Drive service.
 */
public class GoogleDrive extends CloudStorageBase<GoogleDriveContext> {

    private static final long MAXIMUM_FILE_SIZE = 5 * 1024 * 1024 * 1024 * 1024; // Maximum file size : 5120GB

    public GoogleDrive() {
        super(Type.GOOGLE_DRIVE, AuthType.OAUTH20, false, new GoogleDriveContext());
    }

    @Override
    public long getMaximumFileSize() {
        return MAXIMUM_FILE_SIZE;
    }

    @Override
    public String getStorageGatewayFileId(ASPFile file) throws FileNotFoundException {
        Log.i(this, "getStorageGatewayFileId() called, target file = " + file);
        if (!(file instanceof CloudFile)) {
            throw new FileNotFoundException("not a CloudFile!");
        }
        Log.i(this, "getStorageGatewayFileId() returns = " + ((CloudFile) file).getCloudId());
        return ((CloudFile) file).getCloudId();
    }

    @Override
    protected boolean idsUseBase64() {
        return false;
    }

    @Override
    protected boolean deleteFileFromDirectory(CloudDirectory directory, String storageGatewayId) throws Exception {
        Log.i(this, "deleteFileFromDirectory(): directory = " + directory + ", storageGatewayId = " + storageGatewayId);
        CloudFile file = directory.getFileByCloudId(storageGatewayId);
        return (file != null) ? deleteFile(file) : true;
    }

    @Override
    protected AuthorizationState getAuthorizationInDevice() {
        if (!this.isSignIn) {
            Log.i(this, "getAuthorization():: isSignIn :" + isSignIn);
            return AuthorizationState.NO;
        }

        if (this.getContext().mDrive == null) {
            Log.i(this, "getAuthorization():: mDrive is null");
            this.getContext().mDrive = super.getDrive();
        }

        return AuthorizationState.YES;
    }

    /*
     * @see
     * com.mfluent.cloud.googledrive.common.CloudStorageBase#onReceive
     */
    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        Log.i(this, "onReceive():: action = " + action);

        if (action.contentEquals(CLOUD_OAUTH1_RESPONSE)) {
            String accountName = intent.getStringExtra("Account");

            this.context.setUserId(accountName);
            getContext().mDrive = super.getDrive();
            isSignIn = true;
            Log.i(this, "onReceive():: quickSyncMode -> true");

            saveAccountInfoToPref(accountName, this.isSignIn);
            setIsSignedIn(true); // kludge to allow spinner to be displayed in UI

            //saveQuickSyncmodeInfoToPref(this.quickSyncMode);

            Log.i(this, "onReceive():: mDrive :: " + this.getContext().mDrive);

            setAndCheckAuthorizationState(getAuthorizationInDevice());
            broadcastAuthorizationState();
        } else {
            super.onReceive(context, intent);
        }
    }
}

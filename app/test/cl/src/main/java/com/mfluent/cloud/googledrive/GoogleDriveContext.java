/**
 * Copyright, 2015, SEC. All rights reserved.
 */

package com.mfluent.cloud.googledrive;

import android.content.ContentProviderOperation;
import android.content.ContentValues;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.net.Uri;
import android.os.RemoteException;

import com.google.api.client.googleapis.batch.BatchRequest;
import com.google.api.client.googleapis.batch.json.JsonBatchCallback;
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException;
import com.google.api.client.googleapis.json.GoogleJsonError;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.http.HttpHeaders;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.Drive.Changes;
import com.google.api.services.drive.Drive.Files;
import com.google.api.services.drive.model.Change;
import com.google.api.services.drive.model.ChangeList;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;
import com.google.api.services.drive.model.ParentReference;
import com.mfluent.asp.cloudstorage.api.sync.CloudStreamInfo;
import com.mfluent.asp.common.datamodel.ASPMediaStore;
import com.mfluent.asp.common.util.CloudStorageConstants;
import com.mfluent.asp.common.util.MimeType;
import com.mfluent.cloud.googledrive.common.CloudContext;
import com.mfluent.cloud.googledrive.common.CloudDirectory;
import com.mfluent.cloud.googledrive.common.CloudFile;
import com.mfluent.cloud.googledrive.common.CloudStorageBase;
import com.mfluent.cloud.googledrive.common.CloudStorageBase.MediaType;
import com.mfluent.cloud.googledrive.common.CloudStream;
import com.mfluent.cloud.googledrive.common.Metadata;
import com.mfluent.cloud.googledrive.exception.CloudUnauthorizedException;
import com.mfluent.log.Log;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Queue;
import java.util.regex.Pattern;

import platform.com.mfluent.asp.datamodel.filebrowser.CachedFileBrowser;

/**
 * A context for performing Google Drive operations.
 */
public class GoogleDriveContext extends CloudContext {

    public static final String RETRY_MAX_ERROR = "retry to the max";

    private final HashMap<String, List<com.google.api.services.drive.model.File>> driveFilelistCache;

    private List<Change> changeItems_cache = null;

    public HashMap<String, CloudFile> cloudFilesMap_cache = null; //jsublee_150907
    private List<CloudFile> cloudFilesList_cache = null; //jsublee_150907
    public Queue<CloudDirectory> cloudDirListQToSync = null;

    /**
     * Instantiates a new Google Drive context.
     */
    public GoogleDriveContext() {
        driveFilelistCache = new HashMap<>();
        cloudFilesMap_cache = new HashMap<>(); //jsublee_150907
    }

    // To list all files in the root folder, use the alias "root" as the value for folderId.
    public List<com.google.api.services.drive.model.File> getChildFiles(String folderId) throws IOException {
        Log.i(this, "getChildFiles() called, folderId = " + folderId);
        List<com.google.api.services.drive.model.File> dFileList = new ArrayList<com.google.api.services.drive.model.File>();
        //Children.List request = null;

        try {
            String query = "'" + folderId + "'" + " in parents";
            //String query = "'" + folderId + "'" + " in parents and mimeType contains 'image'";
            //String query = "'" + folderId + "'" + " in parents and mimeType!='application/vnd.google-apps.folder'";

            Log.d(this, "getChildFiles() - execute set query: " + query);
            Files.List request = mDrive.files().list().setQ(query);

            int retryCnt = 0;
            String nextPageToken = null;
            do {
                try {
                    Log.d(this, "getChildFiles() - query = " + query + ", retryCnt = " + retryCnt);
                    FileList files = request.execute();
                    if (files != null) {
                        retryCnt = 0; // success to call api, then reset the retryCnt
                        nextPageToken = files.getNextPageToken();
                        Log.d(this, "getChildFiles() - executed files size: " + files.size());
                        dFileList.addAll(files.getItems());
                        Log.d(this, "getChildFiles() - files added: " + files.getItems().size());
                        request.setPageToken(nextPageToken);
                    }
                } catch (GoogleJsonResponseException e) {
                    Log.i(this, "getChildFiles() - GoogleJsonResponseException : code = " + e.getDetails().getCode() + "reason = " + e.getDetails().getErrors().get(0).getReason());

                    if (e.getDetails().getCode() == 403
                            && (e.getDetails().getErrors().get(0).getReason().equals("rateLimitExceeded")
                            || e.getDetails().getErrors().get(0).getReason().equals("userRateLimitExceeded"))) {
                        // Apply exponential backoff.
                        Thread.sleep(1000);//(1 << retryCnt) * 1000 + randomGenerator.nextInt(1001));
                        Log.i(this, "getChildFiles() - getPageToken() = " + request.getPageToken() + " - nextPageToken = " + nextPageToken);
                        request.setPageToken(nextPageToken);
                        retryCnt++;
                    }
                }

            } while (cloudStorage.isSignedIn()
                    && (retryCnt > 0 || (request.getPageToken() != null && request.getPageToken().length() > 0)));

			/*
             * request = this.mDrive.children().list(folderId);
			 * do {
			 * ChildList children = request.execute();
			 * for (ChildReference child : children.getItems()) {
			 * System.out.println("File Id: " + child.getId());
			 * dFile = this.mDrive.files().get(child.getId()).execute();
			 * dFileList.add(dFile);
			 * }
			 * request.setPageToken(children.getNextPageToken());
			 * } while (request.getPageToken() != null && request.getPageToken().length() > 0);
			 */
        } catch (InterruptedException e) {
            Log.e(this, "getChildFiles() - InterruptedException : " + e.getMessage());
        } catch (IOException e) {
            Log.e(this, "getChildFiles() - IOException : " + e.getMessage());
            throw e;
            //request.setPageToken(null);
        }

        return dFileList;
    }

    private boolean isDir(com.google.api.services.drive.model.File dfile) {
        Log.d(this, "isDir() - mimetype = " + dfile.getMimeType());

        String strType = dfile.getMimeType().substring(dfile.getMimeType().lastIndexOf("/") + 1);

        return strType.equals("vnd.google-apps.folder");
    }

    //for delta sync
    private void setFileMeta(CloudFile file, AddFileListener metaListener) {
        Log.i(this, "setFileMeta() for deltasync called");
        Log.i(this, "------------------------------------------------------");

        if (file.isDeleted()) {
            Log.d(this, "setFileMeta() - DELETED FILE!!!!!");
            metaListener.removeFile(file);
            return;
        }

        String path = file.getPath();
        if (path == null || path.isEmpty()) {
            path = getFileFullPathFromCache(file.getCloudId());
            file.setPath(path);

            Log.i(this, "setFileMeta() - path = " + file.getPath());
        }


        //jsub12_150924_1
        CloudFile parent = cloudFilesMap_cache.get(file.getParentCloudId());

        if (parent != null) {
            file.setParentName(parent.getName());
        } else if (file.getParentCloudId().equalsIgnoreCase(CloudStorageConstants.CLOUD_ROOT)) {
            file.setParentName("/");
        } else {
            Metadata fileMetadata = cloudStorage.getFileMetadata(MediaType.none, file.getParentCloudId());
            if (fileMetadata != null) {
                file.setParentName(fileMetadata.displayName);
            } else {
                Log.e(this, "setFileMeta() - fileMetadata is null");
            }
        }

        Log.i(this, "setFileMeta() - # parentName = " + file.getParentName());


        if (file.isReallyDirectory()) { //jsub12.lee_150721
            getDatabaseHelper().findOrSaveDirectory((CloudDirectory) file);

        } else {
            // Because metaListener.addFile() check if the object is an instance of CloudDirectory...
            file = new CloudFile(file);
        }

        metaListener.addFile(file, null);
    }

    //for full sync
    private void setFileMeta(
            com.google.api.services.drive.model.File dfile,
            CloudFile file,
            AddFileListener listener,
            AddFileListener metalistener,
            CloudStorageBase.MediaType mediaType) {

        if (dfile != null) {
            Log.d(this, "setFileMeta(), ----------------------------------");

            file.setName(dfile.getTitle());
            Log.d(this, "setFileMeta() - # filename = " + file.getName());

            boolean isDir = isDir(dfile);
            file.setReallyDirectory(isDir);
            Log.d(this, "setFileMeta() - # is Directory = " + isDir);

            if (isDir) {
                file.setMimeType(MimeType.DIR_MIMETYPE); //jsub12.lee_150721
            } else {
                file.setMimeType(dfile.getMimeType());
            }
            Log.d(this, "setFileMeta() - # mimetype = " + file.getMimeType());

            file.setCloudId(dfile.getId());
            Log.d(this, "setFileMeta() - # cloudId = " + file.getCloudId());

            file.setParentCloudId(getParentsCloudId(dfile));
            if (file.getParent().isRoot()) { //jsub12_150924_1
                file.setParentName("/");
            } else {
                file.setParentName(file.getParent().getName());
            }

            Log.d(this, "setFileMeta() - # parentCloudId = " + file.getParentCloudId());
            Log.d(this, "setFileMeta() - # parentName = " + file.getParentName());

            //jsublee_150901 - supporting google docs
            //file.setDownloadURL(dfile.getId());
            file.setDownloadURL(dfile.getAlternateLink());
            Log.d(this, "setFileMeta() - # alternateLink = " + file.getDownloadURL());

            file.setDeleted(dfile.getLabels().getTrashed());
            Log.d(this, "setFileMeta() - # is trashed? = " + dfile.getLabels().getTrashed());

            Long fileSize = dfile.getFileSize();
            Log.d(this, "setFileMeta() - # size = " + fileSize);
            if (fileSize != null) {
                file.setLength(fileSize);
            }

            Log.d(this, "setFileMeta() - # modifiedDate = " + dfile.getModifiedDate());
            file.setLastModified(dfile.getModifiedDate().getValue());

            Log.d(this, "setFileMeta() - # createdDate = " + dfile.getCreatedDate());
            file.setCreated(dfile.getCreatedDate().getValue());

            String thumb = dfile.getThumbnailLink();
            Log.d(this, "setFileMeta() - # thumb link = " + thumb);
            if (thumb != null) {
                file.setHasThumbnail(true);
                file.setThumbnailURL(thumb);
            } else {
                file.setHasThumbnail(false);
            }

            Long fileVersion = dfile.getVersion();
            Log.d(this, "setFileMeta() - # file version = " + fileVersion);
            if (fileVersion != null) {
                file.setRevision(dfile.getVersion().toString());
            }

            String path = file.getParent().getPath();

            if (file.getParent().isRoot()) {
                path += dfile.getTitle();
            } else {
                path += "/" + dfile.getTitle();
            }

//			if(path.startsWith("null")) {
//				path = path.replaceFirst("null", "");
//			}

            Log.d(this, "setFileMeta() - # parent = " + file.getParent());
            Log.d(this, "setFileMeta() - # path = " + path);
            if (path != null) {
                file.setPath(path);
            }

            if (!file.isReallyDirectory()) {
                file = new CloudFile(file);
            }

            if (((CloudDirectory) listener).getFilePath(file.getPath()) == null) {
                if (!file.isDeleted()) { // it's not trashed file
                    Log.d(this, "setFileMeta() - listener.addFile : " + file);
                    listener.addFile(file, file.getParent());
                } else {
                    Log.e(this, "setFileMeta() - trashed file, so not added!");
                }
            } else {
                Log.e(this, "setFileMeta() - listener.addFile already exist : " + file);
                if (file.isDeleted()) {
                    Log.d(this, "setFileMeta() - trashed file. removeFile() will called");
                    file.getParent().removeFile(file, file.getParent());
                    if (metalistener != null) {
                        metalistener.removeFile(file, file.getParent());
                    }
                }
            }

            CloudStorageBase.MediaType fileMediaType = MediaType.forFile(file);
            Log.d(this, "setFileMeta() - mediaType : " + mediaType + " ,metalistener : " + metalistener);

            if (mediaType != null && metalistener != null && !file.isDeleted()) {
                // we're only interested in <this.mediaType>
                if (fileMediaType != mediaType) {
                    Log.d(this, "setFileMeta() - fileMediaType : " + fileMediaType + " | mediaType : " + mediaType);

                    if (file.isReallyDirectory()) {
                        metalistener.addFile(file, file.getParent());
                    }
                    return;
                } else {
                    Log.d(this, "setFileMeta() - metalistener added target file: " + file + " ,parent file: " + file.getParent());
                    metalistener.addFile(file, file.getParent());
                }
            }
        }
    }

    private void parseMetadata(
            com.google.api.services.drive.model.File dfile,
            CloudDirectory directory,
            AddFileListener listener,
            CloudStorageBase.MediaType mediaType) {
        Log.d(this, "parseMetadata(),----------------------START----------------------");
        Log.d(this, "parseMetadata() - directory: " + directory + ", dfile : " + dfile.getTitle());

        CloudFile newFile = new CloudDirectory(this, directory, dfile.getTitle());
        setFileMeta(dfile, newFile, directory, listener, mediaType);
    }

    //for delta sync
    private void parseMetadata(Change changeItem) {
        Log.i(this, "parseMetadata() for delta sync called");
        Log.i(this, "------------------------------------------------------");

        CloudFile file = new CloudDirectory(this, null, null);

        Log.d(this, "parseMetadata() - # change id = " + changeItem.getId());

        file.setCloudId(changeItem.getFileId());
        Log.d(this, "parseMetadata() - # cloud id = " + file.getCloudId());

        if (changeItem.getDeleted()) {
            Log.d(this, "parseMetadata() - DELETED FILE!!!!!");
            file.setDeleted(true);
            cloudFilesMap_cache.put(file.getCloudId(), file);
            return;
        }

        com.google.api.services.drive.model.File dfile = changeItem.getFile();

        if (dfile != null) {
            boolean isDir = isDir(dfile);
            file.setReallyDirectory(isDir);
            Log.d(this, "parseMetadata() - # is directory? = " + isDir);

            file.setMimeType(isDir ? MimeType.DIR_MIMETYPE : dfile.getMimeType());
            Log.d(this, "parseMetadata() - # MimeType = " + file.getMimeType());

            if (dfile.getLabels().getTrashed()) {
                Log.d(this, "parseMetadata() - # trashed");
                file.setDeleted(true);
                cloudFilesMap_cache.put(file.getCloudId(), file);
                return;
            }

            file.setParentCloudId(getParentsCloudId(dfile));
            Log.d(this, "parseMetadata() - # parentCloudId = " + file.getParentCloudId());

            file.setName(dfile.getTitle());
            Log.d(this, "parseMetadata() - # title = " + file.getName());

            //jsublee_150901 - supporting google docs
            file.setDownloadURL(dfile.getAlternateLink());
            Log.d(this, "parseMetadata() - # DownloadURL = " + file.getDownloadURL());

            Long fileSize = dfile.getFileSize();
            Log.d(this, "parseMetadata() - # filesize = " + fileSize);
            if (fileSize != null) {
                file.setLength(fileSize);
            }

            Log.d(this, "parseMetadata() - # modifiedDate = " + dfile.getModifiedDate());
            file.setLastModified(dfile.getModifiedDate().getValue());

            Log.d(this, "parseMetadata() - # createdDate = " + dfile.getCreatedDate());
            file.setCreated(dfile.getCreatedDate().getValue());

            String thumb = dfile.getThumbnailLink();
            Log.d(this, "parseMetadata() - # hadThumb = " + thumb);
            if (thumb != null) {
                file.setHasThumbnail(true);
                file.setThumbnailURL(thumb);
            } else {
                file.setHasThumbnail(false);
            }

            Long fileVersion = dfile.getVersion();
            Log.d(this, "parseMetadata() - # file Version = " + fileVersion);
            if (fileVersion != null) {
                file.setRevision(dfile.getVersion().toString());
            }
        }

        cloudFilesMap_cache.put(file.getCloudId(), file);
    }

    public boolean doDeltaSync(long startChangeId, AddFileListener listener) throws IOException {
        Log.i(this, "doDeltaSync() called, startChangeId = " + startChangeId);

        if (startChangeId < 0) {
            //In case of this, we need to do full sync.
            return false;
        }

        try {
            if (changeItems_cache == null) { // If there is no cache data
                changeItems_cache = retrieveAllChanges(mDrive, new Long(startChangeId));

                if (changeItems_cache == null) { // If retrieving changes failed
                    return false;
                }

                for (Change item : changeItems_cache) {
                    parseMetadata(item);
                }
                cloudFilesList_cache = new ArrayList<>(cloudFilesMap_cache.values());
            }

            for (CloudFile file : cloudFilesList_cache) {
                setFileMeta(file, listener);
            }
        } catch (UserRecoverableAuthIOException e) {
            throw e;
        } catch (Exception e) {
            Log.e(this, "doDeltaSync() - Exception : " + e.getMessage());
            return false;
        }
        return true;
    }

    public CloudDirectory getDirectoryFromQtoSync() {
        synchronized (cloudDirListQToSync) {
            Log.i(this, "getDirectoryFromQtoSync()");
            if (!cloudDirListQToSync.isEmpty()) {
                return cloudDirListQToSync.remove();
            } else {
                Log.i(this, "getDirectoryFromQtoSync(), Q has no item.");
                return null;
            }
        }
    }

    private static int sPrevLevel = 0;
    private static int sLevelDirCnt = 0;
    private final static int sMAX_LEVEL_CNT = 13; // # of items at list view

    static class APIBatchRequest {
        public boolean bBatchSuccess;
        public int mBatchCountFile;
        public boolean isFirstBatch;
        public ArrayList<String> arrListPageToken;

        public APIBatchRequest() {
            mBatchCountFile = 0;
            isFirstBatch = true;
            arrListPageToken = new ArrayList<String>();
        }

        public void doBatchExecute(BatchRequest batch) {
            try {
                Log.d(this, "doBatchExecute() - Sync - execute ");
                batch.execute();
            } catch (IOException e) {
                Log.e(this, "doBatchExecute() - Sync - IOException : " + e.getMessage());
            }
        }
    }

    public boolean syncDirectoryBatch(final ArrayList<CloudDirectory> targetDir) throws Exception {
        final APIBatchRequest request = new APIBatchRequest();
        JsonBatchCallback<com.google.api.services.drive.model.FileList> jsonBatchCallback = new JsonBatchCallback<FileList>() {
            @Override
            public void onFailure(GoogleJsonError googleJsonError, HttpHeaders httpHeaders) throws IOException {
                Log.e(this, "syncDirectoryBatch() - Fail to get File: " + googleJsonError.getMessage());
                request.bBatchSuccess = false;
            }

            @Override
            public void onSuccess(FileList fileList, HttpHeaders httpHeaders) throws IOException {
                Log.i(this, "syncDirectoryBatch() success folder " + targetDir.get(request.mBatchCountFile).getName());
                boolean bSuccess = false;
                String sNextPageToken = null;
                if (fileList != null) {
                    bSuccess = getChildFileBatch(fileList, targetDir.get(request.mBatchCountFile));
                    sNextPageToken = fileList.getNextPageToken();
                    request.arrListPageToken.add(request.mBatchCountFile, sNextPageToken);
                    Log.e(this, "syncDirectoryBatch() - NextPageToken: " + sNextPageToken);
                }
                // if all items is synced NextPageToken is Null
                if (bSuccess && sNextPageToken == null) {
                    targetDir.remove(request.mBatchCountFile);
                    request.arrListPageToken.remove(request.mBatchCountFile);
                    request.mBatchCountFile--;
                    request.bBatchSuccess = true;

                } else {
                    request.bBatchSuccess = false;
                }
                request.mBatchCountFile++;
                request.isFirstBatch = false;
            }
        };

        try {
            do {
                BatchRequest batchRequest = mDrive.batch();
                if (request.isFirstBatch) {
                    for (int i = 0; i < targetDir.size(); i++) {
                        String query = "'" + targetDir.get(i).getCloudId() + "'" + " in parents";
                        mDrive.files().list().setQ(query).setMaxResults(50).queue(batchRequest, jsonBatchCallback);
                        cloudStorage.deleteChildrenFromDB(targetDir.get(i).getCloudId());
                    }
                } else {
                    request.mBatchCountFile = 0;
                    for (int i = 0; i < targetDir.size(); i++) {
                        String query = "'" + targetDir.get(i).getCloudId() + "'" + " in parents";
                        if (request.arrListPageToken.get(i) != null) {
                            mDrive.files().list().setQ(query).setMaxResults(50).setPageToken(request.arrListPageToken.get(i)).queue(batchRequest, jsonBatchCallback);
                        }
                    }
                }

                request.doBatchExecute(batchRequest);

                if (!request.bBatchSuccess) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        Log.e(this, "syncDirectoryBatch() - InterruptedException: " + e.toString());
                    }
                }
            } while (cloudStorage.isSignedIn() && (!request.bBatchSuccess || targetDir.size() > 0));
        } catch (IOException e) {
            Log.e(this, "syncDirectoryBatch()-IOException: " + e.getMessage());
        }
        Log.d(this, "syncDirectoryBatch : signInStatus - " + cloudStorage.isSignedIn());
        return request.bBatchSuccess;
    }

    private void applyDB(ArrayList<ContentProviderOperation> operationList) {
        try {
            cloudStorage.getContentResolver().applyBatch(ASPMediaStore.AUTHORITY, operationList);
        } catch (RemoteException re) {
            Log.e(this, "applyDB() - RemoteException : " + re.getMessage());
        } catch (OperationApplicationException aoe) {
            Log.e(this, "applyDB() - OperationApplicationException : " + aoe.getMessage());
        } finally {
            operationList.clear();
        }
    }

    public boolean bulkUpdate(ArrayList<com.google.api.services.drive.model.File> fileList) {
        ArrayList<ContentProviderOperation> operationList = new ArrayList<>();
        Uri uri = cloudStorage.getDeviceContentUri();
        String source_id = ASPMediaStore.MediaColumns.SOURCE_MEDIA_ID;
        String selection =  source_id + "=?";

        for (com.google.api.services.drive.model.File file : fileList) {
            if (!cloudStorage.isSignedIn()) {
                Log.d(this, "bulkUpdate() - logout 2");
                return false;
            }

            ContentValues contentValues = null;
            try (Cursor cursor = cloudStorage.getContentResolver().query(uri, new String[]{source_id},
                    selection, new String[] {file.getId()}, null)) {
                if ((cursor != null) && (cursor.moveToFirst())) {
                    Log.d(this, "bulkUpdate() - update file");
                    contentValues = toContentValues(makeParent(file), file);
                    ContentProviderOperation.Builder op = ContentProviderOperation.
                            newUpdate(uri).withSelection(selection, new String[]{file.getId()}).withValues(contentValues);
                    operationList.add(op.build());
                } else {
                    contentValues = toContentValues(file);
                    ContentProviderOperation.Builder op = ContentProviderOperation.newInsert(uri).withValues(contentValues);
                    operationList.add(op.build());
                }
            }

            if (operationList.size() >= CloudStorageBase.BULK_OPERATION_LIMIT) {
                applyDB(operationList);
            }
        }

        if (!cloudStorage.isSignedIn()) {
            Log.d(this, "bulkUpdate() - logout 3");
            return false;
        }

        if (operationList.size() > 0) {
            applyDB(operationList);
        }
        return true;
    }

    public boolean bulkInsert(ArrayList<com.google.api.services.drive.model.File> fileList) {
        ArrayList<ContentProviderOperation> operationList = new ArrayList<>();
        Uri uri = cloudStorage.getDeviceContentUri();
        for (com.google.api.services.drive.model.File file : fileList) {
            if (!cloudStorage.isSignedIn()) {
                Log.d(this, "bulkInsert() - logout 2");
                return false;
            }
            ContentValues contentValues = toContentValues(file);
            ContentProviderOperation.Builder op = ContentProviderOperation.newInsert(uri).withValues(contentValues);
            operationList.add(op.build());

            if (operationList.size() >= CloudStorageBase.BULK_OPERATION_LIMIT) {
                applyDB(operationList);
            }
        }

        if (!cloudStorage.isSignedIn()) {
            Log.d(this, "bulkInsert() - logout 3");
            return false;
        }

        if (operationList.size() > 0) {
            applyDB(operationList);
        }
        return true;
    }

    private CloudDirectory makeParent(File file) {
        Log.d(this, "makeParent()");
        if (file == null || file.getParents() == null) {
            return null;
        }
        CloudDirectory parentDir = new CloudDirectory(this, null, null);
        parentDir.setCloudId(getParentsCloudId(file));
        parentDir.setPath(getParentPath(file));
        return parentDir;
    }

    private boolean bulkInsertCloudFile(ArrayList<CloudFile> fileList) {
        ArrayList<ContentProviderOperation> operationList = new ArrayList<>();
        Uri uri = cloudStorage.getDeviceContentUri();
        for (CloudFile file : fileList) {
            if (!cloudStorage.isSignedIn()) {
                Log.d(this, "bulkInsertCloudFile() - logout 2");
                return false;
            }
            ContentValues contentValues = toContentValues(file);
            ContentProviderOperation.Builder op = ContentProviderOperation.newInsert(uri).withValues(contentValues);
            operationList.add(op.build());

            if (operationList.size() >= CloudStorageBase.BULK_OPERATION_LIMIT) {
                applyDB(operationList);
            }
        }

        if (!cloudStorage.isSignedIn()) {
            Log.d(this, "bulkInsertCloudFile() - logout 3");
            return false;
        }

        if (operationList.size() > 0) {
            applyDB(operationList);
        }
        return true;
    }

    public boolean getChildFileBatch(com.google.api.services.drive.model.FileList fileList, CloudDirectory parentDir) {
        ArrayList<CloudFile> parsedChildList = new ArrayList<CloudFile>();
        int level = parentDir.getLevel();
        if (level != sPrevLevel) {
            Log.i(this, "syncDirectory() - curLevel : " + level);
            sPrevLevel = level;
            sLevelDirCnt = 0;
            if (level >= 2) {
                CachedFileBrowser.checkAllInstances();
            }
        } else {
            sLevelDirCnt++;
            if (sLevelDirCnt > sMAX_LEVEL_CNT) {
                CachedFileBrowser.checkAllInstances();
            }
        }
        List<com.google.api.services.drive.model.File> fileL = fileList.getItems();
        for (com.google.api.services.drive.model.File dFile : fileL) {
            if (!cloudStorage.isSignedIn()) {
                Log.d(this, "getChildFileBatch - logout 1");
                return false;
            }
            CloudFile file = parseMetadataInTreeTraversal(parentDir, dFile);

            if (!file.isDeleted()) {
                parsedChildList.add(file);
                if (file.isReallyDirectory()) {
                    synchronized (cloudDirListQToSync) {
                        Log.i(this, "syncDirectoryBatch() - add Q -> " + file);
                        ((CloudDirectory) file).setLevel(level + 1);
                        cloudDirListQToSync.add((CloudDirectory) file);
                        getDatabaseHelper().findOrSaveDirectory((CloudDirectory) file);
                    }
                }
            }
        }
        bulkInsertCloudFile(parsedChildList);

        // In the case of tree traversal,
        // I'll use the cloudFilesMap_cache as the collection of the sync completed directories.
        cloudFilesMap_cache.put(parentDir.getCloudId(), parentDir);

        return true;
    }

    CloudFile parseMetadataInTreeTraversal(CloudFile parentDir, com.google.api.services.drive.model.File dFile) {
        Log.i(this, "parseMetadataInTreeTraversal() called");
        Log.i(this, "------------------------------------------------------");
        CloudFile file = new CloudDirectory(this, null, null);

        file.setName(dFile.getTitle());
        Log.d(this, "parseMetadataInTreeTraversal() - # filename = " + file.getName());

        boolean isDir = isDir(dFile);
        file.setReallyDirectory(isDir);
        Log.d(this, "parseMetadataInTreeTraversal() - # is Directory = " + isDir);

        file.setMimeType(isDir ? MimeType.DIR_MIMETYPE : dFile.getMimeType());
        Log.d(this, "parseMetadataInTreeTraversal() - # mimetype = " + file.getMimeType());

        file.setCloudId(dFile.getId());
        Log.d(this, "parseMetadataInTreeTraversal() - # cloudId = " + file.getCloudId());

        file.setParentCloudId(parentDir.getCloudId());
        Log.d(this, "parseMetadataInTreeTraversal() - # parentCloudId = " + file.getParentCloudId());

        file.setParentName(parentDir.getName());
        Log.d(this, "parseMetadataInTreeTraversal() - # parentName = " + file.getParentName());

        //supporting google docs
        file.setDownloadURL(dFile.getAlternateLink());
        Log.d(this, "parseMetadataInTreeTraversal() - # alternateLink = " + file.getDownloadURL());

        file.setDeleted(dFile.getLabels().getTrashed());
        Log.d(this, "parseMetadataInTreeTraversal() - # is trashed? = " + dFile.getLabels().getTrashed());

        Long fileSize = dFile.getFileSize();
        Log.d(this, "parseMetadataInTreeTraversal() - # size = " + fileSize);
        if (fileSize != null) {
            file.setLength(fileSize);
        }

        Log.d(this, "parseMetadataInTreeTraversal() - # modifiedDate = " + dFile.getModifiedDate());
        file.setLastModified(dFile.getModifiedDate().getValue() / 1000); //devide by 1000 for cloudgateway

        Log.d(this, "parseMetadataInTreeTraversal() - # createdDate = " + dFile.getCreatedDate());
        file.setCreated(dFile.getCreatedDate().getValue());

        String thumb = dFile.getThumbnailLink();
        Log.d(this, "parseMetadataInTreeTraversal() - # thumb link = " + thumb);
        if (thumb != null) {
            file.setHasThumbnail(true);
            file.setThumbnailURL(thumb);
        } else {
            file.setHasThumbnail(false);
        }

        String path = "/" + dFile.getTitle();
        if (!parentDir.getPath().equals("/")) {
            path = parentDir.getPath() + path;
        }

        Log.d(this, "parseMetadataInTreeTraversal() - # path = " + path);
        file.setPath(path);

        return file;
    }

    private ContentValues toContentValues(CloudFile file) {
        ContentValues values = new ContentValues();

        values.put(ASPMediaStore.BaseASPColumns.DEVICE_ID, cloudStorage.getDeviceId());
        values.put(ASPMediaStore.BaseASPColumns.SOURCE_MEDIA_ID, file.getCloudId());
        values.put(ASPMediaStore.BaseASPColumns.PARENT_CLOUD_ID, file.getParentCloudId());
        values.put(ASPMediaStore.Audio.AudioColumns.DATE_MODIFIED, file.getLastModified());
        values.put(ASPMediaStore.Audio.AudioColumns.DISPLAY_NAME, file.getName());
        values.put(ASPMediaStore.Audio.AudioColumns.MIME_TYPE, file.getMimeType());
        values.put(ASPMediaStore.Audio.AudioColumns.SIZE, file.length());
        values.put(ASPMediaStore.Audio.AudioColumns.FULL_URI, file.getDownloadURL());
        values.put(ASPMediaStore.Audio.AudioColumns.DATA, file.getPath());

        return values;
    }

    private ContentValues toContentValues(com.google.api.services.drive.model.File dFile) {
        ContentValues values = new ContentValues();

        values.put(ASPMediaStore.BaseASPColumns.DEVICE_ID, cloudStorage.getDeviceId());
        values.put(ASPMediaStore.BaseASPColumns.SOURCE_MEDIA_ID, dFile.getId());
        values.put(ASPMediaStore.BaseASPColumns.PARENT_CLOUD_ID, getParentsCloudId(dFile));
        values.put(ASPMediaStore.Audio.AudioColumns.DATE_MODIFIED, dFile.getModifiedDate().getValue() / 1000);
        values.put(ASPMediaStore.Audio.AudioColumns.DISPLAY_NAME, dFile.getTitle());
        values.put(ASPMediaStore.Audio.AudioColumns.MIME_TYPE, isDir(dFile) ? MimeType.DIR_MIMETYPE : dFile.getMimeType());
        values.put(ASPMediaStore.Audio.AudioColumns.SIZE, dFile.getFileSize());
        values.put(ASPMediaStore.Audio.AudioColumns.FULL_URI, dFile.getAlternateLink());
//        values.put(ASPMediaStore.Audio.AudioColumns.DATA, getFileFullPathFromCache(dFile.getId()));

        return values;
    }

    private ContentValues toContentValues(CloudDirectory parentDir, com.google.api.services.drive.model.File dFile) {
        ContentValues values = new ContentValues();

        String path = "/" + dFile.getTitle();
        if ((parentDir != null) && parentDir.getPath() != null && !parentDir.getPath().equals("/")) {
            path = parentDir.getPath() + path;
        }

        values.put(ASPMediaStore.BaseASPColumns.DEVICE_ID, cloudStorage.getDeviceId());
        values.put(ASPMediaStore.BaseASPColumns.SOURCE_MEDIA_ID, dFile.getId());
        values.put(ASPMediaStore.BaseASPColumns.PARENT_CLOUD_ID, getParentsCloudId(dFile));
        values.put(ASPMediaStore.Audio.AudioColumns.DATE_MODIFIED, dFile.getModifiedDate().getValue() / 1000);
        values.put(ASPMediaStore.Audio.AudioColumns.DISPLAY_NAME, dFile.getTitle());
        values.put(ASPMediaStore.Audio.AudioColumns.MIME_TYPE, isDir(dFile) ? MimeType.DIR_MIMETYPE : dFile.getMimeType());
        values.put(ASPMediaStore.Audio.AudioColumns.SIZE, dFile.getFileSize());
        values.put(ASPMediaStore.Audio.AudioColumns.FULL_URI, dFile.getAlternateLink());
        values.put(ASPMediaStore.Audio.AudioColumns.DATA, path);

        return values;
    }

    @Override
    public boolean loadDirectoryFiles(CloudFile file, AddFileListener listener) throws Exception {
        Log.d(this, "loadDirectoryFiles() called");

        if (file == null) {
            Log.d(this, "loadDirectoryFiles() - file = null, return false");
            return false;
        }

        CloudDirectory directory = (CloudDirectory) file;
        Log.d(this, "loadDirectoryFiles() - file path = " + file.getPath() + ", file name = : " + file.getName());

        List<com.google.api.services.drive.model.File> filelist = getChildFiles(directory.isRoot() ? CloudStorageConstants.CLOUD_ROOT : directory.getCloudId());
        Log.d(this, "loadDirectoryFiles() - filelist is : " + filelist);

        boolean success = (filelist != null);
        if (success) {
            for (com.google.api.services.drive.model.File dfile : filelist) {
                parseMetadata(dfile, directory, null, null);
            }
        }

        Log.d(this, "loadDirectoryFiles() - return " + success);

        return success;
    }

    public void putDB(com.google.api.services.drive.model.File dFile) {
        CloudDirectory parentDir = new CloudDirectory(this, null, null);
        parentDir.setCloudId(getParentsCloudId(dFile));
        cloudStorage.getContentResolver().insert(cloudStorage.getDeviceContentUri(), toContentValues(parentDir, dFile));

        if (isDir(dFile)) {
            CloudDirectory dir = new CloudDirectory(this, parentDir, dFile.getTitle());
            dir.setCloudId(dFile.getId());
            dir.setRoot(false);
            getDatabaseHelper().findOrSaveDirectory(dir);
        }
    }

    @Override
    public CloudStreamInfo getFile(String fileId, String contentRange) throws FileNotFoundException, CloudUnauthorizedException {
        CloudFile file = new CloudFile(this, null, null);
        file.setCloudId(fileId);
        CloudStreamInfo cloudStream = new CloudStream(this, file, contentRange);
        return cloudStream;
    }

    // file or directory name can't contain these characters. If so, they'll be replaced with INVALID_REPLACE_WITH
    private static Pattern INVALID_CHARS_PATTERN = Pattern.compile("[\\Q/\\<>:\"*?|\\E]");

    // file or directory name can't equal any of these strings
    private static List<String> INVALID_LIST = Arrays.asList("AUX",
            "CON",
            "COM0",
            "COM1",
            "COM2",
            "COM3",
            "COM4",
            "COM5",
            "COM6",
            "COM7",
            "COM8",
            "COM9",
            "LPT0",
            "LPT1",
            "LPT2",
            "LPT3",
            "LPT4",
            "LPT5",
            "LPT6",
            "LPT7",
            "LPT8",
            "LPT9",
            "NUL",
            "PRN");

    /**
     * santize filename according to Cloud Storage restrictions.
     * eg: http://windows.microsoft.com/en-GB/skydrive/upload-file-cant-faq#
     * prevent any files that:
     * - begin or end with a space, end with a period, or include any of these characters: / \ < > : * " ? |
     * - or equal: AUX, PRN, NUL, CON, COM0, COM1, COM2, COM3, COM4, COM5, COM6, COM7, COM8, COM9, LPT0, LPT1, LPT2, LPT3, LPT4, LPT5, LPT6, LPT7, LPT8, LPT9
     */
    @Override
    public String sanitizeFileName(String filename) {
        if (filename == null) {
            return null;
        }
        // replace any invalid characters with INVALID_REPLACE_WITH
        String clean = INVALID_CHARS_PATTERN.matcher(filename.trim()).replaceAll(CloudContext.INVALID_REPLACE_WITH);

        // make sure no period at the end
        if (clean.endsWith(".")) {
            // find last non-period and cut string up to that point
            int i = clean.length() - 2; // NOTE: -2 because we already know last character is "."
            for (; i >= 0; i--) {
                if (clean.charAt(i) != '.') {
                    break;
                }
            }
            if (i >= 0) {
                clean = clean.substring(0, i + 1);
            } else {
                // filename is all periods! ie: "...." - just create file/directory with INVALID_REPLACE_WITH
                clean = CloudContext.INVALID_REPLACE_WITH;
            }
        }

        // check for invalid file/dir names
        if (INVALID_LIST.contains(clean)) {
            // NOTE: we could fail here or rename file/directory.
            // Since user can't rename files in ASP, I'm renaming to allow this to succeed
            clean += CloudContext.INVALID_REPLACE_WITH;
        }

        // log only if filename has changed (don't even do string compare if logging not enabled)
        if (!clean.equals(filename)) {
            Log.d(this, "sanitizeFileName() - \"" + filename + "\" -> \"" + clean + "\"");
        }
        return clean;
    }

    /**
     * Retrieve a list of Change resources.
     *
     * @param service       Drive API service instance.
     * @param startChangeId ID of the change to start retrieving subsequent
     *                      changes from or {@code null}.
     * @return List of Change resources.
     */
    private List<Change> retrieveAllChanges(Drive service, Long startChangeId) throws IOException {
        Log.i(this, "retrieveAllChanges() called");
        List<Change> result = new ArrayList<Change>();
        Changes.List request = service.changes().list();
        Long largestChangeId = null;

        if (startChangeId != null) {
            request.setStartChangeId(startChangeId);
        }

        try {
            int retryCnt = 0;
            String nextPageToken = null;
            do {
                try {
                    Log.i(this, "retrieveAllChanges() - retryCnt = " + retryCnt);
                    ChangeList changes = request.execute();
                    retryCnt = 0;
                    if (changes != null) {
                        nextPageToken = changes.getNextPageToken();
                        largestChangeId = changes.getLargestChangeId();
                        result.addAll(changes.getItems());
                        request.setPageToken(nextPageToken);
                    }
                } catch (UserRecoverableAuthIOException e) {
                    Log.e(this, "retrieveAllChanges() - UserRecoverableAuthIOException : " + e.getMessage());
                    throw e;
                } catch (GoogleJsonResponseException e) {
                    Log.e(this, "retrieveAllChanges() - GoogleJsonResponseException : " + e.getMessage());
                    if (e.getDetails().getCode() == 403) {
                        String reason = e.getDetails().getErrors().get(0).getReason();
                        if ((reason != null) && (reason.equals("rateLimitExceeded") || (reason.equals("userRateLimitExceeded")))) {
                            Thread.sleep(1000);//(1 << retryCnt) * 1000 + randomGenerator.nextInt(1001));
                            request.setPageToken(nextPageToken);
                            ++retryCnt;
                        }
                    }
                } catch (IOException e) {
                    Log.e(this, "retrieveAllChanges() - IOException : " + e.getMessage());
                    request.setPageToken(null);
                    return null;
                }
            }
            while (cloudStorage.isSignedIn()
                    && ((retryCnt > 0) || (request.getPageToken() != null && request.getPageToken().length() > 0)));
        } catch (InterruptedException e) {
            Log.e(this, "retrieveAllChanges() - InterruptedException : " + e.getMessage());
        }

        cloudStorage.setLargestChangeID(largestChangeId.longValue());

        return result;
    }

    public String getParentsCloudId(com.google.api.services.drive.model.File dFile) {
        List<ParentReference> parentReferences = dFile.getParents();
        ParentReference p = parentReferences.get(0);
        return (p.getIsRoot()) ? CloudStorageConstants.CLOUD_ROOT : p.getId();
    }

    private String getParentPath(com.google.api.services.drive.model.File dFile) {
        Log.d(this, "getParentPath()");
        String ret = null;
        String parentId = getParentsCloudId(dFile);
        Uri uri = cloudStorage.getDeviceContentUri();
        String selection = ASPMediaStore.BaseASPColumns.SOURCE_MEDIA_ID + "=?";
        if (parentId != null) {
            try (Cursor cursor = cloudStorage.getContentResolver().query(uri,
                    new String[]{ASPMediaStore.Audio.AudioColumns.DATA}, selection, new String[]{parentId}, null)) {
                if (cursor != null && cursor.moveToFirst()) {
                    ret = cursor.getString(cursor.getColumnIndex(ASPMediaStore.Audio.AudioColumns.DATA));
                }
            }
        }
        return ret;
    }

    public void initCacheForSync() {
        driveFilelistCache.clear();
        changeItems_cache = null;
        cloudFilesMap_cache.clear();
        cloudFilesList_cache = null;
        cloudDirListQToSync = null;
    }

    //jsublee_150907
    public String getFileFullPathFromCache(String cloudId) {
        String fullPath = null;

        try {
            if (CloudStorageConstants.CLOUD_ROOT.equals(cloudId)) {
                fullPath = "/";
            } else {
                CloudFile file = cloudFilesMap_cache.get(cloudId);
                if (file.getParentCloudId().equals(CloudStorageConstants.CLOUD_ROOT)) {
                    fullPath = "/" + file.getName();
                } else {
                    fullPath = getFileFullPathFromCache(file.getParentCloudId()) + "/" + file.getName();
                }

                //jsub12_150922
                if (fullPath.startsWith("null")) {  // in case rename or create foder
                    if (file.getParentCloudId().equals(CloudStorageConstants.CLOUD_ROOT)) {
                        fullPath = "/" + file.getName();
                    } else {
                        Metadata fileMetadata = cloudStorage.getFileMetadata(MediaType.none, file.getParentCloudId());
                        if (fileMetadata != null) {
                            fullPath = fileMetadata.fullPath;
                        } else {
                            Log.e(this, "getFileFullPathFromCache() - fileMetadata is null");
                        }
                        fullPath = fullPath + "/" + file.getName();
                    }
                }
            }
        } catch (Exception e) {
            Log.e(this, "getFileFullPathFromCache() - Exception : " + e.getMessage());
            fullPath = null;
        }

        return fullPath;
    }
}

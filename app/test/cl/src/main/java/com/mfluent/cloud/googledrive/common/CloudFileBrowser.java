/**
 *
 */

package com.mfluent.cloud.googledrive.common;

import java.io.IOException;
import java.text.CollationKey;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import android.database.DataSetObserver;


import com.mfluent.asp.common.datamodel.ASPFileBrowser;
import com.mfluent.asp.common.datamodel.ASPFileSortType;
import com.mfluent.log.Log;

/**
 * each instance represents an ASPFileBrowser for cloud files
 *
 * @author jrenkel
 */
public class CloudFileBrowser implements ASPFileBrowser<CloudFile> {

    final CloudStorageBase<? extends CloudContext> mCloudStorage;

    CloudDirectory mDirectory; //Current Directory

    List<FilteredFile> filtered;

    ASPFileSortType fileSortType = ASPFileSortType.values()[0];

    Set<DataSetObserver> mObservers;

    int directoryCount = 0;

    static class FilteredFile {

        final CloudFile mFile;

        private FilteredFile(CloudFile file) {
            super();
            mFile = file;
        }

        @Override
        public String toString() {
            return mFile.toString();
        }
    }

    public CloudFileBrowser(CloudStorageBase<? extends CloudContext> cloudStorage) {
        super();
        mCloudStorage = cloudStorage;
    }

    @Override
    public boolean init(CloudFile file, ASPFileSortType fileSortType, boolean forceReload) throws InterruptedException, IOException {
        Log.e(this, "file : " + file + ", forceReload : " + forceReload);

        CloudDirectory directory = null;
        if (file != null) {
            if (!file.isDirectory()) {
                throw new IOException("not a directory");
            }
            directory = (CloudDirectory) file;
        } else {
            directory = mCloudStorage.getRootDirectory();
            directory.unloadFiles();
        }

        setDirectory(directory);
        if (doFilter()) {
            sort(fileSortType);
            return true;
        }
        return false;
    }

    @Override
    protected void finalize() throws Throwable {
        destroy();
    }

    @Override
    public void destroy() {
        try {
            setDirectory(null);
            filtered = null;
        } catch (Exception e) {
            Log.d(this, "destroy() - exception : " + e);
        }
    }

    private synchronized void setDirectory(CloudDirectory directory) {
        Log.i(this, "setDirectory() - directory = " + directory);
        Log.i(this, "setDirectory() - previous directory = " + mDirectory);
        if (mDirectory != null) {
            mDirectory.releaseFiles();
        }
        mDirectory = directory;
        if (mDirectory != null) {
            mDirectory.holdFiles();
        }
    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {
        if (mObservers == null) {
            mObservers = new HashSet<DataSetObserver>();
        }
        mObservers.add(observer);
    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {
        if (mObservers == null) {
            return;
        }
        if (mObservers.remove(observer)) {
            if (mObservers.isEmpty()) {
                mObservers = null;
            }
        }
    }

    @SuppressWarnings("unused")
    private void callAllObserversInvalidated() {
        if (mObservers == null) {
            return;
        }
        for (DataSetObserver observer : mObservers) {
            observer.onInvalidated();
        }
    }

    private void sort(ASPFileSortType fileSortType) throws InterruptedException, IOException {
        if (fileSortType == null) {
            fileSortType = ASPFileSortType.DATE_MODIFIED_ASC;
        }
        this.fileSortType = fileSortType;
        doSort();
    }

    /**
     * @param filter
     * @throws IOException
     */
    private boolean doFilter() throws IOException {
        Log.i(this, "doFilter() ");

        if (mDirectory == null) {
            throw new IOException("browser has not been init'ed");
        }

        if (mDirectory.loadFiles(mCloudStorage)) {
            int count = mDirectory.getCount();
            Log.i(this, "doFilter() - getCount() returns " + count);
            filtered = new ArrayList<FilteredFile>(count);

            for (int index = 0; index < count; index++) {
                CloudFile file = mDirectory.getFile(index);
                if (file == null) {
                    throw new RuntimeException("file is null! wtf?!? index was in range!!!");
                }
                FilteredFile filteredFile = new FilteredFile(file);
                filtered.add(filteredFile);
                Log.i(this, "doFilter() - added file to filtered is = " + filteredFile);
                if (file.isDirectory()) {
                    directoryCount++;
                }
            }
            return true;
        }
        return false;
    }

    private void doSort() {
        FilteredFile[] array = filtered.toArray(new FilteredFile[filtered.size()]);
        Arrays.sort(array, new FilteredFileComparator(fileSortType));
        filtered = new ArrayList<FilteredFile>(Arrays.asList(array));
    }

    @Override
    public CloudFile getCurrentDirectory() {
        return mDirectory == null ? null : mDirectory;
    }

    @Override
    public String getCurrentDirectoryAbsolutePath() {
        String path = mDirectory == null ? null : mDirectory.getParent() == null ? CloudContext.PATH_SEPARATOR : mDirectory.getPath();
        return path;
    }

    @Override
    public CloudFile getParentDirectory() {
        return mDirectory == null ? null : mDirectory.getParent();
    }

    @Override
    public int getCount() {
        return filtered == null ? 0 : filtered.size();
    }

    @Override
    public CloudFile getFile(int index) throws InterruptedException, IOException {
        return filtered == null ? null : filtered.get(index).mFile;
    }

    @Override
    public CloudFile getFileNonBlocking(int index) {
        return filtered == null ? null : filtered.get(index).mFile;
    }

    private static class FilteredFileComparator implements Comparator<FilteredFile> {

        private final ASPFileSortType mSortType;

        public FilteredFileComparator(ASPFileSortType sortType) {
            mSortType = sortType;
        }

        @Override
        public int compare(FilteredFile o1, FilteredFile o2) {
            // always sort directories above files
            int dirDiff = directoryFirstDiff(o1, o2);
            if (dirDiff != 0) {
                return dirDiff;
            }

            switch (mSortType) {
                case DATE_MODIFIED_ASC:
                    return lastModifiedDiff(o1, o2);
                case DATE_MODIFIED_DESC:
                    return lastModifiedDiff(o2, o1);
                case SIZE_ASC:
                    return sizeDiff(o1, o2);
                case SIZE_DESC:
                    return sizeDiff(o2, o1);
                case NAME_ASC:
                    return nameDiff(o1, o2);
                case NAME_DESC:
                    return nameDiff(o2, o1);
                case TYPE_ASC:
                    return typeDiff(o1, o2);
                case TYPE_DESC:
                    return typeDiff(o2, o1);
            }

            return 0;
        }

        protected int lastModifiedDiff(FilteredFile o1, FilteredFile o2) {
            long diff = (o1.mFile.lastModified() - o2.mFile.lastModified());
            if (diff == 0) {
                // secondary compare - name
                return nameDiff(o1, o2);
            }
            return diff > 0 ? 1 : -1;
        }

        protected int sizeDiff(FilteredFile o1, FilteredFile o2) {
            long diff = (o1.mFile.length() - o2.mFile.length());
            if (diff == 0) {
                // secondary compare - name
                return nameDiff(o1, o2);
            }
            return diff > 0 ? 1 : -1;
        }

        protected int nameDiff(FilteredFile o1, FilteredFile o2) {
            CollationKey k1 = o1.mFile.getNameCollationKey();
            CollationKey k2 = o2.mFile.getNameCollationKey();

            if ((k1 == null) && (k2 == null)) {
                return 0;
            } else if (k1 == null) {
                return -1;
            } else if (k2 == null) {
                return 1;
            } else {
                return k1.compareTo(k2);
            }
        }

        protected int typeDiff(FilteredFile o1, FilteredFile o2) {
            String extension1 = extension(o1.mFile.getName());
            String extension2 = extension(o2.mFile.getName());
            return stringDiff(extension1, extension2);
        }

        protected String extension(String s1) {
            if (s1 == null) {
                return null;
            }
            int lastIndexOfLastDot = StringUtils.lastIndexOf(s1, '.');
            if (lastIndexOfLastDot < 0) {
                return null;
            }
            return s1.substring(lastIndexOfLastDot + 1);
        }

        protected int stringDiff(String s1, String s2) {
            if (s1 == null && s2 == null) {
                return 0;
            }
            if (s1 == null) {
                return -1;
            }
            if (s2 == null) {
                return +1;
            }
            return s1.compareToIgnoreCase(s2);
        }

        /**
         * always sort directories before files - in any sort type - if both are directories, always sort by name A-Z
         */
        protected int directoryFirstDiff(FilteredFile o1, FilteredFile o2) {
            int o1Val = (o1.mFile instanceof CloudDirectory) ? 0 : 1;
            int o2Val = (o2.mFile instanceof CloudDirectory) ? 0 : 1;

            // if both directories, sort by name A-Z (EXCEPT last modified date, which a directory has)
            if ((mSortType != ASPFileSortType.DURATION_ASC && mSortType != ASPFileSortType.DURATION_DESC) && o1Val == 0 && o2Val == 0) {
                return nameDiff(o1, o2);
            } else {
                return o1Val - o2Val;
            }
        }

    }

    @Override
    public String toString() {
        String path = getCurrentDirectoryAbsolutePath();
        if (path == null) {
            path = "???";
        }
        return "CloudFileBrowser: " + path + ", files: " + getCount();
    }
}

package com.mfluent.cloud.samsungdrive.common;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.mfluent.asp.common.datamodel.ASPMediaStore;
import com.mfluent.log.Log;
import com.samsung.android.sdk.scloud.decorator.drive.DriveFile;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static int DATABASE_VERSION = 1;

    public enum TABLE_TYPE{
        FILE,
        FOLDER,
        BOTH,
    }

    private static final String _ID = "_id";
    public static final String NAME = "name";
    private static final String CLOUD_ID = "cloud_id";
    private static final String PARENT_CLOUD_ID = "parent_cloud_id";
    private static final String IS_ROOT = "is_root";
    public static final String HASH = "_hash";

    public static final String FOLDER_TABLE = "folders";
    private static final String FOLDER_TABLE_DEF = "CREATE TABLE "
            + FOLDER_TABLE
            + "("
            + _ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + NAME
            + " TEXT,"
            + CLOUD_ID
            + " TEXT,"
            + PARENT_CLOUD_ID
            + " TEXT,"
            + IS_ROOT
            + " INTEGER,"
            + " UNIQUE("
            + CLOUD_ID
            + ", "
            + PARENT_CLOUD_ID
            + ") ON CONFLICT REPLACE"
            + ");";

    public static final String FILE_TABLE = "files";
    private static final String FILE_TABLE_DEF = "CREATE TABLE " + FILE_TABLE + " ("
            //Common columns for all media
            + ASPMediaStore.MediaColumns._ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + ASPMediaStore.MediaColumns.DEVICE_ID
            + " INTEGER,"
            + ASPMediaStore.MediaColumns.SOURCE_MEDIA_ID
            + " TEXT,"
            + ASPMediaStore.MediaColumns.DATE_ADDED
            + " INTEGER,"
            + ASPMediaStore.MediaColumns.DATE_MODIFIED
            + " INTEGER,"
            + ASPMediaStore.TrashColumns.LIST_SHOWN
            + " INTEGER,"
            + ASPMediaStore.TrashColumns.TRASHED
            + " INTEGER,"
            + ASPMediaStore.TrashColumns.PROCESSING
            + " TEXT,"
            + ASPMediaStore.MediaColumns.DATA
            + " TEXT,"
            + ASPMediaStore.MediaColumns.MIME_TYPE
            + " TEXT,"
            + ASPMediaStore.MediaColumns.SIZE
            + " INTEGER,"
            + ASPMediaStore.MediaColumns.MEDIA_TYPE
            + " INTEGER,"
            + ASPMediaStore.MediaColumns.FULL_URI
            + " TEXT,"
            + ASPMediaStore.MediaColumns.THUMBNAIL_URI
            + " TEXT,"
            + ASPMediaStore.MediaColumns.DISPLAY_NAME
            + " TEXT,"
            + ASPMediaStore.MediaColumns.PARENT_CLOUD_ID
            + " TEXT,"
            + HASH
            + " TEXT,"
            + ASPMediaStore.MediaColumns.WIDTH
            + " INTEGER,"
            + ASPMediaStore.MediaColumns.HEIGHT
            + " INTEGER,"
            + ASPMediaStore.Images.ImageColumns.ORIENTATION
            + " INTEGER,"
            + "is_loading"
            + " INTEGER DEFAULT 0"
            + ");";

    private static final String FOLDER_PARENT_INDEX_DEF = "CREATE INDEX folder_parent_idx ON " + FOLDER_TABLE + '(' + PARENT_CLOUD_ID + ");";
    private static final String FOLDER_CLOUD_ID_INDEX_DEF = "CREATE INDEX folder_cloud_id_idx ON " + FOLDER_TABLE + '(' + CLOUD_ID + ");";

    private final ObjectCache<String, CloudDirectory> memoryCache;
    private final CloudStorageBase<?> cloudStorageBase;
    private final String[] singleSelectionArg = new String[1];

    public DatabaseHelper(CloudStorageBase<?> cloudStorageBase) {
        super(cloudStorageBase.getApplicationContext(), cloudStorageBase.getName() + ".db", null, DATABASE_VERSION);
        this.cloudStorageBase = cloudStorageBase;
        memoryCache = new ObjectCache<>();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(FILE_TABLE_DEF);
        db.execSQL(FOLDER_TABLE_DEF);
        db.execSQL(FOLDER_PARENT_INDEX_DEF);
        db.execSQL(FOLDER_CLOUD_ID_INDEX_DEF);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        resetDatabase(db);
    }

    private void resetDatabase(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS " + FOLDER_TABLE + ';');
        db.execSQL("DROP TABLE IF EXISTS " + FILE_TABLE + ';');
        //NOTE: dropping a table will automatically cause SQLite to drop the associated indexes.

        onCreate(db);
    }

    public synchronized void update(String tableName, String where, String[] args, ContentValues values) {
        SQLiteDatabase db = getWritableDatabase();
        db.update(tableName, values, where, args);
    }

    public synchronized void delete(String tableName, String where, String[] args) {
        SQLiteDatabase db = getWritableDatabase();
        db.delete(tableName, where, args);
    }

    public synchronized void newInsert(String tableName, ArrayList<ContentValues> values) {
        SQLiteDatabase db = getWritableDatabase();
        Log.d(this, "HJ newInsert : " + values.size());
        for (ContentValues value : values) {
            db.insert(tableName, null, value);
        }
    }

    public synchronized Cursor query(String table, String[] columns, String selection, String[] selectionArgs, String orderBy) {
        SQLiteDatabase db = getReadableDatabase();
        return db.query(table, columns, selection, selectionArgs, null, null, orderBy);
    }

    private CloudDirectory createDirectoryFromContentValues(CloudContext context, ContentValues values, CloudDirectory parent) {

        CloudDirectory result = new CloudDirectory(context, parent, values.getAsString(NAME));
        cloudStorageBase.setFileStorageGatewayId(result, values.getAsString(CLOUD_ID));

        Long databaseId = values.getAsLong(_ID);
        if (databaseId != null) {
            result.setDatabaseId(databaseId.longValue());
        }
        Boolean isRoot = values.getAsBoolean(IS_ROOT);
        if (isRoot != null) {
            result.setRoot(isRoot.booleanValue());
        }
        result.setCloudId(values.getAsString(CLOUD_ID)); //jsub12.lee_150721
        memoryCache.put(values.getAsString(CLOUD_ID), result);

        return result;
    }

    public synchronized CloudDirectory getDirectoryByCloudId(CloudContext context, String cloudId) {
        return getDirectoryByCloudIdInner(context, cloudId);
    }

    private CloudDirectory getDirectoryByCloudIdInner(CloudContext context, String cloudId) {
        Log.i(this, "getDirectoryByCloudIdInner(), cloudId = " + cloudId);
        CloudDirectory result = null;

        //result = this.memoryCache.get(cloudId);  //jsub12.lee

        if (result == null) {
            SQLiteDatabase db = this.getReadableDatabase();
            ContentValues readValues = null;

            this.singleSelectionArg[0] = cloudId;
            Cursor cursor = db.query(FOLDER_TABLE, null, CLOUD_ID + "=?", this.singleSelectionArg, null, null, null);
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    readValues = new ContentValues();
                    DatabaseUtils.cursorRowToContentValues(cursor, readValues);
                }
                cursor.close();
            }

            if (readValues != null) {
                CloudDirectory parent = null;
                String parentId = readValues.getAsString(PARENT_CLOUD_ID);
                if (StringUtils.isNotEmpty(parentId)) {
                    parent = getDirectoryByCloudIdInner(context, parentId);
                }
                result = createDirectoryFromContentValues(context, readValues, parent);
            }
        }

        return result;
    }

    public synchronized CloudDirectory getRootDirectory(CloudContext context) {
        CloudDirectory result = null;

        SQLiteDatabase db = this.getReadableDatabase();
        ContentValues readValues = null;

        this.singleSelectionArg[0] = "1";
        Log.d(this, "getRootDirectory() called");
        Cursor cursor = db.query(FOLDER_TABLE, null, IS_ROOT + "=?", this.singleSelectionArg, null, null, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                readValues = new ContentValues();
                DatabaseUtils.cursorRowToContentValues(cursor, readValues);
            }
            cursor.close();
        }

        if (readValues != null) {
            result = createDirectoryFromContentValues(context, readValues, null);
        }

        return result;
    }

    public synchronized boolean addDirectory(CloudDirectory directory) {

        try {
            ContentValues contentValues = buildContentValuesForDirectory(directory);
            SQLiteDatabase db = this.getWritableDatabase();
            long id = db.insert(FOLDER_TABLE, null, contentValues);
            if (id != 0) {
                directory.setDatabaseId(id);
                this.memoryCache.put(contentValues.getAsString(CLOUD_ID), directory);
                return true;
            }
        } catch (Exception e) {
            Log.e(this, "Error adding CloudDirectory: " + directory + " : " + e);
        }

        return false;
    }

    public synchronized boolean updateDirectory(CloudDirectory directory) {

        boolean success = false;

        if (directory.getDatabaseId() != 0) {
            try {
                ContentValues contentValues = buildContentValuesForDirectory(directory);
                SQLiteDatabase db = this.getWritableDatabase();

                int updated = db.update(FOLDER_TABLE, contentValues, _ID + "=?", new String[]{Long.toString(directory.getDatabaseId())});
                success = updated != 0;
                if (success) {
                    this.memoryCache.put(contentValues.getAsString(CLOUD_ID), directory);
                }
            } catch (Exception e) {
                Log.e(this, "Error updating CloudDirectory: " + directory + " : " + e);
            }
        }

        return success;
    }

    public synchronized void reset() {
        memoryCache.clear();
        close();
        File databaseFile = this.cloudStorageBase.getApplicationContext().getDatabasePath(getDatabaseName());
        if (databaseFile.exists()) {
            databaseFile.delete();
        }
    }

    private ContentValues buildContentValuesForDirectory(CloudDirectory directory) throws FileNotFoundException {
        ContentValues result = new ContentValues();

        result.put(CLOUD_ID, this.cloudStorageBase.getStorageGatewayFileId(directory));
        result.put(PARENT_CLOUD_ID, directory.getParentCloudId()); //jsub12.lee_150721
        result.put(NAME, directory.getName());
        result.put(IS_ROOT, directory.isRoot());

        return result;
    }

    public synchronized boolean saveOrUpdate(CloudDirectory directory) {
        boolean success = false;
        if (directory.getDatabaseId() != 0) {
            success = updateDirectory(directory);
        }
        if (!success) {
            success = addDirectory(directory);
        }

        return success;
    }

    public synchronized List<CloudDirectory> getSubDirectories(CloudContext context, CloudDirectory cloudDirectory) {
        SQLiteDatabase db = getReadableDatabase();
        List<CloudDirectory> result = new ArrayList<CloudDirectory>();

        try {
            this.singleSelectionArg[0] = this.cloudStorageBase.getStorageGatewayFileId(cloudDirectory);
            Cursor cursor = db.query(FOLDER_TABLE, null, PARENT_CLOUD_ID + "=?", this.singleSelectionArg, null, null, null);

            if (cursor != null) {
                ContentValues values = new ContentValues();
                while (cursor.moveToNext()) {
                    values.clear();
                    DatabaseUtils.cursorRowToContentValues(cursor, values);
                    CloudDirectory child = this.memoryCache.get(values.getAsString(CLOUD_ID));
                    if (child == null) {
                        child = createDirectoryFromContentValues(context, values, cloudDirectory);
                    }
                    if (child != null) {
                        result.add(child);
                    }
                }
                cursor.close();
            }
        } catch (Exception e) {
            Log.e(this, "Error getting subdirectory list for: " + cloudDirectory + " : " + e);
        }

        return result;
    }

    public synchronized void deleteDirectory(CloudDirectory directory) {
        ArrayList<String> deleteIds = new ArrayList<String>();
        ArrayList<String> deleteCloudIds = new ArrayList<String>();

        try {
            String cloudId = this.cloudStorageBase.getStorageGatewayFileId(directory);

            SQLiteDatabase db = getWritableDatabase();

            deleteChildDirectoriesRecursive(db, deleteIds, deleteCloudIds, cloudId);

            deleteCloudIds.add(cloudId);
            deleteIds.add(Long.toString(directory.getDatabaseId()));

            String[] args = null;
            StringBuilder sb = new StringBuilder();
            int lastCount = 0;
            for (int i = 0; i < deleteIds.size(); i += 400) {
                int start = i;
                int count = Math.min(deleteIds.size() - i, 400);
                int limit = start + count;

                if (lastCount != count) {
                    args = new String[count];
                    sb.delete(0, sb.length());
                    sb.append(_ID).append(" IN (");
                    for (int j = 0; j < count; ++j) {
                        if (j > 0) {
                            sb.append(',');
                        }
                        sb.append('?');
                    }
                    sb.append(')');
                }
                int pos = 0;
                for (; start < limit; ++start) {
                    args[pos++] = deleteIds.get(start);
                }

                db.delete(FOLDER_TABLE, sb.toString(), args);
            }

            for (int i = 0; i < deleteCloudIds.size(); ++i) {
                this.memoryCache.remove(deleteCloudIds.get(i));
            }
        } catch (Exception e) {
            Log.e(this, "Error deleting directory: " + directory + " : " + e);
        }
    }

    private static String[] ID_CLOUD_ID_PROJECTION = {_ID, CLOUD_ID};

    private void deleteChildDirectoriesRecursive(SQLiteDatabase db, ArrayList<String> deleteIds, ArrayList<String> deleteCloudIds, String cloudId) {
        int startPos = deleteIds.size();
        int numAdded = 0;
        this.singleSelectionArg[0] = cloudId;
        Cursor cursor = db.query(FOLDER_TABLE, ID_CLOUD_ID_PROJECTION, PARENT_CLOUD_ID + "=?", this.singleSelectionArg, null, null, null);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                deleteIds.add(cursor.getString(0));
                deleteCloudIds.add(cursor.getString(1));
                numAdded++;
            }
            cursor.close();
        }

        int limit = startPos + numAdded;
        for (; startPos < limit; ++startPos) {
            deleteChildDirectoriesRecursive(db, deleteIds, deleteCloudIds, deleteCloudIds.get(startPos));
        }
    }

    /**
     * The purpose of this method to prevent multiple copies of a given
     * directory from being in memory at once. This is to be used during
     * a directory list sync with the remote source. During the sync
     * a temporary directory object will be created. If there is one
     * currently in memory, then we want to overwrite the information
     * in the in-memory copy with the new information and then return
     * the in-memory copy. The caller should then release any reference
     * to the temporary directory object. If there is no in-memory
     * directory object, then the passed in directory will be added to
     * the in-memory cache and returned.
     *
     * @param directory
     * @return
     */
    public synchronized CloudDirectory findOrSaveDirectory(final CloudDirectory directory) {
        CloudDirectory result = directory;
        try {
            ContentValues updateValues = buildContentValuesForDirectory(result);
            String cloudId = updateValues.getAsString(CLOUD_ID);
            CloudDirectory foundInMem = this.memoryCache.get(cloudId);
            SQLiteDatabase db = getWritableDatabase();
            long recordId = 0;
            if (foundInMem != null) {
                foundInMem.copyFromDirectory(result);
                result = foundInMem;

                recordId = foundInMem.getDatabaseId();
            } else {
                this.singleSelectionArg[0] = cloudId;
                Cursor cursor = db.query(FOLDER_TABLE, ID_CLOUD_ID_PROJECTION, CLOUD_ID + "=?", this.singleSelectionArg, null, null, null);
                if (cursor != null) {
                    if (cursor.moveToFirst()) {
                        recordId = cursor.getLong(0);
                        directory.setDatabaseId(recordId);
                    }
                    cursor.close();
                }
                memoryCache.put(cloudId, result);
            }
            if (recordId != 0) {
                singleSelectionArg[0] = Long.toString(recordId);
                db.update(FOLDER_TABLE, updateValues, _ID + "=?", singleSelectionArg);
            } else {
                recordId = db.insert(FOLDER_TABLE, null, updateValues);
                directory.setDatabaseId(recordId);
            }

        } catch (Exception e) {
            Log.e(this, "Error in findOrSaveDirectory: " + directory + " : " + e);
        }

        return result;
    }
}

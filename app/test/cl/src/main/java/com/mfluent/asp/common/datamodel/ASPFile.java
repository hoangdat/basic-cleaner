/**
 *
 */

package com.mfluent.asp.common.datamodel;

/**
 * @author Ilan Klinghofer
 */
public interface ASPFile {

    /**
     * Returns the name of the file or directory represented by this file.
     *
     * @return this file's name or an empty string if there is no name part in
     * the file's path.
     */
    String getName();

    /**
     * Indicates if this file represents a <em>directory</em> on the
     * underlying file system.
     *
     * @return {@code true} if this file is a directory, {@code false} otherwise.
     */
    boolean isDirectory();

    /**
     * Returns the length of this file in bytes.
     * Returns 0 if the file does not exist.
     * The result for a directory is not defined.
     * Note: if length is 0, the file length will not be used when a duplication check is performed.
     *
     * @return the number of bytes in this file.
     */
    long length();

    /**
     * Returns the time when this file was last modified, measured in
     * milliseconds since January 1st, 1970, midnight.
     * Returns 0 if the file does not exist.
     *
     * @return the time when this file was last modified.
     */
    long lastModified();

    /**
     * Returns the special directory type, if any. The special directory
     * type is used by UI code to present a localized string representing
     * this directory.
     *
     * @return the special type of this directory. {@code null} of none.
     */
    String getSpecialDirectoryType();

}

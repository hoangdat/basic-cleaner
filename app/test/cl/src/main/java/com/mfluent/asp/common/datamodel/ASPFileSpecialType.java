
package com.mfluent.asp.common.datamodel;

public enum ASPFileSpecialType {
    INTERNAL_STORAGE,
    EXTERNAL_STORAGE,
}

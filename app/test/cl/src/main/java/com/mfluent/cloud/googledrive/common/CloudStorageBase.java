
package com.mfluent.cloud.googledrive.common;

import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.OperationApplicationException;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.CancellationSignal;
import android.os.RemoteException;
import android.text.TextUtils;
import android.util.Base64;

import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.batch.BatchRequest;
import com.google.api.client.googleapis.batch.json.JsonBatchCallback;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAuthIOException;
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException;
import com.google.api.client.googleapis.json.GoogleJsonError;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.googleapis.media.MediaHttpUploader;
import com.google.api.client.googleapis.media.MediaHttpUploaderProgressListener;
import com.google.api.client.http.FileContent;
import com.google.api.client.http.HttpHeaders;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.Drive.Files;
import com.google.api.services.drive.DriveRequest;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.About;
import com.google.api.services.drive.model.ParentReference;
import com.mfluent.asp.cloudstorage.api.sync.CloudStorageAppDelegate;
import com.mfluent.asp.cloudstorage.api.sync.CloudStorageSync;
import com.mfluent.asp.cloudstorage.api.sync.CloudStreamInfo;
import com.mfluent.asp.common.clouddatamodel.AbsCloudContext.AccessTokenListener;
import com.mfluent.asp.common.clouddatamodel.AbsCloudContext.AuthorizationState;
import com.mfluent.asp.common.datamodel.ASPFile;
import com.mfluent.asp.common.datamodel.ASPFileBrowser;
import com.mfluent.asp.common.datamodel.ASPFileSortType;
import com.mfluent.asp.common.datamodel.ASPMediaStore;
import com.mfluent.asp.common.datamodel.CloudDataModel;
import com.mfluent.asp.common.datamodel.CloudDevice;
import com.mfluent.asp.common.datasource.AspMediaId;
import com.mfluent.asp.common.io.util.InterruptibleOutputStream;
import com.mfluent.asp.common.io.util.ProgressUpdatingOutputStream;
import com.mfluent.asp.common.io.util.StreamProgressListener;
import com.mfluent.asp.common.media.thumbnails.ImageInfo;
import com.mfluent.asp.common.util.CloudStorageConstants;
import com.mfluent.asp.common.util.CloudStorageError;
import com.mfluent.asp.common.util.FileTypeHelper;
import com.mfluent.asp.common.util.MimeType;
import com.mfluent.cloud.googledrive.GoogleDriveContext;
import com.mfluent.cloud.googledrive.common.Metadata.Id;
import com.mfluent.cloud.googledrive.exception.CloudAuthorizationException;
import com.mfluent.log.Log;
import com.samsung.android.sdk.slinkcloud.CloudGatewayConstants;
import com.samsung.android.sdk.slinkcloud.CloudGatewayFileBrowserUtils;
import com.samsung.android.sdk.slinkcloud.CloudGatewayMediaStore;

import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;

import platform.com.mfluent.asp.util.AspFileUtilsSLPF;
import platform.com.mfluent.asp.util.FileUtils;
import platform.com.mfluent.asp.util.IOUtils;
import platform.com.mfluent.asp.util.MFLStorageManagerSLPF;
import platform.com.mfluent.asp.util.NetworkUtilSLPF;

/**
 * <p>
 * Base class for a cloud storage connector implementation.
 * </p>
 * <p>
 * It contains definitions, information, and procedures that are:
 * <ul>
 * <li>independent of a specific cloud storage connector, but</li>
 * <li>specific to the All Share - Play application's use of a cloud storage, via implementation of the CloudStorageSync interface.</li>
 * </ul>
 * </p>
 * <p>
 * Implementations of specific cloud storages:
 * <ul>
 * <li>extend this base class,</li>
 * <li>implement abstract methods that are necessary to the procedures in this class,</li>
 * <li>override methods that are not appropriate to the specific cloud storage, and</li>
 * <li>provide an application independent but cloud storage specific "context" which directly or indirectly extends CloudContext.</li>
 * </ul>
 * </p>
 *
 * @param <T> the "context" used by this cloud storage connector
 */
public abstract class CloudStorageBase<T extends CloudContext> implements CloudStorageSync, AccessTokenListener {

    private static final String ACCESS_SECRET_ID = "accessSecret";
    private static final String ACCESS_TOKEN_ID = "accessToken";
    private static final String METADATA_USER_ID = "metadataUserId";
    private static final String LARGEST_CHANGE_ID = "largestChangeId";
    private static final String SIGNIN_STATE = "signin_state";
    private static final String ACCOUNT_NAME = "account_name";
    private static final String AUTH_PICASA_WEB_ALBUMS = "https://picasaweb.google.com/data/";

    private static final String REASON_RATE_LIMIT_EXCEEDED = "rateLimitExceeded";
    private static final String REASON_USER_RATE_LIMIT_EXCEEDED = "userRateLimitExceeded";
    private static final String REASON_STORAGE_QUOTA_EXCEEDED = "quotaExceeded";
    private static final String MESSAGE_EXCEEDED_STORAGE_QUOTA = "The user has exceeded their Drive storage quota";


    public static final int BULK_OPERATION_LIMIT = 250;
    private static final int API_CALL_BATCH_LIMIT = 50;
    private static final int RETRY_MAX_COUNT = 10;
    private static final int RETRY_MAX_BATCH_COUNT = 10;
    private static final int BUFF_SIZE_FAST = 128 * 1024;

    protected boolean isSignIn = false;

    private boolean synchingWithTreeTraversal = false;

    private CloudDirectory syncFirstDir = null;

    //jsub12.lee_150721
    private CancellationSignal mCancellationSignal = null;
    private String mCancellationFileId = null;

    public enum PreferenceType {
        LAST_SYNC_TIME,
        GOOGLE_DRIVE_USED,
        GMAIL_USED,
        PHOTOS_USED,
    }

    /**
     * The media types supported by procedures in this class, listed in the order in which they will sync, if they will sync as indicated by the presence of the
     * metadata columns attribute. <br>
     * <br>
     * The attributes of each media type are:
     * <ul>
     * <li>the prefix of MIME types for this type of media</li>
     * <li>the vector of metadata columns supported for this type of media (not null => this metadata type will be sync'ed)</li>
     * <li>the integer value that identifies this type of media in the All Share - Play application</li>
     * <li>the name of the directory in the cloud storage where files of this media type are found</li>
     * <li>an indication of whether or not artist and album sub-directories are used in the cloud storage for files of this media type</li>
     * <li>an indication of whether or not files of this media type can be captioned</li>
     * <li>an indication of whether or not the metadata for files of this type include a date taken value</li>
     * </ul>
     */
    public enum MediaType {
        /* @formatter:off */
        image(new MimeTypeMatcher("image/"), Metadata.imageColumns, AspMediaId.MEDIA_TYPE_IMAGE, CloudStorageConstants.PHOTO_FOLDER, ASPMediaStore.Images.Media.CONTENT_URI, true),
        audio(new MimeTypeMatcher("audio/"), Metadata.audioColumns, AspMediaId.MEDIA_TYPE_AUDIO, CloudStorageConstants.MUSIC_FOLDER, ASPMediaStore.Audio.Media.CONTENT_URI, true),
        video(new MimeTypeMatcher("video/"), Metadata.videoColumns, AspMediaId.MEDIA_TYPE_VIDEO, CloudStorageConstants.VIDEO_FOLDER, ASPMediaStore.Video.Media.CONTENT_URI, true),
        document(new DocumentMatcher(), Metadata.documentColumns, AspMediaId.MEDIA_TYPE_DOCUMENT, CloudStorageConstants.DOCUMENTS_FOLDER, ASPMediaStore.Documents.Media.CONTENT_URI, false),
        directory(new MimeTypeMatcher(MimeType.DIR_MIMETYPE), Metadata.directoryColumns, AspMediaId.MEDIA_TYPE_DIRECTORY, null, ASPMediaStore.Directory.Media.CONTENT_URI, false), //jsub12.lee_150721
        none(null, Metadata.defaultColumns, AspMediaId.MEDIA_TYPE_NONE, CloudStorageConstants.FILES_FOLDER, null, false), //jsub12_150922
        caption(null, null, AspMediaId.MEDIA_TYPE_CAPTION, CloudStorageConstants.VIDEO_FOLDER, null, false),
        captionIndex(null, null, AspMediaId.MEDIA_TYPE_CAPTION_INDEX, CloudStorageConstants.VIDEO_FOLDER, null, false),
        /* @formatter:on */;

        private interface CloudFileMediaTypeMatcher {

            boolean matches(CloudFile file);
        }

        public final CloudFileMediaTypeMatcher matcher;

        public final String[] columnNames;

        public final int aspMediaType;

        public final String rootSubdirectoryName;

        public final Uri aspContentUri;

        public final boolean hasThumbnails;

        /* @formatter:off */
        MediaType(
                CloudFileMediaTypeMatcher matcher,
                String[] columnNames,
                int aspMediaType,
                String rootSubdirectoryName,
                Uri aspContentUri,
                boolean hasThumbnails) {
            /* @formatter:on */
            this.matcher = matcher;
            this.columnNames = columnNames;
            this.aspMediaType = aspMediaType;
            this.rootSubdirectoryName = rootSubdirectoryName;
            this.aspContentUri = aspContentUri;
            this.hasThumbnails = hasThumbnails;
        }

        /**
         * get the media type for the specified MIME type
         *
         * @param file the CloudFile for which to get the media type
         * @return the media type, if any, for the specified MIME type
         */
        public static MediaType forFile(CloudFile file) {
            MediaType ret = null;
            if (file != null) {
                for (MediaType mediaType : MediaType.values()) {
                    if (mediaType.matcher != null && mediaType.matcher.matches(file)) {
                        ret = mediaType;
                        break;
                    }
                }
            }

            return ret;
        }

        private static class MimeTypeMatcher implements CloudFileMediaTypeMatcher {
            private final String mimeType;

            public MimeTypeMatcher(String mimeType) {
                this.mimeType = mimeType;
            }

            @Override
            public boolean matches(CloudFile file) {
                String fileMimeType = file.getMimeType();
                return fileMimeType != null && fileMimeType.startsWith(mimeType);
            }
        }

        private static class DocumentMatcher implements CloudFileMediaTypeMatcher {
            @Override
            public boolean matches(CloudFile file) {
                return checkMimeType(file.getMimeType()) ? true : FileTypeHelper.isAspDocument(file.getName());
            }

            private boolean checkMimeType(String fileMimeType) {
                boolean bRet = false;
                if (fileMimeType != null) {
                    if (!fileMimeType.contains("image")
                            && !fileMimeType.contains("audio")
                            && !fileMimeType.contains("video")
                            && !fileMimeType.contains("vnd.asp.dir/dir")) {
                        bRet = true;
                    }

                }
                return bRet;
            }
        }
    }

    /**
     * <p>
     * The "types" of cloud storage connectors.
     * </p>
     * <p>
     * Each cloud storage connector implemented as an extension of this class must be in this list.
     * </p>
     * <p>
     * The attributes of each cloud storage connector "type" are:
     * <ul>
     * <li>the "id" of this cloud storage type in the All Share - Play application, and</li>
     * <li>the "name" of this cloud storage type in the Samsung Link application and, more importantly, <b>to the storage gateway.</b></li>
     * </ul>
     * </p>
     * Note: do NOT reference the names in CloudStorageConstants!!! This creates an undesirable "link" between the plugins and the APK!!! <br/>
     * E.g., to add a new connector or to change the name of an existing connector, the APK would have to be modified; this is NOT good.
     */
    public enum Type {
        GOOGLE_DRIVE("16", "google" + "drive");

        final String id;
        final String name;

        Type(String id, String name) {
            this.id = id;
            this.name = name;
        }
    }

    protected final Type type;

    protected final AuthType authType;

    protected final boolean supportsSignUp;

    protected final T context;

    protected CloudDirectory rootDirectory;

    CloudDevice device = null;

    CloudStorageAppDelegate appDelegate = null;

    private final AtomicBoolean syncRunning = new AtomicBoolean(false);

    ContentResolver contentResolver = null;

    Context appContext = null;

    CloudDataModel dataModel = null;

    protected int preferredThumbnailWidth = -1;

    protected int preferredThumbnailHeight = -1;

    private String metadataUserId; // the userId whose metadata is stored

    private IntentFilter[] intentFilters;

    private boolean syncMetadata = true;

    private MetadataSynchronizer metadataSynchronizer;

    private int totalAdded;

    private int totalUnseen;

    private int totalUnchanged;

    private int totalChanged;

    private boolean inUploadBatch = false;

    private long usedCapacity;

    private long uploadBatchStartTime;

    private long numberOfAuthorizeRequests = 0l;

    private long numberOfGetTokenRequests = 0l;

    private long prevLargestChangeId = -1;


    /**
     * constructor for the CloudStorageBase class subclasses *MUST* invoke this constructor (actually, java will force this)
     *
     * @param type           the type of cloud storage that the subclass implements
     * @param authType       the type of authorization this cloud storage type uses
     * @param supportsSignUp true <=> this cloud storage type supports sign-up from the device
     * @param context        the context of this cloud storage connector
     */
    protected CloudStorageBase(Type type, AuthType authType, boolean supportsSignUp, T context) {
        this.type = type;
        this.authType = authType;
        this.supportsSignUp = supportsSignUp;
        this.context = context;
    }

	/*
     * inversion of control setters from interface CloudStorageSync
	 */

    @Override
    public void setCloudDevice(CloudDevice device) {
        this.device = device;
    }

    @Override
    public void setStorageAppDelegate(CloudStorageAppDelegate appDelegate) {
        this.appDelegate = appDelegate;
    }

    @Override
    public void setContentProvider(ContentResolver contentResolver) {
        this.contentResolver = contentResolver;
    }

    protected CloudDirectory instantiateRootNotFoundInDatabase() {
        CloudDirectory rootDirectory = new CloudDirectory(context, null, "");
        rootDirectory.setRoot(true);
        rootDirectory.setCloudId(CloudStorageConstants.CLOUD_ROOT);

        return rootDirectory;
    }

    @Override
    public void setApplicationContext(Context appContext) {
        this.appContext = appContext;
        DatabaseHelper databaseHelper = new DatabaseHelper(this);
        context.setDatabaseHelper(databaseHelper);
        CloudDirectory rootDirectory = databaseHelper.getRootDirectory(context);
        if (rootDirectory == null) {
            rootDirectory = instantiateRootNotFoundInDatabase();
            databaseHelper.saveOrUpdate(rootDirectory);
        }
        rootDirectory.setPath("/");
        rootDirectory.setDescription("user root directory");
        rootDirectory.setRoot(true);

        Log.d(this, "setApplicationContext() - rootDirectory : " + rootDirectory);
        this.rootDirectory = rootDirectory;
    }

    @Override
    public void setDataModel(CloudDataModel dataModel) {
        this.dataModel = dataModel;
    }

    /*
     * inversion of control initialization from interface CloudStorageSync
     */
    @Override
    public void init() {
        SharedPreferences preferences = getSharedPreferences();

		/*
         * initialize the metadata user ID
		 */
        metadataUserId = preferences.getString(METADATA_USER_ID, null);
        isSignIn = preferences.getBoolean(SIGNIN_STATE, false);

        // In case SignIn state is true, get Drive object.
        if (isSignIn) {
            context.setUserId(preferences.getString(ACCOUNT_NAME, null));
            getContext().mDrive = getDrive();
        }
        getContext().setCloudStorage(this);
        prevLargestChangeId = getLargestChangeIdFromPref();
        Log.i(this, "init():: metadataUserId : ["
                + metadataUserId + "], isSignIn : ["
                + isSignIn + "], account name : ["
                + context.getUserId() + "], prevLargestChangeID : ["
                + prevLargestChangeId + "]");
    }

    protected void saveAccountInfoToPref(String _accountName, boolean signInState) {
        Editor editor = getSharedPreferences().edit();

        if (_accountName == null) {
            Log.i(this, "saveAccountInfoToPref() ; ACCOUNT_NAME & SIGNIN_STATE removed");
            editor.remove(ACCOUNT_NAME);
            editor.remove(SIGNIN_STATE);
        } else {
            editor.putString(ACCOUNT_NAME, _accountName);
            editor.putBoolean(SIGNIN_STATE, signInState);
            Log.i(this, "saveAccountInfoToPref() ; ACCOUNT_NAME : " + _accountName + ", SIGNIN_STATE : " + signInState);
        }
        editor.apply();
    }

    @Override
    public IntentFilter[] getIntentFilters() {
        if (intentFilters == null) {
            initIntentFilters();
        }
        return intentFilters;
    }

    private void initIntentFilters() {
        IntentFilter intentFilters[] = null;
        switch (authType) {
            case OAUTH10:
            case OAUTH20: {
                /* @formatter:off */
                intentFilters = new IntentFilter[]{
                        new IntentFilter(CloudStorageSync.CLOUD_OAUTH1_RESPONSE),
                        new IntentFilter(CLOUD_SIGNIN),
                        device.buildDeviceIntentFilterForAction(CloudDevice.BROADCAST_DEVICE_REFRESH),
                };
                /* @formatter:on */
                break;
            }
            case NONE:
            case USER_PASSWORD:
            default:
                /* @formatter:off */
                intentFilters = new IntentFilter[]{
                        new IntentFilter(CLOUD_SIGNIN),
                        this.device.buildDeviceIntentFilterForAction(CloudDevice.BROADCAST_DEVICE_REFRESH),
                };
                /* @formatter:on */
                break;
        }
        this.intentFilters = intentFilters;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (StringUtils.equals(action, CloudDevice.BROADCAST_DEVICE_REFRESH)) {
            boolean syncMetadata = intent.hasExtra(CloudDevice.REFRESH_FROM_KEY);
            // someday we might set syncThisFirst from an extra in this intent
            requestSync(syncMetadata);
        }
    }

    @Override
    public void reset() {
        long startTime = System.currentTimeMillis();
        try {
            Log.i(this, "reset() starting");
            if (metadataSynchronizer != null) {
                metadataSynchronizer.abort();
            }
            setMetadataUserId(null);
            resetAccessTokenAndSecret();
            saveAccountInfoToPref(null, false);
            saveLargestChangeIdToPref(-1);
            context.getDatabaseHelper().reset();
            isSignIn = false;
        } finally {
            long endTime = System.currentTimeMillis();
            Log.i(this, "reset() took " + (endTime - startTime) + " millisecond(s)");
        }
    }

    @Override
    public File downloadThumbnail(ImageInfo imageInfo, String pathToDownload, String fileName) throws IOException {
        Log.i(this, "downloadThumbnail");
        File result = new File(pathToDownload, fileName);

        InputStream inputStream = null;
        FileOutputStream fos = null;
        try {
            if (getAuthorization()) {
                String mediaId = imageInfo.getSourceMediaId();
                if (mediaId == null) {
                    throw new FileNotFoundException("no mediaId in getThumbnail() request");
                }
                String realFileName = imageInfo.getFileName();
                Log.i(this, "downloadThumbnail(" + mediaId + ") starting " + fileName + " " + realFileName);

                Files.Get getRequest = getContext().mDrive.files().get(mediaId);

                com.google.api.services.drive.model.File dFile = retryRequest(getRequest, RETRY_MAX_COUNT);

                if (dFile != null) {
                    String thumbnailURL = dFile.getThumbnailLink();

                    if (thumbnailURL == null) {
                        throw new FileNotFoundException("mediaId [ " + mediaId + " ] has no thumbailURI " + realFileName);
                    }

                    thumbnailURL = thumbnailURL.replace("s220", "s1440"); //temporary fix
                    Log.i(this, "downloadThumbnail() - thumbnailURL : " + thumbnailURL + "-fileName: " + dFile.getOriginalFilename());
                    URL url = new URL(thumbnailURL);
                    inputStream = url.openStream();

                    if (inputStream == null) {
                        return null;
                    }

                    fos = new FileOutputStream(result);

                    int read = 0;
                    byte[] data = new byte[BUFF_SIZE_FAST];

                    while (read >= 0) {
                        read = inputStream.read(data);
                        if (read > 0) {
                            fos.write(data, 0, read);
                        }
                    }
                    Log.i(this, "downloadThumbnail(" + mediaId + ") finished " + realFileName);
                } else {
                    Log.e(this, "downloadThumbnail() - dFile is null" + realFileName);
                }
            }
        } catch (IOException ioE) {
            Log.e(this, "downloadThumbnail() - IOException : " + ioE.getMessage());
            throw ioE;
        } catch (Exception e) {
            Log.e(this, "downloadThumbnail() - Exception : " + e.getMessage());
            throw new FileNotFoundException(e.getMessage());
        } finally {
            IOUtils.closeQuietly(inputStream);
            IOUtils.closeQuietly(fos);
        }

        return result;
    }

    @Override
    public void setPreferredThumbnailSize(int width, int height) {
        preferredThumbnailWidth = width;
        preferredThumbnailHeight = height;
    }

    //Using cancel code. Don't delete code.
    public void setCancellationSignal(CancellationSignal cancelSignal, String fileId) {
        Log.d(this, "setCancellationSignal : " + fileId);
        mCancellationSignal = cancelSignal;
        mCancellationFileId = fileId;
    }

    @Override
    public final ASPFileBrowser<?> getCloudStorageFileBrowser(String storageGatewayId, ASPFileSortType sort, boolean forceReload, boolean isBrowserForView)
            throws InterruptedException, IOException {

        Log.i(this, "getCloudStorageFileBrowser() called"
                + ", storageGatewayId = " + storageGatewayId
                + ", prevLargestChangeId = " + prevLargestChangeId
                + ", isSignIn = " + isSignIn + ", syncRunning = " + syncRunning.get());

        long oldTime = 0;
        boolean isVisited = false;

        while (isSignIn && // preventing infinite loop even though the reset() is called.
                (syncRunning.get() || prevLargestChangeId == -1)) { // adding this condition due to P151123-03056
            if (!isVisited) {
                isVisited = true;
                if (storageGatewayId != null
                        && !storageGatewayId.equals(CloudStorageConstants.CLOUD_ROOT)
                        && isEmptyDirectory(storageGatewayId)) {
                    Log.i(this, "getCloudStorageFileBrowser() - " + storageGatewayId + "is empty directory");
                    return null;
                }
            }

            if (synchingWithTreeTraversal) {  // in the process of initial full sync
                if (storageGatewayId == null)
                    storageGatewayId = CloudStorageConstants.CLOUD_ROOT;
                if (((GoogleDriveContext) context).cloudFilesMap_cache.containsKey(storageGatewayId)) {
                    // the directory has been already  synched.
                    break;
                }
            }

            long curTime = System.currentTimeMillis();
            if (curTime > oldTime + 5000) {
                oldTime = curTime;
                Log.d(this, "getCloudStorageFileBrowser() - sync is running.." + storageGatewayId);
            }

            if (mCancellationSignal != null && mCancellationSignal.isCanceled()) {
                Log.i(this, "getCloudStorageFileBrowser() - canceled signal");
                return null;
            }
            //tskim because of cancel signal
            Thread.sleep(100);
        }

        Log.d(this, "getCloudStorageFileBrowser() - sync is not running!!, synchingWithTreeTraversal = " + synchingWithTreeTraversal);
        CloudDirectory directory = null;
        if (StringUtils.isNotEmpty(storageGatewayId)) {
            if (synchingWithTreeTraversal && ((GoogleDriveContext) context).cloudFilesMap_cache.containsKey(storageGatewayId)) {
                directory = (CloudDirectory) ((GoogleDriveContext) context).cloudFilesMap_cache.get(storageGatewayId);
            } else {
                directory = context.getDatabaseHelper().getDirectoryByCloudId(context, storageGatewayId);
            }
        }
        Log.i(this, "getCloudStorageFileBrowser() - directory = " + directory);

        boolean bRet = false;
        CloudFileBrowser fileBrowser = (CloudFileBrowser) getCloudStorageFileBrowser();
        if (fileBrowser != null) {
            synchronized (fileBrowser) {
                bRet = fileBrowser.init(directory, sort, true);
            }
        }
        return bRet ? fileBrowser : null;
    }

    //jsub12_151006
    @Override
    public int getCountOfChild(ASPFile file_param) {
        return !(file_param instanceof CloudFile) ? 0 : ((CloudFile) file_param).getChildCount();
    }

    //jsub12_151006
    @Override
    public int getCountOfChildDir(ASPFile file_param) {
        return !(file_param instanceof CloudFile) ? 0 : ((CloudFile) file_param).getChildDirCount();
    }

    @Override
    public int getCountOfDescendants(ASPFile file_param) {
        return !(file_param instanceof CloudFile) ? 0 : ((CloudFile) file_param).getDescendantsCount();
    }

    //jsub12_151006
    @Override
    public int getCountOfDescendantDir(ASPFile file_param) {
        return !(file_param instanceof CloudFile) ? 0 : ((CloudFile) file_param).getDescendantDirCount();
    }

    @Override
    public String getTrashProcessingDir(ASPFile file_param) {
        return null;
    }

    protected abstract boolean idsUseBase64();

    protected CloudDirectory getDirectoryNotInDatabase(String storageGatewayId) throws InterruptedException, IOException {

        Log.d(this, "getDirectoryNotInDatabase() - storageGatewayId : " + storageGatewayId);

        storageGatewayId = decodeStorageGatewayFileId(storageGatewayId);
        Log.d(this, "getDirectoryNotInDatabase() - decode storageGatewayId : " + storageGatewayId);
        CloudDirectory directory = getRootDirectory();
        String path = Metadata.Id.getFilePathFromSourceMediaId(storageGatewayId);
        Log.d(this, "getDirectoryNotInDatabase() - path : " + path);

        String[] pathComponents = path.split("\\" + CloudContext.PATH_SEPARATOR);
        for (int i = 0; i < pathComponents.length; i++) {
            String pathComponent = pathComponents[i];
            Log.d(this, "getDirectoryNotInDatabase() - pathComponent : " + pathComponent);

            if (StringUtils.isEmpty(pathComponent)) {
                continue;
            }
            CloudFile file = directory.getFile(pathComponent);

            if (!(file instanceof CloudDirectory)) {
                throw new IllegalArgumentException("invalid storageGatewayId: " + storageGatewayId);
            }
            directory = (CloudDirectory) file;
        }

        return directory;
    }

    @Override
    public ASPFileBrowser<?> getCloudStorageFileBrowser() {
        CloudFileBrowser cloudFileBrowser = null;
        if (getAuthorization()) {
            cloudFileBrowser = new CloudFileBrowser(this);
        }
        broadcastAuthorizationState();
        return cloudFileBrowser;
    }

    private void saveDetailedQuota(About about) {
        List<About.QuotaBytesByService> quotaList = about.getQuotaBytesByService();
        long[] detailedUsage = new long[3];

        for (About.QuotaBytesByService quotaService : quotaList) {
            switch (quotaService.getServiceName()) {
                case "DRIVE":
                    detailedUsage[0] = quotaService.getBytesUsed();
                    break;
                case "GMAIL":
                    detailedUsage[1] = quotaService.getBytesUsed();
                    break;
                case "PHOTOS":
                    detailedUsage[2] = quotaService.getBytesUsed();
                    break;
            }
        }

        savePreferenceLong(PreferenceType.GOOGLE_DRIVE_USED, detailedUsage[0]);
        savePreferenceLong(PreferenceType.GMAIL_USED, detailedUsage[1]);
        savePreferenceLong(PreferenceType.PHOTOS_USED, detailedUsage[2]);
    }

    private About getAbout() throws IOException {
        int retry_cnt = 100;
        About about = null;
        while (retry_cnt > 0) {
            try {
                Log.i(this, "getAbout() - retry_cnt = " + retry_cnt);
                about = getContext().mDrive.about().get().execute();
                retry_cnt = 0;
            } catch (GoogleJsonResponseException e) {
                Log.e(this, "getAbout() - GoogleJsonResponseException : " + e.getMessage() + "// --------");
                retry_cnt--;
                try {
                    Thread.sleep(500);
                } catch (InterruptedException error) {
                    Log.e(this, "getAbout() - InterruptedException :" + error.getMessage() + "// --------");
                }
            }
        }

        return about;
    }

    @Override
    public void sync() {
        if (!syncRunning.compareAndSet(false, true)) {
            Log.i(this, "sync is already running");
            return;
        }
        long startTime = System.currentTimeMillis();
        syncMetadata = false;

        Log.i(this, "sync() starting, deviceId : " + getDeviceIdAsString());

        boolean success = getAuthorization();
        Log.i(this, "sync() getAuthorization : " + success);

        CloudGatewayFileBrowserUtils.SyncResult result = null;
        try {
            if (success && device != null && !device.getIsInUserRecovering()) {
                String userId = context.getUserId();
                context.setEmail(userId);
                setMetadataUserId(userId);
                device.setWebStorageUserId(userId);
                dataModel.updateDevice(device);

                Log.i(this, "sync(), trying to get About obj");

                About about = getAbout();

                Log.i(this, "sync(), get About obj : " + about);
                if (about != null) {
                    long curLargestChangeId = about.getLargestChangeId();
                    if (prevLargestChangeId < curLargestChangeId) {
                        syncMetadata = true;
                    }
                    Log.i(this, "sync(), syncMetadata is " + syncMetadata);

                    String email = this.context.getEmail();
                    if (email != null) {
                        device.setWebStorageUserId(email);
                        dataModel.updateDevice(device);
                    }

                    if (rootDirectory == null) {
                        Log.e(this, "sync() - rootDirectory is null");
                        result = CloudGatewayFileBrowserUtils.SyncResult.SYNC_FAIL;
                    } else {
                        if (syncMetadata) {
                            if (syncMetadata(curLargestChangeId)) {
                                Log.d(this, "sync() - success");
                                result = CloudGatewayFileBrowserUtils.SyncResult.SYNC_SUCCESS;
                                savePreferenceLong(PreferenceType.LAST_SYNC_TIME, System.currentTimeMillis());
                            } else {
                                Log.d(this, "sync() - fail");
                                result = CloudGatewayFileBrowserUtils.SyncResult.SYNC_FAIL;
                            }
                        } else {
                            result = CloudGatewayFileBrowserUtils.SyncResult.NO_DELTA_METADATA;
                        }
                    }
                }
            }
        } catch (UserRecoverableAuthIOException e) {
            Log.e(this, "sync() - UserRecoverableAuthIOException : " + e);
            CloudAuthorizationException.showAuthActivity(getApplicationContext(), getCloudDevice().getId(), device, e.getIntent());
            result = CloudGatewayFileBrowserUtils.SyncResult.SYNC_FAIL;
        } catch (GoogleAuthIOException e) {
            Log.e(this, "sync() - GoogleAuthIOException : " + e);
            Intent intent = new Intent("com.mfluent.asp.datamodel.Device.BROADCAST_DEVICE_STATE_CHANGE.SLPF_CLOUD");
            intent.putExtra(CloudDevice.DEVICE_ID_EXTRA_KEY, getCloudDevice().getId());
            intent.putExtra("message", "GoogleAuthIOException");
            appDelegate.sendBroadcastMessage(intent);
            setAndCheckAuthorizationState(AuthorizationState.NO);
            result = CloudGatewayFileBrowserUtils.SyncResult.SYNC_FAIL;
        } catch (Exception e) {
            Log.e(this, "sync() - Exception : " + e);
            result = NetworkUtilSLPF.isNetworkAvailable() ? CloudGatewayFileBrowserUtils.SyncResult.SYNC_FAIL : CloudGatewayFileBrowserUtils.SyncResult.NO_NETWORK_ERROR;
        } finally {
            sendSyncBroadcast(CloudGatewayFileBrowserUtils.SYNC_FINISHED, result);   // TODO : Remove check
            broadcastAuthorizationState();
            syncRunning.set(false);
            long endTime = System.currentTimeMillis();

            Log.i(this, "sync() - took " + (endTime - startTime) + " millisecond(s)");
        }
    }

    private void sendSyncBroadcast(String action, CloudGatewayFileBrowserUtils.SyncResult reason) {
        Log.i(this, "sendSyncCancelBroadcast() - action : " + action + ", - reason : " + reason);
        Intent intent = new Intent(action);
        if (reason != null) {
            intent.putExtra(CloudGatewayFileBrowserUtils.SYNC_FINISHED, reason);
        }
        appContext.sendBroadcast(intent);
    }

    @Override
    public UploadResult uploadFile(String targetDirId, File fileToUpload, String mimeType, final StreamProgressListener streamProgressListener) {
        UploadResult ret = new UploadResult();
        ret.mFileId = null;
        ret.mStatus = UploadResult.Status.OTHER;

        long startTime = System.currentTimeMillis();
        String fileName = fileToUpload.getName();
        String canonicalPath = "???";
        try {
            canonicalPath = fileToUpload.getCanonicalPath();
        } catch (IOException e) {
            Log.e(this, "uploadFile() - IOException : " + e.getMessage());
        }
        Log.i(this, "uploadFile(\"" + canonicalPath + "\") starting - targetDirID param = " + targetDirId);

        CloudContext context = getContext();
        try {
            final long length = fileToUpload.length();
            long sizeLimit = getMaximumFileSize();
            if (sizeLimit > 0 && length > sizeLimit) {
                ret.mStatus = UploadResult.Status.TOO_LARGE;
                return ret;
            }

            boolean success = getAuthorization();
            if (!success) {
                ret.mStatus = UploadResult.Status.NOT_AUTHORIZED;
                return ret;
            }

            com.google.api.services.drive.model.File body = new com.google.api.services.drive.model.File();
            body.setTitle(fileName);
            body.setMimeType(mimeType);

            if (targetDirId != null && targetDirId.length() > 0) {
                String cloudId = targetDirId.replace("/", "");
                Log.i(this, "uploadFile() - cloudId = " + cloudId);
                body.setParents(Arrays.asList(new ParentReference().setId(cloudId)));
            }

            FileContent mediaContent = new FileContent(mimeType, fileToUpload);
            Files.Insert insertRequest = context.mDrive.files().insert(body, mediaContent);

            MediaHttpUploader uploader = insertRequest.getMediaHttpUploader();
            MediaHttpUploaderProgressListener uploaderProgressListener = new MediaHttpUploaderProgressListener() {
                long byteTransfer = 0;

                @Override
                public void progressChanged(MediaHttpUploader uploader) throws IOException {

                    switch (uploader.getUploadState()) {
                        case MEDIA_IN_PROGRESS:
                            long totalBytesServerReceived = uploader.getNumBytesUploaded();
                            streamProgressListener.bytesTransferred(totalBytesServerReceived - byteTransfer);
                            byteTransfer = totalBytesServerReceived;
                            break;
                        case MEDIA_COMPLETE:
                            streamProgressListener.bytesTransferred(length - byteTransfer);
                            break;
                    }
                }
            };

            uploader.setProgressListener(uploaderProgressListener);
            uploader.setDirectUploadEnabled(false);
            uploader.setChunkSize(MediaHttpUploader.MINIMUM_CHUNK_SIZE);

            com.google.api.services.drive.model.File dFile = retryRequest(insertRequest, RETRY_MAX_COUNT);

            if (dFile != null && dFile.getId() != null) {
                Log.d(this, "uploadFile() - uploaded file id = " + dFile.getId());
                ret.mStatus = UploadResult.Status.OK;
                ret.mFileId = dFile.getId();
                ((GoogleDriveContext) context).putDB(dFile);
                usedCapacity += length;
                device.setUsedCapacityInBytes(usedCapacity);
            }
        } catch (IOException e) {
            Log.e(this, "uploadFile() - IOException : " + e.getMessage());
            if (e.getMessage().contains(GoogleDriveContext.RETRY_MAX_ERROR)) {
                ret.mStatus = UploadResult.Status.MAX_RETRY_FAIL;
            } else if (e.getMessage().contains(CloudStorageError.NO_NEED_RETRY_ERROR)) {
                ret.mStatus = UploadResult.Status.NO_NEED_RETRY;
            } else if (e.getMessage().contains(CloudStorageError.OUT_OF_STORAGE_ERROR)) {
                ret.mStatus = UploadResult.Status.OUT_OF_SPACE;
            }
        } finally {
            broadcastAuthorizationState();
            long endTime = System.currentTimeMillis();
            Log.i(this, "uploadFile() took " + (endTime - startTime) + " millisecond(s)");
            return ret;
        }
    }

    @Override
    public boolean isRangeDownloadSupported() {
        return true;
    }

    @Override
    public CloudStreamInfo getFile(ASPFile file, String contentRange) throws IOException {
        if (!(file instanceof CloudFile)) {
            throw new FileNotFoundException("not a CloudFile!");
        }
        CloudFile cloudFile = (CloudFile) file;
        Id sourceMediaId = getSourceMediaId(cloudFile);
        if (sourceMediaId == null) {
            throw new FileNotFoundException("file does not have a sourceMediaId");
        }
        CloudStreamInfo streamInfo = getFile(sourceMediaId.toString(), sourceMediaId.toString(), contentRange);
        return streamInfo;
    }

    @Override
    public CloudStreamInfo getFile(String sourceUrl, String sourceId, String contentRange) throws IOException {
        CloudStreamInfo cloudStream = null;
        long startTime = System.currentTimeMillis();
        Log.i(this, "getFile_1(" + sourceId + ") starting");
        try {
            if (getAuthorization()) {
                CloudFile file = new CloudFile(context, null, null);
                Files.Get getRequest = getContext().mDrive.files().get(sourceId);
                com.google.api.services.drive.model.File dFile = retryRequest(getRequest, RETRY_MAX_COUNT);

                if (dFile != null) {
                    file.setMimeType(dFile.getMimeType());

                    String downloadURL = dFile.getDownloadUrl();
                    if (downloadURL == null) { // in case of google docs documents
                        downloadURL = dFile.getExportLinks().get("application/pdf"); // conversion format is pdf.
                    }

                    Log.i(this, "downloadURL : " + downloadURL);
                    file.setDownloadURL(downloadURL);

                    if (dFile.getFileSize() != null) {
                        String strtemp = dFile.getFileSize().toString();
                        file.setLength(Long.parseLong(strtemp));
                    }

                    cloudStream = new CloudStream(context, file, contentRange);
                }
            }
        } catch (IOException ioE) {
            throw ioE;
        } finally {
            broadcastAuthorizationState();
            long endTime = System.currentTimeMillis();
            Log.e(this, "getFile() took " + (endTime - startTime) + " millisecond(s)");
        }
        return cloudStream;
    }

    private static class InputStreamInfo {
        InputStream inputStream;
        long contentLength;
        String mimeType;
    }

    private InputStreamInfo getCloudInputStream(String fileId) throws IOException {
        String range = null;
        CloudStreamInfo streamInfo = getFile(fileId, range);
        InputStreamInfo inputStreamInfo = new InputStreamInfo();
        if (streamInfo != null) {
            inputStreamInfo.inputStream = streamInfo.getInputStream();
            inputStreamInfo.contentLength = streamInfo.getContentLength();
            inputStreamInfo.mimeType = streamInfo.getContentType();
        }
        return inputStreamInfo;
    }

    @Override
    public String downloadFile(StreamProgressListener listener, String fileId, String targetPath, String fileName, boolean bWaitForRename) throws IOException {
        String ret = null;
        File cacheDir = new File(MFLStorageManagerSLPF.getCacheDir(appContext), "filetransfer_cache");
        cacheDir.mkdirs();

        File tmpFile = new File(cacheDir, "DownloadTask.tmp." + UUID.randomUUID().toString());
        InputStreamInfo inputStreamInfo = getCloudInputStream(fileId);
        OutputStream outputStream = null;
        OutputStream innerOutputStream = null;
        long bytesCopied = 0;
        try {
            ////auto retry file transfer
            innerOutputStream = AspFileUtilsSLPF.openFileOutputStreamWithRetry(tmpFile, true);
            innerOutputStream = new ProgressUpdatingOutputStream(innerOutputStream, listener);
            outputStream = new InterruptibleOutputStream(innerOutputStream);

            bytesCopied = IOUtils.copyLarge(inputStreamInfo.inputStream, outputStream);
        } catch (IOException e) {
            Log.e(this, "downloadFile() - IOException : " + e.getMessage());
            e.printStackTrace();
        } finally {
            IOUtils.closeQuietly(innerOutputStream);
            IOUtils.closeQuietly(outputStream);
            IOUtils.closeQuietly(inputStreamInfo.inputStream);
        }

        long contentLength = inputStreamInfo.contentLength;
        if (contentLength != 0 && contentLength != bytesCopied) {
            throw new RuntimeException("only " + bytesCopied + " of " + contentLength + " byte(s) were transferred!!!");
        }

        long tmpFileLength;
        File targetFile = new File(targetPath, fileName);
        try {
            tmpFileLength = AspFileUtilsSLPF.getFileSizeWithRetry(tmpFile);

            if (inputStreamInfo.mimeType != null && inputStreamInfo.mimeType.contains("application/vnd.google-apps")) {
                targetFile = new File(targetFile.getParent(), targetFile.getName() + ".pdf");
                bWaitForRename = true;
            }

            targetFile = AspFileUtilsSLPF.moveFile(tmpFile, targetFile, bWaitForRename);
            ret = targetFile.getName();
        } finally {
            FileUtils.deleteQuietly(tmpFile);
            tmpFile = null;
        }

        ////auto retry file transfer
        long targetLength = AspFileUtilsSLPF.getFileSizeWithRetry(targetFile);
        if (tmpFileLength != targetLength) {
            throw new RuntimeException("only " + targetLength + " of " + tmpFileLength + " byte(s) were copied!!!");
        }

        return ret;
    }

    // TODO : Remove check
    @Override
    public int deleteFiles(String directoryGatewayFileId, ASPFileSortType sortType, String... storageGatewayFilesToDelete)
            throws InterruptedException, IOException {
        Log.i(this, "deleteFiles(), directoryGatewayFileId = " + directoryGatewayFileId);

        int numFilesDeleted = 0;
        if (StringUtils.isNotEmpty(directoryGatewayFileId)) {
            CloudDirectory directory = context.getDatabaseHelper().getDirectoryByCloudId(context, directoryGatewayFileId);

            if (directory == null) {
                directory = getDirectoryNotInDatabase(directoryGatewayFileId);
            }

            if (directory != null) {
                directory.holdFiles();

                try {
                    for (int i = 0; i < storageGatewayFilesToDelete.length; ++i) {
                        boolean result = deleteFileFromDirectory(directory, storageGatewayFilesToDelete[i]);
                        if (!result) {
                            break;
                        }
                        numFilesDeleted++;
                    }
                } catch (Exception e) {
                    Log.e(this, "Failed to delete files : "
                            + Arrays.toString(storageGatewayFilesToDelete)
                            + " in directory:"
                            + directoryGatewayFileId);
                    Log.e(this, "deleteFiles() - Exception : " + e.getMessage());
                    if (e instanceof InterruptedException) {
                        throw (InterruptedException) e;
                    } else if (e instanceof IOException) {
                        throw (IOException) e;
                    }
                } finally {
                    directory.releaseFiles();
                }

                if (numFilesDeleted > 0) {
                    ContentResolver cr = getApplicationContext().getContentResolver();
                    cr.notifyChange(CloudGatewayMediaStore.FileBrowser.FileList.getFileListUri(getCloudDevice().getId(), directoryGatewayFileId), null);
                    cr.notifyChange(
                            CloudGatewayMediaStore.FileBrowser.DirectoryInfo.getDirectoryInfoUri(getCloudDevice().getId(), directoryGatewayFileId),
                            null);
                }
            }
        }

        return numFilesDeleted;
    }

    // TODO : Remove check
    protected boolean deleteFileFromDirectory(CloudDirectory directory, String storageGatewayId) throws Exception {
        Log.i(this, "deleteFileFromDirectory() called");
        storageGatewayId = decodeStorageGatewayFileId(storageGatewayId);

        String path = Metadata.Id.getFilePathFromSourceMediaId(storageGatewayId);
        String[] pathComponents = path.split("\\" + CloudContext.PATH_SEPARATOR);
        boolean success = false;
        if (pathComponents.length > 0) {
            String name = pathComponents[pathComponents.length - 1];
            CloudFile file = directory.getFile(name);
            success = (file != null) ? deleteFile(file) : true;
        }

        return success;
    }

    private class APIBatchRequest {
        public boolean bBatchSuccess;
        public int mBatchRetryCnt;

        public APIBatchRequest() {
            mBatchRetryCnt = 0;
        }

        public void doBatchExecute(BatchRequest batch) throws IOException {
            try {
                Log.d(this, "doBatchExecute() - execute " + mBatchRetryCnt);
                batch.execute();
            } catch (IOException e) {
                Log.e(this, "doBatchExecute() - IOException : " + e.getMessage());
                throw e;
            } catch (IllegalStateException e) {
                Log.e(this, "doBatchExecute() - IllegalStateException : " + e.getMessage());
            }
        }
    }

    @Override
    public boolean deleteFileBatch(final String[] sourceIds) {
        final APIBatchRequest request = new APIBatchRequest();
        final Result partialSuccess = new Result(false);

        JsonBatchCallback<Void> voidCallback = new JsonBatchCallback<Void>() {
            @Override
            public void onSuccess(Void id, HttpHeaders httpHeaders) throws IOException {
                Log.d(this, "deleteFileBatch batchSuccess " + id);
                partialSuccess.mRet = true;
            }

            @Override
            public void onFailure(GoogleJsonError error, HttpHeaders httpHeaders) throws IOException {
                Log.e(this, "deleteFileBatch batchFail " + error.getMessage());

                if (error.getMessage().contains("Rate Limit Exceeded")) {
                    request.mBatchRetryCnt++;
                } else if (error.getMessage().contains("File not found")) {
                    return;
                }
                request.bBatchSuccess = false;
            }
        };

        try {
            if (getAuthorization()) {
                int retryMaxBatchCnt = RETRY_MAX_BATCH_COUNT * sourceIds.length;
                BatchRequest batch = null;
                do {
                    request.bBatchSuccess = true;
                    for (int index = 0; index < sourceIds.length; index++) {
                        String id = sourceIds[index];

                        if (batch == null) {
                            batch = getDrive().batch();
                        }

                        if (id != null) {
                            Files.Delete deleteRequest = getContext().mDrive.files().delete(id);
                            deleteRequest.queue(batch, voidCallback);
                        }

                        if ((batch != null) && ((batch.size() >= API_CALL_BATCH_LIMIT) || (index == sourceIds.length - 1))) {
                            Log.d(this, "deleteFileBatch() - doBatchExecute, batch.size() : " + batch.size());
                            request.doBatchExecute(batch);
                            batch = null;
                            if (!request.bBatchSuccess) {
                                try {
                                    Thread.sleep(1000);
                                } catch (InterruptedException e) {
                                    Log.e(this, "deleteFileBatch() InterruptedException");
                                }
                            }
                        }
                    }
                }
                while (!request.bBatchSuccess && (request.mBatchRetryCnt > 0) && (request.mBatchRetryCnt < retryMaxBatchCnt));
            }
        } catch (IOException e) {
            Log.e(this, "deleteFileBatch() - IOException : " + e.getMessage());
        }

        if (partialSuccess.mRet) {
            if (request.bBatchSuccess) { // all success
                deleteFileBatchFromDB(sourceIds);
            } else {   // partial success - deleted id is not come through onSuccess() sometimes
                requestSync(true);
            }
        }
        return request.bBatchSuccess;
    }

    @Override
    public boolean deletePermanentlyBatch(String[] sourceIds) {
        return deleteFileBatch(sourceIds);
    }

    @Override
    public boolean deleteFile(String sourceUrl, String sourceId) {
        Log.i(this, "deleteFile() - sourceUrl = " + sourceUrl + ", sourceId = " + sourceId);

        String fileId = Metadata.Id.getFileIdFromSourceMediaId(sourceId);
        CloudFile file = new CloudFile(getContext(), null, null);
        file.setCloudId(fileId);
        file.setDownloadURL(sourceUrl);
        boolean success = false;
        try {
            success = deleteFile(file);
        } catch (Exception e) {
            Log.e(this, "deleteFile() - Exception deleting file : " + e);
        }
        return success;
    }

    public boolean deleteFile(CloudFile file) throws Exception {
        long startTime = System.currentTimeMillis();
        boolean success = false;
        try {
            String cloudId = file.getCloudId();
            Log.i(this, "deleteFile(" + cloudId + ") starting");

            if (getAuthorization()) {
                try {
                    retryRequest(getContext().mDrive.files().delete(cloudId), RETRY_MAX_COUNT);
                    success = true;
                    CloudDirectory parent = file.getParent();
                    if (parent != null) {
                        parent.removeFile(file);
                    }

                    deleteFileFromDB(file.getCloudId());
                } catch (GoogleJsonResponseException e) {
                    if (e.getDetails().getCode() == 404) {
                        Log.d(this, "deleteFile() - GoogleJsonResponseException, return true");
                        return true;
                    }
                }
            }
        } catch (IOException e1) {
            Log.e(this, "deleteFile() - An error occurred : " + e1);
            success = false;
        } finally {
            broadcastAuthorizationState();
            long endTime = System.currentTimeMillis();
            Log.i(this, "deleteFile() took " + (endTime - startTime) + " millisecond(s)");
            return success;
        }
    }

    /*
     * methods that complement methods in interface CloudStorageSync
     */
    @Override
    public CloudDevice getCloudDevice() {
        return device;
    }

    public ContentResolver getContentResolver() {
        return contentResolver;
    }

    @Override
    public Context getApplicationContext() {
        return appContext;
    }

    protected CloudDataModel getDataModel() {
        return dataModel;
    }

    /**
     * get authorization for the connector to access the cloud storage, without using the storage gateway.
     *
     * @return true iff the connector is authorized to access the cloud storage
     */
    protected abstract AuthorizationState getAuthorizationInDevice();

    /**
     * get the limit on the size of files in this cloud storage
     *
     * @return the limit on the size of files in this cloud storage, 0 if no limit
     */
    @Override
    public abstract long getMaximumFileSize();

    protected Metadata.Id getSourceMediaId(CloudFile file) {
        String metadataId = getMetadataId(file);
        if (metadataId == null) {
            throw new RuntimeException("metadataId is null!!!");
        }
        String directoryId = "";

        return new Metadata.Id(directoryId, metadataId);
    }

    protected String getMetadataId(CloudFile file) {
        return (file != null) ? file.getCloudId() : "";
    }

    @Override
    public String getStorageGatewayFileId(ASPFile file) throws FileNotFoundException {
        if (!(file instanceof CloudFile)) {
            throw new FileNotFoundException("not a CloudFile!");
        }
        CloudFile cloudFile = (CloudFile) file;
        Id sourceMediaId = getSourceMediaId(cloudFile);
        if (sourceMediaId == null) {
            throw new FileNotFoundException("file does not have a sourceMediaId");
        }
        String fileMetadataPath = sourceMediaId.toString();
        Log.i(this, "getStorageGatewayFileId(), sourceMediaId = " + sourceMediaId + ", fileMetadataPath = " + fileMetadataPath);
        MediaType mediaType = MediaType.forFile(cloudFile);
        int aspMediaType = AspMediaId.MEDIA_TYPE_NONE;
        if (mediaType != null) {
            aspMediaType = mediaType.aspMediaType;
        }
        String storageGatewayFileId = getStorageGatewayFileId(aspMediaType, fileMetadataPath, null);
        Log.i(this, "getStorageGatewayFileId(), returned storageGatewayFileId = " + storageGatewayFileId);
        return storageGatewayFileId;
    }

    @Override
    public String getStorageGatewayFileId(int aspMediaType, String sourceUrl, String sourceId) {
        String storageGatewayFileId = Metadata.Id.getFilePathFromSourceMediaId(sourceUrl);
        if (idsUseBase64()) {
            storageGatewayFileId = Base64.encodeToString(storageGatewayFileId.getBytes(), Base64.NO_WRAP | Base64.URL_SAFE);
        }
        return storageGatewayFileId;
    }

    protected String decodeStorageGatewayFileId(String fileId) {
        if (idsUseBase64()) {
            fileId = new String(Base64.decode(fileId, Base64.NO_WRAP | Base64.URL_SAFE));
        }
        return fileId;
    }

    @Override
    public CloudStreamInfo getFile(String fileId, String contentRange) throws IOException, IllegalStateException {
        //fileId = decodeStorageGatewayFileId(fileId);
        CloudStreamInfo cloudStream = null;
        long startTime = System.currentTimeMillis();
        Log.i(this, "getFile_2(" + fileId + ") starting");
        try {
            if (getAuthorization()) {
                CloudFile file = new CloudFile(context, null, null);

                Files.Get getRequest = getContext().mDrive.files().get(fileId);
                com.google.api.services.drive.model.File dFile = retryRequest(getRequest, RETRY_MAX_COUNT);

                if (dFile != null) {
                    String downloadURL = dFile.getDownloadUrl();
                    if (downloadURL == null) { // in case of google docs documents
                        downloadURL = dFile.getExportLinks().get("application/pdf"); // conversion format is pdf.
                    }

                    Log.i(this, "getFile()::downloadURL : " + downloadURL);
                    file.setDownloadURL(downloadURL);
                    file.setMimeType(dFile.getMimeType());

                    if (dFile.getFileSize() != null) {
                        String strtemp = dFile.getFileSize().toString();
                        file.setLength(Long.parseLong(strtemp));
                    }

                    cloudStream = new CloudStream(context, file, contentRange);
                }
            }
        } catch (IOException ioE) {
            throw ioE;
        } finally {
            broadcastAuthorizationState();
            long endTime = System.currentTimeMillis();
            Log.i(this, "getFile() took " + (endTime - startTime) + " millisecond(s)");
        }

        return cloudStream;
    }

    /**
     * get authorization to access the cloud storage.
     *
     * @return true iff now authorized to access the cloud storage
     * @throws CloudAuthorizationException if authorization state could not be determined
     */
    protected boolean getAuthorization() {
        try {
            numberOfAuthorizeRequests += 1;
            boolean authorized = checkAuthorization();

            if (!authorized) {
                authorized = setAndCheckAuthorizationState(getAuthorizationInDevice());
            }
            broadcastAuthorizationState();

            return authorized;
        } finally {
            Log.d(this, numberOfAuthorizeRequests + " getAuthorization() request(s); " + numberOfGetTokenRequests + " getToken() request(s)");
        }
    }

    protected void broadcastAuthorizationState() {
        AuthorizationState authorizationState = getAuthorizationState();
        setIsSignedIn(authorizationState == AuthorizationState.YES);
        broadcastMessage(authorizationState.broadcast);
    }

    protected final String getName() {
        return type.name;
    }

    protected final T getContext() {
        return context;
    }

    protected void resetAccessTokenAndSecret() {
        setAccessTokenAndSecret(null, null);
    }

    /**
     * set the authorization token and secret that the connector is to use to access the cloud storage.
     * </br></br>
     * these values are very much connector dependent, and both may not necessarily be used by all connectors.
     *
     * @param token       the token that the connector is to use to access the cloud storage
     * @param tokenSecret the token secret that the connector is to use to access the cloud storage
     */
    protected void setAccessTokenAndSecret(String token, String tokenSecret) {
        context.setAndCheckAccessTokenAndSecret(token, tokenSecret);
    }

    protected AuthorizationState getAuthorizationState() {
        return context.getAuthorizationState();
    }

    public boolean setAndCheckAuthorizationState(AuthorizationState authorizationState) {
        return context.setAndCheckAuthorizationState(authorizationState);
    }

    public boolean checkAuthorization() {
        return context.checkAuthorization();
    }

    protected final void setIsSignedIn(boolean isSignedIn) {
        CloudDevice device = getCloudDevice();
        if (device != null && device.isWebStorageSignedIn() != isSignedIn) {
            device.setWebStorageSignedIn(isSignedIn);
            getDataModel().updateDevice(device);
        } else {
            Log.e(this, "setIsSignedIn() - isSignedIn : " + isSignedIn + ", device : " + device);
        }
    }

    public boolean isSignedIn() {
        Log.i(this, "isSignedIn(), return = " + getCloudDevice().isWebStorageSignedIn());
        return getCloudDevice().isWebStorageSignedIn();
    }

    public final CloudDirectory getRootDirectory() {
        return rootDirectory;
    }

    public final void requestSync(boolean syncMetadata) {
        this.syncMetadata |= syncMetadata;    // only the sync() method can rest this!!!
        if (appDelegate != null) {
            appDelegate.requestSync();
        }
    }

    @Override
    public void newToken(String token, String secret) {
        SharedPreferences preferences = getSharedPreferences();
        String accessToken = preferences.getString(ACCESS_TOKEN_ID, null);
        String accessSecret = preferences.getString(ACCESS_SECRET_ID, null);
        if (!StringUtils.equals(token, accessToken) || !StringUtils.equals(secret, accessSecret)) {
            Log.d(this, "persisting token : " + token + "; secret : " + secret);
            Editor editor = preferences.edit();
            if (accessToken != null) {
                editor.remove(ACCESS_TOKEN_ID);
            }
            if (token != null) {
                editor.putString(ACCESS_TOKEN_ID, token);
            }
            if (accessSecret != null) {
                editor.remove(ACCESS_SECRET_ID);
            }
            if (secret != null) {
                editor.putString(ACCESS_SECRET_ID, secret);
            }
            editor.apply();
        }
    }

    protected final void setMetadataUserId(String userId) {
        if (!StringUtils.equals(metadataUserId, userId)) {
            Editor editor = getSharedPreferences().edit();
            if (metadataUserId != null) {
                try {
                    device.deleteAllMetaData(contentResolver);
                } catch (Exception e) {
                    Log.e(this, "setMetadataUserId() - Exception deleting all metadata : " + e);
                }
                editor.remove(METADATA_USER_ID);
            }
            metadataUserId = userId;
            if (metadataUserId != null) {
                editor.putString(METADATA_USER_ID, metadataUserId);
            }
            editor.apply();
        }
    }

    public final Metadata getFileMetadata(MediaType mediaType, String mediaId) {  //jsub12_150922
        Metadata metadata = null;
        ContentResolver contentResolver = getContentResolver();
        String[] columnNames = mediaType.columnNames;
        if (columnNames != null) {
            Uri uri = getDeviceContentUri();
            /* @formatter:off */
            try (Cursor cursor = contentResolver.query(
                    uri,
                    columnNames,
                    ASPMediaStore.BaseASPColumns.SOURCE_MEDIA_ID + "= ?",
                    new String[]{mediaId},
                    null)) {
            /* @formatter:on */
                if (cursor != null && cursor.moveToFirst()) {
                    metadata = metadataFromCursor(cursor, Metadata.State.UNSEEN);
                }
            }
        }
        return metadata;
    }

    //jsub12.lee_150721
    protected final ArrayList<Metadata> getMetadataOfChildren(MediaType mediaType, String parentCloudId) {
        Log.i(this, "getMetadataOfChildren(), mediaType = " + mediaType + ", parentCloudId = " + parentCloudId);

        ArrayList<Metadata> metaList = new ArrayList<Metadata>();
        ContentResolver contentResolver = getContentResolver();
        String[] columnNames = mediaType.columnNames;
        if (columnNames != null) {
            Uri uri = getDeviceContentUri();
            Log.i(this, "getMetadataOfChildren(), uri = " + uri);

            Cursor cursor = contentResolver.query(
                    uri,
                    columnNames,
                    ASPMediaStore.BaseASPColumns.PARENT_CLOUD_ID + "= ?",
                    new String[]{parentCloudId},
                    null);

            Log.i(this, "getMetadataOfChildren(), cursor after query = " + cursor);

            if (cursor != null) {
                Metadata metadata;
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    metadata = metadataFromCursor(cursor, Metadata.State.UNSEEN);
                    metaList.add(metadata);
                    cursor.moveToNext();
                }
                cursor.close();
            }
        } else {
            Log.i(this, "getMetadataOfChildren(), columnNames is null");
        }

        return metaList;
    }

    protected final void broadcastMessage(String intentAction) {
        Log.d(this, "broadcasting message : " + intentAction);
        if (appDelegate != null) {
            Intent intent = new Intent(intentAction);
            if (device != null) {
                intent.putExtra(CloudDevice.DEVICE_ID_EXTRA_KEY, device.getId());
            }
            appDelegate.sendBroadcastMessage(intent);
        } else {
            Log.d(this, "no appDelegate!");
        }
    }

    private SharedPreferences getSharedPreferences() {
        // int id = device.getId();
        String instanceName = type.name/* + "-" + Integer.toString(id) */;
        SharedPreferences preferences = appContext.getSharedPreferences(instanceName, Context.MODE_PRIVATE);
        return preferences;
    }

    private boolean isEmptyDirectory(String dirId) throws IOException {
        Log.i(this, "isEmptyDirectory(), Dir ID = " + dirId);
        if (dirId == null) {
            dirId = CloudStorageConstants.CLOUD_ROOT;
        }
        boolean result = false;
        try {
            List<com.google.api.services.drive.model.File> fileList = ((GoogleDriveContext) context).getChildFiles(dirId);
            result = (fileList == null) || (fileList.isEmpty());
        } catch (UserRecoverableAuthIOException e) {
            CloudAuthorizationException.showAuthActivity(getApplicationContext(), getCloudDevice().getId(), device, e.getIntent());
        }
        return result;
    }

    /**
     * synchronize all metadata, i.e., all MediaType's.
     *
     * @return true iff successful
     * @throws Exception
     */
    private boolean syncMetadata(long curLargestChangeId) throws Exception {
        Log.e(this, "syncMetadata");
        totalAdded = 0;
        totalUnseen = 0;
        totalUnchanged = 0;
        totalChanged = 0;

        int tryCnt_deltaSync = 0;
        boolean doDeltaSync;
        boolean success;

        do {
            ((GoogleDriveContext) context).initCacheForSync();
            doDeltaSync = false;
            if (prevLargestChangeId > 0) {
                if (curLargestChangeId <= prevLargestChangeId) {

                    Log.i(this, "syncMetadata():: Current latestChangeId = " + curLargestChangeId
                            + ", Previous latestChangeId = " + prevLargestChangeId
                            + ", no need to sync");
                    return true;
                }
                doDeltaSync = true;
                tryCnt_deltaSync++;
            }

            if (!doDeltaSync) { // only for the full sync of cloudgateway
                success = syncWithTreeTraversal();
            } else {
                success = syncDelta();
            }
        }
        while (!success && tryCnt_deltaSync > 0); // if failed to do delta sync, then try to do full sync

        if (success && !doDeltaSync && isSignedIn()) {
            setLargestChangeID(curLargestChangeId);
        }

        Log.d(this, "metadata totals: "
                + totalAdded + " added; "
                + totalUnseen + " unseen; "
                + totalUnchanged + " unchanged; "
                + totalChanged + " changed");

        Log.e(this, "syncMetadata metadata totals: "
                + totalAdded + " added; "
                + totalUnseen + " unseen; "
                + totalUnchanged + " unchanged; "
                + totalChanged + " changed");

        return success;
    }

    private boolean syncWithTreeTraversal() throws Exception {
        synchingWithTreeTraversal = true;
        Log.i(this, "syncWithTreeTraversal() called");

        deleteAllFileInfoFromDB();
        CloudDirectory root = getRootDirectory();
        root.setLevel(0);

        ((GoogleDriveContext) context).cloudDirListQToSync = new LinkedList<>();
        ((GoogleDriveContext) context).cloudDirListQToSync.add(root);

        Log.i(this, "Start Batch for Sync ");
        long startTime = System.currentTimeMillis();
        CloudDirectory targetDir;
        ArrayList<CloudDirectory> arrTargetDir = new ArrayList<CloudDirectory>();
        int countFile = 0;
        while (isSignedIn() && (targetDir = getDirectoryToSync()) != null) {
            arrTargetDir.add(targetDir);
            countFile = countFile + 1;
            if (((GoogleDriveContext) context).cloudDirListQToSync.isEmpty() || countFile == API_CALL_BATCH_LIMIT) {
                ((GoogleDriveContext) context).syncDirectoryBatch(arrTargetDir);
                arrTargetDir.clear();
                countFile = 0;
            }
        }
        long endTime = System.currentTimeMillis();
        Log.i(this, "  finish Batch sync() - took " + (endTime - startTime) + " millisecond(s)");
        //finish
        synchingWithTreeTraversal = false;
        ((GoogleDriveContext) context).cloudDirListQToSync = null;
        ((GoogleDriveContext) context).cloudFilesMap_cache.clear();
        return true;
    }

    private CloudDirectory getDirectoryToSync() {
        Log.i(this, "getDirectoryToSync() called");

        if (syncFirstDir != null) {
            CloudDirectory tmp = syncFirstDir;
            syncFirstDir = null;
            return tmp;
        } else {
            return ((GoogleDriveContext) context).getDirectoryFromQtoSync();
        }
    }

    /**
     * Delta sync
     *
     * @return true iff the synchronization was successful
     * @throws Exception
     */
    private boolean syncDelta() throws Exception {
        Map<String, Metadata> aspMetadata = getMetadata();
        boolean success = (aspMetadata != null);

        try {
            if (success) {
                metadataSynchronizer = new MetadataSynchronizer(this, aspMetadata, rootDirectory);

                //The startChangeId for doDeltaSync()'s param should be +1 than the prevLargestChangeId.
                success = ((GoogleDriveContext) getContext()).doDeltaSync(prevLargestChangeId + 1, metadataSynchronizer);
                if (!success) {
                    setLargestChangeID(-1);
                } else {
                    postProcessMetadata(aspMetadata);
                }
                metadataSynchronizer = null;
            }
        } catch (UserRecoverableAuthIOException e) {
            CloudAuthorizationException.showAuthActivity(getApplicationContext(), getCloudDevice().getId(), device, e.getIntent());
        }
        return success;
    }

    private void postProcessMetadata(Map<String, Metadata> aspMetadata) {
        int added = 0;
        int unseen = 0;
        int unchanged = 0;
        int changed = 0;
        Log.d(this, "postProcessMetadata() start");
        Iterator<Entry<String, Metadata>> iterator = aspMetadata.entrySet().iterator();
        ArrayList<ContentProviderOperation> operationList = new ArrayList<>();
        while (iterator.hasNext()) {
            Entry<String, Metadata> entry = iterator.next();
            String fileId = entry.getKey();
            Log.d(this, "postProcessMetadata fileId: " + fileId);
            Metadata metadata = entry.getValue();
            if (metadata != null && getDeviceId() != null) {
                ContentResolver contentResolver = getContentResolver();
                Uri uri = ASPMediaStore.Files.getContentUriForDevice(getDeviceId());
                switch (metadata.state) {
                    case ADDED:
                        try {
                            added += 1;
                            ContentValues contentValues = metadata.toContentValues(getDeviceId());

                            ContentProviderOperation.Builder op = ContentProviderOperation.newInsert(uri).withValues(contentValues);
                            operationList.add(op.build());
                        } catch (Exception e) {
                            Log.e(this, "postProcessMetadata() - Exception inserting metadata : " + e);
                        }
                        break;
                    case UNSEEN:
                        try {
                            unseen += 1;
                            ContentProviderOperation.Builder op = ContentProviderOperation.newDelete(uri).withSelection(
                                    ASPMediaStore.BaseASPColumns.SOURCE_MEDIA_ID + " = ?",
                                    new String[]{metadata.id.toString()});
                            operationList.add(op.build());

                        } catch (Exception e) {
                            Log.e(this, "postProcessMetadata() - Exception deleting metadata : " + e);
                        }
                        break;
                    case UNCHANGED:
                        unchanged += 1;
                        /*
                         * since the metadata is unchanged, well, don't change it!
						 */
                        break;
                    case CHANGED:
                        try {
                            changed += 1;
                            ContentValues contentValues = metadata.toContentValues(getDeviceId());
                            ContentProviderOperation.Builder op = ContentProviderOperation
                                    .newUpdate(uri)
                                    .withValues(contentValues)
                                    .withSelection(ASPMediaStore.BaseASPColumns.SOURCE_MEDIA_ID + " = ?", new String[]{metadata.id.toString()});
                            operationList.add(op.build());
                        } catch (Exception e) {
                            Log.e(this, "postProcessMetadata() - exception updating metadata : " + e);
                        }
                        break;
                }
                if (operationList.size() >= BULK_OPERATION_LIMIT) {
                    try {
                        contentResolver.applyBatch(ASPMediaStore.AUTHORITY, operationList);
                    } catch (RemoteException re) {
                        Log.e(this, "postProcessMetadata() - RemoteException updating metadata : " + re);
                    } catch (OperationApplicationException aoe) {
                        Log.e(this, "postProcessMetadata() OperationApplicationException updating metadata : " + aoe);
                    } finally {
                        operationList.clear();
                    }
                }
            } else {
                Log.e(this, "file " + fileId + " metadata not in Map<> when it should have been!");
            }
        }

        if (operationList.size() > 0) {
            try {
                contentResolver.applyBatch(ASPMediaStore.AUTHORITY, operationList);
            } catch (RemoteException re) {
                Log.e(this, "postProcessMetadata() RemoteException updating metadata : " + re);
            } catch (OperationApplicationException aoe) {
                Log.e(this, "postProcessMetadata() OperationApplicationException updating metadata : " + aoe);
            } finally {
                operationList.clear();
            }
        }

        Log.d(this, added + " added; " + unseen + " unseen; " + unchanged + " unchanged; " + changed + " changed");
        totalAdded += added;
        totalUnseen += unseen;
        totalUnchanged += unchanged;
        totalChanged += changed;
    }

    private Map<String, Metadata> getMetadata() {
        Map<String, Metadata> directoryMetadata = new HashMap<>();
        ContentResolver contentResolver = getContentResolver();
        int deviceId = getCloudDevice().getId();
        Uri uri = ASPMediaStore.Files.getContentUriForDevice(deviceId);//getDeviceContentUri(mediaType);
            /* @formatter:off */
        try (Cursor cursor = contentResolver.query(
                uri,
                MediaType.none.columnNames,
                null,
                null,
                null)) {
            if (cursor != null && cursor.moveToFirst()) {
                while (!cursor.isAfterLast()) {
                    /*
                     * assume the metadata is UNSEEN; if it is CHANGED or UNCHANGED, the add file listener will mark it so
					 */
                    Metadata fileMetadata = metadataFromCursor(cursor, Metadata.State.UNCHANGED);
                    directoryMetadata.put(fileMetadata.id.toString(), fileMetadata);
                    cursor.moveToNext();
                }
            }
        }

        return directoryMetadata;
    }

    private void deleteFileFromDB(String fileId) {
        String where = ASPMediaStore.BaseASPColumns.SOURCE_MEDIA_ID + "= ?";
        String[] selectionArgs = {fileId};

        ContentResolver contentResolver = getContentResolver();
        Uri uri = getDeviceContentUri();

        int count = contentResolver.delete(uri, where, selectionArgs);
        Log.i(this, "deleteFileFromDB(), " + count + " files deleted");
    }

    private void deleteFileBatchFromDB(String[] fileIds) {
        ContentResolver contentResolver = getContentResolver();
        Uri uri = getDeviceContentUri();

        ArrayList<ContentProviderOperation> operationList = new ArrayList<>();
        for (String fileId : fileIds) {
            ContentProviderOperation.Builder op = ContentProviderOperation
                    .newDelete(uri)
                    .withSelection(ASPMediaStore.BaseASPColumns.SOURCE_MEDIA_ID + "= ?", new String[]{fileId});
            operationList.add(op.build());
        }

        try {
            contentResolver.applyBatch(ASPMediaStore.AUTHORITY, operationList);
        } catch (RemoteException re) {
            Log.e(this, "deleteFileBatchFromDB() - RemoteException updating : " + re);
        } catch (OperationApplicationException aoe) {
            Log.e(this, "deleteFileBatchFromDB() OperationApplicationException : " + aoe);
        } finally {
            operationList.clear();
        }

        Log.i(this, "deleteFileBatchFromDB(), files deleted");
    }

    public void deleteChildrenFromDB(String parentDirId) {
        String where = ASPMediaStore.BaseASPColumns.PARENT_CLOUD_ID + "= ?";
        String[] selectionArgs = {parentDirId};

        ContentResolver contentResolver = getContentResolver();
        Uri uri = getDeviceContentUri();

        int count = contentResolver.delete(uri, where, selectionArgs);
        Log.i(this, "deleteChildrenFromDB(), " + count + " files deleted");
    }

    public void deleteAllFileInfoFromDB() {
        String where = ASPMediaStore.BaseASPColumns.DEVICE_ID + "= ?";
        ContentResolver contentResolver = getContentResolver();
        Uri uri = getDeviceContentUri();
        String[] selectionArgs = {String.valueOf(getDeviceId())};
        int count = contentResolver.delete(uri, where, selectionArgs);
        Log.i(this, "deleteAllFileInfoFromDB(), deviceId = " + getDeviceId() + ", " + count + " files deleted");
    }

    public Uri getDeviceContentUri() {
        int deviceId = getCloudDevice().getId();
        return ASPMediaStore.Files.getContentUriForDevice(deviceId);
    }

    /* @formatter:off */
    private Metadata metadataFromCursor(
            Cursor cursor,
            Metadata.State state) {
        /* @formatter:on */
        String id = getCursorString(cursor, ASPMediaStore.BaseASPColumns.SOURCE_MEDIA_ID);
        if (id == null) {
            throw new RuntimeException("sourceMediaId is null");
        }
        long length = getCursorLong(cursor, ASPMediaStore.Audio.AudioColumns.SIZE);
        String parentCloudId = getCursorString(cursor, ASPMediaStore.BaseASPColumns.PARENT_CLOUD_ID); //jsub12.lee_150715_2
        String parentName = getCursorString(cursor, ASPMediaStore.Images.ImageColumns.BUCKET_DISPLAY_NAME); //jsub12_150924_1
        long dateModified = getCursorLong(cursor, ASPMediaStore.Audio.AudioColumns.DATE_MODIFIED);
        String displayName = getCursorString(cursor, ASPMediaStore.Audio.AudioColumns.DISPLAY_NAME);
        String mimeType = getCursorString(cursor, ASPMediaStore.Audio.AudioColumns.MIME_TYPE);
        String thumbnailURI = getCursorString(cursor, ASPMediaStore.Audio.AudioColumns.THUMBNAIL_URI);
        String fullURI = getCursorString(cursor, ASPMediaStore.Audio.AudioColumns.FULL_URI);
        String fullPath = getCursorString(cursor, ASPMediaStore.Audio.AudioColumns.DATA);  //jsublee_150907

        /* @formatter:off */
        Metadata fileMetadata = new Metadata(
                id,
                state,
                length,
                parentCloudId, //jsub12.lee_150715_2
                parentName,
                dateModified,
                displayName,
                mimeType,
                thumbnailURI,
                fullURI,
                fullPath);
        /* @formatter:on */
        return fileMetadata;
    }

    private long getCursorLong(Cursor cursor, String name) {
        long result = 0;
        int columnIndex = cursor.getColumnIndex(name);
        if (columnIndex >= 0) {
            result = cursor.getLong(columnIndex);
        }
        return result;
    }

    private String getCursorString(Cursor cursor, String name) {
        String result = null;
        int columnIndex = cursor.getColumnIndex(name);
        if (columnIndex >= 0) {
            result = cursor.getString(columnIndex);
        }
        return result;
    }

    private String getDeviceIdAsString() {
        String deviceIdAsString = "[unknown]";
        Integer deviceId = getDeviceId();
        if (deviceId != null) {
            deviceIdAsString = deviceId.toString();
        }
        return deviceIdAsString;
    }

    public Integer getDeviceId() {
        Integer deviceId = null;
        CloudDevice cloudDevice = getCloudDevice();
        if (cloudDevice != null) {
            deviceId = cloudDevice.getId();
        }
        return deviceId;
    }

    public void setFileStorageGatewayId(CloudFile result, String asString) {
        // TODO Auto-generated method stub
        result.setCloudId(asString);
    }

    public void setLargestChangeID(long _changeId) {
        prevLargestChangeId = _changeId;
        saveLargestChangeIdToPref(this.prevLargestChangeId);
    }

    private void saveLargestChangeIdToPref(long _largestChangeID) {
        Log.i(this, "saveLargestChangeIdToPref() called");

        Editor editor = getSharedPreferences().edit();
        editor.remove(LARGEST_CHANGE_ID);
        editor.putLong(LARGEST_CHANGE_ID, _largestChangeID);
        editor.apply();
    }

    private long getLargestChangeIdFromPref() {
        Log.i(this, "getLargestChangeIdFromPref() called");
        SharedPreferences preferences = getSharedPreferences();

        return preferences.getLong(LARGEST_CHANGE_ID, -1);
    }

    protected Drive getDrive() {
        Log.i(this, "getDrive() called, AccountName : " + context.getUserId());

        //GoogleAccountCredential credential = GoogleAccountCredential.usingOAuth2(getApplicationContext(), Collections.singleton(DriveScopes.DRIVE));
        GoogleAccountCredential credential = GoogleAccountCredential.usingOAuth2(
                getApplicationContext(),
                Arrays.asList(DriveScopes.DRIVE, AUTH_PICASA_WEB_ALBUMS));

        credential.setSelectedAccountName(context.getUserId());

        HttpTransport mTransport = AndroidHttp.newCompatibleTransport();
        JsonFactory mJsonFactory = new GsonFactory();

        return new Drive.Builder(mTransport, mJsonFactory, credential).setApplicationName("Cloud Manager").build();
    }

    @Override
    public boolean moveFile(String curDirId, String sourceId, String targetDirId) {
        long startTime = System.currentTimeMillis();
        Log.i(this, "moveFile(), curDirId = " + curDirId + ", sourceId = " + sourceId + ", targetDirId = " + targetDirId);

        try {
            if (getAuthorization()) {
                com.google.api.services.drive.model.File dFile = new com.google.api.services.drive.model.File();
                ParentReference newParent = new ParentReference();
                newParent.setId(targetDirId);

                ArrayList<ParentReference> parentList = new ArrayList<ParentReference>();
                parentList.add(newParent);

                dFile.setParents(parentList);

                getContext().mDrive.files().patch(sourceId, dFile).execute();

                ((GoogleDriveContext) getContext()).putDB(dFile);
                return true;
            }
        } catch (Exception e) {
            Log.e(this, "moveFile() - Exceptions : " + e.getMessage());
        } finally {
            broadcastAuthorizationState();
            long endTime = System.currentTimeMillis();
            Log.i(this, "moveFile() took  " + (endTime - startTime) + " millisecond(s)");
        }

        return false;
    }

    @Override
    public Result moveFileBatch(ArrayList<String[]> moveList, final StreamProgressListener listener) throws IOException {
        Log.i(this, "moveFileBatch() called - sourceIds.size() : " + moveList.size());
        final APIBatchRequest request = new APIBatchRequest();
        final ArrayList<com.google.api.services.drive.model.File> successList = new ArrayList<>();
        final Result ret = new Result(true);

        JsonBatchCallback<com.google.api.services.drive.model.File> fileJsonBatchCallback = new JsonBatchCallback<com.google.api.services.drive.model.File>() {
            @Override
            public void onFailure(GoogleJsonError googleJsonError, HttpHeaders httpHeaders) throws IOException {
                Log.e(this, "moveFileBatch batchFatil " + googleJsonError.getMessage());

                if (googleJsonError.getMessage().contains("Rate Limit Execeeded")) {
                    request.mBatchRetryCnt++;
                }
                request.bBatchSuccess = false;
            }

            @Override
            public void onSuccess(com.google.api.services.drive.model.File file, HttpHeaders httpHeaders) throws IOException {
                Log.d(this, "moveFileBatch()/fileJsonBatchCallback - onSuccess() : " + file.getTitle());
                listener.bytesTransferred(1);
                successList.add(file);
            }
        };

        try {
            if (getAuthorization()) {
                int retryMaxBatchCnt = RETRY_MAX_BATCH_COUNT * moveList.size();
                BatchRequest batch = null;
                do {
                    request.bBatchSuccess = true;
                    for (String[] file : moveList) {
                        String sourceId = file[0];
                        String newName = file[1];
                        String targetDirId = file[2];
                        Log.d(this, "moveFileBatch() - sourceId : " + sourceId + ", newName : " + newName + ", targetDirId : " + targetDirId);

                        if (sourceId != null) {
                            com.google.api.services.drive.model.File dFile = new com.google.api.services.drive.model.File();
                            dFile.setTitle(newName);
                            ParentReference newParent = new ParentReference();
                            newParent.setId(targetDirId);

                            ArrayList<ParentReference> parentList = new ArrayList<ParentReference>();
                            parentList.add(newParent);

                            dFile.setParents(parentList);

                            if (batch == null) {
                                batch = getDrive().batch();
                            }
                            Files.Patch moveRequest = getContext().mDrive.files().patch(sourceId, dFile);
                            moveRequest.queue(batch, fileJsonBatchCallback);
                        }


                        if ((batch != null) && ((batch.size() >= API_CALL_BATCH_LIMIT) || (moveList.size() == moveList.indexOf(file) + 1))) {
                            Log.d(this, "moveFileBatch() - doBatchExecute, batch.size() : " + batch.size());
                            request.doBatchExecute(batch);
                            batch = null;
                        }
                    }

                }
                while (!request.bBatchSuccess && (request.mBatchRetryCnt > 0) && (request.mBatchRetryCnt < retryMaxBatchCnt));
            }
        } catch (IOException e) {
            Log.e(this, "moveFileBatch() - IOException : " + e.getMessage());
            throw e;
        }

        if ((successList != null) && !successList.isEmpty()) {
            for (com.google.api.services.drive.model.File file : successList) {
                ret.mFileIdList.add(file.getId());
                ret.mFileNameList.add(file.getTitle());
                ret.mFileDstIdList.add(((GoogleDriveContext) getContext()).getParentsCloudId(file));
            }

            ((GoogleDriveContext) getContext()).bulkUpdate(successList);
        }

        ret.mRet = request.bBatchSuccess;
        return ret;
    }

    @Override
    public String createDirectory(String parentId, String directoryName) {
        long startTime = System.currentTimeMillis();
        Log.i(this, "CloudStorageBase::createDirectory(), parentId = " + parentId + ", directoryName = " + directoryName);
        String createdDirId = null;

        try {
            if (getAuthorization()) {
                com.google.api.services.drive.model.File dFile = new com.google.api.services.drive.model.File();
                dFile.setTitle(directoryName);
                dFile.setMimeType("application/vnd.google-apps.folder");

                if (parentId != null && parentId.length() > 0) {
                    dFile.setParents(Arrays.asList(new ParentReference().setId(parentId)));
                }

                Files.Insert insertRequest = getContext().mDrive.files().insert(dFile);
                com.google.api.services.drive.model.File file = retryRequest(insertRequest, RETRY_MAX_COUNT);

                if (file != null) {
                    createdDirId = file.getId();
                    Log.i(this, "createDirectory(), returned file id = " + createdDirId);

                    ((GoogleDriveContext) getContext()).putDB(file);
                }
            }
        } catch (Exception e) {
            Log.e(this, "createDirectory() - Exception : " + e.getMessage());
        } finally {
            broadcastAuthorizationState();
            long endTime = System.currentTimeMillis();
            Log.i(this, "createDirectory() took  " + (endTime - startTime) + " millisecond(s)");
            return createdDirId;
        }
    }

    @Override
    public boolean renameFile(String directoryId, String sourceId, String newName) {
        long startTime = System.currentTimeMillis();
        Log.i(this, "renameFile(), directoryId = " + directoryId + ", sourceId = " + sourceId + ", newName = " + newName);

        boolean success = false;

        try {
            if (getAuthorization()) {
                com.google.api.services.drive.model.File dFile = new com.google.api.services.drive.model.File();
                dFile.setTitle(newName);
                //rename the file
                String cloudId = sourceId.replace("//", "");

                Files.Patch patchRequest = getContext().mDrive.files().patch(cloudId, dFile);
                patchRequest.setFields("title");
                com.google.api.services.drive.model.File file = retryRequest(patchRequest, RETRY_MAX_COUNT);

                if (file != null) {
                    requestSync(true);
                    Log.i(this, "renameFile() returns true");
                    success = true;
                }
            }
        } catch (Exception exc) {
            Log.e(this, "renameFile() - Exception : " + exc.getMessage());
            success = false;
        } finally {
            broadcastAuthorizationState();
            long endTime = System.currentTimeMillis();
            Log.i(this, "renameFile() took  " + (endTime - startTime) + " millisecond(s)");
        }
        Log.i(this, "renameFile() returns " + success);
        return success;
    }

    @Override
    public boolean copyFile(String parentDirectoryId, String sourceId, String targetDirId, String newName) {
        long startTime = System.currentTimeMillis();
        Log.i(this, "copyFile(), parentDirectoryId = " + parentDirectoryId + ", sourceId = " + sourceId
                + ", targetDirId = " + targetDirId + ", newName = " + newName);

        try {
            if (getAuthorization()) {
                String existingFileId = isExistingFilename(targetDirId, newName);

                //Set File's metadata
                com.google.api.services.drive.model.File copiedFile = new com.google.api.services.drive.model.File();
                copiedFile.setTitle(newName);

                //Set the parent folder
                if (targetDirId != null && targetDirId.length() > 0) {
                    copiedFile.setParents(Arrays.asList(new ParentReference().setId(targetDirId)));
                }

                getContext().mDrive.files().copy(sourceId, copiedFile).execute();

                if (existingFileId != null) {
                    getContext().mDrive.files().delete(existingFileId).execute();
                    Log.i(this, "copyFile(), success to delete existing file, " + existingFileId);
                }

                ((GoogleDriveContext) getContext()).putDB(copiedFile);
                return true;
            }
        } catch (Exception e) {
            Log.e(this, "copyFile() - Exception : " + e.getMessage());
        } finally {
            broadcastAuthorizationState();
            long endTime = System.currentTimeMillis();
            Log.i(this, "copyFile() took  " + (endTime - startTime) + " millisecond(s)");
        }

        return false;
    }

    /**
     * checks if the filename is already existing in the target directory
     *
     * @param targetDirId the cloudId of target directory
     * @param filename    the filename to be checked
     * @return the cloudId of the child file existent in the directory, or null
     * if there is no file of which the filename is matched
     */
    private String isExistingFilename(String targetDirId, String filename) {
        Log.i(this, "isExistingFilename(), targetDirId = " + targetDirId + ", filename = " + filename);

        String retVal = null;
        Cursor cursor = contentResolver.query(
                getDeviceContentUri(),
                new String[]{ASPMediaStore.Documents.DocumentColumns.SOURCE_MEDIA_ID},
                ASPMediaStore.BaseASPColumns.PARENT_CLOUD_ID + "=? AND " + ASPMediaStore.Documents.DocumentColumns.DISPLAY_NAME + "=?",
                new String[]{targetDirId, filename},
                null);

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                retVal = getCursorString(cursor, ASPMediaStore.BaseASPColumns.SOURCE_MEDIA_ID);
            }
            cursor.close();
        }

        Log.i(this, "isExistingFilename(), return = " + retVal);
        return retVal;
    }

    @Override
    public Result copyFileBatch(ArrayList<String[]> copyList, final StreamProgressListener listener) throws IOException {
        Result ret = new Result(true);
        ArrayList<com.google.api.services.drive.model.File> successList = new ArrayList<>();
        long startTime = System.currentTimeMillis();
        try {
            if (getAuthorization()) {
                for (String[] file : copyList) {
                    String sourceId = file[0];
                    String newName = file[1];
                    String targetDirId = file[2];
                    Log.d(this, "copyFileBatch() - sourceId : " + sourceId + ", newName : " + newName + ", targetDirId : " + targetDirId);

                    if (sourceId != null) {
                        //Set File's metadata
                        com.google.api.services.drive.model.File copiedFile = new com.google.api.services.drive.model.File();
                        copiedFile.setTitle(newName);

                        //Set the parent folder
                        if (targetDirId != null && targetDirId.length() > 0) {
                            copiedFile.setParents(Arrays.asList(new ParentReference().setId(targetDirId)));
                        }

                        Files.Copy copyRequest = getContext().mDrive.files().copy(sourceId, copiedFile);

                        com.google.api.services.drive.model.File dFile = retryRequest(copyRequest, RETRY_MAX_COUNT);

                        if (dFile != null && dFile.getId() != null) {
                            Log.d(this, "copyFileBatch() - copied file id = " + dFile.getId());
                            successList.add(dFile);
                            listener.bytesTransferred(1);
                        } else {
                            Log.e(this, "copyFileBatch() - file copy failed");
                            ret.mRet = false;
                        }
                    }
                }
            }
        } catch (IOException e) {
            Log.e(this, "copyFileBatch() - IOException : " + e.getMessage());
            throw e;
        }

        if ((successList != null) && !successList.isEmpty()) {
            for (com.google.api.services.drive.model.File file : successList) {
                ret.mFileIdList.add(file.getId());
                ret.mFileNameList.add(file.getTitle());
                ret.mFileDstIdList.add(((GoogleDriveContext) getContext()).getParentsCloudId(file));
            }

            ((GoogleDriveContext) getContext()).bulkInsert(successList);
        }
        long endTime = System.currentTimeMillis();
        Log.d(this, "copyFileBatch () took " + (endTime - startTime) + " millisecond(s)");
        return ret;
    }

/*
    @Override
    public Result copyFileBatch(ArrayList<String[]> copyList, final StreamProgressListener listener) throws IOException {
        final APIBatchRequest request = new APIBatchRequest();
        final ArrayList<com.google.api.services.drive.model.File> successList = new ArrayList<>();
        final Result ret = new Result(true);

        JsonBatchCallback<com.google.api.services.drive.model.File> fileJsonBatchCallback = new JsonBatchCallback<com.google.api.services.drive.model.File>() {
            @Override
            public void onFailure(GoogleJsonError googleJsonError, HttpHeaders httpHeaders) throws IOException {
                Log.e(this, "copyFileBatch() - onFailure : " + googleJsonError.getMessage());

                if (googleJsonError.getMessage().contains("Rate Limit Exceeded")) {
                    request.mBatchRetryCnt++;
                }

                request.bBatchSuccess = false;
                throwQuotaException(googleJsonError.getMessage());
            }

            @Override
            public void onSuccess(com.google.api.services.drive.model.File file, HttpHeaders httpHeaders) throws IOException {
                Log.d(this, "copyFileBatch() - onSuccess : " + file.getTitle());
                listener.bytesTransferred(1);
                successList.add(file);
            }
        };

        try {
            if (getAuthorization()) {
                int retryMaxBatchCnt = RETRY_MAX_BATCH_COUNT * copyList.size();
                ArrayList<String> existingFileIdList = new ArrayList<>();
                BatchRequest batch = null;
                do {
                    request.bBatchSuccess = true;
                    for (String[] file : copyList) {
                        String sourceId = file[0];
                        String newName = file[1];
                        String targetDirId = file[2];
                        Log.d(this, "copyFileBatch() - sourceId : " + sourceId + ", newName : " + newName + ", targetDirId : " + targetDirId);

                        if (sourceId != null) {
                            String existingFileId = isExistingFilename(targetDirId, newName);

                            //Set File's metadata
                            com.google.api.services.drive.model.File copiedFile = new com.google.api.services.drive.model.File();
                            copiedFile.setTitle(newName);

                            //Set the parent folder
                            if (targetDirId != null && targetDirId.length() > 0) {
                                copiedFile.setParents(Arrays.asList(new ParentReference().setId(targetDirId)));
                            }

                            if (batch == null) {
                                batch = getDrive().batch();
                            }
                            Files.Copy copyRequest = getContext().mDrive.files().copy(sourceId, copiedFile);
                            copyRequest.queue(batch, fileJsonBatchCallback);

                            if (existingFileId != null) {
                                existingFileIdList.add(existingFileId);
                            }
                        }
                        if ((batch.size() >= API_CALL_BATCH_LIMIT) || (copyList.size() == copyList.indexOf(file) + 1)) {
                            Log.d(this, "copyFileBatch() - batch.size() : " + batch.size());
                            request.doBatchExecute(batch);
                            batch = null;
                            if (!request.bBatchSuccess) {
                                try {
                                    Thread.sleep(1000);
                                } catch (InterruptedException e) {
                                    Log.e(this, "copyFileBatch() - InterruptedException : " + e.getMessage());
                                }
                            }
                        }
                    }
                }
                while (!request.bBatchSuccess && (request.mBatchRetryCnt > 0) && (request.mBatchRetryCnt < retryMaxBatchCnt));

                if (existingFileIdList.size() > 0) {
                    String[] deleteExistingId = new String[existingFileIdList.size()];
                    int index = 0;
                    for (String existingFileId : existingFileIdList) {
                        deleteExistingId[index++] = existingFileId;
                    }
                    request.bBatchSuccess = request.bBatchSuccess && deleteFileBatch(deleteExistingId);
                }
            }
        } catch (IOException e) {
            Log.e(this, "copyFileBatch() - IOException : " + e.getMessage());
            throw e;
        }

        if ((successList != null) && !successList.isEmpty()) {
            for (com.google.api.services.drive.model.File file : successList) {
                ret.mFileIdList.add(file.getId());
                ret.mFileNameList.add(file.getTitle());
                ret.mFileDstIdList.add(((GoogleDriveContext) getContext()).getParentsCloudId(file));
            }

            ((GoogleDriveContext) getContext()).bulkInsert(successList);
        }
        ret.mRet = request.bBatchSuccess;
        return ret;
    }
*/
    public ArrayList<CloudFile> getChildren(String targetDirId, boolean bNeedDetailInfo, Result result) {
        Log.i(this, "getChildren(), targetDirId = " + targetDirId);
        ArrayList<CloudFile> files = new ArrayList<>();
        ArrayList<Metadata> childrenMeta = getMetadataOfChildren(MediaType.directory, targetDirId);

        Log.i(this, "getChildren(), count of child meta = " + childrenMeta.size());
        for (Metadata childMeta : childrenMeta) {
            if (mCancellationSignal != null && mCancellationSignal.isCanceled() && mCancellationFileId.equals(targetDirId)) {
                Log.i(this, "getChildren(), cancelled " + targetDirId); // to prevent memory leak issue
                result.mRet = false;
                break;
            }
            files.add(getFileFromFileMeta(childMeta, bNeedDetailInfo));
        }

        return files;
    }

    //jsub12.lee_150721
    private CloudFile getFileFromFileMeta(Metadata fileMeta, boolean bNeedDetailInfo) {
        CloudFile file;
        Log.d(this, "getFileFromFileMeta(), bNeedDetailInfo = " + bNeedDetailInfo
                + ", --------- Added -----------------------------------------------");

        if (fileMeta.mimeType.equals(MimeType.DIR_MIMETYPE)) {
            file = new CloudDirectory(this.context, null, fileMeta.displayName);
        } else {
            file = new CloudFile(this.context, null, fileMeta.displayName);
        }

        file.setCloudId(Metadata.Id.getFileIdFromSourceMediaId(fileMeta.id.toString()));
        file.setMimeType(fileMeta.mimeType);
        Log.i(this, "getFileFromFileMeta(), #cloudId = " + file.getCloudId()
                + ", #dateTaken = " + file.getCreated() + ", #mimeType = " + file.getMimeType());

        if (bNeedDetailInfo && file.getMimeType().equals(MimeType.DIR_MIMETYPE)) {
            //jsub12_151006
            ArrayList<Metadata> childrenMetadata = getMetadataOfChildren(MediaType.directory, file.getCloudId());
            file.setChildCount(childrenMetadata.size());
            file.setChildDirCount(getCountOfDirFromMetadataList(childrenMetadata));
            //file.setLength(getTotalSizeOfChildFile(childrenMetadata));

            ArrayList<Metadata> metadataOfDescendants = getMetadataOfDescendants(file.getCloudId());
            file.setDescendantsCount(metadataOfDescendants.size());
            file.setDescendantDirCount(getCountOfDirFromMetadataList(metadataOfDescendants));
            file.setLength(getTotalSizeOfDescendants(metadataOfDescendants));

            Log.i(this, "getFileFromFileMeta(), #DIR child count = "
                    + "# DIR child count = " + file.getChildCount()
                    + "# DIR child dir count = " + file.getChildDirCount()
                    + "# DIR descendants count = " + file.getDescendantsCount()
                    + "# DIR descendant dir count = " + file.getDescendantDirCount()
                    + "# DIR length = " + file.length());
        } else {
            file.setLength(fileMeta.length);
            Log.i(this, "getFileFromFileMeta(), # length = " + file.length());
        }

        file.setName(fileMeta.displayName);
        file.setLastModified(fileMeta.dateModified);
        Log.i(this, "getFileFromFileMeta(), # name = " + file.getName() + ", # dateModified = " + fileMeta.dateModified);

        return file;
    }

    //jsub12_151006
    private int getCountOfDirFromMetadataList(ArrayList<Metadata> metadataList) {
        int retVal = 0;
        for (Metadata metadata : metadataList) {
            if (metadata.mimeType.equals(MimeType.DIR_MIMETYPE)) {
                retVal++;
            }
        }

        return retVal;
    }

    //jsub12_151006
    private long getTotalSizeOfDescendants(ArrayList<Metadata> metadataList) {
        long retVal = 0;
        for (Metadata metadata : metadataList) {
            if (!metadata.mimeType.equals(MimeType.DIR_MIMETYPE)) {
                retVal = retVal + metadata.length;
            }
        }

        return retVal;
    }

    private ArrayList<Metadata> getMetadataOfDescendants(String ancestorCloudId) {
        Log.i(this, "getMetadataOfDescendants(), ancestorCloudId = " + ancestorCloudId);

        ArrayList<Metadata> metadataOfDescendants = new ArrayList<Metadata>();
        ArrayList<Metadata> metadataOfChildren = getMetadataOfChildren(MediaType.none, ancestorCloudId);

        for (Metadata metadata : metadataOfChildren) {
            if (metadata.mimeType.equals(MimeType.DIR_MIMETYPE)) {
                metadataOfDescendants.addAll(getMetadataOfDescendants(metadata.id.toString()));
            }
        }

        metadataOfDescendants.addAll(metadataOfChildren);

        return metadataOfDescendants;
    }

    public <T> T retryRequest(DriveRequest<T> request, int retryTimes) throws IOException {
        int retryCnt = 0;
        try {
            do {
                try {
                    Log.d(this, "retryRequest: " + request.getClass().getName() + " " + request + "---retry = " + retryCnt);
                    return request.execute();
                } catch (GoogleJsonResponseException e) {
                    Log.d(this, "retryRequest GoogleJsonResponseException: " + e.getDetails().getErrors().get(0).getReason());
                    int code = e.getDetails().getCode();
                    switch (code) {
                        case 403:
                            String reason = e.getDetails().getErrors().get(0).getReason();
                            String message = e.getDetails().getErrors().get(0).getMessage();
                            switch (reason) {
                                case REASON_RATE_LIMIT_EXCEEDED:
                                case REASON_USER_RATE_LIMIT_EXCEEDED:
                                    try {
                                        Thread.sleep(1000);
                                    } catch (InterruptedException e1) {
                                        e1.printStackTrace();
                                    }
                                    ++retryCnt;
                                    break;
                                case REASON_STORAGE_QUOTA_EXCEEDED:
                                    throwQuotaException(message);
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case 404:
                            throw e;
                        case 408:
                        case 500:
                        case 502:
                        case 503:
                        case 504:
                            Log.d(this, "retryRequest - retry error: " + code + "- " + e.getMessage());
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e1) {
                                e1.printStackTrace();
                            }
                            ++retryCnt;
                            break;
                        default:
                            Log.d(this, "retryRequest - no_need_retry exception: " + e.getMessage());
                            throw new IOException(CloudStorageError.NO_NEED_RETRY_ERROR + e.getMessage());
                    }
                }
            } while (retryCnt > 0 && retryCnt < retryTimes);
        } catch (UserRecoverableAuthIOException e) {
            CloudAuthorizationException.showAuthActivity(getApplicationContext(), getCloudDevice().getId(), device, e.getIntent());
        }
        throw new IOException(CloudStorageError.RETRY_MAX_ERROR);
    }

    private void savePreferenceLong(PreferenceType type, long value) {
        Log.i(this, "savePreferenceLong() " + type.name() + " " + value);

        Editor editor = getSharedPreferences().edit();
        editor.remove(type.name());
        editor.putLong(type.name(), value);
        editor.apply();
    }

    private long getPreferenceLong(PreferenceType type) {
        SharedPreferences preferences = getSharedPreferences();
        Log.i(this, "getPreferenceLong() " + type.name() + " " + preferences.getLong(type.name(), 0));
        return preferences.getLong(type.name(), 0);
    }

    @Override
    public boolean emptyTrash() {
        return false;
    }

    @Override
    public boolean deleteTrash(String sourceId) {
        return false;
    }

    @Override
    public boolean deleteTrashBatch(String[] sourceIds) {
        return false;
    }

    @Override
    public String restoreTrash(String sourceId) {
        return null;
    }

    @Override
    public ArrayList<String> restoreTrashBatch(String[] sourceIds) {
        return null;
    }

    @Override
    public SQLiteDatabase getReadableCloudDatabase() {
        return context.getDatabaseHelper().getReadableDatabase();
    }

    @Override
    public long getLastSync() {
        return getPreferenceLong(PreferenceType.LAST_SYNC_TIME);
    }

    @Override
    public long[] getDetailedUsed() {
        long[] detailedUsage = new long[3];

        detailedUsage[0] = getPreferenceLong(PreferenceType.GOOGLE_DRIVE_USED);
        detailedUsage[1] = getPreferenceLong(PreferenceType.GMAIL_USED);
        detailedUsage[2] = getPreferenceLong(PreferenceType.PHOTOS_USED);

        return detailedUsage;
    }

    @Override
    public long[] updateQuota() {
        long[] bRet = null;
        About about = null;
        try {
            about = getAbout();
        } catch (IOException e) {
            Log.e(this, "exception while getting About obj :" + e.getMessage() + "// --------");
        }

        if (device != null && about != null) {
            saveDetailedQuota(about);
            long quota = about.getQuotaBytesTotal();
            long used = about.getQuotaBytesUsedAggregate();

            Log.i(this, "updateQuota(), quota : " + quota + ", used : " + used);
            if ((device.getCapacityInBytes() != quota) || (device.getUsedCapacityInBytes() != used)) {
                Log.i(this, "updateQuota(), capacity and / or used changed, will sync metadata");
                device.setCapacityInBytes(quota);
                device.setUsedCapacityInBytes(used);
                dataModel.updateDevice(device);
            }
            bRet = new long[2];
            bRet[0] = quota;
            bRet[1] = used;
        } else {
            Log.i(this, "updateQuota(), device is null or about is null");
        }

        return bRet;
    }

    private void throwQuotaException(String message) throws IOException {
        if (TextUtils.equals(message, MESSAGE_EXCEEDED_STORAGE_QUOTA)) {
            Log.d(this, "throwQuotaException() out of storage");
            throw new IOException(CloudStorageError.OUT_OF_STORAGE_ERROR + ": " + MESSAGE_EXCEEDED_STORAGE_QUOTA);
        }
    }

    @Override
    public void cancel(CloudGatewayConstants.OperationType type) {

    }
}


package com.mfluent.asp.common.util;

import java.util.Iterator;
import java.util.Set;

import android.content.Intent;
import android.os.Bundle;

public class IntentHelper {

	public static String intentToString(Intent intent) {
		StringBuilder str = new StringBuilder();
		if (intent == null) {
			str.append("intent is null");
			str.toString();
		} else {
			str.append(intent.getAction());
			str.append(" ");
			try {
				Bundle bundle = intent.getExtras();
				if (bundle != null) {
					Set<String> keys = bundle.keySet();
					Iterator<String> it = keys.iterator();
					while (it.hasNext()) {
						String key = it.next();
						str.append(key);
						str.append(":");
						str.append(bundle.get(key));
						str.append(", ");
					}
				}

			} catch (Exception e) {
				// Don't let logging cause any potential problems.
				str.append("failed to parse intent: ").append(e.getMessage());
			}
		}
		return str.toString();
	}

}

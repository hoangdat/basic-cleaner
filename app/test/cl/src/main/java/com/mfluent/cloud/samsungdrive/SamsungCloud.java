/**
 * Copyright, 2015, SEC. All rights reserved.
 */

package com.mfluent.cloud.samsungdrive;

import android.content.Context;
import android.content.Intent;

import com.mfluent.asp.common.datamodel.ASPFile;
import com.mfluent.asp.common.clouddatamodel.AbsCloudContext.AuthorizationState;
import com.mfluent.cloud.samsungdrive.common.CloudDirectory;
import com.mfluent.cloud.samsungdrive.common.CloudFile;
import com.mfluent.cloud.samsungdrive.common.CloudStorageBase;
import com.mfluent.log.Log;

import java.io.FileNotFoundException;

/**
 * An implementation of cloud storage for the Samsung Drive service.
 */
public class SamsungCloud extends CloudStorageBase<SamsungDriveContext> {

    private static final long MAXIMUM_FILE_SIZE = 1024 * 1024 * 1024; // Maximum file size : 1GB

    public SamsungCloud() {
        super(Type.SAMSUNG_DRIVE, AuthType.OAUTH20, false, new SamsungDriveContext());
    }

    @Override
    public long getMaximumFileSize() {
        return MAXIMUM_FILE_SIZE;
    }

    @Override
    public String getStorageGatewayFileId(ASPFile file) throws FileNotFoundException {
        Log.i(this, "getStorageGatewayFileId() called, target file = " + file);
        if (!(file instanceof CloudFile)) {
            throw new FileNotFoundException("not a CloudFile!");
        }
        Log.i(this, "getStorageGatewayFileId() returns = " + ((CloudFile) file).getCloudId());
        return ((CloudFile) file).getCloudId();
    }

    @Override
    protected boolean idsUseBase64() {
        return false;
    }

    @Override
    protected boolean deleteFileFromDirectory(CloudDirectory directory, String storageGatewayId) throws Exception {
        Log.i(this, "deleteFileFromDirectory(): directory = " + directory + ", storageGatewayId = " + storageGatewayId);
        CloudFile file = directory.getFileByCloudId(storageGatewayId);
        return (file != null) ? deleteFile(file) : true;
    }

    @Override
    protected AuthorizationState getAuthorizationInDevice() {
        if (!this.isSignIn) {
            Log.i(this, "getAuthorization():: isSignIn :" + isSignIn);
            return AuthorizationState.NO;
        }

        return AuthorizationState.YES;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        Log.i(this, "onReceive():: action = " + action);

        if (action.contentEquals(CLOUD_OAUTH1_RESPONSE)) {
            final String accountName = intent.getStringExtra("Account");

            this.context.setUserId(accountName);
            registerSppReceiver();

            isSignIn = true;
            saveAccountInfoToPref(accountName, isSignIn);
            setIsSignedIn(true);

            setAndCheckAuthorizationState(getAuthorizationInDevice());
            broadcastAuthorizationState();
        } else {
            super.onReceive(context, intent);
        }
    }
}

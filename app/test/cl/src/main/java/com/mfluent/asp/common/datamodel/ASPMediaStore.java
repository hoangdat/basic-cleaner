package com.mfluent.asp.common.datamodel;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;
import android.util.Base64;

import com.samsung.android.sdk.slinkcloud.CloudGatewayMediaStore;

import java.io.File;

/**
 * @author Ilan Klinghofer
 */
public final class ASPMediaStore extends CloudGatewayMediaStore {
    public static final int DEFAULT_THUMBNAIL_WIDTH = 512;
    public static final int DEFAULT_THUMBNAIL_HEIGHT = 384;

    private static Uri buildOrphanCleanupUri(long deviceId, String path) {
        return Uri.parse(ContentResolver.SCHEME_CONTENT + "://" + AUTHORITY + '/' + path + "/cleanup/" + deviceId);
    }

    public static class CallMethods extends CloudGatewayMediaStore.CallMethods {
    }

    public interface MediaTypes {
        int IMAGE = Files.FileColumns.MEDIA_TYPE_IMAGE;
        int VIDEO = Files.FileColumns.MEDIA_TYPE_VIDEO;
        int AUDIO = Files.FileColumns.MEDIA_TYPE_AUDIO;
        int ALBUM = Files.FileColumns.MEDIA_TYPE_ALBUM;
        int DOCUMENT = Files.FileColumns.MEDIA_TYPE_DOCUMENT;
        int DIRECTORY = Files.FileColumns.MEDIA_TYPE_DIRECTORY; //jsub12.lee_150717
        int NONE = Files.FileColumns.MEDIA_TYPE_NONE;

        int CAPTION = ALBUM + 1;
        int CAPTION_INDEX = ALBUM + 2;
    }

    public interface DeviceColumns extends CloudGatewayMediaStore.DeviceColumns {

        /**
         * The sort order of devices returned by the device list from server.
         * <p/>
         * Type: INTEGER
         * </P>
         */
        String SERVER_SORT_KEY = "server_sort_key";

        /**
         * The max number of the connection for transmission.
         * <p/>
         * Type: INTEGER
         * </P>
         */
        String MAX_NUM_TX_CONN = "max_num_tx_connection";

        /**
         * The javascript to be injected to the OAuth webview screen
         * <p/>
         * Type: TEXT
         * </P>
         */
        String OAUTH_WEBVIEW_JS = "oAuth_webview_js";

    }

    public static class Device extends CloudGatewayMediaStore.Device implements DeviceColumns {
    }

    public interface BaseASPColumns extends CloudGatewayMediaStore.BaseSamsungLinkColumns {

        /**
         * The id of the media on the source device.
         * <p/>
         * Type: TEXT
         * </P>
         */
        String SOURCE_MEDIA_ID = "source_media_id";

        /**
         * The cloud id of parent directory
         * <p/>
         * Type: TEXT
         * </P>
         */
        String PARENT_CLOUD_ID = "parent_cloud_id";  //jsub12.lee_150715_2
    }

    public interface KeywordColumns {
        String KEYWORD_TYPE = "keyword_type";
    }

    public interface JournalColumns {

        /**
         * The primary key of this table.
         * <p/>
         * Type: INTEGER
         * </P>
         */
        String _ID = BaseColumns._ID;

        /**
         * Whether or not this journal entry is for a record deletion.
         * <p/>
         * Type: INTEGER (Boolean)
         * </P>
         */
        String IS_DELETE = "is_delete";

        /**
         * The foreign key to the SOURCE_MEDIA_ID field in the media table that
         * this journal relates to.
         * <p/>
         * Type: TEXT
         * </P>
         */
        String MEDIA_ID = "media_id";

        /**
         * The _id field from the original entry in the journal table for this
         * piece of media. If this journal record is the original record, then
         * this field will be null.
         * <p/>
         * Type: INTEGER
         * </P>
         */
        String ORIG_JOURNAL_ID = "orig_journal_id";

        /**
         * The timestamp that this journal entry was made. In milliseconds since
         * Jan 1, 1970 GMT.
         * <p/>
         * Type: INTEGER (long)
         * </P>
         */
        String TIMESTAMP = "timestamp";

        Uri getContentUri();
    }

    public interface MediaColumns extends CloudGatewayMediaStore.MediaColumns, BaseASPColumns {

        /**
         * The thumbnail uri
         * <p/>
         * Type: TEXT
         * </P>
         */
        String THUMBNAIL_URI = "thumbnail_uri";
    }

    public interface DateTakenGroupingColumns {
        String DATE_CODE = "date_code";
    }

    public static final class Images extends CloudGatewayMediaStore.Images {

        public interface ImageColumns extends CloudGatewayMediaStore.Images.ImageColumns, MediaColumns {

        }

        public static final class Media extends CloudGatewayMediaStore.Images.Media implements ImageColumns, DateTakenGroupingColumns {
        }

        public static final class Journal implements JournalColumns {

            public static final String PATH = "image_journal";

            public static final String CONTENT_TYPE = ASPMediaStore.buildContentType(PATH);

            public static final String ENTRY_CONTENT_TYPE = ASPMediaStore.buildEntryContentType(PATH);

            public static final Uri CONTENT_URI = ASPMediaStore.buildContentUri(PATH);

            @Override
            public Uri getContentUri() {
                return CONTENT_URI;
            }
        }
    }

    public static final class Video extends CloudGatewayMediaStore.Video {

        public interface VideoColumns extends CloudGatewayMediaStore.Video.VideoColumns, MediaColumns {
        }

        public static final class Media extends CloudGatewayMediaStore.Video.Media implements VideoColumns, DateTakenGroupingColumns {
        }

        public static final class Journal implements JournalColumns {

            public static final String PATH = "video_journal";

            public static final String CONTENT_TYPE = ASPMediaStore.buildContentType(PATH);

            public static final String ENTRY_CONTENT_TYPE = ASPMediaStore.buildEntryContentType(PATH);

            public static final Uri CONTENT_URI = ASPMediaStore.buildContentUri(PATH);

            @Override
            public Uri getContentUri() {
                return CONTENT_URI;
            }
        }
    }

    /**
     * @author Ilan Klinghofer
     */
    public static final class Audio {

        public interface AlbumColumns extends CloudGatewayMediaStore.Audio.AlbumColumns {

            /**
             * The thumbnail uri
             * <p/>
             * Type: TEXT
             * </P>
             */
            String THUMBNAIL_URI = MediaColumns.THUMBNAIL_URI;
        }

        public static final class Albums extends CloudGatewayMediaStore.Audio.Albums {
            public static Uri getOrphanCleanUriForDevice(int deviceId) {
                return ASPMediaStore.buildOrphanCleanupUri(deviceId, PATH);
            }
        }

        public interface ArtistColumns extends CloudGatewayMediaStore.Audio.ArtistColumns {

        }

        public static final class Artists extends CloudGatewayMediaStore.Audio.Artists implements ArtistColumns {

            public static Uri getOrphanCleanUriForDevice(int deviceId) {
                return ASPMediaStore.buildOrphanCleanupUri(deviceId, PATH);
            }
        }

        public interface AudioColumns extends CloudGatewayMediaStore.Audio.AudioColumns, MediaColumns {

            /**
             * The id of the album on the device's database.
             * <p/>
             * Type: TEXT
             * </P>
             */
            String SOURCE_ALBUM_ID = "source_album_id";
        }

        public static final class Media extends CloudGatewayMediaStore.Audio.Media implements AudioColumns {
        }

        public static final class Journal implements JournalColumns {

            public static final String PATH = "audio_journal";

            public static final String CONTENT_TYPE = ASPMediaStore.buildContentType(PATH);

            public static final String ENTRY_CONTENT_TYPE = ASPMediaStore.buildEntryContentType(PATH);

            public static final Uri CONTENT_URI = ASPMediaStore.buildContentUri(PATH);

            @Override
            public Uri getContentUri() {
                return CONTENT_URI;
            }
        }
    }

    public static final class Documents extends CloudGatewayMediaStore.Documents {

        public interface DocumentColumns extends CloudGatewayMediaStore.Documents.DocumentColumns, MediaColumns {

        }

        public static final class Media extends CloudGatewayMediaStore.Documents.Media implements DocumentColumns {
        }

        public static final class Journal implements JournalColumns {

            public static final String PATH = "document_journal";

            public static final String CONTENT_TYPE = ASPMediaStore.buildContentType(PATH);

            public static final String ENTRY_CONTENT_TYPE = ASPMediaStore.buildEntryContentType(PATH);

            public static final Uri CONTENT_URI = ASPMediaStore.buildContentUri(PATH);

            @Override
            public Uri getContentUri() {
                return CONTENT_URI;
            }
        }
    }

    public static class ThumbnailCache {

        public static final String PATH = "thumb_cache";

        public static final Uri CONTENT_URI = ASPMediaStore.buildContentUri(PATH);
    }

    public static class ReadLock {

        public static final String PATH = "read_lock";

        public static final Uri CONTENT_URI = ASPMediaStore.buildContentUri(PATH);
    }

    public static class Files extends CloudGatewayMediaStore.Files {

        public interface FileColumns extends CloudGatewayMediaStore.Files.FileColumns, MediaColumns {

        }

        public static final class Keywords implements KeywordColumns, FileColumns {

            public static final String PATH = "file_keywords";

            public static final String CONTENT_TYPE = ASPMediaStore.buildContentType(PATH);

            public static final String ENTRY_CONTENT_TYPE = ASPMediaStore.buildEntryContentType(PATH);

            public static final Uri CONTENT_URI = ASPMediaStore.buildContentUri(PATH);

            public static Uri getContentUriForDevice(int deviceId) {
                return ASPMediaStore.buildDeviceContentUri(deviceId, PATH);
            }

            public static Uri getEntryUri(int rowId) {
                return ASPMediaStore.buildEntryIdUri(rowId, PATH);
            }

            public static Uri getOrphanCleanUriForDevice(int deviceId) {
                return ASPMediaStore.buildOrphanCleanupUri(deviceId, PATH);
            }
        }
    }

    public static class TemporaryFiles {

        public static final String PATH = "tmpFiles";

        public static final String ENTRY_CONTENT_TYPE = ASPMediaStore.buildEntryContentType(PATH);

        private static final int BASE64_FLAGS = Base64.URL_SAFE | Base64.NO_WRAP;

        public static File getFileForUri(Uri uri) {
            String b64 = uri.getLastPathSegment();
            String filePath = new String(Base64.decode(b64, BASE64_FLAGS));
            return new File(filePath);
        }
    }

    public static class TrashColumns {
        public static final String TRASHED = "trashed";
        public static final String LIST_SHOWN = "restoreAllowed";
        public static final String PROCESSING = CloudGatewayMediaStore.Files.FileColumns.TRASH_PROCESSING;
    }
}

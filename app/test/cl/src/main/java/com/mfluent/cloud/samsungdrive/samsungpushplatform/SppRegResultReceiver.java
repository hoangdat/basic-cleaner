package com.mfluent.cloud.samsungdrive.samsungpushplatform;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.mfluent.cloud.samsungdrive.common.CloudStorageBase;
import com.mfluent.cloud.samsungdrive.common.DriveConstants;
import com.mfluent.log.Log;

import platform.com.mfluent.asp.util.NetworkUtilSLPF;

public class SppRegResultReceiver extends BroadcastReceiver {

    public static final String PUSH_REGISTRATION_CHANGED_ACTION = "com.sec.spp.RegistrationChangedAction";
    public static final String EXTRA_PUSH_STATUS = "com.sec.spp.Status";

    public static final int PUSH_REGISTRATION_SUCCESS = 0;
    public static final int PUSH_REGISTRATION_FAIL = 1;
    public static final int PUSH_DEREGISTRATION_SUCCESS = 2;
    public static final int PUSH_DEREGISTRATION_FAIL = 3;

    public static final String EXTRA_REGID = "RegistrationID";
    public static final String EXTRA_ERROR = "Error";

    String mPreferenceFileName;
    Context mAppContext;

    SppRegResultReceiver(String preferenceFileName, Context appContext) {
        mPreferenceFileName = preferenceFileName;
        mAppContext = appContext;
    }

    private void savePushTokenValue(String pushToken) {
        SharedPreferences preferences = mAppContext.getSharedPreferences(mPreferenceFileName, Context.MODE_PRIVATE);
        String name = CloudStorageBase.PreferenceType.SPP_REG_ID.name();

        SharedPreferences.Editor editor = preferences.edit();
        editor.remove(name);
        editor.putString(name, pushToken);
        editor.apply();

        DriveConstants.apiClient.pushToken = pushToken;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String appId = intent.getStringExtra("appId");
        if (TextUtils.isEmpty(appId) || !appId.equals(SppManager.MY_APPID)) {
            Log.d(this, "This isn't my result. appID : " + appId);
            return;
        }

        int result = intent.getIntExtra(EXTRA_PUSH_STATUS, PUSH_REGISTRATION_FAIL);
        int errorCode;
        switch (result) {
            case PUSH_REGISTRATION_SUCCESS:
                String regId = intent.getStringExtra(EXTRA_REGID);
                Log.d(this, "onReceive() - SPP Registration Succeed. RegID : " + regId);
                savePushTokenValue(regId);
                break;
            case PUSH_REGISTRATION_FAIL:
                errorCode = intent.getIntExtra(EXTRA_ERROR, 0);
                Log.d(this, "Registration Failed. ErrorCode : " + errorCode);

                retryOperation(true, context);
                break;
            case PUSH_DEREGISTRATION_SUCCESS:
                Log.d(this, "Deregistration Succeed.");

                context.unregisterReceiver(this);
                break;

            case PUSH_DEREGISTRATION_FAIL:
                errorCode = intent.getIntExtra(EXTRA_ERROR, 0);
                Log.d(this, "Deregistration Failed. ErrorCode : " + errorCode);

                retryOperation(false, context);
                break;
        }
    }

    private void retryOperation(final boolean bReg, final Context context) {
        if (NetworkUtilSLPF.isNetworkAvailable()) {
            new Thread(
                    new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Thread.sleep(5000);
                                if (bReg) {
                                    SppManager.requestRegistration(context);
                                } else {
                                    SppManager.requestDeregistration(context);
                                }
                            } catch (InterruptedException e) {
                                e.getMessage();
                            }
                        }
                    }
            ).start();
        } else {
            // TODO : retry when network is reconnected
            Log.e(this, "retryOperation - Network is not connected");
        }
    }
}
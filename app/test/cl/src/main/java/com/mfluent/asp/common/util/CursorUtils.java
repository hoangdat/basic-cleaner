/**
 *
 */

package com.mfluent.asp.common.util;

import android.database.Cursor;

import com.mfluent.log.Log;

/**
 * @author Ilan Klinghofer
 */
public class CursorUtils {

    private static final String TAG = CursorUtils.class.getSimpleName();

    //private static void ASPLOG(final String text) { Log.d(TAG, text); }

    public static boolean getBoolean(Cursor cursor, String colName) {
        int colIndex = cursor.getColumnIndex(colName);
        if (colIndex >= 0) {
            return cursor.getInt(colIndex) == 0 ? false : true;
        } else {
            Log.d(TAG, "getBoolean() - invalid column: " + colName);
            return false;
        }
    }

    /**
     * @return - value for given cursor/column name, or 0 if colName not found
     */
    public static int getInt(Cursor cursor, String colName) {
        int colIndex = cursor.getColumnIndex(colName);
        if (colIndex >= 0) {
            return cursor.getInt(colIndex);
        } else {
            Log.d(TAG, "getInt() - invalid column : " + colName);
            return 0;
        }
    }

    /**
     * @return - value for given cursor/column name, or null if colName not found
     */
    public static String getString(Cursor cursor, String colName) {
        int colIndex = cursor.getColumnIndex(colName);
        if (colIndex >= 0) {
            return cursor.getString(colIndex);
        } else {
            Log.d(TAG, "getString() - invalid column : " + colName);
            return null;
        }
    }

    public static long getLong(Cursor cursor, String colName) {
        int colIndex = cursor.getColumnIndex(colName);
        if (colIndex >= 0) {
            return cursor.getLong(colIndex);
        } else {
            Log.d(TAG, "getLong() - invalid column : " + colName);
            return 0;
        }
    }
}


package com.mfluent.cloud.googledrive.exception;

import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;

import com.mfluent.asp.common.datamodel.CloudDevice;
import com.samsung.android.sdk.slinkcloud.CloudGatewayConstants;

import android.content.Context;

import platform.com.mfluent.asp.framework.AuthDelegate.GoogleDriveAuthActivity;

/**
 * this exception is thrown when the authorization state of the cloud connector cannot be determined.
 * </br></br>
 * if the authorization was thought to be known and good but is discovered to be bad, then CloudUnauthorizedException should be thrown instead.
 */
public class CloudAuthorizationException extends Exception {
    private static final String MSG_IN_BUNDLE = "message";
    private static final String INTENT_IN_BUNDEL = "intent";
    //	private static final String BUNDLE_IN_INTENT_EXTRA = "bundle";
    private static final long serialVersionUID = 1249404576282496863L;

    public static void showAuthActivity(Context context, int deviceId, CloudDevice device, Intent i) {
        device.setIsInUserRecovering(true);

        Bundle bundle = new Bundle();
        bundle.putString(MSG_IN_BUNDLE, "UserRecoverableAuthIOException");
        bundle.putInt(CloudDevice.DEVICE_ID_EXTRA_KEY, deviceId);
        bundle.putParcelable(INTENT_IN_BUNDEL, i);

        Intent intent = new Intent();
        intent.setComponent(new ComponentName(CloudGatewayConstants.TARGET_APK_PACKAGE, GoogleDriveAuthActivity.class.getName()));

        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        intent.putExtra("bundle", bundle);
        context.startActivity(intent);
    }
}

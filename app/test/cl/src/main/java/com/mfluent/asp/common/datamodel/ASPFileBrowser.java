/**
 *
 */

package com.mfluent.asp.common.datamodel;

import android.database.DataSetObserver;

import java.io.IOException;

/*********************************************************************************************************************
 * ***** WARNING: IF YOU MODIFY THIS INTERFACE YOU NEED TO UPDATE THE CLOUD API VERSION IN CloudStorageSync.java ******
 *********************************************************************************************************************/

/**
 * @author Ilan Klinghofer
 */
public interface ASPFileBrowser<T extends ASPFile> {

    /**
     * Initializes this file browser
     * This call may block on the network or other i/o operations and should be executed in an AsyncTask.
     *
     * @param directory    the directory to browse files in. If null, defaults to an implementation specific directory.
     * @param fileSortType the type of sort to apply
     * @param forceReload  if true, ignore the cached version
     * @throws InterruptedException
     * @throws IOException
     */
    boolean init(T directory, ASPFileSortType fileSortType, boolean forceReload) throws InterruptedException, IOException;

    /**
     * Destroys the file browser. Implementers should perform any necessary cleanup.
     * The call may not block and is safe to call from the UI thread.
     */
    void destroy();

    /**
     * Register an observer that is called when changes happen to the contents
     * of this FileBrowser's data set
     *
     * @param observer the object that gets notified when the FileBrowser's data set changes.
     * @see #unregisterDataSetObserver(DataSetObserver)
     */
    void registerDataSetObserver(DataSetObserver observer);

    /**
     * Unregister an observer that has previously been registered with this
     * FileBrowser via {@link #registerContentObserver}.
     *
     * @param observer the object to unregister.
     * @see #registerDataSetObserver(DataSetObserver)
     */
    void unregisterDataSetObserver(DataSetObserver observer);

    /**
     * Retrieves the current directory that this browser is initialized for.
     */
    T getCurrentDirectory();

    /**
     * Returns the absolute path of the current directory.
     */
    String getCurrentDirectoryAbsolutePath();

    /**
     * Retrieves the parent directory. Returns null if the current directory is the root of the file system.
     */
    T getParentDirectory();

    /**
     * Retrieves the total number of files in the current directory
     */
    int getCount();

    /**
     * Retrieves the file information for the file at the given position.
     * This call may block on the network or other i/o operations and should be executed in an AsyncTask.
     *
     * @param index 0 based index of the file
     * @return the file information
     * @throws InterruptedException
     * @throws IOException
     */
    T getFile(int index) throws InterruptedException, IOException;

    /**
     * Retrieves the file information for the file at the given position.
     * This call may NOT block. If the file is not immediately available, returns null
     *
     * @param index 0 based index of the file
     * @return the file information
     * @throws InterruptedException
     */
    T getFileNonBlocking(int index);
}

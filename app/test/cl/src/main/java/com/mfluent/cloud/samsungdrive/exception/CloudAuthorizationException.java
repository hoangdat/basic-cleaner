
package com.mfluent.cloud.samsungdrive.exception;

import android.content.ComponentName;
import android.content.Intent;

import com.mfluent.asp.common.datamodel.CloudDevice;
import com.mfluent.log.Log;
import com.samsung.android.sdk.slinkcloud.CloudGatewayConstants;

import android.content.Context;

import platform.com.mfluent.asp.framework.AuthDelegate.SamsungDriveAuthActivity;

/**
 * this exception is thrown when the authorization state of the cloud connector cannot be determined.
 * </br></br>
 * if the authorization was thought to be known and good but is discovered to be bad, then CloudUnauthorizedException should be thrown instead.
 */
public class CloudAuthorizationException extends Exception {
    private static final String TAG = "CloudAuthorizationException";

    public static void showAuthActivity(Context context, CloudDevice device) {
        Log.d(TAG, "showAuthActivity() for SamsungDrive");
        device.setIsInUserRecovering(true);

        Intent intent = new Intent();
        intent.setComponent(new ComponentName(CloudGatewayConstants.TARGET_APK_PACKAGE, SamsungDriveAuthActivity.class.getName()));
        intent.putExtra(CloudDevice.DEVICE_ID_EXTRA_KEY, device.getId());

        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }
}


package com.mfluent.cloud.googledrive.exception;

import android.content.Intent;

/**
 * Thrown when the API can only be used when linked but it's not.
 *
 * @author MFluent
 * @version 1.5
 */
public class CloudUnauthorizedException extends CloudException {

    private static final long serialVersionUID = -3088429379526691394L;
    private Intent intent;

    /**
     * Instantiates a new cloud unauthorized exception.
     *
     * @param message the message
     */
    public CloudUnauthorizedException(String message) {
        super(message);
    }

    public CloudUnauthorizedException(String message, Intent i) {
        super(message);
        intent = i;
    }

    public Intent getIntent() {
        return intent;
    }
}

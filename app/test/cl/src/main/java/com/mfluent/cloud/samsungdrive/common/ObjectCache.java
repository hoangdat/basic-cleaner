/*******************************************************************************
 * mFLuent LLC. CONFIDENTIAL PROPRIETARY
 * Copyright (c) 2010, mFLuent LLC. All rights reserved.
 * Use, distribution, or disclosure of this file or its contents in full or in
 * part is prohibited without prior written authorization from mFluent LLC.
 ******************************************************************************/

package com.mfluent.cloud.samsungdrive.common;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

public final class ObjectCache<K, V> {

	private static final int DEFAULT_CAPACITY = 16;
	private static final int MIN_PUTS_BETWEEN_PURGE = 10;

	//the number of put() calls between purging empty references
	private int mPutsBetweenPurge;

	private int mNumPuts = 0;

	private final HashMap<K, WeakReference<V>> mHashMap;

	public ObjectCache(int initialCapacity) {

		//this should ensure that a purge happens before the first rehash and at reasonable intervals afterwards
		int rate = (initialCapacity * 3) / 4;
		mPutsBetweenPurge = (rate > MIN_PUTS_BETWEEN_PURGE ? rate - 1 : MIN_PUTS_BETWEEN_PURGE);

		mHashMap = new HashMap<K, WeakReference<V>>(initialCapacity, 0.75f);
	}

	public ObjectCache() {
		this(DEFAULT_CAPACITY);
	}

	/**
	 * Clears empty references from the table.
	 */
	private void purge() {
		Iterator<Entry<K, WeakReference<V>>> set = mHashMap.entrySet().iterator();
		while (set.hasNext()) {
			Entry<K, WeakReference<V>> entry = set.next();
			if (entry.getValue().get() == null) {
				set.remove();
			}
		}
		mNumPuts = 0;
	}

	public V get(K key) {

		V value = null;

		WeakReference<V> ref = mHashMap.get(key);
		if (ref == null) {
			return null;
		}

		value = ref.get();
		if (value == null) {
			remove(key);
		}

		return value;
	}

	public V put(K key, V value) {
		if (key == null) {
			throw new IllegalArgumentException("Null keys are not allowed.");
		} else if (value == null) {
			throw new IllegalArgumentException("Null values are not allowed.");
		}

		V retVal = null;

		WeakReference<V> ref = mHashMap.put(key, new WeakReference<V>(value));

		// clear out the empty values every so often
		churn();

		if (ref != null) {
			retVal = ref.get();
		}

		return retVal;

	}

	public V remove(K key) {
		WeakReference<V> ref = mHashMap.remove(key);
		V removed = null;
		if (ref != null) {
			removed = ref.get();
		}

		return removed;
	}

	/**
	 * Called to register object loading, to try to figure out when to purge.
	 */
	public void churn() {
		if (mPutsBetweenPurge > 0) {

			mNumPuts++;
			if (mNumPuts >= mPutsBetweenPurge) {
				purge();
			}
		}
	}

	public void clear() {
		mHashMap.clear();
		mNumPuts = 0;
	}
}

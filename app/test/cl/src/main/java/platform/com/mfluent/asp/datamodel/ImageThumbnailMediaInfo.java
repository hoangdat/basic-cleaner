
package platform.com.mfluent.asp.datamodel;

import com.mfluent.asp.common.datamodel.ASPMediaStore.Images.ImageColumns;
import com.mfluent.asp.common.datasource.AspMediaId;
import com.samsung.android.sdk.slinkcloud.CloudGatewayMediaStore;

public class ImageThumbnailMediaInfo extends ThumbnailMediaInfo {

	private static final String[] PROJECTION = {
			ImageColumns._ID,
			ImageColumns.DATA,
			ImageColumns.MEDIA_TYPE,
			ImageColumns.SOURCE_MEDIA_ID,
			ImageColumns.FULL_URI,
			ImageColumns.THUMBNAIL_URI,
			ImageColumns.DISPLAY_NAME,
			ImageColumns.ORIENTATION,
			ImageColumns.DEVICE_ID,
			ImageColumns.WIDTH,
			ImageColumns.HEIGHT};

	private static final class InstanceHolder {

		private static ImageThumbnailMediaInfo sInstance = new ImageThumbnailMediaInfo();
	}

	public static ImageThumbnailMediaInfo getInstance() {
		return InstanceHolder.sInstance;
	}

	private ImageThumbnailMediaInfo() {

	}

	@Override
	protected String[] getQueryTableProjection() {
		return PROJECTION;
	}

	@Override
	public String getQueryTableName(MediaInfoContext context) {
		return FilesMediaInfo.TABLE_NAME;//ImageMediaInfo.VIEW_NAME;
	}

	@Override
	public String getInsertUpdateDeleteTableName() {
		return null;
	}

	@Override
	public String getEntryContentType() {
		return CloudGatewayMediaStore.Images.Thumbnails.ENTRY_CONTENT_TYPE;
	}

	@Override
	public String getContentType() {
		return CloudGatewayMediaStore.Images.Thumbnails.CONTENT_TYPE;
	}

	@Override
	protected int getMediaType() {
		return AspMediaId.MEDIA_TYPE_IMAGE;
	}
}



package platform.com.mfluent.asp.ui;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;
import android.view.Window;

import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.services.drive.DriveScopes;
import com.mfluent.asp.cloudstorage.api.sync.CloudStorageSync;
import com.mfluent.asp.common.datamodel.CloudDevice;
import com.mfluent.asp.common.util.IntentHelper;
import com.mfluent.cloud.samsungdrive.common.DriveConstants;
import com.mfluent.cloud.samsungdrive.common.SamsungAccountImp;
import com.mfluent.log.Log;
import com.samsung.android.sdk.slinkcloud.CloudGatewayConstants;
import com.samsung.android.sdk.slinkcloud.CloudGatewayDeviceTransportType;
import com.samsung.android.slinkcloud.R;

import org.apache.commons.lang3.StringUtils;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import platform.com.mfluent.asp.datamodel.DataModelSLPF;
import platform.com.mfluent.asp.datamodel.DeviceSLPF;
import platform.com.mfluent.asp.framework.IASPApplication2;
import platform.com.mfluent.asp.framework.ServiceLocatorSLPF;
import platform.com.mfluent.asp.framework.StorageProviderDatabaseHelper;
import platform.com.mfluent.asp.framework.StorageProviderInfo;
import platform.com.mfluent.asp.sync.CloudStorageSyncManager;
import platform.com.mfluent.asp.util.DeviceUtilSLPF;
import platform.com.mfluent.asp.util.NetworkUtilSLPF;
import uicommon.com.mfluent.asp.util.BundleReadingUtils;

public class StorageSignInSettingDialogFragment extends DialogFragment {

    interface StorageSignInDialogFragListener {

        void storageSignInDialogDismissed(boolean deviceRemoved);

        void storageDeviceWasAddedInSignInDialogFrag();

        void startOAuthSignInActivity(Intent intent);

        void signInFailed();
    }

    private StorageSignInDialogFragListener mDelegate = null;

    //private static final int AUTHORIZATION_CODE = 1001;
    private static final int ACCOUNT_CODE_GOOGLE = 1002;
    private static final int ACCOUNT_CODE_SAMSUNG = 1003;
    protected static final int LOGIN_TIMEOUT = 30 * 1000;

    private Account mAccount;

    private static final String ARG_STORAGE_TYPE = "storageType";
    private static final String ARG_IS_UI_APP_THEME = "isUiAppTheme";
    private static final String ARG_SIGN_IN_ACCOUNT = "signInAccount";
    private static final String ARG_IS_AUTO_UPLOAD = "isAutoUpload";
    private static final String SAVED_INSTANCE_KEY_NEED_TO_START_REQUESTED_SIGN_IN = "com.mfluent.asp.ui.StorageSignInSettingDialogFragment.SAVED_INSTANCE_KEY_NEED_TO_START_REQUESTED_SIGN_IN";

    private static final String TAG = "StorageSignInSettingDialogFragment";
    private View mProgress = null;

    private DeviceSLPF mSelectedStorageDevice = null;

    private static final String ADD_SAMSUNG_ACCOUNT = "com.osp.app.signin.action.ADD_SAMSUNG_ACCOUNT";
    private static final String SA_CLIENT_ID = "gc4z299bi4";
    private static final String SA_CLIENT_SECRET = "A9DFBE5A1BF6BE4955486E9B36074C7C";
    private static final String SA_CLIENT_OSP_VERSION = "OSP_02";
    private static final String SA_CLIENT_MODE = "ADD_ACCOUNT";

    private boolean mIsCloudLaunchOauthBroadcastReceived = false;
    private boolean mIsLoading = false;
    private boolean mLoadingFirst = true;
    private boolean mIsNetworkDialog = false;
    private boolean mNeedToStartRequestedSignIn;
    private boolean mIsAutoUpload = false;
    private boolean mWouldNeedToCancel = false;

    private Bundle mSavedInstanceState = null;

    private LocalBroadcastManager mLocalBroadcastManager;
    private AccountManager mAccountManager;

    public static StorageSignInSettingDialogFragment newInstance(String storageType, boolean isUiAppTheme, String signInAccount) {
        Bundle args = new Bundle();
        args.putString(ARG_STORAGE_TYPE, storageType);
        args.putBoolean(ARG_IS_UI_APP_THEME, isUiAppTheme);
        if (signInAccount != null) {
            args.putString(ARG_SIGN_IN_ACCOUNT, signInAccount);
        }

        StorageSignInSettingDialogFragment fragment = new StorageSignInSettingDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static StorageSignInSettingDialogFragment newInstance(boolean isUiAppTheme) {
        Bundle args = new Bundle();
        args.putBoolean(ARG_IS_UI_APP_THEME, isUiAppTheme);

        StorageSignInSettingDialogFragment fragment = new StorageSignInSettingDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static StorageSignInSettingDialogFragment newInstanceForAdditionalClouds(boolean isUiAppTheme) {
        Bundle args = new Bundle();
        args.putBoolean(ARG_IS_UI_APP_THEME, isUiAppTheme);
        args.putBoolean("additional_cloud_selection", true);

        StorageSignInSettingDialogFragment fragment = new StorageSignInSettingDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mLocalBroadcastManager = LocalBroadcastManager.getInstance(getActivity());
        mAccountManager = AccountManager.get(getActivity());
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        processSavedInstanceState(savedInstanceState);

        mIsAutoUpload = isAutoUpload();

        View storageListView = View.inflate(getActivity(), R.layout.dialog_frag_list, null);
        mProgress = storageListView.findViewById(R.id.loading_layout);

        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        if (mIsAutoUpload) {
            dialog.setTitle(R.string.add_service_auto_upload);
        }
        dialog.setView(storageListView);

        if (savedInstanceState != null) { // We are entering this fragment as a result of an orientation/config change
            restoreStateOnOrientationChangeForLoading(savedInstanceState);
        }

        Dialog storageDialog = dialog.create();
        storageDialog.setCanceledOnTouchOutside(false);

        storageDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        storageDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        return storageDialog;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof StorageSignInDialogFragListener) { // If called from an activity that implements StorageSignInDialogFragListener
            mDelegate = (StorageSignInDialogFragListener) activity;
        } else {
            throw new IllegalStateException("StorageSignInSettingDialogFragment must be attached to activity that implements StorageSignInDialogFragListener.");
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        mSavedInstanceState = outState;

        // Orientation change in progress
        if (mSelectedStorageDevice == null) {
            outState.putInt("selected_device_key", -1);
        } else {
            outState.putInt("selected_device_key", mSelectedStorageDevice.getId());
        }

        outState.putBoolean("cloud_launch_broadcast_recd", mIsCloudLaunchOauthBroadcastReceived);

        outState.putBoolean("loading_status", mIsLoading);
        outState.putBoolean("network_dialog_status", mIsNetworkDialog);
        outState.putBoolean(SAVED_INSTANCE_KEY_NEED_TO_START_REQUESTED_SIGN_IN, mNeedToStartRequestedSignIn);
        outState.putBoolean("auto_upload_status", mIsAutoUpload);
    }

    private void processSavedInstanceState(Bundle savedInstanceState) {

        int deviceId = BundleReadingUtils.getInt("selected_device_key", 0, savedInstanceState);

        if (deviceId > 0) {
            DataModelSLPF dataModel = DataModelSLPF.getInstance();
            mSelectedStorageDevice = dataModel.getDeviceById(deviceId);
        } else {
            mSelectedStorageDevice = null;
        }

        mIsCloudLaunchOauthBroadcastReceived = BundleReadingUtils.getBoolean("cloud_launch_broadcast_recd", false, savedInstanceState);
        mNeedToStartRequestedSignIn = BundleReadingUtils.getBoolean(
                SAVED_INSTANCE_KEY_NEED_TO_START_REQUESTED_SIGN_IN,
                getRequestedStorage() != null,
                savedInstanceState);

    }

    private void restoreStateOnOrientationChangeForLoading(Bundle savedInstanceState) {

        boolean isLoading = savedInstanceState.getBoolean("loading_status");
        boolean isNetworkDialog = savedInstanceState.getBoolean("network_dialog_status");
        boolean isAutoUpload = savedInstanceState.getBoolean("auto_upload_status");

        mIsLoading = isLoading;
        mIsNetworkDialog = isNetworkDialog;
        mIsAutoUpload = isAutoUpload;
    }

    @Override
    public void onStart() {
        super.onStart();

        if (mSavedInstanceState != null) {
            //Not call OnDestroy & onCreateDialog
            mLoadingFirst = true;
            restoreStateOnOrientationChangeForLoading(mSavedInstanceState);
        }

        hideLoading();

        if (mNeedToStartRequestedSignIn) {
            StorageProviderInfo requestedStorage = getRequestedStorage();
            if (requestedStorage != null) {
                getDialog().setTitle("");
                ((AlertDialog) getDialog()).getButton(AlertDialog.BUTTON_POSITIVE).setVisibility(View.GONE);
                startSignIn(requestedStorage);
                mNeedToStartRequestedSignIn = false;
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.v(this, "onResume(), mWouldNeedToCancel = " + mWouldNeedToCancel);

        if (mWouldNeedToCancel) {
            try {
                AccountManager accountManager = AccountManager.get(getActivity());
                Account[] accounts = accountManager.getAccountsByType(SignInOutUtils.AccountType_SamsungAccount); // Samsung Account

                Log.v(this, "onResume(), accounts.length = " + accounts.length);

                if (accounts.length == 0) {
                    mWouldNeedToCancel = false;
                    onCancel(null);
                    getActivity().finish();
                    return;
                }
            } catch (SecurityException e) {
                Log.e(this, "onResume() - SecurityException : " + e.getMessage());
            }
        }


        CloudStorageSyncManager.registerForOAuthLaunch(getActivity(), cloudLaunchOauthBrowserReceiver);

        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(
                signInAuthSuccess,
                new IntentFilter(CloudStorageSync.CLOUD_AUTHENTICATION_SUCCESS));

        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(
                cloudAuthUnknownReceiver,
                new IntentFilter(CloudStorageSync.CLOUD_AUTHENTICATION_UNKNOWN));

        if (mIsLoading) {
            showLoading(LOGIN_TIMEOUT);
        } else if (mIsNetworkDialog) {
            DataModelSLPF dataModel = DataModelSLPF.getInstance();
            DeviceSLPF localDevice = dataModel.getLocalDevice();
            showDialog(R.string.cloud_access_settings_title, getResources().getString(R.string.failed_to_connect));

            if (!localDevice.isPresence()) {
                Log.e(this, " Show Progress Dialog Timeout!!!");
            } else {
                Log.e(this, "Unexpected status!! Show Progress Dialog Timeout!!!");
            }
        }
    }

    @Override
    public void onPause() {
        Log.v(this, "onPause()");

        super.onPause();

        if (mDelayedRunnable != null) {
            mHandler.removeCallbacks(mDelayedRunnable);
        }
    }

    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();

        Log.v(this, "onDestroy()");

        CloudStorageSyncManager.unregisterForOAuthLaunch(getActivity(), cloudLaunchOauthBrowserReceiver);
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(signInAuthSuccess);
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(cloudAuthUnknownReceiver);
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.v(this, "onStop()");
    }

    private StorageProviderInfo getRequestedStorage() {
        final Bundle args = getArguments();
        if (args == null) {
            return null;
        }
        StorageProviderDatabaseHelper providerDB = StorageProviderDatabaseHelper.getInstance(getActivity());
        final String requestedStorageName = args.getString(ARG_STORAGE_TYPE);
        return (requestedStorageName == null) ? null : providerDB.get(requestedStorageName);
    }

    private boolean isAutoUpload() {
        final Bundle args = getArguments();
        if (args == null) {
            return false;
        }
        boolean isAutoUpload = args.getBoolean(ARG_IS_AUTO_UPLOAD, false);
        return isAutoUpload;
    }

    private DeviceSLPF storageDeviceExistsInDataModel(String storageId) {
        DataModelSLPF dataModel = DataModelSLPF.getInstance();
        ArrayList<DeviceSLPF> storages = new ArrayList<DeviceSLPF>();
        List<DeviceSLPF> storageDevicesInDataModel = dataModel.getDevicesByType(CloudGatewayDeviceTransportType.WEB_STORAGE);
        storages.clear();
        for (DeviceSLPF device : storageDevicesInDataModel) {
            storages.add(device);
        }
        for (DeviceSLPF storageDevice : storages) {
            if (storageDevice.getWebStorageType().equals(storageId)) {
                return storageDevice;
            }
        }
        return null;
    }

    private void signIn(int deviceId) {
        Log.v(this, "signIn() : called");
        Intent intent = new Intent(CloudStorageSync.CLOUD_SIGNIN);
        intent.putExtra(CloudDevice.DEVICE_ID_EXTRA_KEY, deviceId);
        mLocalBroadcastManager.sendBroadcast(intent);
    }

    private void broadcastDeviceStateChange(int deviceId) {
        Intent intent = new Intent(DeviceSLPF.BROADCAST_DEVICE_STATE_CHANGE);
        intent.putExtra(CloudDevice.DEVICE_ID_EXTRA_KEY, deviceId);
        mLocalBroadcastManager.sendBroadcast(intent);
    }

    private final BroadcastReceiver cloudLaunchOauthBrowserReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(this, "cloudLaunchOauthBrowserReceiver, RX broadcast: " + intent.getAction());

            int broadcastedStorageId = intent.getExtras().getInt(CloudDevice.DEVICE_ID_EXTRA_KEY);

            if (CloudStorageSync.CLOUD_LAUNCH_OAUTH1_BROWSER.equals(intent.getAction())
                    && mSelectedStorageDevice != null
                    && broadcastedStorageId == mSelectedStorageDevice.getId()) {

                hideLoading();

                mIsCloudLaunchOauthBroadcastReceived = true;

                if (intent.getExtras().getString(CloudStorageSync.OAUTH1_URI) == null) {
                    if (mDelegate != null) {
                        mDelegate.storageDeviceWasAddedInSignInDialogFrag();
                    }
                    dismissAllowingStateLoss(); // Null uri indicates we were already signed in, so we can dismiss this dialogfragment
                } else {
                    if (mDelegate != null) {
                        mDelegate.startOAuthSignInActivity(OAuthWebView.createOAuthWebViewIntent(context, intent));
                    }
                    dismissAllowingStateLoss();
                }
            }
        }
    };

    @Override
    public void onCancel(DialogInterface dialog) {
        if (dialog != null) {
            super.onCancel(dialog);
        }

        Log.v(this, "onCancel()");

        SharedPreferences pref = (getActivity()).getSharedPreferences(
                IASPApplication2.Name_bShouldCloudSelectionPopupDisplayedAutomatically,
                Activity.MODE_PRIVATE);
        Editor editor = pref.edit();
        editor.putBoolean(IASPApplication2.Key_bShouldCloudSelectionPopupDisplayedAutomatically, false);
        editor.apply();

        // If user hits the back key before sign in, then sign out the device if it had been added to the device list.
        boolean deviceGotAddedBeforeCancelButtonClicked = mSelectedStorageDevice != null
                && storageDeviceExistsInDataModel(mSelectedStorageDevice.getWebStorageType()) != null;

        if (mDelegate != null) {
            mDelegate.storageSignInDialogDismissed(deviceGotAddedBeforeCancelButtonClicked);
        }

        if (mSelectedStorageDevice != null) {
            mSelectedStorageDevice.setPendingSetup(false);
        }

        if (deviceGotAddedBeforeCancelButtonClicked) { // if true, then need to remove the device from the data model
            final Context context = getActivity().getApplicationContext();
            AsyncTask.execute(new Runnable() {

                @Override
                public void run() {
                    StorageSignInOutHelper signOutHelper = new StorageSignInOutHelper(context);
                    signOutHelper.signOutOfStorageService(mSelectedStorageDevice);
                }
            });
        }
    }

    private static class HideLoadingRunnable implements Runnable {

        private final WeakReference<StorageSignInSettingDialogFragment> mStorageSignInSettingDialogFragment;

        public HideLoadingRunnable(StorageSignInSettingDialogFragment fragment) {
            mStorageSignInSettingDialogFragment = new WeakReference<StorageSignInSettingDialogFragment>(fragment);
        }

        @Override
        public void run() {
            StorageSignInSettingDialogFragment fragment = mStorageSignInSettingDialogFragment.get();
            Log.e(TAG, "HideLoadingRunnable - run(), fragment = " + fragment);
            if (fragment != null && fragment.getActivity() != null) {
                fragment.hideLoading();

                DataModelSLPF dataModel = DataModelSLPF.getInstance();
                DeviceSLPF localDevice = dataModel.getLocalDevice();

                fragment.showDialog(R.string.cloud_access_settings_title, fragment.getResources().getString(R.string.failed_to_connect));
                if (localDevice.isPresence()) {
                    Log.e(TAG, "HideLoadingRunnable - run(), Unexpected status!! Show Progress Dialog Timeout!!!");
                }
            }
        }
    }

    private final Handler mHandler = new Handler();
    private Runnable mDelayedRunnable;

    public void showLoading(int milli) {
        Log.i(this, "showLoading() called, milli = " + milli);

        if (mProgress == null) {
            return;
        }

        mProgress.setVisibility(View.VISIBLE);
        mIsLoading = true;

        Log.e(this, " this.signInButtons.size() ");

        if (mDelayedRunnable == null) {
            mDelayedRunnable = new HideLoadingRunnable(this);
        }

        mHandler.postDelayed(mDelayedRunnable, milli);
    }

    public void hideLoading() {
        Log.i(this, "hideLoading() called");

        if (mLoadingFirst) {
            mLoadingFirst = false;
            if (mProgress == null) {
                return;
            }
            mProgress.setVisibility(View.GONE);
        } else {
            mLoadingFirst = false;
            mIsLoading = false;
            if (mProgress == null) {
                return;
            }
            mProgress.setVisibility(View.GONE);
        }
    }

    @Override
    public void dismiss() {
        super.dismiss();
        // Make sure we don't send anything else to the delegate since we consider ourselves done
        mDelegate = null;
    }

    @Override
    public void dismissAllowingStateLoss() {
        super.dismissAllowingStateLoss();

        // Make sure we don't send anything else to the delegate since we consider ourselves done
        mDelegate = null;
    }

    private void showDialog(int titleId, String message) {
        if (mDelayedRunnable != null) {
            mHandler.removeCallbacks(mDelayedRunnable);
        }

        if (mSelectedStorageDevice != null) {
            mSelectedStorageDevice.delete();
        }

        mIsNetworkDialog = true;

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(titleId);
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setNegativeButton(R.string.common_popup_confirm, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                mIsNetworkDialog = false;
                dialog.cancel();
                handleSignInFailed();
            }
        });

        this.getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {
                // note: builder.create() needs to be called from UI thread
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });

    }

    private void handleSignInFailed() {
        if (mDelegate != null) {
            mDelegate.signInFailed();
        }
        dismiss();
    }

    private void startSignIn(StorageProviderInfo selectedStorageItem) {
        Log.d(this, "startSignIn() begin");
        boolean bNeedLoading = true;
        if (getActivity() == null) {
            return;
        }

        if (!NetworkUtilSLPF.isNetworkAvailable()) {
            //ErrorDialogBuilder.showSimpleErrorDialog(getFragmentManager(), R.string.common_no_network);
            Log.i(this, "network is not available now (network disconnection detected from NetworkUtil)");
            return;
        }

        if (selectedStorageItem != null && StringUtils.isNotEmpty(selectedStorageItem.getMain())) {
            if (selectedStorageItem.isOAuth()) {
                Log.d(this, "oAuth() is true");

                final DataModelSLPF dataModel = DataModelSLPF.getInstance();
                mSelectedStorageDevice = storageDeviceExistsInDataModel(selectedStorageItem.getSpName());

                if (mSelectedStorageDevice != null && mSelectedStorageDevice.getCloudNeedFirstUpdate()) {
                    Log.i(this, "mSelectedStorageDevice=" + mSelectedStorageDevice + " is first updated so can't login now...");
                    getActivity().setResult(Activity.RESULT_CANCELED);
                    dismiss();
                    getActivity().onBackPressed();
                    return;
                }

                if (mSelectedStorageDevice != null) {
                    DeviceSLPF device = DataModelSLPF.getInstance().getDeviceById(mSelectedStorageDevice.getId());
                    Bundle args = getArguments();
                    String userId = (device != null) ? device.getWebStorageUserId() : null;
                    String signInAccount = (args != null) ? args.getString(ARG_SIGN_IN_ACCOUNT) : null;
                    if (userId != null && !CloudGatewayConstants.ADD_ACCOUNT.equals(signInAccount)
                            && !userId.equals(signInAccount)) {
                        StorageSignInOutHelper signOutHelper = new StorageSignInOutHelper(getActivity().getApplicationContext());
                        signOutHelper.signOutOfStorageService(mSelectedStorageDevice);
                    }
                }

                Log.d(this, "startSignIn() - existAccountFromMyFiles() : " + existAccountFromMyFiles());
                if (!existAccountFromMyFiles()
                        && (mSelectedStorageDevice != null && mSelectedStorageDevice.isWebStorageSignedIn())) {
                    mSelectedStorageDevice.setPendingSetup(true);
                    dataModel.updateDevice(mSelectedStorageDevice);
                    broadcastDeviceStateChange(mSelectedStorageDevice.getId());
                } else {
                    if (mSelectedStorageDevice == null) {
                        mSelectedStorageDevice = new DeviceSLPF(getActivity());
                        mSelectedStorageDevice.setAliasName(selectedStorageItem.getName());
                    }
                    mSelectedStorageDevice.setWebStorageEnableSync(true);
                    mSelectedStorageDevice.setWebStorageType(selectedStorageItem.getSpName());
                    mSelectedStorageDevice.setPendingSetup(true);

                    //in case of GoogleDrive, do the authorization by account manager
                    Log.i(this, "::startSignIn(), :" + " storage type is " + selectedStorageItem.getSpName());
                    switch (selectedStorageItem.getSpName()) {
                        case DeviceUtilSLPF.GOOGLEDRIVE_WEBSTORAGE_NAME:
                            Log.d(this, "-- if google --");
                            doAuthForGoogle();
                            bNeedLoading = false;
                            break;
                        case DeviceUtilSLPF.SAMSUNGDRIVE_WEBSTORAGE_NAME:
                            Log.d(this, "-- if Samsung --");
                            getSamsungAccount();
                            bNeedLoading = false;
                            break;
                        default:
                            Log.d(this, "-- else --");
                            new AsyncTask<DeviceSLPF, Void, DeviceSLPF>() {

                                @Override
                                protected DeviceSLPF doInBackground(DeviceSLPF... params) {
                                    Log.d(this, "-- doInBackground --");
                                    DeviceSLPF d = params[0];
                                    Log.i(this, "::doInBackground:" + " adding: " + d);
                                    d.commitChanges();
                                    return d;
                                }

                                @Override
                                protected void onPostExecute(DeviceSLPF result) {
                                    Log.i(this, "onPostExecute()::StartSignIn, device id" + result.getId());
                                    signIn(result.getId());
                                }
                            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, mSelectedStorageDevice);
                    }
                }

                if (bNeedLoading)
                    showLoading(LOGIN_TIMEOUT);
            } else {
                Log.d(this, "oAuth() is false");
                dismiss();
            }
        } else {
            Log.i(this, "selectedStorageItem should not be null!! selectedStorageItem=" + selectedStorageItem);
        }
    }

    private boolean existAccountFromMyFiles() {
        Bundle args = getArguments();
        return args != null && args.getString(ARG_SIGN_IN_ACCOUNT) != null;
    }

    // Broadcast receiver to handle the case when user selects to log into a storage device and the
    // cloud storage connector does not respond
    private boolean isCloudAuthUnknownDialogShowing = false;

    private final BroadcastReceiver cloudAuthUnknownReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(this, "cloudAuthUnknownReceiver, RX broadcast = " + intent.getAction() + ", mIsCloudLaunchOauthBroadcastReceived = " + mIsCloudLaunchOauthBroadcastReceived);
            if (mIsCloudLaunchOauthBroadcastReceived) {
                return;
            }

            if (isCloudAuthUnknownDialogShowing) {
                return;
            }

            int broadcastedStorageId = intent.getExtras().getInt(CloudDevice.DEVICE_ID_EXTRA_KEY);

            if (mSelectedStorageDevice != null
                    && broadcastedStorageId == mSelectedStorageDevice.getId()) {

                mSelectedStorageDevice.setPendingSetup(false);

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle(R.string.home_no_network_connection);//builder.setTitle(R.string.common_popup_notification);
                builder.setMessage(getResources().getString(R.string.failed_to_connect));//builder.setMessage(getResources().getString(R.string.cloud_auth_unknown));
                builder.setCancelable(false);
                builder.setNegativeButton(R.string.common_popup_confirm, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mSelectedStorageDevice.setPendingSetup(false);
                        mSelectedStorageDevice.delete();
                        isCloudAuthUnknownDialogShowing = false;
                        dialog.cancel();
                        handleSignInFailed();
                    }
                });

                isCloudAuthUnknownDialogShowing = true;
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        }
    };

    // Broadcast receiver for situation where storage provider is already signed in when the user is attempting to re-sign-in
    // from this StorageSignIn dialog fragment
    private final BroadcastReceiver signInAuthSuccess = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            Log.i(this, "onReceive:: " + IntentHelper.intentToString(intent));

            if (mSelectedStorageDevice == null) {
                Log.i(this, "oneceive:: mSelectedStorageDevice is null");
                return;
            }

            int broadcastedStorageId = intent.getIntExtra(CloudDevice.DEVICE_ID_EXTRA_KEY, DeviceSLPF.INVALID_DEVICE_ID);
            DeviceSLPF storageDevice = DataModelSLPF.getInstance().getDeviceById(broadcastedStorageId);

            if (storageDevice == null) {
                Log.i(this, "oneceive:: storageDevice is null");
                return;
            }

            if (broadcastedStorageId == mSelectedStorageDevice.getId()) {
                mSelectedStorageDevice.setPendingSetup(false);
                mSelectedStorageDevice.commitChanges();
                if (mDelegate != null) {
                    mDelegate.storageDeviceWasAddedInSignInDialogFrag();
                }
                try {
                    dismiss();
                } catch (Exception e) {
                    Log.e(this, "signInAuthSuccess/onReceive() - Exception : " + e.getMessage());
                }
            }
        }
    };

    private void getSamsungAccount() {
        try {
            final Account[] accounts = mAccountManager.getAccountsByType(SignInOutUtils.AccountType_SamsungAccount);

            if (accounts.length == 0) {
                Intent intent = new Intent(ADD_SAMSUNG_ACCOUNT);
                intent.putExtra(DriveConstants.CLIENT_ID, SA_CLIENT_ID);
                intent.putExtra(DriveConstants.CLIENT_SECRET, SA_CLIENT_SECRET);
                intent.putExtra(DriveConstants.MYPACKAGE, getActivity().getPackageName());
                intent.putExtra(DriveConstants.OSP_VER, SA_CLIENT_OSP_VERSION);
                intent.putExtra(DriveConstants.MODE, SA_CLIENT_MODE);
                startActivityForResult(intent, ACCOUNT_CODE_SAMSUNG);
            } else {
                showLoading(LOGIN_TIMEOUT);
                StorageSignInSettingDialogFragment.this.mAccount = accounts[0];

                final SamsungAccountImp samsungAccountImp = new SamsungAccountImp(DriveConstants.APPID, DriveConstants.APP_SECRET);
                samsungAccountImp.request(getContext(), DriveConstants.sIsTokenExpired || DriveConstants.apiClient.accessToken == null || DriveConstants.apiClient.accessToken.equals(""), DriveConstants.apiClient.accessToken, new SamsungAccountImp.IResultListener() {
                    @Override
                    public void onResult(Bundle result) {
                        if (result.getInt(DriveConstants.RCODE) == DriveConstants.ResultCode.SUCCESS) {
                            DriveConstants.setConstants(result, false);

                            newInstanceOfAuthTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, mSelectedStorageDevice);
                        } else {
                            Log.e(this, "getSamsungAccount()/onReceive() - onResult : " + result.getInt(DriveConstants.RCODE));
                        }
                    }
                });
            }
        } catch (SecurityException e) {
            Log.e(this, "getSamsungAccunt() - SecurityException : " + e.getMessage());
        }
    }

    private void doAuthForGoogle() {
        Log.i(this, "doAuth() called");

        try {
            final Account[] accounts = mAccountManager.getAccountsByType(SignInOutUtils.AccountType_GoogleAccount);
            final int size = accounts.length;

            if (existAccountFromMyFiles()) {
                Bundle args = getArguments();
                if (args.getString(ARG_SIGN_IN_ACCOUNT).equals(CloudGatewayConstants.ADD_ACCOUNT)) {
                    Intent addAccountIntent = new Intent(Settings.ACTION_ADD_ACCOUNT);
                    addAccountIntent.putExtra(Settings.EXTRA_ACCOUNT_TYPES, new String[]{SignInOutUtils.AccountType_GoogleAccount});
                    startActivityForResult(addAccountIntent, ACCOUNT_CODE_GOOGLE);
                    return;
                } else {
                    showLoading(LOGIN_TIMEOUT);
                    for (Account account : accounts) {
                        if (account.name.equals(args.getString(ARG_SIGN_IN_ACCOUNT))) {
                            mAccount = account;
                            break;
                        }
                    }
                    newInstanceOfAuthTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, mSelectedStorageDevice);
                }
            } else {
                if (size == 0) { // no Google account in the AccountManager
                    GoogleAccountCredential credential = GoogleAccountCredential.usingOAuth2((getActivity()), Arrays.asList(DriveScopes.DRIVE));
                    Intent intent = credential.newChooseAccountIntent();
                    startActivityForResult(intent, ACCOUNT_CODE_GOOGLE);
                    return;
                } else if (size == 1) { //one Google account in the AccountManager
                    showLoading(LOGIN_TIMEOUT);
                    StorageSignInSettingDialogFragment.this.mAccount = accounts[0];
                    newInstanceOfAuthTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, mSelectedStorageDevice);
                    return;
                } else { // in case Google accounts over one in the AccountManager, show the popup for selecting account
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle(R.string.select_account);

                    String[] names = new String[size];
                    for (int i = 0; i < size; i++) {
                        if (accounts[i] != null) {
                            names[i] = accounts[i].name;
                        } else {
                            Log.e(this, "doAuthForGoogle() - accounts[" + i + "] is null");
                        }
                    }

                    builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialogInterface) {
                            getActivity().finish();
                        }
                    });
                    builder.setItems(names, new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            showLoading(LOGIN_TIMEOUT);
                            StorageSignInSettingDialogFragment.this.mAccount = accounts[which];
                            newInstanceOfAuthTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, mSelectedStorageDevice);
                        }
                    }).show();
                }
            }
        } catch (SecurityException e) {
            Log.e(this, "doAuthForGoogle() - SecurityException : " + e.getMessage());
        }

    }

    private AsyncTask<DeviceSLPF, Void, DeviceSLPF> newInstanceOfAuthTask() {
        return new AsyncTask<DeviceSLPF, Void, DeviceSLPF>() {

            @Override
            protected DeviceSLPF doInBackground(DeviceSLPF... params) {
                DeviceSLPF d = params[0];
                Log.i(this, "newInstanceOfAuthTask() - doInBackground : adding " + d);
                d.commitChanges();
                return d;
            }

            @Override
            protected void onPostExecute(DeviceSLPF result) {
                Log.i(this, "onPostExecute() : StartSignIn, device id" + result.getId());
                signIn(result.getId());

                new Thread(new Runnable() {

                    @Override
                    public void run() {
                        try {
                            Thread.sleep(2000);
                        } catch (InterruptedException e) {
                            Log.i(this, e.getMessage());
                        }

                        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(getActivity());
                        Intent intent = new Intent(CloudStorageSync.CLOUD_OAUTH1_RESPONSE);
                        intent.putExtra("Account", StorageSignInSettingDialogFragment.this.mAccount.name);
                        intent.putExtra(CloudDevice.DEVICE_ID_EXTRA_KEY, mSelectedStorageDevice.getId());
                        localBroadcastManager.sendBroadcast(intent);
                        Log.i(this, "broadcasting intent : " + IntentHelper.intentToString(intent));

                        IASPApplication2 aspApp = ServiceLocatorSLPF.get(IASPApplication2.class);
                        DeviceSLPF device = DataModelSLPF.getInstance().getDeviceById(mSelectedStorageDevice.getId());
                        if (device != null) {
                            Log.i(this, "onActivityResult: device obj : " + device);
                            StorageProviderDatabaseHelper providerDB = StorageProviderDatabaseHelper.getInstance(aspApp.getApplicationContext());
                            StorageProviderInfo spInfo = providerDB.get(device.getWebStorageType());
                            if (spInfo != null) {
                                Log.e(this, "storage provider name : " + spInfo.getSpName());
                                spInfo.setLoginStatus(true);
                                providerDB.saveOrUpdate(spInfo);
                            } else {
                                Log.e(this, "Web storage type : " + device.getWebStorageType() + ", storage provider is null");
                            }
                        }
                    }
                }).start();
            }
        };
    }

    private void backKeyPressed() {
        Intent intent = new Intent(CloudDevice.BROADCAST_DEVICE_STATE_CHANGE);
        intent.putExtra(CloudDevice.DEVICE_ID_EXTRA_KEY, mSelectedStorageDevice.getId());
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
        getActivity().onBackPressed();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != Activity.RESULT_CANCELED) {
            switch (requestCode) {
                case ACCOUNT_CODE_GOOGLE:
                    doAuthForGoogle();
                    break;
                case ACCOUNT_CODE_SAMSUNG:
                    getSamsungAccount();
                    break;
            }
        } else {
            backKeyPressed();
        }
    }
}

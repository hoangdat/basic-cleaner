package platform.com.mfluent.asp.datamodel.call;

import android.content.Context;
import android.os.Bundle;
import android.util.Base64;

import com.mfluent.asp.cloudstorage.api.sync.CloudStorageSync;
import com.mfluent.asp.common.util.CloudStorageError;
import com.mfluent.log.Log;
import com.samsung.android.sdk.slinkcloud.CloudGatewayConstants;
import com.samsung.android.sdk.slinkcloud.CloudGatewayDeviceTransportType;
import com.samsung.android.sdk.slinkcloud.CloudGatewayFileBrowserUtils;
import com.samsung.android.sdk.slinkcloud.CloudGatewayMediaStore;
import com.samsung.android.sdk.slinkcloud.CloudGatewayMediaStore.CallMethods.MRRControlBatch;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import platform.com.mfluent.asp.datamodel.DataModelSLPF;
import platform.com.mfluent.asp.datamodel.DeviceSLPF;
import platform.com.mfluent.asp.datamodel.filebrowser.CachedFileBrowser;
import platform.com.mfluent.asp.framework.IASPApplication2;
import platform.com.mfluent.asp.util.FileUtils;
import platform.com.mfluent.asp.util.NetworkUtilSLPF;
import platform.com.samsung.android.slinkcloud.NetResourceCacheManager;

public class RemoteFolderControlBatchCallHandler implements CallHandler {
    public boolean removeLocalFile(String sourceMediaID) {
        boolean bSuccess = false;
        String decodedPath;
        try {
            decodedPath = new String(Base64.decode(sourceMediaID, Base64.NO_WRAP | Base64.URL_SAFE));
        } catch (Exception e) {
            decodedPath = sourceMediaID;
        }

        if (decodedPath != null && !decodedPath.startsWith("/")) {
            decodedPath = sourceMediaID;
        }

        File fDelete = new File(decodedPath);
        if (fDelete != null) {
            if (fDelete.exists()) {
                bSuccess = FileUtils.deleteQuietly(fDelete);
                if (!bSuccess) {
                    Log.e(this, "removeLocalFile()::file delete failed: local file id = " + sourceMediaID);
                }
            } else {
                bSuccess = true;
            }
        }
        return bSuccess;
    }

    private Bundle callForLocalStorageBatch(Bundle result, final int nCommand, String[] sourceIds) {
        boolean bRet = true;
        String retStr = null;
        switch (nCommand) {
            case CloudGatewayFileBrowserUtils.REMOTE_FOLDER_MKDIR_BATCH:
                break;
            case CloudGatewayFileBrowserUtils.REMOTE_FOLDER_RMDIR_BATCH:
                /* FALL THROUGH */
            case CloudGatewayFileBrowserUtils.REMOTE_FOLDER_RMDIR_PERMANENTLY_BATCH:
                boolean bDelSuccess;
                for (String sourceId : sourceIds) {
                    bDelSuccess = removeLocalFile(sourceId);
                    if (!bDelSuccess) {
                        bRet = false;
                    }
                }
                break;
            case CloudGatewayFileBrowserUtils.REMOTE_FOLDER_MOVE_BATCH:
                break;
            case CloudGatewayFileBrowserUtils.REMOTE_FOLDER_COPY_BATCH:
                break;
        }
        result.putBoolean(CloudGatewayMediaStore.CallMethods.KEY_RESULT, bRet);
        result.putString(CloudGatewayMediaStore.CallMethods.KEY_RESULT_STR, retStr);
        return result;
    }

    private Bundle callForWebStorageBatch(Bundle result, Context context, final int nCommand, final long deviceId, String[] parentDirectoryIds, String[] sourceIds, String[] newItems, DeviceSLPF device) {
        boolean bRet = false;

        try {

            CloudStorageSync cloudStorageSync = device.getCloudStorageSync();
            if (cloudStorageSync != null) {
                switch (nCommand) {
                    case CloudGatewayFileBrowserUtils.REMOTE_FOLDER_MKDIR_BATCH:
                        ////check first tmp file cache
//                    retStr = CachedFileBrowser.deltaFileLayerFind((int) deviceId, newItems, parentDirectoryIds);
//                    if (retStr != null) {
//                        Log.i(this, "cached cloud id is detected! name=" + newItems + ", parentid=" + parentDirectoryIds);
//                        bRet = true;
//                    } else {
//                    bRet = cloudStorageSync.createDirectoryBatch(parentDirectoryIds, newItems);
//                        if (retStr != null) {
//                            Log.i(this, "returned FileID=" + retStr);
//                            bRet = true;
//                            CachedFileBrowser.deltaFileLayerAdd(device, context, newItems, parentDirectoryIds, retStr, true);
//                        }
//                    }
                        break;
                    case CloudGatewayFileBrowserUtils.REMOTE_FOLDER_RMDIR_BATCH:
                        bRet = cloudStorageSync.deleteFileBatch(sourceIds);
                        if (bRet) {
                            for (String sourceId : sourceIds) {
                                CachedFileBrowser.deltaFileLayerRemove(device.getId(), sourceId);
                            }
                            NetResourceCacheManager.getInstance(context).deleteCache(CachedFileBrowser.URI_MATCHER + CloudGatewayMediaStore.Trash.Trash_ID, null, device.getId());
                        }
                        break;
                    case CloudGatewayFileBrowserUtils.REMOTE_FOLDER_RMDIR_PERMANENTLY_BATCH:
                        bRet = cloudStorageSync.deletePermanentlyBatch(sourceIds);
                        if (bRet) {
                            for (String sourceId : sourceIds) {
                                CachedFileBrowser.deltaFileLayerRemove(device.getId(), sourceId);
                            }
                        }
                        break;
                    case CloudGatewayFileBrowserUtils.REMOTE_TRASH_RESTORE_BATCH:
                        Log.d(this, "restoreTrashBatch call");
                        if (sourceIds != null && sourceIds.length > 0) {
                            ArrayList<String> parentSuccess = cloudStorageSync.restoreTrashBatch(sourceIds);
                            ArrayList<String> doneCache = new ArrayList<>();
                            if (parentSuccess != null && !parentSuccess.isEmpty()) {
                                int size = parentSuccess.size();
                                for (int i = 0; i < size; i++) {
                                    if (doneCache.contains(parentSuccess.get(i))) {
                                        continue;
                                    }
                                    Log.d(this, "restoreTrashBatch deleteCache file = " + parentSuccess.get(i));
                                    NetResourceCacheManager.getInstance(context).deleteCache(CachedFileBrowser.URI_MATCHER + parentSuccess.get(i), null, (int) deviceId);
                                    doneCache.add(parentSuccess.get(i));
                                }
                                NetResourceCacheManager.getInstance(context).deleteCache(CachedFileBrowser.URI_MATCHER + CloudGatewayMediaStore.Trash.Trash_ID, null, (int) deviceId);
                            }
                        }
                        break;
                    case CloudGatewayFileBrowserUtils.REMOTE_TRASH_DELETE_BATCH:
                        if (sourceIds != null && sourceIds.length > 0) {
                            bRet = cloudStorageSync.deleteTrashBatch(sourceIds);
                            if (bRet) {
                                for (int i = 0; i < sourceIds.length; i++) {
                                    Log.d(this, "deleteTrashBatch deltaFileLayerRemove file = " + sourceIds[i]);
                                    CachedFileBrowser.deltaFileLayerRemoveTrash((int) deviceId, sourceIds[i], false);
                                }
                            }
                            NetResourceCacheManager.getInstance(context).deleteCache(CachedFileBrowser.URI_MATCHER + CloudGatewayMediaStore.Trash.Trash_ID, null, (int) deviceId);
                        }
                        break;
                    case CloudGatewayFileBrowserUtils.REMOTE_FOLDER_RMDIR_BATCH_CANCEL:
                        Log.d(this, "callForWebStorageBatch() - delete cancel call");
                        cloudStorageSync.cancel(CloudGatewayConstants.OperationType.DELETE);
                        break;
                }
            }
        } catch (IOException e) {
            result.putInt(CloudGatewayMediaStore.CallMethods.KEY_RESULT_QUOTA_ERROR, CloudStorageError.getExceptionError(e));
        } finally {
            result.putBoolean(CloudGatewayMediaStore.CallMethods.KEY_RESULT, bRet);
            return result;
        }
    }

    @Override
    public Bundle call(Context context, String method, String arg, Bundle extras) {
        Bundle result = new Bundle();

        final long deviceId = extras.getLong(MRRControlBatch.INTENT_ARG_BATCH_DEVICEID);
        final int nCommand = extras.getInt(MRRControlBatch.INTENT_ARG_BATCH_CMD);

        String[] parentDirectoryIds = extras.getStringArray(MRRControlBatch.INTENT_ARG_BATCH_PARENTDIRECTORYID);
        String[] sourceIds = extras.getStringArray(MRRControlBatch.INTENT_ARG_BATCH_SOURCEID);
        String[] newItems = extras.getStringArray(MRRControlBatch.INTENT_ARG_BATCH_NEWITEM);

        DataModelSLPF dataModel = DataModelSLPF.getInstance();
        DeviceSLPF device = dataModel.getDeviceById((int) deviceId);

        result.putBoolean(CloudGatewayMediaStore.CallMethods.KEY_RESULT, false);
        if (device == null) {
            Log.i(this, "device is null for device id=" + deviceId + ",cmd=" + nCommand);
            return result;
        }

        if (!NetworkUtilSLPF.isNetworkAvailable() || IASPApplication2.checkNeedWifiOnlyBlock()) {
            Log.e(this, "Network is blocked");
            return result;
        }

        Log.i(this, "RemoteFolderControlBatchCallHandler called cmd=" + nCommand);

        CloudGatewayDeviceTransportType transportType = device.getDeviceTransportType();

        switch (transportType) {
            case WEB_STORAGE:
                return callForWebStorageBatch(result, context, nCommand, deviceId, parentDirectoryIds, sourceIds, newItems, device);
            default:
                return callForLocalStorageBatch(result, nCommand, sourceIds);
        }
    }
}

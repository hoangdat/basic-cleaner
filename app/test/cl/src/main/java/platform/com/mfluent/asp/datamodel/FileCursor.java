/**
 * 
 */

package platform.com.mfluent.asp.datamodel;

import java.io.File;

import org.apache.commons.lang3.ObjectUtils;

import android.database.AbstractCursor;
import android.provider.MediaStore;

import com.mfluent.asp.common.util.FileTypeHelper;

/**
 * @author Ilan Klinghofer
 */
public class FileCursor extends AbstractCursor {

	private final File mFile;

	private final String[] COLUMN_NAMES = new String[] {
			MediaStore.MediaColumns.DATA,
			MediaStore.MediaColumns.SIZE,
			MediaStore.MediaColumns.DATE_MODIFIED,
			MediaStore.MediaColumns.MIME_TYPE};

	private static final int DATA_COLUMN_INDEX = 0;

	private static final int SIZE_COLUMN_INDEX = 1;

	private static final int DATE_MODIFIED_COLUMN_INDEX = 2;

	private static final int MIME_TYPE_COLUMN_INDEX = 3;

	public FileCursor(File file) {
		mFile = file;
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.AbstractCursor#getCount()
	 */
	@Override
	public int getCount() {
		return mFile.exists() ? 1 : 0;
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.AbstractCursor#getColumnNames()
	 */
	@Override
	public String[] getColumnNames() {
		return this.COLUMN_NAMES;
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.AbstractCursor#getString(int)
	 */
	@Override
	public String getString(int column) {
		return ObjectUtils.toString(getValueForColumn(column));
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.AbstractCursor#getShort(int)
	 */
	@Override
	public short getShort(int column) {
		return ((Number) getValueForColumn(column)).shortValue();
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.AbstractCursor#getInt(int)
	 */
	@Override
	public int getInt(int column) {
		return ((Number) getValueForColumn(column)).intValue();
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.AbstractCursor#getLong(int)
	 */
	@Override
	public long getLong(int column) {
		return ((Number) getValueForColumn(column)).longValue();
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.AbstractCursor#getFloat(int)
	 */
	@Override
	public float getFloat(int column) {
		return ((Number) getValueForColumn(column)).floatValue();
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.AbstractCursor#getDouble(int)
	 */
	@Override
	public double getDouble(int column) {
		return ((Number) getValueForColumn(column)).doubleValue();
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.AbstractCursor#isNull(int)
	 */
	@Override
	public boolean isNull(int column) {
		return getValueForColumn(column) == null;
	}

	private Object getValueForColumn(int column) {
		switch (column) {
			case DATA_COLUMN_INDEX:
				return mFile.getAbsolutePath();
			case SIZE_COLUMN_INDEX:
				return mFile.length();
			case DATE_MODIFIED_COLUMN_INDEX:
				return mFile.lastModified();
			case MIME_TYPE_COLUMN_INDEX:
				String mimeType = FileTypeHelper.getMimeTypeForFile(mFile.getName(), null);
				return mimeType;
			default:
				return null;
		}
	}
}

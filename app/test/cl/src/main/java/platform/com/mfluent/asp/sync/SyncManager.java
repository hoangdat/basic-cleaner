
package platform.com.mfluent.asp.sync;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;

import com.mfluent.log.Log;
import com.samsung.android.sdk.slinkcloud.CloudGatewayDeviceTransportType;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import platform.com.mfluent.asp.datamodel.DataModelSLPF;
import platform.com.mfluent.asp.datamodel.DeviceSLPF;
import platform.com.mfluent.asp.framework.IASPApplication2;
import platform.com.mfluent.asp.framework.ServiceLocatorSLPF;

/**
 * Responsible for coordinating all metadata syncing into the db.
 *
 * @author Ilan Klinghofer
 */
public class SyncManager extends SingleThreadedSyncManagerSLPF {

    private ConcurrentHashMap<DeviceSLPF, DeviceMetadataSyncManager> deviceSyncManagers = new ConcurrentHashMap<DeviceSLPF, DeviceMetadataSyncManager>();
    private CloudStorageTypeListSyncManager cloudStorageTypeListSyncManager;

    private boolean started;

    private final Lock syncLock = new ReentrantLock();

    private static SyncManager sInstance;

    public static synchronized SyncManager getInstance(Context context) {
        if (context == null) {
            throw new IllegalArgumentException("Context must not be null.");
        }
        if (sInstance == null) {
            sInstance = new SyncManager(context);
        }

        return sInstance;
    }

    private SyncManager(Context context) {
        super(context);
        cloudStorageTypeListSyncManager = new CloudStorageTypeListSyncManager(mContext);

        LocalBroadcastManager.getInstance(context).registerReceiver(
                masterResetBroadcastReceiver,
                new IntentFilter(IASPApplication2.BROADCAST_MASTER_RESET));
    }

    private final BroadcastReceiver masterResetBroadcastReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            //intentionally doing the master reset in the onReceive thread so that ASPApplication masterReset does not continue until doMasterReset() is done
            Log.i(this, "masterReset");

            stop(intent);

            deviceSyncManagers.clear();
            cloudStorageTypeListSyncManager.resetPreviousSyncTime();

            LocalBroadcastManager.getInstance(context).registerReceiver(
                    masterResetCompleteBroadcastReceiver,
                    new IntentFilter(IASPApplication2.BROADCAST_MASTER_RESET_COMPLETE));
        }
    };

    private final BroadcastReceiver masterResetCompleteBroadcastReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            start(intent);

            LocalBroadcastManager.getInstance(context).unregisterReceiver(this);
        }
    };

    @Override
    protected Collection<IntentFilter> getSyncIntentFilters() {
        Collection<IntentFilter> syncIntentFilters = super.getSyncIntentFilters();
        syncIntentFilters.add(new IntentFilter(DataModelSLPF.BROADCAST_DEVICE_LIST_CHANGE));
        syncIntentFilters.add(new IntentFilter(IASPApplication2.BROADCAST_GO_HOME));

        return syncIntentFilters;
    }

    /*
     * (non-Javadoc)
     * @see com.mfluent.asp.sync.SingleThreadedSyncManager#start()
     */
    @Override
    public void doStart(Intent intent) {
        super.doStart(intent);

        ServiceLocatorSLPF.bind(cloudStorageTypeListSyncManager, CloudStorageTypeListSyncManager.class);
    }

    private void startSyncManagers(Intent intent) {
        if (started) {
            return;
        }

        Log.i(this, "Starting up sync managers");

        //		ASPApplication app = ServiceLocator.get(ASPApplication.class);
        cloudStorageTypeListSyncManager.start(intent);

        Log.i(this, "Starting up metadataSyncManager managers");
        for (DeviceMetadataSyncManager metadataSyncManager : deviceSyncManagers.values()) {
            metadataSyncManager.start(intent);
        }

        started = true;
    }

    @Override
    protected void doStop(Intent intent) {
        LocalBroadcastManager.getInstance(mContext).unregisterReceiver(masterResetBroadcastReceiver);

        syncLock.lock();
        try {
            stopSyncManagers(intent);

            super.doStop(intent);
        } finally {
            syncLock.unlock();
        }
    }

    private void stopSyncManagers(Intent intent) {
        if (!started) {
            return;
        }

        cloudStorageTypeListSyncManager.resetPreviousSyncTime();
        cloudStorageTypeListSyncManager.stop(intent);

        for (DeviceMetadataSyncManager metadataSyncManager : deviceSyncManagers.values()) {
            metadataSyncManager.stop(intent);
        }

        started = false;
    }

    @Override
    protected void doSync(Intent intent) {
        Log.i(this, "Trace synclock: doSync start");
        syncLock.lock();
        try {
            if (!wasHome()) {
                return;
            }

            // check local device registered flag

            //			if (ContentAggregatorService.SERVICE_ON_CREATE_INTENT.equals(intent.getAction())) {
            //				//ignore this one - just for bootstrapping
            //				return;
            //			}

            try {
                startSyncManagers(intent);
                Log.i(this, "End up startSyncManagers");
                DataModelSLPF dataModel = DataModelSLPF.getInstance();

                Set<DeviceSLPF> devicesToRemove = new HashSet<DeviceSLPF>(deviceSyncManagers.keySet());

                for (DeviceSLPF device : dataModel.getDevices()) {
                    if (device.isSupportedDeviceType()) {
                        DeviceMetadataSyncManager metadataSyncManager = deviceSyncManagers.get(device);
                        if ((metadataSyncManager == null) &&
                                (device.getDeviceTransportType() == CloudGatewayDeviceTransportType.WEB_STORAGE)) {
                            metadataSyncManager = new CloudStorageSyncManager(mContext, device);
                            metadataSyncManager.start(intent);
                            deviceSyncManagers.put(device, metadataSyncManager);
                        }
                    }
                    devicesToRemove.remove(device);
                }

                //anything left in devicesToRemove needs to be removed
                for (DeviceSLPF device : devicesToRemove) {
                    Log.i(this, "doSync:Removing " + device + " from sync manager");
                    DeviceMetadataSyncManager metadataSyncManager = deviceSyncManagers.remove(device);
                    if (metadataSyncManager != null) {
                        metadataSyncManager.stop(intent);
                    }
                }
            } catch (Exception e) {
                Log.e(this, "doSync: Trouble syncing " + e);
            }
        } finally {
            syncLock.unlock();
            Log.i(this, "Trace synclock: doSync end");
        }
    }

    @Override
    protected boolean syncOnServiceCreate() {
        return true;
    }

    private boolean wasHome() {
        return ServiceLocatorSLPF.get(IASPApplication2.class).wasInitSyncStart();
    }
}

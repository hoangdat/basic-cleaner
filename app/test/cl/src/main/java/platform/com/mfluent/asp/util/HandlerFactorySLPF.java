
package platform.com.mfluent.asp.util;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

import android.os.Handler;
import android.os.Looper;

public class HandlerFactorySLPF {

	private static final AtomicInteger id = new AtomicInteger(1);

	private static final HandlerFactorySLPF sInstance = new HandlerFactorySLPF();

	public static HandlerFactorySLPF getInstance() {
		return sInstance;
	}

	private HandlerFactorySLPF() {
	}

	public Handler start(Handler.Callback callback) throws InterruptedException {
		return start("Handler-" + id.getAndIncrement(), callback);
	}

	public Handler start(String threadName, final Handler.Callback callback) throws InterruptedException {
		final BlockingQueue<Handler> queue = new ArrayBlockingQueue<Handler>(1);

		new Thread(new Runnable() {

			@Override
			public void run() {
				Looper.prepare();

				queue.add(new Handler(callback));
				Looper.loop();
			}
		}, threadName).start();

		return queue.take();
	}
}

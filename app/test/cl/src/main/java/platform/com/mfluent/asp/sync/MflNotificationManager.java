
package platform.com.mfluent.asp.sync;

import android.os.Handler;

import com.mfluent.log.Log;

import org.json.JSONObject;

import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import platform.com.mfluent.asp.datamodel.DataModelSLPF;
import platform.com.mfluent.asp.datamodel.DeviceSLPF;

public class MflNotificationManager {

    private static final int NOTIFICATION_DEVICE_LIST_QUIET_PERIOD = 20000;

    private final Handler mHandler = new Handler();
    private final ExecutorService mPokeExecutor = Executors.newSingleThreadExecutor();
    private Runnable mPendingDeviceListNotificationChecker = null;

    public void deviceListChange(final DeviceSLPF changedDevice, final boolean isWebStorageSignedIn) {
        Log.i(this, "deviceListChange : " + changedDevice.getAliasName());
        Log.i(this, "isWebStorageSignedIn : " + isWebStorageSignedIn);

        mPendingDeviceListNotificationChecker = new Runnable() {

            @Override
            public void run() {
                pokePeersToSyncDeviceList(changedDevice, isWebStorageSignedIn);
            }
        };

        mHandler.removeCallbacks(mPendingDeviceListNotificationChecker);

        mHandler.postDelayed(mPendingDeviceListNotificationChecker, NOTIFICATION_DEVICE_LIST_QUIET_PERIOD);
    }

    public void pokePeersToSyncDeviceList(final DeviceSLPF changedDevice, final boolean isWebStorageSignedIn) {
        Set<DeviceSLPF> peersToPoke = DataModelSLPF.getInstance().getDevices();
        final DeviceSLPF localDevice = DataModelSLPF.getInstance().getLocalDevice();

        for (final DeviceSLPF device : peersToPoke) {

            if (device == localDevice) {
                continue;
            }
            mPokeExecutor.execute(new Runnable() {

                @Override
                public void run() {
                    // Only poke devices that implement the 1.6 (or greater) server protocol and are online
                    if (/*device.isDeviceTransportType(CloudGatewayDeviceTransportType.SLINK) && */device.isPresence()) {
                        doNTSDeviceListPoke(device, changedDevice, isWebStorageSignedIn);
                    }
                }
            });
        }
    }

    private void doNTSDeviceListPoke(DeviceSLPF device, DeviceSLPF changedDevice, boolean isWebStorageSignedIn) {
        // Only poke the peer if we think it's online
        if (!device.isPresence()) {
            return;
        }

        try {
            Log.i(this, "doNTSDeviceListPoke");
            JSONObject requestObject = new JSONObject();
            requestObject.put("SPName", changedDevice.getWebStorageType());
            requestObject.put("IsSignin", isWebStorageSignedIn);
        } catch (Exception e) {
            Log.e(this, "doNTSDeviceListPoke() - device : " + device.getDisplayName() + ", Exception : " + e.getMessage());
        }
    }
}

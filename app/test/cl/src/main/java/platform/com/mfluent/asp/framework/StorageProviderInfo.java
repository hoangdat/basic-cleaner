
package platform.com.mfluent.asp.framework;

public class StorageProviderInfo {
    private int mId;
    private boolean mIsOAuth;
    private boolean mLoginStatus;
    private String mName;
    private String mSpName;
    private String mType;

    private String mMain;

    private boolean mSupportsSignup = false;
    private boolean mForbidden = false;

    private int mMaxNumTxConn;
    private String mOAuthWebViewJS;

    private int mSortKey;

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public boolean isOAuth() {
        return mIsOAuth;
    }

    public void setOAuth(boolean isOAuth) {
        mIsOAuth = isOAuth;
    }

    public boolean isLoginStatus() {
        return mLoginStatus;
    }

    public void setLoginStatus(boolean loginStatus) {
        mLoginStatus = loginStatus;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getSpName() {
        return mSpName;
    }

    public void setSpName(String spName) {
        mSpName = spName;
    }

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        mType = type;
    }

    /**
     * @return the plugins CloudStorageSync class name
     */
    public String getMain() {
        return mMain;
    }

    public void setMain(String main) {
        mMain = main;
    }

    public boolean getSupportSignUp() {
        return mSupportsSignup;
    }

    public void setSupportsSignUp(boolean supportsSignUp) {
        mSupportsSignup = supportsSignUp;
    }

    @Override
    public String toString() {
        return "StorageProviderInfo [name: "
                + mName
                + ", isLoggedIn: "
                + mLoginStatus
                + "]";
    }

    public void setSortKey(int sortKey) {
        mSortKey = sortKey;
    }

    public int getSortKey() {
        return mSortKey;
    }

    public boolean getForbidden() {
        return mForbidden;
    }

    public void setForbidden(boolean forbidden) {
        mForbidden = forbidden;
    }

    public int getMaxNumTxConn() {
        return mMaxNumTxConn;
    }

    public void setMaxNumTxConn(int maxNumTxConn) {
        mMaxNumTxConn = maxNumTxConn;
    }

    public String getOAuthWebViewJS() {
        return mOAuthWebViewJS;
    }

    public void setOAuthWebViewJS(String OAuthWebViewJS) {
        mOAuthWebViewJS = OAuthWebViewJS;
    }
}

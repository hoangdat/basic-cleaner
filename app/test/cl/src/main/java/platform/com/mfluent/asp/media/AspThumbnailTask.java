/**
 *
 */

package platform.com.mfluent.asp.media;

import com.mfluent.asp.common.media.thumbnails.ImageInfo;

/**
 * @author michaelgierach
 */
public class AspThumbnailTask extends AspMediaTask {

    public final ThumbnailGetter mGetter;

    public AspThumbnailTask(ImageInfo imageInfo, ThumbnailGetter getter) {
        super(imageInfo);
        mGetter = getter;
    }

    @Override
    protected void cancelAction() {
        super.cancelAction();

        if (mGetter != null) {
            mGetter.cancel();
        }
    }
}

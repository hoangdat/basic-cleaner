
package platform.com.mfluent.asp.util;

import java.util.ArrayList;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.commons.lang3.StringUtils;

public abstract class TokenBasedReadWriteLock<T> {

	private static class TokenLockStruct {

		Object mToken;
		Thread mThread;
		String mExternalToken;
		int mReentrantCount = 1;
	}

	private final ReentrantLock mLock;
	private final Condition mReadSignal;
	private final Condition mWriteSignal;
	private final ArrayList<TokenLockStruct> mReadTokens;
	private TokenLockStruct mWriteToken;

	public TokenBasedReadWriteLock() {
		this.mLock = new ReentrantLock();
		this.mReadSignal = this.mLock.newCondition();
		this.mWriteSignal = this.mLock.newCondition();
		this.mReadTokens = new ArrayList<TokenLockStruct>(5);
		this.mWriteToken = null;
	}

	protected abstract T generateNextToken();

	private TokenLockStruct findReadTokenByThread(Thread thread) {
		int size = this.mReadTokens.size();

		for (int i = 0; i < size; ++i) {
			TokenLockStruct curr = this.mReadTokens.get(i);
			if (curr.mThread == thread) {
				return curr;
			}
		}

		return null;
	}

	private TokenLockStruct findReadTokenByToken(T token) {
		int size = this.mReadTokens.size();

		for (int i = 0; i < size; ++i) {
			TokenLockStruct curr = this.mReadTokens.get(i);
			if (curr.mToken.equals(token)) {
				return curr;
			}
		}

		return null;
	}

	private TokenLockStruct findReadTokenByExternalToken(String externalToken) {
		int size = this.mReadTokens.size();

		for (int i = 0; i < size; ++i) {
			TokenLockStruct curr = this.mReadTokens.get(i);
			if (StringUtils.equals(externalToken, curr.mExternalToken)) {
				return curr;
			}
		}

		return null;
	}

	public boolean threadHasReadLock() {
		Thread currentThread = Thread.currentThread();
		this.mLock.lock();
		try {
			return (findReadTokenByThread(currentThread) != null);
		} finally {
			this.mLock.unlock();
		}
	}

	@SuppressWarnings("unchecked")
	public T readLock() {
		Thread currentThread = Thread.currentThread();
		this.mLock.lock();
		try {
			TokenLockStruct lockStruct = null;

			lockStruct = findReadTokenByThread(currentThread);
			if (lockStruct != null) {
				lockStruct.mReentrantCount++;

			} else {
				while (this.mWriteToken != null && this.mWriteToken.mThread != currentThread) {
					try {
						this.mWriteSignal.await();
					} catch (InterruptedException ie) {
						//Do not return here as this method is not named readLockInterruptably().
					}
				}

				lockStruct = new TokenLockStruct();
				lockStruct.mThread = currentThread;
				lockStruct.mToken = generateNextToken();

				this.mReadTokens.add(lockStruct);
			}

			return (T) lockStruct.mToken;
		} finally {
			this.mLock.unlock();
		}
	}

	@SuppressWarnings("unchecked")
	public T readLock(String externalToken) {
		if (StringUtils.isEmpty(externalToken)) {
			throw new IllegalArgumentException("Missing external token for lock request.");
		}

		this.mLock.lock();
		try {
			TokenLockStruct lockStruct = null;

			lockStruct = findReadTokenByExternalToken(externalToken);
			if (lockStruct != null) {
				lockStruct.mReentrantCount++;

			} else {
				while (this.mWriteToken != null) {
					try {
						this.mWriteSignal.await();
					} catch (InterruptedException ie) {
						//Do not return here as this method is not named readLockInterruptably().
					}
				}

				lockStruct = new TokenLockStruct();
				lockStruct.mToken = generateNextToken();
				lockStruct.mExternalToken = externalToken;

				this.mReadTokens.add(lockStruct);
			}

			return (T) lockStruct.mToken;
		} finally {
			this.mLock.unlock();
		}
	}

	@SuppressWarnings("unchecked")
	public T writeLock() {
		Thread currentThread = Thread.currentThread();
		this.mLock.lock();
		try {
			if (this.mWriteToken != null && this.mWriteToken.mThread == currentThread) {
				this.mWriteToken.mReentrantCount++;
			} else {
				TokenLockStruct readLocked = findReadTokenByThread(currentThread);
				if (readLocked != null) {
					throw new IllegalStateException("You should not try to acquire a write lock while holding a read lock.");
				}

				while (this.mReadTokens.size() > 0 || this.mWriteToken != null) {
					try {
						if (this.mWriteToken != null) {
							this.mWriteSignal.await();
						} else {
							this.mReadSignal.await();
						}
					} catch (InterruptedException ie) {
						//Do not return here as this method is not named writeLockInterruptably().
					}
				}

				this.mWriteToken = new TokenLockStruct();
				this.mWriteToken.mThread = currentThread;
				this.mWriteToken.mToken = generateNextToken();
			}

			return (T) this.mWriteToken.mToken;
		} finally {
			this.mLock.unlock();
		}
	}

	public boolean readUnlock(T token) {
		boolean unlocked = false;

		this.mLock.lock();
		try {
			TokenLockStruct readStruct = findReadTokenByToken(token);
			if (readStruct != null) {
				unlocked = true;
				readStruct.mReentrantCount--;
				if (readStruct.mReentrantCount <= 0) {
					this.mReadTokens.remove(readStruct);
					if (this.mReadTokens.size() == 0) {
						this.mReadSignal.signalAll();
					}
				}
			} else {
				throw new IllegalStateException("Read token is invalid. Cannot unlock.");
			}

		} finally {
			this.mLock.unlock();
		}

		return unlocked;
	}

	public boolean writeUnlock(T token) {
		boolean unlocked = false;

		this.mLock.lock();
		try {
			unlocked = (this.mWriteToken != null && this.mWriteToken.mToken.equals(token));
			if (unlocked) {
				this.mWriteToken.mReentrantCount--;
				if (this.mWriteToken.mReentrantCount <= 0) {
					this.mWriteToken = null;
					this.mWriteSignal.signalAll();
				}
			} else {
				throw new IllegalStateException("Write token is invalid. Cannot unlock.");
			}
		} finally {
			this.mLock.unlock();
		}

		return unlocked;
	}

}


package platform.com.mfluent.asp.filetransfer;

import android.content.Context;

public class FileTransferManagerSingleton {

	private static FileTransferManager sInstance;

	public static synchronized FileTransferManager getInstance(Context context) {
		if (context == null) {
			throw new NullPointerException("context is null");
		}
		if (sInstance == null) {
			sInstance = new FileTransferManagerImpl(context.getApplicationContext(), true);
		}
		return sInstance;
	}
}

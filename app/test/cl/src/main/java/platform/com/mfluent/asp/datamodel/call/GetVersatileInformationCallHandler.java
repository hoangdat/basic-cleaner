
package platform.com.mfluent.asp.datamodel.call;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.util.Log;


import platform.com.mfluent.asp.datamodel.DataModelSLPF;
import platform.com.mfluent.asp.datamodel.DeviceSLPF;
import platform.com.mfluent.asp.framework.IASPApplication2;

import com.mfluent.asp.cloudstorage.api.sync.CloudStorageSync;
import com.samsung.android.sdk.slinkcloud.CloudGatewayDeviceInfoUtils;
import com.samsung.android.sdk.slinkcloud.CloudGatewayDeviceTransportType;
import com.samsung.android.sdk.slinkcloud.CloudGatewayMediaStore;

import java.util.ArrayList;

public class GetVersatileInformationCallHandler implements CallHandler {
    @Override
    public Bundle call(Context context, String method, String arg, Bundle extras) {
        Bundle result = null;
        try {
            final long deviceId = extras.getLong(CloudGatewayMediaStore.CallMethods.GetVersatileInformation.INTENT_ARG_DEVICEID);
            final String strCommand = extras.getString(CloudGatewayMediaStore.CallMethods.GetVersatileInformation.INTENT_ARG_INFO_TYPE);
            if (strCommand == null)
                return null;

            DeviceSLPF objDev = DataModelSLPF.getInstance().getDeviceById(deviceId);
            result = new Bundle();

            switch (strCommand) {
                case CloudGatewayDeviceInfoUtils.READY_TO_LOGIN: {
                    boolean bRet = false;
                    if (objDev != null) {
                        if (objDev.isDeleted() || objDev.getCloudStorageSync() == null || objDev.getCloudNeedFirstUpdate()) {
                            Log.i("VIC", "device is not ready to login bNeedFirstSync=" + objDev.getCloudNeedFirstUpdate() + " " + objDev.getCloudStorageSync());
                            bRet = false;
                        } else {
                            bRet = true;
                        }
                    }
                    Log.i("VIC", objDev + " device result=" + bRet);
                    result.putBoolean("result", bRet);
                    break;
                }
                case CloudGatewayDeviceInfoUtils.GET_LAST_SYNC_TIME: {
                    long bRet = 0;
                    if (objDev != null) {
                        CloudStorageSync cloudStorageSync = objDev.getCloudStorageSync();
                        if (!objDev.isDeleted() && (cloudStorageSync != null)) {
                            bRet = cloudStorageSync.getLastSync();
                        }
                    }
                    result.putLong("result", bRet);
                    break;
                }
                case CloudGatewayDeviceInfoUtils.GET_STORAGE_USED: {
                    long[] bRet = new long[3];
                    if (objDev != null) {
                        CloudStorageSync cloudStorageSync = objDev.getCloudStorageSync();
                        if (!objDev.isDeleted() && (cloudStorageSync != null)) {
                            bRet = cloudStorageSync.getDetailedUsed();
                        }
                    }
                    result.putLongArray("result", bRet);
                    break;
                }
                case CloudGatewayDeviceInfoUtils.UPDATE_QUOTA: {
                    long[] bRet = null;
                    if (objDev != null) {
                        CloudStorageSync cloudStorageSync = objDev.getCloudStorageSync();
                        if (!objDev.isDeleted() && (cloudStorageSync != null)) {
                            bRet = cloudStorageSync.updateQuota();
                        }
                    }
                    result.putLongArray("result", bRet);
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            result = null;
        }
        return result;
    }
}

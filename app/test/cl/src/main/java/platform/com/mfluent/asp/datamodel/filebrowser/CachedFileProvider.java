
package platform.com.mfluent.asp.datamodel.filebrowser;

import android.content.Context;

import com.mfluent.asp.common.datamodel.ASPFile;
import com.mfluent.asp.common.datamodel.ASPFileBrowser;
import com.mfluent.asp.common.datamodel.ASPFileProvider;
import com.mfluent.asp.common.datamodel.ASPFileSortType;
import com.mfluent.asp.common.datamodel.CloudDevice;
import com.mfluent.log.Log;
import com.samsung.android.sdk.slinkcloud.CloudGatewayDeviceTransportType;
import com.samsung.android.sdk.slinkcloud.CloudGatewayMediaStore;

import java.io.FileNotFoundException;
import java.io.IOException;

import platform.com.mfluent.asp.datamodel.DeviceSLPF;

public class CachedFileProvider implements ASPFileProvider {

    private boolean isNewFileBrowser = false;
    private ASPFileProvider mOrgProvider = null;
    private final DeviceSLPF mDevice;
    private final Context mContext;

    public CachedFileProvider(ASPFileProvider orgProvider, Context context, DeviceSLPF device) {
        mOrgProvider = orgProvider;
        mContext = context.getApplicationContext();
        mDevice = device;
    }

    public void setNonBlockingBrowser() {
        isNewFileBrowser = true;
    }

    @Override
    public String getStorageGatewayFileId(ASPFile file) throws FileNotFoundException {
        if (file instanceof CachedCloudFile) {
            CachedCloudFile cloudFile = (CachedCloudFile) file;
            return cloudFile.getCloudId();
        }
        return (mOrgProvider != null) ? mOrgProvider.getStorageGatewayFileId(file) : null;
    }

    @Override
    public Context getApplicationContext() {
        return mContext;
    }

    @Override
    public CloudDevice getCloudDevice() {
        return mDevice;
    }

    public ASPFileBrowser<?> getCloudStorageFileBrowser2(String storageGatewayID, ASPFileSortType sortType, boolean forceReload, String strOrgSortOption, String selection, String[] selectionArgs, boolean isBrowserForView)
            throws InterruptedException, IOException {
        if (mOrgProvider == null) {
            Log.e(this, "getCloudStorageFileBrowser2 - mOrgProvider null");
            return null;
        }
        if (isNewFileBrowser) {
            if (storageGatewayID != null && storageGatewayID.equals(CloudGatewayMediaStore.FileBrowser.ROOT_DIRECTORY_ID)) {
                CloudDevice clDev = getCloudDevice();
                DeviceSLPF device = (DeviceSLPF) clDev;
                if (clDev != null && device.getDeviceTransportType() == CloudGatewayDeviceTransportType.WEB_STORAGE) {
                    storageGatewayID = "";
                }
            }
            Log.i(this, "getCloudStorageFileBrowser2 new for id=" + storageGatewayID + ",strOrgSortOption=" + strOrgSortOption);
            CachedFileBrowser result = new CachedFileBrowser(mDevice, mContext);
            if ((result != null) && result.initCache(storageGatewayID, sortType, forceReload, mOrgProvider, strOrgSortOption, selection, selectionArgs, isBrowserForView))
                return result;
            return null;
        } else {
            try {
                return mOrgProvider.getCloudStorageFileBrowser(storageGatewayID, sortType, forceReload, isBrowserForView);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
    }

    @Override
    public ASPFileBrowser<?> getCloudStorageFileBrowser(String storageGatewayID, ASPFileSortType sortType, boolean forceReload, boolean isBrowserForView)
            throws InterruptedException, IOException {
        return getCloudStorageFileBrowser2(storageGatewayID, sortType, forceReload, null, null, null, isBrowserForView);
    }

    @Override
    public int deleteFiles(String directoryStorageGatewayId, ASPFileSortType sortType, String... storageGatewayFileIdsToDelete)
            throws InterruptedException, IOException {
        return (mOrgProvider != null) ? mOrgProvider.deleteFiles(directoryStorageGatewayId, sortType, storageGatewayFileIdsToDelete) : 0;
    }

    //jsub12_151006
    @Override
    public int getCountOfChild(ASPFile file_param) {
        return 0;
    }

    //jsub12_151006
    @Override
    public int getCountOfChildDir(ASPFile file_param) {
        return 0;
    }

    @Override
    public int getCountOfDescendants(ASPFile file) {
        return 0;
    }

    @Override
    public int getCountOfDescendantDir(ASPFile file) {
        return 0;
    }

    @Override
    public String getTrashProcessingDir(ASPFile file) {
        return null;
    }
}

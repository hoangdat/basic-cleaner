package platform.com.mfluent.asp.filetransfer;

import android.content.Context;
import android.database.Cursor;
import android.text.TextUtils;

import com.mfluent.asp.cloudstorage.api.sync.CloudStorageSync;
import com.mfluent.asp.common.datamodel.ASPFile;
import com.mfluent.asp.common.datamodel.ASPFileProvider;
import com.mfluent.asp.common.datasource.AspMediaId;
import com.mfluent.asp.common.util.CloudStorageError;
import com.mfluent.asp.common.util.FileTypeHelper;
import com.mfluent.log.Log;
import com.samsung.android.sdk.slinkcloud.CloudGatewayConstants.OperationType;
import com.samsung.android.sdk.slinkcloud.CloudGatewayFileBrowserUtils;
import com.samsung.android.sdk.slinkcloud.CloudGatewayFileTransferUtils;
import com.samsung.android.sdk.slinkcloud.CloudGatewayFileTransferUtils.TransferOptions;
import com.samsung.android.sdk.slinkcloud.CloudGatewayMediaStore;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import platform.com.mfluent.asp.datamodel.DataModelSLPF;
import platform.com.mfluent.asp.datamodel.DeviceSLPF;
import platform.com.mfluent.asp.datamodel.filebrowser.CachedFileBrowser;
import platform.com.mfluent.asp.datamodel.filebrowser.LocalASPFileSLPF;
import platform.com.mfluent.asp.util.AspFileUtilsSLPF;
import platform.com.mfluent.asp.util.FileUtils;
import platform.com.samsung.android.slinkcloud.NetResourceCacheManager;

class UploadTask extends FileTransferTask implements FileTransferTask.UploadCompleteListener {

    public static class FileUploadInfo extends FileTransferTask.TransferTaskInfo {
        public File fileToUpload;
        public String mimeType;
        public boolean completed;
        public boolean skipped;
        public boolean skipTransfer = false;
        public String displayName;
        public String relativeTargetPath;
        public String targetFolderID;
        public String fileName;
        public boolean isDirectory = false;
        public ASPFile aspDirectory = null;

        public FileUploadInfo() {
            relativeTargetPath = null;
            targetFolderID = null;
            fileName = null;
        }
    }

    private final ArrayList<FileUploadInfo> filesToUpload = new ArrayList<FileUploadInfo>(3);
    private Context mContext = null;

    public UploadTask(Context context, DeviceSLPF targetDevice, FileTransferTaskListener listener, String sessionId, TransferOptions options) {
        super(context, targetDevice, listener, sessionId, options);
        mContext = context;
    }

    @Override
    public void addTask(ASPFile file, DeviceSLPF sourceDevice) {
        Log.d(this, "addTask(ASPFile, sourceDevice)");
        if (sourceDevice.isLocalDevice() == false) {
            throw new IllegalArgumentException("Upload source device must be the local device");
        }
        addTask(((LocalASPFileSLPF) file).getFile(), AspMediaId.MEDIA_TYPE_NONE);
        addSourceDevice(sourceDevice);
    }

    @Override
    public boolean addTask(ASPFile file, DeviceSLPF sourceDevice, String relativeTarget, Map<String, String> targetFolderCache) {
        boolean bRet = true;

        if (file == null)
            return false;

        File tmpFile = ((LocalASPFileSLPF) file).getFile();
        Log.d(this, "addTask(ASPFile file, DeviceSLPF sourceDevice,  String relativeTarget)");
        FileUploadInfo mainUploadInfo = new FileUploadInfo();
        mainUploadInfo.fileToUpload = tmpFile;
        if (tmpFile != null)
            mainUploadInfo.mimeType = FileTypeHelper.getMimeTypeForFile(tmpFile);

        mainUploadInfo.relativeTargetPath = relativeTarget;
        if (!TextUtils.isEmpty(relativeTarget)) {
            mainUploadInfo.targetFolderID = findFolderIDfromRelativePath(relativeTarget, targetFolderCache);
            if (mRenameSkipResult.equals(mainUploadInfo.targetFolderID))
                bRet = false;
        }

        if (file.isDirectory()) {
            mainUploadInfo.skipTransfer = true;
            mainUploadInfo.isDirectory = true;
            mainUploadInfo.aspDirectory = file;
        }
        mainUploadInfo.fileName = file.getName();
        filesToUpload.add(mainUploadInfo);
        setTotalBytesToTransfer(getTotalBytesToTransfer() + file.length());

        addSourceDevice(DataModelSLPF.getInstance().getLocalDevice());
        return bRet;
    }

    public void addTask(File file, int mediaType) {
        Log.d(this, "addTask(ASPFile, mediaType : " + mediaType);
        FileUploadInfo mainUploadInfo = new FileUploadInfo();
        mainUploadInfo.fileToUpload = file;
        mainUploadInfo.mimeType = FileTypeHelper.getMimeTypeForFile(file);
        mainUploadInfo.fileName = file.getName();

        Log.d(this, "addTask() , mimeType : " + mainUploadInfo.mimeType);
        filesToUpload.add(mainUploadInfo);

        setTotalBytesToTransfer(getTotalBytesToTransfer() + file.length());
        addSourceDevice(DataModelSLPF.getInstance().getLocalDevice());
    }

    /*
     * (non-Javadoc)
     * @see com.mfluent.asp.filetransfer.FileTransferTask#isDownload()
     */
    @Override
    public boolean isDownload() {
        return false;
    }

    /*
     * (non-Javadoc)
     * @see com.mfluent.asp.filetransfer.FileTransferTask#getNumberOfFiles()
     */
    @Override
    public int getNumberOfFiles() {
        return filesToUpload.size();
    }

    /*
     * (non-Javadoc)
     * @see com.mfluent.asp.filetransfer.FileUploader.UploadCompleteListener#onUploadComplete(com.mfluent.asp.filetransfer.FileUploader.FileUploadInfo)
     */
    @Override
    public void onUploadComplete(FileUploadInfo fileUploadInfo) {
        Log.d(this, "onUploadComplete() - fileUploadInfo displayName : " + fileUploadInfo.fileName + ", mimeType : " + fileUploadInfo.mimeType);
        fileUploadInfo.completed = true;
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("UploadTask: #").append(getNumberOfFiles());
        if (getOptions().deleteSourceFilesWhenTransferIsComplete) {
            sb.append(" +delete");
        }
        return sb.toString();
    }

    @Override
    public int getCurrentFileIndex() {
        int currentFileIndex = 0;
        for (FileUploadInfo info : filesToUpload) {
            currentFileIndex++;
            if (!info.completed) {
                break;
            }
        }

        return currentFileIndex;
    }

    //multi channel codes
    @Override
    protected void prepareMultiChannel() throws Exception {
        super.prepareMultiChannel();
        //check folder to file list to send
        ASPFileProvider fileProvider = beginFolderSourceAnalysisBeforeSending();
        if (fileProvider != null) {
            String strCloudId;

            int nSize = filesToUpload.size();
            for (int nCnt = 0; nCnt < nSize; nCnt++) {
                FileUploadInfo fileUpload = filesToUpload.get(nCnt);
                //for (FileUploadInfo fileUpload : this.filesToUpload) {
                if (fileUpload.isDirectory) {
                    try {
                        LocalASPFileSLPF tmpASPFile = new LocalASPFileSLPF(fileUpload.fileToUpload);
                        strCloudId = fileProvider.getStorageGatewayFileId(tmpASPFile);
                    } catch (Exception e) {
                        Log.e(this, "prepareMultiChannel() - Exception : " + e.getMessage());
                        strCloudId = null;
                    }
                    checkSourceFolderBeforeSending(fileProvider, strCloudId, fileUpload.aspDirectory, fileUpload.fileName);
                }
            }
            endFolderSourceAnalysisBeforeSending();
        }
        //check rename condition
        if (getOptions().waitForRename) {
            for (final FileUploadInfo fileUpload : filesToUpload) {
                if (isCanceled()) {
                    Log.i(this, "prepareMultiChannel() - canceled");
                    break;
                }
                String targetDirId = null;
                if (fileUpload.skipTransfer) {
                    Log.i(this, "prepareMultiChannel() - skip checkRename " + fileUpload.fileName + " due to dirprecheck done");
                    continue;
                }
                if (fileUpload.targetFolderID != null) {
                    targetDirId = fileUpload.targetFolderID;
                } else if (getOptions().targetDirectoryPath != null) {
                    targetDirId = getOptions().targetDirectoryPath;
                }

                final String finalTargetDirId = targetDirId;
                requestRename(targetDirId, fileUpload.fileName, new RenameDataItem.OnReceiveRSPRenameStatus() {
                    @Override
                    public void onReceiveRenameStatus(RenameDataItem dataItem, String strRenamed) {
                        Log.d(this, "prepareMultiChannel() - onReceiveRenameStatus newName = " + strRenamed);
                        processRenameResult(fileUpload, dataItem, strRenamed, finalTargetDirId);
                    }
                });
            }
        }
    }

    @Override
    protected void processRenameResult(TransferTaskInfo taskInfo, RenameDataItem dataItem, String strNewName, String targetDirId) {
        int nRspStatus = 0;
        if (dataItem != null) {
            nRspStatus = dataItem.nStatus;
            if (dataItem.bApplyAll) {
                mbRenameApplyedAll = true;
                m_nRenameApplyedAllStatus = nRspStatus;
            }
        }
        if (taskInfo instanceof FileUploadInfo) {
            FileUploadInfo uploadInfo = (FileUploadInfo) taskInfo;
            if (nRspStatus == CloudGatewayFileTransferUtils.RENAME_STATUS_RSP_RENAME) {
                uploadInfo.fileName = strNewName;
            } else if (nRspStatus == CloudGatewayFileTransferUtils.RENAME_STATUS_RSP_REPLACE) {
                if (!uploadInfo.isDirectory) {
                    String selection = "_display_name=? and parent_cloud_id=?";
                    String[] selectionArgs = new String[]{uploadInfo.fileName, targetDirId};

                    final String FILE_ID = "source_media_id";
                    final int nTargetCloudDevId = getTargetDevice().getId();

                    try (Cursor cursor = mContext.getContentResolver().query(CloudGatewayMediaStore.CloudFiles.getCloudFileUri(nTargetCloudDevId), new String[]{"source_media_id"}, selection, selectionArgs, null)) {
                        if (cursor != null && cursor.moveToFirst()) {
                            String[] cloudIds = new String[cursor.getCount()];
                            int count = 0;
                            do {
                                cloudIds[count++] = cursor.getString(cursor.getColumnIndex(FILE_ID));
                            } while (cursor.moveToNext());
                            CloudGatewayFileBrowserUtils.getInstance(mContext).mrrControlBatchCommand(nTargetCloudDevId,CloudGatewayFileBrowserUtils.REMOTE_FOLDER_RMDIR_BATCH, cloudIds, null);
                        }
                    } catch (Exception e) {
                        Log.e(this, "processRenameResult() - Exception : " + e.getMessage());
                    }
                }
            } else if (nRspStatus == CloudGatewayFileTransferUtils.RENAME_STATUS_RSP_SKIP) {
                Log.i(this, "processRenameResult() - RENAME_STATUS_RSP_SKIP");
                uploadInfo.skipTransfer = true;
            }
        }
    }

    @Override
    protected boolean onExitMultiChannel() {
        boolean success = true;
        boolean bDelSuccess = true;
        for (FileUploadInfo fileUpload : filesToUpload) {
            File file = fileUpload.fileToUpload;
            if (getOptions().deleteSourceFilesWhenTransferIsComplete && !fileUpload.skipTransfer) {
                Log.d(this, "onExitMultiChannel() - delete: " + file);
                if (fileUpload.completed) {
                    // files tab (or video caption file)
                    bDelSuccess = FileUtils.deleteQuietly(file);
                }
                if (!bDelSuccess) {
                    success = false;
                }
            }

            if (isErrorOccurred()) {
                break;
            }
        }
        return success;
    }

    @Override
    protected void doThreadSafeTransfer(int nChannelId) throws Exception {
        CloudStorageSync.UploadResult result = new CloudStorageSync.UploadResult();
        result.mStatus = CloudStorageSync.UploadResult.Status.OK;
        Log.d(this, "doThreadSafeTransfer() Start");

        try {
            if (isErrorOccurred() || getTargetDevice() == null) {
                return;
            }

            //if capacityInBytes is 0, probably did not initialize the capacity yet - go ahead and try the transfer
            if (getTargetDevice().getCapacityInBytes() > 0 && getTargetDevice().getAvailableCapacityInBytes() < getTotalBytesToTransfer()) {
                Log.d(this, "doThreadSafeTransfer() Device Full");
                setErrorOccurred(true);
                result.mStatus = CloudStorageSync.UploadResult.Status.OUT_OF_SPACE;
                return;
            }

            DeviceSLPF mDevice = getTargetDevice();
            CloudStorageSync cloudStorageSync = mDevice.getCloudStorageSync();
            if (cloudStorageSync == null) {
                Log.e(this, "doTransfer() - Trying to upload to a device that is not found. [" + mDevice + "]");
                throw new IllegalStateException("doTransfer() - Device does not exist : " + mDevice);
            }

            int nItemCnt = 0;
            for (FileUploadInfo fileUpload : filesToUpload) {
                if (isCanceled()) {
                    Log.d(this, "doThreadSafeTransfer() canceled");
                    break;
                }
                nItemCnt++;
                if (!checkMultiChannelItem(nChannelId, nItemCnt)) {
                    continue;
                }
                if (fileUpload.completed) {
                    Log.i(this, "doThreadSafeTransfer() - fileUpload completed nItemCnt=" + nItemCnt);
                    continue;
                }
                File file = fileUpload.fileToUpload;
                if (file.exists() == false) {
                    Log.e(this, "doThreadSafeTransfer() - upload file doesn't exist! id: " + file.getName());
                    setErrorOccurred(true);
                    cancel();
                    return;
                }

                if (fileUpload.isDirectory || fileUpload.skipTransfer) {
                    Log.i(this, "doThreadSafeTransfer() - skipping file due to duplicate. file : " + file.getName());
                    fileUpload.skipped = fileUpload.completed = true;
                    bytesTransferred(fileUpload.fileToUpload.length());
                } else {
                    result = doTransfer(fileUpload);
                }
            }
        } finally {
            switch (result.mStatus) {
                case MAX_RETRY_FAIL:
                    Log.i(this, "doThreadSafeTransfer upload failed & retry to the max");
                    throw new IOException("retry to the max");
                case NO_NEED_RETRY:
                    Log.i(this, "doThreadSafeTransfer upload failed & no need retry");
                    throw new IOException(CloudStorageError.NO_NEED_RETRY_ERROR);
                case OUT_OF_SPACE:
                    Log.i(this, "doThreadSafeTransfer upload failed & out of space");
                    throw new IOException(CloudStorageError.OUT_OF_STORAGE_ERROR);
                case REACHED_MAX_ITEMS:
                    Log.d(this, "doThreadSafeTransfer upload failed & reach max items");
                    throw new IOException(CloudStorageError.OUT_OF_STORAGE_ERROR);
                case OTHER:
                    throw new IOException();
            }
        }
    }

    //added by shirley for persistent file transfer
    @Override
    public long getSourceMediaId(int index) {
        return 0;
    }

    @Override
    public int getSentFileNumForMultiChannelSending() {
        int cnt = 0;
        for (FileUploadInfo info : filesToUpload) {
            if (info.completed) {
                ++cnt;
            }
        }
        return cnt;
    }

    @Override
    protected String getTransferredFileList() {
        StringBuilder strRet = new StringBuilder();
        try {
            int nSize = filesToUpload.size();
            for (int nCnt = 0; nCnt < nSize; nCnt++) {
                FileUploadInfo fileUpload = filesToUpload.get(nCnt);
                File file = fileUpload.fileToUpload;
                strRet.append(file.getAbsolutePath());
                if (nCnt < nSize - 1)
                    strRet.append(";:;:");
            }
        } catch (Exception e) {
            Log.e(this, "getTransferredFileList() - Exception : " + e.getMessage());
        }
        return strRet.toString();
    }

    public CloudStorageSync.UploadResult doTransfer(FileUploadInfo uploadInfo) throws Exception {
        CloudStorageSync.UploadResult result = new CloudStorageSync.UploadResult();
        result.mStatus = CloudStorageSync.UploadResult.Status.OK;

        DeviceSLPF mDevice = getTargetDevice();
        String mTargetDirectoryPath = getOptions().targetDirectoryPath;

        CloudStorageSync cloudStorageSync = mDevice.getCloudStorageSync();
        if (cloudStorageSync == null) {
            Log.e(this, "doTransfer() - Trying to upload to a device that is not found. [" + mDevice + "]");
            throw new IllegalStateException("doTransfer() - Device does not exist : " + mDevice);
        }

        File tmpFileNew;
        boolean bNeedRemove;
        try {
// jsub12.lee, apply uploading with target directory, ~
            String targetDirId = null;
            if (uploadInfo.targetFolderID != null) {
                targetDirId = uploadInfo.targetFolderID;
            } else if (mTargetDirectoryPath != null) {
                targetDirId = mTargetDirectoryPath;
            }

            if (uploadInfo.fileName != null && uploadInfo.fileToUpload.getName().equals(uploadInfo.fileName) == false) {
                tmpFileNew = new File(getTargetPrivateDirectory(), uploadInfo.fileName);
                AspFileUtilsSLPF.copyFile(uploadInfo.fileToUpload, tmpFileNew);
                bNeedRemove = true;
            } else {
                tmpFileNew = uploadInfo.fileToUpload;
                bNeedRemove = false;
            }
            Log.i(this, "doTransfer() - fileToUpload.getName()=" + uploadInfo.fileToUpload.getName() + ", fileUploadInfo.fileName=" + uploadInfo.fileName);
            result = cloudStorageSync.uploadFile(targetDirId, tmpFileNew, uploadInfo.mimeType, this);
            if (bNeedRemove) {
                tmpFileNew.delete();
            }

            Log.d(this, "doTransfer() - result = " + result);
            switch (result.mStatus) {
                case OK:
                    Log.d(this, "doTransfer() - OK " + targetDirId);
                    if ((targetDirId != null) && targetDirId.equals(mTargetDirectoryPath))
                        CachedFileBrowser.deltaFileLayerAdd(mDevice, mContext, uploadInfo.fileName, targetDirId, result.mFileId, false);
                    NetResourceCacheManager.getInstance(mContext).deleteCache(CachedFileBrowser.URI_MATCHER + targetDirId, null, mDevice.getId());
                    break;
                case TOO_LARGE:
                    throw new StorageUploadFileTooLargeException();
                case MAX_RETRY_FAIL:
                    Log.d(this, "doTransfer() - doWebStorageUpload: upload failed & retry to the max : " + result);
                    throw new IOException("upload failed & retry to the max : " + result);
                case NO_NEED_RETRY:
                    Log.d(this, "doTransfer() - doWebStorageUpload: upload failed & no need retry : " + result);
                    throw new IOException(CloudStorageError.NO_NEED_RETRY_ERROR);
                case OUT_OF_SPACE:
                    Log.d(this, "doTransfer() - doWebStorageUpload: upload failed & out of space : " + result);
                    throw new IOException(CloudStorageError.OUT_OF_STORAGE_ERROR);
                case REACHED_MAX_ITEMS:
                    Log.d(this, "doTransfer() - doWebStorageUpload: upload failed & reach max item : " + result);
                    throw new IOException(CloudStorageError.REACH_MAX_ITEM_ERROR);
                default:
                    ////auto retry file transfer
                    Log.d(this, "doTransfer() - doWebStorageUpload: throw IOException and return " + result);
                    throw new IOException("web storage upload failed: " + result);
            }

            if (this != null) {
                this.onUploadComplete(uploadInfo);
            }
        } finally {
            return result;
        }
    }

    private File getTargetPrivateDirectory() {
        File tmpF = new File(mContext.getExternalFilesDir(null), ".tmp");
        if (!tmpF.mkdirs()) {
        }

        File noMediaF = new File(tmpF, ".nomedia");
        if (!noMediaF.exists()) {
            try {
                noMediaF.createNewFile();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                Log.e(this, "getTargetPrivateDirectory() - IOException : " + e.getMessage());
                e.printStackTrace();
            }
        }
        return tmpF;
    }

    public static class StorageUploadFileTooLargeException extends Exception {

        private static final long serialVersionUID = 8539180568086602460L;

    }

    @Override
    public void cancel() {
        super.cancel();

        DeviceSLPF targetDevice = getTargetDevice();
        if (targetDevice != null) {
            CloudStorageSync cloudStorageSync = targetDevice.getCloudStorageSync();
            cloudStorageSync.cancel(OperationType.UPLOAD);
        }
    }
}
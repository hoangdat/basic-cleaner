
package platform.com.mfluent.asp.sync;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import com.mfluent.log.Log;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.Collection;

import platform.com.mfluent.asp.datamodel.DataModelSLPF;
import platform.com.mfluent.asp.datamodel.DeviceSLPF;
import platform.com.mfluent.asp.framework.IASPApplication2;
import platform.com.mfluent.asp.framework.ServiceLocatorSLPF;

public abstract class DeviceMetadataSyncManager extends SingleThreadedSyncManagerSLPF {
    protected final DeviceSLPF device;

    public DeviceSLPF getDevice() {
        return device;
    }

    public DeviceMetadataSyncManager(Context context, DeviceSLPF device) {
        super(context);
        this.device = device;
    }

    @Override
    protected Collection<IntentFilter> getSyncIntentFilters() {
        Collection<IntentFilter> syncIntentFilters = super.getSyncIntentFilters();
        syncIntentFilters.add(new IntentFilter(DataModelSLPF.BROADCAST_REFRESH_ALL));
        syncIntentFilters.add(new IntentFilter(IASPApplication2.BROADCAST_GO_HOME));
        syncIntentFilters.add(device.buildDeviceIntentFilterForAction(DeviceSLPF.BROADCAST_DEVICE_REFRESH));

        return syncIntentFilters;
    }

    @Override
    protected final void doSync(Intent intent) {
        Log.v(this, "doSync called @" + this);
        if (shouldSync(intent) == false) {
            return;
        }

        getDevice().setIsSyncing(true);
        getDevice().setIsSynchronized(false); /* Auto_Archive_by_TF */
        getDevice().commitChanges();

        try {
            doSyncInner(intent);
        } finally {
            getDevice().setIsSyncing(false);
            getDevice().setIsSynchronized(true); /* Auto_Archive_by_TF */
            getDevice().commitChanges();

//			if ((getDevice().getDeviceTransportType() == CloudGatewayDeviceTransportType.WEB_STORAGE) && getDevice().isWebStorageSignedIn()) {
//				AspCommonUtils.notifyInitialSyncDeviceToSFinder(getDevice());
//			}
        }
    }

    protected abstract boolean shouldSync(Intent intent);

    protected abstract void doSyncInner(Intent intent);

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("device", device).toString();
    }

    protected void startPrefetchService() {
        Context context = ServiceLocatorSLPF.get(IASPApplication2.class);
        Intent intent = new Intent(ASPThumbnailPrefetcherService.ACTION_PREFETCH_DEVICE, null, context, ASPThumbnailPrefetcherService.class);
        intent.putExtra(ASPThumbnailPrefetcherService.EXTRA_DEVICE_ID, getDevice().getId());

        context.startService(intent);
    }
}

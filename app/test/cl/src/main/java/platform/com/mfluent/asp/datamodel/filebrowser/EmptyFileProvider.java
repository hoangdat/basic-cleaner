
package platform.com.mfluent.asp.datamodel.filebrowser;

import android.content.Context;

import com.mfluent.asp.common.datamodel.ASPFile;
import com.mfluent.asp.common.datamodel.ASPFileBrowser;
import com.mfluent.asp.common.datamodel.ASPFileProvider;
import com.mfluent.asp.common.datamodel.ASPFileSortType;
import com.mfluent.asp.common.datamodel.CloudDevice;

import java.io.FileNotFoundException;
import java.io.IOException;

import platform.com.mfluent.asp.datamodel.DeviceSLPF;

public class EmptyFileProvider implements ASPFileProvider {

    private final Context mContext;
    private final DeviceSLPF mDevice;

    public EmptyFileProvider(Context context, DeviceSLPF device) {
        this.mContext = context;
        this.mDevice = device;
    }

    @Override
    public Context getApplicationContext() {
        return this.mContext;
    }

    @Override
    public CloudDevice getCloudDevice() {
        return this.mDevice;
    }

    @Override
    public String getStorageGatewayFileId(ASPFile file) throws FileNotFoundException {
        return "";
    }

    @Override
    public ASPFileBrowser<?> getCloudStorageFileBrowser(String storageGatewayID, ASPFileSortType sortType, boolean forceReload, boolean isBrowserForView)
            throws InterruptedException, IOException {
        return new EmptyFileBrowserSLPF();
    }

    @Override
    public int deleteFiles(String directoryStorageGatewayId, ASPFileSortType sortType, String... storageGatewayFileIdsToDelete) {
        return 0;
    }

    //jsub12_151006
    @Override
    public int getCountOfChild(ASPFile file_param) {
        return 0;
    }

    //jsub12_151006
    @Override
    public int getCountOfChildDir(ASPFile file_param) {
        return 0;
    }

    @Override
    public int getCountOfDescendants(ASPFile var1) {
        return 0;
    }

    @Override
    public int getCountOfDescendantDir(ASPFile var1) {
        return 0;
    }

    @Override
    public String getTrashProcessingDir(ASPFile file) {
        return null;
    }
}


package platform.com.mfluent.asp.framework;

import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.support.v4.content.LocalBroadcastManager;

import com.mfluent.asp.common.util.CursorUtils;
import com.mfluent.log.Log;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class StorageProviderDatabaseHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 16;

    private static final String DATABASE_NAME = "sp.db";

    private static final String TABLE_STORAGE_PROVIDER = "providers";

    private static final String TABLE_META = "meta";

    private static final String META_KEY = "key";

    private static final String META_VALUE = "value";

    // Common table column names
    private static final String KEY_ID = "id", NAME_KEY = "nameKey", SPNAME = "spName", DISPLAY_ORDER = "displayOrder", TYPE_KEY = "typeKey",
            IS_OAUTH = "isOAuth", LOGIN_STATUS = "loginStatus", MAIN = "main",
            SUPPORTS_SIGNUP = "supportsSignup", SORT_KEY = "sort_key",
            IS_FORBIDDEN = "is_forbidden", MAX_NUM_TX_CONN = "max_num_tx_conn", OAUTH_WEBVIEW_JS = "oAuthWebViewJS";

    //private final File databaseFile;

    private static final String[] PROVIDER_SELECT_COLUMNS = new String[]{
            KEY_ID,
            NAME_KEY,
            SPNAME,
            IS_OAUTH,
            LOGIN_STATUS,
            MAIN,
            SUPPORTS_SIGNUP,
            SORT_KEY,
            IS_FORBIDDEN,
            MAX_NUM_TX_CONN,
            OAUTH_WEBVIEW_JS};

    private static StorageProviderDatabaseHelper sInstance;

    private final BroadcastReceiver masterResetBroadcastReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.i(this, "masterReset");
            reset();
        }
    };

    public static synchronized StorageProviderDatabaseHelper getInstance(Context context) {
        if (context == null) {
            throw new NullPointerException("context is null");
        }
        if (sInstance == null) {
            sInstance = new StorageProviderDatabaseHelper(context);
        }
        return sInstance;
    }

    private StorageProviderDatabaseHelper(Context context) {
        super(context.getApplicationContext(), DATABASE_NAME, null, DATABASE_VERSION);
        //databaseFile = context.getDatabasePath(DATABASE_NAME);

        LocalBroadcastManager.getInstance(context).registerReceiver(
                masterResetBroadcastReceiver,
                new IntentFilter(IASPApplication2.BROADCAST_MASTER_RESET));
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older tables if they exist
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_STORAGE_PROVIDER);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_META);
        onCreate(db);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        // Build Folders Table
        String CREATE_FOLDERS_TABLE = "CREATE TABLE "
                + TABLE_STORAGE_PROVIDER
                + "("
                + KEY_ID
                + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + NAME_KEY
                + " TEXT,"
                + SPNAME
                + " TEXT,"
                + DISPLAY_ORDER
                + " INTEGER, "
                + TYPE_KEY
                + " TEXT,"
                + IS_OAUTH
                + " INTEGER,"
                + LOGIN_STATUS
                + " INTEGER,"

                + MAIN
                + " TEXT,"

                + SUPPORTS_SIGNUP
                + " INTEGER,"
                + SORT_KEY
                + " INTEGER,"
                + IS_FORBIDDEN
                + " INTEGER,"

                + MAX_NUM_TX_CONN
                + " INTEGER,"
                + OAUTH_WEBVIEW_JS
                + " TEXT,"

                + " UNIQUE ("
                + NAME_KEY
                + ")"
                + ")";
        db.execSQL(CREATE_FOLDERS_TABLE);

        // Build Meta Table
        String CREATE_META_TABLE = "CREATE TABLE "
                + TABLE_META
                + "("
                + KEY_ID
                + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + META_KEY
                + " TEXT,"
                + META_VALUE
                + " TEXT"
                + ")";
        db.execSQL(CREATE_META_TABLE);
    }

    public void reset() {
        SQLiteDatabase db = getWritableDatabase();

        db.execSQL("DELETE FROM " + TABLE_STORAGE_PROVIDER);
        db.execSQL("DELETE FROM " + TABLE_META);
    }

    private void storageInfoToColumnBuilder(StorageProviderInfo storageProviderInfo, ContentValues values) {

        String name = storageProviderInfo.getName();
        values.put(NAME_KEY, name);
        values.put(SPNAME, storageProviderInfo.getSpName());
        values.put(TYPE_KEY, storageProviderInfo.getType());
        values.put(IS_OAUTH, storageProviderInfo.isOAuth());
        boolean loginStatus = storageProviderInfo.isLoginStatus();
        values.put(LOGIN_STATUS, loginStatus);

        values.put(MAIN, storageProviderInfo.getMain());

        values.put(SUPPORTS_SIGNUP, storageProviderInfo.getSupportSignUp());
        values.put(SORT_KEY, storageProviderInfo.getSortKey());
        values.put(IS_FORBIDDEN, storageProviderInfo.getForbidden());

        values.put(MAX_NUM_TX_CONN, storageProviderInfo.getMaxNumTxConn());
        values.put(OAUTH_WEBVIEW_JS, storageProviderInfo.getOAuthWebViewJS());

    }

    private void mapCursorToObject(Cursor cursor, StorageProviderInfo spInfo) {

        spInfo.setId(cursor.getInt(cursor.getColumnIndex(KEY_ID)));
        spInfo.setName(cursor.getString(cursor.getColumnIndex(NAME_KEY)));
        spInfo.setSpName(cursor.getString(cursor.getColumnIndex(SPNAME)));

        if (cursor.getInt(cursor.getColumnIndex(IS_OAUTH)) != 0) {
            spInfo.setOAuth(true);
        } else {
            spInfo.setOAuth(false);
        }

        if (cursor.getInt(cursor.getColumnIndex(LOGIN_STATUS)) != 0) {
            spInfo.setLoginStatus(true);
        } else {
            spInfo.setLoginStatus(false);
        }

        spInfo.setMain(cursor.getString(cursor.getColumnIndex(MAIN)));
        spInfo.setSortKey(cursor.getInt(cursor.getColumnIndex(SORT_KEY)));
        spInfo.setForbidden(CursorUtils.getBoolean(cursor, IS_FORBIDDEN));

        spInfo.setMaxNumTxConn(CursorUtils.getInt(cursor, MAX_NUM_TX_CONN));
        spInfo.setOAuthWebViewJS(CursorUtils.getString(cursor, OAUTH_WEBVIEW_JS));

        if (cursor.getInt(cursor.getColumnIndex(SUPPORTS_SIGNUP)) != 0) {
            spInfo.setSupportsSignUp(true);
        } else {
            spInfo.setSupportsSignUp(false);
        }
    }

    public synchronized Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(TABLE_STORAGE_PROVIDER, projection, selection, selectionArgs, null, null, sortOrder, null);

        return cursor;
    }

    public synchronized StorageProviderInfo get(String name) {
        if (StringUtils.isEmpty(name)) {
            return null;
        }
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.query(TABLE_STORAGE_PROVIDER, PROVIDER_SELECT_COLUMNS, SPNAME + " = ?", new String[]{name}, null, null, null, null);

        StorageProviderInfo provider = null;
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                provider = new StorageProviderInfo();
                mapCursorToObject(cursor, provider);
            }
            cursor.close();
        }

        return provider;
    }

    public synchronized List<StorageProviderInfo> findAllStorageProviders() {
        List<StorageProviderInfo> storageProviders = new ArrayList<StorageProviderInfo>();
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_STORAGE_PROVIDER, PROVIDER_SELECT_COLUMNS, null, null, null, null, null, null);

        if (cursor != null) {
            while (cursor.moveToNext()) {
                StorageProviderInfo spInfo = new StorageProviderInfo();
                mapCursorToObject(cursor, spInfo);
                storageProviders.add(spInfo);

                Log.d(this, "findAllStorageProviders() - getting all storage: " + spInfo);
            }
            cursor.close();
        }

        return storageProviders;
    }

    public synchronized void add(StorageProviderInfo storageProviderInfo) {

        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        storageInfoToColumnBuilder(storageProviderInfo, values);

        // Inserting Row
        long result = db.insert(TABLE_STORAGE_PROVIDER, null, values);
        if (result > 0) {
            storageProviderInfo.setId((int) result);
        }

    }

    public synchronized StorageProviderInfo update(StorageProviderInfo storageProviderInfo) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        storageInfoToColumnBuilder(storageProviderInfo, values);
        try {
            db.update(TABLE_STORAGE_PROVIDER, values, KEY_ID + " = ?", new String[]{Integer.toString(storageProviderInfo.getId())});
        } catch (NullPointerException e) {
            Log.e(this, "update() - NullPointerException : " + e.getMessage());
        }

        return storageProviderInfo;
    }

    public synchronized void saveOrUpdate(StorageProviderInfo storageProviderInfo) {

        if (storageProviderInfo == null) {
            throw new IllegalStateException();
        }

        if (storageProviderInfo.getId() == 0) {
            add(storageProviderInfo);
        } else {
            update(storageProviderInfo);
        }
    }

    private synchronized void deleteMeta(String key) {
        SQLiteDatabase db = getWritableDatabase();
        db.delete(TABLE_META, META_KEY + " = ?", new String[]{key});
    }

    public synchronized void saveOrUpdateMetaData(String key, String value) {
        if (value == null) {
            deleteMeta(key);
            return;
        }

        SQLiteDatabase db = getWritableDatabase();
        Cursor cursor = db.query(TABLE_META, new String[]{META_VALUE}, META_KEY + " = ?", new String[]{key}, null, null, null, null);
        try {
            ContentValues values = new ContentValues();
            values.put(META_VALUE, value);

            if (cursor == null || cursor.getCount() == 0) {
                // key did not exist so insert
                values.put(META_KEY, key);
                db.insert(TABLE_META, null, values);
            } else {
                // key already exists so update
                db.update(TABLE_META, values, META_KEY + " = ?", new String[]{String.valueOf(key)});
            }
        } finally {
            cusorCloseHelper(cursor);
        }
    }

    private void cusorCloseHelper(Cursor cursor) {
        try {
            if (cursor != null && cursor.isClosed() == false) {
                cursor.close();
            }
        } catch (Exception e) {
            Log.e(this, "cusorCloseHelper() - failed to close cursor becuase Exception : " + e.getMessage());
        }
    }
}


package platform.com.mfluent.asp.datamodel.filebrowser;

import android.content.ContentResolver;
import android.database.ContentObserver;
import android.database.DataSetObserver;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;

import com.mfluent.asp.common.datamodel.ASPFileBrowser;
import com.mfluent.asp.common.datamodel.ASPFileProvider;
import com.mfluent.asp.common.datamodel.ASPFileSortType;
import com.mfluent.asp.common.datamodel.CloudDevice;
import com.mfluent.log.Log;
import com.samsung.android.sdk.slinkcloud.CloudGatewayMediaStore;
import com.samsung.android.sdk.slinkcloud.CloudGatewayMediaStore.FileBrowser;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.IOException;
import java.util.HashMap;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class ASPFileBrowserManager {

    private static final int STATE_NONE = 0;
    private static final int STATE_LOADING = 1;
    private static final int STATE_LOADED = 2;

    public static class ASPFileBrowserReference {

        private final ASPFileBrowser<?> mFileBrowser;
        private int mRefCount = 0;

        public ASPFileBrowserReference(ASPFileBrowser<?> fileBrowser) {
            mFileBrowser = fileBrowser;
            mRefCount = 1;
        }

        public ASPFileBrowser<?> getFileBrowser() {
            return mFileBrowser;
        }

        private synchronized void acquire() {
            mRefCount++;
        }

        private synchronized void release() {
            if (mRefCount > 1) {
                mRefCount--;
            } else if (mRefCount == 1) {
                mRefCount = 0;
                if (mFileBrowser != null) {
                    mFileBrowser.destroy();
                }
            }
        }
    }

    private static class MapKey {

        public final long mDeviceId;
        public final String mDirectoryId;
        public final ASPFileSortType mSortType;
        public final String mSelection;
        private int mHashCode = 0;
        ////zero project myfiles
        public boolean isNonBlockingBrowser = false;

        public MapKey(long deviceId, String directoryId, ASPFileSortType sortType, String selection) {
            mDeviceId = deviceId;
            mDirectoryId = directoryId;
            mSortType = sortType;
            mSelection = selection;
        }

        ////zero project myfiles
        public void setNonBlockingBrowser() {
            isNonBlockingBrowser = true;
        }

        @Override
        public boolean equals(Object o) {
            boolean bRet = false;
            if (o != null) {
                MapKey other = (MapKey) o;
                ////zero project myfiles
                bRet = ((mDeviceId == other.mDeviceId)
                        && (mSortType == other.mSortType)
                        && (StringUtils.equals(mDirectoryId, other.mDirectoryId))
                        && isNonBlockingBrowser == other.isNonBlockingBrowser
                        && (StringUtils.equals(mSelection, other.mSelection)));
            }

            return bRet;
        }

        @Override
        public int hashCode() {
            if (mHashCode == 0) {
                HashCodeBuilder builder = new HashCodeBuilder();
                builder.append(mDeviceId);
                builder.append(mDirectoryId);
                builder.append(mSortType);
                ////zero project myfiles
                builder.append(isNonBlockingBrowser);
                mHashCode = builder.toHashCode();
            }

            return mHashCode;
        }
    }

    private static class MapValue {

        public int mState = STATE_NONE;
        public final Condition mCondition;
        private ASPFileBrowserReference mFileBrowserRef;
        private final DataSetObserver mDataSetObserver;
        private final ContentObserver mContentObserver;
        private final ContentResolver mContentResolver;
        private Uri[] mUris = null;
        public int mRefCount = 1;
        public boolean mForceRefresh = true;

        public MapValue(ContentResolver contentResolver, Condition condition, Handler handler) {
            mCondition = condition;
            mContentResolver = contentResolver;

            mContentObserver = new ContentObserver(handler) {

                @Override
                public void onChange(boolean selfChange) {
                    mForceRefresh = true;
                }
            };

            mDataSetObserver = new DataSetObserver() {

                @Override
                public void onChanged() {
                    doUriNotifications();
                }

                @Override
                public void onInvalidated() {
                    doUriNotifications();
                }
            };
        }

        private void doUriNotifications() {
            if (mUris != null) {
                for (Uri uri : mUris) {
                    Log.i(this, "doUriNotifications() - uri=" + uri);
                    mContentResolver.notifyChange(uri, null);
                }
            }
        }

        public void setFileBrowser(ASPFileBrowserReference fileBrowserRef, Uri[] uris) {
            if (fileBrowserRef != mFileBrowserRef) {
                if (mFileBrowserRef != null) {
                    clearFileBrowser();
                }
                mFileBrowserRef = fileBrowserRef;
                if (fileBrowserRef != null) {
                    mFileBrowserRef.getFileBrowser().registerDataSetObserver(mDataSetObserver);
                    mUris = uris;
                    if (uris != null && uris[0] != null) {
                        mContentResolver.registerContentObserver(uris[0], false, mContentObserver);
                    }
                }
            }
        }

        public void clearFileBrowser() {
            if (mFileBrowserRef != null) {
                //this.fileBrowserRef.getFileBrowser().unregisterDataSetObserver(this.dataSetObserver);
                mFileBrowserRef = null;
                mContentResolver.unregisterContentObserver(mContentObserver);
            }
            mUris = null;
            mRefCount = 0;
            mState = STATE_NONE;
        }
    }

    private static class InstanceHolder {

        public static final ASPFileBrowserManager sInstance = new ASPFileBrowserManager();
    }

    public static ASPFileBrowserManager getInstance() {
        return InstanceHolder.sInstance;
    }

    private final HashMap<MapKey, MapValue> mFileBrowserMap;
    private final ReentrantLock mLock;
    private final Handler mHandler;

    private ASPFileBrowserManager() {
        mFileBrowserMap = new HashMap<MapKey, MapValue>();
        mLock = new ReentrantLock();
        mHandler = new Handler(Looper.getMainLooper());
    }

    public static Uri[] getUriArray(String storageFileId, long deviceId, ASPFileProvider provider, ASPFileBrowserReference result) {
        Uri[] uris = null;
        try {
            if (StringUtils.isEmpty(storageFileId)) {
                uris = new Uri[2];
                uris[1] = FileBrowser.FileList2.getDefaultFileListUri(deviceId);
                if (result != null && provider != null) {
                    uris[0] = FileBrowser.FileList2.getFileListUri(
                            deviceId,
                            provider.getStorageGatewayFileId(result.getFileBrowser().getCurrentDirectory()));
                } else
                    uris[0] = null;
            } else if (CloudGatewayMediaStore.FileBrowser.ROOT_DIRECTORY_ID.equals(storageFileId)) {
                uris = new Uri[2];
                uris[1] = FileBrowser.FileList2.getFileListUri(deviceId, storageFileId);
                if (result != null && provider != null) {
                    uris[0] = FileBrowser.FileList2.getFileListUri(
                            deviceId,
                            provider.getStorageGatewayFileId(result.getFileBrowser().getCurrentDirectory()));
                } else
                    uris[0] = null;
            } else {
                uris = new Uri[1];
                uris[0] = FileBrowser.FileList2.getFileListUri(deviceId, storageFileId);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return uris;
    }

    ////zero project myfiles
    public ASPFileBrowserReference getLoadedFileBrowser3(
            ASPFileProvider provider,
            String storageFileId,
            ASPFileSortType sortType,
            String selection,
            String[] selectionArgs,
            String strOrgSortOption,
            boolean isBrowserForView) throws InterruptedException, IOException {
        ASPFileBrowserReference result = null;
        CloudDevice device = provider.getCloudDevice();

        if (device == null) {
            Log.e(this, "getLoadedFileBrowser3() - device is null");
            return null;
        }
        final long deviceId = device.getId();
        Log.i(this, "getLoadedFileBrowser3() - getID=" + deviceId + ",storageFileId=" + storageFileId + ",sortType=" + sortType);
        String strKeySel = selection;
        if (strKeySel != null && selectionArgs != null && selectionArgs.length > 0) {
            strKeySel += "&" + selectionArgs[0];
        }
        final MapKey searchKey = new MapKey(deviceId, storageFileId, sortType, strKeySel);

        ////zero project myfiles, choose between ASP10FileBrowser (old ui app) or ASP20FileBrowser (myfiles2)
        searchKey.setNonBlockingBrowser();

        MapValue valueToLoad = null;
        mLock.lockInterruptibly();
        try {
            MapValue value = mFileBrowserMap.get(searchKey);
            if (value != null) {
                value.mRefCount++;
                while (value.mState == STATE_LOADING) {
                    ////20 sec json timeout for ASP20FileBrowser
                    ////120 sec is enough
                    value.mCondition.awaitNanos(120000000);
                }
                result = value.mFileBrowserRef;
                if (result == null || value.mForceRefresh) {
                    valueToLoad = value;
                }
            } else {
                valueToLoad = new MapValue(provider.getApplicationContext().getContentResolver(), mLock.newCondition(), mHandler);
                mFileBrowserMap.put(searchKey, valueToLoad);
            }
            if (valueToLoad != null) {
                valueToLoad.mState = STATE_LOADING;
            }
        } finally {
            mLock.unlock();
        }
        Log.d(this, "getLoadedFileBrowser3 - result : " + result + ", valueToLoad : " + valueToLoad + " , status : " + ((valueToLoad != null) ? valueToLoad.mState : null));
        if (valueToLoad != null) {
            try {
                result = createAndLoadASPFileBrowser(provider, storageFileId, sortType, strOrgSortOption, selection, selectionArgs, isBrowserForView);
                //} catch (Exception e) {	// exception should not be caught until the ASPMediaStoreProvider
                //e.printStackTrace();
            } finally {
                mLock.lockInterruptibly();
                try {
                    Uri[] uris = getUriArray(storageFileId, deviceId, provider, result);
                    valueToLoad.setFileBrowser(result, uris);
                    valueToLoad.mState = (result != null) ? STATE_LOADED : STATE_NONE;
                } catch (Exception e2) {
                    Log.e(this, "getLoadedFileBrowser3() - error during setURI in fileBrowser Exception : " + e2.getMessage());
                } finally {
                    valueToLoad.mCondition.signalAll();
                    mLock.unlock();
                }
            }
        } else if (result != null) {
            result.acquire();
        }

        return result;
    }

    public ASPFileBrowserReference getLoadedFileBrowser2(
            ASPFileProvider provider,
            String storageFileId,
            ASPFileSortType sortType,
            String strOrgSortOption) throws InterruptedException, IOException {
        return getLoadedFileBrowser3(provider, storageFileId, sortType, null, null, strOrgSortOption, false);
    }

    public void releaseLoadedFileBrowser(ASPFileBrowserReference fileBrowserRef, long deviceId, String storageFileId, ASPFileSortType sortType, String selection) {
        final MapKey searchKey = new MapKey(deviceId, storageFileId, sortType, selection);
        if (fileBrowserRef != null) {
            ASPFileBrowser<?> objRet = fileBrowserRef.getFileBrowser();
            if (objRet != null && objRet.getClass() == CachedFileBrowser.class) {
                searchKey.setNonBlockingBrowser();
            }
            fileBrowserRef.release();
        }

        //Log.i("INFO", "mFileBrowserMap try rm key=" + searchKey);
        mLock.lock();
        try {
            MapValue found = mFileBrowserMap.get(searchKey);
            if (found != null) {
                if (found.mRefCount > 1) {
                    found.mRefCount--;
                } else if (found.mRefCount == 1) {
                    found.clearFileBrowser();
                    found.mRefCount = 0;
                    found.mState = STATE_NONE;
                    found.mCondition.signalAll();
                    mFileBrowserMap.remove(searchKey);
                }
                Log.i(this, "mFileBrowserMap.check refCount=" + found.mRefCount + ", fbrowserSize=" + mFileBrowserMap.size());
            }
        } finally {
            mLock.unlock();
        }
    }

    private ASPFileBrowserReference createAndLoadASPFileBrowser(ASPFileProvider provider, String storageFileId, ASPFileSortType sortType, String strOrgSortOption, String selection, String[] selectionArgs, boolean isBrowserForView)
            throws InterruptedException, IOException {
        ASPFileBrowserReference result = null;

        if (provider != null) {
            ASPFileBrowser<?> browser;
            if (provider instanceof CachedFileProvider) {
                CachedFileProvider tmpProvider = (CachedFileProvider) provider;
                browser = tmpProvider.getCloudStorageFileBrowser2(storageFileId, sortType, true, strOrgSortOption, selection, selectionArgs, isBrowserForView);
            } else {
                browser = provider.getCloudStorageFileBrowser(storageFileId, sortType, true, isBrowserForView);
            }
            if (browser != null) {
                result = new ASPFileBrowserReference(browser);
                if (browser.getCount() > 0)
                    browser.getFile(browser.getCount() - 1);
            }
        }

        return result;
    }
}


package platform.com.mfluent.asp.datamodel;

import android.database.Cursor;

public abstract class PublicMediaInfo extends MediaInfo {

    protected abstract String getPrivateQueryTableName();

    @Override
    public String getQueryTableName(MediaInfoContext context) {
        //		if (this.isPublicApi() && context.provider.isExternalBinder() && !context.provider.hasPrivateApiPermission()) {
        //			return getPublicQueryTableName();
        //		} else {
        return getPrivateQueryTableName();
        //		}
    }

    @Override
    public Cursor handleQuery(
            MediaInfoContext mediaInfoContext,
            String[] projection,
            String where,
            String[] whereArgs,
            String sortBy,
            String groupBy,
            String having,
            String limit,
            long entryId) {

        String tableName = getQueryTableName(mediaInfoContext);
        return mediaInfoContext.db.query(tableName, projection, where, whereArgs, groupBy, having, sortBy, limit);
    }
}


package platform.com.mfluent.asp.datamodel;

import android.content.ContentResolver;
import android.database.CharArrayBuffer;
import android.database.ContentObserver;
import android.database.CrossProcessCursor;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.database.CursorWindow;
import android.database.DataSetObserver;
import android.net.Uri;
import android.os.Bundle;
import android.provider.BaseColumns;

public class ReadLockCursor implements CrossProcessCursor {

	private static final String[] COLUMNS = {BaseColumns._ID};

	private final ASPMediaStoreProvider mProvider;
	private Long mLockToken;
	private boolean mClosed = false;

	public ReadLockCursor(ASPMediaStoreProvider provider) {
		this.mProvider = provider;
		this.mLockToken = provider.readLock();
	}

	@Override
	public int getCount() {
		return 0;
	}

	@Override
	public int getPosition() {
		return -1;
	}

	@Override
	public boolean move(int offset) {
		return false;
	}

	@Override
	public boolean moveToPosition(int position) {
		return false;
	}

	@Override
	public boolean moveToFirst() {
		return false;
	}

	@Override
	public boolean moveToLast() {
		return false;
	}

	@Override
	public boolean moveToNext() {
		return false;
	}

	@Override
	public boolean moveToPrevious() {
		return false;
	}

	@Override
	public boolean isFirst() {
		return false;
	}

	@Override
	public boolean isLast() {
		return false;
	}

	@Override
	public boolean isBeforeFirst() {
		return true;
	}

	@Override
	public boolean isAfterLast() {
		return false;
	}

	@Override
	public int getColumnIndex(String columnName) {
		return -1;
	}

	@Override
	public int getColumnIndexOrThrow(String columnName) throws IllegalArgumentException {
		if (BaseColumns._ID.equals(columnName)) {
			return 0;
		}
		throw new IllegalArgumentException();
	}

	@Override
	public String getColumnName(int columnIndex) {
		if (columnIndex == 0) {
			return BaseColumns._ID;
		}
		throw new ArrayIndexOutOfBoundsException();
	}

	@Override
	public String[] getColumnNames() {
		return COLUMNS;
	}

	@Override
	public int getColumnCount() {
		return 1;
	}

	@Override
	public byte[] getBlob(int columnIndex) {
		throwNotOnRecord();

		return null;
	}

	@Override
	public String getString(int columnIndex) {
		throwNotOnRecord();

		return null;
	}

	@Override
	public void copyStringToBuffer(int columnIndex, CharArrayBuffer buffer) {
		throwNotOnRecord();

	}

	@Override
	public short getShort(int columnIndex) {
		throwNotOnRecord();

		return 0;
	}

	@Override
	public int getInt(int columnIndex) {
		throwNotOnRecord();

		return 0;
	}

	@Override
	public long getLong(int columnIndex) {
		throwNotOnRecord();

		return 0;
	}

	@Override
	public float getFloat(int columnIndex) {
		throwNotOnRecord();

		return 0.0f;
	}

	@Override
	public double getDouble(int columnIndex) {
		throwNotOnRecord();

		return 0.0f;
	}

	@Override
	public int getType(int columnIndex) {
		if (columnIndex == 0) {
			return Cursor.FIELD_TYPE_INTEGER;
		}

		return Cursor.FIELD_TYPE_NULL;
	}

	@Override
	public boolean isNull(int columnIndex) {
		throwNotOnRecord();

		return true;
	}

	@Override
	public void deactivate() {
		releaseLock();
	}

	private void throwNotOnRecord() {
		throw new CursorIndexOutOfBoundsException(-1, 0);
	}

	private void releaseLock() {
		if (mLockToken != null) {
			mProvider.readUnlock(mLockToken);
			mLockToken = null;
		}
	}

	@Override
	@Deprecated
	public boolean requery() {
		boolean result = false;
		if (!mClosed) {
			result = true;
			if (mLockToken == null) {
				mLockToken = mProvider.readLock();
			}
		}

		return result;
	}

	@Override
	public void close() {
		releaseLock();
		mClosed = true;
	}

	@Override
	public boolean isClosed() {
		return mClosed;
	}

	@Override
	public void registerContentObserver(ContentObserver observer) {

	}

	@Override
	public void unregisterContentObserver(ContentObserver observer) {

	}

	@Override
	public void registerDataSetObserver(DataSetObserver observer) {

	}

	@Override
	public void unregisterDataSetObserver(DataSetObserver observer) {

	}

	@Override
	public void setNotificationUri(ContentResolver cr, Uri uri) {

	}

	@Override
	public Uri getNotificationUri() {
		return null;
	}

	@Override
	public boolean getWantsAllOnMoveCalls() {
		return false;
	}

    @Override
    public void setExtras(Bundle bundle) {

    }


    @Override
	public Bundle getExtras() {
		return null;
	}

	@Override
	public Bundle respond(Bundle extras) {
		return null;
	}

	@Override
	public CursorWindow getWindow() {
		return null;
	}

	@Override
	public void fillWindow(int position, CursorWindow window) {

	}

	@Override
	public boolean onMove(int oldPosition, int newPosition) {
		return false;
	}

}

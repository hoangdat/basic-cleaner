/**
 *
 */

package platform.com.mfluent.asp.datamodel;

import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.support.v4.content.LocalBroadcastManager;
import android.util.SparseArray;

import com.mfluent.asp.common.datamodel.ASPMediaStore;
import com.mfluent.asp.common.datamodel.CloudDataModel;
import com.mfluent.asp.common.datamodel.CloudDevice;
import com.mfluent.log.Log;
import com.samsung.android.sdk.slinkcloud.CloudGatewayDeviceTransportType;
import com.samsung.android.sdk.slinkcloud.CloudGatewayMediaStore;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.locks.ReentrantLock;

import platform.com.mfluent.asp.framework.IASPApplication2;
import platform.com.mfluent.asp.util.BroadcastReceiverManager;
import platform.com.mfluent.asp.util.CollectionUtils;
import platform.com.mfluent.asp.util.Predicate;
import uicommon.com.mfluent.asp.util.CachedExecutorService;

/**
 * The root of the application's data model.
 *
 * @author Ilan Klinghofer
 */
public class DataModelSLPF implements CloudDataModel {

    public static final String BROADCAST_DEVICE_LIST_CHANGE = "com.mfluent.asp.DataModel.DEVICE_LIST_CHANGE.SLPF_CLOUD";

    public static final String BROADCAST_REFRESH_ALL = "com.mfluent.asp.DataModel.REFRESH_ALL.SLPF_CLOUD";

    private static DataModelSLPF sInstance = null;

    private final ASPMediaStoreProvider mProvider;

    private final LocalBroadcastManager localBraodcastManager;

    private final ReentrantLock devicesLock = new ReentrantLock();

    private final SparseArray<DeviceSLPF> devices = new SparseArray<DeviceSLPF>();

    private DeviceSLPF localDevice;

    private final BroadcastReceiver masterResetBroadcastReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.i(this, "masterReset");

            ContentResolver cr = context.getContentResolver();
            Set<DeviceSLPF> devices = new HashSet<DeviceSLPF>(getDevices());
            for (DeviceSLPF device : devices) {
                if (!device.isLocalDevice()) {
                    try {
                        device.delete();
                    } catch (Exception e) {
                        Log.e(this, "masterResetBroadcastReceiver/onReceive - Trouble removing device " + device + ", Exception : " + e);
                    }
                } else {
                    StringBuilder sbWhere = new StringBuilder();
                    sbWhere.append(ASPMediaStore.Images.ImageColumns.LATITUDE)
                           .append(" IS NULL OR ")
                           .append(ASPMediaStore.Images.ImageColumns.LONGITUDE)
                           .append(" IS NULL");

                    Log.d(this, "onReceive masterReset where: " + sbWhere.toString());

                    cr.delete(ASPMediaStore.Files.getContentUriForDevice(device.getId()), sbWhere.toString(), null);
                }
            }

            cr.delete(ASPMediaStore.Images.Journal.CONTENT_URI, null, null);
            cr.delete(ASPMediaStore.Video.Journal.CONTENT_URI, null, null);
            cr.delete(ASPMediaStore.Audio.Journal.CONTENT_URI, null, null);
            cr.delete(ASPMediaStore.Documents.Journal.CONTENT_URI, null, null);

            cr.delete(ASPMediaStore.Files.Keywords.getOrphanCleanUriForDevice(0), null, null);
            cr.delete(ASPMediaStore.Audio.Artists.getOrphanCleanUriForDevice(0), null, null);
            cr.delete(ASPMediaStore.Audio.Albums.getOrphanCleanUriForDevice(0), null, null);
        }
    };

    public static DataModelSLPF getInstance() {
        return sInstance;
    }

    static void initInstance(ASPMediaStoreProvider provider) {
        if (sInstance == null) {
            sInstance = new DataModelSLPF(provider);
            sInstance.initializeFromDB();
        }
    }

    public Context getContext() {
        return mProvider.getContext();
    }

    /**
     * Retrieves the set of all devices registered with this Samsung account
     *
     * @return all devices
     */
    public Set<DeviceSLPF> getDevices() {
        devicesLock.lock();
        try {
            HashSet<DeviceSLPF> set = new HashSet<DeviceSLPF>();
            for (int i = 0; i < devices.size(); i++) {
                DeviceSLPF device = devices.valueAt(i);
                set.add(device);
            }
            return set;
        } finally {
            devicesLock.unlock();
        }
    }

    public List<DeviceSLPF> getDevicesByType(CloudGatewayDeviceTransportType type) {
        ArrayList<DeviceSLPF> devicesWithType = new ArrayList<DeviceSLPF>();
        devicesLock.lock();
        try {
            for (int i = 0; i < devices.size(); i++) {
                DeviceSLPF device = devices.valueAt(i);
                if (device.getDeviceTransportType().equals(type)) {
                    devicesWithType.add(device);
                }
            }
        } finally {
            devicesLock.unlock();
        }

        return devicesWithType;
    }

        /**
     * Retrieves the device that represent this local android device
     *
     * @return the local device
     */
    public DeviceSLPF getLocalDevice() {
        if (localDevice == null) {
            throw new IllegalStateException("no local device");
        }

        return localDevice;
    }

    public DeviceSLPF getDeviceByStorageType(String storageType) {
        DeviceSLPF retDev = null;
        try {
            for (int i = 0; i < devices.size(); i++) {
                DeviceSLPF device = devices.valueAt(i);
                if (StringUtils.equals(device.getWebStorageType(), storageType)) {
                    retDev = device;
                    break;
                }
            }
        } catch (Exception e) {
            Log.e(this, "getDeviceByStorageType() - storageType : " + storageType + ", Exception : " + e.getMessage());
        }
        return retDev;
    }

    public DeviceSLPF getDeviceById(long id) {
        return devices.get((int) id);
    }

    public void sendLocalBroadcast(Intent intent) {
        localBraodcastManager.sendBroadcast(intent);
    }


    private DataModelSLPF(ASPMediaStoreProvider provider) {
        mProvider = provider;
        localBraodcastManager = LocalBroadcastManager.getInstance(provider.getContext());
        localBraodcastManager.registerReceiver(masterResetBroadcastReceiver, new IntentFilter(IASPApplication2.BROADCAST_MASTER_RESET));
    }

    /**
     *
     */
    private void initializeFromDB() {
        try(Cursor cursor = mProvider.query(ASPMediaStore.Device.CONTENT_URI, null, null, null, null)) {
            devicesLock.lock();

            devices.clear();
            int nUnregisterCount = 0;
            try {
                while (cursor != null && cursor.moveToNext()) {
                    DeviceSLPF device = new DeviceSLPF(getContext(), cursor);
                    ////P140723-07752, do we need keep this Unregistered device always?
                    nUnregisterCount++;
                    if (nUnregisterCount > 16 /*&& device.getDeviceTransportType() == CloudGatewayDeviceTransportType.UNREGISTERED*/) {
                        continue;
                    }
                    Log.d(this, "initializeFromDB() - caching device : " + device);

                    if (device.isLocalDevice()) {
                        localDevice = device;
                    }

                    devices.put(device.getId(), device);
                }
                Log.i(this, "initializeFromDB() - nUnregisterCount=" + nUnregisterCount);

                if (localDevice == null) {
                    Log.d(this, "initializeFromDB() - creating local device");
                    localDevice = new DeviceSLPF(getContext());
                    localDevice.setDeviceTransportType(CloudGatewayDeviceTransportType.LOCAL);

                    localDevice.setAllSyncsMediaType(DeviceSLPF.DEFAULT_LOCAL_SYNCED_MEDIA_TYPES);
                }

                localDevice.setDeviceNetworkMode(BroadcastReceiverManager.getInstance(mProvider.getContext()).getNetworkStatus());

                localDevice.commitChanges();
            } catch (Exception e){
                Log.e(this, "initializeFromDB() - Exception : " + e.getMessage());
            }

        } finally {
            devicesLock.unlock();
        }
    }

    /**
     * Adds the device to the database and to the datamodel. This method should NOT be invoked from the main thread.
     *
     * @param device the device to add
     */
    public void addDevice(final DeviceSLPF device) {
        Log.d(this, "addDevice() - add device [" + device + "]");

        if (device.getId() != 0) {
            devicesLock.lock();
            try {
                devices.put(device.getId(), device);
            } finally {
                devicesLock.unlock();
            }
            // Remove from blacklist
//            UnknownPeerIdBlacklist.getInstance(this.provider.getContext()).remove(device.getPeerId());
            localBraodcastManager.sendBroadcast(new Intent(BROADCAST_DEVICE_LIST_CHANGE));
        }
    }

    /**
     * Removes the device from the datamodel.
     *
     * @param device the device to remove
     */
    public void removeDevice(DeviceSLPF device) {
        devicesLock.lock();
        try {
            devices.remove(device.getId());
        } finally {
            devicesLock.unlock();
        }

        localBraodcastManager.sendBroadcast(new Intent(BROADCAST_DEVICE_LIST_CHANGE));

        Intent intent = new Intent(CloudGatewayMediaStore.Device.ACTION_DEVICE_DELETED_BROADCAST);
        intent.setDataAndType(ASPMediaStore.Device.getDeviceEntryUri(device.getId()), CloudGatewayMediaStore.Device.CONTENT_TYPE);
        ////reducing BRdcast
        localBraodcastManager.sendBroadcast(intent);
    }

    // TRY NOT TO USE THIS, BUT USE Device.commitChanges INSTEAD
    @Override
    public void updateDevice(final CloudDevice cloudDevice) {
        CachedExecutorService.getInstance().execute(new Runnable() {

            @Override
            public void run() {
                ((DeviceSLPF) cloudDevice).commitChanges();
            }
        });
    }

    public DeviceSLPF getFirstDeviceForStorageType(final String type) {
        DeviceSLPF device = null;
        try {
            devicesLock.lock();
            device = (DeviceSLPF) CollectionUtils.find(getDevices(), new Predicate() {

                @Override
                public boolean evaluate(Object arg0) {
                    return StringUtils.equalsIgnoreCase(type, ((DeviceSLPF) arg0).getWebStorageType());
                }
            });
        } finally {
            devicesLock.unlock();
        }
        return device;
    }
}

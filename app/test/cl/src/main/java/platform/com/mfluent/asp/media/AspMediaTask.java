/**
 *
 */

package platform.com.mfluent.asp.media;

/**
 * @author michaelgierach
 */
public abstract class AspMediaTask {

    private final Thread mExecutingThread;
    private boolean mCancelled = false;
    private boolean mFinished = false;
    private final Object mTaskInfo;

    public AspMediaTask(Object taskInfo) {
        mTaskInfo = taskInfo;

        mExecutingThread = Thread.currentThread();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        } else if (other instanceof AspMediaTask) {
            AspMediaTask otherTask = (AspMediaTask) other;

            return (mTaskInfo.equals(otherTask.mTaskInfo));
        }

        return false;
    }

    @Override
    public int hashCode() {
        return mTaskInfo.hashCode();
    }

    public final synchronized void cancel() {
        if ((!mCancelled) && (!mFinished)) {
            mCancelled = true;
            cancelAction();
            mExecutingThread.interrupt();
        }
    }

    protected void cancelAction() {
    }

    public final synchronized void finish() {
        if ((!mCancelled) && (!mFinished)) {
            if (Thread.currentThread() != mExecutingThread) {
                throw new IllegalStateException("You can only finish on the executing thread.");
            }
            mFinished = true;
            finishAction();
        }
        notifyAll();
    }

    private void finishAction() {
    }
}

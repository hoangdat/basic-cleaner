
package platform.com.mfluent.asp.datamodel.filebrowser;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.text.TextUtils;

import com.mfluent.asp.cloudstorage.api.sync.CloudStorageSync;
import com.mfluent.asp.common.datamodel.ASPFile;
import com.mfluent.asp.common.datamodel.ASPFileBrowser;
import com.mfluent.asp.common.datamodel.ASPFileProvider;
import com.mfluent.asp.common.datamodel.ASPFileSelectionHelper;
import com.mfluent.asp.common.datamodel.ASPFileSortType;
import com.mfluent.asp.common.datamodel.ASPFileSpecialType;
import com.mfluent.asp.common.datamodel.ASPMediaStore;
import com.mfluent.asp.common.util.CloudStorageConstants;
import com.mfluent.asp.common.util.CursorUtils;
import com.mfluent.log.Log;
import com.samsung.android.sdk.slinkcloud.CloudGatewayMediaStore;
import com.samsung.android.slinkcloud.Manifest;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Method;
import java.text.CollationKey;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import platform.com.mfluent.asp.datamodel.DeviceSLPF;
import platform.com.mfluent.asp.util.CollectionUtils;
import platform.com.mfluent.asp.util.Predicate;
import platform.com.samsung.android.slinkcloud.NetResourceCacheManager;
import uicommon.com.mfluent.asp.util.CachedExecutorService;

public class CachedFileBrowser implements ASPFileBrowser<ASPFile> {
    private static String TAG = "CachedFileBroswer";

    ASPFileBrowser<?> mOrgBrowser = null;
    public static final String URI_MATCHER = "cachedfilelist://";
    private DeviceSLPF mDevice = null;
    private Context mContext = null;
    private ASPFileProvider mOrgProvider = null;
    private ArrayList<CachedCloudFile> mFileCache = null;
    public static final String RESTAPI_FILE_TYPE = "application/json";

    String mObjectKey = null;
    String mStorageGatewayID = null;
    ASPFileSortType mSortType = null;
    boolean mForceReload = false;
    CachedCloudFile mParentDirectory = null;
    CachedCloudFile mCurrentDirectory = null;
    String mCurrentDirPath = "";
    private final Set<DataSetObserver> dataSetObservers = new HashSet<DataSetObserver>();
    private int mCntOfGrandChildren = 0;
    private int mOldCntOfGrandChildren = 0;
    private int mTotalFileCount = 0;
    private int mOldTotalFileCount = 0;
    private long mDirRevision = 0;
    private long mOldDirRevision = 0;
    private final Lock mStateLock = new ReentrantLock();
    private final ReentrantLock mLockSaveToFile = new ReentrantLock();
    private final Condition mConditionSave = mLockSaveToFile.newCondition();
    private boolean mIsSaving = false;

    private boolean mIsBrowserForView = true;

    private ASPFileSelectionHelper mSelectionHelper;

    public static final int STATE_ZERO = 0;
    public static final int STATE_CACHE_CHECKED = 1;
    public static final int STATE_SAVE_CACHE_DONE = 2;

    public volatile int mFileBrowserStatus = STATE_ZERO;

    public final static long INTERVAL_MIN_CHECKREVISION_SEC = 30;

    public final static int MAX_SQL_ITEMS = 1500;
    public final static int MAX_DELTA_CACHE_ITEMS = 1000;

    private static volatile int sBgTaskCount = 0;
    private static final int MAX_BG_JOB = 6;

    private String mStrOrgSortOption = null;

    private final Object mDuringTaskSync = new Object();
    private volatile boolean mFlagTask = false;

    private String mSelection = null;
    private String[] mSelectionArgs = null;
    private CancellationSignal mCancellationSignal = null;
    private static BroadcastReceiver sCancelReceiver = null;

    private static class TempFileItem {
        public long nOperationTime;
        public String strParentID;
        public String strCloudID;
        public String strPath;
        public String strName;
        public boolean isDirectory;
        public int nDevID;

        public TempFileItem() {
            nOperationTime = System.currentTimeMillis();
        }

        @Override
        protected Object clone() throws CloneNotSupportedException {
            TempFileItem tmpItem = new TempFileItem();
            tmpItem.strParentID = strParentID;
            tmpItem.strCloudID = strCloudID;
            tmpItem.strPath = strPath;
            tmpItem.strName = strName;
            tmpItem.isDirectory = isDirectory;
            tmpItem.nDevID = nDevID;
            tmpItem.nOperationTime = nOperationTime;
            return tmpItem;
        }
    }

    private static class TempFileList {
        public HashMap<String, TempFileItem> map;

        public TempFileList() {
            map = new HashMap<String, TempFileItem>();
        }
    }

    private final static ConcurrentMap<String, CachedFileBrowser> sInstanceMap = new ConcurrentHashMap<String, CachedFileBrowser>();
    private final static HashMap<String, TempFileItem> sTempFileMap = new HashMap<String, TempFileItem>();
    private final static HashMap<String, TempFileList> sTempMapByParentID = new HashMap<String, TempFileList>();
    private final static HashMap<String, String> sTempRmMap = new HashMap<String, String>();
    private final static HashMap<String, TempFileList> sTempRenameMapByParentID = new HashMap<String, TempFileList>();
    private final static HashMap<String, String> sTempRmTrashMap = new HashMap<String, String>();

    //delta file cache layer compensate the time gap between mkdir/rmdir operation and cloud sync done
    private static String deltaFileLayerMakeKey(int nDevId, String strParentID, String strName) {
        String strID = strParentID + "_" + strName + "_" + nDevId;
        return strID;
    }

    //return existing folder cloud id if it exist in delta layer
    public static String deltaFileLayerFind(int nDevId, String strName, String strParentID) {
        String strKey = deltaFileLayerMakeKey(nDevId, strParentID, strName);
        String strRet = null;
        synchronized (sTempFileMap) {
            TempFileItem objItem = sTempFileMap.get(strKey);
            if (objItem != null) {
                strRet = objItem.strCloudID;
            }
        }
        return strRet;
    }

    //this list is used for virtual file adding/removing when filebrowser is made
    private static HashMap<String, TempFileItem> deltaAddedFileLayerGetList(int nDevId, String strParentID) {
        HashMap<String, TempFileItem> retObj = null;
        synchronized (sTempFileMap) {
            String strParentKey = deltaFileLayerMakeKey(nDevId, strParentID, "");
            TempFileList tmpList = sTempMapByParentID.get(strParentKey);
            if (tmpList != null) {
                retObj = (HashMap<String, TempFileItem>) tmpList.map.clone();
            }
        }
        return retObj;
    }

    public static void deltaFileLayerAdd(DeviceSLPF objDev, Context context, String strName, String strParentID, String strNewID, boolean isDirectory) {
        if (objDev == null)
            return;
        int nDevId = objDev.getId();
        String strKey = deltaFileLayerMakeKey(nDevId, strParentID, strName);
        TempFileItem objItem = new TempFileItem();
        objItem.isDirectory = isDirectory;
        objItem.strName = strName;
        objItem.strParentID = strParentID;
        objItem.strPath = null;
        objItem.strCloudID = strNewID;
        objItem.nDevID = nDevId;
        boolean bNeedSync = false;
        synchronized (sTempFileMap) {
            if (sTempFileMap.size() > MAX_DELTA_CACHE_ITEMS) {
                bNeedSync = true;
            } else {
                sTempFileMap.put(strKey, objItem);
                String strParentKey = deltaFileLayerMakeKey(nDevId, strParentID, "");
                TempFileList tmpList = sTempMapByParentID.get(strParentKey);
                if (tmpList == null) {
                    tmpList = new TempFileList();
                    tmpList.map.put(strNewID, objItem);
                    sTempMapByParentID.put(strParentKey, tmpList);
                } else {
                    tmpList.map.put(strNewID, objItem);
                }
            }
        }
        if (bNeedSync)
            NetResourceCacheManager.getInstance(context).deleteCache(CachedFileBrowser.URI_MATCHER + strParentID, null, nDevId);
        Log.i(TAG, "deltaFileLayerAdd() - add mkdir id strName=" + strName + ", strParentID=" + strParentID + "strNewID=" + strNewID + "bNeedSync=" + bNeedSync);
        notifyAllInstances(nDevId);
    }

    private static void deltaAddedFileLayerRemoveReal(int nDevId, String strName, String strParentID) {
        Log.d(TAG, "deltaAddedFileLayerRemoveReal");
        String strKey = deltaFileLayerMakeKey(nDevId, strParentID, strName);
        synchronized (sTempFileMap) {
            TempFileItem objItem = sTempFileMap.get(strKey);
            if (objItem != null) {
                String strParentKey = deltaFileLayerMakeKey(objItem.nDevID, objItem.strParentID, "");
                TempFileList tmpList = sTempMapByParentID.get(strParentKey);
                if (tmpList != null)
                    tmpList.map.remove(objItem.strCloudID);
                sTempFileMap.remove(strKey);
            }
        }
    }

    /**
     * Call when delete item in trash list
     *
     * @param nDevId     device id
     * @param strCloudID item cloud id
     */

    public static void deltaFileLayerRemoveTrash(int nDevId, String strCloudID, boolean isRestore) {
        int nCnt = 0;
        boolean bFound = false;
        int nMax = MAX_DELTA_CACHE_ITEMS * 2;
        Log.i(TAG, "deltaFileLayerRemoveTrash() - remove folder from tmpcache");

        synchronized (sTempFileMap) {
            Iterator it = sTempFileMap.entrySet().iterator();
            TempFileItem objItem;
            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();
                objItem = (TempFileItem) pairs.getValue();
                if (TextUtils.equals(strCloudID, objItem.strCloudID)) {
                    String strParentKey = deltaFileLayerMakeKey(nDevId, objItem.strParentID, "");
                    TempFileList tmpList = sTempMapByParentID.get(strParentKey);
                    if (tmpList != null)
                        tmpList.map.remove(objItem.strCloudID);
                    it.remove();
                    bFound = true;
                    break;
                }
                nCnt++;
                if (nCnt > nMax) {
                    Log.i(TAG, "deltaFileLayerRemoveTrash() - break deltaFileLayerRemove loop due to over " + nMax + " count!");
                    break;
                }
            }
        }
        if (bFound == false) {
            String strRmKey = deltaFileLayerMakeKey(nDevId, strCloudID, "");
            if (sTempRmTrashMap.size() < nMax) {
                synchronized (sTempRmTrashMap) {
                    sTempRmTrashMap.put(strRmKey, strCloudID);
                }
            }
            //for update the normal list when restore a file
            if (isRestore) {
                synchronized (sTempRmMap) {
                    sTempRmMap.remove(strRmKey);
                }
            }
        }
        notifyAllInstances(nDevId);
    }

    public static void deltaFileLayerRemove(int nDevId, String strCloudID) {
        int nCnt = 0;
        boolean bFound = false;
        int nMax = MAX_DELTA_CACHE_ITEMS * 2;
        Log.i(TAG, "deltaFileLayerRemove() - remove folder from tmpcache");

        synchronized (sTempFileMap) {
            Iterator it = sTempFileMap.entrySet().iterator();
            TempFileItem objItem;
            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();
                objItem = (TempFileItem) pairs.getValue();
                if (TextUtils.equals(strCloudID, objItem.strCloudID)) {
                    String strParentKey = deltaFileLayerMakeKey(nDevId, objItem.strParentID, "");
                    TempFileList tmpList = sTempMapByParentID.get(strParentKey);
                    if (tmpList != null)
                        tmpList.map.remove(objItem.strCloudID);
                    it.remove();
                    bFound = true;
                    break;
                }
                nCnt++;
                if (nCnt > nMax) {
                    Log.i(TAG, "deltaFileLayerRemove() - break deltaFileLayerRemove loop due to over " + nMax + " count!");
                    break;
                }
            }
        }
        if (bFound == false) {
            String strRmKey = deltaFileLayerMakeKey(nDevId, strCloudID, "");
            if (sTempRmMap.size() < nMax) {
                synchronized (sTempRmMap) {
                    sTempRmMap.put(strRmKey, strCloudID);
                }
            }
            synchronized (sTempRmTrashMap) {
                sTempRmTrashMap.remove(strRmKey);
            }
        }
        notifyAllInstances(nDevId);
    }


    //this list is used for virtual file renamed when filebrowser is made
    private static HashMap<String, TempFileItem> deltaRenamedFileLayerGetList(int nDevId, String strParentID) {
        HashMap<String, TempFileItem> retObj = null;
        synchronized (sTempRenameMapByParentID) {
            String strParentKey = deltaFileLayerMakeKey(nDevId, strParentID, "");
            TempFileList tmpList = sTempRenameMapByParentID.get(strParentKey);
            if (tmpList != null) {
                retObj = (HashMap<String, TempFileItem>) tmpList.map.clone();
            }
        }
        return retObj;
    }

    private static void deltaRenamedFileLayerRemoveReal(int nDevId, String strParentID, String strCloudID) {
        String strKey = deltaFileLayerMakeKey(nDevId, strParentID, "");
        synchronized (sTempRenameMapByParentID) {
            TempFileList tmpList = sTempRenameMapByParentID.get(strKey);
            if (tmpList != null)
                tmpList.map.remove(strCloudID);
        }
    }

    //called when rename function
    public static void deltaFileLayerRename(DeviceSLPF objDev, Context context, String strParentID, String strCloudID, String strName, boolean isDirectory) {
        if (objDev == null)
            return;
        int nDevId = objDev.getId();

        TempFileItem objItem = new TempFileItem();
        objItem.isDirectory = isDirectory;
        objItem.strName = strName;
        objItem.strParentID = strParentID;
        objItem.strPath = null;
        objItem.strCloudID = strCloudID;
        objItem.nDevID = nDevId;
        boolean bNeedSync = false;
        synchronized (sTempRenameMapByParentID) {
            if (sTempRenameMapByParentID.size() > MAX_DELTA_CACHE_ITEMS) {
                bNeedSync = true;
            } else {
                String strParentKey = deltaFileLayerMakeKey(nDevId, strParentID, "");
                TempFileList tmpList = sTempRenameMapByParentID.get(strParentKey);
                if (tmpList == null) {
                    tmpList = new TempFileList();
                    tmpList.map.put(strCloudID, objItem);
                    sTempRenameMapByParentID.put(strParentKey, tmpList);
                } else {
                    tmpList.map.put(strCloudID, objItem);
                }
            }
        }
        if (bNeedSync) {
            NetResourceCacheManager.getInstance(context).deleteCache(CachedFileBrowser.URI_MATCHER + strParentID, null, nDevId);
        } else {
            notifyAllInstances(nDevId);
        }
        Log.i(TAG, "deltaFileLayerRename() - add rename id strName=" + strName + ", strParentID=" + strParentID + ", strCloudID=" + strCloudID + ", bNeedSync=" + bNeedSync);
    }

    public static void notifyAllInstances(int nDevId) {
        Log.i(TAG, "notifyAllInstances started...");
        try {
            for (Object obj : sInstanceMap.entrySet()) {
                Map.Entry pairs = (Map.Entry) obj;
                CachedFileBrowser tmpCache = (CachedFileBrowser) pairs.getValue();
                boolean bTrigger = true;
                if (nDevId > 0) {
                    if (tmpCache.getDeviceId() != nDevId)
                        bTrigger = false;
                }
                if (bTrigger)
                    tmpCache.notifyChange(true);
            }
        } catch (Exception e) {
            Log.e(TAG, "notifyAllInstances() - Exception : " + e.getMessage());
        }
        Log.i(TAG, "notifyAllInstances ended...");
    }

    public static void checkAllInstances() {
        Log.i(TAG, "checkAllInstances started...");
        try {
            for (Map.Entry pairs : sInstanceMap.entrySet()) {
                CachedFileBrowser tmpCache = (CachedFileBrowser) pairs.getValue();
                tmpCache.tryToCheck();
            }
        } catch (Exception e) {
            Log.e(TAG, "checkAllInstances() - Exception : " + e.getMessage());
        }
        Log.i(TAG, "checkAllInstances ended...");
    }

    public static void checkCancelFileBrowser(String strCloudID) {
        Log.i(TAG, "checkCancelFileBrowser started...");
        try {
            for (Map.Entry pairs : sInstanceMap.entrySet()) {
                CachedFileBrowser tmpCache = (CachedFileBrowser) pairs.getValue();
                if (tmpCache == null || !tmpCache.mIsBrowserForView) {
                    continue;
                }

                boolean isRoot = (TextUtils.isEmpty(tmpCache.mStorageGatewayID) && TextUtils.equals(strCloudID, CloudStorageConstants.CLOUD_ROOT));
                if (isRoot || TextUtils.equals(tmpCache.mStorageGatewayID, strCloudID)) {
                    tmpCache.cancelSignal();
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "checkCancelFileBrowser() - Exception : " + e.getMessage());
        }
        Log.i(TAG, "checkCancelFileBrowser ended...");
    }

    public CachedFileBrowser(DeviceSLPF device, Context context) {
        mDevice = device;
        mContext = context;
        mFileCache = new ArrayList<CachedCloudFile>();
        mSelectionHelper = null;
        mFileBrowserStatus = STATE_ZERO;
        synchronized (sInstanceMap) {
            if (sCancelReceiver == null) {
                IntentFilter cancelIntentFilter = new IntentFilter();
                cancelIntentFilter.addAction("com.samsung.android.slinkcloud.cancelloadfilebrowser");
                sCancelReceiver = new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        Bundle extras = intent.getExtras();
                        if (extras != null) {
                            String cloudId = extras.getString("cloudid");
                            if (cloudId != null) {
                                Log.i(this, "receive cloudid=" + cloudId);
                                checkCancelFileBrowser(cloudId);
                            }
                        }
                    }
                };
                context.registerReceiver(sCancelReceiver, cancelIntentFilter, Manifest.permission.PUBLIC_ACCESS, null);
            }
        }
    }

    public void cancelSignal() {
        if (mCancellationSignal != null) {
            Log.i(this, "try to cancel~" + mStorageGatewayID);
            mCancellationSignal.cancel();
        }
    }

    private CachedCloudFile findCloudItemByName(ArrayList<CachedCloudFile> fileCache, String strName) {
        final String strParam = strName;
        return (CachedCloudFile) CollectionUtils.find(fileCache, new Predicate() {

            @Override
            public boolean evaluate(Object arg) {
                CachedCloudFile objRet = (CachedCloudFile) arg;
                return TextUtils.equals(objRet.getName(), strParam);
            }
        });
    }

    private CachedCloudFile findCloudItemByCloudID(ArrayList<CachedCloudFile> fileCache, String strCloudID) {
        final String strParam = strCloudID;
        return (CachedCloudFile) CollectionUtils.find(fileCache, new Predicate() {

            @Override
            public boolean evaluate(Object arg) {
                CachedCloudFile objRet = (CachedCloudFile) arg;
                return TextUtils.equals(objRet.getCloudId(), strParam);
            }
        });
    }

    public int getDeviceId() {
        return mDevice != null ? mDevice.getId() : 0;
    }

    private void checkTmpRenamedFileList(ArrayList<CachedCloudFile> fileCache) {
        if (mStorageGatewayID != null && mDevice != null && fileCache != null && fileCache.isEmpty() == false) {
            Iterator it;
            int nMaxCount = 0;
            HashMap<String, TempFileItem> tmpMap = deltaRenamedFileLayerGetList(mDevice.getId(), mStorageGatewayID);
            Log.i(this, "checkTmpRenamedFileList() - item tmpMap=" + (tmpMap == null ? false : true) + " for mStorageGatewayID=" + mStorageGatewayID);
            if (tmpMap != null && tmpMap.isEmpty() == false) {
                //set added delta folder or files
                it = tmpMap.entrySet().iterator();
                CachedCloudFile tmpFile;
                while (it.hasNext()) {
                    Map.Entry pairs = (Map.Entry) it.next();
                    TempFileItem objItem = (TempFileItem) pairs.getValue();
                    tmpFile = findCloudItemByCloudID(fileCache, objItem.strCloudID);
                    if (tmpFile != null && objItem.strName.equals(tmpFile.getName())) {
                        Log.i(this, "checkTmpRenamedFileList() - sync done remove item from map name=" + objItem.strName);
                        deltaRenamedFileLayerRemoveReal(objItem.nDevID, objItem.strParentID, objItem.strCloudID);
                    } else {
                        if (tmpFile != null) {
                            tmpFile.setName(objItem.strName);
                            Log.i(this, "checkTmpRenamedFileList() - rename virtual name=" + objItem.strName);
                        }
                    }
                    nMaxCount++;
                    if (nMaxCount > 400) {
                        Log.i(this, "checkTmpAddedFileList() - skip due to over 400 loops");
                        break;
                    }
                }
            }
        }
    }

    private void checkTmpAddedFileList(ArrayList<CachedCloudFile> fileCache) {
        if (mStorageGatewayID != null && mDevice != null && fileCache != null) {
            Iterator it;
            int nMaxCount = 0;
            HashMap<String, TempFileItem> tmpMap = deltaAddedFileLayerGetList(mDevice.getId(), mStorageGatewayID);
            Log.i(this, "checkTmpAddedFileList() - item tmpMap=" + (tmpMap == null ? false : true));
            if (tmpMap != null && tmpMap.isEmpty() == false) {
                //set added delta folder or files
                it = tmpMap.entrySet().iterator();
                CachedCloudFile tmpFile;
                while (it.hasNext()) {
                    Map.Entry pairs = (Map.Entry) it.next();
                    TempFileItem objItem = (TempFileItem) pairs.getValue();
                    tmpFile = findCloudItemByCloudID(fileCache, objItem.strCloudID);
                    if (tmpFile != null) {
                        Log.i(this, "checkTmpAddedFileList() - sync done remove item from map name=" + objItem.strName);
                        deltaAddedFileLayerRemoveReal(objItem.nDevID, objItem.strName, objItem.strParentID);
                    } else {
                        tmpFile = new CachedCloudFile();
                        tmpFile.setCloudId(objItem.strCloudID);
                        tmpFile.setDeviceId(objItem.nDevID);
                        tmpFile.setName(objItem.strName);
                        tmpFile.setDirectory(objItem.isDirectory);
                        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
                        if (calendar != null)
                            tmpFile.setLastModified(calendar.getTimeInMillis() / 1000);
                        fileCache.add(tmpFile);
                        Log.i(this, "checkTmpAddedFileList() - add virtual file name=" + objItem.strName);
                    }
                    nMaxCount++;
                    if (nMaxCount > 100) {
                        Log.i(this, "checkTmpAddedFileList() - skip due to over 100 lopps");
                        break;
                    }
                }
            }
        }
    }

    private void checkTmpRemovedFileList(ArrayList<CachedCloudFile> fileCache) {
        //set removed delta folder
        int nMaxCount = 0;
        Iterator it;
        CachedCloudFile tmpFile;
        int nDevId = 0;
        if (mDevice != null)
            nDevId = mDevice.getId();
        if (mStorageGatewayID != null && !mStorageGatewayID.equals(CloudGatewayMediaStore.Trash.Trash_ID)
                && mDevice != null && sTempRmMap.isEmpty() == false && fileCache.isEmpty() == false) {
            it = fileCache.iterator();
            while (it.hasNext()) {
                tmpFile = (CachedCloudFile) it.next();
                String strCloudID = tmpFile.getCloudId();
                String strFound = null;
                String strKey = deltaFileLayerMakeKey(nDevId, strCloudID, "");
                synchronized (sTempRmMap) {
                    strFound = sTempRmMap.get(strKey);
                }
                if (strFound != null) {
                    Log.i(this, "checkTmpRemovedFileList() - remove virtual file strCloudID=" + strCloudID);
                    it.remove();
                }
                nMaxCount++;
                if (nMaxCount > 100) {
                    Log.i(this, "checkTmpRemovedFileList() - skip due to over 100 lopps");
                    break;
                }
            }
        }
        if (sTempRmMap.size() >= MAX_DELTA_CACHE_ITEMS * 2) {
            synchronized (sTempRmMap) {
                sTempRmMap.clear();
            }
        }
    }

    private void checkTmpRemovedTrash(ArrayList<CachedCloudFile> fileCache) {
        int nMaxCount = 0;
        Iterator it;
        CachedCloudFile tmpFile;
        int nDevId = 0;
        if (mDevice != null)
            nDevId = mDevice.getId();
        if (mStorageGatewayID != null && mStorageGatewayID.equals(CloudGatewayMediaStore.Trash.Trash_ID)
                && mDevice != null && sTempRmTrashMap.isEmpty() == false && fileCache.isEmpty() == false) {
            it = fileCache.iterator();
            while (it.hasNext()) {
                tmpFile = (CachedCloudFile) it.next();
                String strCloudID = tmpFile.getCloudId();
                String strFound = null;
                String strKey = deltaFileLayerMakeKey(nDevId, strCloudID, "");
                synchronized (sTempRmTrashMap) {
                    strFound = sTempRmTrashMap.get(strKey);
                }
                if (strFound != null) {
                    Log.i(this, "checkTmpRemovedFileList() - remove virtual file strCloudID=" + strCloudID);
                    it.remove();
                }
                nMaxCount++;
                if (nMaxCount > 100) {
                    Log.i(this, "checkTmpRemovedFileList() - skip due to over 100 lopps");
                    break;
                }
            }
        }
        if (sTempRmTrashMap.size() >= MAX_DELTA_CACHE_ITEMS * 2) {
            synchronized (sTempRmTrashMap) {
                sTempRmTrashMap.clear();
            }
        }
    }


    private boolean checkChangeListChanged(ArrayList<CachedCloudFile> fileCache1, ArrayList<CachedCloudFile> fileCache2) {
        if (fileCache1 == null || fileCache2 == null)
            return true;
        int nSz1 = fileCache1.size();
        if (nSz1 != fileCache2.size())
            return true;

        if (nSz1 > 2000)
            nSz1 = 2000;

        try {
            CachedCloudFile tmpFile1, tmpFile2;
            int cnt;

            for (cnt = 0; cnt < nSz1; cnt++) {
                tmpFile1 = fileCache1.get(cnt);
                tmpFile2 = fileCache2.get(cnt);

                long fileSize1 = tmpFile1.length();
                long fileSize2 = tmpFile2.length();
                if (fileSize1 != fileSize2) {
                    Log.d(this, "checkChangeListChanged() - fileSize : " + fileSize1 + " " + fileSize2);
                    return true;
                }

                Bundle bundle1 = (Bundle) fileCache1.get(cnt).getEtcDataObject();
                Bundle bundle2 = (Bundle) fileCache2.get(cnt).getEtcDataObject();
                if (bundle1 != null && bundle2 != null) {
                    String processing1 = bundle1.getString("asp_trashProcessing");
                    String processing2 = bundle2.getString("asp_trashProcessing");
                    Log.d(this, "checkChangeListChanged processing: " + processing1 + " " + processing2);
                    if (!TextUtils.equals(processing1, processing2)) {
                        return true;
                    }
                }
            }
        } catch (Exception e) {
            Log.e(this, "checkChangeListChanged() - Exception : " + e.getMessage());
        }
        return false;
    }

    @Override
    public void destroy() {
        try {
            mLockSaveToFile.lock();
            while ((mOrgBrowser != null) && mIsSaving) {
                try {
                    mConditionSave.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            if (mOrgBrowser != null) {
                mOrgBrowser.destroy();
            }
        } finally {
            mLockSaveToFile.unlock();
        }
        mStateLock.lock();
        try {
            mFileCache.clear();
            Log.i(this, "destroy(): key = " + mObjectKey + ", storageId = " + mStorageGatewayID + ", path = " + mCurrentDirPath + ", selection=" + mSelection);
            mParentDirectory = null;
            mCurrentDirectory = null;
            mCurrentDirPath = "";
            mTotalFileCount = 0;
            mCntOfGrandChildren = 0;
            mFileBrowserStatus = STATE_ZERO;
        } catch (Exception e) {
            Log.e(this, "destroy() - Exception : " + e.getMessage());
        } finally {
            try {
                sInstanceMap.remove(mObjectKey);
            } catch (Exception e) {
                Log.e(this, "destroy() - Exception in finally : " + e.getMessage());
            } finally {
                mStateLock.unlock();
            }
        }
    }

    @Override
    public int getCount() {
        int nRet = 0;
        mStateLock.lock();
        try {
            nRet = this.mTotalFileCount;
        } catch (Exception e) {
            Log.e(this, "getCount() - Exception : " + e.getMessage());
        } finally {
            mStateLock.unlock();
        }
        return nRet;
    }

    public int getCurrentSize() {
        return getCount();
    }

    @Override
    public ASPFile getCurrentDirectory() {
        if (mCurrentDirectory == null) {
            return null;
        }
        ASPFile apsFile = null;
        mStateLock.lock();
        try {
            apsFile = new CachedCloudFile(mCurrentDirectory);
        } catch (Exception e) {
            Log.e(this, "getCurrentDirectory() = Exception : " + e.getMessage());
        } finally {
            mStateLock.unlock();
        }
        return apsFile;
    }

    @Override
    public String getCurrentDirectoryAbsolutePath() {
        String strDir = null;
        mStateLock.lock();
        try {
            //strDir = new String(this.mCurrentDirPath); //P151120-06656
            strDir = mCurrentDirPath;
        } catch (Exception e) {
            Log.e(this, "getCurrentDirectoryAbsolutePath() - Exception : " + e.getMessage());
        } finally {
            mStateLock.unlock();
        }
        return strDir;
    }

    @Override
    public ASPFile getFile(int arg0) throws InterruptedException, IOException {
        CachedCloudFile tmpFile = null;
        mStateLock.lock();
        try {
            if (mFileCache.isEmpty() == false) {
                tmpFile = mFileCache.get(arg0);
            }
        } catch (Exception e) {
            Log.e(this, "getFile() - Exception : " + e.getMessage());
        } finally {
            mStateLock.unlock();
        }
        return tmpFile;
    }

    @Override
    public ASPFile getFileNonBlocking(int arg0) {
        try {
            return getFile(arg0);
        } catch (InterruptedException e) {
            Log.e(this, "getFileNonBlocking() - InterruptedException : " + e.getMessage());
        } catch (IOException e) {
            Log.e(this, "getFileNonBlocking() - IOException : " + e.getMessage());
        }
        return null;
    }

    @Override
    public ASPFile getParentDirectory() {
        CachedCloudFile tmpFile = null;
        mStateLock.lock();
        try {
            if (mParentDirectory != null) {
                if (mDevice.isLocalDevice() && mCurrentDirPath.equals("/storage/emulated/0")) {
                    tmpFile = null;
                } else {
                    tmpFile = new CachedCloudFile(mParentDirectory);
                }
            }
        } catch (Exception e) {
            Log.e(this, "getParentDirectory() - Exception : " + e.getMessage());
        } finally {
            mStateLock.unlock();
        }

        return tmpFile;
    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {
        if (observer != null && !dataSetObservers.contains(observer)) {
            dataSetObservers.add(observer);
        }
    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {
        dataSetObservers.remove(observer);
    }

    private int guessMetaDataLocally(CachedCloudFile file, JSONObject fileInfo) {
        int nMediaId = -1;
        int nCloudDevId = mDevice.getId();
        try {
            file.setDeviceId(nCloudDevId);
            fileInfo.put("asp_deviceId", nCloudDevId);
        } catch (Exception e) {
            Log.e(this, "guessMetaDataLocally() - Exception : " + e.getMessage());
        }

        Cursor aspCursor = null;
        try {
            if (!file.isDirectory()) {
                final String[] projection = new String[]{"_id", "device_id", "source_media_id", "full_uri", "mime_type"};

                if (this.mContext != null) {
                    String selection = "source_media_id=?";
                    if (mStorageGatewayID == CloudGatewayMediaStore.Trash.Trash_ID) {
                        selection = selection + " AND trashed='1'";
                    }
                    aspCursor = this.mContext.getContentResolver().query(
                            CloudGatewayMediaStore.CloudFiles.getCloudFileUri(nCloudDevId),
                            projection,
                            selection,
                            new String[]{file.getCloudId()},
                            null);

                    if (aspCursor != null && aspCursor.getCount() > 0) {
                        aspCursor.moveToFirst();
                        Bundle mediaInfoBundle = new Bundle();
                        nMediaId = (int) CursorUtils.getLong(aspCursor, ASPMediaStore.BaseASPColumns._ID);
                        String full_uri = aspCursor.getString(aspCursor.getColumnIndexOrThrow(ASPMediaStore.Documents.DocumentColumns.FULL_URI)); //jsublee_150901
                        String mimeType = aspCursor.getString(aspCursor.getColumnIndexOrThrow(ASPMediaStore.MediaColumns.MIME_TYPE)); //jsublee_150901

                        mediaInfoBundle.putInt("asp_mediaId", nMediaId);
                        mediaInfoBundle.putString("asp_full_uri", full_uri); //jsublee_150901
                        mediaInfoBundle.putString("asp_mimetype", mimeType); //jsublee_150901

                        fileInfo.put("asp_mediaId", nMediaId);
                        fileInfo.put("asp_full_uri", full_uri); //jsublee_150901
                        fileInfo.put("asp_mimetype", mimeType); //jsublee_150901

                        file.setEtcDataObject(mediaInfoBundle);
                    }
                }
            } else { //in case of directory, //jsub12_151006
                Bundle mediaInfoBundle = new Bundle();

                mediaInfoBundle.putInt("asp_childCnt", fileInfo.optInt("asp_childCnt", 0));
                mediaInfoBundle.putInt("asp_childDirCnt", fileInfo.optInt("asp_childDirCnt", 0));
                mediaInfoBundle.putInt("asp_descendantsCnt", fileInfo.optInt("asp_descendantsCnt", 0));
                mediaInfoBundle.putInt("asp_descendantDirCnt", fileInfo.optInt("asp_descendantDirCnt", 0));
                mediaInfoBundle.putString("asp_trashProcessing", fileInfo.optString("asp_trashProcessing", null));

                file.setEtcDataObject(mediaInfoBundle);
            }
        } catch (Exception e1) {
            Log.e(this, "guessMetaDataLocally() - FileBrowser.MEDIA_ID Exception : " + e1);
        } finally {
            if (aspCursor != null) {
                aspCursor.close();
            }
        }

        return nMediaId;
    }

    private boolean saveFileBrowserToCache(CloudStorageSync.Result result) {
        ///put them to cache
        JSONObject json = new JSONObject();
        JSONArray arr = new JSONArray();

        int totalCntOfGrandChildren = 0;
        CachedCloudFile parentDir = null;
        ArrayList<CachedCloudFile> arrTmp = new ArrayList<CachedCloudFile>();

        //getCloudStorageFileBrowser is blocking function. it takes many minutes if there are many files in this directory
        //TBD: need getCloudStorageFileBrowser with limit (<100) for lazy loading in ListView (nonblocking when no cache)
        mIsSaving = true;
        CachedCloudFile curDir = null;
        String strCurPath = null;
        ASPFileSelectionHelper tmpSelectionHelper = null;
        try {
            mOrgBrowser = mOrgProvider.getCloudStorageFileBrowser(mStorageGatewayID, ASPFileSortType.DATE_MODIFIED_ASC, mForceReload, false);
            if (mOrgBrowser == null) {
                Log.e(this, "saveFileBrowserToCache() mOrgBrowser is null");
                return false;
            }

            mStorageGatewayID = mOrgProvider.getStorageGatewayFileId(mOrgBrowser.getCurrentDirectory());

            int nMax = mOrgBrowser.getCount();
            Log.i(this, "saveFileBrowserToCache() - mOrgBrowser.getCount()=" + nMax);
            tmpSelectionHelper = new ASPFileSelectionHelper(nMax);
            ASPFileBrowser<?> fileBrowser = mOrgBrowser;
            strCurPath = fileBrowser.getCurrentDirectoryAbsolutePath();
            ASPFile parentOrgDirectory = fileBrowser.getParentDirectory();
            ASPFile curOrgDirectory = fileBrowser.getCurrentDirectory();
            if (curOrgDirectory == null) {
                Log.e(this, "saveFileBrowserToCache() curOrgDirectory is null");
                return false;
            }
            curDir = new CachedCloudFile(curOrgDirectory);
            if (strCurPath == null || strCurPath.isEmpty()) {
                if (mCurrentDirPath != null && mCurrentDirPath.isEmpty() == false) {
                    strCurPath = mCurrentDirPath;
                    Log.i(this, "saveFileBrowserToCache() - strCurPath is null, replace with mCurrentDirPath=" + mCurrentDirPath);
                }
            }
            Log.i(this, "saveFileBrowserToCache() - strCurPath=" + strCurPath + ",mStorageGatewayID=" + mStorageGatewayID + ",curOrgDirectory=" + curOrgDirectory.getName());
            if (strCurPath != null && strCurPath.isEmpty() == false) {
                curDir.setName(strCurPath);
            }
            if (parentOrgDirectory != null) {
                Log.i(this, "parentOrgDirectory=" + parentOrgDirectory + ", strCurPath2 : " + strCurPath);
                parentDir = new CachedCloudFile(parentOrgDirectory);
                if (parentDir.getName() == null || parentDir.getName().isEmpty()) {
                    if (strCurPath != null && strCurPath.isEmpty() == false) {
                        File tmpFile = new File(strCurPath);
                        parentDir.setName(tmpFile.getParent());
                        Log.i(this, "saveFileBrowserToCache() - new parentDir=" + parentDir.getName());
                    }
                }
            } else {
                Log.i(this, "saveFileBrowserToCache() - parentOrgDirectory is null from " + curOrgDirectory);
                parentDir = new CachedCloudFile();
                parentDir.setName("");
                parentDir.setDirectory(true);
            }

            ASPFile file;
            String parentCloudID = (parentOrgDirectory != null) ? mOrgProvider.getStorageGatewayFileId(parentOrgDirectory) : "";
            String currentCloudID = mOrgProvider.getStorageGatewayFileId(curOrgDirectory);
            Log.i(this, "saveFileBrowserToCache() - parentCloudID=" + parentCloudID + ",currentCloudID=" + currentCloudID);
            if (parentOrgDirectory != null) {
                parentDir.setCloudId(parentCloudID);
            }
            curDir.setCloudId(currentCloudID);
            for (int nCnt = 0; nCnt < nMax + 2; nCnt++) {
                CachedCloudFile newFile = null;
                switch (nCnt) {
                    case 0:
                        file = curDir;
                        if (file != null) {
                            newFile = new CachedCloudFile(file);
                            newFile.setCloudId(currentCloudID);
                        }
                        break;
                    case 1:
                        file = parentDir;
                        if (file != null) {
                            newFile = new CachedCloudFile(file);
                            if (parentOrgDirectory != null) {
                                newFile.setCloudId(parentCloudID);
                            }
                        }
                        break;
                    default:
                        file = fileBrowser.getFile(nCnt - 2);
                        if (file != null) {
                            newFile = new CachedCloudFile(file);
                            newFile.setCloudId(mOrgProvider.getStorageGatewayFileId(file));
                            arrTmp.add(newFile);
                            if (newFile.isDirectory()) {
                                tmpSelectionHelper.setIsDirectory(nCnt - 2);
                            }
                        }
                        break;
                }
                JSONObject fileInfo = new JSONObject();

                //jsub12_151006
                int childCnt = 0;
                int childDirCnt = 0;
                int descendantsCnt = 0;
                int descendantDirCnt = 0;
                String trashProcessing = null;
                if (file.isDirectory()) {
                    childCnt = mOrgProvider.getCountOfChild(file);
                    childDirCnt = mOrgProvider.getCountOfChildDir(file);
                    descendantsCnt = mOrgProvider.getCountOfDescendants(file);
                    descendantDirCnt = mOrgProvider.getCountOfDescendantDir(file);
                    trashProcessing = mOrgProvider.getTrashProcessingDir(file);
                    totalCntOfGrandChildren += descendantsCnt;
                }

                if (newFile != null) {
                    fileInfo.put("name", newFile.getName());
                    fileInfo.put("isDirectory", newFile.isDirectory());
                    fileInfo.put("modified", newFile.lastModified());
                    fileInfo.put("fileSize", newFile.length());
                    fileInfo.put("special", newFile.getSpecialDirectoryType());
                    fileInfo.put("cloudId", newFile.getCloudId());
                }

                fileInfo.put("asp_childCnt", childCnt); //jsub12_151006
                fileInfo.put("asp_childDirCnt", childDirCnt); //jsub12_151006

                fileInfo.put("asp_descendantsCnt", descendantsCnt);
                fileInfo.put("asp_descendantDirCnt", descendantDirCnt);

                fileInfo.put("asp_trashProcessing", trashProcessing);

                if (nCnt < MAX_SQL_ITEMS && mDevice != null && mDevice.getId() != 1)
                    guessMetaDataLocally(newFile, fileInfo);

                arr.put(fileInfo);
            }
        } catch (FileNotFoundException e) {
            Log.e(this, "saveFileBrowserToCache() - FileNotFoundException : " + e.getMessage());
        } catch (Exception e) {
            Log.e(this, "saveFileBrowserToCache() - Exception : " + e.getMessage());
            return false;
        } finally {
            try {
                mLockSaveToFile.lock();
                mIsSaving = false;
                try {
                    mConditionSave.signalAll();
                } catch (Exception e) {
                    Log.d(this, "saveFileBrowserToCache() Exception = " + e.getMessage());
                }
            } finally {
                mLockSaveToFile.unlock();
            }
        }

        long dirRevision = (curDir != null) ? curDir.lastModified() : 0;
        int totalCount = arrTmp.size();
        Log.i(this, "saveFileBrowserToCache() - name=" + mStorageGatewayID + ",dirRevision=" + dirRevision + ",totalCount=" + totalCount);
        try {
            json.put("dirRevision", dirRevision);
            json.put("totalCount", totalCount);
            json.put("files", arr);
            json.put("totalCntOfGrandChildren", totalCntOfGrandChildren);
            ByteArrayOutputStream inBuffer = new ByteArrayOutputStream();
            inBuffer.write(json.toString().getBytes());
            if (mStorageGatewayID != null) {
                NetResourceCacheManager.getInstance(mContext).deleteCache(URI_MATCHER + mStorageGatewayID, null, mDevice.getId());
                NetResourceCacheManager.getInstance(mContext).saveCache(
                        URI_MATCHER + mStorageGatewayID,
                        null,
                        mDevice.getId(),
                        dirRevision,
                        inBuffer,
                        RESTAPI_FILE_TYPE);
            }
        } catch (Exception e) {
            Log.e(this, "saveFileBrowserToCache() - Exception : " + e.getMessage());
        }
        //user's sorting at the final step (cache is always saved with last_date sorting order)
        try {
            checkTmpAddedFileList(arrTmp);
            checkSortOption(arrTmp);
            checkTmpRemovedFileList(arrTmp);
            checkTmpRemovedTrash(arrTmp);
            checkTmpRenamedFileList(arrTmp);
        } catch (Exception e) {
            Log.e(this, "saveFileBrowserToCache() - Exception : " + e.getMessage());
        }

        totalCount = (mSelection != null) ? doFiltering(arrTmp) : arrTmp.size();
        if (result != null) {
            result.mRet = checkChangeListChanged(mFileCache, arrTmp);
        }
        mStateLock.lock();
        try {
            mDirRevision = dirRevision;
            mParentDirectory = parentDir;
            mTotalFileCount = totalCount;
            mCntOfGrandChildren = totalCntOfGrandChildren;
            mCurrentDirectory = curDir;
            mFileCache = arrTmp;
            mCurrentDirPath = strCurPath;
            mSelectionHelper = tmpSelectionHelper;
        } finally {
            mStateLock.unlock();
        }
        mFileBrowserStatus = STATE_SAVE_CACHE_DONE;
        return true;
    }

    private void getFileBrowserFromCache(ByteArrayOutputStream retBuffer) {
        try {
            JSONObject json = new JSONObject(new String(retBuffer.toByteArray()));
            int totalCount = 0;
            int nPrevTotalCount = json.optInt("totalCount", 0);
            long dirRevision = json.optLong("dirRevision", 0);
            int totalCntOfGrandChildren = json.optInt("totalCntOfGrandChildren", 0);
            mSelectionHelper = new ASPFileSelectionHelper(nPrevTotalCount);
            JSONArray files = json.optJSONArray("files");
            Log.i(this, "getFileBrowserFromCache() - start " + files.length());
            for (int i = 0; i < files.length(); i++) {
                ASPFileSpecialType specialType = null;
                JSONObject fileInfo = files.getJSONObject(i);
                String name = fileInfo.optString("name");
                String specialStr = json.optString("special");
                if (StringUtils.isNotEmpty(specialStr)) {
                    try {
                        specialType = ASPFileSpecialType.valueOf(specialStr);
                    } catch (Exception e) {
                        Log.e(this, "getFileBrowserFromCache() - Exception : " + e.getMessage());
                    }
                }
                // parent directory ("..") is limited to first file only (isn't returned on searches or next windows)
                CachedCloudFile file = new CachedCloudFile();
                boolean isDir = fileInfo.optBoolean("isDirectory", false);
                if (isDir) {
                    file.setDirectory(true);
                }
                file.setLength(fileInfo.optLong("fileSize", 0));
                file.setName(name);
                file.setLastModified(fileInfo.optLong("modified", 0));
                file.setSpecialType(specialType);
                file.setCloudId(fileInfo.optString("cloudId", ""));
                Bundle objBundle = new Bundle();
                objBundle.putInt("asp_mediaId", fileInfo.optInt("asp_mediaId", -1));
                objBundle.putInt("asp_mediaType", fileInfo.optInt("asp_mediaType", 0));
                objBundle.putInt("asp_orientation", fileInfo.optInt("asp_orientation", 0));
                objBundle.putInt("asp_width", fileInfo.optInt("asp_width", 0));
                objBundle.putInt("asp_height", fileInfo.optInt("asp_height", 0));
                objBundle.putLong("asp_albumId", fileInfo.optLong("asp_albumId", 0));
                objBundle.putString("asp_full_uri", fileInfo.optString("asp_full_uri", null)); //jsublee_150901
                objBundle.putString("asp_mimetype", fileInfo.optString("asp_mimetype", null)); //jsublee_150901
                objBundle.putInt("asp_childCnt", fileInfo.optInt("asp_childCnt", 0)); //jsub12_151006
                objBundle.putInt("asp_childDirCnt", fileInfo.optInt("asp_childDirCnt", 0)); //jsub12_151006
                objBundle.putInt("asp_descendantsCnt", fileInfo.optInt("asp_descendantsCnt", 0));
                objBundle.putInt("asp_descendantDirCnt", fileInfo.optInt("asp_descendantDirCnt", 0));
                objBundle.putString("asp_trashProcessing", fileInfo.optString("asp_trashProcessing", null));

                file.setDeviceId(fileInfo.optInt("asp_deviceId", 0));
                file.setEtcDataObject(objBundle);
                if (i == 0) {
                    mCurrentDirectory = file;
                    mCurrentDirPath = file.getName();
                } else if (i == 1) {
                    mStateLock.lock();
                    try {
                        mParentDirectory = file;
                    } finally {
                        mStateLock.unlock();
                    }
                } else {
                    mFileCache.add(file);
                    if (isDir) {
                        mSelectionHelper.setIsDirectory(totalCount);
                    }
                    totalCount++;
                }
            }
            mDirRevision = dirRevision;
            mOldCntOfGrandChildren = totalCntOfGrandChildren;
            checkTmpAddedFileList(mFileCache);
            checkSortOption(mFileCache);
            checkTmpRemovedFileList(mFileCache);
            checkTmpRemovedTrash(mFileCache);
            checkTmpRenamedFileList(mFileCache);
            mTotalFileCount = mFileCache.size();
            Log.i(this, "getFileBrowserFromCache() - end " + mTotalFileCount);
        } catch (JSONException e) {
            Log.e(this, "getFileBrowserFromCache() - JSONException : " + e.getMessage());
        }
    }

    private void checkSortOption(ArrayList<CachedCloudFile> fileCache) {
        if (mStrOrgSortOption != null) {
            String[] arrSplit = StringUtils.split(mStrOrgSortOption, ',');
            if (arrSplit != null) {
                ASPFileSortType type;
                for (int nSortCnt = arrSplit.length - 1; nSortCnt >= 0; nSortCnt--) {
                    String strSortPureOption = arrSplit[nSortCnt].trim();
                    type = ASPFileSortType.getSortTypeFromCursorSortOrder(strSortPureOption);
                    if (type != null) {
                        Log.i(this, "checkSortOption type=" + type);
                        Collections.sort(fileCache, new VersatileFileBrowserComparator(type));
                    }
                }
            } else {
                Log.e(this, "checkSortOption() arrSplit is null");
            }
        } else {
            Log.i(this, "checkSortOption mSortType=" + mSortType);
            if (mSortType != null)
                Collections.sort(fileCache, new VersatileFileBrowserComparator(mSortType));
        }
    }

    public boolean initCache(String storageGatewayID, ASPFileSortType sortType, boolean forceReload, ASPFileProvider provider, String strOrgSortOption, String selection, String[] selectionArgs, boolean isBrowserForView) {
        mOrgProvider = provider;
        mStorageGatewayID = storageGatewayID;
        mSortType = sortType;
        mForceReload = forceReload;
        mStrOrgSortOption = strOrgSortOption;
        mIsBrowserForView = isBrowserForView;

        if (mDevice == null) {
            Log.e(this, "initCache() - mDevice == null");
            return false;
        }

        if (sortType == null) {
            sortType = ASPFileSortType.DATE_MODIFIED_ASC;
        }
        mSelection = selection;
        mSelectionArgs = selectionArgs;

        mObjectKey = storageGatewayID + "_" + selection + "_" + sortType + "_" + System.currentTimeMillis();
        if (TextUtils.isEmpty(storageGatewayID)) {
            storageGatewayID = CloudStorageConstants.CLOUD_ROOT;
            Log.i(this, "initCache() - strFileId changed from null to root");
        }
        setCancellationSignal(new CancellationSignal(), storageGatewayID);
        sInstanceMap.put(mObjectKey, this);
        ////load cache data
        Log.i(this, "initCache() - beforeLoadCache " + storageGatewayID + " " + mDevice.getId());
        try {
            if (mDevice.isLocalDevice() == false) {
                ByteArrayOutputStream retBuffer = new ByteArrayOutputStream();
                long nLmt = NetResourceCacheManager.getInstance(mContext).loadCache(URI_MATCHER + storageGatewayID, null, mDevice.getId(), retBuffer);
                if (nLmt >= 0) {
                    getFileBrowserFromCache(retBuffer);
                    mFileBrowserStatus = STATE_CACHE_CHECKED;
                } else {
                    Log.i(this, "initCache() - miss cached filelist id=" + storageGatewayID + ", device=" + mDevice.getId());
                }
            }
        } catch (Exception e) {
            Log.e(this, "initCache() - Exception : " + e.getMessage());
            return false;
        }
        ////save real directory info to cache
        try {
            if (mSelection == null && mFileBrowserStatus == STATE_CACHE_CHECKED) {
                mOldDirRevision = mDirRevision;
                mOldTotalFileCount = mTotalFileCount;

                long nUpdatedSec = NetResourceCacheManager.getInstance(mContext).getCheckTime(URI_MATCHER + storageGatewayID, null, mDevice.getId());

                if ((nUpdatedSec >= 0) && (nUpdatedSec < INTERVAL_MIN_CHECKREVISION_SEC)) {
                    Log.i(this, "initCache() - CachedFileBrowser no need check. LMT updated before " + nUpdatedSec + "sec");
                    mFileBrowserStatus = STATE_SAVE_CACHE_DONE;
                } else {
                    Log.i(this, "initCache() - saveFileBrowserToCache 1 : LMT updated before " + nUpdatedSec + "sec");
                    return saveFileBrowserToCache(null);
                }
            } else if (mFileBrowserStatus != STATE_CACHE_CHECKED) {
                Log.i(this, "initCache() - saveFileBrowserToCache 2");
                return saveFileBrowserToCache(null);
            }
        } catch (Exception e) {
            mOrgBrowser = null;
            Log.e(this, "initCache() - Exception : " + e.getMessage());
            return false;
        }
        if (mSelection != null) {
            doFiltering(mFileCache);
        }
        return true;
    }

    private boolean StringEqualCheck(String strVal1, String strVal2, boolean bIgnoreCase) {
        if (strVal1 == null || strVal2 == null)
            return false;

        return (bIgnoreCase) ? strVal1.equalsIgnoreCase(strVal2) : strVal1.equals(strVal2);
    }

    private int doFiltering(ArrayList<CachedCloudFile> arrFileList) {
        boolean bIgnore = false;
        int nFieldType = 0;
        try {
            String[] arrParsed = this.mSelection.split("=");
            if (arrParsed != null && arrParsed.length > 0 && mSelectionArgs != null && mSelectionArgs.length > 0) {
                String strField = arrParsed[0];
                String strValue = mSelectionArgs[0];
                if (arrFileList != null && arrFileList.size() > 0 && strField != null && strValue != null) {
                    if (strField.indexOf("_display_name") >= 0) {
                        nFieldType = (strField.indexOf('!') >= 0) ? 1 : 2;
                    } else if (strField.indexOf("is_directory") >= 0) {
                        nFieldType = 3;
                    } else if (strField.indexOf(CloudGatewayMediaStore.FileBrowser.FileBrowserColumns.FILE_ID) >= 0) { //add types as per the request from myFiles
                        nFieldType = (strField.indexOf('!') >= 0) ? 4 : 5;
                    } else if (strField.indexOf("document_id") >= 0) {
                        nFieldType = 6;
                    }

                    Log.i(this, "doFiltering() - strValue=" + strValue + ",nFieldType=" + nFieldType);
                    if (strField.indexOf("_ignore_case") >= 0) {
                        bIgnore = true;
                    }

                    CachedCloudFile tmpFile;
                    Iterator<CachedCloudFile> it = arrFileList.iterator();
                    while (it.hasNext()) {
                        tmpFile = it.next();
                        if (tmpFile != null) {
                            if (nFieldType == 1 && StringEqualCheck(strValue, tmpFile.getName(), bIgnore)) {
                                it.remove();
                            } else if (nFieldType == 2 && StringEqualCheck(strValue, tmpFile.getName(), bIgnore) == false) {
                                it.remove();
                            } else if (nFieldType == 3 && strValue.equals("0") && tmpFile.isDirectory()) {
                                it.remove();
                            } else if (nFieldType == 3 && strValue.equals("1") && tmpFile.isDirectory() == false) {
                                it.remove();
                            } else if (nFieldType == 4 && StringEqualCheck(strValue, tmpFile.getCloudId(), bIgnore)) {  //add types as per the request from myFiles
                                it.remove();
                            } else if (nFieldType == 5 && StringEqualCheck(strValue, tmpFile.getCloudId(), bIgnore) == false) {
                                it.remove();
                            } else if (nFieldType == 6 && StringEqualCheck(strValue, ((Bundle) tmpFile.getEtcDataObject()).getString("document_id"), bIgnore) == false) {
                                it.remove();
                            }
                        }
                    }
                    mTotalFileCount = arrFileList.size();
                    Log.i(this, "doFiltering size=" + mTotalFileCount);
                }
            }
        } catch (Exception e) {
            Log.e(this, "doFiltering() - Exception : " + e.getMessage());
        }

        return (arrFileList != null) ? arrFileList.size() : 0;
    }

    public void setCancellationSignal(CancellationSignal cancellationSignal, String fileId) {
        mCancellationSignal = cancellationSignal;
        if (cancellationSignal == null) {
            Log.i(this, "setCancellationSignal() - cancellationSignal is null");
            return;
        }
        if (mOrgProvider != null) {
            try {
                Class klass = mOrgProvider.getClass();
                Log.i(this, "setCancellationSignal() - mOrgProvider class name = " + klass);
                Class[] paramTypes = {CancellationSignal.class, String.class};
                Method m = klass.getMethod("setCancellationSignal", paramTypes);
                Object[] arguments = {cancellationSignal, fileId};
                m.invoke(mOrgProvider, arguments);
                Log.i(this, "setCancellationSignal() - success!!");
            } catch (Exception e) {
                Log.e(this, "setCancellationSignal() - Exception : " + e.getMessage());
            }
        }
    }

    @Override
    public boolean init(ASPFile arg0, ASPFileSortType arg1, boolean arg3) throws InterruptedException, IOException {
        ////do nothing here
        Log.i(this, "CachedFileBrowser do nothing in init function!");
        return true;
    }

    public void notifyChange(boolean bForced) {
        Log.i(this, "CachedFileBrowser notifyChange!");
        if (bForced || mFileBrowserStatus != CachedFileBrowser.STATE_ZERO) {
            Log.i(this, "CachedFileBrowser mFileBrowserStatus=" + mFileBrowserStatus + ", count=" + mTotalFileCount);
            for (DataSetObserver observer : this.dataSetObservers) {
                Log.i(this, "CachedFileBrowser observer uri=" + observer);
                observer.onInvalidated();
            }
        }
    }

    public void tryToCheck() {
        Log.i(this, "tryToCheck key =" + mObjectKey + ", mStorageGatewayID=" + mStorageGatewayID + ", name=" + mCurrentDirPath + ", mFileBrowserStatus=" + mFileBrowserStatus);
        if (mFileBrowserStatus != STATE_SAVE_CACHE_DONE)
            return;

        boolean bRet = NetResourceCacheManager.getInstance(mContext).checkExist(URI_MATCHER + mStorageGatewayID, null, mDevice.getId());
        if (!bRet) {
            mOldTotalFileCount = 0;
        }
        Log.i(this, "tryToCheck create VfbBackgroundJob " + bRet + "- for key = " + mObjectKey);
        CachedExecutorService.getInstance().submit(new VfbBackgroundJob());
    }

    private class VfbBackgroundJob implements Callable<Void> {

        @Override
        public Void call() throws Exception {
            try {
                boolean before = false;
                synchronized (mDuringTaskSync) {
                    before = mFlagTask;
                    mFlagTask = true;
                }

                if (!before && sBgTaskCount < MAX_BG_JOB) {
                    sBgTaskCount++;

                    Log.i(this, "VfbBackgroundJob sBgTaskCount = " + sBgTaskCount + "-key = " + mObjectKey);
                    CloudStorageSync.Result result = new CloudStorageSync.Result(true);

                    if (saveFileBrowserToCache(result)) {
                        Log.i(this, "mOldDirRevision = " + mOldDirRevision + ", mDirRevision = " + mDirRevision
                                + "\nmOldTotalFileCount = " + mOldTotalFileCount + ", mTotalFileCount = " + mTotalFileCount
                                + "\nmOldCntOfGrandChildren = " + mOldCntOfGrandChildren + ", mCntOfGrandChildren = " + mCntOfGrandChildren
                                + ", isChanged = " + result.mRet + ", mFileBrowserStatus=" + mFileBrowserStatus);

                        if (mFileBrowserStatus >= STATE_CACHE_CHECKED && result.mRet) {
                            CachedFileBrowser.this.mOldDirRevision = CachedFileBrowser.this.mDirRevision;
                            mOldTotalFileCount = mTotalFileCount;
                            mOldCntOfGrandChildren = mCntOfGrandChildren;
                            notifyChange(false);
                        }

                        if (sBgTaskCount > 0)
                            sBgTaskCount--;
                    }
                }
            } catch (Exception e) {
                Log.e(this, "call() - Exception : " + e.getMessage());

                if (sBgTaskCount > 0)
                    sBgTaskCount--;
            } finally {
                synchronized (mDuringTaskSync) {
                    mFlagTask = false;
                }
            }
            return null;
        }
    }

    private static class VersatileFileBrowserComparator implements Comparator<ASPFile> {

        private final ASPFileSortType sortType;

        public VersatileFileBrowserComparator(ASPFileSortType sortType) {
            this.sortType = sortType;
        }

        @Override
        public int compare(ASPFile o1, ASPFile o2) {
            // always sort directories above files
            int dirDiff = directoryFirstDiff(o1, o2);
            if (dirDiff != 0) {
                return dirDiff;
            }
            switch (sortType) {
                case DATE_MODIFIED_ASC:
                    return lastModifiedDiff(o1, o2);
                case DATE_MODIFIED_DESC:
                    return lastModifiedDiff(o2, o1);
                case SIZE_ASC:
                    return sizeDiff(o1, o2);
                case SIZE_DESC:
                    return sizeDiff(o2, o1);
                case NAME_ASC:
                    return nameDiff(o1, o2);
                case NAME_DESC:
                    return nameDiff(o2, o1);
                case TYPE_ASC:
                    return typeDiff(o1, o2);
                default:
                    return typeDiff(o2, o1);
            }
        }

        protected int lastModifiedDiff(ASPFile o1, ASPFile o2) {
            long diff = (o1.lastModified() - o2.lastModified());
            if (diff == 0) {
                // secondary compare - name
                return nameDiff(o1, o2);
            }
            return diff > 0 ? 1 : -1;
        }

        protected int sizeDiff(ASPFile o1, ASPFile o2) {
            long diff = (o1.length() - o2.length());
            if (diff == 0) {
                // secondary compare - name
                return nameDiff(o1, o2);
            }
            return diff > 0 ? 1 : -1;
        }

        protected int nameDiff(ASPFile o1, ASPFile o2) {
            CollationKey k1 = Collator.getInstance().getCollationKey(o1.getName().toLowerCase());
            CollationKey k2 = Collator.getInstance().getCollationKey(o2.getName().toLowerCase());

            if ((k1 == null) && (k2 == null)) {
                return 0;
            } else if (k1 == null) {
                return -1;
            } else if (k2 == null) {
                return 1;
            } else {
                return k1.compareTo(k2);
            }
        }

        protected int typeDiff(ASPFile o1, ASPFile o2) {
            String extension1 = extension(o1.getName());
            String extension2 = extension(o2.getName());
            return stringDiff(extension1, extension2);
        }

        protected String extension(String s1) {
            if (s1 == null) {
                return null;
            }
            int lastIndexOfLastDot = StringUtils.lastIndexOf(s1, '.');
            if (lastIndexOfLastDot < 0) {
                return null;
            }
            return s1.substring(lastIndexOfLastDot + 1);
        }

        protected int stringDiff(String s1, String s2) {
            if (s1 == null && s2 == null) {
                return 0;
            }
            if (s1 == null) {
                return -1;
            }
            if (s2 == null) {
                return +1;
            }
            return s1.compareToIgnoreCase(s2);
        }

        /**
         * always sort directories before files - in any sort type - if both are directories, always sort by name A-Z
         */
        protected int directoryFirstDiff(ASPFile o1, ASPFile o2) {
            int o1Val = (o1.isDirectory()) ? 0 : 1;
            int o2Val = (o2.isDirectory()) ? 0 : 1;

            // if both directories, sort by name A-Z (EXCEPT last modified date, which a directory has)
            if ((this.sortType != ASPFileSortType.DURATION_ASC && this.sortType != ASPFileSortType.DURATION_DESC) && o1Val == 0 && o2Val == 0) {
//				return nameDiff(o1, o2);
                switch (this.sortType) {
                    case DATE_MODIFIED_ASC:
                        return lastModifiedDiff(o1, o2);
                    case DATE_MODIFIED_DESC:
                        return lastModifiedDiff(o2, o1);
                    case SIZE_ASC:
                        return sizeDiff(o1, o2);
                    case SIZE_DESC:
                        return sizeDiff(o2, o1);
                    case NAME_ASC:
                        return nameDiff(o1, o2);
                    case NAME_DESC:
                        return nameDiff(o2, o1);
                    case TYPE_ASC:
                        return typeDiff(o1, o2);
                    default:
                        return typeDiff(o2, o1);
                }
            } else {
                return o1Val - o2Val;
            }
        }
    }
}

/**
 *
 */

package platform.com.mfluent.asp.media;

import org.apache.commons.lang3.builder.HashCodeBuilder;

import android.net.Uri;

import platform.com.mfluent.asp.datamodel.DeviceSLPF;

/**
 * @author michaelgierach
 */
public class AspMediaInfo {

	private final long entryId;
	private final DeviceSLPF mDevice;

	private final int mediaType;     // see
									// com.mfluent.asp.datasource.AspMediaId
	private final String mDeviceMediaId;
	private final Uri uri;
	private final String sourceUri;
	private final long offset;
	private final long length;

	public AspMediaInfo(long entryId, DeviceSLPF device, int mediaType, String deviceMediaId, Uri uri, String sourceUri) {
		this(entryId, device, mediaType, deviceMediaId, uri, sourceUri, 0L, -1L);
	}

	private AspMediaInfo(long entryId, DeviceSLPF device, int mediaType, String deviceMediaId, Uri uri, String sourceUri, long offset, long length) {
		this.entryId = entryId;
		this.mDevice = device;
		this.mediaType = mediaType;
		this.mDeviceMediaId = deviceMediaId;
		this.uri = uri;
		this.sourceUri = sourceUri;
		this.offset = offset;
		this.length = length;
	}

	public final long getOffset() {
		return this.offset;
	}

	public final long getLength() {
		return this.length;
	}

	public final String getDeviceMediaId() {
		return this.mDeviceMediaId;
	}

	public final DeviceSLPF getDevice() {
		return this.mDevice;
	}

	public int getMediaType() {
		return this.mediaType;
	}

	/**
	 * @return the entryId
	 */
	public long getEntryId() {
		return this.entryId;
	}

	/**
	 * @return the uri
	 */
	public Uri getUri() {
		return this.uri;
	}

	/**
	 * @return the sourceUri
	 */
	public String getSourceUri() {
		return this.sourceUri;
	}

	@Override
	public String toString() {
		return "_ID:" + this.entryId + " local id:" + this.mDeviceMediaId + " uri:" + this.uri;
	}

	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}

		if (o instanceof AspMediaInfo) {
			AspMediaInfo other = (AspMediaInfo) o;
			return ((other.entryId == this.entryId) && (other.mediaType == this.mediaType) && (other.offset == this.offset) && (other.length == this.length));
		}

		return false;
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(this.entryId).append(this.mediaType).append(this.offset).append(this.length).hashCode();
	}
}

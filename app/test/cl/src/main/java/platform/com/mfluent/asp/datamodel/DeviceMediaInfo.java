/**
 *
 */

package platform.com.mfluent.asp.datamodel;

import android.net.Uri;

import com.mfluent.asp.common.datamodel.ASPMediaStore;

/**
 * @author michaelgierach
 */
public class DeviceMediaInfo extends PublicMediaInfo {
    static final String TABLE_NAME = "DEVICES";

    private static final class InstanceHolder {

        private static DeviceMediaInfo sInstance = new DeviceMediaInfo();
    }

    public static DeviceMediaInfo getInstance() {
        return InstanceHolder.sInstance;
    }

    private DeviceMediaInfo() {

    }

    @Override
    public Uri getEntryUri(long entryId) {
        return MediaInfo.buildEntryIdUri(entryId, ASPMediaStore.Device.PATH);
    }

    @Override
    public Uri getContentUriForDevice(long deviceId) {
        return MediaInfo.buildDeviceContentUri(deviceId, ASPMediaStore.Device.PATH);
    }

    @Override
    public Uri getContentUri() {
        return ASPMediaStore.Device.CONTENT_URI;
    }

    @Override
    protected String getPrivateQueryTableName() {
        return TABLE_NAME;
    }

    @Override
    public String getInsertUpdateDeleteTableName() {
        return TABLE_NAME;
    }

    @Override
    public String getEntryContentType() {
        return ASPMediaStore.Device.ENTRY_CONTENT_TYPE;
    }

    @Override
    public String getContentType() {
        return ASPMediaStore.Device.CONTENT_TYPE;
    }

    @Override
    public String[] getStreamContentType(MediaInfoContext mediaInfoContext) {
        return null;
    }

    @Override
    protected String getDeviceIdColumn() {
        return null;
    }
}

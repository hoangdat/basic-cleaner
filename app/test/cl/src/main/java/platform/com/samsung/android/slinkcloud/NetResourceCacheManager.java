
package platform.com.samsung.android.slinkcloud;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

import com.mfluent.log.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.locks.ReentrantLock;

import uicommon.com.mfluent.asp.util.CachedExecutorService;

public class NetResourceCacheManager {

    private static final String CACHE_DIR_NAME = "netres_cache";
    private static final String DATABASE_NAME = "asp_cache.db";
    private static final int DATABASE_VERSION = 1;

    ////TBD: if too many cache over MAX_NETRES_CACHE_SIZE, then reduce cache files until over  NORMAL_NETRES_CACHE_SIZE
    private static final int MAX_NETRES_CACHE_SIZE = (100 * 1024 * 1024);
    private static final int NORMAL_NETRES_CACHE_SIZE = (MAX_NETRES_CACHE_SIZE / 2);
    //private static final int MAX_NETRES_CACHE_SIZE = (500 * 1024);
    //private static final int NORMAL_NETRES_CACHE_SIZE = (MAX_NETRES_CACHE_SIZE / 2);
    private static final int MAX_BGTHREAD_THROTTLE = 1000;
    private static final int BGTHREAD_INTERVAL_MSEC = (1000 * 60 * 60 * 12); ////12 hours

    private static final String TABLE_CACHE = "cache_table";
    private static final String COLUMNS_KEY = "KEY";
    private static final String COLUMNS_UPDATED_TIME = "updated_time";
    private static final String COLUMNS_USED_TIME = "checked_time";
    private static final String COLUMNS_LAST_MODIFIED_TIME = "last_modified_time";
    private static final String COLUMNS_FILE_ID = "asp_id";
    private static final String COLUMNS_FILE_NAME = "filename";
    private static final String COLUMNS_ORIGINAL_SIZE = "original_size";
    private static final String COLUMNS_CACHED_SIZE = "cached_size";
    private static final String COLUMNS_MIME_TYPE = "mime_type";
    private static final String COLUMNS_DEVICE_ID = "device_id";
    private static final String COLUMNS_URI = "uri";
    private static final String COLUMNS_URI_PARAM = "uri_param";
    private static final String COLUMNS_NAME = "name";
    private static final String COLUMNS_ETC = "etc";

    private static final String TABLE_CACHE_CREATE = "CREATE TABLE "
            + TABLE_CACHE
            + "("
            + COLUMNS_KEY
            + " TEXT PRIMARY KEY, "
            + COLUMNS_UPDATED_TIME
            + " INTEGER, "
            + COLUMNS_USED_TIME
            + " INTEGER, "
            + COLUMNS_LAST_MODIFIED_TIME
            + " INTEGER, "
            + COLUMNS_FILE_ID
            + " INTEGER, "
            + COLUMNS_FILE_NAME
            + " TEXT, "
            + COLUMNS_ORIGINAL_SIZE
            + " INTEGER, "
            + COLUMNS_CACHED_SIZE
            + " INTEGER, "
            + COLUMNS_MIME_TYPE
            + " TEXT, "
            + COLUMNS_DEVICE_ID
            + " INTEGER, "
            + COLUMNS_URI
            + " TEXT, "
            + COLUMNS_URI_PARAM
            + " TEXT, "
            + COLUMNS_NAME
            + " TEXT, "
            + COLUMNS_ETC
            + " TEXT)";

    private static final String TABLE_CACHE_DROP = "DROP TABLE IF EXISTS " + TABLE_CACHE;

    private static CacheDBHelper mDBHelper;
    private static NetResourceCacheManager mInstance = null;
    private Context mContext = null;
    private final ReentrantLock mCacheSync = new ReentrantLock();
    private SQLiteDatabase mDB;
    private String mBaseDirName = "dummdirectory";

    private long nLastChangeTime = 0;
    private Future<Void> mBgJob = null;

    private String makeKeyColumn(String strUri, String strParam, int nDevId) {
        StringBuffer strRet = new StringBuffer("" + nDevId + "_" + strUri);
        if (strParam != null) {
            strRet.append("_" + strParam);
        }
        return strRet.toString();
    }

    private File makeTmpFileUsingKey(String strKey) {
        long nHash = strKey.hashCode();
        if (nHash < 0) {
            nHash = -nHash;
        }
        String strTmpFileName = mBaseDirName + "/data_" + nHash + ".tmp";
        File file = new File(strTmpFileName);
        int nCount = 1;
        while (file.exists()) {
            strTmpFileName = this.mBaseDirName + "/data_" + nHash + "_" + nCount + ".tmp";
            file = new File(strTmpFileName);
            nCount++;
        }
        return file;
    }

    private boolean checkNeedBGJob() {
        boolean bRet = false;
        if (mBgJob != null && !mBgJob.isCancelled() && mBgJob.isDone()) {
            return false;
        }
        long nCurTime = System.currentTimeMillis();
        if ((nLastChangeTime == 0) || (nCurTime > BGTHREAD_INTERVAL_MSEC + nLastChangeTime)) {
            bRet = true;
        }
        if (bRet) {
            mBgJob = CachedExecutorService.getInstance().submit(new CheckCacheBackgroudJob());
        }
        nLastChangeTime = nCurTime;
        return bRet;
    }

    private long getFolderSize(String strPath, boolean isInBGJob) {
        long nTotalSize = 0;
        int nCount = 0;
        try {
            File file = new File(strPath);
            File[] childFileList = file.listFiles();
            if (childFileList == null) {
                return 0;
            }
            for (File childFile : childFileList) {
                if (childFile.isDirectory() == false) {
                    nTotalSize += childFile.length();
                }
                nCount++;
                if (nCount > MAX_BGTHREAD_THROTTLE && isInBGJob) {
                    nCount = 0;
                    ////in order no to be detected battery draining APK, slow down bg threads.
                    Thread.sleep(100);
                }
            }
        } catch (Exception e) {
            Log.e(this, "getFolderSize() - Exception error during CheckCacheBackgroudJob : " + e);
        }
        return nTotalSize;
    }

    @Override
    protected void finalize() throws Throwable {
        try {
            if (mDBHelper != null) {
                mDBHelper.close();
            }
            if (this.mBgJob != null && !this.mBgJob.isCancelled() && this.mBgJob.isDone()) {
                this.mBgJob.cancel(true);
            }
        } catch (Exception e) {
            Log.e(this, "finalize() - Exception error in finalize : " + e);
        }
        super.finalize();
    }

    public static NetResourceCacheManager getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new NetResourceCacheManager(context);
        }
        return mInstance;
    }

    public NetResourceCacheManager(Context context) {
        mContext = context;
        if (mDBHelper == null) {
            mDBHelper = new CacheDBHelper(context, DATABASE_NAME, null, DATABASE_VERSION);
        }
        if (mDB == null) {
            mDB = mDBHelper.getWritableDatabase();
        }
        File baseDir = mContext.getDir(CACHE_DIR_NAME, Context.MODE_PRIVATE);
        if (baseDir != null) {
            mBaseDirName = baseDir.getAbsolutePath();
            Log.i(this, "NetResourceCacheManager() - mBaseDirName : " + mBaseDirName);
        }
    }

    public void deleteCache(String strUri, String strParam, int nDevId) {
        StringBuffer strSql;

        ////first, select from SQL
        if (strUri != null) {
            strSql = new StringBuffer("SELECT * FROM " + TABLE_CACHE + " WHERE " + COLUMNS_URI + " = \"" + strUri + "\" AND " + COLUMNS_DEVICE_ID + " = " + nDevId);
        } else {
            strSql = new StringBuffer("SELECT * FROM " + TABLE_CACHE + " WHERE " + COLUMNS_DEVICE_ID + " = " + nDevId);
        }
        if (strParam != null) {
            strSql.append(" AND " + COLUMNS_URI_PARAM + " = \"" + strParam + "\";");
        } else {
            strSql.append(";");
        }
        mCacheSync.lock();
        try {
            ////check if the cache file exist
            Cursor cursor = this.mDB.rawQuery(strSql.toString(), null);
            if (cursor != null) {
                if (cursor.getCount() > 0) {
                    StringBuffer strPath;
                    File file;
                    while (cursor.moveToNext()) {
                        strPath = new StringBuffer(mBaseDirName + "/" + cursor.getString(cursor.getColumnIndex(COLUMNS_FILE_NAME)));
                        file = new File(strPath.toString());
                        if (file != null && file.exists()) {
                            file.delete();
                            Log.i(this, "deleteCache() - Delete NetResCache! url=" + strUri);
                        }
                    }
                }

                cursor.close();
            }
            ////delete all items from DB table
            strSql.delete(0, strSql.length());
            if (strUri != null) {
                strSql.append(COLUMNS_URI + " =? AND " + COLUMNS_DEVICE_ID + " =?");
                String[] args;
                if (strParam != null) {
                    strSql.append(" AND " + COLUMNS_URI_PARAM + " =?");
                    args = new String[]{strUri, "" + nDevId, strParam};
                } else {
                    args = new String[]{strUri, "" + nDevId};
                }
                mDB.delete(TABLE_CACHE, strSql.toString(), args);
            } else {
                strSql.append(COLUMNS_DEVICE_ID + " =?");
                String[] args;
                if (strParam != null) {
                    strSql.append(" AND " + COLUMNS_URI_PARAM + " =?");
                    args = new String[]{"" + nDevId, strParam};
                } else {
                    args = new String[]{"" + nDevId};
                }
                mDB.delete(TABLE_CACHE, strSql.toString(), args);
            }
        } catch (Exception e) {
            Log.e(this, "deleteCache() - Eception during deleteCache : " + e);
        } finally {
            mCacheSync.unlock();
        }
    }

    public boolean checkExist(String strUri, String strParam, int nDevId) {
        boolean bRet = false;

        ////check parameter first
        if (strUri == null) {
            return false;
        }
        String strKey = makeKeyColumn(strUri, strParam, nDevId);
        String strSql = "SELECT * FROM " + TABLE_CACHE + " WHERE " + COLUMNS_KEY + " = \"" + strKey + "\";";
        mCacheSync.lock();
        try {
            ////check if the cache file exist
            Cursor cursor = this.mDB.rawQuery(strSql, null);
            if (cursor != null) {
                bRet = (cursor.getCount() > 0);
                cursor.close();
            }
        } catch (Exception e) {
            Log.e(this, "checkExist() - Exception error during getCheckTime : " + e);
        } finally {
            mCacheSync.unlock();
        }
        return bRet;
    }

    public long getCheckTime(String strUri, String strParam, int nDevId) {
        long nCurTime = (System.currentTimeMillis() / 1000);
        long nOldTime = 0;
        ////check parameter first
        if (strUri == null) {
            return -1;
        }
        String strKey = makeKeyColumn(strUri, strParam, nDevId);
        String strSql = "SELECT * FROM " + TABLE_CACHE + " WHERE " + COLUMNS_KEY + " = \"" + strKey + "\";";
        mCacheSync.lock();
        try {
            ////check if the cache file exist
            Cursor cursor = this.mDB.rawQuery(strSql, null);
            if (cursor != null && cursor.getCount() > 0) {
                cursor.moveToFirst();
                nOldTime = cursor.getLong(cursor.getColumnIndex(COLUMNS_UPDATED_TIME));
            } else {
                Log.i(this, "getCheckTime() - can't find url=" + strUri);
            }
            if (cursor != null) {
                cursor.close();
            }

        } catch (Exception e) {
            Log.e(this, "getCheckTime() - Exception error during getCheckTime : " + e);
        } finally {
            mCacheSync.unlock();
        }
        Log.d(this, "getCheckTime() - cur : " + nCurTime + " ,old : " + nOldTime + " ,ret : " + (nCurTime - nOldTime));
        return nCurTime - nOldTime;
    }

    public long loadCache(String strUri, String strParam, int nDevId, ByteArrayOutputStream retBuffer) {
        long retLM = -1;
        long tmpLM = -1;
        StringBuffer strPath = null;

        ////check parameter first
        if (strUri == null || retBuffer == null) {
            return retLM;
        }
        String strKey = makeKeyColumn(strUri, strParam, nDevId);
        String strSql = "SELECT * FROM " + TABLE_CACHE + " WHERE " + COLUMNS_KEY + " = \"" + strKey + "\";";
        mCacheSync.lock();
        try {
            ////check if the cache file exist
            Cursor cursor = mDB.rawQuery(strSql, null);
            if (cursor != null) {
                if (cursor.getCount() > 0) {
                    cursor.moveToFirst();
                    strPath = new StringBuffer(cursor.getString(cursor.getColumnIndex(COLUMNS_FILE_NAME)));
                    tmpLM = cursor.getLong(cursor.getColumnIndex(COLUMNS_LAST_MODIFIED_TIME));
                }

                cursor.close();
            }
            if (strPath != null) {
                ////second, update db to keep cache size small
                long nCurTime = System.currentTimeMillis();
                nCurTime = nCurTime / 1000;
                ContentValues contentToUpdate = new ContentValues();
                contentToUpdate.put(COLUMNS_USED_TIME, nCurTime);
                mDB.update(TABLE_CACHE, contentToUpdate, COLUMNS_KEY + "=?", new String[]{strKey});
            }
        } catch (Exception e) {
            strPath = null;
            Log.e(this, "loadCache() - Exception error during getCachedFile, " + e);
        } finally {
            mCacheSync.unlock();
        }
        if (strPath == null) {
            return retLM;
        }
        ////check cache file
        strPath.insert(0, this.mBaseDirName + "/");

        File file = new File(strPath.toString());
        Log.d(this, "loadCache() - cache file location : " + strPath);
        if (file != null && file.exists()) {
            try (FileInputStream fis = new FileInputStream(file)) {
                byte[] buffer = new byte[8196];
                int readcount;
                while ((readcount = fis.read(buffer)) != -1) {
                    retBuffer.write(buffer, 0, readcount);
                }
                retLM = tmpLM;
            } catch (Exception e) {
                Log.e(this, "loadCache() - Exception : can't read cache file to memory buffer e = " + e);
            }
        }
        //checkNeedBGJob();
        return retLM;
    }

    public boolean saveCache(String strUri, String strParam, int nDevId, long lmt, ByteArrayOutputStream inBuffer, String strMimeType) {
        boolean bRet = true;
        String strKey;
        long nCurTime;
        long nCacheFileSize = 0;
        File tmpFile = null;
        byte[] dataArray;

        ////check parameter
        if (strUri == null || lmt < 0 || inBuffer == null || strMimeType == null) {
            return false;
        }
        strKey = makeKeyColumn(strUri, strParam, nDevId);

        mCacheSync.lock();
        try {
            ////first, save tmp cache file
            tmpFile = makeTmpFileUsingKey(strKey);
            dataArray = inBuffer.toByteArray();
            nCacheFileSize = dataArray.length;

            try (FileOutputStream fsOutput = new FileOutputStream(tmpFile)) {
                fsOutput.write(dataArray);
            }
        } catch (Exception e1) {
            bRet = false;
            Log.e(this, "saveCache() - Exception : " + e1.getMessage());
        } finally {
            mCacheSync.unlock();
        }

        if (bRet == false || tmpFile == null) {
            return bRet;
        }
        ////second, update db
        nCurTime = System.currentTimeMillis();
        nCurTime = nCurTime / 1000;
        ContentValues initialValues = new ContentValues();
        initialValues.put(COLUMNS_KEY, strKey);
        initialValues.put(COLUMNS_FILE_NAME, tmpFile.getName());
        initialValues.put(COLUMNS_LAST_MODIFIED_TIME, lmt);
        initialValues.put(COLUMNS_UPDATED_TIME, nCurTime);
        initialValues.put(COLUMNS_USED_TIME, nCurTime);
        initialValues.put(COLUMNS_MIME_TYPE, strMimeType);
        initialValues.put(COLUMNS_DEVICE_ID, nDevId);
        initialValues.put(COLUMNS_URI, strUri);
        initialValues.put(COLUMNS_ORIGINAL_SIZE, nCacheFileSize);
        initialValues.put(COLUMNS_CACHED_SIZE, nCacheFileSize);
        if (strParam != null) {
            initialValues.put(COLUMNS_URI_PARAM, strParam);
        }
        mCacheSync.lock();
        try {
            int id = (int) mDB.insertWithOnConflict(TABLE_CACHE, null, initialValues, SQLiteDatabase.CONFLICT_IGNORE);
            if (id == -1) {
                mDB.update(TABLE_CACHE, initialValues, COLUMNS_KEY + "=?", new String[]{strKey});
            }
        } catch (Exception e) {
            Log.e(this, "saveCache() - Exception error during update cache DB e=" + e);
            bRet = false;
        } finally {
            mCacheSync.unlock();
        }
        checkNeedBGJob();

        return bRet;
    }

    private static class CacheDBHelper extends SQLiteOpenHelper {

        public CacheDBHelper(Context context, String name, CursorFactory factory, int version) {
            super(context, name, factory, version);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(TABLE_CACHE_CREATE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL(TABLE_CACHE_DROP);
            onCreate(db);
        }

    }

    private class CheckCacheBackgroudJob implements Callable<Void> {

        @Override
        public Void call() throws Exception {
            long nTotalSize = 0;
            int nLoopCount = 0;
            StringBuffer strPath = null;
            String strKey = null;
            String strSql = null;
            File file = null;

            nTotalSize = getFolderSize(NetResourceCacheManager.this.mBaseDirName, true);
            if (nTotalSize < MAX_NETRES_CACHE_SIZE) {
                Log.i(this, "CheckCacheBackgroudJob/call() - no need to delete cache files nTotalSize=" + nTotalSize);
                return null;
            }
            Log.i(this, "CheckCacheBackgroudJob/call() - start job nTotalSize=" + nTotalSize + ", MAX_NETRES_CACHE_SIZE=" + MAX_NETRES_CACHE_SIZE);

            while (nTotalSize >= NORMAL_NETRES_CACHE_SIZE && nLoopCount < 1000) {
                ////find victim with LRU algorithm
                strPath = null;
                strSql = "SELECT " + COLUMNS_KEY + ", " + COLUMNS_FILE_NAME + ", MIN(" + COLUMNS_USED_TIME + ") FROM " + TABLE_CACHE + ";";
                mCacheSync.lock();
                Cursor cursor = null;
                try {
                    ////check if the cache file exist
                    cursor = NetResourceCacheManager.this.mDB.rawQuery(strSql, null);
                    if (cursor != null && cursor.getCount() > 0) {
                        cursor.moveToFirst();
                        strPath = new StringBuffer(cursor.getString(cursor.getColumnIndex(COLUMNS_FILE_NAME)));
                        strKey = cursor.getString(cursor.getColumnIndex(COLUMNS_KEY));
                    }

                    if (strPath != null && strKey != null) {
                        ////second, delete file
                        strPath.insert(0, NetResourceCacheManager.this.mBaseDirName + "/");
                        file = new File(strPath.toString());
                        if (file != null && file.exists()) {
                            nTotalSize -= file.length();
                            file.delete();
                        }
                        ////third, delete row from the DB
                        strSql = COLUMNS_KEY + " =?";
                        String[] args = new String[]{strKey};
                        NetResourceCacheManager.this.mDB.delete(TABLE_CACHE, strSql, args);
                    } else {
                        break;
                    }
                    Log.i(this, "CheckCacheBackgroudJob/call() - delete strPath=" + strPath + ",nTotalSize=" + nTotalSize);
                } catch (Exception e) {
                    Log.e(this, "CheckCacheBackgroudJob/call() - Exception : " + e.getMessage());
                    nTotalSize = NORMAL_NETRES_CACHE_SIZE;
                } finally {
                    NetResourceCacheManager.this.mCacheSync.unlock();
                    if (cursor != null) {
                        try {
                            cursor.close();
                        } catch (Exception e) {
                            Log.e(this, "CheckCacheBackgroudJob/call() - Exception during close cursor : " + e.getMessage());
                        }
                    }
                }
                nLoopCount++;
                if ((nLoopCount % 10) == 9) {
                    Thread.sleep(100);
                }
            }
            Log.i(this, "CheckCacheBackgroudJob reducing job done nLoopCount=" + nLoopCount);
            return null;
        }
    }

}

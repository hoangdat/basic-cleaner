
package platform.com.mfluent.asp.util;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import com.mfluent.log.Log;
import com.samsung.android.slinkcloud.R;

import java.io.File;
import java.util.ArrayList;

public class AspCommonUtils {
    private static String TAG = "AspCommonUtils";
    public static final String DEVICE_ID = "device_id";
    public static final String INITIAL_SYNC_COMPLETE_NOTIFIED_PREF_KEY = "com.mfluent.asp.sync.AspCommonUtils.INITIAL_SYNC_COMPLETE_NOTIFIED_PREF_KEY";

    public static final int CHECK_PERMISSION_FORCE_FILEOP = 1;
    public static final int CHECK_PERMISSION_FORCE_ACCOUNTOP = 4;

    public static final String TEST_THUMB_CACHE_DIR = "/storage/emulated/0/.cloudthumbnails";

    public static boolean checkFileOpertionPermissioin() {
        boolean bRet = true;
        try {
            File file = new File(TEST_THUMB_CACHE_DIR);
            if (!file.exists()) {
                bRet = file.mkdir();
            }

            if (bRet && file.exists()) {
                bRet = file.delete();
            }
        } catch (Exception e) {
            bRet = false;
        }
        return bRet;
    }

    public static boolean checkAccountOperation(Context context) {
        boolean bRet = false;
        try {
            AccountManager acm = AccountManager.get(context);
            Account[] accList = acm.getAccounts();
            if (accList != null)
                bRet = true;
        } catch (SecurityException e) {
            Log.e(TAG, "checkAccountOperation() - Exception : " + e.getMessage());
            bRet = false;
        }
        return bRet;
    }

    public static final int MSG_CODE_FOR_PERMISSION_SETTING = 20192;

    public static boolean checkPermissionDialogResult(Activity act, int requestCode, int which, Bundle extras) {
        boolean bRet = false;
        if (requestCode == AspCommonUtils.MSG_CODE_FOR_PERMISSION_SETTING) {
            if (which == DialogInterface.BUTTON_POSITIVE) {
                AspCommonUtils.gotoPermissionSetting(act);
            }
            act.finish();
            bRet = true;
        }
        return bRet;
    }

    public static void popupPermissionSetting(Activity param) {
        final Activity tmpAct = param;
        AlertDialog.Builder alt_bld = new AlertDialog.Builder(param);
        int nFileOperation = 0;
        int nAccountOperation = 0;
        //if(checkFileOpertionPermissioin()==false)
        nFileOperation = -1;
        nAccountOperation = -1;
        String strFormat = param.getResources().getString(R.string.runtime_permission_msg1) + "\r\n\r\n";
        if (nFileOperation < 0)
            strFormat += param.getResources().getString(R.string.storage_permission);
        if (nAccountOperation < 0) {
            if (nFileOperation < 0)
                strFormat += ", ";
            strFormat += param.getResources().getString(R.string.contacts_permission);
        }
        String strAppName = param.getResources().getString(R.string.cloud_access_settings_title);

        String strMsg = String.format(strFormat, strAppName);
        alt_bld.setMessage(strMsg).setCancelable(false).setPositiveButton(R.string.app_menu_settings, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int id) {
                checkPermissionDialogResult(tmpAct, MSG_CODE_FOR_PERMISSION_SETTING, DialogInterface.BUTTON_POSITIVE, null);
            }
        }).setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int id) {
                checkPermissionDialogResult(tmpAct, MSG_CODE_FOR_PERMISSION_SETTING, DialogInterface.BUTTON_NEGATIVE, null);
            }
        });
        AlertDialog alert = alt_bld.create();
        // Title for AlertDialog
        alert.setTitle(R.string.common_popup_notification);
        alert.show();
    }

    public static void gotoPermissionSetting(Activity activity) {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" + activity.getPackageName()));
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(intent);
    }

    public static boolean checkDangerousPermission(Activity activity, Context context, int nReqCode) {
        boolean bRet = false;
        ArrayList<String> permissions = new ArrayList<String>();

        if (!getPermissionCheck(context, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (!getPermissionCheck(context, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            permissions.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (!getPermissionCheck(context, Manifest.permission.GET_ACCOUNTS)) {
            permissions.add(Manifest.permission.GET_ACCOUNTS);
        }
        if (!getPermissionCheck(context, Manifest.permission.READ_PHONE_STATE)) {
            permissions.add(Manifest.permission.READ_PHONE_STATE);
        }

        if (!permissions.isEmpty()) {
            int count = permissions.size();
            String[] params = new String[count];
            for (int position = 0 ; position < count ; position++) {
                params[position] = permissions.get(position);
            }

            ActivityCompat.requestPermissions(activity, params, nReqCode);
            bRet = true;
        }

        return bRet;
    }

    private static boolean getPermissionCheck(Context context, String permission) {
        return PackageManager.PERMISSION_DENIED != ContextCompat.checkSelfPermission(context, permission);
    }
}


package platform.com.samsung.android.slinkcloud;

import platform.com.mfluent.asp.framework.IASPApplication2;
import platform.com.mfluent.asp.framework.ServiceLocatorSLPF;

import android.app.ActivityManager;
import android.app.ActivityManager.MemoryInfo;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;

import com.mfluent.log.Log;

public class HeatManager {
    private static String TAG = "HeatManager";

    private static final String ACTION_SIOP_LEVEL = (Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) ? "android.intent.action.CHECK_SIOP_LEVEL" : "com.samsung.intent.action.CHECK_SIOP_LEVEL";
    private static final String EXTRA_SIOP_LEVEL = "siop_level_broadcast";
    private static final int SIOP_LEVEL_DEFAULT = 0;
    private static final int SIOP_LEVEL_OVERHEAT_THRESHOLD = 2;

    private static HeatManager sInstance = null;

    private int mSIOPLevel = SIOP_LEVEL_DEFAULT;

    private HeatManager(Context context) {
        Context appContext = context.getApplicationContext();
        Intent stickyIntent = appContext.registerReceiver(siopReceiver, new IntentFilter(ACTION_SIOP_LEVEL));
        Log.d(this, "HeatManager() - registered SIOP receiver stickyIntent: " + stickyIntent);
        if (stickyIntent != null && stickyIntent.hasExtra(EXTRA_SIOP_LEVEL)) {
            mSIOPLevel = stickyIntent.getIntExtra(EXTRA_SIOP_LEVEL, SIOP_LEVEL_DEFAULT);
        }
    }

    public static synchronized HeatManager getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new HeatManager(context);
        }
        return sInstance;
    }

    public int getSIOPLevel() {
        return mSIOPLevel;
    }

    public void setSIOPLevel(int sIOPLevel) {
        mSIOPLevel = sIOPLevel;
    }

    public boolean isDeviceOverheated() {
        return (mSIOPLevel >= SIOP_LEVEL_OVERHEAT_THRESHOLD);
    }

    private static BroadcastReceiver siopReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (sInstance == null) {
                return;
            }
            HeatManager observer = HeatManager.getInstance(context);
            if (intent.hasExtra(EXTRA_SIOP_LEVEL)) {
                int siopLevel = intent.getIntExtra(EXTRA_SIOP_LEVEL, observer.getSIOPLevel());
                observer.setSIOPLevel(siopLevel);
                Log.d(this, "onReceive: set SIOP level " + siopLevel);
            } else {
                Log.d(this, "onReceive: SIOP level not found");
            }
        }
    };

    public static int getRemainRAM() {
        int nRam = 1000;
        try {
            Context appContext = ServiceLocatorSLPF.get(IASPApplication2.class).getApplicationContext();
            MemoryInfo mi = new MemoryInfo();
            ActivityManager activityManager = (ActivityManager) appContext.getSystemService(Context.ACTIVITY_SERVICE);
            activityManager.getMemoryInfo(mi);
            long availableMem = mi.availMem / 1048576L;
            Log.i(TAG, "getRemainRAM() - uvailable mem=" + availableMem);
            nRam = (int) availableMem;
        } catch (Exception e) {
            Log.e(TAG, "getRemainRAM() - Exception : " + e.getMessage());
        }
        return nRam;

    }
}

/**
 * 
 */

package platform.com.mfluent.asp.util;

import java.io.File;
import java.lang.reflect.Method;

import platform.com.mfluent.asp.datamodel.DataModelSLPF;
import platform.com.mfluent.asp.datamodel.DeviceSLPF;
import android.content.Context;
import android.os.Environment;
import android.os.storage.StorageVolume;
import android.support.v4.content.ContextCompat;

import com.mfluent.log.Log;

/**
 * @author Ilan Klinghofer
 */
public class MFLStorageManagerSLPF {

	private static final String TAG = "MFLStorageManager";

	private static final File SDCARD_0_DIR = new File("/storage/sdcard0");
	private static final File EMULATED_DIR = new File("/storage/emulated");

	private final android.os.storage.StorageManager baseStorageManager;

	public MFLStorageManagerSLPF(Context context) {
		this.baseStorageManager = (android.os.storage.StorageManager) context.getSystemService(Context.STORAGE_SERVICE);
	}

	public StorageVolume[] getVolumeList() {
		try {
			Method m = android.os.storage.StorageManager.class.getMethod("getVolumeList");
			return (StorageVolume[]) m.invoke(this.baseStorageManager);
		} catch (Exception e) {
			Log.w(this, "getVolumeList:" + "failed to invoke method android.os.storage.StorageManager.getVolumeList - " + e.getMessage());

			throw new NoSuchMethodError("android.os.storage.StorageManager.getVolumeList");
		}
	}

	public String getVolumeState(String path) {
		try {
			Method m = android.os.storage.StorageManager.class.getMethod("getVolumeState", String.class);
			return (String) m.invoke(this.baseStorageManager, path);
		} catch (Exception e) {
			Log.w(this, "getVolumeState:" + "failed to invoke method android.os.storage.StorageManager.getVolumeState - " + e.getMessage());
			throw new NoSuchMethodError("android.os.storage.StorageManager.getVolumeState");
		}
	}

	public static boolean isLocalSaveLocationExternal() {
		DeviceSLPF localDevice = DataModelSLPF.getInstance().getLocalDevice();
		if (localDevice == null) {
			throw new IllegalStateException("no local device");
		}

		boolean useInternal = localDevice.getUseInternalStorage();
		boolean hasExternal = localDevice.getHasExternalStorage();

		return hasExternal && !useInternal;
	}

	public static File getStorageParentDir() {
		return MFLStorageManagerSLPF.getStorageParentDir(true);
	}

	public static File getStorageParentDir(boolean removeEmulated) {
		if (MFLStorageManagerSLPF.isLocalSaveLocationExternal()) {
			return StorageStatusSLPF.getSDCardMemoryPath();
		} else {
			File externalStorageDirectory = Environment.getExternalStorageDirectory();
			if (removeEmulated && EMULATED_DIR.equals(externalStorageDirectory.getParentFile())) {
				if (SDCARD_0_DIR.exists() && SDCARD_0_DIR.isDirectory()) {
					externalStorageDirectory = SDCARD_0_DIR;
				}
			}

			return externalStorageDirectory;
		}
	}

	public static File getCacheDir(Context context) {
		File[] cacheDirs = ContextCompat.getExternalCacheDirs(context);
		File cacheDir;
		if (cacheDirs.length >= 2 && cacheDirs[1] != null && MFLStorageManagerSLPF.isLocalSaveLocationExternal()) {
			cacheDir = cacheDirs[1];
		} else {
			cacheDir = cacheDirs[0];
		}

		if (cacheDir == null) {
			Log.d(TAG, "WTF!!::getCacheDir:cacheDir is null");
			//cacheDir = new File(Environment.getExternalStorageDirectory().getPath() + "/Android/data/com.samsung.android.slinkcloud/cache");
		} else {
			Log.d(TAG, "::getCacheDir:" + cacheDir.getAbsolutePath() + "  cacheDirs.length : " + cacheDirs.length);
		}

		return cacheDir;

	}

	public static File getStorageFilesDirectory(boolean removeEmulated) {
		File file = new File(MFLStorageManagerSLPF.getStorageParentDir(removeEmulated), "files");
		file.mkdir();
		return file;
	}

	/**
	 * check if device supports user-based storage; /storage/emulated/<user id>/
	 * - not the same as Environment.isExternalStorageEmulated()
	 * 
	 * @see android.os.Environment.UserEnvironment
	 */
	public static boolean hasEmulatedStorage() {
		String rawExternalStorage = System.getenv("EMULATED_STORAGE_TARGET");
		return (rawExternalStorage != null && rawExternalStorage.length() > 0);
	}
}

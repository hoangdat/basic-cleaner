
package platform.com.mfluent.asp.filetransfer;

import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import platform.com.mfluent.asp.datamodel.DataModelSLPF;
import platform.com.mfluent.asp.datamodel.DeviceSLPF;
import platform.com.mfluent.asp.framework.IASPApplication2;

import com.mfluent.log.Log;
import com.samsung.android.sdk.slinkcloud.CloudGatewayFileTransferUtils;
import com.samsung.android.sdk.slinkcloud.CloudGatewayFileTransferUtils.TransferOptions;
import com.samsung.android.sdk.slinkcloud.CloudGatewayMediaSet;

public class FileTransferStarterService extends IntentService {

    public FileTransferStarterService() {
        super(FileTransferStarterService.class.getSimpleName());
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent == null)
            return;
        Log.i(this, "onHandleIntent " + intent);

        if (IASPApplication2.checkNeedWifiOnlyBlock()) {
            Log.i(this, "it's wifi only mode return error before filetransfer service start...");
            return;
        }
        FileTransferManager fileTransferManager = FileTransferManagerSingleton.getInstance(this);

        TransferOptions options = intent.getParcelableExtra(CloudGatewayFileTransferUtils.EXTRA_TRANSFER_OPTIONS);

        if (CloudGatewayFileTransferUtils.ACTION_TRANSFER.equals(intent.getAction())) {
            String strSessionID = intent.getStringExtra(CloudGatewayFileTransferUtils.EXTRA_SESSION_ID);
            CloudGatewayMediaSet mediaSet = getMediaSet(intent);
            if (strSessionID != null && mediaSet == null) {
                try {
                    Intent retryIntent = new Intent(FileTransferManager.RETRY);
                    retryIntent.putExtra("sessionId", strSessionID);
                    LocalBroadcastManager mgr = LocalBroadcastManager.getInstance(getApplicationContext());
                    mgr.sendBroadcast(retryIntent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return;
            }
            if (mediaSet == null) {
                return;
            }

            DeviceSLPF targetDevice = getTargetDevice(intent);
            if (targetDevice == null) {
                return;
            }

            fileTransferManager.transfer(mediaSet, targetDevice, options, strSessionID);
        } else if (CloudGatewayFileTransferUtils.ACTION_CANCEL.equals(intent.getAction())) {
            boolean isAutoUpload = intent.getBooleanExtra(CloudGatewayFileTransferUtils.EXTRA_IS_CANCEL_FOR_AUTO_UPLOAD, false);

            if (isAutoUpload) {
                fileTransferManager.cancelAllForAutoUpload();
            } else {
                final String sessionId = intent.getStringExtra(CloudGatewayFileTransferUtils.EXTRA_SESSION_ID);
                Intent cancelIntent = new Intent(FileTransferManager.CANCEL);
                cancelIntent.putExtra("sessionId", sessionId);
                LocalBroadcastManager.getInstance(this).sendBroadcast(cancelIntent);
            }
        }
    }

    private CloudGatewayMediaSet getMediaSet(Intent intent) {
        CloudGatewayMediaSet mediaSet = CloudGatewayMediaSet.createFromIntent(intent);
        if (mediaSet == null) {
            long[] ids = intent.getLongArrayExtra(CloudGatewayFileTransferUtils.EXTRA_ROW_IDS);
            if (ids != null) {
                mediaSet = CloudGatewayMediaSet.createFromCloudGatewayMediaStoreIds(ids);
            }
        }

        if (mediaSet == null) {
            Log.w(this, "Intent " + intent + " missing required extra '" + CloudGatewayMediaSet.class.getName() + "'");
        }

        return mediaSet;
    }

    private DeviceSLPF getTargetDevice(Intent intent) {
        if (!intent.hasExtra(CloudGatewayFileTransferUtils.EXTRA_DEVICE_ID)) {
            Log.w(this, "Intent " + intent + " missing required extra '" + CloudGatewayFileTransferUtils.EXTRA_DEVICE_ID + "'");
            return null;
        }
        long deviceId = intent.getLongExtra(CloudGatewayFileTransferUtils.EXTRA_DEVICE_ID, -1);

        DeviceSLPF targetDevice = DataModelSLPF.getInstance().getDeviceById((int) deviceId);
        if (targetDevice == null) {
            Log.w(this, "Device " + deviceId + " does not exist");
            return null;
        }

        return targetDevice;
    }
}

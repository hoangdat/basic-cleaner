/**
 * Copyright (c) 2009 Samsung Electronics, Inc.
 * All right reserved.
 * This software is the confidential and proprietary information of Samsung
 * Electronics, Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with Samsung Electronics.
 * Revision History
 * Author Date Description
 * ------------------ ------------- ------------------
 */

package platform.com.mfluent.asp.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Environment;
import android.os.StatFs;
import android.os.storage.StorageVolume;

import com.mfluent.log.Log;

import java.io.File;

import platform.com.mfluent.asp.framework.IASPApplication2;
import platform.com.mfluent.asp.framework.ServiceLocatorSLPF;


/**
 * The Class StorageStatus.
 */
public class StorageStatusSLPF {
    private static final String TAG = "StorageStatusSLPF";
    private static final Context mContext = ServiceLocatorSLPF.get(IASPApplication2.class);

    @SuppressLint("SdCardPath")
    private static final String EXTERNAL_SD_ICS = "/mnt/extSdCard";

    /**
     * External memory available.
     *
     * @return true, if successful
     */
    private static boolean externalMemoryAvailable() {
        return android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);
    }

    /**
     * @return
     */
    private static StatFs getSDCardStatFS() {
        if (!StorageStatusSLPF.externalMemoryAvailable()) {
            return null;
        }

        File path = StorageStatusSLPF.getSDCardMemoryPath();
        if (!path.exists()) {
            return null;
        }

        if (!path.canWrite()) {
            return null;
        }

        if (path.equals(Environment.getExternalStorageDirectory())) {
            return null;
        }

        StatFs fs;
        try {
            fs = new StatFs(path.getPath());
        } catch (Exception e) {
            Log.e(TAG, "getSDCardStatFS() - Exception : " + e.getMessage());
            fs = null;
        }
        return fs;
    }

    public static File getSDCardMemoryPath() {
        File path = null;

        try {
            MFLStorageManagerSLPF mStorageManager = new MFLStorageManagerSLPF(mContext);
            StorageVolume[] storageVolumes = mStorageManager.getVolumeList();
            for (StorageVolume volume : storageVolumes) {
                if ("sd".equals(volume.semGetSubSystem()) && volume.isRemovable()) {
                    path = new File(volume.semGetPath());
                    break;
                }
            }
        } catch (NoSuchMethodError e) {
            Log.e(TAG, "getSDCardMemoryPath() - NoSuchMethodError : " + e.getMessage());
        }

        if (path == null) {
            path = new File(EXTERNAL_SD_ICS);
        }
        return path;
    }

    /**
     * Checks if is second external available.
     *
     * @return true, if is second external available
     */
    public static boolean isSecondExternalAvailable() {
        return StorageStatusSLPF.getSDCardStatFS() != null;
    }

    public static int isVolumeMounted(String path) {
        int ret = -1;
        try {
            MFLStorageManagerSLPF mStorageManager = new MFLStorageManagerSLPF(mContext);
            StorageVolume[] storageVolumes = mStorageManager.getVolumeList();

            for (StorageVolume volume : storageVolumes) {
                if (volume.semGetPath().equalsIgnoreCase(path)) {
                    String state = mStorageManager.getVolumeState(path);
                    if (Environment.MEDIA_MOUNTED.equals(state)) {
                        ret = 1;
                    } else {
                        ret = 0;
                    }
                    break;
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "isVolumeMounted() - Exception : " + e.getMessage());
        }

        return ret;
    }
}


package platform.com.mfluent.asp.framework;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Helper class for managing singletons.
 * 
 * @author Ilan Klinghofer
 */
public class ServiceLocatorSLPF {

	private static final Map<Class<?>, Object> SINGLETONS = new ConcurrentHashMap<Class<?>, Object>();

	//    private static Context mContext;

	@SuppressWarnings("unchecked")
	public static <T>T get(Class<T> c) {
		return (T) SINGLETONS.get(c);
	}

	public static <T>void bind(T obj, Class<? extends T> c) {
		if (obj == null) {
			SINGLETONS.remove(c);
		} else {
			SINGLETONS.put(c, obj);
		}
	}

	//    public static void reset() {
	//        SINGLETONS.clear();
	//    }

	public static void unbind(Class<?> c) {
		SINGLETONS.remove(c);
	}

	/**
	 * get the application context (same as ServiceLocator.get(ASPApplication.class).getApplicationContext())
	 */
	//    public static Context getContext() { return mContext; }

	//    public static void setContext(Context context) { mContext = context; }
}

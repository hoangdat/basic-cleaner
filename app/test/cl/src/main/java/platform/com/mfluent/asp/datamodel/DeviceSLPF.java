/**
 *
 */

package platform.com.mfluent.asp.datamodel;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentFilter.MalformedMimeTypeException;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Looper;
import android.os.PatternMatcher;

import com.mfluent.asp.cloudstorage.api.sync.CloudStorageSync;
import com.mfluent.asp.common.datamodel.ASPMediaStore;
import com.mfluent.asp.common.datamodel.CloudDevice;
import com.mfluent.asp.common.util.CursorUtils;
import com.mfluent.log.Log;
import com.samsung.android.sdk.slinkcloud.CloudGatewayDeviceTransportType;
import com.samsung.android.sdk.slinkcloud.CloudGatewayNetworkMode;
import com.samsung.android.slinkcloud.R;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Set;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import platform.com.mfluent.asp.filetransfer.FileTransferManagerSingleton;
import platform.com.mfluent.asp.framework.IASPApplication2;
import platform.com.mfluent.asp.framework.ServiceLocatorSLPF;
import platform.com.mfluent.asp.media.AspThumbnailCache;
import platform.com.mfluent.asp.sync.MflNotificationManager;
import platform.com.mfluent.asp.util.DeviceUtilSLPF;
import platform.com.mfluent.asp.util.StorageStatusSLPF;
import platform.com.samsung.android.slinkcloud.NetResourceCacheManager;
import uicommon.com.mfluent.asp.util.AccountUtil;
import uicommon.com.mfluent.asp.util.CachedExecutorService;

/**
 * Represents a single device that is registered with the Samsung account
 */
public class DeviceSLPF implements CloudDevice {

    private static final CloudGatewayDeviceTransportType[] TRANSPORT_PRIORITY = {
            CloudGatewayDeviceTransportType.LOCAL,
            CloudGatewayDeviceTransportType.WEB_STORAGE};

    private static final CloudGatewayNetworkMode[] NETWORK_MODE_RANK_TABLE = {
            CloudGatewayNetworkMode.WIFI,
            CloudGatewayNetworkMode.MOBILE_LTE,
            CloudGatewayNetworkMode.MOBILE_3G,
            CloudGatewayNetworkMode.MOBILE_2G,
            CloudGatewayNetworkMode.OFF};

    public static final int INVALID_DEVICE_ID = 0;

    private int mId;

    private String mAliasName;

    private CloudGatewayNetworkMode mNetworkMode = CloudGatewayNetworkMode.OFF;

    private long mCapacityInBytes;
    private long mUsedCapacityInBytes;

    private CloudGatewayDeviceTransportType mDeviceTransportType = CloudGatewayDeviceTransportType.UNKNOWN;

    private String mWebStorageType;
    private String mWebStorageUserId;
    private String mWebStorageEncryptedUserId;
    private String mWebStoragePw;

    private boolean mIsWebStorageSignedIn = false;
    private boolean mIsWebStorageAccountExist = false;
    private boolean mIsWebStorageEnableSync = false;

    private boolean mWebServerReachable;

    private int mServerSortKey = -1;

    // This is transient
    private boolean mPendingSetup = false;
    private long nPendingSetupTime = 0;

    private boolean mDeleted;

    private int mMaxNumTxConnection = 0;
    private String mOAuthWebViewJS = null;

    private final ReadWriteLock mDeleteLock = new ReentrantReadWriteLock();

    private CloudStorageSync mCloudStorageSync;

    private static HashMap<String, Long> sDeletedTimeMap = new HashMap<String, Long>();

    public enum SyncedMediaType {
        IMAGES,
        AUDIO,
        VIDEOS,
        DOCUMENTS
        // IF YOU ADD SOMETHING HERE, THAT THE LOCAL DEVICE OR WEB STORAGE DOES NOT SYNC, THEN YOU MUST
        // MODIFY THE INITIALIZATION OF DEFAULT_LOCAL_SYNCED_MEDIA_TYPES AND DEFAULT_WEB_STORAGE_SYNCED_MEDIA_TYPES TO NOT INCLUDE IT
    }

    public static final Set<SyncedMediaType> DEFAULT_LOCAL_SYNCED_MEDIA_TYPES = Collections.unmodifiableSet(EnumSet.allOf(SyncedMediaType.class));

    private boolean mIsSyncing;
    private boolean mIsSynchronized; /* Auto_Archive_by_TF */
    private boolean mSupportPush;

    private boolean cloudNeedFirstUpdate = false;

    private final Context mContext;

    private boolean dirty = true;

    ////after merging ui apk and platform jar, infinite cloud sync loop has been found because ui apk refreshDeviceListFromDbAsync
    ////so it is needed distinguish local broadcast messages between SLPF jar and UI APK
    public static final String BROADCAST_DEVICE_STATE_CHANGE = "com.mfluent.asp.datamodel.Device.BROADCAST_DEVICE_STATE_CHANGE.SLPF_CLOUD";
    public static final String BROADCAST_DEVICE_STATE_CHANGE_WITH_DATA = "com.mfluent.asp.datamodel.Device.BROADCAST_DEVICE_STATE_CHANGE_WITH_DATA.SLPF_CLOUD";

    public DeviceSLPF(Context context) {
        mContext = context.getApplicationContext();
    }

    public DeviceSLPF(Context context, Cursor cursor) {
        this(context);
        initializeFromDb(cursor);
    }

    public boolean getCloudNeedFirstUpdate() {
        return cloudNeedFirstUpdate;
    }

    public void setCloudNeedFirstUpdate(boolean bNeed) {
        cloudNeedFirstUpdate = bNeed;
    }

    /**
     * @return the primary key id of the device in the db
     */
    @Override
    public int getId() {
        return mId;
    }

    /**
     * The display name of the device
     *
     * @return the display name of the device
     */
    public String getDisplayName() {
        if (StringUtils.isNotBlank(getAliasName())) {
            return getAliasName();
        }
        // NOTE: Can a web storage account ever have an alias? If not, can move this check to top
        else if (isDeviceTransportType(CloudGatewayDeviceTransportType.WEB_STORAGE)) {
            return getWebStorageType();
        } else {
            return mContext.getString(R.string.settings_my_device_name);
        }
    }

    /**
     * A return value of 0 is the highest priority and higher numbers are considered lower priority.
     * The device priority is based off of three items: the device network mode, the device transport type and
     * the device physical type.
     * Each of these items makes up a part of a 32-bit integer:
     * Network Mode: 0x7F000000
     * Transport Type: 0x00FF0000
     * Physical Type: 0x0000FFFF
     * This makes the Network Mode the most significant part of the priority, followed by the Transport Type and then the Physical Type.
     */
    public int getDevicePriority() {
        int result = 0;

        int foundPos = 0;
//		CloudGatewayDeviceTransportType transportType = this.getDeviceTransportType();

        //Multi source content action priority changed - yyj.yoo
        if (getDeviceNetworkMode() != CloudGatewayNetworkMode.WIFI) {
            foundPos = ArrayUtils.indexOf(NETWORK_MODE_RANK_TABLE, this.getDeviceNetworkMode());

            if (foundPos < 0) {
                foundPos = NETWORK_MODE_RANK_TABLE.length;
            }
            result |= foundPos << 24;
        }

        if (getDeviceNetworkMode() == CloudGatewayNetworkMode.WIFI) {
            foundPos = ArrayUtils.indexOf(TRANSPORT_PRIORITY, getDeviceTransportType());
        } else {
            foundPos = ArrayUtils.lastIndexOf(TRANSPORT_PRIORITY, getDeviceTransportType());
        }

        if (foundPos < 0) {
            foundPos = TRANSPORT_PRIORITY.length;
        }

        result |= foundPos << 16;

        return result;
    }

    public boolean isLocalDevice() {
        return mDeviceTransportType == CloudGatewayDeviceTransportType.LOCAL;
    }

    /**
     * The transport type of the device
     *
     * @return the type of the device
     */
    public CloudGatewayDeviceTransportType getDeviceTransportType() {
        return mDeviceTransportType;
    }

    /**
     * Convenience method to check if transport is type of {@link #mDeviceTransportType}
     */
    public boolean isDeviceTransportType(CloudGatewayDeviceTransportType deviceType) {
        return mDeviceTransportType == deviceType;
    }

    /**
     * Sets the transport type of the device
     *
     * @param deviceType the new device type
     */
    public void setDeviceTransportType(CloudGatewayDeviceTransportType deviceType) {
        if (deviceType == null) {
            deviceType = CloudGatewayDeviceTransportType.UNKNOWN;
        }

        if (ObjectUtils.notEqual(mDeviceTransportType, deviceType)) {
            mDeviceTransportType = deviceType;
            dirty = true;

            if (deviceType == CloudGatewayDeviceTransportType.UNKNOWN) {
                final int deviceId = getId();
                final Context context = mContext;
                Runnable runnable = new Runnable() {

                    @Override
                    public void run() {
                        AspThumbnailCache.getInstance(context).clearDevicePrefetchList(deviceId);
                    }
                };

                if (Looper.getMainLooper().getThread() == Thread.currentThread()) {
                    CachedExecutorService.getInstance().execute(runnable);
                } else {
                    runnable.run();
                }

                try {
                    deleteAllMetaData(mContext.getContentResolver());
                } catch (IllegalArgumentException e) {
                    Log.e(this, "setDeviceTransportType() - Failed to deleteAllMetaData : " + e);
                }
            }
        }
    }

    /**
     * Checks if device is online
     *
     * @return true if presence is online
     */
    public boolean isPresence() {
        if (isLocalDevice() || (mDeviceTransportType == CloudGatewayDeviceTransportType.WEB_STORAGE)) {
            return true;
        }

        return mNetworkMode != CloudGatewayNetworkMode.OFF;
    }

    public boolean isSupportedDeviceType() {
        return true;
    }

    /**
     * Current network mode of the device
     *
     * @return current network mode of the device
     */
    public CloudGatewayNetworkMode getDeviceNetworkMode() {
        return mNetworkMode;
    }

    public void setDeviceNetworkMode(CloudGatewayNetworkMode networkMode) {
        if (networkMode != mNetworkMode) {
            Log.i(this, "setDeviceNetworkMode(), new: " + networkMode + " old: " + networkMode + " for device: " + this);

            mNetworkMode = networkMode;
            dirty = true;

            if (networkMode == CloudGatewayNetworkMode.OFF) {
                setWebServerReachable(false);
            }
            broadcastDeviceStateChange();

            if (networkMode != CloudGatewayNetworkMode.OFF) {
                Intent intent = new Intent(BROADCAST_DEVICE_NOW_ONLINE);
                intent.setDataAndType(ASPMediaStore.Device.getDeviceEntryUri(getId()), ASPMediaStore.Device.CONTENT_TYPE);
                intent.putExtra(DEVICE_ID_EXTRA_KEY, getId());
                sendLocalBroadcast(intent);
            }
        }
    }

    /**
     * Total storage capacity of the device in bytes
     *
     * @return total capacity of the storage device
     */
    @Override
    public long getCapacityInBytes() {
        return mCapacityInBytes;
    }

    @Override
    public void setCapacityInBytes(long capacityInBytes) {
        setCapacityInBytes(capacityInBytes, 1);
    }

    public void setCapacityInBytes(long capacityInBytes, int callType) {
        if (isLocalDevice()) {
            return;
        }

        if (mCapacityInBytes != capacityInBytes) {
            mCapacityInBytes = capacityInBytes;
            dirty = true;
            if (callType != 1)
                broadcastDeviceStateChange();
        }
    }

    /**
     * Currently used storage capacity of the device in bytes
     *
     * @return current network mode of the device
     */
    @Override
    public long getUsedCapacityInBytes() {
        return mUsedCapacityInBytes;
    }

    public long getAvailableCapacityInBytes() {
        return getCapacityInBytes() - getUsedCapacityInBytes();
    }

    public void setUsedCapacityInBytes(long usedCapacityInBytes, int callType) {
        if (isLocalDevice()) {
            return;
        }

        if (mUsedCapacityInBytes != usedCapacityInBytes) {
            mUsedCapacityInBytes = usedCapacityInBytes;
            dirty = true;
            if (callType != 1)
                broadcastDeviceStateChange();
        }
    }

    @Override
    public void setUsedCapacityInBytes(long usedCapacityInBytes) {
        setUsedCapacityInBytes(usedCapacityInBytes, 1);
    }

    /**
     * @return the webStorageType
     */
    public String getWebStorageType() {
        return mWebStorageType;
    }

    public void setWebStorageType(String type) {
        if (StringUtils.equals(mWebStorageType, type)) {
            return;
        }

        mWebStorageType = type;
        dirty = true;
    }

    /**
     * @return the aliasName
     */
    public String getAliasName() {
        return mAliasName;
    }

    public void setAliasName(String name) {
        setAliasName(name, 0);

        setSupportPush(DeviceUtilSLPF.SAMSUNGDRIVE_ALIAS_NAME.equals(name));
    }

    public void setAliasName(String name, int callType) {
        if (StringUtils.equals(mAliasName, name)) {
            return;
        }

        mAliasName = name;
        dirty = true;
        if (callType != 1)
            broadcastDeviceStateChange();

    }

    /**
     * @return the webStorageUserId
     */
    @Override
    public String getWebStorageUserId() {
        return mWebStorageUserId;
    }

    @Override
    public void setWebStorageUserId(String webId) {
        if (StringUtils.equals(mWebStorageUserId, webId)) {
            return;
        }

        mWebStorageUserId = webId;
        dirty = true;
    }

    @Override
    public String getWebStorageEncryptedUserId() {
        return mWebStorageEncryptedUserId;
    }

    @Override
    public void setWebStorageEncryptedUserId(String encryptedId) {
        if (StringUtils.equals(mWebStorageEncryptedUserId, encryptedId)) {
            return;
        }

        mWebStorageEncryptedUserId = encryptedId;
    }

    /**
     * @param userPw the userPw to set
     */
    public void setWebStoragePw(String userPw) {
        if (StringUtils.equals(mWebStoragePw, userPw)) {
            return;
        }

        mWebStoragePw = userPw;
        dirty = true;
    }

    /**
     * @return the isSignedIn
     */
    @Override
    public boolean isWebStorageSignedIn() {
        return mIsWebStorageSignedIn;
    }

    /**
     * @param isWebStorageSignedIn the isSignedIn to set
     */
    @Override
    public void setWebStorageSignedIn(boolean isWebStorageSignedIn) {
        setWebStorageSignedIn(isWebStorageSignedIn, 1);
    }

    public void setWebStorageSignedIn(boolean isSignedIn, int callType) {
        if (isSignedIn == false) {
            Log.i(this, "setWebStorageSignedIn2: false dev=" + mWebStorageType);
        }
        IASPApplication2.traceStack();
        if (mIsWebStorageSignedIn != isSignedIn) {
            Log.i(this, "setWebStorageSignedIn(), Changing login state of: " + mWebStorageType + " to login= " + isSignedIn);
            if (isSignedIn == true) {
                setPendingSetup(false);
            }

            mIsWebStorageSignedIn = isSignedIn;
            dirty = true;
            if (isSignedIn) {
                setWebStorageAccountExist(true);
            } else {
                if (getWebStorageType() != null && getWebStorageType().indexOf("google") >= 0) {
                    setWebStorageAccountExist(AccountUtil.hasGoogleAccount(mContext));
                } else {
                    setWebStorageAccountExist(false);
                }
            }

            if (callType != 1)
                broadcastDeviceStateChange();

            if (ServiceLocatorSLPF.get(MflNotificationManager.class) != null && isDeviceTransportType(CloudGatewayDeviceTransportType.WEB_STORAGE)) {
                ServiceLocatorSLPF.get(MflNotificationManager.class).deviceListChange(this, isSignedIn);
            }
        }
    }

    public void setWebStorageAccountExist(boolean bExist) {
        if (mIsWebStorageAccountExist != bExist) {
            mIsWebStorageAccountExist = bExist;
            dirty = true;
        }
    }

    public void setWebStorageEnableSync(boolean bEnable) {
        if (mIsWebStorageEnableSync != bEnable) {
            mIsWebStorageEnableSync = bEnable;
            dirty = true;
        }
    }

    public boolean isWebStorageEnableSync() {
        return mIsWebStorageEnableSync;
    }

    /**
     * @return true if the device has external storage(ie. SDcard)
     */
    public boolean getHasExternalStorage() {
        return isLocalDevice() ? StorageStatusSLPF.isSecondExternalAvailable() : false;
    }

    /**
     * @return true if the device is set to save incoming files to internal memory
     */
    public boolean getUseInternalStorage() {
        SharedPreferences pref = mContext.getSharedPreferences(IASPApplication2.PREFERENCES_NAME, Activity.MODE_PRIVATE);
        return pref.getBoolean("useInternal", true);
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        ToStringBuilder builder = new ToStringBuilder(this).append(getId()).append(getDisplayName()).append(getDeviceTransportType());
        if (getDeviceTransportType() == CloudGatewayDeviceTransportType.WEB_STORAGE) {
            builder.append(getWebStorageType());
            if (isSyncing()) {
                builder.append("+syncing");
            }
        } else {
            builder.append(isPresence() ? "ONLINE" : "OFFLINE");
            builder.append(mNetworkMode);
        }

        return builder.toString();
    }

    public void broadcastDeviceStateChange() {
        Intent intent = new Intent(BROADCAST_DEVICE_STATE_CHANGE);
        intent.putExtra(DEVICE_ID_EXTRA_KEY, getId());
        sendLocalBroadcast(intent);

        Intent intentWithData = new Intent(BROADCAST_DEVICE_STATE_CHANGE_WITH_DATA);
        intentWithData.setDataAndType(ASPMediaStore.Device.getDeviceEntryUri(getId()), ASPMediaStore.Device.CONTENT_TYPE);
        intentWithData.putExtra(DEVICE_ID_EXTRA_KEY, getId());
        sendLocalBroadcast(intentWithData);
    }

    private void sendLocalBroadcast(Intent intent) {
        DataModelSLPF dataModel = DataModelSLPF.getInstance();
        if (dataModel != null) {
            dataModel.sendLocalBroadcast(intent);
        }
    }

    public void broadcastDeviceRefresh() {
        broadcastDeviceRefresh(0);
    }

    @Override
    public IntentFilter buildDeviceIntentFilterForAction(String action) {
        IntentFilter intentFilter = new IntentFilter(action);
        Uri deviceUri = ASPMediaStore.Device.getDeviceEntryUri(getId());
        intentFilter.addDataScheme(deviceUri.getScheme());
        intentFilter.addDataAuthority(deviceUri.getHost(), String.valueOf(deviceUri.getPort()));
        intentFilter.addDataPath(deviceUri.getPath(), PatternMatcher.PATTERN_LITERAL);
        try {
            intentFilter.addDataType(ASPMediaStore.Device.CONTENT_TYPE);
        } catch (MalformedMimeTypeException e) {
            throw new RuntimeException("Trouble creating intentFilter", e);
        }

        return intentFilter;
    }

    /**
     * send out device refresh messsage with optional params
     *
     * @param from - one of the REFRESH_FROM_* values (0 if N/A)
     */
    public void broadcastDeviceRefresh(int from) {
        Log.i(this, "broadcastDeviceRefresh(), device: " + this + ", where: " + from);

        Intent intent = new Intent(BROADCAST_DEVICE_REFRESH);
        intent.setDataAndType(ASPMediaStore.Device.getDeviceEntryUri(getId()), ASPMediaStore.Device.CONTENT_TYPE);
        intent.putExtra(DEVICE_ID_EXTRA_KEY, getId());
        if (from > 0) {
            intent.putExtra(REFRESH_FROM_KEY, from);
        }

        sendLocalBroadcast(intent);
    }

    public int getServerSortKey() {
        return mServerSortKey;
    }

    public void setServerSortKey(int key) {
        if (mServerSortKey == key) {
            return;
        }

        mServerSortKey = key;
        dirty = true;
    }

    public CloudStorageSync getCloudStorageSync() {
        return mCloudStorageSync;
    }

    public void setCloudStorageSync(CloudStorageSync cloudStorageSync) {
        mCloudStorageSync = cloudStorageSync;
    }

    @Override
    public void deleteAllMetaData(ContentResolver cr) {
        cr.delete(ASPMediaStore.Images.Media.getContentUriForDevice(mId), null, null);
        cr.delete(ASPMediaStore.Video.Media.getContentUriForDevice(mId), null, null);
        cr.delete(ASPMediaStore.Audio.Media.getContentUriForDevice(mId), null, null);
        cr.delete(ASPMediaStore.Documents.Media.getContentUriForDevice(mId), null, null);
        cr.delete(ASPMediaStore.Audio.Albums.getOrphanCleanUriForDevice(mId), null, null);
        cr.delete(ASPMediaStore.Audio.Artists.getOrphanCleanUriForDevice(mId), null, null);
        cr.delete(ASPMediaStore.Files.Keywords.getOrphanCleanUriForDevice(mId), null, null);
    }

    public void setWebServerReachable(boolean webServerReachable) {
        if (webServerReachable != mWebServerReachable) {
            mWebServerReachable = webServerReachable;
            dirty = true;
            broadcastDeviceStateChange();
        }
    }

    public void setPendingSetup(boolean pendingSetup) {
        mPendingSetup = pendingSetup;
        if (pendingSetup == true) {
            nPendingSetupTime = System.currentTimeMillis();
        }
    }

    public boolean checkPendingDelay() {
        long nTime = System.currentTimeMillis();
        return nPendingSetupTime + 120000 < nTime;
    }

    public boolean getPendingSetup() {
        return mPendingSetup;
    }

    public boolean isSyncing() {
        return mIsSyncing;
    }

    private void setSupportPush(boolean supportPush) {
        Log.d(this, "setSupportPush : " + supportPush);
        mSupportPush = supportPush;
    }

    public boolean isSupportPush() {
        Log.d(this, "isSupportPush : " + mAliasName + " " + mSupportPush);
        return mSupportPush;
    }

    public void setIsSyncing(boolean isSyncing) {
        if (mIsSyncing == isSyncing) {
            return;
        }

        mIsSyncing = isSyncing;
        dirty = true;
    }

    /* Auto_Archive_by_TF */
    public boolean isSynchronized() {
        return mIsSynchronized;
    }

    public void setIsSynchronized(boolean isSynchronized) {
        mIsSynchronized = isSynchronized;
    }

    public void setAllSyncsMediaType(Set<SyncedMediaType> syncedMediaTypes) {
        if (ObjectUtils.equals(syncedMediaTypes, syncedMediaTypes)) {
            return;
        }

        syncedMediaTypes.clear();
        syncedMediaTypes.addAll(syncedMediaTypes);
        dirty = true;
    }

    public boolean isDeleted() {
        return mDeleted;
    }

    public boolean commitChanges() {
        if (!dirty) {
            return false;
        }

        if (isDeleted()) {
            Log.w(this, "Someone tried to update a deleted device : " + new Exception());
            return false;
        }

        if (mId == 0) {
            Log.d(this, "Inserting device " + this);
            ////check peer id in order to duplicated DB insertion
            boolean bDuplicated = false;
            try {
                if (mWebStorageType != null && isDeviceTransportType(CloudGatewayDeviceTransportType.WEB_STORAGE)) {
                    if (DataModelSLPF.getInstance().getDeviceByStorageType(mWebStorageType) != null) {
                        Log.i("INFO", "Canceling device insertion (duplicated row detected) storageType=" + mWebStorageType);
                        bDuplicated = true;
                    }
                }
            } catch (Exception e) {
                Log.e(this, "commitChanges() - Exception : " + e.getMessage());
            }
            if (bDuplicated == false) {
                Uri uri = mContext.getContentResolver().insert(ASPMediaStore.Device.CONTENT_URI, getContentValues());
                if (uri != null) {
                    mId = (Integer.parseInt(uri.getLastPathSegment()));
                    if (getServerSortKey() < 0) {
                        setServerSortKey(getId());
                    }
                    DataModelSLPF.getInstance().addDevice(this);
                }
            }
        } else {
            Log.d(this, "Updating device " + this);
            mContext.getContentResolver().update(ASPMediaStore.Device.getDeviceEntryUri(getId()), getContentValues(), null, null);
        }
        dirty = false;
        return true;
    }

    public void delete() {
        if (isLocalDevice()) {
            Log.d(this, "Ignoring remove request for local device : " + new Exception());
            return;
        }

        if (isDeleted()) {
            Log.w(this, "Trying to delete a device that has already been deleted : " + new Exception());
            return;
        }

        boolean bNeedCloudDelete = false;
        if (isDeviceTransportType(CloudGatewayDeviceTransportType.WEB_STORAGE)) {
            synchronized (sDeletedTimeMap) {
                long nDeletedTime = System.currentTimeMillis();
                sDeletedTimeMap.put(getWebStorageType(), nDeletedTime);
            }
            setWebStorageSignedIn(false);
            ////tskim fix for device spinner
            setWebStorageUserId(null);
            setCapacityInBytes(0);
            setUsedCapacityInBytes(0);
            bNeedCloudDelete = true;
        }

        Log.i(this, "Deleting " + this);

        FileTransferManagerSingleton.getInstance(mContext).cancelAllForDevice(this);

        AspThumbnailCache.getInstance(mContext).clearDevicePrefetchList(getId());

        mDeleteLock.writeLock().lock();
        try {
            if (bNeedCloudDelete == false) {
                mDeleted = true;
                mContext.getContentResolver().delete(ASPMediaStore.Device.getDeviceEntryUri(getId()), null, null);
            } else {
                String where = "device_id = ?";
                String[] args = new String[]{"" + getId()};
                if (mWebStorageType != null && !mWebStorageType.equals(DeviceUtilSLPF.GOOGLEDRIVE_WEBSTORAGE_NAME)) {
                    if (getCloudStorageSync() != null) {
                        SQLiteDatabase db = getCloudStorageSync().getReadableCloudDatabase();
                        db.delete(ASPMediaStore.Files.PATH, where, args);
                    }
                } else {
                    mContext.getContentResolver().delete(ASPMediaStore.Files.CONTENT_URI, where, args);
                }

                if (getCloudStorageSync() != null) {  //P151126-06479
                    getCloudStorageSync().reset();
                    setCloudStorageSync(null);
                }
                String instanceName = getWebStorageType();
                SharedPreferences preferences = mContext.getSharedPreferences(instanceName, Context.MODE_PRIVATE);
                preferences.edit().clear().apply();
                preferences = null;
            }
        } finally {
            mDeleteLock.writeLock().unlock();
        }

        if (bNeedCloudDelete == false) {
            DataModelSLPF.getInstance().removeDevice(this);
        } else {
            NetResourceCacheManager.getInstance(mContext).deleteCache(null, null, getId());
            commitChanges();
            broadcastDeviceRefresh();
        }
    }

    private ContentValues getContentValues() {
        ContentValues values = new ContentValues();
        values.put(ASPMediaStore.DeviceColumns.ALIAS_NAME, getAliasName());
        values.put(ASPMediaStore.DeviceColumns.TRANSPORT_TYPE, getDeviceTransportType().name());
        values.put(ASPMediaStore.DeviceColumns.DEVICE_PRIORITY, getDevicePriority());
        values.put(ASPMediaStore.DeviceColumns.WEB_STORAGE_TYPE, getWebStorageType());
        values.put(ASPMediaStore.DeviceColumns.WEB_STORAGE_USER_ID, getWebStorageUserId());
        values.put(ASPMediaStore.DeviceColumns.WEB_STORAGE_IS_SIGNED_IN, (isWebStorageSignedIn() == false ? 0 : 1));
        values.put(ASPMediaStore.DeviceColumns.WEB_STORAGE_ENABLE_SYNC, (isWebStorageEnableSync() == false ? 0 : 1));
        values.put(ASPMediaStore.DeviceColumns.WEB_STORAGE_TOTAL_CAPACITY, getCapacityInBytes());
        values.put(ASPMediaStore.DeviceColumns.WEB_STORAGE_USED_CAPACITY, getUsedCapacityInBytes());
        values.put(ASPMediaStore.DeviceColumns.SERVER_SORT_KEY, getServerSortKey());
        getDeviceNetworkMode().toContentValues(values);
        values.put(ASPMediaStore.DeviceColumns.IS_SYNCING, isSyncing() == false ? 0 : 1);
        values.put(ASPMediaStore.DeviceColumns.MAX_NUM_TX_CONN, getMaxNumTxConnection());
        values.put(ASPMediaStore.DeviceColumns.OAUTH_WEBVIEW_JS, getOAuthWebViewJS());
        return values;
    }

    private void initializeFromDb(Cursor cursor) {
        mId = (CursorUtils.getInt(cursor, ASPMediaStore.DeviceColumns._ID));
        setAliasName(CursorUtils.getString(cursor, ASPMediaStore.DeviceColumns.ALIAS_NAME), 1);
        setDeviceTransportType(CloudGatewayDeviceTransportType.getDeviceTransportType(cursor));
        setWebStorageType(CursorUtils.getString(cursor, ASPMediaStore.DeviceColumns.WEB_STORAGE_TYPE));
        setWebStorageUserId(CursorUtils.getString(cursor, ASPMediaStore.DeviceColumns.WEB_STORAGE_USER_ID));
        setWebStorageSignedIn(CursorUtils.getBoolean(cursor, ASPMediaStore.DeviceColumns.WEB_STORAGE_IS_SIGNED_IN), 1);
        setWebStorageEnableSync(CursorUtils.getBoolean(cursor, ASPMediaStore.DeviceColumns.WEB_STORAGE_ENABLE_SYNC));
        setCapacityInBytes(CursorUtils.getLong(cursor, ASPMediaStore.DeviceColumns.WEB_STORAGE_TOTAL_CAPACITY), 1);
        setUsedCapacityInBytes(CursorUtils.getLong(cursor, ASPMediaStore.DeviceColumns.WEB_STORAGE_USED_CAPACITY), 1);
        setServerSortKey(CursorUtils.getInt(cursor, ASPMediaStore.DeviceColumns.SERVER_SORT_KEY));
        setDeviceNetworkMode(CloudGatewayNetworkMode.getNetworkMode(cursor));

        setMaxNumTxConnection(CursorUtils.getInt(cursor, ASPMediaStore.DeviceColumns.MAX_NUM_TX_CONN));
        setOAuthWebViewJS(CursorUtils.getString(cursor, ASPMediaStore.DeviceColumns.OAUTH_WEBVIEW_JS));

        dirty = false;
        broadcastDeviceStateChange();
    }

    public int getMaxNumTxConnection() {
        return mMaxNumTxConnection;
    }

    public void setMaxNumTxConnection(int maxNumTxConnection) {
        if (mMaxNumTxConnection == maxNumTxConnection) {
            return;
        }

        mMaxNumTxConnection = maxNumTxConnection;
        dirty = true;
    }

    public String getOAuthWebViewJS() {
        return mOAuthWebViewJS;
    }

    public void setOAuthWebViewJS(String OAuthWebViewJS_param) {
        if (OAuthWebViewJS_param != null
                && mOAuthWebViewJS != null
                && OAuthWebViewJS_param.equals(mOAuthWebViewJS)) {
            return;
        }

        mOAuthWebViewJS = OAuthWebViewJS_param;
        dirty = true;
    }

    @Override
    public boolean getIsInUserRecovering() {
        // TODO Auto-generated method stub
        return IASPApplication2.isInGoogleDriveUserRecovering;
    }

    @Override
    public void setIsInUserRecovering(boolean arg0) {
        // TODO Auto-generated method stub
        IASPApplication2.isInGoogleDriveUserRecovering = arg0;
    }
}
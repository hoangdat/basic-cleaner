
package platform.com.mfluent.asp.ui;

import android.content.Context;
import android.content.Intent;

import com.mfluent.asp.cloudstorage.api.sync.CloudStorageSync;
import com.mfluent.asp.common.datamodel.CloudDevice;
import com.mfluent.log.Log;

import java.net.ConnectException;

import platform.com.mfluent.asp.datamodel.DeviceSLPF;
import platform.com.mfluent.asp.framework.ServiceLocatorSLPF;
import platform.com.mfluent.asp.framework.StorageGatewayManager;
import platform.com.mfluent.asp.sync.CloudStorageTypeListSyncManager;

public class StorageSignInOutHelper {

    public static final String BROADCAST_SIGNOUT = StorageSignInOutHelper.class.getName() + "_BROADCAST_SIGNOUT";

    private final Context mContext;

    public StorageSignInOutHelper(Context context) {
        mContext = context;
    }

    /**
     * Signs out of the storage service for the given device. This should not be called on the main thread.
     *
     * @param storageDevice The storage device to sign out of
     */
    public void signOutOfStorageService(final DeviceSLPF storageDevice) {
        Log.i(this, "::signOutOfStorageService:" + "Signing out of: " + storageDevice);

        boolean isSignOutSuccess = true;
        CloudStorageTypeListSyncManager cloudListSyncManager = ServiceLocatorSLPF.get(CloudStorageTypeListSyncManager.class);

        try {
            // stop transport call reset on storageDevice
            if (cloudListSyncManager != null) {
                cloudListSyncManager.stop(null);
            }

            CloudStorageSync cloudStorageSync = (storageDevice != null) ? storageDevice.getCloudStorageSync() : null;
            if (cloudStorageSync != null) {
                cloudStorageSync.reset();
            }

            // Attempt to SignOut of StorageGateway
            try {
                StorageGatewayManager storageGatewayHelper = StorageGatewayManager.getInstance(mContext);
                if (storageDevice != null && storageGatewayHelper != null) {
                    String response = storageGatewayHelper.logout(storageDevice.getWebStorageType());

                    storageDevice.setWebStorageUserId(null);
                    storageDevice.setWebStoragePw(null);

                    isSignOutSuccess = "false".equals(response);
                } else {
                    isSignOutSuccess = false;
                }
            } catch (ConnectException e) {
                isSignOutSuccess = false;
                e.printStackTrace();
            }

            // delete device
            if (isSignOutSuccess && storageDevice != null) {
                storageDevice.delete();
            }
        } finally {
            Intent intent = new Intent(CloudDevice.BROADCAST_DEVICE_SIGNOUT_RESPONSE);
            intent.putExtra("signout_response", isSignOutSuccess);
            mContext.sendBroadcast(intent);
            Log.i(this, "::signOutOfStorageService:" + "--- Signed out of: " + storageDevice);
        }
    }
}

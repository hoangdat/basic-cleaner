
package platform.com.mfluent.asp.framework.AuthDelegate;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;

import com.mfluent.asp.common.datamodel.CloudDevice;
import com.mfluent.cloud.samsungdrive.common.DriveConstants;
import com.mfluent.cloud.samsungdrive.common.SamsungAccountImp;
import com.mfluent.log.Log;

import platform.com.mfluent.asp.datamodel.DeviceSLPF;
import platform.com.mfluent.asp.ui.SignInOutUtils;

public class SamsungDriveAuthActivity extends Activity {

    private final int REQUEST_ID_GET_ACCESSTOKEN = 1;
    private int mDeviceId = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(this, "onCreate()");

        Intent intent = getIntent();
        mDeviceId = intent.getIntExtra(CloudDevice.DEVICE_ID_EXTRA_KEY, 0);

        AccountManager manager = AccountManager.get(this);
        Account[] accountArr = manager.getAccountsByType(SignInOutUtils.AccountType_SamsungAccount);
        if (accountArr.length > 0) {
            String[] additionalData = new String[]{SamsungAccountImp.USER_ID, SamsungAccountImp.COUNTRY_CODE};
            Intent bundle = new Intent("com.msc.action.samsungaccount.REQUEST_ACCESSTOKEN");
            bundle.putExtra(DriveConstants.CLIENT_ID, DriveConstants.APPID);
            bundle.putExtra(DriveConstants.CLIENT_SECRET, DriveConstants.APP_SECRET);
            bundle.putExtra(DriveConstants.MYPACKAGE, this.getPackageName());
            bundle.putExtra(DriveConstants.ADDITIONAL, additionalData);
            startActivityForResult(bundle, REQUEST_ID_GET_ACCESSTOKEN);
        } else {
            Log.d(this, "onCreate() - There was no Samsung Account");
            finish();
        }

        return;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_ID_GET_ACCESSTOKEN:
                if (resultCode == RESULT_OK) {
                    Bundle bundle = data.getExtras();
                    DriveConstants.setConstants(bundle, false);

                    Intent intent = new Intent(DeviceSLPF.BROADCAST_DEVICE_STATE_CHANGE);
                    intent.putExtra(CloudDevice.DEVICE_ID_EXTRA_KEY, mDeviceId);
                    LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
                } else {
                    Log.e(this, "onActivityResult() - resultCode : " + resultCode);
                }
                break;
            default:
                Log.e(this, "onActivityResult() - invalid requestCode : " + requestCode);
                break;
        }
        finish();
    }
}

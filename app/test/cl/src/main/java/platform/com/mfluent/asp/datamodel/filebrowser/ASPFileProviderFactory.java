package platform.com.mfluent.asp.datamodel.filebrowser;

import android.content.Context;

import com.mfluent.asp.common.datamodel.ASPFileProvider;
import com.mfluent.log.Log;

import platform.com.mfluent.asp.datamodel.DeviceSLPF;

public class ASPFileProviderFactory {

    private ASPFileProviderFactory() {
    }

    public static ASPFileProvider getFileProviderForDevice(Context context, DeviceSLPF device) {
        if (device == null) {
            return new EmptyFileProvider(context, device);
        }

        switch (device.getDeviceTransportType()) {
            case LOCAL:
                return new CachedFileProvider(new LocalFileProvider(context, device), context, device);
            case WEB_STORAGE:
                Log.d("ASPFileProvider", "getFileProviderForDevice : " + device.getCloudStorageSync());
                return new CachedFileProvider(device.getCloudStorageSync(), context, device);
            default:
                return new EmptyFileProvider(context, device);
        }
    }
}

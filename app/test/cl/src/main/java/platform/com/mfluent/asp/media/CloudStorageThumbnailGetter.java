/**
 *
 */

package platform.com.mfluent.asp.media;

import com.mfluent.asp.cloudstorage.api.sync.CloudStorageSync;
import com.mfluent.asp.common.datamodel.ASPMediaStore;
import com.mfluent.asp.common.media.thumbnails.ImageInfo;

import java.io.File;

import platform.com.mfluent.asp.datamodel.DataModelSLPF;
import platform.com.mfluent.asp.datamodel.DeviceSLPF;

/**
 * @author Ilan Klinghofer
 */
public class CloudStorageThumbnailGetter extends BaseRemoteThumbnailGetter {

    public CloudStorageThumbnailGetter(ImageInfo mediaInfo, AspThumbnailCache fileCache) {
        super(mediaInfo, fileCache);
    }

    @Override
    protected File openRemoteFile(String tmpPathToDownload, String tmpNameFile) throws Exception {
        ImageInfo mediaInfo = getMediaInfo();
        DeviceSLPF device = DataModelSLPF.getInstance().getDeviceById(mediaInfo.getDeviceId());
        if (device == null || device.isWebStorageSignedIn() == false) {
            return null;
        }

        CloudStorageSync cloudStorageSync = device.getCloudStorageSync();
        if (cloudStorageSync == null) {
            return null;
        }

        cloudStorageSync.setPreferredThumbnailSize(ASPMediaStore.DEFAULT_THUMBNAIL_WIDTH, ASPMediaStore.DEFAULT_THUMBNAIL_HEIGHT);
        File result = cloudStorageSync.downloadThumbnail(mediaInfo, tmpPathToDownload, tmpNameFile);

        return result;
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        Thread blockingThread = getWorkingThread();
        if (blockingThread != null) {
            blockingThread.interrupt();
        }
    }
}


package platform.com.mfluent.asp.framework;

import java.util.concurrent.TimeUnit;

import platform.com.mfluent.asp.sync.CloudStorageSyncManager;
import platform.com.samsung.android.slinkcloud.HeatManager;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;

import com.mfluent.log.Log;

public class CloudManagerMainService extends Service {
    private static boolean DYNAMIC_DEATH_FOR_CONTENTPROVIDER_SPEED = true;
    private final static int MIN_DRAM_TO_DIE = 500; //300MB

    private static final long SERVICE_INACTIVITY_TIMEOUT = TimeUnit.SECONDS.toMillis(30);

    //private SyncManager mSyncManager;
    private HandlerThread mThread;
    private MyWorkerHandler mHandler;
    private boolean mBound = false;
    private int mLastStartId = 0;

    private class MyWorkerHandler extends Handler {

        public static final int MSG_TIMEOUT = 0;
        //		public static final int MSG_UPDATE_CHECK = 1;
        private long lastAction = 0L;

        public MyWorkerHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {

            boolean handled = true;
            boolean resetTimeout = true;

            switch (msg.what) {
                case MSG_TIMEOUT:
                    if (!mBound  && ((lastAction + SERVICE_INACTIVITY_TIMEOUT) < SystemClock.uptimeMillis())) {
                        if (IASPApplication2.sKeepAliveServiceFromNativeApp || DYNAMIC_DEATH_FOR_CONTENTPROVIDER_SPEED && HeatManager.getRemainRAM() > MIN_DRAM_TO_DIE) {
                            Log.i(this, "Delay activity stop...bE=" + IASPApplication2.sKeepAliveServiceFromNativeApp);
                            //CMHServiceInterface.clearDeferedList(getApplicationContext());
                            CloudStorageSyncManager.checkNoDeviceStatus();
                        } else {
                            Log.i(this, "Stopping self due to no activity for " + SERVICE_INACTIVITY_TIMEOUT + "ms. Bound? " + mBound);
                            //CMHServiceInterface.clearDeferedList(getApplicationContext());
                            stopSelf(mLastStartId);
                            resetTimeout = false;
                        }
                    }
                    break;
                default:
                    handled = false;
                    resetTimeout = false;
                    break;
            }

            if (!handled) {
                super.handleMessage(msg);
            } else if (resetTimeout) {
                resetTimeout();
            }
        }

        public void resetTimeout() {
            this.lastAction = SystemClock.uptimeMillis();
            if (hasMessages(MSG_TIMEOUT)) {
                removeMessages(MSG_TIMEOUT);
            }
            Message msg = obtainMessage(MSG_TIMEOUT);
            sendMessageDelayed(msg, SERVICE_INACTIVITY_TIMEOUT);
        }
    }

    private class MyBinder extends Binder {

    }

    @Override
    public IBinder onBind(Intent intent) {
        mBound = true;
        return new MyBinder();
    }

    @Override
    public boolean onUnbind(Intent intent) {
        mBound = false;
        mHandler.resetTimeout();
        return super.onUnbind(intent);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mBound = false;
        IASPApplication2.sInstanceExist = true;

        mThread = new HandlerThread(getClass().getSimpleName());
        mThread.start();
        mHandler = new MyWorkerHandler(mThread.getLooper());
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String action = (intent == null) ? null : intent.getAction();

        Log.d(this, "onStartCommand : " + action + ", " + intent);

        mLastStartId = startId;
        IASPApplication2 app = ServiceLocatorSLPF.get(IASPApplication2.class);
        // Don't start up this service until the user has passed the login screen
        app.initSyncStart();
        mHandler.resetTimeout();
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        IASPApplication2.sInstanceExist = false;
        //this.mSyncManager.stop(null);
        mThread.quit();
        super.onDestroy();
    }
}

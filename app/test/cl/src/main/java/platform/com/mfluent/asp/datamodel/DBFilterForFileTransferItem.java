package platform.com.mfluent.asp.datamodel;

import android.content.Context;

import java.util.HashMap;

/**
 * Created by sec on 2015-08-07.
 */
public class DBFilterForFileTransferItem {

    static DBFilterForFileTransferItem sInstance = null;
    Context mContext = null;

    public static synchronized DBFilterForFileTransferItem getInstance() {
        if (sInstance == null) {
            sInstance = new DBFilterForFileTransferItem();
        }
        return sInstance;
    }

    private final HashMap<Integer, Boolean> mSyncBlockDeviceMap = new HashMap<Integer, Boolean>();

    public void setContext(Context context) {
        mContext = context;
    }

    public boolean isDeviceSyncBlocked(int nDevId) {
        boolean bRet = false;
        if (mSyncBlockDeviceMap.isEmpty() == false) {
            synchronized (mSyncBlockDeviceMap) {
                Boolean objRet = mSyncBlockDeviceMap.get(Integer.valueOf(nDevId));
                if (objRet != null)
                    bRet = objRet;
            }
        }
        return bRet;
    }
}

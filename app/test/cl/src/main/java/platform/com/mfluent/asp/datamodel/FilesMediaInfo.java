package platform.com.mfluent.asp.datamodel;

import android.net.Uri;

import com.mfluent.asp.common.datamodel.ASPMediaStore;

public class FilesMediaInfo extends BaseMediaFileInfo {
    static final String TABLE_NAME = "FILES";
//    static final String VIEW_NAME = "FILES_DEVICES";

    static final String DEVICE_ID_INDEX = "FILES_DEVICE_ID_IDX";
    static final String SOURCE_MEDIA_INDEX = "FILES_SRC_MDA_IDX";
    static final String MIME_TYPE_INDEX = "FILES_MIME_TYPE_IDX";

    private static final class InstanceHolder {
        private static FilesMediaInfo sInstance = new FilesMediaInfo();
    }

    public static FilesMediaInfo getInstance() {
        return InstanceHolder.sInstance;
    }

    private FilesMediaInfo() {
    }

    @Override
    public Uri getEntryUri(long rowId) {
        return MediaInfo.buildEntryIdUri(rowId, ASPMediaStore.Files.PATH);
    }

    @Override
    public Uri getContentUri() {
        return ASPMediaStore.Files.CONTENT_URI;
    }

    @Override
    public Uri getContentUriForDevice(long deviceId) {
        return MediaInfo.buildDeviceContentUri(deviceId, ASPMediaStore.Files.PATH);
    }

    @Override
    protected String getPrivateQueryTableName() {
//        return VIEW_NAME;
        return TABLE_NAME;
    }

    @Override
    public String getInsertUpdateDeleteTableName() {
        return TABLE_NAME;
    }

    @Override
    public String getEntryContentType() {
        return ASPMediaStore.Files.ENTRY_CONTENT_TYPE;
    }

    @Override
    public String getContentType() {
        return ASPMediaStore.Files.CONTENT_TYPE;
    }
}

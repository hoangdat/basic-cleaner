
package platform.com.mfluent.asp.util;

import android.content.Context;
import android.text.format.Formatter;

import com.mfluent.asp.common.util.FileTypeHelper;

public class UiUtilsSLPF {

	/**
	 * get MIME type for a given filename
	 * 
	 * @return MIME type or null if unrecognized
	 */
	public static String getMimeFromFilename(String filename) {
		if (filename == null || filename.contains(".rmvb") || filename.contains(".rm")) {
			return null;
		}
		return FileTypeHelper.getMimeTypeForFile(filename, null);
	}

	public static String formatShortFileSize(Context context, long sizeInBytes) {
		return Formatter.formatShortFileSize(context, sizeInBytes);
	}
}

package platform.com.mfluent.asp.sync;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.support.v4.content.LocalBroadcastManager;

import com.mfluent.asp.cloudstorage.api.sync.CloudStorageAppDelegate;
import com.mfluent.asp.cloudstorage.api.sync.CloudStorageSync;
import com.mfluent.asp.common.datamodel.CloudDevice;
import com.mfluent.asp.common.util.IntentHelper;
import com.mfluent.log.Log;
import com.samsung.android.sdk.slinkcloud.CloudGatewayDeviceTransportType;
import com.samsung.android.sdk.slinkcloud.CloudGatewayNetworkManager;

import org.apache.commons.lang3.StringUtils;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import platform.com.mfluent.asp.datamodel.DBFilterForFileTransferItem;
import platform.com.mfluent.asp.datamodel.DataModelSLPF;
import platform.com.mfluent.asp.datamodel.DeviceSLPF;
import platform.com.mfluent.asp.datamodel.filebrowser.CachedFileCheckService;
import platform.com.mfluent.asp.framework.DynamicCloudStorageLoader;
import platform.com.mfluent.asp.framework.IASPApplication2;
import platform.com.mfluent.asp.framework.ServiceLocatorSLPF;
import platform.com.mfluent.asp.framework.StorageProviderDatabaseHelper;
import platform.com.mfluent.asp.framework.StorageProviderInfo;
import platform.com.mfluent.asp.ui.StorageSignInOutHelper;

public class CloudStorageSyncManager extends DeviceMetadataSyncManager implements CloudStorageAppDelegate {
    private static final String TAG = "CloudStorageSyncManager";

    private static final int PLUGIN_NOT_FOUND_RETRY_TIMEOUT = 2 * 5 * 1000;

    private ScheduledExecutorService scheduledExecutorService = null;

    private ScheduledFuture<?> futureRetry = null;

    private final BroadcastReceiver deviceUpdatedBroadcastReceiver = new CloudStorageSyncBroadcastReceiver();

    private Intent syncIntent = null;

    public static boolean bNeedPoCToken = false;

    public static final String strCloudRefreshEvent = "com.samsung.android.slinkcloud.OnlyCloudsDeviceRefresh";
    public static long mLastSyncDoneTime = 0;

    public CloudStorageSyncManager(final Context context, final DeviceSLPF device) {
        super(context, device);
    }

    /*
     * override methods in SingleThreadedSyncManager
     */
    @Override
    public String getThreadName() {
        String threadName = super.getThreadName() + " (" + device.getDisplayName() + ")";
        return threadName;
    }

    @Override
    protected void doStart(Intent intent) {
        super.doStart(intent);

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(DeviceSLPF.BROADCAST_DEVICE_STATE_CHANGE);
        intentFilter.addAction(CloudStorageSync.CLOUD_STORAGE_CAPACITY_CHANGED);
        intentFilter.addAction(CloudStorageSync.CLOUD_APP_STARTED);
        intentFilter.addAction(CloudStorageSync.CLOUD_APP_STOPPED);

        LocalBroadcastManager.getInstance(mContext).registerReceiver(deviceUpdatedBroadcastReceiver, intentFilter);
    }

    @Override
    protected void doStop(Intent intent) {
        super.doStop(intent);

        if (intent != null) {
            String action = intent.getAction();
            if (StringUtils.equals(action, IASPApplication2.BROADCAST_MASTER_RESET)) {
                if (getDevice().getCloudStorageSync() != null) {
                    getDevice().getCloudStorageSync().reset();
                }
            }
        }

        LocalBroadcastManager broadcastMgr = LocalBroadcastManager.getInstance(mContext);
        broadcastMgr.unregisterReceiver(deviceUpdatedBroadcastReceiver);

    }

    @Override
    protected boolean syncOnServiceCreate() {
        return true;
    }

    @Override
    protected Collection<IntentFilter> getSyncIntentFilters() {
        /*
         * NOTE: Since the Samsung Link Platform application can now be killed and will potentially come back
		 * very often, we do not want to sync web storages every time we come up. So, we will not call the super
		 * implementation of this method because it specifies syncing on the ASPApplication.BROADCAST_GO_HOME
		 * broadcast.
		 */

        Collection<IntentFilter> syncIntentFilters = new HashSet<IntentFilter>();
        syncIntentFilters.add(new IntentFilter(DataModelSLPF.BROADCAST_REFRESH_ALL));
        syncIntentFilters.add(device.buildDeviceIntentFilterForAction(DeviceSLPF.BROADCAST_DEVICE_REFRESH));
        syncIntentFilters.add(new IntentFilter(strCloudRefreshEvent));

        return syncIntentFilters;
    }

    @Override
    protected boolean shouldSync(Intent intent) {

        if (getDevice().getCloudStorageSync() == null) {
            loadCloudStoragePlugin();
            if (getDevice().getCloudStorageSync() != null) {
                Log.i(this, "shouldSync() - Loaded : " + device.getWebStorageType());
            }
        }

        if (futureRetry != null) {
            futureRetry.cancel(true);
            futureRetry = null;
        }

        if (getDevice().getCloudStorageSync() == null) {
            // Cloud storage library not loaded. Wait and try again.
            Log.e(this, "shouldSync() - Could not load web storage types from server.");
            if (scheduledExecutorService == null) {
                scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
            }
            futureRetry = scheduledExecutorService.schedule(new Runnable() {

                @Override
                public void run() {
                    requestSync();
                }
            }, PLUGIN_NOT_FOUND_RETRY_TIMEOUT, TimeUnit.SECONDS);
            return false;
        }

        return true;
    }

    ////tskim to reduce sync request like local device sync
    @Override
    public synchronized void sync(Intent intent) {
        if (intent != null) {
            if (intent.getAction().equals(strCloudRefreshEvent)) {
                syncDelayed(intent, 0);
            } else {
                syncDelayed(intent, 500);
            }
        }
    }

    @Override
    protected void doSyncInner(Intent syncReason) {
        if (device != null) {
            device.setCloudNeedFirstUpdate(false);
        }
        if (device == null || device.isWebStorageEnableSync() == false) {
            Log.i(this, "doSyncInner() - cloud storage sync is not enabled device=" + device);
            return;
        }

        boolean bBlocked = false;
        try {
            bBlocked = DBFilterForFileTransferItem.getInstance().isDeviceSyncBlocked(device.getId());
        } catch (Exception e) {
            Log.e(this, "doSyncInner() - Exception : " + e.getMessage());
        }
        if (bBlocked) {
            Log.i(this, "doSyncInner() - skip cloud sync cause during transfer");
            syncDelayed(syncReason, TimeUnit.SECONDS.toMillis(5));
            return;
        }
        try {
            Log.v(this, "doSyncInner() called");
            long start = System.currentTimeMillis();
            device.setIsSyncing(true);
            device.setIsSynchronized(false); /* Auto_Archive_by_TF */

            getDevice().getCloudStorageSync().sync();
            Log.i(this, "doSyncInner() - onReceive:" + device.getWebStorageType() + " sync took " + (System.currentTimeMillis() - start) + "ms");

            if (!device.getPendingSetup() && device.checkPendingDelay() && !device.isWebStorageSignedIn()) {
                IASPApplication2 app = ServiceLocatorSLPF.get(IASPApplication2.class);
                StorageProviderDatabaseHelper providerDB = StorageProviderDatabaseHelper.getInstance(app.getApplicationContext());
                StorageProviderInfo spInfo = null;
                if (providerDB != null) {
                    spInfo = providerDB.get(device.getWebStorageType());
                }
                if (spInfo != null && spInfo.isLoginStatus() == true) {
                    Log.e(this, "doSyncInner() - device/provider table mismatch bug fix -> sign out: " + spInfo.getSpName());
                }
            }
            if (!device.isSupportPush()) {
                CachedFileCheckService.triggerScan(20);
            }
            mLastSyncDoneTime = System.currentTimeMillis();
        } catch (Exception e) {
            Log.e(this, "doSyncInner() - Last change exception caught for cloud storage. Sync failed for device: [" + device + "] : " + e.getMessage());
        }

        startPrefetchService();
    }

	/*
     * end of overridden methods in SingleThreadedSyncManager
	 */

    /*
     * implement methods in CloudStorageAppDelegate
     */
    @Override
    public void sendBroadcastMessage(Intent intent) {
        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(mContext);
        if (localBroadcastManager != null) {
            Log.d(this, "sendBroadcastMessage() - CloudStorage SendBroadcast: [" + intent.getAction() + "] for device id=[" + getDevice().getId() + "]");
            localBroadcastManager.sendBroadcast(intent);
        }
    }

    @Override
    public void requestSync() {
        /*
         * SingleThreadSyncManager checks for duplicates based on reference to intent, not value of intent fields!!!
		 * so we need a unique intent to prevent "over-sync()ing"
		 */
        if (syncIntent == null) {
            syncIntent = new Intent(DeviceSLPF.BROADCAST_DEVICE_STATE_CHANGE);
            syncIntent.putExtra(CloudDevice.DEVICE_ID_EXTRA_KEY, device.getId());
        }

        LocalBroadcastManager.getInstance(mContext).sendBroadcast(syncIntent);
    }

    public static void checkNoDeviceStatus() {
        try {
            List<DeviceSLPF> arr = DataModelSLPF.getInstance().getDevicesByType(CloudGatewayDeviceTransportType.WEB_STORAGE);
            if (arr == null || arr.isEmpty()) {
                Log.i(TAG, "zero device list, check and refresh all");
                IASPApplication2 asp = ServiceLocatorSLPF.get(IASPApplication2.class);
                CloudGatewayNetworkManager.getInstance(asp.getApplicationContext()).requestRefresh();
            }
        } catch (Exception e) {
            Log.e(TAG, "checkNoDeviceStatus() - Exception : " + e.getMessage());
        }
    }

    private class CloudStorageSyncBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String intentAction = intent.getAction();
            Log.v(this, "onReceive of CloudStorageSyncBroadcastReceiver, intent : " + intentAction);

            CloudStorageSync storageSync = getDevice().getCloudStorageSync();
            if (storageSync == null) {
                Log.d(this, "deviceUpdatedBroadcastReceiver/onReceive() - cloudStorage is null!");
                return;
            }

            if (intentAction != null) {
                if (intentAction.equals(CloudStorageSync.CLOUD_APP_STARTED) || intentAction.equals(CloudStorageSync.CLOUD_APP_STOPPED)) {
                    storageSync.onReceive(context, intent);
                } else {
                    int cloudDeviceId = intent.getExtras().getInt(CloudDevice.DEVICE_ID_EXTRA_KEY);
                    Log.i(this, "deviceUpdatedBroadcastReceiver/onReceive() - " + IntentHelper.intentToString(intent));

                    if (CloudStorageSyncManager.this.device.getId() == cloudDeviceId) {
                        Log.i(this, "deviceUpdatedBroadcastReceiver/onReceive() - Notify Device(" + CloudStorageSyncManager.this.device.getId() + ") action=" + intent.getAction());

                        // TODO : Add cloud storage full UX
//                    if (intentAction.equals(CloudStorageSync.CLOUD_STORAGE_CAPACITY_CHANGED)) {
//                        String whichStorage = CloudStorageSyncManager.this.device.getWebStorageType();
//                        if (checkIfNeedNotification(whichStorage)) {
//                            showStorageFullNotification(whichStorage);
//                        }
//                        return;
//                    }

                        String message = intent.getStringExtra("message");
                        if (message != null && message.equalsIgnoreCase("GoogleAuthIOException")) {
                            Log.i(this, "deviceUpdatedBroadcastReceiver/onReceive() - CloudStorageSyncBroadcastReceiver, need to logout GoogleDrive");

                            final Context context_tmp = context;
                            AsyncTask.execute(new Runnable() {

                                @Override
                                public void run() {
                                    StorageSignInOutHelper signOutHelper = new StorageSignInOutHelper(context_tmp);
                                    signOutHelper.signOutOfStorageService(CloudStorageSyncManager.this.device);
                                }
                            });

                        } else {
                            Log.d(this, "HJ sync " + TAG + " " + intentAction);
                            storageSync.onReceive(context, intent);
                            sync(intent);
                        }
                    }
                }
            }
        }
    }

    private void loadCloudStoragePlugin() {
        Log.d(this, "loadCloudStoragePlugin() called");
        if (getDevice().getCloudStorageSync() != null) {
            return;
        }
        try {
            StorageProviderDatabaseHelper providerDB = StorageProviderDatabaseHelper.getInstance(mContext);
            StorageProviderInfo cloudStorageType = providerDB.get(device.getWebStorageType());
            if (cloudStorageType == null) {
                Log.e(this, "loadCloudStoragePlugin() - No storage type defined for " + device.getWebStorageType());
                return;
            }

            CloudStorageSync cloudStorageSync = DynamicCloudStorageLoader.newInstanceOf(cloudStorageType, mContext);
            if (cloudStorageSync == null) {
                Log.e(this, "loadCloudStoragePlugin() - cloudStorageType : " + cloudStorageType + ", cloudStorageSync is null");
                return;
            }
            getDevice().setCloudStorageSync(cloudStorageSync);

			/*
             * call the inversion of control setters
			 */
            IASPApplication2 app = ServiceLocatorSLPF.get(IASPApplication2.class);

            Intent providerConfig = new Intent(CloudStorageSync.PROVIDER_CONFIGURATION);
            cloudStorageSync.onReceive(app, providerConfig);

            cloudStorageSync.setContentProvider(app.getContentResolver());
            cloudStorageSync.setCloudDevice(device);
            cloudStorageSync.setDataModel(DataModelSLPF.getInstance());
            cloudStorageSync.setStorageAppDelegate(this);
            /*
             * then call the inversion of control initializer
			 */
            cloudStorageSync.init();

            Log.d(this, "loadCloudStoragePlugin() - Loaded: " + cloudStorageSync.getClass().toString());

            LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(mContext);
            IntentFilter[] intentFilters = getDevice().getCloudStorageSync().getIntentFilters();
            for (IntentFilter filter : intentFilters) {
                localBroadcastManager.registerReceiver(deviceUpdatedBroadcastReceiver, filter);
            }

        } catch (Exception e) {
            Log.e(this, "loadCloudStoragePlugin() - failed to load web storage plugin for [" + device.getWebStorageType() + "] : " + e.getMessage());
        }
    }

    public static void registerForOAuthLaunch(Context context, BroadcastReceiver receiver) {
        LocalBroadcastManager.getInstance(context).registerReceiver(receiver, new IntentFilter(CloudStorageSync.CLOUD_LAUNCH_OAUTH1_BROWSER));
    }

    public static void unregisterForOAuthLaunch(Context context, BroadcastReceiver receiver) {
        LocalBroadcastManager.getInstance(context).unregisterReceiver(receiver);
    }
}

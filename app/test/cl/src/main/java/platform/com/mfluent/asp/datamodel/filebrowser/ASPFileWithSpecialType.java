
package platform.com.mfluent.asp.datamodel.filebrowser;

import com.mfluent.asp.common.datamodel.ASPFile;
import com.mfluent.asp.common.datamodel.ASPFileSpecialType;

abstract class ASPFileWithSpecialType implements ASPFile {

	private ASPFileSpecialType specialType;

	public void setSpecialType(ASPFileSpecialType specialType) {
		this.specialType = specialType;
	}

	@Override
	public String getSpecialDirectoryType() {
		String result = null;

		if (this.specialType != null) {
			result = this.specialType.toString();
		}

		return result;
	}

}

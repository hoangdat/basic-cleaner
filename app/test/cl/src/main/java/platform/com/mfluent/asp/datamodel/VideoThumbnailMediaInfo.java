
package platform.com.mfluent.asp.datamodel;

import com.mfluent.asp.common.datamodel.ASPMediaStore.Video.VideoColumns;
import com.mfluent.asp.common.datasource.AspMediaId;
import com.samsung.android.sdk.slinkcloud.CloudGatewayMediaStore;

public class VideoThumbnailMediaInfo extends ThumbnailMediaInfo {

	private static final String[] PROJECTION = {
			VideoColumns._ID,
			VideoColumns.DATA,
			VideoColumns.MEDIA_TYPE,
			VideoColumns.SOURCE_MEDIA_ID,
			VideoColumns.FULL_URI,
			VideoColumns.DISPLAY_NAME,
			VideoColumns.THUMBNAIL_URI,
			VideoColumns.DEVICE_ID,
			VideoColumns.WIDTH,
			VideoColumns.HEIGHT};

	private static final class InstanceHolder {

		private static VideoThumbnailMediaInfo sInstance = new VideoThumbnailMediaInfo();
	}

	public static VideoThumbnailMediaInfo getInstance() {
		return InstanceHolder.sInstance;
	}

	private VideoThumbnailMediaInfo() {

	}

	@Override
	protected String[] getQueryTableProjection() {
		return PROJECTION;
	}

	@Override
	public String getQueryTableName(MediaInfoContext context) {
        return FilesMediaInfo.TABLE_NAME;//VideoMediaInfo.VIEW_NAME;
	}

	@Override
	public String getInsertUpdateDeleteTableName() {
		return null;
	}

	@Override
	public String getEntryContentType() {
		return CloudGatewayMediaStore.Video.Media.ENTRY_CONTENT_TYPE;
	}

	@Override
	public String getContentType() {
		return CloudGatewayMediaStore.Video.Media.CONTENT_TYPE;
	}

	@Override
	protected int getMediaType() {
		return AspMediaId.MEDIA_TYPE_VIDEO;
	}
}

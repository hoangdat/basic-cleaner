package platform.com.mfluent.asp.util;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import platform.com.mfluent.asp.framework.StorageProviderInfo;

/**
 * Created by sec on 2016-01-08.
 */
public class CollectionUtils {

    public static Object find(Set set, Predicate predic) {
        Object ret = null;
        Object tmp = null;
        if(predic==null)
            return null;
        Iterator it = set.iterator();
        while(it.hasNext()) {
            tmp = it.next();
            if(tmp != null) {
                if(predic.evaluate(tmp)) {
                    ret = tmp;
                    break;
                }
            }
        }
        return ret;
    }

    public static Object find(List list, Predicate predic) {
        Object ret = null;
        Object tmp = null;
        if(predic==null)
            return null;
        Iterator it = list.iterator();
        while(it.hasNext()) {
            tmp = it.next();
            if(tmp != null) {
                if(predic.evaluate(tmp)) {
                    ret = tmp;
                    break;
                }
            }
        }
        return ret;
    }

    public static boolean exists(List list, Predicate predic) {
        return find(list, predic) != null;
    }
}


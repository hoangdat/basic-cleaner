package platform.com.mfluent.asp.filetransfer;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import com.mfluent.log.Log;

/**
 * Created by js271.kim on 2015-11-26.
 */
public class FileTransferNotiReceiver extends BroadcastReceiver {

    private static String TAG = "FileTransferNotiReceiver";
    public static final String FILE_TRANSFER_CANCEL = "platform.com.mfluent.asp.filetransfer.FileTransferNotiReceiver.CANCEL";

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent == null || intent.getAction() == null) {
            return;
        }

        String action = intent.getAction();
        if(action.equals(FileTransferNotiReceiver.FILE_TRANSFER_CANCEL)) {
            Log.d(TAG, "onReceive() - cancel...");

            if (intent != null) {
                String sessionId = intent.getStringExtra("sessionId");
                boolean isDeleteSessionAfterCancel = intent.getBooleanExtra("isDeleteSessionAfterCancel", true);

                Intent cancelIntent = new Intent(FileTransferManager.CANCEL);
                cancelIntent.putExtra("sessionId", sessionId);
                cancelIntent.putExtra("isDeleteSessionAfterCancel", isDeleteSessionAfterCancel);
                LocalBroadcastManager.getInstance(context).sendBroadcast(cancelIntent);
            }
        }
    }
}

/**
 * 
 */

package platform.com.mfluent.asp.media;


/**
 * @author michaelgierach
 */
public class AspGetMediaTask extends AspMediaTask {

	/**
	 * @param taskInfo
	 */
	public AspGetMediaTask(Object taskInfo) {
		super(taskInfo);
	}

	private MediaGetter mGetter = null;

	public final void setMediaGetter(MediaGetter getter) {
		mGetter = getter;
	}

	@Override
	protected void cancelAction() {
		super.cancelAction();

		if (mGetter != null) {
			mGetter.cancel();
		}
	}
}

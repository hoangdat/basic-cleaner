/**
 *
 */

package platform.com.mfluent.asp.media;

import java.io.FileNotFoundException;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import android.content.ContentProvider.PipeDataWriter;
import android.os.ParcelFileDescriptor;

import platform.com.mfluent.asp.datamodel.ASPMediaStoreProvider;
import platform.com.mfluent.asp.util.IOUtils;

/**
 * @author michaelgierach
 */
public abstract class BaseRemoteMediaGetter extends MediaGetter implements PipeDataWriter<AspMediaInfo> {

    private static final int STATE_CONNECTING = 0;
    protected static final int STATE_HAS_SOME_DATA = 1;
    protected static final int STATE_FAILED = 2;

    private int mState;

    private ReentrantLock mLock = new ReentrantLock();
    private final Condition mCondition;

    /**
     * @param mediaInfo
     * @param provider
     */
    public BaseRemoteMediaGetter(AspMediaInfo mediaInfo, ASPMediaStoreProvider provider) {
        super(mediaInfo, provider);

        mState = STATE_CONNECTING;
        mLock = new ReentrantLock();
        mCondition = mLock.newCondition();
    }

    /*
     * (non-Javadoc)
     * @see com.mfluent.asp.media.mediagetters.MediaGetter#getMedia()
     */
    @Override
    protected ParcelFileDescriptor getMedia() throws FileNotFoundException, InterruptedException {
        ParcelFileDescriptor result = getProvider().openPipeHelper(getMediaInfo().getUri(), null, null, getMediaInfo(), this);

        mLock.lockInterruptibly();
        try {
            while (mState == STATE_CONNECTING) {
                mCondition.await();
            }

            if (mState == STATE_FAILED) {
                if (result != null) {
                    IOUtils.closeQuietly(result);
                }
                result = null;
            }
        } finally {
            mLock.unlock();
        }

        if (result == null) {
            throw new FileNotFoundException();
        }

        return result;
    }

    protected void setState(int state) throws InterruptedException {
        mLock.lockInterruptibly();
        try {
            if (mState != state) {
                mState = state;
                mCondition.signal();
            }
        } finally {
            mLock.unlock();
        }
    }

    /*
     * (non-Javadoc)
     * @see com.mfluent.asp.media.mediagetters.MediaGetter#cancel()
     */
    @Override
    public void cancel() {
        // TODO Auto-generated method stub

    }

}

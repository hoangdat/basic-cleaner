
package platform.com.mfluent.asp.datamodel.filebrowser;

import android.content.Context;
import android.content.res.Resources;
import android.database.AbstractWindowedCursor;
import android.database.CursorWindow;
import android.database.StaleDataException;
import android.net.Uri;
import android.os.Bundle;

import com.mfluent.asp.common.datamodel.ASPFile;
import com.mfluent.asp.common.datamodel.ASPFileBrowser;
import com.mfluent.asp.common.datamodel.ASPFileProvider;
import com.mfluent.asp.common.datamodel.ASPFileSortType;
import com.mfluent.asp.common.datamodel.ASPFileSpecialType;
import com.mfluent.asp.common.datamodel.ASPMediaStore;
import com.mfluent.log.Log;
import com.samsung.android.sdk.slinkcloud.CloudGatewayMediaStore;
import com.samsung.android.sdk.slinkcloud.CloudGatewayMediaStore.FileBrowser.DirectoryInfoColumns;
import com.samsung.android.sdk.slinkcloud.CloudGatewayMediaStore.FileBrowser.FileBrowserColumns;
import com.samsung.android.slinkcloud.R;

import org.apache.commons.lang3.StringUtils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

import platform.com.mfluent.asp.datamodel.DataModelSLPF;
import platform.com.mfluent.asp.datamodel.DeviceSLPF;
import uicommon.com.mfluent.asp.util.FrameworkReflector;

public class CachedFileBrowserCursor extends AbstractWindowedCursor {

    private static final String[] DEFAULT_PROJECTION = {
            FileBrowserColumns._ID,
            FileBrowserColumns.MEDIA_ID,
            FileBrowserColumns.FILE_ID,
            FileBrowserColumns.DISPLAY_NAME,
            FileBrowserColumns.LOCAL_DATA,
            FileBrowserColumns.MIME_TYPE,
            FileBrowserColumns.LAST_MODIFIED,
            FileBrowserColumns.SIZE,
            CloudGatewayMediaStore.MediaColumns.DEVICE_ID,
            ASPMediaStore.Documents.DocumentColumns.FULL_URI,
            CloudGatewayMediaStore.Files.FileColumns.CHILD_CNT,    //jsub12_151006
            CloudGatewayMediaStore.Files.FileColumns.CHILD_DIR_CNT, //jsub12_151006
            CloudGatewayMediaStore.Files.FileColumns.DESCENDANTS_CNT,
            CloudGatewayMediaStore.Files.FileColumns.DESCENDANTS_DIR_CNT,
            CloudGatewayMediaStore.Files.FileColumns.TRASH_PROCESSING
    };

    private enum FieldValues {
        _ID,
        MEDIA_ID,
        FILE_ID,
        LOCAL_DATA,
        DISPLAY_NAME,
        MIME_TYPE,
        LAST_MODIFIED,
        SIZE,
        DEVICE_ID,
        FULL_URI,     //jsublee_150901
        CHILD_CNT,    //jsub12_151006
        CHILD_DIR_CNT, //jsub12_151006
        DESCENDANTS_CNT,
        DESCENDANTS_DIR_CNT,
        TRASH_PROCESSING
    }

    private static AtomicInteger mInstanceCount = new AtomicInteger();

    private final String[] mProjection;
    private final FieldValues[] mFieldMap;
    private final String mInstanceNumber;
    private int mLastFilledWindowSize = 0;

    private ASPFileProvider mProvider;
    private ASPFileBrowserManager.ASPFileBrowserReference mFileBrowserRef;
    private long mDeviceId;
    private String mRemoteFolderId;
    private String mSelection;
    private String mFirstArg = null;
    private ASPFileSortType mSortType;
    private boolean mReleaseRefOnClose = false;
    private Resources mResources;
    private String mStrOriginalSortRequest = null;

    private void addDirectoryInfo(ASPFileBrowser<?> objFileBrowser) {
        Bundle bundleDir = new Bundle();
        ASPFile directory = objFileBrowser.getCurrentDirectory();
        ASPFile parent = objFileBrowser.getParentDirectory();
        if (bundleDir != null && directory != null) {
            try {
                bundleDir.putString(DirectoryInfoColumns.FILE_ID, getStorageGatewayId(directory));
                bundleDir.putString(DirectoryInfoColumns.DISPLAY_NAME, getLocalizedFileName(directory));
                bundleDir.putLong(DirectoryInfoColumns.FILE_COUNT, objFileBrowser.getCount());
                bundleDir.putString(DirectoryInfoColumns.PATH, objFileBrowser.getCurrentDirectoryAbsolutePath());
                if (parent != null) {
                    bundleDir.putString(DirectoryInfoColumns.PARENT_DISPLAY_NAME, getLocalizedFileName(parent));
                    bundleDir.putString(DirectoryInfoColumns.PARENT_FILE_ID, getStorageGatewayId(parent));
                    Log.i(this, "parent storage id=" + getStorageGatewayId(parent));
                    //bundleDir.putLong(DirectoryInfoColumns.PARENT_ICON_ID, getCannedIconIdForFile(parent));
                }
                //bundleDir.putLong(DirectoryInfoColumns.ICON_ID, getCannedIconIdForFile(directory));
            } catch (Exception e) {
                Log.e(this, "error during addDirectoryInfo, maybe ICON_ID, " + e.getCause());
            }
            FrameworkReflector.AbstractWindowedCursor_setExtras(this, bundleDir);
        }
    }

    protected boolean addStringToWindow(String value, int row, int column) {
        if (value != null) {
            return mWindow.putString(value, row, column);
        } else {
            return mWindow.putNull(row, column);
        }
    }

    protected boolean addLongToWindow(Long value, int row, int column) {
        if (value != null) {
            return mWindow.putLong(value.longValue(), row, column);
        } else {
            return mWindow.putNull(row, column);
        }
    }

    protected ASPFileSpecialType getFileSpecialType(ASPFile file) {
        ASPFileSpecialType result = null;

        if (file != null) {
            String specialType = file.getSpecialDirectoryType();
            if (StringUtils.isNotEmpty(specialType)) {
                try {
                    result = ASPFileSpecialType.valueOf(specialType);
                } catch (Exception e) {
                    Log.e(this, "getFileSpecialType() - Exception : " + e.getMessage());
                }
            }
        }

        return result;
    }

    protected String getStorageGatewayId(ASPFile file) throws FileNotFoundException {
        String id = null;
        if (file != null) {
            id = mProvider.getStorageGatewayFileId(file);
        }

        return id;
    }

    protected String getLocalizedFileName(ASPFile file) {
        return getLocalizedFileName(file, getFileSpecialType(file));
    }

    protected String getLocalizedFileName(ASPFile file, ASPFileSpecialType specialType) {
        String name = null;

        if (file != null) {
            name = file.getName();
            if (specialType != null) {
                int resId = -1;
                switch (specialType) {
                    case EXTERNAL_STORAGE:
                        resId = R.string.file_sdcard_root;
                        break;
                    case INTERNAL_STORAGE:
                        resId = R.string.file_device_root;
                        break;
                }
                if (resId != -1) {
                    name = this.mResources.getString(resId);
                }
            }
        }

        return name;
    }

    public CachedFileBrowserCursor(Context context, long deviceId, String remoteFolderId, String[] projection, String sortType, String selection, String[] selectionArgs)
            throws InterruptedException, IOException {
        mResources = context.getResources();
        mDeviceId = deviceId;
        mRemoteFolderId = remoteFolderId;
        DeviceSLPF device = DataModelSLPF.getInstance().getDeviceById(deviceId);
        mProvider = ASPFileProviderFactory.getFileProviderForDevice(context, device);
        checkMultiSortOption(sortType);
        mSortType = getASPFileSortType(sortType);
        mSelection = selection;
        if (selectionArgs != null && selectionArgs.length > 0)
            mFirstArg = selectionArgs[0];
        try {
            if (mProvider instanceof CachedFileProvider) {
                CachedFileProvider newProvider = (CachedFileProvider) mProvider;
                newProvider.setNonBlockingBrowser();
            }
            mFileBrowserRef = ASPFileBrowserManager.getInstance().getLoadedFileBrowser3(
                    mProvider,
                    remoteFolderId,
                    mSortType,
                    selection,
                    selectionArgs,
                    mStrOriginalSortRequest, true);
            mReleaseRefOnClose = true;
        } catch (InterruptedException e) {
            throw e;
        } catch (IOException e) {
            throw e;
        }
        mProjection = (projection == null) ? DEFAULT_PROJECTION : projection;
        mFieldMap = createFieldMap(mProjection);
        mInstanceNumber = "file_browser_" + Integer.toString(mInstanceCount.incrementAndGet());

        Log.i(this, "CachedFileBrowserCursor created remoteFolderId=" + remoteFolderId + ", sortType=" + sortType);

        if (mFileBrowserRef != null) {
            Uri uri = CloudGatewayMediaStore.FileBrowser.FileList2.getFileListUri(deviceId, remoteFolderId);
            ////add directory information
            ASPFileBrowser<?> objFileBrowser = mFileBrowserRef.getFileBrowser();
            if (objFileBrowser != null) {
                addDirectoryInfo(objFileBrowser);
                setNotificationUri(context.getContentResolver(), uri);
            }
        } else {
            Log.e(this, "CachedFileBrowserCursor fileRef is NULL!");
        }
    }

    protected boolean checkMultiSortOption(String sortStr) {
        String[] split = StringUtils.split(sortStr, ',');
        if (split != null && split.length > 1) {
            mStrOriginalSortRequest = sortStr;
            Log.i(this, "Cursor: mStrOriginalSortRequest=" + mStrOriginalSortRequest);
            return true;
        }
        mStrOriginalSortRequest = null;
        return false;
    }

    protected ASPFileSortType getASPFileSortType(String sortStr) {
        if (StringUtils.isEmpty(sortStr)) {
            return ASPFileSortType.DATE_MODIFIED_DESC;
        }
        String sortChanged = sortStr;
        String[] split = StringUtils.split(sortStr, ',');
        if (split != null && split.length > 1) {
            sortChanged = split[0];
            //throw new IllegalArgumentException("Only a single sort field may be specified.");
        }

        ASPFileSortType type = ASPFileSortType.getSortTypeFromCursorSortOrder(sortChanged);
        if (type == null) {
            type = ASPFileSortType.DATE_MODIFIED_DESC;
        }

        return type;
    }

    private FieldValues[] createFieldMap(String[] projection) throws IllegalArgumentException {
        FieldValues[] result = new FieldValues[projection.length];
        for (int i = 0; i < projection.length; ++i) {
            String field = projection[i];
            if (FileBrowserColumns._ID.equals(field)) {
                result[i] = FieldValues._ID;
            } else if (FileBrowserColumns.MEDIA_ID.equals(field)) {
                result[i] = FieldValues.MEDIA_ID;
            } else if (FileBrowserColumns.FILE_ID.equals(field)) {
                result[i] = FieldValues.FILE_ID;
            } else if (FileBrowserColumns.LOCAL_DATA.equals(field)) {
                result[i] = FieldValues.LOCAL_DATA;
            } else if (FileBrowserColumns.DISPLAY_NAME.equals(field)) {
                result[i] = FieldValues.DISPLAY_NAME;
            } else if (FileBrowserColumns.LAST_MODIFIED.equals(field)) {
                result[i] = FieldValues.LAST_MODIFIED;
            } else if (FileBrowserColumns.MIME_TYPE.equals(field)) {
                result[i] = FieldValues.MIME_TYPE;
            } else if (CloudGatewayMediaStore.MediaColumns.DEVICE_ID.equals(field)) {
                result[i] = FieldValues.DEVICE_ID;
            } else if (CloudGatewayMediaStore.MediaColumns.SIZE.equals(field)) {
                result[i] = FieldValues.SIZE;
            } else if (ASPMediaStore.Documents.DocumentColumns.FULL_URI.equals(field)) {
                result[i] = FieldValues.FULL_URI;
            } else if (CloudGatewayMediaStore.Files.FileColumns.CHILD_CNT.equals(field)) { //jsub12_151006
                result[i] = FieldValues.CHILD_CNT;
            } else if (CloudGatewayMediaStore.Files.FileColumns.CHILD_DIR_CNT.equals(field)) { //jsub12_151006
                result[i] = FieldValues.CHILD_DIR_CNT;
            } else if (CloudGatewayMediaStore.Files.FileColumns.DESCENDANTS_CNT.equals(field)) {
                result[i] = FieldValues.DESCENDANTS_CNT;
            } else if (CloudGatewayMediaStore.Files.FileColumns.DESCENDANTS_DIR_CNT.equals(field)) {
                result[i] = FieldValues.DESCENDANTS_DIR_CNT;
            } else if (CloudGatewayMediaStore.Files.FileColumns.TRASH_PROCESSING.equals(field)) {
                result[i] = FieldValues.TRASH_PROCESSING;
            } else {
                throw new IllegalArgumentException("Unsupported column: " + field);
            }
        }
        return result;
    }

    @Override
    public String[] getColumnNames() {
        return mProjection;
    }

    int memodCnt = 0;

    @Override
    public int getCount() {
        int nRet = 0;
        if (memodCnt > 0)
            return memodCnt;
        if (mFileBrowserRef != null) {
            ASPFileBrowser<?> objBrowser = mFileBrowserRef.getFileBrowser();
            if (objBrowser.getClass() == CachedFileBrowser.class) {
                CachedFileBrowser objVBrowser = (CachedFileBrowser) objBrowser;
                nRet = objVBrowser.getCurrentSize();
            } else {
                nRet = objBrowser.getCount();
            }
            memodCnt = nRet;
        } else {
            return 0;
        }
        return nRet;
    }

    @Override
    public void fillWindow(int position, CursorWindow window) {
        Long nNull = Long.valueOf(0);
        String strNull = "NULL";
        clearOrCreateWindow2(mInstanceNumber);

        mWindow.setNumColumns(mFieldMap.length);
        int fillPos = FrameworkReflector.DatabaseUtils_cursorPickFillWindowStartPosition(this, position, mLastFilledWindowSize);
        int endPos = fillPos + mLastFilledWindowSize;
        ASPFileBrowser<?> fileBrowser = null;
        if (mFileBrowserRef != null) {
            fileBrowser = mFileBrowserRef.getFileBrowser();
            if (endPos == fillPos) {
                endPos = fileBrowser.getCount();
            }
        }
        mWindow.setStartPosition(fillPos);

        if (fileBrowser == null) {
            return;
        }

        for (; fillPos < endPos; ++fillPos) {
            if (!mWindow.allocRow()) {
                mLastFilledWindowSize = mWindow.getNumRows();
                break;
            }
            ASPFile file = fileBrowser.getFileNonBlocking(fillPos);

            for (int i = 0; i < mFieldMap.length; ++i) {
                boolean success = false;
                try {
                    ASPFileSpecialType specialType = getFileSpecialType(file);
                    switch (mFieldMap[i]) {
                        case FILE_ID:
                            if (file != null) {
                                success = addStringToWindow(getStorageGatewayId(file), fillPos, i);
                            } else {
                                success = addStringToWindow(strNull, fillPos, i);
                            }
                            break;
                        case LOCAL_DATA:
                            if (file instanceof LocalASPFileSLPF) {
                                success = addStringToWindow(((LocalASPFileSLPF) file).getFile().getAbsolutePath(), fillPos, i);
                            } else {
                                success = addStringToWindow(strNull, fillPos, i);
                            }
                            break;
                        case DISPLAY_NAME:
                            if (file != null) {
                                success = addStringToWindow(getLocalizedFileName(file, specialType), fillPos, i);
                            } else {
                                success = addStringToWindow(strNull, fillPos, i);
                            }
                            break;
                        case LAST_MODIFIED:
                            if (file != null) {
                                success = mWindow.putLong(file.lastModified(), fillPos, i);
                            } else {
                                success = addLongToWindow(nNull, fillPos, i);
                            }
                            break;
                        case MIME_TYPE: { //jsublee_150901
                            String mimeType = strNull;
                            if (file != null) {
                                if (file.isDirectory()) {
                                    mimeType = CloudGatewayMediaStore.FileBrowser.MIME_TYPE_DIR;
                                } else if (file.getClass() == CachedCloudFile.class) {
                                    CachedCloudFile objCloudFile = (CachedCloudFile) file;
                                    Bundle objBundle = (Bundle) objCloudFile.getEtcDataObject();
                                    if (objBundle != null) {
                                        mimeType = objBundle.getString("asp_mimetype");
                                    }
                                }
                            }
                            success = addStringToWindow(mimeType, fillPos, i);
                            break;
                        }
                        case SIZE:
                            if (file != null) {
                                success = mWindow.putLong(file.length(), fillPos, i);
                            } else {
                                success = addLongToWindow(null, fillPos, i);
                            }
                            break;
                        case _ID:
                            success = mWindow.putLong(fillPos, fillPos, i);
                            break;
                        case MEDIA_ID: {
                            int nMediaId = -1;
                            if (file != null && file.getClass() == CachedCloudFile.class) {
                                CachedCloudFile objCloudFile = (CachedCloudFile) file;
                                Bundle objBundle = (Bundle) objCloudFile.getEtcDataObject();
                                if (objBundle != null) {
                                    nMediaId = objBundle.getInt("asp_mediaId");
                                }
                            }
                            success = mWindow.putLong(nMediaId, fillPos, i);
                            break;
                        }
                        case DEVICE_ID: {
                            int nDeviceId = 0;
                            if (file != null && file.getClass() == CachedCloudFile.class) {
                                CachedCloudFile objCloudFile = (CachedCloudFile) file;
                                nDeviceId = objCloudFile.getDeviceId();
                            }
                            success = mWindow.putLong(nDeviceId, fillPos, i);
                            break;
                        }
                        case FULL_URI: {
                            String full_uri = "";
                            if (file != null && file.getClass() == CachedCloudFile.class) {
                                CachedCloudFile objCloudFile = (CachedCloudFile) file;
                                Bundle objBundle = (Bundle) objCloudFile.getEtcDataObject();
                                if (objBundle != null) {
                                    full_uri = objBundle.getString("asp_full_uri");
                                }
                            }
                            if (full_uri == null) {
                                full_uri = "";
                            }
                            success = mWindow.putString(full_uri, fillPos, i);
                            break;
                        }
                        //jsub12_151006
                        case CHILD_CNT: {
                            int childCnt = 0;
                            if (file != null && file.getClass() == CachedCloudFile.class) {
                                CachedCloudFile objCloudFile = (CachedCloudFile) file;
                                Bundle objBundle = (Bundle) objCloudFile.getEtcDataObject();
                                if (objBundle != null) {
                                    childCnt = objBundle.getInt("asp_childCnt");
                                }
                            }
                            success = mWindow.putLong(childCnt, fillPos, i);
                            break;
                        }
                        case CHILD_DIR_CNT: {
                            int childDirCnt = 0;
                            if (file != null && file.getClass() == CachedCloudFile.class) {
                                CachedCloudFile objCloudFile = (CachedCloudFile) file;
                                Bundle objBundle = (Bundle) objCloudFile.getEtcDataObject();
                                if (objBundle != null) {
                                    childDirCnt = objBundle.getInt("asp_childDirCnt");
                                }
                            }
                            success = mWindow.putLong(childDirCnt, fillPos, i);
                            break;
                        }
                        case DESCENDANTS_CNT: {
                            int descendantsCnt = 0;
                            if (file != null && file.getClass() == CachedCloudFile.class) {
                                CachedCloudFile objCloudFile = (CachedCloudFile) file;
                                Bundle objBundle = (Bundle) objCloudFile.getEtcDataObject();
                                if (objBundle != null) {
                                    descendantsCnt = objBundle.getInt("asp_descendantsCnt");
                                }
                            }
                            success = mWindow.putLong(descendantsCnt, fillPos, i);
                            break;
                        }
                        case DESCENDANTS_DIR_CNT: {
                            int descendantDirCnt = 0;
                            if (file != null && file.getClass() == CachedCloudFile.class) {
                                CachedCloudFile objCloudFile = (CachedCloudFile) file;
                                Bundle objBundle = (Bundle) objCloudFile.getEtcDataObject();
                                if (objBundle != null) {
                                    descendantDirCnt = objBundle.getInt("asp_descendantDirCnt");
                                }
                            }
                            success = mWindow.putLong(descendantDirCnt, fillPos, i);
                            break;
                        }
                        case TRASH_PROCESSING: {
                            String processing = null;
                            if (file != null && file.getClass() == CachedCloudFile.class) {
                                CachedCloudFile objCloudFile = (CachedCloudFile) file;
                                Bundle objBundle = (Bundle) objCloudFile.getEtcDataObject();
                                if (objBundle != null) {
                                    processing = objBundle.getString("asp_trashProcessing");
                                }
                                processing = (processing != null) ? processing : "dummy";
                                success = mWindow.putString(processing, fillPos, i);
                            }
                            break;
                        }
                    }
                } catch (IOException e) {
                    throw new StaleDataException(e.toString());
                }

                if (!success) {
                    mWindow.freeLastRow();
                    mLastFilledWindowSize = mWindow.getNumRows();
                    endPos = fillPos;
                    break;
                }
            }
        }
    }

    @Override
    public boolean onMove(int oldPosition, int newPosition) {
        // Make sure the row at newPosition is present in the window
        if (mWindow == null
                || newPosition < mWindow.getStartPosition()
                || newPosition >= (mWindow.getStartPosition() + mWindow.getNumRows())) {
            fillWindow(newPosition, mWindow);
        }

        return true;
    }

    @Override
    public void close() {
        super.close();
        if (mReleaseRefOnClose) {
            mReleaseRefOnClose = false;
            String strSelection = mSelection;
            if (mFirstArg != null)
                strSelection += "&" + mFirstArg;
            Log.d(this, "releaseLoadedFileBrowser : " + mFileBrowserRef + " ,deviceId : " + mDeviceId + " ,dirID : " + mRemoteFolderId + " ,sortType : " + mSortType);
            ASPFileBrowserManager.getInstance().releaseLoadedFileBrowser(
                    mFileBrowserRef,
                    mDeviceId,
                    mRemoteFolderId,
                    mSortType,
                    strSelection);
            mFileBrowserRef = null;
        }
    }

    protected void clearOrCreateWindow2(String name) {
        if (mWindow == null) {
            mWindow = new CursorWindow(name);
        } else {
            mWindow.clear();
        }
    }
}

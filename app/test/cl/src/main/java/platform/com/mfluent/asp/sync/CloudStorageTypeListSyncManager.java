
package platform.com.mfluent.asp.sync;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;

import com.mfluent.log.Log;
import com.samsung.android.sdk.slinkcloud.CloudGatewayDeviceTransportType;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.Collection;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import platform.com.mfluent.asp.datamodel.DataModelSLPF;
import platform.com.mfluent.asp.datamodel.DeviceSLPF;
import platform.com.mfluent.asp.filetransfer.FileTransferTask;
import platform.com.mfluent.asp.framework.IASPApplication2;
import platform.com.mfluent.asp.framework.ServiceLocatorSLPF;
import platform.com.mfluent.asp.framework.StorageGatewayManager;
import platform.com.mfluent.asp.framework.StorageProviderDatabaseHelper;
import platform.com.mfluent.asp.framework.StorageProviderInfo;
import platform.com.mfluent.asp.ui.StorageSignInOutHelper;
import platform.com.mfluent.asp.util.DeviceUtilSLPF;
import uicommon.com.mfluent.asp.util.AccountUtil;

public class CloudStorageTypeListSyncManager extends SingleThreadedSyncManagerSLPF {

    private static final long PROD_TIMEOUT = TimeUnit.HOURS.toMillis(12);

    private static final long DEV_TIMEOUT = PROD_TIMEOUT;

    private static final long UPGRAGE_CHECK_TIMEOUT_IN_MILLIS = IASPApplication2.PRODUCTION_BUILD ? PROD_TIMEOUT : DEV_TIMEOUT;

    private static final String SYNC_ON_TIMER_INTENT = "SYNC_ON_TIMER_INTENT";

    private static final String CLOUDPLUGINS_DIR = "cloudplugins";

    private long previousSyncTime = 0;

    private static final String PREVIOUS_SYNC_TIME_KEY = "com.mfluent.asp.sync.CloudStorageTypeListSyncManager.PREVIOUS_SYNC_TIME";

    private Timer upgradeTimer = null;

    public static final boolean STORAGE_JAR_REINSTALL_EVERYTIME_TO_TEST = true;
    private CloudJarReinstallReceiver cloudJarReinstaller = null;

    public CloudStorageTypeListSyncManager(Context context) {
        super(context);
        if (STORAGE_JAR_REINSTALL_EVERYTIME_TO_TEST && cloudJarReinstaller == null) {
            cloudJarReinstaller = new CloudJarReinstallReceiver();
            LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(context);
            localBroadcastManager.registerReceiver(cloudJarReinstaller, new IntentFilter("CloudJarReinstallReceiver.BR"));
        }
    }

    private void startUpgradeMonitor() {
        // We only setup a single upgrade timer
        if (upgradeTimer != null) {
            return;
        }
        Log.i(this, "startUpgradeMonitor() - init cloud upgrade timer");

        upgradeTimer = new Timer();
        TimerTask upgradeTask = new TimerTask() {

            @Override
            public void run() {
                try {
                    if (isAppOnForeground()) {
                        return;
                    }

                    sync(new Intent(SYNC_ON_TIMER_INTENT));
                } catch (Exception e) {
                    Log.d(this, "startUpgradeMonitor() - Exception : " + e);
                }
            }
        };

        upgradeTimer.schedule(upgradeTask, 1000, UPGRAGE_CHECK_TIMEOUT_IN_MILLIS);
    }

    @Override
    protected boolean syncOnServiceCreate() {
        return true;
    }

    @Override
    protected void doSync(Intent intent) {
        if (FileTransferTask.bDuringMultiChannelTransfer) {
            return;
        }
        startUpgradeMonitor();
        if (previousSyncTime == 0) {
            unpersistPreviousSyncTime();
        }
        int syncReason = getSyncReason(intent);

        long start = System.currentTimeMillis();
        Log.i(this, "doSync() - start sync. Reason: " + syncReason);

        syncStorageProviderListFromFramework();

        syncStorageList();

        try {
            syncStorageDeviceList();
        } catch (InterruptedException e) {
            Log.e(this, "doSync() - InterruptedException : " + e);
        } finally {
            Log.i(this, "doSync() - finish sync " + (System.currentTimeMillis() - start) + " ms");
        }

        previousSyncTime = System.currentTimeMillis();
        persistPreviousSyncTime();
    }

    @Override
    protected Collection<IntentFilter> getSyncIntentFilters() {
        Collection<IntentFilter> syncIntentFilters = super.getSyncIntentFilters();
        syncIntentFilters.add(new IntentFilter(DataModelSLPF.BROADCAST_REFRESH_ALL));
        syncIntentFilters.add(new IntentFilter(IASPApplication2.BROADCAST_GO_HOME));

        return syncIntentFilters;
    }

    @Override
    protected void doStop(Intent intent) {
        super.doStop(intent);

        if (upgradeTimer != null) {
            upgradeTimer.cancel();
            upgradeTimer = null;
        }
    }

    private void syncStorageProviderListFromFramework() {
        StorageGatewayManager sgManager = StorageGatewayManager.getInstance(mContext);
        sgManager.getStorageProviderList();
    }

    public static void checkCloudTest() {
        if (STORAGE_JAR_REINSTALL_EVERYTIME_TO_TEST) {
            try {
                IASPApplication2 aspApp = ServiceLocatorSLPF.get(IASPApplication2.class);
                File dexdir = new File(aspApp.getDir(CLOUDPLUGINS_DIR, Context.MODE_PRIVATE).getAbsolutePath());
                File[] files = dexdir.listFiles();
                if (files != null) {
                    for (File file : files) {
                        if (file.isDirectory() == false) {
                            file.delete();
                        }
                    }
                }
            } catch (Exception e) {
                Log.e(CloudStorageTypeListSyncManager.class.getSimpleName(), "checkCloudTest() - Exception : " + e);
            }
        }
    }

    /**
     * "SpName":"ndrive",
     * "ver":"1",
     * "download":"http:\/\/d3j2aiqzxb5fab.cloudfront.net\/jar_collection\/ndrive.jar",
     * "jarName":"ndrive.jar",
     * "cacheDisabled":"N",
     * "mainClass":"com.mfluent.cloud.ndrive.NDrive"
     */
    private void syncStorageList() {
        JSONObject json;
        try {
            Log.v(this, "syncStorageList() called");

            StorageProviderDatabaseHelper dbHelper = StorageProviderDatabaseHelper.getInstance(mContext);

            IASPApplication2 aspApp = ServiceLocatorSLPF.get(IASPApplication2.class);
            json = new JSONObject(aspApp.getContentOfFileInAsset("clouds/catalog.json"));

            JSONArray items = json.getJSONArray("Items");
            for (int i = 0; i < items.length(); i++) {
                try {
                    JSONObject item = items.getJSONObject(i);

                    String provider = item.getString("SpName");
                    if (StringUtils.isEmpty(provider)) {
                        Log.e(this, "syncStorageList() - Did not expect an empty provider here.");
                        continue;
                    }
                    StorageProviderInfo storageInfo = dbHelper.get(provider);

                    if (storageInfo == null) {
                        // The catalog has all possible storage plugins
                        // If the storageInfo does not already exist just got to
                        // next item.
                        Log.d(this, "syncStorageList() - Will not load: " + provider);
                        continue;
                    }

                    Log.d(this, "syncStorageList() - load : " + provider);

                    String mainClass = item.getString("mainClass");
                    storageInfo.setMain(mainClass);

                    storageInfo.setSupportsSignUp(false); //supportsSignup(storageInfo));

                    int maxNumTxConn = item.getInt("maxNumTxConn");
                    storageInfo.setMaxNumTxConn(maxNumTxConn);

                    String OAuthWebViewJS = item.getString("OAuthWebViewJS");
                    storageInfo.setOAuthWebViewJS(OAuthWebViewJS);

                    // TODOMK check the resource version
                    dbHelper.saveOrUpdate(storageInfo);
                } catch (Exception e) {
                    Log.e(this, "syncStorageList() - Failed up update plugin information because: " + e.getMessage() + " json(" + json.toString() + ")");
                }
            }
        } catch (RuntimeException runE) {
            Log.e(this, "syncStorageList() - RuntimeException : " + runE.getMessage());
        } catch (Exception e) {
            Log.e(this, "Trouble syncing web storage list because " + e.getMessage());
            return;
        }
    }

    private void syncStorageDeviceList() throws InterruptedException {
        DataModelSLPF dataModel = DataModelSLPF.getInstance();

        final ConcurrentHashMap<String, DeviceSLPF> webTypeToDeviceMap = new ConcurrentHashMap<String, DeviceSLPF>();
        for (DeviceSLPF device : dataModel.getDevicesByType(CloudGatewayDeviceTransportType.WEB_STORAGE)) {
            webTypeToDeviceMap.put(device.getWebStorageType(), device);
        }

        StorageProviderDatabaseHelper providerDB = StorageProviderDatabaseHelper.getInstance(mContext);
        List<StorageProviderInfo> allProviders = providerDB.findAllStorageProviders();
        boolean needSyncCloud = false;
        for (StorageProviderInfo provider : allProviders) {
            DeviceSLPF device = dataModel.getFirstDeviceForStorageType(provider.getSpName());
            webTypeToDeviceMap.remove(provider.getSpName());

            if (device == null) {
                device = new DeviceSLPF(mContext);
                device.setDeviceTransportType(CloudGatewayDeviceTransportType.WEB_STORAGE);
                if (StringUtils.isEmpty(provider.getSpName())) {
                    Log.e(this, "syncStorageDeviceList() - Storage type should not be null here.");
                }

                device.setWebStorageType(provider.getSpName());
                if (device.getWebStorageType().indexOf(DeviceUtilSLPF.GOOGLEDRIVE_WEBSTORAGE_NAME) >= 0) {
                    device.setWebStorageAccountExist(AccountUtil.hasGoogleAccount(mContext));
                }
            }

            device.setAliasName(provider.getName());
            if (provider.isLoginStatus() && !device.isWebStorageSignedIn() && (device.getId() <= 0)) {
                device.setWebStorageSignedIn(true);
            }

            int storageOrderNum = provider.getSortKey();
            device.setServerSortKey(storageOrderNum);

            device.setMaxNumTxConnection(provider.getMaxNumTxConn());
            device.setOAuthWebViewJS(provider.getOAuthWebViewJS());

            if (device.commitChanges()) {
                device.broadcastDeviceStateChange();
                device.setCloudNeedFirstUpdate(true);
                needSyncCloud = true;
            }
        }
        if (needSyncCloud) {
            Log.d(this, "syncStorageDeviceList() - needSyncCloud");

            final Handler ssome = new Handler(mContext.getMainLooper());
            ssome.postDelayed(new Runnable() {
                @Override
                public void run() {
                    IASPApplication2 asp = ServiceLocatorSLPF.get(IASPApplication2.class);
                    LocalBroadcastManager.getInstance(asp).sendBroadcast(new Intent(CloudStorageSyncManager.strCloudRefreshEvent));
                }
            }, 9000);
        }

        for (DeviceSLPF device : webTypeToDeviceMap.values()) {
            if (device.getPendingSetup() == false) {
                Log.i(this, "syncStorageDeviceList() - Removing: storage device: " + device);

                StorageSignInOutHelper signOutHelper = new StorageSignInOutHelper(mContext);
                signOutHelper.signOutOfStorageService(device);
            }
        }

    }

    private boolean isAppOnForeground() {
        Context context = ServiceLocatorSLPF.get(IASPApplication2.class);

        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<RunningAppProcessInfo> runningProcesses = activityManager.getRunningAppProcesses();
        if (runningProcesses == null) {
            return false;
        }
        final String packageName = context.getPackageName();
        for (RunningAppProcessInfo runningProcess : runningProcesses) {
            if (runningProcess.importance == RunningAppProcessInfo.IMPORTANCE_FOREGROUND && runningProcess.processName.equals(packageName)) {
                return true;
            }
        }
        return false;
    }

    public void resetPreviousSyncTime() {
        previousSyncTime = 0L;
        persistPreviousSyncTime();
    }

    private void persistPreviousSyncTime() {
        SharedPreferences preferences = getSharedPreferences();
        Editor edit = preferences.edit();
        if (previousSyncTime == 0) {
            edit.remove(PREVIOUS_SYNC_TIME_KEY);
        } else {
            edit.putLong(PREVIOUS_SYNC_TIME_KEY, previousSyncTime);
        }
        edit.apply();
    }

    private void unpersistPreviousSyncTime() {
        SharedPreferences preferences = getSharedPreferences();
        previousSyncTime = preferences.getLong(PREVIOUS_SYNC_TIME_KEY, 0);
    }

    private SharedPreferences getSharedPreferences() {
        IASPApplication2 aspApplication = ServiceLocatorSLPF.get(IASPApplication2.class);
        String instanceName = getClass().getName();
        SharedPreferences preferences = aspApplication.getSharedPreferences(instanceName, Context.MODE_PRIVATE);
        return preferences;
    }

    private static class CloudJarReinstallReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.v(this, "onReceive of CloudJarReinstallReceiver, intent : " + intent.getAction());
            CloudStorageTypeListSyncManager.checkCloudTest();
        }
    }
}


package platform.com.mfluent.asp.datamodel.call;

import android.content.Context;
import android.os.Bundle;

public interface CallHandler {

	Bundle call(Context context, String method, String arg, Bundle extras);
}

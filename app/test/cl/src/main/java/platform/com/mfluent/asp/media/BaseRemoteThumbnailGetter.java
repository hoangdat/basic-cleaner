
package platform.com.mfluent.asp.media;

import android.net.Uri;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;

import com.mfluent.asp.common.media.thumbnails.ImageInfo;
import com.mfluent.log.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilterInputStream;
import java.io.InputStream;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import platform.com.mfluent.asp.datamodel.ASPMediaStoreProvider;
import platform.com.mfluent.asp.datamodel.DataModelSLPF;
import platform.com.mfluent.asp.framework.IASPApplication2;
import platform.com.mfluent.asp.util.FileUtils;
import platform.com.mfluent.asp.util.IOUtils;
import platform.com.mfluent.asp.util.MFLStorageManagerSLPF;
import uicommon.com.mfluent.asp.util.FileImageInfo;

public abstract class BaseRemoteThumbnailGetter extends ThumbnailGetter implements ASPMediaStoreProvider.PipeDataWriter<ImageInfo> {

    private static final int STATE_CONNECTING = 0;
    protected static final int STATE_HAS_SOME_DATA = 1;
    protected static final int STATE_FAILED = 2;

    private int mState;

    private final ReentrantLock mLock;
    private final Condition mCondition;

    private final AspThumbnailCache mFileCache;

    public BaseRemoteThumbnailGetter(ImageInfo mediaInfo, AspThumbnailCache fileCache) {
        super(mediaInfo);
        mFileCache = fileCache;
        mState = STATE_CONNECTING;
        mLock = new ReentrantLock();
        mCondition = mLock.newCondition();
    }

    protected void setState(int state) throws InterruptedException {
        mLock.lockInterruptibly();
        try {
            if (mState != state) {
                mState = state;
                mCondition.signal();
            }
        } finally {
            mLock.unlock();
        }
    }

    protected abstract File openRemoteFile(String tmpPathToDownload, String tmpNameFile) throws Exception;

    @Override
    protected ParcelFileDescriptor getThumbnailFileDescriptor(
            ASPMediaStoreProvider provider,
            boolean getFromCache,
            boolean putInCache,
            boolean readCacheOnly,
            long groupId) throws FileNotFoundException, InterruptedException {
        ParcelFileDescriptor result = null;

        if (IASPApplication2.checkNeedWifiOnlyBlock()) {
            readCacheOnly = true;
        }

        if (getFromCache || readCacheOnly) {
            FileImageInfo fileInfo = mFileCache.getFile(getMediaInfo());
            if (fileInfo != null && fileInfo.getFile() != null && fileInfo.getFile().exists()) {
                result = ParcelFileDescriptor.open(fileInfo.getFile(), ParcelFileDescriptor.MODE_READ_ONLY);
            }
        }

        if (result == null && !readCacheOnly) {
            if (!putInCache) {
                result = getRemoteThumbnailFileDescriptor(provider);
            } else {
                FileImageInfo fileInfo = null;
                try {
                    fileInfo = getThumbnail();
                } catch (InterruptedException ie) {
                    throw ie;
                } catch (Exception e) {
                    Log.d(this, "getThumbnailFileDescriptor() - Exception : " + e);
                }
                if (fileInfo != null && fileInfo.getFile() != null && fileInfo.getFile().exists()) {
                    result = ParcelFileDescriptor.open(fileInfo.getFile(), ParcelFileDescriptor.MODE_READ_ONLY);
                }
            }
        }

        if (result == null) {
            throw new FileNotFoundException("Failed to retrieve thumbnail for ImageInfo: " + getMediaInfo());
        }

        return result;
    }

    private ParcelFileDescriptor getRemoteThumbnailFileDescriptor(ASPMediaStoreProvider provider) throws FileNotFoundException, InterruptedException {
        ParcelFileDescriptor result = provider.openPipeHelper(null, null, null, getMediaInfo(), this);

        mLock.lockInterruptibly();
        try {
            while (mState == STATE_CONNECTING) {
                mCondition.await();
            }

            if (mState == STATE_FAILED) {
                if (result != null) {
                    IOUtils.closeQuietly(result);
                }
                result = null;
            }
        } finally {
            mLock.unlock();
        }

        if (result == null) {
            throw new FileNotFoundException();
        }

        return result;
    }

    @Override
    public FileImageInfo getCachedFileImageInfo() {
        return this.mFileCache.getFile(getMediaInfo());
    }

    @Override
    public void writeDataToPipe(ParcelFileDescriptor output, Uri uri, String mimeType, Bundle opts, ImageInfo args) {
        File file = null;
        InputStream content = null;
        FileOutputStream dest = null;
        byte[] buffer = new byte[8 * 1024];

        ImageInfo mediaInfo = getMediaInfo();
        String tmpThumbName = "Thumbnail.tmp." + mediaInfo.getMediaType() + "_" + mediaInfo.getContentId() + "_" + mediaInfo.getDeviceId();

        try {
            file = openRemoteFile(getTmpThumbnailDirectory(), tmpThumbName);
            if (file == null || (file != null && !file.exists())) {
                Log.e(this, "writeDataToPipe() - file is null");
                return;
            }
            content = new FileInputStream(file);
            if (content == null) {
                Log.e(this, "writeDataToPipe() - content is null");
                return;
            }

            dest = new FileOutputStream(output.getFileDescriptor());

            int readResult = 0;
            while (readResult >= 0) {
                readResult = content.read(buffer);
                if (readResult > 0) {
                    setState(STATE_HAS_SOME_DATA);
                    dest.write(buffer, 0, readResult);
                }
            }
        } catch (Exception e) {
            Log.e(this, "writeDataToPipe() - Exception : " + e);
            try {
                setState(STATE_FAILED);
            } catch (InterruptedException ie) {
                Log.e(this, "writeDataToPipe() - InterruptedException : " + e);
            }
        } finally {
            IOUtils.closeQuietly(content);
            IOUtils.closeQuietly(dest);
            if (file != null) {
                FileUtils.deleteQuietly(file);
            }
        }
    }

    @Override
    protected final FileImageInfo getThumbnail() throws Exception {
        FileImageInfo result = getCachedFileImageInfo();
        if (result != null)
            return result;

        File tmpFile = null;
        ImageInfo mediaInfo = getMediaInfo();
        String tmpThumbName = "Thumbnail.tmp." + mediaInfo.getMediaType() + "_" + mediaInfo.getContentId() + "_" + mediaInfo.getDeviceId();

        try {
            tmpFile = openRemoteFile(getTmpThumbnailDirectory(), tmpThumbName);

            if (tmpFile != null && tmpFile.exists()) {
                mFileCache.putFile(mediaInfo, tmpFile);
                result = mFileCache.getFile(mediaInfo);
            }
        } finally {
            FileUtils.deleteQuietly(tmpFile);
        }

        return result;
    }

    public static class InputStreamWithContentLength extends FilterInputStream {

        private long mContentLength = -1L;

        public InputStreamWithContentLength(InputStream is) {
            super(is);
        }

        public void setContentLength(long contentLength) {
            mContentLength = contentLength;
        }

        public long getContentLength() {
            return mContentLength;
        }
    }

    private String getTmpThumbnailDirectory() {
        File cacheDir = new File(MFLStorageManagerSLPF.getCacheDir(DataModelSLPF.getInstance().getContext()), "thumbnail_tmp");
        cacheDir.mkdirs();

        return cacheDir.getAbsolutePath();
    }

    private File createThumbnailTemp(String fileName) {
        return new File(getTmpThumbnailDirectory(), fileName);
    }
}

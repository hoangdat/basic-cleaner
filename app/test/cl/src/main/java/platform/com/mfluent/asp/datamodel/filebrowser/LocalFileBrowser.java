/**
 *
 */

package platform.com.mfluent.asp.datamodel.filebrowser;

import android.annotation.SuppressLint;
import android.database.DataSetObserver;
import android.os.Environment;
import android.os.FileObserver;
import android.os.storage.StorageVolume;
import android.util.Base64;

import com.mfluent.asp.common.datamodel.ASPFileBrowser;
import com.mfluent.asp.common.datamodel.ASPFileSelectionHelper;
import com.mfluent.asp.common.datamodel.ASPFileSortType;
import com.mfluent.asp.common.datamodel.ASPFileSpecialType;
import com.mfluent.log.Log;
import com.samsung.android.sdk.slinkcloud.CloudGatewayMediaStore;

//import org.apache.commons.io.comparator.CompositeFileComparator;
//import org.apache.commons.io.comparator.ExtensionFileComparator;
//import org.apache.commons.io.comparator.LastModifiedFileComparator;
//import org.apache.commons.io.comparator.SizeFileComparator;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.FileFilter;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import platform.com.mfluent.asp.framework.ServiceLocatorSLPF;
import platform.com.mfluent.asp.util.MFLStorageManagerSLPF;
import platform.com.mfluent.asp.util.StorageStatusSLPF;

/**
 * @author Ilan Klinghofer
 */
@SuppressLint("SdCardPath")
public class LocalFileBrowser implements ASPFileBrowser<LocalASPFileSLPF> {

    private static final Set<String> OEM_HIDDEN_PATHS = new HashSet<String>();

    private static final Set<String> SYSTEM_FOLDERS = new HashSet<String>();

    static {
        OEM_HIDDEN_PATHS.add("/acct");
        OEM_HIDDEN_PATHS.add("/mnt/asec");
        OEM_HIDDEN_PATHS.add("/mnt/obb");
        OEM_HIDDEN_PATHS.add("/mnt/secure");

        // if multi-user device, only show "/storage/emulated/<user id>" to match My Files app
        // - on multi-user devices, /storage/sdcard0 is a symbolic link to /storage/emulated/legacy
        if (MFLStorageManagerSLPF.hasEmulatedStorage()) {
            OEM_HIDDEN_PATHS.add("/storage/emulated/legacy");
            OEM_HIDDEN_PATHS.add("/storage/sdcard0");
        }

        // adding additional entry for China device
        SYSTEM_FOLDERS.add("/storage/container");

        SYSTEM_FOLDERS.add("/system");
        SYSTEM_FOLDERS.add("/sys");
        SYSTEM_FOLDERS.add("/proc");
    }

    private LocalASPFileSLPF currentDirectory;

    private LocalASPFileSLPF parentDirectory;

    private final List<File> files = new ArrayList<File>();

    private FileFilter fileFilter = null;

    private final Lock initLock = new ReentrantLock();

    private final Condition initializedCondition = this.initLock.newCondition();

    private boolean initialized;

    private ASPFileSelectionHelper selectionHelper;

    private DataSetObserver dataSetObserver = null;

    private FileObserver currentDirectoryObserver;

    public static LocalASPFileSLPF getFileFromStorageGatewayId(String storageGatewayId) {
        if (StringUtils.isEmpty(storageGatewayId)) {
            return null;
        }
        File file;
        if (CloudGatewayMediaStore.FileBrowser.ROOT_DIRECTORY_ID.equals(storageGatewayId)) {
            //file = LocalFileBrowser.getRootDirectory();
            file = MFLStorageManagerSLPF.getStorageParentDir(false);

        } else {
            String decodedPath = new String(Base64.decode(storageGatewayId, Base64.NO_WRAP | Base64.URL_SAFE));
            file = new File(decodedPath);
        }
        return new LocalASPFileSLPF(file);
    }

    public static String getStorageGatewayId(LocalASPFileSLPF aspFile) {
        File file = aspFile.getFile();
        String path = file.getAbsolutePath();
        return new String(Base64.encode(path.getBytes(), Base64.NO_WRAP | Base64.URL_SAFE));
    }

    /*
     * (non-Javadoc)
	 * @see com.mfluent.asp.datamodel.ASPFileBrowser#init(com.mfluent.asp.datamodel.ASPFile, com.mfluent.asp.datamodel.ASPFileBrowser.FileSortType)
	 */
    @Override
    public boolean init(LocalASPFileSLPF directory, ASPFileSortType fileSortType, boolean forceReload) throws InterruptedException {
        initLock.lockInterruptibly();

        try {
            initialized = false;
            files.clear();

            if (directory == null) {
                directory = new LocalASPFileSLPF(MFLStorageManagerSLPF.getStorageFilesDirectory(false));
            }

            File rootFile = LocalFileBrowser.getRootDirectory();
            File intStorage = Environment.getExternalStorageDirectory();
            File extStorage = StorageStatusSLPF.getSDCardMemoryPath();
            File currFile = directory.getFile();
            currentDirectory = directory;
            if (currFile.equals(rootFile)) {
                parentDirectory = null;
            } else if (currFile.equals(intStorage) || currFile.equals(extStorage)) {
                currentDirectory.setSpecialType(currFile.equals(intStorage) ? ASPFileSpecialType.INTERNAL_STORAGE : ASPFileSpecialType.EXTERNAL_STORAGE);
                parentDirectory = new LocalASPFileSLPF(rootFile);
            } else {
                File parentFile = currentDirectory.getFile().getParentFile();
                parentDirectory = new LocalASPFileSLPF(parentFile);
            }

            doFilter(true);
            doSort(fileSortType);
            initializeSelectionHelper();
            initialized = true;
            initializedCondition.signalAll();
        } finally {
            initLock.unlock();
        }
        return true;
    }

    public static File getRootDirectory() {
        // NOTE: don't pass 'false' to getStorageParentDir() - even though we're calling getStorageFilesDirectory(false) above
        return MFLStorageManagerSLPF.getStorageParentDir().getParentFile();
    }

    /*
     * (non-Javadoc)
     * @see com.mfluent.asp.datamodel.ASPFileBrowser#destroy()
     */
    @Override
    public void destroy() {
        //unregisterDataSetObserver(null);
        files.clear();
    }

    @SuppressWarnings("unchecked")
    private void doSort(ASPFileSortType fileSortType) {

        if (getParentDirectory() == null) {
            /**
             * Root directory is a special case and we won't sort anything.
             */
            return;
        }

        Comparator<File> comparator;
        switch (fileSortType) {
//            case DATE_MODIFIED_ASC:
//                comparator = new DirectoryFirstComparator(new CompositeFileComparator(
//                        LastModifiedFileComparator.LASTMODIFIED_COMPARATOR,
//                        new FileNameCollationComparator(false)));
//                break;
//            case DATE_MODIFIED_DESC:
//                comparator = new DirectoryFirstComparator(new CompositeFileComparator(
//                        LastModifiedFileComparator.LASTMODIFIED_REVERSE,
//                        new FileNameCollationComparator(false)));
//                break;
//            case SIZE_ASC:
//                comparator = new DirectoryFirstComparator(new CompositeFileComparator(
//                        SizeFileComparator.SIZE_COMPARATOR,
//                        new FileNameCollationComparator(false)));
//                break;
//            case SIZE_DESC:
//                comparator = new DirectoryFirstComparator(new CompositeFileComparator(SizeFileComparator.SIZE_REVERSE, new FileNameCollationComparator(false)));
//                break;
            case NAME_ASC:
                comparator = new DirectoryFirstComparator(new FileNameCollationComparator(false));
                break;
            case NAME_DESC:
                comparator = new DirectoryFirstComparator(new FileNameCollationComparator(true));
                break;
//            case TYPE_ASC:
//                comparator = new DirectoryFirstComparator(new CompositeFileComparator(
//                        ExtensionFileComparator.EXTENSION_INSENSITIVE_COMPARATOR,
//                        new FileNameCollationComparator(false)));
//                break;
//            case TYPE_DESC:
//                comparator = new DirectoryFirstComparator(new CompositeFileComparator(
//                        ExtensionFileComparator.EXTENSION_INSENSITIVE_REVERSE,
//                        new FileNameCollationComparator(false)));
//                break;
            default:
                comparator = new DirectoryFirstComparator(new FileNameCollationComparator(false));
                break;
        }

        Collections.sort(files, comparator);
    }

    private void doFilter(boolean bAutoFilter) {
        files.clear();
        fileFilter = null;

        if (getParentDirectory() == null) {
            /*
             * This is a special case where we are at the root directory. We are to only
			 * show the internal storage directory and the external storage directory (if
			 * available).
			 */

            File internalStorage = Environment.getExternalStorageDirectory();
            files.add(internalStorage);

            if (StorageStatusSLPF.isSecondExternalAvailable()) {
                File extStorage = StorageStatusSLPF.getSDCardMemoryPath();
                if (extStorage.exists()) {
                    files.add(StorageStatusSLPF.getSDCardMemoryPath());
                }
            }

            return;
        }

        fileFilter = new FileFilter() {

            @Override
            public boolean accept(File file) {
                return !file.isHidden() && isOKToShow(file);
            }
        };

        File[] filesArray;
        if (bAutoFilter == false) {// || checkNeedFiltering(this.currentDirectory.getFile().getAbsolutePath())) {
            filesArray = currentDirectory.getFile().listFiles(fileFilter);
        } else {
            Log.i(this, "no filter applied");
            filesArray = currentDirectory.getFile().listFiles();
        }
        if (filesArray != null) {
            for (File file : filesArray) {
                files.add(file);
            }
        }
    }

    public boolean isOKToShow(File file) {
        boolean bRet = true;
        String path = file.getAbsolutePath();
        if (isSystemFolder(path) || isOEMHiddenItem(path)) {
            bRet = false;
        } else if (isVolumePath(path) && !isVolumeMounted(path)) {
            bRet = false;
        } else {
            File root = LocalFileBrowser.getRootDirectory();
            if (root == null || !path.contains(root.getAbsolutePath())) {
                Log.e(this, "isOKToShow() - root : " + root);
                bRet = false;
            }
        }

        return bRet;
    }

    private boolean isVolumePath(String path) {
        StorageVolume[] storageVolumes = ServiceLocatorSLPF.get(MFLStorageManagerSLPF.class).getVolumeList();
        for (StorageVolume storageVolume : storageVolumes) {
            if (path.equals(storageVolume.semGetPath())) {
                return true;
            }
        }

        return false;
    }

    private boolean isVolumeMounted(String path) {
        return StorageStatusSLPF.isVolumeMounted(path) == 1;
    }

    private boolean isSystemFolder(String path) {
        return SYSTEM_FOLDERS.contains(path);
    }

    private boolean isOEMHiddenItem(String path) {
        return OEM_HIDDEN_PATHS.contains(path);
    }

    /*
     * (non-Javadoc)
     * @see com.mfluent.asp.datamodel.ASPFileBrowser#getCurrentDirectory()
     */
    @Override
    public LocalASPFileSLPF getCurrentDirectory() {
        return currentDirectory;
    }

    /*
     * (non-Javadoc)
     * @see com.mfluent.asp.datamodel.ASPFileBrowser#getParentDirectory()
     */
    @Override
    public LocalASPFileSLPF getParentDirectory() {
        return parentDirectory;
    }

    /*
     * (non-Javadoc)
     * @see com.mfluent.asp.datamodel.ASPFileBrowser#getCurrentDirectoryAbsolutePath()
     */
    @Override
    public String getCurrentDirectoryAbsolutePath() {
        if (currentDirectory == null) {
            return "";
        }
        return currentDirectory.getFile().getAbsolutePath();
    }

    /*
     * (non-Javadoc)
     * @see com.mfluent.asp.datamodel.ASPFileBrowser#getCount()
     */
    @Override
    public int getCount() {
        return files.size();
    }

    /*
     * (non-Javadoc)
     * @see com.mfluent.asp.datamodel.ASPFileBrowser#getFile(int)
     */
    @Override
    public LocalASPFileSLPF getFile(int index) throws InterruptedException {
        initLock.lockInterruptibly();
        try {
            while (!initialized) {
                initializedCondition.await();
            }

            //if we got here we are initialized
            return getFileNonBlocking(index);
        } finally {
            initLock.unlock();
        }
    }

    /*
     * (non-Javadoc)
     * @see com.mfluent.asp.datamodel.ASPFileBrowser#getFileNonBlocking(int)
     */
    @Override
    public LocalASPFileSLPF getFileNonBlocking(int index) {
        if (index >= files.size()) {
            return null;
        }
        File file = files.get(index);
        LocalASPFileSLPF result = new LocalASPFileSLPF(file);
        if (getParentDirectory() == null) {
            Environment.getExternalStorageDirectory();

            if (file.equals(Environment.getExternalStorageDirectory())) {
                result.setSpecialType(ASPFileSpecialType.INTERNAL_STORAGE);
            } else if (file.equals(StorageStatusSLPF.getSDCardMemoryPath())) {
                result.setSpecialType(ASPFileSpecialType.EXTERNAL_STORAGE);
            }
        }
        return result;
    }

    private void initializeSelectionHelper() {
        selectionHelper = new ASPFileSelectionHelper(files.size());
        for (int i = 0; i < files.size(); i++) {
            if (files.get(i).isDirectory()) {
                selectionHelper.setIsDirectory(i);
            }
        }
    }

    @Override
    public String toString() {
        return "LocalFileBrowser: " + getCurrentDirectoryAbsolutePath() + ", root: " + (parentDirectory == null) + ", files: " + getCount();
    }

    /*
     * (non-Javadoc)
     * @see com.mfluent.asp.datamodel.ASPFileBrowser#registerDataSetObserver(android.database.DataSetObserver)
     */
    @Override
    public void registerDataSetObserver(DataSetObserver observer) {
        if (observer == null) {
            unregisterDataSetObserver(null);
            return;
        }

        dataSetObserver = observer;

        if (currentDirectoryObserver != null) {
            currentDirectoryObserver.stopWatching();
        }

        String path = getCurrentDirectoryAbsolutePath();

        int access = FileObserver.MODIFY
                | FileObserver.ATTRIB
                | FileObserver.MOVED_FROM
                | FileObserver.MOVED_TO
                | FileObserver.DELETE
                | FileObserver.CREATE
                | FileObserver.DELETE_SELF
                | FileObserver.MOVE_SELF;

        currentDirectoryObserver = new FileObserver(path, access) {

            @Override
            public void onEvent(int event, String path) {
                // ignore IN_IGNORED event '32768' (http://stackoverflow.com/questions/10305054/android-fileobserver-passing-undocumented-event-32768)
                if (event > FileObserver.MOVE_SELF) {
                    return;
                }
                if (fileFilter == null
                        || currentDirectory == null
                        || !fileFilter.accept(new File(currentDirectory.getFile(), path))) {
                    return;
                }
                Log.d(this, "registerDataSetObserver() - FileObserver : event = " + event + ", path = " + path);
                notifyObservers();
            }
        };
        currentDirectoryObserver.startWatching();
    }

    /*
     * (non-Javadoc)
     * @see com.mfluent.asp.datamodel.ASPFileBrowser#unregisterDataSetObserver(android.database.DataSetObserver)
     */
    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {
        if (currentDirectoryObserver != null) {
            currentDirectoryObserver.stopWatching();
            currentDirectoryObserver = null;
        }
        dataSetObserver = null;
    }

    /**
     *
     */
    private void notifyObservers() {
        if (dataSetObserver != null) {
            dataSetObserver.onChanged();
        }
    }

    private static class DirectoryFirstComparator implements Comparator<File> {

        private final Comparator<File> baseComparator;

        public DirectoryFirstComparator(Comparator<File> comparator) {
            baseComparator = comparator;
        }

        /*
         * (non-Javadoc)
         * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
         */
        @Override
        public int compare(File lhs, File rhs) {
            if (lhs.isDirectory()) {
                if (rhs.isDirectory()) {
                    return baseComparator.compare(lhs, rhs);
                } else {
                    return -1;
                }

            } else if (rhs.isDirectory()) {
                //lhs is a file
                return 1;
            } else {
                //both are files
                return baseComparator.compare(lhs, rhs);
            }
        }
    }

    private static class FileNameCollationComparator implements Comparator<File> {

        private final Collator collator;

        private final boolean mReverse;

        public FileNameCollationComparator(boolean reverse) {
            mReverse = reverse;
            collator = Collator.getInstance();
        }

        @Override
        public int compare(File lhs, File rhs) {
            int result = collator.compare(lhs.getName().toLowerCase(Locale.getDefault()), rhs.getName().toLowerCase(Locale.getDefault()));

            if (mReverse) {
                result = result * -1;
            }

            return result;
        }

    }
}

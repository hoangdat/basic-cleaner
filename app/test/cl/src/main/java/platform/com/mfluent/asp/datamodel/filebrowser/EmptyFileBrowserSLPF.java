/**
 *
 */

package platform.com.mfluent.asp.datamodel.filebrowser;

import android.database.DataSetObserver;

import com.mfluent.asp.common.datamodel.ASPFileBrowser;
import com.mfluent.asp.common.datamodel.ASPFileSortType;

import java.io.IOException;

/**
 * @author Ilan Klinghofer
 */
public class EmptyFileBrowserSLPF implements ASPFileBrowser<LocalASPFileSLPF> {

    /*
     * (non-Javadoc)
     * @see com.mfluent.asp.datamodel.ASPFileBrowser#destroy()
     */
    @Override
    public void destroy() {
    }

    /*
     * (non-Javadoc)
     * @see com.mfluent.asp.datamodel.ASPFileBrowser#getCurrentDirectory()
     */
    @Override
    public LocalASPFileSLPF getCurrentDirectory() {
        return null;
    }

    /*
     * (non-Javadoc)
     * @see com.mfluent.asp.datamodel.ASPFileBrowser#getParentDirectory()
     */
    @Override
    public LocalASPFileSLPF getParentDirectory() {
        return null;
    }

    /*
     * (non-Javadoc)
     * @see com.mfluent.asp.datamodel.ASPFileBrowser#getCount()
     */
    @Override
    public int getCount() {
        return 0;
    }

    /*
     * (non-Javadoc)
     * @see com.mfluent.asp.datamodel.ASPFileBrowser#getFile(int)
     */
    @Override
    public LocalASPFileSLPF getFile(int index) {
        return null;
    }

    /*
     * (non-Javadoc)
     * @see com.mfluent.asp.datamodel.ASPFileBrowser#getFileNonBlocking(int)
     */
    @Override
    public LocalASPFileSLPF getFileNonBlocking(int index) {
        return null;
    }

    /*
     * (non-Javadoc)
     * @see com.mfluent.asp.datamodel.ASPFileBrowser#getCurrentDirectoryAbsolutePath()
     */
    @Override
    public String getCurrentDirectoryAbsolutePath() {
        return "";
    }

    /*
     * (non-Javadoc)
     * @see com.mfluent.asp.datamodel.ASPFileBrowser#registerDataSetObserver(android.database.DataSetObserver)
     */
    @Override
    public void registerDataSetObserver(DataSetObserver observer) {
    }

    /*
     * (non-Javadoc)
     * @see com.mfluent.asp.datamodel.ASPFileBrowser#unregisterDataSetObserver(android.database.DataSetObserver)
     */
    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {
    }

    @Override
    public boolean init(LocalASPFileSLPF directory, ASPFileSortType fileSortType, boolean forceReload) throws InterruptedException, IOException {
        return true;
    }
}

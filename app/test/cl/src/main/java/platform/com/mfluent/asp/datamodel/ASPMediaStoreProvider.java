
package platform.com.mfluent.asp.datamodel;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ClipDescription;
import android.content.ContentProvider;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.OperationApplicationException;
import android.content.SharedPreferences;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteTransactionListener;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.ParcelFileDescriptor;
import android.os.SystemClock;
import android.support.v4.content.LocalBroadcastManager;
import android.util.LruCache;
import android.util.Pair;

import com.mfluent.asp.cloudstorage.api.sync.CloudStorageSync;
import com.mfluent.asp.common.datamodel.ASPMediaStore;
import com.mfluent.asp.common.util.FileTypeHelper;
import com.mfluent.log.Log;
import com.samsung.android.sdk.slinkcloud.CloudGatewayDeviceTransportType;
import com.samsung.android.sdk.slinkcloud.CloudGatewayMediaStore;

import org.apache.commons.lang3.ArrayUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import platform.com.mfluent.asp.datamodel.MediaInfo.MediaInfoContext;
import platform.com.mfluent.asp.datamodel.call.CallHandler;
import platform.com.mfluent.asp.datamodel.call.GetVersatileInformationCallHandler;
import platform.com.mfluent.asp.datamodel.call.ProviderReservationCallHandler;
import platform.com.mfluent.asp.datamodel.call.RemoteFolderControlBatchCallHandler;
import platform.com.mfluent.asp.datamodel.call.RemoteFolderControlCallHandler;
import platform.com.mfluent.asp.datamodel.filebrowser.CachedFileBrowserCursor;
import platform.com.mfluent.asp.filetransfer.FileTransferManager;
import platform.com.mfluent.asp.filetransfer.FileTransferManagerSingleton;
import platform.com.mfluent.asp.framework.CloudManagerMainService;
import platform.com.mfluent.asp.framework.IASPApplication2;
import platform.com.mfluent.asp.framework.ServiceLocatorSLPF;
import platform.com.mfluent.asp.framework.StorageProviderDatabaseHelper;
import platform.com.mfluent.asp.media.AspThumbnailCache;
import platform.com.mfluent.asp.ui.StorageSignInOutHelper;
import platform.com.mfluent.asp.util.AspCommonUtils;
import platform.com.mfluent.asp.util.DeviceUtilSLPF;
import platform.com.mfluent.asp.util.HandlerFactorySLPF;
import platform.com.mfluent.asp.util.TokenBasedReadWriteLock;

/**
 * Provides access to the asp media store.
 *
 * @author Ilan Klinghofer
 */
public class ASPMediaStoreProvider extends ContentProvider {

    private static DatabaseHelper dbHelper;

    private final TokenBasedReadWriteLock<Long> mLock;

    private final NotifySender notifySender = new NotifySender();

    private ASPMediaStoreProviderTransaction mCurrTransaction = null;

    private enum OldVersions {
        Ver30017(30017),
        Ver30018(30018);

        private int versionNum;

        OldVersions(int versionNum) {
            this.versionNum = versionNum;
        }

        public int getVersionNum() {
            return versionNum;
        }
    }

    private static class PublicApiEntry {
        private static ArrayList<PublicApiEntry> sEntered = new ArrayList<PublicApiEntry>();

        public static synchronized void enter() {
            PublicApiEntry entry = new PublicApiEntry();

            int found = sEntered.indexOf(entry);
            if (found >= 0) {
                sEntered.get(found).mCount++;
            } else {
                sEntered.add(entry);
            }

        }

        public static synchronized void leave() {
            PublicApiEntry entry = new PublicApiEntry();

            int found = sEntered.indexOf(entry);
            if (found >= 0) {
                entry = sEntered.get(found);
                if (entry.mCount <= 1) {
                    sEntered.remove(found);
                } else {
                    entry.mCount--;
                }
            }
        }

        private final Thread mThread = Thread.currentThread();
        private int mCount = 0;

        @Override
        public int hashCode() {
            return this.mThread.hashCode();
        }

        @Override
        public boolean equals(Object o) {
            if (o == null) {
                return false;
            }

            if (o == this) {
                return true;
            }

            PublicApiEntry other = (PublicApiEntry) o;
            return other.mThread.equals(this.mThread);
        }
    }

    /**
     * A UriMatcher instance
     */
    private static UriMatcher URI_MATCHER;

    /*
     * Constants used by the Uri matcher to choose an action based on the
	 * pattern of the incoming URI
	 */
    protected static final int ASP_CONTENT_PATTERN = 1;
    protected static final int ASP_ENTRY_PATTERN = 2;
    protected static final int ASP_DEVICE_CONTENT_PATTERN = 3;
    protected static final int ASP_DEVICE_ORPHAN_CLEANUP_PATTERN = 4;
    protected static final int ASP_FILE_CONTENT_PATTERN = 5;
    protected static final int ASP_TEMPORARY_FILES_PATTERN = 6;
    protected static final int ASP_DEVICE_ENTRY_PATTERN = 7;

    protected static final int ASP_THUMBNAIL_CACHE_PATTERN = 14;

    protected static final int ASP_READ_LOCK_PATTERN = 21;

    ////zero project myfiles, new cursor for myfiles
    protected static final int ASP_FILE_BROWSER_2_DEFAULT_PATTERN = 23;
    ////zero project myfiles, new cursor for myfiles
    protected static final int ASP_FILE_BROWSER_2_PATTERN = 24;
    protected static final int ASP_TRASH_PATTERN = 25;

    protected static final int ASP_STORAGE_PROVIDER_DB_QUERY_PATTERN = 27;

    protected static final int ASP_CLOUD_GATEWAY_STREAM_PATTERN = 28;

    private static ConcurrentHashMap<String, MediaInfo> MEDIA_TYPE_TO_INFO_MAP;

    private static final int MEDIA_TYPE_PATH_SEGMENT = 0;

    private static final int ENTRY_ID_PATH_SEGMENT = 2;

    private static final int DEVICE_ID_PATH_SEGMENT = 2;

    private static Map<String, CallHandler> METHOD_TO_CALL_HANDLER_MAP;

    public static void initProviderUri() {
        MEDIA_TYPE_TO_INFO_MAP = new ConcurrentHashMap<>();
        MEDIA_TYPE_TO_INFO_MAP.put(ASPMediaStore.Device.PATH, DeviceMediaInfo.getInstance());
        MEDIA_TYPE_TO_INFO_MAP.put(ASPMediaStore.Files.PATH, FilesMediaInfo.getInstance());
        MEDIA_TYPE_TO_INFO_MAP.put(CloudGatewayMediaStore.Images.Thumbnails.PATH, ImageThumbnailMediaInfo.getInstance());
        MEDIA_TYPE_TO_INFO_MAP.put(CloudGatewayMediaStore.Video.Thumbnails.PATH, VideoThumbnailMediaInfo.getInstance());

        URI_MATCHER = new UriMatcher(UriMatcher.NO_MATCH);

        URI_MATCHER.addURI(ASPMediaStore.AUTHORITY, ASPMediaStore.ReadLock.PATH, ASP_READ_LOCK_PATTERN);
        URI_MATCHER.addURI(ASPMediaStore.AUTHORITY, ASPMediaStore.ThumbnailCache.PATH + "/*", ASP_THUMBNAIL_CACHE_PATTERN);
        ////security group request to resolve this issue
        //		URI_MATCHER.addURI(ASPMediaStore.AUTHORITY, ASPMediaStore.TemporaryFiles.PATH + "/entry/*", ASP_TEMPORARY_FILES_PATTERN);
        URI_MATCHER.addURI(ASPMediaStore.AUTHORITY, "cloudstream/*", ASP_CLOUD_GATEWAY_STREAM_PATTERN);

        ////zero project myfiles, new cursor for myfiles
        URI_MATCHER.addURI(ASPMediaStore.AUTHORITY, CloudGatewayMediaStore.FileBrowser.FileList2.PATH + "/#", ASP_FILE_BROWSER_2_DEFAULT_PATTERN);
        URI_MATCHER.addURI(ASPMediaStore.AUTHORITY, CloudGatewayMediaStore.FileBrowser.FileList2.PATH + "/#/*", ASP_FILE_BROWSER_2_PATTERN);

        URI_MATCHER.addURI(ASPMediaStore.AUTHORITY, CloudGatewayMediaStore.Trash.PATH + "/#", ASP_TRASH_PATTERN);

        URI_MATCHER.addURI(ASPMediaStore.AUTHORITY, CloudGatewayMediaStore.StorageProvider.PATH + "/*", ASP_STORAGE_PROVIDER_DB_QUERY_PATTERN);

        URI_MATCHER.addURI(ASPMediaStore.AUTHORITY, "*", ASP_CONTENT_PATTERN);
        URI_MATCHER.addURI(ASPMediaStore.AUTHORITY, "*/#", ASP_FILE_CONTENT_PATTERN);
        URI_MATCHER.addURI(ASPMediaStore.AUTHORITY, "*/entry/#", ASP_ENTRY_PATTERN);
        URI_MATCHER.addURI(ASPMediaStore.AUTHORITY, "*/device/#/entry/#", ASP_DEVICE_ENTRY_PATTERN);
        URI_MATCHER.addURI(ASPMediaStore.AUTHORITY, "*/device/#", ASP_DEVICE_CONTENT_PATTERN);

        Map<String, CallHandler> methodToCallHandlerTempMap = new HashMap<String, CallHandler>();
        methodToCallHandlerTempMap.put(CloudGatewayMediaStore.CallMethods.GetVersatileInformation.NAME, new GetVersatileInformationCallHandler());
        methodToCallHandlerTempMap.put(CloudGatewayMediaStore.CallMethods.MRRControl.NAME, new RemoteFolderControlCallHandler());
        methodToCallHandlerTempMap.put(CloudGatewayMediaStore.CallMethods.MRRControlBatch.NAME, new RemoteFolderControlBatchCallHandler());
        methodToCallHandlerTempMap.put(CloudGatewayMediaStore.CallMethods.ProviderReservation.NAME, new ProviderReservationCallHandler());
        METHOD_TO_CALL_HANDLER_MAP = Collections.unmodifiableMap(methodToCallHandlerTempMap);
    }

    protected final class ASPMediaStoreProviderTransaction implements SQLiteTransactionListener {

        private final ArrayList<Pair<MediaInfo, Object>> mediaInfoTransactions = new ArrayList<Pair<MediaInfo, Object>>();
        private final HashSet<Uri> mPendingNotificationUris = new HashSet<Uri>();
        private final SQLiteDatabase mDb;

        public ASPMediaStoreProviderTransaction(SQLiteDatabase db) {
            this.mDb = db;
        }

        public void beginTransaction() {
            this.mDb.beginTransactionWithListener(this);
        }

        public void setTransactionSuccessful() {
            this.mDb.setTransactionSuccessful();
        }

        public void endTransaction() {
            this.mDb.endTransaction();
        }

        @Override
        public void onBegin() {
            this.mediaInfoTransactions.clear();
            this.mPendingNotificationUris.clear();
        }

        @Override
        public void onCommit() {
            for (Pair<MediaInfo, Object> pair : this.mediaInfoTransactions) {
                pair.first.commitTransaction(ASPMediaStoreProvider.this, this.mDb, pair.second);
            }
            for (Uri uri : this.mPendingNotificationUris) {
                ASPMediaStoreProvider.this.notifySender.notifyChange(uri);
            }
        }

        @Override
        public void onRollback() {
            for (Pair<MediaInfo, Object> pair : this.mediaInfoTransactions) {
                pair.first.failedTransaction(ASPMediaStoreProvider.this, this.mDb, pair.second);
            }
        }

        public Object getTransactionData(MediaInfo mediaInfo) {
            for (Pair<MediaInfo, Object> pair : this.mediaInfoTransactions) {
                if (pair.first == mediaInfo) {
                    return pair.second;
                }
            }

            return null;
        }

        public void setTransactionData(MediaInfo mediaInfo, Object transactionData) {
            Pair<MediaInfo, Object> newPair = new Pair<MediaInfo, Object>(mediaInfo, transactionData);
            for (int i = 0; i < this.mediaInfoTransactions.size(); ++i) {
                Pair<MediaInfo, Object> pair = this.mediaInfoTransactions.get(i);
                if (pair.first == mediaInfo) {
                    this.mediaInfoTransactions.set(i, newPair);
                    return;
                }
            }

            this.mediaInfoTransactions.add(newPair);
        }

        public void putChangeUri(Uri uri) {
            this.mPendingNotificationUris.add(uri);
        }
    }

    public ASPMediaStoreProvider() {
        this.mLock = new TokenBasedReadWriteLock<Long>() {

            private long mLastGeneratedToken = 0L;

            @Override
            protected Long generateNextToken() {
                if (this.mLastGeneratedToken == 0) {
                    this.mLastGeneratedToken = System.currentTimeMillis();
                } else {
                    this.mLastGeneratedToken++;
                }

                return Long.valueOf(this.mLastGeneratedToken);
            }

        };
    }

    /**
     * Initializes the provider by creating a new DatabaseHelper. onCreate() is
     * called automatically when Android creates the provider in response to a
     * resolver request from a client.
     */
    @Override
    public boolean onCreate() {
        if (dbHelper == null) {
            dbHelper = new DatabaseHelper(getContext());
        }
        IASPApplication2 asp = ServiceLocatorSLPF.get(IASPApplication2.class);
        if (asp != null)
            asp.initialize1();
        if (URI_MATCHER == null) {
            ASPMediaStoreProvider.initProviderUri();
        }

        DataModelSLPF.initInstance(this);

        // Listen for locale changes so we can inform interested ASP listeners to update (e.g. MusicListFrag when sorted A-Z must update it's query)
        IntentFilter filter = new IntentFilter(Intent.ACTION_LOCALE_CHANGED);
        Context context = getContext();
        context.registerReceiver(this.localChangeListener, filter);

        return true;
    }

    public BroadcastReceiver localChangeListener = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            Locale locale = Locale.getDefault();
            resetDbLocale(locale);
            ASPMediaStoreProvider.this.notifySender.notifyChange(ASPMediaStore.buildContentUri(""));
        }
    };

    private void resetDbLocale(Locale locale) {
        ASPMediaStoreProvider.dbHelper.getWritableDatabase().setLocale(locale);
    }

    /**
     * This method is called when a client calls {@link android.content.ContentResolver#query(Uri, String[], String, String[], String)} . Queries the database
     * and returns a cursor containing the results.
     *
     * @return A cursor containing the results of the query. The cursor exists
     * but is empty if the query returns no results or an exception
     * occurs.
     * @throws IllegalArgumentException if the incoming URI pattern is invalid.
     */
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        String additionalSelection = null;
        String mediaType;
        String limit = uri.getQueryParameter(CloudGatewayMediaStore.QUERY_STR_LIMIT);
        long entryId = -1;

        int patternId = URI_MATCHER.match(uri);
        List<String> pathSegments = uri.getPathSegments();

        Log.i(this, "query uri = " + uri + ", patternId : " + patternId);
        switch (patternId) {
            case ASP_CONTENT_PATTERN:
                mediaType = pathSegments.get(MEDIA_TYPE_PATH_SEGMENT);
                break;
            case ASP_FILE_CONTENT_PATTERN: {
                long deviceId = CloudGatewayMediaStore.CloudFiles.getDeviceIdFromUri(uri);
                DeviceSLPF device = DataModelSLPF.getInstance().getDeviceById(deviceId);
                mediaType = pathSegments.get(MEDIA_TYPE_PATH_SEGMENT);

                if (device != null) {
                    Cursor c = null;

                    if (mediaType != null) {
                        Long lockToken = this.mLock.readLock();
                        try {
                            SQLiteDatabase db = null;
                            String tableName = null;
                            if (deviceId != 0 && !DeviceUtilSLPF.GOOGLEDRIVE_WEBSTORAGE_NAME.equalsIgnoreCase(device.getWebStorageType())) {
                                CloudStorageSync cloudBase = device.getCloudStorageSync();
                                if (cloudBase != null) {
                                    db = cloudBase.getReadableCloudDatabase();
                                    tableName = ASPMediaStore.Files.PATH;
                                }

                                if (selection == null || !selection.contains("trashed")) {
                                    additionalSelection = "trashed ='0'";
                                    selection = (selection == null) ? additionalSelection : additionalSelection + " AND (" + selection + ")";
                                }
                            } else {
                                db = ASPMediaStoreProvider.dbHelper.getReadableDatabase();
                                tableName = FilesMediaInfo.TABLE_NAME;

                                if (additionalSelection != null) {
                                    selection = (selection == null) ? additionalSelection : additionalSelection + " AND (" + selection + ")";
                                }
                            }

                            if (db != null) {
                                c = db.query(tableName, projection, selection, selectionArgs, null, null, sortOrder, limit);
                                if (c != null) {
                                    c.setNotificationUri(getContext().getContentResolver(), uri);
                                }
                            }
                        } catch (Throwable t) {
                            Log.e(this, "Error in query file pattern for Uri: " + uri.toString() + " : " + t);
                            t.printStackTrace();
                        } finally {
                            this.mLock.readUnlock(lockToken);
                        }
                    }
                    return c;
                }
            }

            case ASP_ENTRY_PATTERN:
                mediaType = pathSegments.get(MEDIA_TYPE_PATH_SEGMENT);
                additionalSelection = ASPMediaStore.BaseASPColumns._ID + "=?";
                String entryIdStr = pathSegments.get(ENTRY_ID_PATH_SEGMENT);
                selectionArgs = ASPMediaStoreProvider.addWhereArg(selectionArgs, entryIdStr);
                entryId = Long.parseLong(entryIdStr);
                break;
            case ASP_DEVICE_CONTENT_PATTERN:
                mediaType = pathSegments.get(MEDIA_TYPE_PATH_SEGMENT);
                additionalSelection = ASPMediaStore.BaseASPColumns.DEVICE_ID + "=?";
                selectionArgs = ASPMediaStoreProvider.addWhereArg(selectionArgs, pathSegments.get(DEVICE_ID_PATH_SEGMENT));
                break;
            case ASP_TEMPORARY_FILES_PATTERN: {
                File f = ASPMediaStore.TemporaryFiles.getFileForUri(uri);
                return new FileCursor(f);
            }
            case ASP_THUMBNAIL_CACHE_PATTERN:
            case ASP_CLOUD_GATEWAY_STREAM_PATTERN:
                return null;

            case ASP_FILE_BROWSER_2_DEFAULT_PATTERN:
            case ASP_FILE_BROWSER_2_PATTERN: {
                String documentId = null;
                if (patternId == ASP_FILE_BROWSER_2_PATTERN) {
                    documentId = CloudGatewayMediaStore.FileBrowser.FileList2.getDirectoryIdFromUri(uri);
                }
                long deviceId = CloudGatewayMediaStore.FileBrowser.FileList2.getDeviceIdFromUri(uri);
                try {
                    return new CachedFileBrowserCursor(getContext(), deviceId, documentId, projection, sortOrder, selection, selectionArgs);
                } catch (InterruptedException e) {
                    return null;
                } catch (IOException e) {
                    return null;
                }
            }

            case ASP_TRASH_PATTERN: {
                long deviceId = CloudGatewayMediaStore.Trash.getDeviceIdFromUri(uri);
                try {
                    return new CachedFileBrowserCursor(getContext(), deviceId, CloudGatewayMediaStore.Trash.Trash_ID, projection, sortOrder, null, null);
                } catch (InterruptedException e) {
                    return null;
                } catch (IOException e) {
                    return null;
                }
            }

            case ASP_STORAGE_PROVIDER_DB_QUERY_PATTERN:
                StorageProviderDatabaseHelper dbHelper = StorageProviderDatabaseHelper.getInstance(this.getContext());
                if (dbHelper != null) {
                    return dbHelper.query(uri, projection, additionalSelection, selectionArgs, sortOrder);
                } else {
                    return null;
                }
            case ASP_READ_LOCK_PATTERN:
                return new ReadLockCursor(this);
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        Cursor c = null;

        if (mediaType != null) {
            MediaInfo mediaInfo = MEDIA_TYPE_TO_INFO_MAP.get(mediaType);

            Long lockToken = mLock.readLock();
            try {
                if (selection == null) {
                    selection = additionalSelection;
                } else if (additionalSelection != null) {
                    selection = "(" + selection + ") AND " + additionalSelection;
                }

                SQLiteDatabase db = ASPMediaStoreProvider.dbHelper.getReadableDatabase();
                MediaInfoContext mediaInfoContext = new MediaInfoContext(this, db, uri, patternId, mLock);

                c = mediaInfo.handleQuery(mediaInfoContext, projection, selection, selectionArgs, sortOrder, null, null, limit, entryId);

                // Tells the Cursor what URI to watch, so it knows when its source data changes
                if (c != null) {
                    c.setNotificationUri(getContext().getContentResolver(), uri);
                }
            } catch (Throwable t) {
                Log.e(this, "Error in query for Uri: " + uri.toString() + " : " + t);
                t.printStackTrace();
            } finally {
                mLock.readUnlock(lockToken);
            }
        }

        return c;
    }

    /**
     * This is called when a client calls {@link android.content.ContentResolver#getType(Uri)}. Returns the MIME
     * data type of the URI given as a parameter.
     *
     * @param uri The URI whose MIME type is desired.
     * @return The MIME type of the URI.
     * @throws IllegalArgumentException if the incoming URI pattern is invalid.
     */
    @Override
    public String getType(Uri uri) {
        Log.d(this, "getType for uri(" + uri + ")");

        String mediaType;
        boolean isEntry = false;

        switch (URI_MATCHER.match(uri)) {
            case ASP_CONTENT_PATTERN:
            case ASP_DEVICE_CONTENT_PATTERN:
                mediaType = uri.getPathSegments().get(MEDIA_TYPE_PATH_SEGMENT);
                break;
            case ASP_ENTRY_PATTERN:
                mediaType = uri.getPathSegments().get(MEDIA_TYPE_PATH_SEGMENT);
                isEntry = true;
                break;
            case ASP_THUMBNAIL_CACHE_PATTERN:
            case ASP_TEMPORARY_FILES_PATTERN: {
                File f = ASPMediaStore.TemporaryFiles.getFileForUri(uri);
//				String mimeType = FileTypeHelper.getMimeTypeForFile(f.getName(), null);
//				return mimeType;
                return FileTypeHelper.getMimeTypeForFile(f.getName(), null); //P151119-02266
            }
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        MediaInfo mediaInfo = MEDIA_TYPE_TO_INFO_MAP.get(mediaType);
//        checkAccess(mediaInfo, false);

        if (isEntry) {
            return mediaInfo.getEntryContentType();
        } else {
            return mediaInfo.getContentType();
        }
    }

    /**
     * Called by a client to determine the types of data streams that this
     * content provider supports for the given URI. The default implementation
     * returns null, meaning no types. If your content provider stores data
     * of a particular type, return that MIME type if it matches the given
     * mimeTypeFilter. If it can perform type conversions, return an array
     * of all supported MIME types that match mimeTypeFilter.
     *
     * @param uri            The data in the content provider being queried.
     * @param mimeTypeFilter The type of data the client desires. May be
     *                       a pattern, such as *\/* to retrieve all possible data types.
     * @return Returns null if there are no possible data streams for the
     * given mimeTypeFilter. Otherwise returns an array of all available
     * concrete MIME types.
     * @see #getType(Uri)
     * @see #openTypedAssetFile(Uri, String, Bundle)
     * @see ClipDescription#compareMimeTypes(String, String)
     */
    @Override
    public String[] getStreamTypes(Uri uri, String mimeTypeFilter) {
        Log.d(this, "getStreamTypes for uri : " + uri + ", mimeTypeFilter : " + mimeTypeFilter);

        String[] contentTypes = null;

        switch (URI_MATCHER.match(uri)) {
            case ASP_ENTRY_PATTERN: {
                String mediaType = uri.getPathSegments().get(MEDIA_TYPE_PATH_SEGMENT);
                MediaInfo mediaInfo = MEDIA_TYPE_TO_INFO_MAP.get(mediaType);
                if (mediaInfo != null) {
//                    checkAccess(mediaInfo, false);
                    SQLiteDatabase db = ASPMediaStoreProvider.dbHelper.getReadableDatabase();
                    Long lockToken = this.mLock.readLock();
                    try {
                        MediaInfoContext mediaInfoContext = new MediaInfoContext(this, db, uri, ASP_ENTRY_PATTERN, this.mLock);
                        contentTypes = mediaInfo.getStreamContentType(mediaInfoContext);
                    } finally {
                        this.mLock.readUnlock(lockToken);
                    }

                }
                break;
            }
            case ASP_THUMBNAIL_CACHE_PATTERN:
            case ASP_TEMPORARY_FILES_PATTERN: {
                String mimeType = getType(uri);
                if (mimeType != null) {
                    contentTypes = new String[]{mimeType};
                }
                break;
            }
        }

        if (contentTypes != null) {
            List<String> foundTypes = new ArrayList<String>();
            for (String contentType : contentTypes) {
                if (ClipDescription.compareMimeTypes(contentType, mimeTypeFilter)) {
                    foundTypes.add(contentType);
                }
            }
            return foundTypes.toArray(new String[foundTypes.size()]);
        }

        return null;
    }

    @Override
    public ParcelFileDescriptor openFile(Uri uri, String mode) throws FileNotFoundException {
        Log.d(this, "openFile for uri : " + uri);

        ParcelFileDescriptor result = null;

        if (!"r".equals(mode)) {
            throw new IllegalArgumentException("Content is read-only.");
        }

        switch (URI_MATCHER.match(uri)) {
            case ASP_ENTRY_PATTERN: {
                String mediaType = uri.getPathSegments().get(MEDIA_TYPE_PATH_SEGMENT);
                MediaInfo mediaInfo = MEDIA_TYPE_TO_INFO_MAP.get(mediaType);
                if (mediaInfo == null) {
                    mediaInfo = FilesMediaInfo.getInstance();
//                    throw new IllegalArgumentException("Unknown URI " + uri);
                }

                long entryId = Long.parseLong(uri.getPathSegments().get(ENTRY_ID_PATH_SEGMENT));

                SQLiteDatabase db = ASPMediaStoreProvider.dbHelper.getReadableDatabase();
                try {
                    result = mediaInfo.openFile(new MediaInfoContext(this, db, uri, ASP_ENTRY_PATTERN, this.mLock), entryId);
                } catch (InterruptedException e) {
                    Thread.interrupted();
                    //throw new FileNotFoundException("Interrupted while attempting to openFile for uri " + uri);
                } catch (Exception e) {
                    ////no fc please...
                    e.printStackTrace();
                }
                break;
            }
            case ASP_TEMPORARY_FILES_PATTERN: {
                File f = ASPMediaStore.TemporaryFiles.getFileForUri(uri);
                return ParcelFileDescriptor.open(f, ParcelFileDescriptor.MODE_READ_ONLY);
            }
            case ASP_DEVICE_ENTRY_PATTERN: {
                List<String> pathSegments = uri.getPathSegments();
                String mediaType = pathSegments.get(MEDIA_TYPE_PATH_SEGMENT);
                MediaInfo mediaInfo = MEDIA_TYPE_TO_INFO_MAP.get(mediaType);
                if (mediaInfo == null) {
                    mediaInfo = FilesMediaInfo.getInstance();
                }

                long deviceId = Long.parseLong(pathSegments.get(DEVICE_ID_PATH_SEGMENT));
                DeviceSLPF device = DataModelSLPF.getInstance().getDeviceById(deviceId);

                long entryId = Long.parseLong(pathSegments.get(4));

                SQLiteDatabase db = ASPMediaStoreProvider.dbHelper.getReadableDatabase();
                if (device != null) {
                    if (deviceId != 0 && !DeviceUtilSLPF.GOOGLEDRIVE_WEBSTORAGE_NAME.equals(device.getWebStorageType())) {
                        CloudStorageSync cloudStorageBase = device.getCloudStorageSync();
                        db = cloudStorageBase.getReadableCloudDatabase();
                    }
                } else {
                    Log.e(this, "DeviceSLPF device =" + device);
                }

                try {
                    result = mediaInfo.openFile(new MediaInfoContext(this, db, uri, ASP_DEVICE_ENTRY_PATTERN, this.mLock), entryId);
                } catch (InterruptedException e) {
                    Thread.interrupted();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
            }
            case ASP_THUMBNAIL_CACHE_PATTERN: {
                AspThumbnailCache cache = AspThumbnailCache.getInstance(getContext());
                File f = new File(cache.getCacheDir(), uri.getLastPathSegment());
                return ParcelFileDescriptor.open(f, ParcelFileDescriptor.MODE_READ_ONLY);
            }
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        return result;
    }


    protected Long readLock() {
        return this.mLock.readLock();
    }

    protected boolean readUnlock(Long lockToken) {
        return this.mLock.readUnlock(lockToken);
    }

    @Override
    public Bundle call(String method, String arg, Bundle extras) {
        Log.d(this, "call for method : " + method + ", arg : " + arg);

        PublicApiEntry.enter();
        try {
            CallHandler callHandler = METHOD_TO_CALL_HANDLER_MAP.get(method);
            if (callHandler != null) {
                return callHandler.call(getContext(), method, arg, extras);
            }

            //TODO move the rest of these into CallHandlers
            Bundle result = null;

            if (ASPMediaStore.CallMethods.GetSignInStatus.NAME.equals(method)) {
                result = new Bundle();
                result.putBoolean(ASPMediaStore.CallMethods.KEY_RESULT, true);
            } else if (CloudGatewayMediaStore.CallMethods.Is3boxSupported.NAME.equals(method)) {
                result = new Bundle();
                result.putBoolean(ASPMediaStore.CallMethods.KEY_RESULT, false);
            } else if (ASPMediaStore.CallMethods.IsInitialized.NAME.equals(method)) {
                result = new Bundle();
                boolean status = true;
                result.putBoolean(ASPMediaStore.CallMethods.KEY_RESULT, status);
            } else if (ASPMediaStore.CallMethods.SignOutOfStorageService.NAME.equals(method)) {
                int deviceId = Integer.parseInt(arg);
                DataModelSLPF dataModel = DataModelSLPF.getInstance();
                DeviceSLPF device = dataModel.getDeviceById(deviceId);

                StorageSignInOutHelper signOutHelper = new StorageSignInOutHelper(getContext());
                signOutHelper.signOutOfStorageService(device);
            } else if (CloudGatewayMediaStore.CallMethods.GetInitialSyncDoneDevices.NAME.equals(method)) {
                result = new Bundle();
                result.putIntegerArrayList(ASPMediaStore.CallMethods.KEY_RESULT, getInitialSyncDoneDevices());
            } else if (CloudGatewayMediaStore.CallMethods.SetWifiOnly.NAME.equals(method)) {
                try {
                    result = new Bundle();
                    if (extras != null) {
                        boolean bEnable = extras.getBoolean(ASPMediaStore.CallMethods.SetWifiOnly.INTENT_ARG_ENABLE_BOOLEAN, true);
                        Log.i(this, "1 SetWifiOnly " + bEnable);
                        SharedPreferences pref = getContext().getSharedPreferences(IASPApplication2.PREFERENCES_NAME, Activity.MODE_PRIVATE);
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putBoolean(CloudGatewayMediaStore.CallMethods.SetWifiOnly.RET_ENABLE_BOOLEAN, bEnable);
                        editor.apply();
                    } else {
                        SharedPreferences prefs = ServiceLocatorSLPF.get(IASPApplication2.class).getSharedPreferences(
                                IASPApplication2.PREFERENCES_NAME,
                                Activity.MODE_PRIVATE);
                        boolean bRet = prefs.getBoolean(CloudGatewayMediaStore.CallMethods.SetWifiOnly.RET_ENABLE_BOOLEAN, true);
                        Log.i(this, "2 GetWifiOnly " + bRet);
                        result.putBoolean(CloudGatewayMediaStore.CallMethods.SetWifiOnly.RET_ENABLE_BOOLEAN, bRet);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (CloudGatewayMediaStore.CallMethods.KeepServiceAlive.NAME.equals(method)) {
                boolean bEnable = extras.getBoolean(ASPMediaStore.CallMethods.KeepServiceAlive.INTENT_ARG_ENABLE_BOOLEAN, false);
//                String callPackage = extras.getString(ASPMediaStore.CallMethods.KeepServiceAlive.INTENT_ARG_PKGNAME);
//				if(callPackage != null && callPackage.indexOf(".myfiles")>=0) {
//					if(bEnable==true)
//						IASPApplication2.sUseDataNetworkTmp = true;
//					else
//						IASPApplication2.sUseDataNetworkTmp = false;
//				}

                IASPApplication2.sKeepAliveServiceFromNativeApp = bEnable;
                if (!IASPApplication2.sInstanceExist && bEnable) {
                    getContext().startService(new Intent(getContext(), CloudManagerMainService.class));
                }

                Intent stopIntent = new Intent(bEnable ? CloudStorageSync.CLOUD_APP_STARTED : CloudStorageSync.CLOUD_APP_STOPPED);
                LocalBroadcastManager.getInstance(getContext()).sendBroadcast(stopIntent);

                result = new Bundle();
                result.putBoolean("RESULT", true);
            } else if (CloudGatewayMediaStore.CallMethods.CancelFileTransfer.NAME.equals(method)) {
                boolean bRet = false;
                if (extras != null) {
                    String sessionId = extras.getString(CloudGatewayMediaStore.CallMethods.CancelFileTransfer.INTENT_ARG_SESSION_ID);

                    if ((sessionId != null) && FileTransferManagerSingleton.getInstance(getContext()).checkCancelable(sessionId)) {
                        bRet = true;
                        final String fSessionId = sessionId;
                        Intent cancelIntent = new Intent(FileTransferManager.CANCEL);
                        cancelIntent.putExtra("sessionId", fSessionId);
                        LocalBroadcastManager.getInstance(getContext()).sendBroadcast(cancelIntent);

                        Log.d(this, "call CloudGatewayMediaStore.CallMethods.CancelFileTransfer.NAME : " + sessionId);
                    }
                }
                result = new Bundle();
                result.putBoolean(CloudGatewayMediaStore.CallMethods.KEY_RESULT, bRet);
            } else {
                result = super.call(method, arg, extras);
            }

            return result;
        } finally {
            PublicApiEntry.leave();
        }
    }

    private ArrayList<Integer> getInitialSyncDoneDevices() {
        Log.d(this, "getInitialSyncDoneDevices()");

        ArrayList<Integer> results = new ArrayList<Integer>();
        String[] fromColumns = {CloudGatewayMediaStore.DeviceColumns._ID};
        String where = /*CloudGatewayMediaStore.DeviceColumns.TRANSPORT_TYPE + " == ?" + " OR (" +*/ CloudGatewayMediaStore.DeviceColumns.TRANSPORT_TYPE + " == ?";
        String[] whereArgs = new String[]{/*CloudGatewayDeviceTransportType.SLINK.name(),*/ CloudGatewayDeviceTransportType.WEB_STORAGE.name()};
        SQLiteDatabase db = ASPMediaStoreProvider.dbHelper.getReadableDatabase();

        Cursor cursor = db.query(DeviceMediaInfo.TABLE_NAME, fromColumns, where, whereArgs, null, null, null, null);

        if (cursor != null) {
            SharedPreferences prefs = ServiceLocatorSLPF.get(IASPApplication2.class).getSharedPreferences(
                    IASPApplication2.PREFERENCES_NAME,
                    Activity.MODE_PRIVATE);
            while (cursor.moveToNext()) {
                int deviceId = cursor.getInt(0);
                StringBuilder initialSyncComNotPrefKey = new StringBuilder();

                initialSyncComNotPrefKey.append(AspCommonUtils.INITIAL_SYNC_COMPLETE_NOTIFIED_PREF_KEY).append("_DEVICE_ID_").append(deviceId);

                if (prefs.getBoolean(initialSyncComNotPrefKey.toString(), false)) {
                    //logger.trace("::getInitialSyncDoneDevices() initialSyncComNotPrefKey:{} is true", initialSyncComNotPrefKey);
                    results.add(Integer.valueOf(deviceId));
                }
            }
            cursor.close();
        }
        Log.d(this, "getInitialSyncDoneDevices() results:" + results.toString());

        return results;
    }

    @Override
    public ContentProviderResult[] applyBatch(ArrayList<ContentProviderOperation> operations) throws OperationApplicationException {
        if ((operations == null) || operations.isEmpty()) {
            return null;
        }
        boolean hasWrite = false;
        int size = operations.size();
        for (int i = 0; i < size; ++i) {
            if (operations.get(i).isWriteOperation()) {
                hasWrite = true;
                break;
            }
        }

        ContentProviderResult[] result = new ContentProviderResult[size];

        final SQLiteDatabase db;
        final Long lockToken;
        if (hasWrite) {
            db = ASPMediaStoreProvider.dbHelper.getWritableDatabase();
            lockToken = mLock.writeLock();
            mCurrTransaction = new ASPMediaStoreProviderTransaction(db);
        } else {
//            db = ASPMediaStoreProvider.dbHelper.getReadableDatabase();
            lockToken = mLock.readLock();
        }
        try {
            if (hasWrite) {
                mCurrTransaction.beginTransaction();
            }
            int i = 0;
            try {
                for (; i < size; ++i) {
                    ContentProviderOperation operation = operations.get(i);
                    result[i] = operation.apply(this, result, i);
                }
                if (hasWrite) {
                    mCurrTransaction.setTransactionSuccessful();
                }
            } catch (OperationApplicationException oae) {
                ContentProviderOperation failed = operations.get(i);
                Log.e(this, "Error applying batch. Failed : " + failed + " - " + oae);
                throw oae;
            } finally {
                if (hasWrite) {
                    mCurrTransaction.endTransaction();
                    mCurrTransaction = null;
                }
            }
        } finally {
            if (hasWrite) {
                mLock.writeUnlock(lockToken);
            } else {
                mLock.readUnlock(lockToken);
            }
        }

        return result;
    }

    /**
     * This is called when a client calls {@link android.content.ContentResolver#insert(Uri, ContentValues)}.
     * Inserts a new row into the database. This method sets up default values
     * for any columns that are not included in the incoming map. If rows were
     * inserted, then listeners are notified of the change.
     *
     * @return The row ID of the inserted row.
     * @throws SQLException if the insertion fails.
     */
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        Log.d(this, "insert for uri : " + uri);
        String tableName = (uri.toString().endsWith("device")) ? DeviceMediaInfo.TABLE_NAME : FilesMediaInfo.TABLE_NAME;

        long rowId = 0;
        Long lockToken = mLock.writeLock();
        try {
            // Opens the database object in "write" mode.
            SQLiteDatabase db = ASPMediaStoreProvider.dbHelper.getWritableDatabase();

            // Performs the insert and returns the ID of the new media.
            rowId = db.insert(tableName, null, values);
            Log.d(this, "insert " + rowId);
            if (rowId <= 0) {
                throw new Exception("Row ID is <= 0! " + rowId);
            }
        } catch (Exception e) {
            Log.d(this, "Insert failed for uri: " + uri + " values: " + values + ", Exception : " + e);
            e.printStackTrace();
        } finally {
            mLock.writeUnlock(lockToken);
        }

        // If the insert succeeded, the row ID exists.
        if (rowId > 0) {
            Uri newUri = (uri.toString().endsWith("device")) ? DeviceMediaInfo.buildEntryIdUri(rowId, ASPMediaStore.Device.PATH) : FilesMediaInfo.buildEntryIdUri(rowId, ASPMediaStore.Files.PATH);
            // Notifies observers registered against this provider that the data changed.
            //tmp notifyUriChange(null, mediaInfo, deviceIds);
            //notifyUriChange(newUri, null, null);  // ---->

            return newUri;
        }

        return null; //if it did not succeed, we will just return null;
    }

    /**
     * This is called when a client calls {@link android.content.ContentResolver#delete(Uri, String, String[])}.
     * Deletes records from the database. If the incoming URI matches the media
     * ID URI pattern, this method deletes the one record specified by the ID in
     * the URI. Otherwise, it deletes a a set of records. The record or records
     * must also match the input selection criteria specified by where and
     * whereArgs.
     * If rows were deleted, then listeners are notified of the change.
     *
     * @return If a "where" clause is used, the number of rows affected is
     * returned, otherwise 0 is returned. To delete all rows and get a
     * row count, use "1" as the where clause.
     * @throws IllegalArgumentException if the incoming URI pattern is invalid.
     */
    @Override
    public int delete(Uri uri, String where, String[] whereArgs) {
        Log.d(this, "delete for uri : " + uri);

        String mediaType;
        StringBuffer additionalWhere = null;
        boolean isCleanup = false;
        int deviceId = -1;
        ArrayList<String> deviceIds = new ArrayList<String>();
        int patternId = URI_MATCHER.match(uri);
        switch (patternId) {
            case ASP_CONTENT_PATTERN:
                mediaType = uri.getPathSegments().get(MEDIA_TYPE_PATH_SEGMENT);
                break;
            case ASP_ENTRY_PATTERN:
                mediaType = uri.getPathSegments().get(MEDIA_TYPE_PATH_SEGMENT);
                additionalWhere = new StringBuffer(ASPMediaStore.BaseASPColumns._ID + "=?");
                String entryId = uri.getPathSegments().get(ENTRY_ID_PATH_SEGMENT);
                whereArgs = ASPMediaStoreProvider.addWhereArg(whereArgs, entryId);
                if (mediaType.equals(ASPMediaStore.Device.PATH)) {
                    deviceIds.add(entryId);
                }
                break;
            case ASP_DEVICE_ORPHAN_CLEANUP_PATTERN:
                isCleanup = true;
                mediaType = uri.getPathSegments().get(MEDIA_TYPE_PATH_SEGMENT);
                break;
            case ASP_DEVICE_CONTENT_PATTERN:
                mediaType = uri.getPathSegments().get(MEDIA_TYPE_PATH_SEGMENT);
                additionalWhere = new StringBuffer(ASPMediaStore.BaseASPColumns.DEVICE_ID + "=?");
                deviceId = Integer.parseInt(uri.getPathSegments().get(DEVICE_ID_PATH_SEGMENT));
                deviceIds.add(Integer.toString(deviceId));
                whereArgs = ASPMediaStoreProvider.addWhereArg(whereArgs, uri.getPathSegments().get(DEVICE_ID_PATH_SEGMENT));
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        MediaInfo mediaInfo = MEDIA_TYPE_TO_INFO_MAP.get(mediaType);

        if (!isCleanup) {
            String mediaTypeFilter = mediaInfo.getFilteredMediaTypeArg();
            if (mediaTypeFilter != null) {
                if (additionalWhere != null) {
                    additionalWhere.append(" AND " + ASPMediaStore.MediaColumns.MEDIA_TYPE + "=?");
                } else {
                    additionalWhere = new StringBuffer(ASPMediaStore.MediaColumns.MEDIA_TYPE + "=?");
                }

                whereArgs = ASPMediaStoreProvider.addWhereArg(whereArgs, mediaTypeFilter);
            }
        }

        if (where == null) {
            where = (additionalWhere != null ? additionalWhere.toString() : null);
        } else if (additionalWhere != null) {
            where = "(" + where + ") AND " + additionalWhere;
        }

        int count;
        Long lockToken = this.mLock.writeLock();
        try {
            SQLiteDatabase db = ASPMediaStoreProvider.dbHelper.getWritableDatabase();

            MediaInfoContext mediaInfoContext = new MediaInfoContext(this, db, uri, patternId, this.mLock);

            if (isCleanup) {
                count = mediaInfo.handleOrphanCleanup(mediaInfoContext, where, whereArgs, deviceId);
            } else {
                if (deviceIds.size() == 0) {
                    mediaInfo.getAffectedDeviceIds(mediaInfoContext, where, whereArgs, deviceIds);
                }
                count = mediaInfo.handleDelete(mediaInfoContext, where, whereArgs, true);
            }

        } finally {
            this.mLock.writeUnlock(lockToken);
        }

		/*
         * Gets a handle to the content resolver object for the current context,
		 * and notifies it that the incoming URI changed. The object passes this
		 * along to the resolver framework, and observers that have registered
		 * themselves for the provider are notified.
		 */
        if (count > 0 && !isCleanup) {

            notifyUriChange(null, mediaInfo, deviceIds);

            if (deviceIds.isEmpty() == false) {
                notifyUriChange(ASPMediaStore.buildContentUri(""), null, null);
            }
        }

        // Returns the number of rows deleted.
        return count;
    }

    private static String[] addWhereArg(String[] origWhereArgs, String whereArg) {
        if (whereArg == null) {
            return origWhereArgs;
        }

        String[] result = new String[origWhereArgs == null ? 1 : origWhereArgs.length + 1];
        if (origWhereArgs != null) {
            System.arraycopy(origWhereArgs, 0, result, 0, origWhereArgs.length);
        }
        result[result.length - 1] = whereArg;

        return result;
    }

    /**
     * This is called when a client calls {@link android.content.ContentResolver#update(Uri, ContentValues, String, String[])} Updates records in the database. The
     * column names specified by the keys
     * in the values map are updated with new data specified by the values in
     * the map. If the incoming URI matches the media ID URI pattern, then the
     * method updates the one record specified by the ID in the URI; otherwise,
     * it updates a set of records. The record or records must match the input
     * selection criteria specified by where and whereArgs. If rows were
     * updated, then listeners are notified of the change.
     *
     * @param uri       The URI pattern to match and update.
     * @param values    A map of column names (keys) and new values (values).
     * @param where     An SQL "WHERE" clause that selects records based on their
     *                  column values. If this is null, then all records that match
     *                  the URI pattern are selected.
     * @param whereArgs An array of selection criteria. If the "where" param contains
     *                  value placeholders ("?"), then each placeholder is replaced by
     *                  the corresponding element in the array.
     * @return The number of rows updated.
     * @throws IllegalArgumentException if the incoming URI pattern is invalid.
     */
    @Override
    public int update(Uri uri, ContentValues values, String where, String[] whereArgs) {
        Log.d(this, "update for uri : " + uri);

        String mediaType;
        StringBuffer additionalWhere = null;
        long recordId = 0;
        ArrayList<String> deviceIds = new ArrayList<String>();
        int patternId = URI_MATCHER.match(uri);
        switch (patternId) {
            case ASP_CONTENT_PATTERN:
                mediaType = uri.getPathSegments().get(MEDIA_TYPE_PATH_SEGMENT);
                break;
            case ASP_ENTRY_PATTERN:
                mediaType = uri.getPathSegments().get(MEDIA_TYPE_PATH_SEGMENT);
                additionalWhere = new StringBuffer(ASPMediaStore.BaseASPColumns._ID + "=?");
                recordId = Long.parseLong(uri.getPathSegments().get(ENTRY_ID_PATH_SEGMENT));
                whereArgs = ASPMediaStoreProvider.addWhereArg(whereArgs, Long.toString(recordId));
                break;
            case ASP_DEVICE_CONTENT_PATTERN:
                mediaType = uri.getPathSegments().get(MEDIA_TYPE_PATH_SEGMENT);
                additionalWhere = new StringBuffer(ASPMediaStore.BaseASPColumns.DEVICE_ID + "=?");

                String deviceId = uri.getPathSegments().get(DEVICE_ID_PATH_SEGMENT);
                deviceIds.add(deviceId);
                whereArgs = ASPMediaStoreProvider.addWhereArg(whereArgs, deviceId);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        MediaInfo mediaInfo = MEDIA_TYPE_TO_INFO_MAP.get(mediaType);
        String mediaTypeFilter = mediaInfo.getFilteredMediaTypeArg();
        if (mediaTypeFilter != null) {
            if (additionalWhere != null) {
                additionalWhere.append(" AND " + ASPMediaStore.MediaColumns.MEDIA_TYPE + "=?");
            } else {
                additionalWhere = new StringBuffer(ASPMediaStore.MediaColumns.MEDIA_TYPE + "=?");
            }

            whereArgs = ASPMediaStoreProvider.addWhereArg(whereArgs, mediaTypeFilter);
        }

        if (where == null) {
            where = (additionalWhere != null ? additionalWhere.toString() : null);
        } else if (additionalWhere != null) {
            where = "(" + where + ") AND " + additionalWhere;
        }

        int count = 0;
//        checkAccess(mediaInfo, true);
        Long lockToken = this.mLock.writeLock();
        try {
            SQLiteDatabase db = ASPMediaStoreProvider.dbHelper.getWritableDatabase();
            MediaInfoContext mediaInfoContext = new MediaInfoContext(this, db, uri, patternId, this.mLock);
            count = mediaInfo.handleUpdate(mediaInfoContext, values, where, whereArgs, recordId, values);
            if (deviceIds.isEmpty()) {
                String deviceId = values.getAsString(ASPMediaStore.BaseASPColumns.DEVICE_ID);
                if (deviceId != null) {
                    deviceIds.add(deviceId);
                } else {
                    mediaInfo.getAffectedDeviceIds(mediaInfoContext, where, whereArgs, deviceIds);
                }
            }

        } finally {
            this.mLock.writeUnlock(lockToken);
        }

		/*
         * Gets a handle to the content resolver object for the current context,
		 * and notifies it that the incoming URI changed. The object passes this
		 * along to the resolver framework, and observers that have registered
		 * themselves for the provider are notified.
		 */
        if (count > 0) {
            notifyUriChange(uri, mediaInfo, deviceIds);
        }

        // Returns the number of rows deleted.
        return count;
    }

    private void notifyUriChange(Uri uri, MediaInfo mediaInfo, Collection<String> deviceIds) {
        if (uri != null) {
            notifyUriChangeInner(uri);
        }
        if (mediaInfo != null) {
            Uri temp = mediaInfo.getContentUri();
            if (uri == null || !uri.toString().startsWith(temp.toString())) {
                notifyUriChangeInner(temp);
            }
            if (deviceIds != null) {
                for (String stringId : deviceIds) {
                    long deviceId = Long.parseLong(stringId);
                    temp = mediaInfo.getContentUriForDevice(deviceId);
                    notifyUriChangeInner(temp);
                }
            }
        }
    }

    private void notifyUriChangeInner(Uri uri) {
        if (mCurrTransaction != null) {
            mCurrTransaction.putChangeUri(uri);
        } else {
            notifySender.notifyChange(uri);
        }
    }

    @Override
    public void shutdown() {
        Long lockToken = this.mLock.writeLock();
        try {
            ASPMediaStoreProvider.dbHelper.close();
        } finally {
            this.mLock.writeUnlock(lockToken);
        }
    }

    private class DatabaseHelper extends SQLiteOpenHelper {

        private class DependencyGraph {

            private final HashMap<String, Pair<String[], String>> mTableGraph = new HashMap<String, Pair<String[], String>>();
            private final HashMap<String, Pair<String[], String>> mViewGraph = new HashMap<String, Pair<String[], String>>();

            public DependencyGraph() {
                final String[] nil = new String[0];

                addToTableGraph(DeviceMediaInfo.TABLE_NAME, nil, createDevicesTable());
                addToTableGraph(FilesMediaInfo.TABLE_NAME, nil, createFilesTable());

//                addToViewGraph(FilesMediaInfo.VIEW_NAME, nil, createFilesDevicesView());
            }

            private void addToTableGraph(String key, String[] value, String tableCreationSql) {
                if (mTableGraph.containsKey(key)) {
                    throw new IllegalStateException("Key: " + key + " already exists in dependency graph. Check your code.");
                }
                if (ArrayUtils.contains(value, key)) {
                    throw new IllegalArgumentException("Circular dependency. Key: " + key + " cannot depend on itself. Check your code.");
                }
                mTableGraph.put(key, new Pair<String[], String>(value, tableCreationSql));
            }

            private void addToViewGraph(String key, String[] value, String viewCreationSql) {
                if (mViewGraph.containsKey(key)) {
                    throw new IllegalStateException("Key: " + key + " already exists in dependency graph. Check your code.");
                }
                if (ArrayUtils.contains(value, key)) {
                    throw new IllegalArgumentException("Circular dependency. Key: " + key + " cannot depend on itself. Check your code.");
                }
                mViewGraph.put(key, new Pair<String[], String>(value, viewCreationSql));
            }

            public void markTableChanged(String tableName, ArrayList<String> viewChangeList) {
                Pair<String[], String> dependants = mTableGraph.get(tableName);
                if (dependants != null) {
                    for (int i = 0; i < dependants.first.length; ++i) {
                        markViewChanged(dependants.first[i], viewChangeList);
                    }
                } else {
                    throw new IllegalArgumentException("Table: " + tableName + " not found in table graph.");
                }
            }

            public int markViewChanged(String viewName, ArrayList<String> viewChangeList) {
                int pos = viewChangeList.indexOf(viewName);
                if (pos < 0) {
                    pos = Integer.MAX_VALUE;

                    Pair<String[], String> dependants = mViewGraph.get(viewName);
                    if (dependants != null) {
                        for (int i = 0; i < dependants.first.length; ++i) {
                            int tempPos = markViewChanged(dependants.first[i], viewChangeList);
                            pos = Math.min(pos, tempPos);
                        }
                    } else {
                        throw new IllegalArgumentException("View: " + viewName + " not find in view graph.");
                    }

                    pos = Math.min(viewChangeList.size(), pos);

                    viewChangeList.add(pos, viewName);
                }

                return pos;
            }

            public ArrayList<String> getAllViewsDependencyList() {
                ArrayList<String> result = new ArrayList<String>(this.mViewGraph.size());

                for (String name : this.mViewGraph.keySet()) {
                    markViewChanged(name, result);
                }

                return result;
            }

            public void createAllTables(SQLiteDatabase db) {

                for (String name : this.mTableGraph.keySet()) {
                    Pair<String[], String> data = this.mTableGraph.get(name);

                    logAndExecCmd(db, data.second);
                }
            }

            public void createTable(SQLiteDatabase db, String tableName) {
                Pair<String[], String> data = this.mTableGraph.get(tableName);
                logAndExecCmd(db, data.second);
            }

            public void dropAllTables(SQLiteDatabase db) {
                for (String name : mTableGraph.keySet()) {
                    dropTable(db, name);
                }
            }

            public void dropAllViewsInDependencyList(SQLiteDatabase db, ArrayList<String> dependancyList) throws SQLiteException {
                for (int i = dependancyList.size() - 1; i >= 0; --i) {
                    dropView(db, dependancyList.get(i));
                }
            }

            public void createAllViewInDependencyList(SQLiteDatabase db, ArrayList<String> dependancyList) throws SQLiteException {
                for (int i = 0; i < dependancyList.size(); ++i) {
                    Pair<String[], String> data = this.mViewGraph.get(dependancyList.get(i));
                    if (data != null) {
                        logAndExecCmd(db, data.second);
                    }
                }
            }
        }

        private static final int DATABASE_VERSION = 30018;

        private WeakReference<DependencyGraph> mDependencyGraph;

        public DatabaseHelper(Context context) {
            super(context, "asp.db", null, DATABASE_VERSION);
        }

        private DependencyGraph getDependencyGraph() {
            DependencyGraph result = null;
            if (this.mDependencyGraph != null) {
                result = this.mDependencyGraph.get();
            }
            if (result == null) {
                result = new DependencyGraph();
                this.mDependencyGraph = new WeakReference<>(result);
            }

            return result;
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            DependencyGraph graph = getDependencyGraph();

            graph.createAllTables(db);

            logAndExecCmd(db, createSingleFieldIndex(FilesMediaInfo.DEVICE_ID_INDEX, FilesMediaInfo.TABLE_NAME, ASPMediaStore.MediaColumns.DEVICE_ID));
            logAndExecCmd(db, createSingleFieldIndex(FilesMediaInfo.SOURCE_MEDIA_INDEX, FilesMediaInfo.TABLE_NAME, ASPMediaStore.MediaColumns.SOURCE_MEDIA_ID));
            logAndExecCmd(db, createSingleFieldIndex(FilesMediaInfo.MIME_TYPE_INDEX, FilesMediaInfo.TABLE_NAME, ASPMediaStore.MediaColumns.MIME_TYPE));

//            logAndExecCmd(db, createFileDeleteTrigger());
            logAndExecCmd(db, createDeviceDeleteTrigger());
//            logAndExecCmd(db, createKeywordDeleteTrigger());

            ArrayList<String> allViews = graph.getAllViewsDependencyList();

            graph.createAllViewInDependencyList(db, allViews);

            AspThumbnailCache.getInstance(getContext()).clearCache();

            ////reducing BRdcast
            //getContext().sendBroadcast(new Intent(CloudGatewayMediaStore.ACTION_VERSION_CHANGED_BROADCAST));
        }

        @Override
        public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            // Logs that the database is being upgraded
            Log.d(this, "Downgrading database from version " + oldVersion + " to " + newVersion + ", which will destroy all old data");
            DependencyGraph graph = getDependencyGraph();

            ArrayList<String> viewList = graph.getAllViewsDependencyList();

            //starting with version 54, the 4 main media tables
            //(IMAGES, VIDEO, AUDIO, and DOCUMENTS) are now merged into
            //the new FILES table. So after 54, we will drop views which
            //took the place of the old tables.
//            dropTableOrView(db, ImageMediaInfo.VIEW_NAME);
//            dropTableOrView(db, VideoMediaInfo.VIEW_NAME);
//            dropTableOrView(db, AudioMediaInfo.VIEW_NAME);
//            dropTableOrView(db, DocumentMediaInfo.VIEW_NAME);

            graph.dropAllViewsInDependencyList(db, viewList);
            graph.dropAllTables(db);

            // Recreates the database with a new version
            onCreate(db);

            db.setVersion(DATABASE_VERSION);
        }

        private void dropTableOrView(SQLiteDatabase db, String name) throws SQLiteException {
            try {
                dropView(db, name);
            } catch (SQLiteException exception) {
                dropTable(db, name);
            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.d(this, "onUpgrade() - oldVersion : " + oldVersion + ", newVersion : " + newVersion);
//            Log.d(this, "createFilesDevicesView() : " + createFilesDevicesView());
            if (newVersion == 1) {
                return;
            }

            try {
                DependencyGraph graph = getDependencyGraph();
                ArrayList<String> viewChangeList = new ArrayList<String>();

				/*
                 * Put all upgrade commands here. Typical upgrade patterns:
				 * 1) Add column to table:
				 * - addColumnToTable(db, tableName, columnDef)
				 * - graph.markTableChanged(tableName, viewChangeList)
				 * 2) Change in view:
				 * - graph.markViewChanged(viewName, viewChangeList)
				 * 3) Data changes:
				 * - execute change SQL statements (using tables only)
				 */
                if (oldVersion < OldVersions.Ver30018.getVersionNum()) {
                    // SQLite - alter table Drop is not supported..
                    // 1) alter existed table name -> create table -> copy the previous data -> drop existed table name
                    // 2) use the previous login info but update the table right after logout from Settings OR from Access Cloud

//                    String[] dropColumn = {"peer_id", "imei", "eimei", "web_storage_account_id", "web_storage_pw_id",
//                            "web_storage_email_id", "video_remote_play_degraded_dialog_disabled", "sync_key_images", "sync_key_audio",
//                            "sync_key_videos", "sync_key_documents", "is_sync_server", "supports_push_notification", "is_hls_server",
//                            "is_ts_server", "device_unique_id", "udn_wifi", "udn_wifi_direct", "last_recv_image_journal_id",
//                            "last_recv_video_journal_id", "last_recv_music_journal_id", "last_recv_document_journal_id",
//                            "sync_server_supported_media_types", "is_remote_wakeup", "is_remote_wakeup_support",
//                            "smallest_drive_capacity", "host_peer_id", "model_name", "model_id", "mac_bt", "mac_ble",
//                            "mac_wifi", "mac_wifi_direct"};
//
//                    dropColumnFromTable(db, DeviceMediaInfo.TABLE_NAME, dropColumn);
//                    graph.markTableChanged(DeviceMediaInfo.TABLE_NAME, viewChangeList);
                }

				/*
                 * End upgrade commands.
				 */

                //re-create all views that need to be re-made
                graph.dropAllViewsInDependencyList(db, viewChangeList);
                graph.createAllViewInDependencyList(db, viewChangeList);

                //set version last
                db.setVersion(DATABASE_VERSION);
            } catch (SQLiteException eFromTheDb) {
                Log.w(this, "Failed to upgrade database, will perform wipe. - " + eFromTheDb);
                onDowngrade(db, oldVersion, newVersion);
                return;
                //throw eFromTheDb;
            }
        }

        // This will be used in onUpgrade as changes are made to the schema
//        private void addColumnToTable(SQLiteDatabase db, String tableName, String columnDef) {
//            logAndExecCmd(db, "ALTER TABLE " + tableName + " ADD COLUMN " + columnDef + ';');
//        }

        private void dropTable(SQLiteDatabase db, String tableName) {
            logAndExecCmd(db, "DROP TABLE IF EXISTS " + tableName + ';');
        }

        private void dropView(SQLiteDatabase db, String viewName) {
            logAndExecCmd(db, "DROP VIEW IF EXISTS " + viewName + ';');
        }

        @SuppressWarnings("unused")
        private void dropIndex(SQLiteDatabase db, String indexName) {
            logAndExecCmd(db, "DROP INDEX IF EXISTS " + indexName + ';');
        }

        @SuppressWarnings("unused")
        private void dropTrigger(SQLiteDatabase db, String triggerName) {
            logAndExecCmd(db, "DROP TRIGGER IF EXISTS " + triggerName + ';');
        }

        private void logAndExecCmd(SQLiteDatabase db, String cmd) {
            Log.d(this, "logAndExecCmd : " + cmd);
            db.execSQL(cmd);
        }

        private String createJournalTable(String tableName) {
            return

                    "CREATE TABLE IF NOT EXISTS "
                            + tableName
                            + "("
                            + ASPMediaStore.JournalColumns._ID
                            + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                            + ASPMediaStore.JournalColumns.MEDIA_ID
                            + " TEXT,"
                            + ASPMediaStore.JournalColumns.IS_DELETE
                            + " INTEGER,"
                            + ASPMediaStore.JournalColumns.ORIG_JOURNAL_ID
                            + " INTEGER,"
                            + ASPMediaStore.JournalColumns.TIMESTAMP
                            + " INTEGER"
                            + ");";
        }

        /*
         * private String populateJournalTable(String journalTableName, String mediaTableName)
		 * {
		 * return
		 * "INSERT INTO " + journalTableName + " ("
		 * + ASPMediaStore.JournalColumns.MEDIA_ID + ','
		 * + ASPMediaStore.JournalColumns.IS_DELETE
		 * + ") SELECT "
		 * + ASPMediaStore.BaseASPColumns.SOURCE_MEDIA_ID
		 * + ", 0 AS cancelled"
		 * + " FROM " + mediaTableName + " INNER JOIN " + DeviceMediaInfo.TABLE_NAME
		 * + " ON " + mediaTableName + '.' + ASPMediaStore.BaseASPColumns.DEVICE_ID + '=' + DeviceMediaInfo.TABLE_NAME + '.' + ASPMediaStore.DeviceColumns._ID
		 * + " WHERE " + ASPMediaStore.DeviceColumns.TRANSPORT_TYPE + '=' + Device.DeviceTransportType.LOCAL.ordinal()
		 * + " ORDER BY " + ASPMediaStore.MediaColumns.DATE_MODIFIED
		 * + ";";
		 * }
		 */

        private String createDevicesTable() {
            return
                    "CREATE TABLE IF NOT EXISTS "
                            + DeviceMediaInfo.TABLE_NAME
                            + " ("
                            + ASPMediaStore.DeviceColumns._ID
                            + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                            + ASPMediaStore.DeviceColumns.ALIAS_NAME
                            + " TEXT,"
                            + ASPMediaStore.DeviceColumns.TRANSPORT_TYPE
                            + " TEXT DEFAULT '"
                            + CloudGatewayDeviceTransportType.UNKNOWN.name()
                            + "',"
                            + ASPMediaStore.DeviceColumns.DEVICE_PRIORITY
                            + " INTEGER DEFAULT "
                            + Integer.MAX_VALUE
                            + ","
                            + ASPMediaStore.DeviceColumns.WEB_STORAGE_TYPE
                            + " TEXT,"
                            + ASPMediaStore.DeviceColumns.WEB_STORAGE_USER_ID
                            + " TEXT,"
                            + ASPMediaStore.DeviceColumns.WEB_STORAGE_IS_SIGNED_IN
                            + " INTEGER,"
                            + ASPMediaStore.DeviceColumns.WEB_STORAGE_TOTAL_CAPACITY
                            + " INTEGER,"
                            + ASPMediaStore.DeviceColumns.WEB_STORAGE_USED_CAPACITY
                            + " INTEGER,"
                            + ASPMediaStore.DeviceColumns.SERVER_SORT_KEY
                            + " INTEGER,"
                            + ASPMediaStore.DeviceColumns.NETWORK_MODE
                            + " TEXT,"
                            + ASPMediaStore.DeviceColumns.IS_SYNCING
                            + " INTEGER,"
                            + CloudGatewayMediaStore.DeviceColumns.WEB_STORAGE_ENABLE_SYNC
                            + " INTEGER,"
                            + ASPMediaStore.DeviceColumns.MAX_NUM_TX_CONN
                            + " INTEGER,"
                            + ASPMediaStore.DeviceColumns.OAUTH_WEBVIEW_JS
                            + " TEXT"
                            + ");";
        }

        private String createFilesTable() {
            return

                    "CREATE TABLE IF NOT EXISTS " + FilesMediaInfo.TABLE_NAME + " ("
                            //Common columns for all media
                            + ASPMediaStore.MediaColumns._ID
                            + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                            + ASPMediaStore.MediaColumns.DEVICE_ID
                            + " INTEGER,"
                            + ASPMediaStore.MediaColumns.SOURCE_MEDIA_ID
                            + " TEXT,"
                            + ASPMediaStore.MediaColumns.DATE_ADDED
                            + " INTEGER,"
                            + ASPMediaStore.MediaColumns.DATE_MODIFIED
                            + " INTEGER,"
                            + ASPMediaStore.MediaColumns.DATA
                            + " TEXT,"
                            + ASPMediaStore.MediaColumns.MIME_TYPE
                            + " TEXT,"
                            + ASPMediaStore.MediaColumns.SIZE
                            + " INTEGER,"
                            + ASPMediaStore.MediaColumns.MEDIA_TYPE
                            + " INTEGER,"
                            + ASPMediaStore.MediaColumns.FULL_URI
                            + " TEXT,"
                            + ASPMediaStore.MediaColumns.THUMBNAIL_URI
                            + " TEXT,"
                            + ASPMediaStore.MediaColumns.DISPLAY_NAME
                            + " TEXT,"
                            + ASPMediaStore.MediaColumns.PARENT_CLOUD_ID
                            + " TEXT,"
                            //Common columns images & video
                            + ASPMediaStore.MediaColumns.WIDTH
                            + " INTEGER,"
                            + ASPMediaStore.MediaColumns.HEIGHT
                            + " INTEGER,"
                            + ASPMediaStore.Images.ImageColumns.ORIENTATION
                            + " INTEGER,"
                            + "is_loading"
                            + " INTEGER DEFAULT 0"
                            + ");";
        }

        private String createSingleFieldIndex(String indexName, String tableName, String fieldName) {
            return

                    "CREATE INDEX IF NOT EXISTS " + indexName + " ON " + tableName + '(' + fieldName + ");";
        }

        private String createDeviceDeleteTrigger() {
            return

                    "CREATE TRIGGER IF NOT EXISTS device_delete"
                            + " AFTER DELETE ON "
                            + DeviceMediaInfo.TABLE_NAME
                            + " FOR EACH ROW BEGIN"
                            + " DELETE FROM "
                            + FilesMediaInfo.TABLE_NAME
                            + " WHERE "
                            + ASPMediaStore.BaseASPColumns.DEVICE_ID
                            + "=OLD."
                            + ASPMediaStore.DeviceColumns._ID
                            + ';'
                            + " END;";
        }
    }

    private class NotifySender {

        private final Handler handler;

        private static final long THROTTLE_TIME = 1000;

        private static final int NOTIFY = 1;

        private static final int NOTIFY_DELAYED = 2;

        public NotifySender() {
            try {
                handler = HandlerFactorySLPF.getInstance().start("NotifySender", new HandlerCallback());
            } catch (InterruptedException e) {
                throw new RuntimeException("Trouble starting handler for NotifySender", e);
            }
        }

        public void notifyChange(Uri uri) {
            handler.sendMessage(handler.obtainMessage(NOTIFY, uri));
        }

        private class HandlerCallback implements Handler.Callback {
            private final LruCache<Uri, Info> uriToInfoMap = new LruCache<Uri, Info>(10);

            @Override
            public boolean handleMessage(Message msg) {
                long currentTime = SystemClock.elapsedRealtime();

                switch (msg.what) {
                    case NOTIFY: {
                        Uri uri = (Uri) msg.obj;
                        Info info = uriToInfoMap.get(uri);

                        if (info == null) {
                            info = new Info();
                            info.uri = uri;
                            uriToInfoMap.put(uri, info);
                        }

                        long timeDiff = currentTime - info.lastSentTime;
                        Log.d(this, "lastTimeSent : " + info.lastSentTime + ", timeDiff : " + timeDiff + ", uri : " + uri);

                        if (timeDiff > THROTTLE_TIME) {
                            info.lastSentTime = currentTime;
                            sendNoti(info);
                        } else {
                            if (info.delayedNotiScheduled) {
                                //ignore this uri - delayed noti already scheduled
                                Log.d(this, "ignoring noti for " + uri);
                            } else {
                                long delay = Math.min(Math.abs(THROTTLE_TIME - timeDiff), THROTTLE_TIME);
                                Log.d(this, "delaying noti for " + uri + " by " + delay);
                                info.delayedNotiScheduled = true;
                                NotifySender.this.handler.sendMessageDelayed(NotifySender.this.handler.obtainMessage(NOTIFY_DELAYED, info), delay);
                            }
                        }
                        return true;
                    }
                    case NOTIFY_DELAYED: {
                        Info info = (Info) msg.obj;
                        info.lastSentTime = currentTime;
                        info.delayedNotiScheduled = false;
                        sendNoti(info);
                        return true;
                    }
                }

                return false;
            }

            private void sendNoti(Info info) {
                Log.d(this, "Notifying change on URI: " + info.uri);
                getContext().getContentResolver().notifyChange(info.uri, null);
            }

            private class Info {
                Uri uri;
                long lastSentTime;
                boolean delayedNotiScheduled;
            }

        }
    }
}

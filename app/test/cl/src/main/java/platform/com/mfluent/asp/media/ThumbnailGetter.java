/**
 *
 */

package platform.com.mfluent.asp.media;

import android.os.ParcelFileDescriptor;

import com.mfluent.asp.common.media.thumbnails.ImageInfo;
import com.mfluent.log.Log;

import java.io.FileNotFoundException;
import java.util.HashMap;

import platform.com.mfluent.asp.datamodel.ASPMediaStoreProvider;
import uicommon.com.mfluent.asp.util.FileImageInfo;

/**
 * @author michaelgierach
 */
public abstract class ThumbnailGetter {

    private static final HashMap<String, AspThumbnailTask> mCurrTasks = new HashMap<String, AspThumbnailTask>();

    private final ImageInfo mMediaInfo;

    private boolean mCancelled = false;
    private Thread mWorkingThread = null;

    public ThumbnailGetter(ImageInfo mediaInfo) {
        mMediaInfo = new ImageInfo(mediaInfo);
    }

    public final ImageInfo getMediaInfo() {
        return mMediaInfo;
    }

    protected abstract FileImageInfo getThumbnail() throws Exception;

    public final void cancel() {
        if (!mCancelled) {
            mCancelled = true;
            onCancelled();
        }
    }

    protected void onCancelled() {

    }

    public final FileImageInfo openThumbnail() throws Exception {
        FileImageInfo result = null;

        AspThumbnailTask task = null;
        boolean found = true;

		/*
         * First we will check to make sure that this thread is the only
		 * one that is loading this thumbnail. If it is not, then we must
		 * wait until our turn.
		 */
        String strKeyMediaInfo = null;
        if (mMediaInfo != null && mMediaInfo.getSourceMediaId() != null) {
            strKeyMediaInfo = mMediaInfo.getSourceMediaId();
        }

        while (found) {

            synchronized (mCurrTasks) {
                if (strKeyMediaInfo != null) {
                    task = mCurrTasks.get(strKeyMediaInfo);
                    if (task == null) {
                        task = new AspThumbnailTask(mMediaInfo, this);
                        mCurrTasks.put(strKeyMediaInfo, task);
                        found = false;
                    }
                }
            }
            if (found) {
                synchronized (task) {
                    try {
                        task.wait();
                    } catch (InterruptedException ie) {
                        Log.d(this, "openThumbnail() - InterruptedException : " + ie.getMessage());
                    }
                }
            }
        }

		/*
		 * It is now our turn to execute and get the thumbnail.
		 */
        try {
            if (mCancelled) {
                return null;
            }

            mWorkingThread = Thread.currentThread();

            boolean canSkip = false;
            result = getCachedFileImageInfo();
            if (result != null) {
                ImageInfo resultInfo = result.getImageInfo();
                canSkip = ((resultInfo.getDesiredHeight() >= mMediaInfo.getDesiredHeight())
                        && (resultInfo.getDesiredWidth() >= mMediaInfo.getDesiredWidth()));
            }

            if (!canSkip) {
                result = getThumbnail();
            } else {
                Log.d(this, "openThumbnail() - skipping far fetch because file cache has acceptable copy");
            }
        } finally {
            synchronized (mCurrTasks) {
                if (strKeyMediaInfo != null) {
                    mCurrTasks.remove(strKeyMediaInfo);
                }
            }
            task.finish();
            mWorkingThread = null;
        }

        if (result == null || result.getFile() == null) {
            throw new FileNotFoundException("openThumbnail() - result = " + result + ", " + mMediaInfo);
        }

        return result;
    }

    public abstract FileImageInfo getCachedFileImageInfo();

    public final ParcelFileDescriptor openThumbnailFileDescriptor(
            ASPMediaStoreProvider provider,
            boolean getFromCache,
            boolean putInCache,
            boolean readCacheOnly,
            long groupId) throws FileNotFoundException, InterruptedException {
        ParcelFileDescriptor result = null;

        AspThumbnailTask task = null;
        boolean found = true;

        /*
         * First we will check to make sure that this thread is the only
         * one that is loading this thumbnail. If it is not, then we must
         * wait until our turn.
         */
        String strKeyMediaInfo = null;
        if (mMediaInfo != null && mMediaInfo.getSourceMediaId() != null) {
            strKeyMediaInfo = mMediaInfo.getSourceMediaId();
        }
        while (found) {

            synchronized (mCurrTasks) {
                if (strKeyMediaInfo != null) {
                    task = mCurrTasks.get(strKeyMediaInfo);
                    if (task == null) {
                        task = new AspThumbnailTask(mMediaInfo, this);
                        mCurrTasks.put(strKeyMediaInfo, task);
                        found = false;
                    }
                }
            }
            if (found) {
                synchronized (task) {
                    try {
                        task.wait();
                    } catch (InterruptedException ie) {
                        Log.d(this, "openThumbnailFileDescriptor() - InterruptedException : " + ie.getMessage());
                    }
                }
            }
        }

		/*
		 * It is now our turn to execute and get the thumbnail.
		 */
        try {
            if (this.mCancelled) {
                return null;
            }
            result = getThumbnailFileDescriptor(provider, getFromCache, putInCache, readCacheOnly, groupId);
        } finally {
            synchronized (mCurrTasks) {
                if (strKeyMediaInfo != null) {
                    mCurrTasks.remove(strKeyMediaInfo);
                }
            }
            task.finish();
        }

        return result;
    }

    protected Thread getWorkingThread() {
        return mWorkingThread;
    }

    protected abstract ParcelFileDescriptor getThumbnailFileDescriptor(
            ASPMediaStoreProvider provider,
            boolean getFromCache,
            boolean putInCache,
            boolean readCacheOnly,
            long groupId) throws FileNotFoundException, InterruptedException;
}

/**
 * 
 */

package platform.com.mfluent.asp.media;

import java.io.File;
import java.io.FileNotFoundException;

import android.os.ParcelFileDescriptor;

import platform.com.mfluent.asp.datamodel.ASPMediaStoreProvider;

/**
 * @author michaelgierach
 */
public class LocalMediaGetter extends MediaGetter {

	/**
	 * @param mediaInfo
	 * @param provider
	 */
	public LocalMediaGetter(AspMediaInfo mediaInfo, ASPMediaStoreProvider provider) {
		super(mediaInfo, provider);
	}

	/*
	 * (non-Javadoc)
	 * @see com.mfluent.asp.media.mediagetters.MediaGetter#cancel()
	 */
	@Override
	public void cancel() {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * @see com.mfluent.asp.media.mediagetters.MediaGetter#getMedia()
	 */
	@Override
	protected ParcelFileDescriptor getMedia() throws FileNotFoundException {
		ParcelFileDescriptor result = null;

		File file = new File(getMediaInfo().getSourceUri());

		result = ParcelFileDescriptor.open(file, ParcelFileDescriptor.MODE_READ_ONLY);

		if (result == null) {
			throw new FileNotFoundException("Local image not found.");
		}

		return result;
	}

}

/**
 *
 */

package platform.com.mfluent.asp.sync;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.support.v4.content.LocalBroadcastManager;
import android.util.LruCache;

import com.mfluent.asp.common.datamodel.CloudDevice;
import com.mfluent.asp.common.util.IntentHelper;
import com.mfluent.log.Log;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.lang.ref.WeakReference;
import java.util.Collection;
import java.util.HashSet;

import platform.com.mfluent.asp.framework.IASPApplication2;
import platform.com.mfluent.asp.util.HandlerFactorySLPF;

/**
 * @author Ilan Klinghofer
 */
public abstract class SingleThreadedSyncManagerSLPF {
    private Handler handler;

    private boolean started = false;

    private static final int SYNC_MESSAGE = 1;
    private static final int SYNC_RETRY = 2;

    protected final Context mContext;

    private final LruCache<MyIntent, Long> pendingSyncIntents = new LruCache<MyIntent, Long>(10);

    protected final BroadcastReceiver syncBroadcastReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            sync(intent);
        }
    };

    private boolean syncBroadcastReceiverIsRegistered = false;

    protected SingleThreadedSyncManagerSLPF(Context context) {
        mContext = context.getApplicationContext();
    }

    public final void start(Intent intent) {
        if (!started) {
            doStart(intent);
            started = true;
        }
        sync(intent);
    }

    protected void doStart(Intent intent) {
        Log.i(this, "doStart()");

        try {
            handler = HandlerFactorySLPF.getInstance().start(new SyncHandler<SingleThreadedSyncManagerSLPF>(SingleThreadedSyncManagerSLPF.this));
        } catch (InterruptedException e) {
            throw new RuntimeException("Interrupted while waiting for handler exchange", e);
        }

        Collection<IntentFilter> syncIntentFilters = getSyncIntentFilters();
        if (syncIntentFilters != null) {
            for (IntentFilter intentFilter : syncIntentFilters) {
                LocalBroadcastManager.getInstance(mContext).registerReceiver(syncBroadcastReceiver, intentFilter);
                syncBroadcastReceiverIsRegistered = true;
            }
        }
    }

    public String getThreadName() {
        return getClass().getName();
    }

    public final void stop(Intent intent) {
        if (started) {
            doStop(intent);
            started = false;
        }
    }

    protected void doStop(Intent intent) {
        Log.i(this, "doStop()");

        if (syncBroadcastReceiverIsRegistered) {
            LocalBroadcastManager.getInstance(mContext).unregisterReceiver(syncBroadcastReceiver);
        }
        if (handler != null) {
            handler.getLooper().quit();
        }
    }

    public synchronized void sync(Intent intent) {
        syncDelayed(intent, 0);
    }

    public synchronized void syncDelayed(Intent intent, long delayMillis) {
        if (!syncOnServiceCreate() && IASPApplication2.SERVICE_ON_CREATE_INTENT.equals(intent.getAction())) {
            return;
        }

        //if there is already a sync message in the queue with the same intent, don't bother to queue up another one
        MyIntent myIntent = new MyIntent(SYNC_RETRY, intent);
        Long previousIntentTime = pendingSyncIntents.get(myIntent);
        if (previousIntentTime == null) {
            previousIntentTime = 0l;
        }

        long currentTime = SystemClock.elapsedRealtime();
        if (currentTime < previousIntentTime) {
            Log.d(this, "syncDelayed - Ignoring sync intent " + intent + " because one is already queued");
            return;
        }

        Message syncMessage = handler.obtainMessage(SYNC_MESSAGE, intent);

        long throttleDelay = getThrottleTimeInMillis() - (currentTime - previousIntentTime);
        if (throttleDelay > delayMillis) {
            Log.d(this, "syncDelayed - Delaying sync intent " + intent
                    + " because not enough time has elapsed since the last one. Original delay: " + delayMillis
                    + "New delay : " + throttleDelay);
            delayMillis = throttleDelay;
        }

        if (delayMillis > 0) {
            pendingSyncIntents.put(myIntent, currentTime + delayMillis);
            handler.sendMessageDelayed(syncMessage, delayMillis);
        } else {
            pendingSyncIntents.put(myIntent, currentTime);
            handler.sendMessage(syncMessage);
        }

        handler.removeMessages(SYNC_RETRY, intent);
    }

    private static class SyncHandler<T extends SingleThreadedSyncManagerSLPF> implements Handler.Callback {

        protected final WeakReference<T> ref;

        public SyncHandler(T reference) {
            ref = new WeakReference<T>(reference);
        }

        @Override
        public final boolean handleMessage(Message msg) {
            if (msg.what == SYNC_MESSAGE || msg.what == SYNC_RETRY) {
                T t = ref.get();
                if (t != null) {
                    Intent intent = (Intent) msg.obj;
                    Log.i(this, "Syncing : " + t + " with intent " + IntentHelper.intentToString(intent));
                    try {
                        t.doSync(intent);
                    } catch (Throwable e) {
                        Log.e(this, "Exception in doSync() : " + e);
                    }
                } else {
                    Log.d(this, "WeakReference has been nulled out " + this);
                }

                return true;
            }

            return false;
        }
    }

    protected abstract void doSync(Intent intent);

    protected boolean syncOnServiceCreate() {
        return false;
    }

    /**
     * Subclasses can override this method to return an IntentFilter that will be used to register a receiver that will cause a sync when it is notified.
     * Return null to disable the sync receiver.
     */
    protected Collection<IntentFilter> getSyncIntentFilters() {
        return new HashSet<IntentFilter>();
    }

    protected Handler getHandler() {
        return handler;
    }

    protected int getSyncReason(Intent intent) {
        return intent.getIntExtra(CloudDevice.REFRESH_FROM_KEY, CloudDevice.REFRESH_FROM_UNKNOWN);
    }

    protected long getThrottleTimeInMillis() {
        return 10000;
    }

    private class MyIntent {

        private final int mWhat;

        private final Intent mIntent;

        public MyIntent(int what, Intent intent) {
            mWhat = what;
            mIntent = intent;
        }

        @Override
        public boolean equals(Object o) {
            boolean bRet = false;
            if (o instanceof MyIntent) {
                MyIntent other = (MyIntent) o;
                Intent otherIntent = other.mIntent;

                EqualsBuilder equalsBuilder = new EqualsBuilder()
                        .append(mWhat, other.mWhat)
                        .append(mIntent.getAction(), otherIntent.getAction())
                        .append(mIntent.getData(), otherIntent.getData())
                        .append(getSyncReason(mIntent), getSyncReason(otherIntent));

                bRet = equalsBuilder.isEquals();
            }

            return bRet;
        }

        @Override
        public int hashCode() {
            //@formatter:off
            HashCodeBuilder hashCodeBuilder = new HashCodeBuilder()
                    .append(mWhat)
                    .append(mIntent.getAction())
                    .append(mIntent.getData())
                    .append(getSyncReason(mIntent));
            //@formatter:on

            return hashCodeBuilder.hashCode();
        }
    }
}

/**
 *
 */

package platform.com.mfluent.asp.datamodel;

import java.io.FileNotFoundException;

import org.apache.commons.lang3.StringUtils;

import platform.com.mfluent.asp.media.AspMediaInfo;
import platform.com.mfluent.asp.media.CloudStorageMediaGetter;
import platform.com.mfluent.asp.media.LocalMediaGetter;
import platform.com.mfluent.asp.media.MediaGetter;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.ParcelFileDescriptor;

import com.mfluent.asp.common.datamodel.ASPMediaStore;
import com.mfluent.asp.common.util.FileTypeHelper;
import com.samsung.android.sdk.slinkcloud.CloudGatewayDeviceTransportType;

/**
 * @author michaelgierach
 */
public abstract class BaseMediaFileInfo extends PublicMediaInfo {

    @Override
    public String[] getStreamContentType(MediaInfoContext mediaInfoContext) {
        final String[] projection = {ASPMediaStore.MediaColumns.MIME_TYPE};
        String result = null;

        try (Cursor cursor = mediaInfoContext.provider.query(mediaInfoContext.uri, projection, null, null, null)) {
            if ((cursor != null) && (cursor.moveToFirst())) {
                result = cursor.getString(0);
            }
        }

        if (result != null) {
            return new String[]{result};
        } else {
            return null;
        }

    }

    private AspMediaInfo queryMediaInfo(MediaInfoContext mediaInfoContext, long rowId) {

        final String[] projection = {
                ASPMediaStore.BaseASPColumns.DEVICE_ID,
                ASPMediaStore.BaseASPColumns.SOURCE_MEDIA_ID,
                ASPMediaStore.MediaColumns.MEDIA_TYPE,
                ASPMediaStore.MediaColumns.FULL_URI,
                ASPMediaStore.MediaColumns.DATA};

        AspMediaInfo aspMediaInfo = null;
        Long lockToken = mediaInfoContext.dbLock.readLock();
        try (Cursor cursor = mediaInfoContext.db.query(
                getQueryTableName(mediaInfoContext),
                projection,
                ASPMediaStore.BaseASPColumns._ID + "=?",
                new String[]{Long.toString(rowId)},
                null,
                null,
                null)) {
            if (cursor != null) {
                int sourceMediaColumn = cursor.getColumnIndex(ASPMediaStore.BaseASPColumns.SOURCE_MEDIA_ID);
                int deviceIdColumn = cursor.getColumnIndex(ASPMediaStore.BaseASPColumns.DEVICE_ID);
                int mediaTypeColumn = cursor.getColumnIndex(ASPMediaStore.MediaColumns.MEDIA_TYPE);

                if (cursor.moveToFirst()) {
                    String deviceMediaId = cursor.getString(sourceMediaColumn);
                    int deviceId = cursor.getInt(deviceIdColumn);
                    DeviceSLPF foundDevice = DataModelSLPF.getInstance().getDeviceById(deviceId);
                    if (foundDevice != null) {
                        int uriColumn = cursor.getColumnIndex(foundDevice.getDeviceTransportType() == CloudGatewayDeviceTransportType.LOCAL ? ASPMediaStore.MediaColumns.DATA : ASPMediaStore.MediaColumns.FULL_URI);
                        int mediaType = cursor.getInt(mediaTypeColumn);
                        aspMediaInfo = new AspMediaInfo(rowId, foundDevice, mediaType, deviceMediaId, mediaInfoContext.uri, cursor.getString(uriColumn));
                    }
                }
            }
        } finally {
            mediaInfoContext.dbLock.readUnlock(lockToken);
        }

        return aspMediaInfo;
    }

    @Override
    public ParcelFileDescriptor openFile(MediaInfoContext mediaInfoContext, long entryId) throws FileNotFoundException, InterruptedException {
        MediaGetter getter = null;

        AspMediaInfo aspMediaInfo = queryMediaInfo(mediaInfoContext, entryId);

        if (aspMediaInfo == null) {
            throw new FileNotFoundException("Record does not exist for Uri: " + mediaInfoContext.uri);
        }
        DeviceSLPF device = aspMediaInfo.getDevice();

        switch (device.getDeviceTransportType()) {
            case LOCAL:
                getter = new LocalMediaGetter(aspMediaInfo, mediaInfoContext.provider);
                break;

            case WEB_STORAGE:
                getter = new CloudStorageMediaGetter(aspMediaInfo, mediaInfoContext.provider);
                break;
            default:
                //TODO
                break;
        }

        if (getter == null) {
            throw new FileNotFoundException("No ImageMediaGetter has been implemented.");
        }

        return getter.openMedia();
    }

    @Override
    public long handleInsert(MediaInfoContext mediaInfoContext, ContentValues values, ContentValues origValues) {
        if (!values.containsKey(ASPMediaStore.MediaColumns.DATE_ADDED)) {
            Long temp = Long.valueOf(System.currentTimeMillis() / 1000);
            if (values.containsKey(ASPMediaStore.MediaColumns.DATE_MODIFIED)) {
                temp = values.getAsLong(ASPMediaStore.MediaColumns.DATE_MODIFIED);
            } else {
                values.put(ASPMediaStore.MediaColumns.DATE_MODIFIED, temp);
            }
            values.put(ASPMediaStore.MediaColumns.DATE_ADDED, temp);
        } else if (!values.containsKey(ASPMediaStore.MediaColumns.DATE_MODIFIED)) {
            Long temp = values.getAsLong(ASPMediaStore.MediaColumns.DATE_ADDED);
            values.put(ASPMediaStore.MediaColumns.DATE_MODIFIED, temp);
        }

        String mimeType = values.getAsString(ASPMediaStore.MediaColumns.MIME_TYPE);
        if (StringUtils.isEmpty(mimeType)) {
            mimeType = FileTypeHelper.getMimeTypeForFile(values.getAsString(ASPMediaStore.MediaColumns.DISPLAY_NAME), null);

            if (StringUtils.isNotEmpty(mimeType)) {
                values.put(ASPMediaStore.MediaColumns.MIME_TYPE, mimeType);
            }
        }

        return super.handleInsert(mediaInfoContext, values, origValues);
    }
}


package platform.com.mfluent.asp.util;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import platform.com.mfluent.asp.datamodel.DataModelSLPF;
import platform.com.mfluent.asp.datamodel.DeviceSLPF;
import android.content.ContentResolver;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.net.Uri;
import android.provider.BaseColumns;
import android.provider.MediaStore;

import com.mfluent.asp.common.datamodel.ASPMediaStore;
import com.samsung.android.sdk.slinkcloud.CloudGatewayMediaSet;
import com.samsung.android.sdk.slinkcloud.CloudGatewayMediaStore;

public class CloudGatewayMediaSetHelper {

	private static final int MAX_IDS_PER_QUERY = 250;

	public interface CursorRowHandler {

		/**
		 * callback for each row in the cursor
		 * 
		 * @param cursor
		 *            the cursor
		 * @return
		 *         true to continue looping through the cursor or false to stop looping
		 */
		boolean handleCursorRow(Cursor cursor);
	}

	public DeviceSLPF getSourceDevice(ContentResolver contentResolver, CloudGatewayMediaSet mediaSet, DataModelSLPF dataModel) {

		if (mediaSet.isLocalFilePathsMediaSet()) {
			//assume local device
			return dataModel.getLocalDevice();
		}

		if (!mediaSet.isCloudGatewayUri()) {
			//assume local device
			return dataModel.getLocalDevice();
		}

		if (isMediaStoreSet(mediaSet)) {
			//assume local device
			return dataModel.getLocalDevice();
		}

		if (CloudGatewayMediaStore.FileBrowser.FileList.isFileListUri(mediaSet.getUri())) {
			long deviceId = CloudGatewayMediaStore.FileBrowser.FileList.getDeviceIdFromUri(mediaSet.getUri());

			return dataModel.getDeviceById(deviceId);
		}

		String[] projection = new String[] {"DISTINCT " + CloudGatewayMediaStore.BaseSamsungLinkColumns.DEVICE_ID};

		final Set<Long> deviceIds = new HashSet<Long>();
		runQueries(contentResolver, mediaSet, projection, new CursorRowHandler() {

			@Override
			public boolean handleCursorRow(Cursor cursor) {
				deviceIds.add(cursor.getLong(0));
				return true;
			}
		});

		if (deviceIds.size() != 1) {
			return null;
		}

		return dataModel.getDeviceById(deviceIds.iterator().next());
	}

	public void runQueries(ContentResolver contentResolver, CloudGatewayMediaSet mediaSet, String[] projection, CursorRowHandler handler) {
		String[] ids = mediaSet.getIds();
		if (ids == null) {
			ids = new String[] {};
		}

		Uri uri = mediaSet.getUri();
		String idField = StringUtils.defaultIfEmpty(mediaSet.getIdColumnName(), BaseColumns._ID);
		if (CloudGatewayMediaStore.FileBrowser.FileList.isFileListUri(uri)) {
			runQueriesMatchingIds(contentResolver, uri, projection, ids, mediaSet, handler);
		} else {
			if (mediaSet.isInclude() == false) {
				String selection;
				String[] selectionArgs;
				if (ids.length == 0) {
					//all are selected, no filtering required
					selection = mediaSet.getSelection();
					selectionArgs = mediaSet.getSelectionArgs();
				} else {
					if ((ids.length + (mediaSet.getSelectionArgs() == null ? 0 : mediaSet.getSelectionArgs().length)) > MAX_IDS_PER_QUERY) {
						//there are too many inverse selections to use bound sql arguments, so we will just use the values directly in the selection
						StringBuilder sb = new StringBuilder();
						sb.append(idField).append(" NOT IN (");
						boolean isString = mediaSet.getIdColumnType() == Cursor.FIELD_TYPE_STRING;
						for (int i = 0; i < ids.length; ++i) {
							if (i > 0) {
								sb.append(',');
							}
							if (isString) {
								sb.append('\'');
								sb.append(ids[i].replace("'", "''"));
								sb.append('\'');
							} else {
								sb.append(ids[i]);
							}
						}
						sb.append(')');

						selection = DatabaseUtils.concatenateWhere(mediaSet.getSelection(), sb.toString());
						selectionArgs = mediaSet.getSelectionArgs();

					} else {
						selection = createSqlInParameterizedString(idField, ids.length, false);
						selectionArgs = buildSelectionArgs(ids, 0, ids.length);

						selection = DatabaseUtils.concatenateWhere(mediaSet.getSelection(), selection);
						selectionArgs = DatabaseUtils.appendSelectionArgs(mediaSet.getSelectionArgs(), selectionArgs);
					}
				}

				runQueriesInner(contentResolver, uri, projection, selection, selectionArgs, handler);
			} else {

				for (int i = 0; i < ids.length; i += MAX_IDS_PER_QUERY) {
					int count = Math.min(ids.length - i, MAX_IDS_PER_QUERY);
					String selection = createSqlInParameterizedString(idField, count, mediaSet.isInclude());
					String[] selectionArgs = buildSelectionArgs(ids, i, count);

					selection = DatabaseUtils.concatenateWhere(mediaSet.getSelection(), selection);
					selectionArgs = DatabaseUtils.appendSelectionArgs(mediaSet.getSelectionArgs(), selectionArgs);

					runQueriesInner(contentResolver, uri, projection, selection, selectionArgs, handler);
				}
			}
		}
	}

	public void runMediaStoreQueries(ContentResolver contentResolver, String[] ids, String[] projection, CursorRowHandler handler) {
		if (ids == null) {
			ids = new String[] {};
		}

		for (int i = 0; i < ids.length; i += MAX_IDS_PER_QUERY) {
			int count = Math.min(ids.length - i, MAX_IDS_PER_QUERY);
			String selection = createSqlInParameterizedString(BaseColumns._ID, count, true);
			String[] selectionArgs = buildSelectionArgs(ids, i, count);

			runQueriesInner(contentResolver, MediaStore.Files.getContentUri("external"), projection, selection, selectionArgs, handler);
		}
	}

	private void runQueriesInner(
			ContentResolver contentResolver,
			Uri uri,
			String[] projection,
			String selection,
			String[] selectionArgs,
			CursorRowHandler handler) {
		Cursor cursor = contentResolver.query(uri, projection, selection, selectionArgs, null);

		if (cursor == null) {
			return;
		}

		try {
			if (!cursor.moveToFirst()) {
				return;
			}

			do {
				handler.handleCursorRow(cursor);
			} while (cursor.moveToNext());
		} finally {
			cursor.close();
		}
	}

	private void runQueriesMatchingIds(ContentResolver cr, Uri uri, String[] projection, String[] ids, CloudGatewayMediaSet mediaSet, CursorRowHandler handler) {
		Cursor cursor = cr.query(uri, projection, null, null, mediaSet.getSortOrder());

		if (cursor != null) {
			try {
				int idColumn = cursor.getColumnIndex(mediaSet.getIdColumnName());
				if (idColumn >= 0) {
					if (cursor.moveToFirst()) {
						String[] sortedIds = ids.clone();
						Arrays.sort(sortedIds);
						boolean include = mediaSet.isInclude();
						do {
							String currId = cursor.getString(idColumn);
							if (include == (Arrays.binarySearch(sortedIds, currId) >= 0)) {
								handler.handleCursorRow(cursor);
							}
						} while (cursor.moveToNext());
					}
				}
			} finally {
				cursor.close();
			}
		}
	}

	private String createSqlInParameterizedString(String field, int numParameters, boolean include) {
		StringBuilder sb = new StringBuilder(100);
		sb.append(field);

		if (include) {
			sb.append(" IN (");
		} else {
			sb.append(" NOT IN (");
		}

		for (int i = 0; i < numParameters; i++) {
			if (i > 0) {
				sb.append(',');
			}
			sb.append('?');
		}
		sb.append(')');

		return sb.toString();
	}

	private String[] buildSelectionArgs(String[] ids, int startIndex, int count) {
		String[] result = new String[count];
		for (int i = startIndex; i < startIndex + count; i++) {
			result[i - startIndex] = ids[i];
		}

		return result;
	}

	public boolean isMediaStoreSet(CloudGatewayMediaSet mediaSet) {
		return CloudGatewayMediaStore.Files.CONTENT_URI.equals(mediaSet.getUri())
				&& ASPMediaStore.MediaColumns.SOURCE_MEDIA_ID.equals(mediaSet.getIdColumnName())
				&& mediaSet.isInclude()
				&& mediaSet.getIds() != null
				&& mediaSet.getIds().length > 0;
	}
}

/**
 *
 */

package platform.com.mfluent.asp.datamodel;

import java.io.FileNotFoundException;
import java.util.ArrayList;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.ParcelFileDescriptor;

import com.mfluent.asp.common.datamodel.ASPMediaStore;

import platform.com.mfluent.asp.datamodel.ASPMediaStoreProvider.ASPMediaStoreProviderTransaction;
import platform.com.mfluent.asp.util.TokenBasedReadWriteLock;

/**
 * @author michaelgierach
 */
public abstract class MediaInfo {

    public static class MediaInfoContext {

        public final ASPMediaStoreProvider provider;
        public final ASPMediaStoreProviderTransaction transaction;
        public final SQLiteDatabase db;
        public final Uri uri;
        public final int patternId;
        public final TokenBasedReadWriteLock<Long> dbLock;

        public MediaInfoContext(
                final ASPMediaStoreProvider provider,
                final SQLiteDatabase db,
                final Uri uri,
                final int patternId,
                TokenBasedReadWriteLock<Long> dbLock) {
            this.provider = provider;
            this.db = db;
            this.uri = uri;
            this.patternId = patternId;
            this.transaction = null;
            this.dbLock = dbLock;
        }
    }

    protected static final Uri buildEntryIdUri(long entryId, String path) {
        return Uri.parse(ContentResolver.SCHEME_CONTENT + "://" + ASPMediaStore.AUTHORITY + "/" + path + "/entry/" + entryId);
    }

    protected static final Uri buildDeviceContentUri(long deviceId, String path) {
        return Uri.parse(ContentResolver.SCHEME_CONTENT + "://" + ASPMediaStore.AUTHORITY + "/" + path + "/device/" + deviceId);
    }


    public void commitTransaction(ASPMediaStoreProvider provider, SQLiteDatabase db, Object transactionObject) {
    }

    public void failedTransaction(ASPMediaStoreProvider provider, SQLiteDatabase db, Object transactionObject) {
    }

    public abstract Uri getEntryUri(long rowId);

    public abstract Uri getContentUri();

    public abstract Uri getContentUriForDevice(long deviceId);

    public abstract String getQueryTableName(MediaInfoContext context);

    public abstract String getInsertUpdateDeleteTableName();

    public abstract String getEntryContentType();

    public abstract String getContentType();

    public abstract String[] getStreamContentType(MediaInfoContext mediaInfoContext);

    public String getFilteredMediaTypeArg() {
        return null;
    }

    public ParcelFileDescriptor openFile(MediaInfoContext mediaInfoContext, long entryId) throws FileNotFoundException, InterruptedException {
        throw new FileNotFoundException("No files available for Uri:" + mediaInfoContext.uri);
    }

    protected String getDeviceIdColumn() {
        return ASPMediaStore.BaseASPColumns.DEVICE_ID;
    }

    public void getAffectedDeviceIds(MediaInfoContext mediaInfoContext, String where, String[] whereArgs, ArrayList<String> list) {
        String deviceIdColumn = getDeviceIdColumn();
        if (deviceIdColumn == null || list == null) {
            return;
        }

        String[] projection = new String[]{"DISTINCT " + deviceIdColumn};

        try(Cursor cursor = mediaInfoContext.db.query(getQueryTableName(mediaInfoContext), projection, where, whereArgs, null, null, null, null)){
            if (cursor != null && cursor.moveToFirst()) {
                while(cursor.moveToNext()) {
                    list.add(Integer.toString(cursor.getInt(0)));
                }
            }
        }
    }

    public int handleOrphanCleanup(MediaInfoContext mediaInfoContext, String where, String[] whereArgs, int deviceId) {
        return 0;
    }

    public Cursor handleQuery(
            MediaInfoContext mediaInfoContext,
            String[] projection,
            String where,
            String[] whereArgs,
            String sortBy,
            String groupBy,
            String having,
            String limit,
            long entryId) {
        return mediaInfoContext.db.query(getQueryTableName(mediaInfoContext), projection, where, whereArgs, groupBy, having, sortBy, limit);
    }

    public long handleInsert(MediaInfoContext mediaInfoContext, ContentValues values, ContentValues origValues) {
        return mediaInfoContext.db.insert(getInsertUpdateDeleteTableName(), null, values);
    }

    public int handleUpdate(MediaInfoContext mediaInfoContext, ContentValues values, String where, String[] whereArgs, long recordId, ContentValues origValues) {
        return mediaInfoContext.db.update(getInsertUpdateDeleteTableName(), values, where, whereArgs);
    }

    public int handleDelete(MediaInfoContext mediaInfoContext, String where, String[] whereArgs, boolean cleanupOrphans) {
        return mediaInfoContext.db.delete(getInsertUpdateDeleteTableName(), where, whereArgs);
    }
}

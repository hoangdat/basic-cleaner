
package platform.com.mfluent.asp.filetransfer;

import android.content.Context;
import android.database.Cursor;
import android.text.TextUtils;

import com.mfluent.asp.cloudstorage.api.sync.CloudStorageSync;
import com.mfluent.asp.cloudstorage.api.sync.CloudStorageSync.UploadResult;
import com.mfluent.asp.common.datamodel.ASPFile;
import com.mfluent.asp.common.datamodel.ASPFileProvider;
import com.mfluent.asp.common.datamodel.ASPMediaStore;
import com.mfluent.asp.common.util.CloudStorageError;
import com.mfluent.log.Log;
import com.samsung.android.sdk.slinkcloud.CloudGatewayConstants.OperationType;
import com.samsung.android.sdk.slinkcloud.CloudGatewayFileBrowserUtils;
import com.samsung.android.sdk.slinkcloud.CloudGatewayFileTransferUtils;
import com.samsung.android.sdk.slinkcloud.CloudGatewayFileTransferUtils.TransferOptions;
import com.samsung.android.sdk.slinkcloud.CloudGatewayMediaStore;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import platform.com.mfluent.asp.datamodel.DeviceSLPF;
import platform.com.mfluent.asp.datamodel.filebrowser.ASPFileProviderFactory;
import platform.com.mfluent.asp.datamodel.filebrowser.CachedCloudFile;
import platform.com.mfluent.asp.datamodel.filebrowser.CachedFileBrowser;
import platform.com.mfluent.asp.filetransfer.UploadTask.FileUploadInfo;
import platform.com.mfluent.asp.filetransfer.UploadTask.StorageUploadFileTooLargeException;
import platform.com.mfluent.asp.util.AspFileUtilsSLPF;
import platform.com.mfluent.asp.util.FileUtils;
import platform.com.mfluent.asp.util.UiUtilsSLPF;
import platform.com.samsung.android.slinkcloud.NetResourceCacheManager;

public class C2CTransferTask extends FileTransferTask implements FileTransferTask.UploadCompleteListener {

    private static class C2CItemInfo extends TransferItemInfo {
        public String sourceMediaId;
        public ASPFile aspFile = null;
        public ASPFile aspDirectory = null;
        public String storageGatewayFileId = null;
        public String fileName;
        public String mimeType;
        public File targetFile;
        public long length;
        public boolean isDirectory = false;
    }

    private static class C2CTaskInfo extends TransferTaskInfo {

        DeviceSLPF sourceDevice;
        C2CItemInfo mainDownload;
        File targetDirectory;
        String targetFolderID = null;
        boolean skipTransfer = false;
        File tmpFile;
        long totalBytesToTransfer;
        int nTaskId = 0;
        boolean bNeedReplace = false;

        public C2CTaskInfo(int nNewTaskId) {
            nTaskId = nNewTaskId;
        }
    }

    private final ArrayList<C2CTaskInfo> tasks = new ArrayList<>();
    private final Set<C2CTaskInfo> completedTasks = new HashSet<C2CTaskInfo>();
    private final Set<C2CTaskInfo> skippedTasks = new HashSet<>();

    private final File cacheDir;

    private Context context = null;

    private boolean isDownloading = false;

    private long totalSizeToUpload = 0;
    private int nTaskCount = 0;
    private int nSentFileNum = 0;

    private OperationType opType = OperationType.NONE;

    public C2CTransferTask(
            Context context,
            DeviceSLPF targetDevice,
            FileTransferTaskListener listener,
            File cacheDir,
            TransferOptions options,
            String strPremadeSession) {
        super(context, targetDevice, listener, strPremadeSession, options);

        this.cacheDir = cacheDir;
        this.context = context;
        this.nTaskCount = 0;
    }

    private File getTargetPrivateDirectory() {
        Log.d(this, "getTargetPrivateDirectory() called");

        File tmpF = new File(getContext().getExternalFilesDir(null), ".tmp");
        if (!tmpF.mkdirs()) {
            Log.d(this, "getTargetPrivateDirectory() - Failed make dir tmpF");
        }

        File noMediaF = new File(tmpF, ".nomedia");
        if (!noMediaF.exists()) {
            try {
                noMediaF.createNewFile();
            } catch (IOException e) {
                Log.e(this, "getTargetPrivateDirectory() - IOException : " + e.getMessage());
            }
        }
        return tmpF;
    }

    @Override
    public void addTask(ASPFile file, DeviceSLPF sourceDevice) {
        addTask(file, sourceDevice, null, null);
    }

    @Override
    public boolean addTask(ASPFile file, DeviceSLPF sourceDevice, String relativeTarget, Map<String, String> targetFolderCache) {
        boolean bRet = true;
        Log.d(this, "addTask() - ASPFile : " + file + ", sourceDevice : " + sourceDevice);
        C2CTaskInfo taskInfo = new C2CTaskInfo(nTaskCount++);

        taskInfo.mainDownload = new C2CItemInfo();
        if (file instanceof CachedCloudFile) {
            CachedCloudFile cachedFile = (CachedCloudFile) file;
            taskInfo.mainDownload.aspFile = null;
            taskInfo.mainDownload.storageGatewayFileId = cachedFile.getCloudId();
            Log.i(this, "c2c addFile CachedCloudFile storageGatewayFileId=" + taskInfo.mainDownload.storageGatewayFileId);
        } else {
            taskInfo.mainDownload.aspFile = file;
        }

        if (file.isDirectory()) {
            taskInfo.mainDownload.isDirectory = true;
            taskInfo.skipTransfer = true;
            taskInfo.mainDownload.aspDirectory = file;
            taskInfo.mainDownload.length = 0;
            taskInfo.totalBytesToTransfer = 0;
        } else {
            taskInfo.mainDownload.length = file.length();
            taskInfo.totalBytesToTransfer = file.length();
        }
        taskInfo.mainDownload.fileName = file.getName();
        taskInfo.mainDownload.sourceMediaId = null;
        taskInfo.sourceDevice = sourceDevice;

        String convertedFileName = taskInfo.mainDownload.fileName;
        // convert filename to ASP Media Type
        taskInfo.mainDownload.mimeType = UiUtilsSLPF.getMimeFromFilename(convertedFileName);
        taskInfo.targetDirectory = getTargetPrivateDirectory();
        taskInfo.mainDownload.targetFile = new File(taskInfo.targetDirectory, convertedFileName);

        if (TextUtils.isEmpty(relativeTarget) == false) {
            taskInfo.targetFolderID = findFolderIDfromRelativePath(relativeTarget, targetFolderCache);
            if (mRenameSkipResult.equals(taskInfo.targetFolderID))
                bRet = false;
        }
        Log.i(this, "taskInfo.targetFolderID = " + taskInfo.targetFolderID);

        setTotalBytesToTransfer(getTotalBytesToTransfer() + (taskInfo.totalBytesToTransfer * 2));

        tasks.add(taskInfo);
        addSourceDevice(sourceDevice);

        return bRet;
    }

    @Override
    public boolean isDownload() {
        return isDownloading;
    }

    @Override
    public int getNumberOfFiles() {
        return tasks.size();
    }

    private File getCacheFile(String strTargetName) {
        File tmpDir = new File(cacheDir + "/" + getSessionId());
        if (tmpDir == null || tmpDir.exists() == false) {
            tmpDir.mkdirs();
        }
        File tmpF = new File(tmpDir.getAbsolutePath(), strTargetName);
        return tmpF;
    }

    private boolean checkDownloadSkip(C2CTaskInfo task) {
        boolean bSkip = false;
        try {
            if (getOptions().skipIfDuplicate) {
                if (task.mainDownload.aspFile != null) {
                    if (task.mainDownload.targetFile.exists()) {
                        if (task.mainDownload.aspFile.length() <= 0) {
                            bSkip = true;
                        } else if (task.mainDownload.targetFile.length() == task.mainDownload.aspFile.length()) {
                            bSkip = true;
                        }
                        if (bSkip) {
                            Log.d(this, "checkDuplication(), duplicate file : " + task.mainDownload.targetFile);
                        }
                    }
                }
            }
        } catch (Exception e) {
            Log.e(this, "checkDownloadSkip() - Exception : " + e.getMessage());
        }
        if (task.mainDownload.targetFile == null || task.mainDownload.targetFile.exists() == false) {
            bSkip = false;
        }
        return bSkip;
    }

    private void doThreadSafeOneItemTransfer(C2CTaskInfo task, C2CItemInfo downloadItem) throws Exception {
        //download
        boolean bTmpFileNeedToDelete = true;

        if (task.skipTransfer) {
            lockMultiChannel();
            skippedTasks.add(task);
            unlockMultiChannel();

            Log.d(this, "skip upload : " + downloadItem.targetFile);
            return;
        }

        FileUploadInfo fileUpload = new FileUploadInfo();
        if (checkDownloadSkip(task) == false) {
            //real download
            Log.d(this, "download start()");
            opType = OperationType.DOWNLOAD;

            CloudStorageSync cloudStorageSync = task.sourceDevice.getCloudStorageSync();
            String downloadResult = cloudStorageSync.downloadFile(this, downloadItem.storageGatewayFileId, task.targetDirectory.getAbsolutePath(), downloadItem.fileName, getOptions().waitForRename);
            if (downloadResult == null) {
                throw new IOException("Download error - source file name : " + task.mainDownload.fileName);
            } else {
                task.mainDownload.fileName = downloadResult;
            }

            //now we have completed the download - will delete task.tmpFile when done with it
            task.tmpFile = new File(task.targetDirectory.getAbsolutePath(), downloadItem.fileName);
            long tmpFileLength = AspFileUtilsSLPF.getFileSizeWithRetry(task.tmpFile);
            if (task.mainDownload.length > 0 && tmpFileLength != task.mainDownload.length) {
                Log.i(this, "strange realdownloaddate_size=" + tmpFileLength + ",metadata_file_size=" + task.mainDownload.length);
            }
        } else {
            bTmpFileNeedToDelete = false;
            task.tmpFile = new File(task.mainDownload.targetFile.getAbsolutePath());
        }
        fileUpload.mimeType = downloadItem.mimeType;
        fileUpload.fileToUpload = task.tmpFile;
        fileUpload.targetFolderID = task.targetFolderID;
        fileUpload.fileName = task.mainDownload.fileName;
        totalSizeToUpload += task.tmpFile.length();
        Log.d(this, "totalSizeToUpload : " + UiUtilsSLPF.formatShortFileSize(context, totalSizeToUpload));
        Log.d(this, "fileUpload displayName : " + task.mainDownload.fileName + ", mimeType : " + task.mainDownload.mimeType);

        //upload tmp file!
        opType = OperationType.UPLOAD;
        doWebStorageUpload(fileUpload);
        //delete tmp file!
        if (bTmpFileNeedToDelete) {
            FileUtils.deleteQuietly(task.tmpFile);
        }

        //add completed task
        lockMultiChannel();
        completedTasks.add(task);
        unlockMultiChannel();
    }

    private void doWebStorageUpload(FileUploadInfo fileUploadInfo) throws Exception {
        DeviceSLPF device = getTargetDevice();
        CloudStorageSync cloudStorageSync = device.getCloudStorageSync();
        if (cloudStorageSync == null) {
            Log.d(this, "doWebStorageUpload() -  Trying to upload to a device that is not found.(" + device);
            throw new IllegalStateException("Device does not exist: " + device);
        }

        if (isCanceled()) {
            Log.d(this, "doWebStorageUpload() canceled");
            return;
        }

        if (fileUploadInfo.completed) {
            this.bytesTransferred(fileUploadInfo.fileToUpload.length());
            return;
        }

        String targetDirId = null;
        if (fileUploadInfo.targetFolderID != null) {
            targetDirId = fileUploadInfo.targetFolderID;
        } else if (getOptions().targetDirectoryPath != null) {
            targetDirId = getOptions().targetDirectoryPath;
        }
        UploadResult result = cloudStorageSync.uploadFile(targetDirId, fileUploadInfo.fileToUpload, fileUploadInfo.mimeType, this);

        Log.d(this, "doWebStorageUpload() - C2C uploading targetDirId=" + targetDirId + " ,result : " + result);

        switch (result.mStatus) {
            case OK:
                Log.d(this, "doWebStorageUpload() - totalSizeUploaded : " + UiUtilsSLPF.formatShortFileSize(context, getBytesTransferred()));
                if ((targetDirId != null) && targetDirId.equals(getOptions().targetDirectoryPath))
                    CachedFileBrowser.deltaFileLayerAdd(device, context, fileUploadInfo.fileName, targetDirId, result.mFileId, false);
                NetResourceCacheManager.getInstance(context).deleteCache(CachedFileBrowser.URI_MATCHER + targetDirId, null, device.getId());
                break;
            case TOO_LARGE:
                throw new StorageUploadFileTooLargeException();
            case NO_NEED_RETRY:
                throw new IOException(CloudStorageError.NO_NEED_RETRY_ERROR);
            case MAX_RETRY_FAIL:
                Log.d(this, "doWebStorageUpload: upload failed & retry to the max : " + result);
                throw new IOException("upload failed & retry to the max : " + result);
            case OUT_OF_SPACE:
                Log.d(this, "doTransfer() - doWebStorageUpload: upload failed & out of space : " + result);
                throw new IOException(CloudStorageError.OUT_OF_STORAGE_ERROR);
            case REACHED_MAX_ITEMS:
                Log.d(this, "doTransfer() - doWebStorageUpload: upload failed & reach max items : " + result);
                throw new IOException(CloudStorageError.REACH_MAX_ITEM_ERROR);
            default:
                ////auto retry file transfer
                //throw new Exception("web storage upload failed: " + result);
                throw new IOException("web storage upload failed: " + result);
        }

        if (this != null) {
            onUploadComplete(fileUploadInfo);
        }
    }

    @Override
    public void destroy() {
        for (C2CTaskInfo task : tasks) {
            FileUtils.deleteQuietly(task.tmpFile);
        }
    }

    @Override
    public int getCurrentFileIndex() {
        long totalBytesTransferred = getBytesTransferred();
        long sum = 0;
        int currentFileIndex = 0;
        for (C2CTaskInfo downloadTaskInfo : tasks) {
            sum += downloadTaskInfo.mainDownload.length;
            if (totalBytesTransferred <= sum) {
                break;
            }

            currentFileIndex++;
        }

        return currentFileIndex;
    }

    /*
     * (non-Javadoc)
     * @see com.mfluent.asp.filetransfer.FileUploader.UploadCompleteListener#onUploadComplete(com.mfluent.asp.filetransfer.FileUploader.FileUploadInfo)
     */
    @Override
    public void onUploadComplete(FileUploadInfo fileUploadInfo) {
        Log.d(this, "onUploadComplete() - fileUploadInfo displayName : " + fileUploadInfo.displayName
                + ", mimeType : " + fileUploadInfo.mimeType);

        fileUploadInfo.completed = true;
    }

    //multi channel codes
    @Override
    protected void prepareMultiChannel() throws Exception {
        super.prepareMultiChannel();
        Log.i(this, "C2C prepareMultiChannel");
        //check folder to file list to send
        ASPFileProvider fileProvider = beginFolderSourceAnalysisBeforeSending();
        if (fileProvider != null) {
            String strCloudId = null;

            int nSize = tasks.size();
            for (int nCnt = 0; nCnt < nSize; nCnt++) {
                C2CTaskInfo taskInfo = tasks.get(nCnt);
                //for (C2CTaskInfo taskInfo : this.tasks) {
                if (taskInfo.mainDownload.isDirectory) {
                    try {
                        if (taskInfo.mainDownload.storageGatewayFileId != null) {
                            strCloudId = taskInfo.mainDownload.storageGatewayFileId;
                        } else if (taskInfo.mainDownload.aspFile != null) {
                            strCloudId = fileProvider.getStorageGatewayFileId(taskInfo.mainDownload.aspFile);
                        }
                    } catch (Exception e) {
                        Log.e(this, "prepareMultiChannel() - Exception : " + e.getMessage());
                        strCloudId = null;
                    }
                    checkSourceFolderBeforeSending(fileProvider, strCloudId, taskInfo.mainDownload.aspDirectory, taskInfo.mainDownload.fileName);
                }
            }
            endFolderSourceAnalysisBeforeSending();
        }
        //check rename condition
        if (getOptions().waitForRename) {
            for (final C2CTaskInfo taskInfo : tasks) {
                if (isCanceled()) {
                    Log.i(this, "prepareMultiChannel() - canceled");
                    break;
                }
                if (taskInfo.mainDownload.isDirectory) {
                    Log.i(this, "skip checkRename " + taskInfo.mainDownload.fileName + " due to dirprecheck done");
                    continue;
                }
                String targetDirId = null;
                if (taskInfo.targetFolderID != null) {
                    targetDirId = taskInfo.targetFolderID;
                } else if (getOptions().targetDirectoryPath != null) {
                    targetDirId = getOptions().targetDirectoryPath;
                }

                final String finalTargetDirId = targetDirId;
                requestRename(targetDirId, taskInfo.mainDownload.fileName, new RenameDataItem.OnReceiveRSPRenameStatus() {
                    @Override
                    public void onReceiveRenameStatus(RenameDataItem dataItem, String strRenamed) {
                        Log.d(this, "onReceiveRenameStatus - strRenamed: " + strRenamed);
                        processRenameResult(taskInfo, dataItem, strRenamed, finalTargetDirId);
                    }
                });
            }
        }
    }


    @Override
    protected void processRenameResult(TransferTaskInfo taskInfo, RenameDataItem dataItem, String strNewName, String targetDirId) {
        int nRspStatus = 0;
        if (dataItem != null) {
            nRspStatus = dataItem.nStatus;
            if (dataItem.bApplyAll) {
                mbRenameApplyedAll = true;
                m_nRenameApplyedAllStatus = nRspStatus;
            }
        }
        if (taskInfo instanceof C2CTaskInfo) {
            C2CTaskInfo c2cTask = (C2CTaskInfo) taskInfo;
            if (nRspStatus == CloudGatewayFileTransferUtils.RENAME_STATUS_RSP_RENAME) {
                c2cTask.targetDirectory = getTargetPrivateDirectory();
                c2cTask.mainDownload.fileName = strNewName;
            } else if (nRspStatus == CloudGatewayFileTransferUtils.RENAME_STATUS_RSP_REPLACE) {
                c2cTask.bNeedReplace = true;
                if (!c2cTask.mainDownload.isDirectory) {
                    String selection = "_display_name=? and parent_cloud_id=?";
                    String[] selectionArgs = new String[]{c2cTask.mainDownload.fileName, targetDirId};
                    Cursor cursor = null;
                    try {
                        String parentId = null;
                        cursor = context.getContentResolver().query(CloudGatewayMediaStore.CloudFiles.getCloudFileUri(c2cTask.sourceDevice.getId()), new String[]{ASPMediaStore.Files.FileColumns.PARENT_CLOUD_ID}, ASPMediaStore.Files.FileColumns.SOURCE_MEDIA_ID + "=?", new String[]{c2cTask.mainDownload.storageGatewayFileId}, null);
                        if ((cursor != null) && (cursor.getCount() != 0) && cursor.moveToFirst()) {
                            parentId = cursor.getString(cursor.getColumnIndex(ASPMediaStore.Files.FileColumns.PARENT_CLOUD_ID));
                            cursor.close();
                        }
                        if (parentId != null && !parentId.equals(targetDirId)) {
                            cursor = context.getContentResolver().query(CloudGatewayMediaStore.CloudFiles.getCloudFileUri(getTargetDevice().getId()), new String[]{ASPMediaStore.Files.FileColumns.SOURCE_MEDIA_ID}, selection, selectionArgs, null);
                            if ((cursor != null) && (cursor.getCount() != 0) && cursor.moveToFirst()) {
                                String[] cloudIds = new String[cursor.getCount()];
                                int count = 0;
                                do {
                                    cloudIds[count++] = cursor.getString(cursor.getColumnIndex(ASPMediaStore.Files.FileColumns.SOURCE_MEDIA_ID));
                                } while (cursor.moveToNext());
                                CloudGatewayFileBrowserUtils.getInstance(context).mrrControlBatchCommand(getTargetDevice().getId(),CloudGatewayFileBrowserUtils.REMOTE_FOLDER_RMDIR_BATCH, cloudIds, null);
                                cursor.close();
                            }
                        } else {
                            c2cTask.skipTransfer = true;
                        }
                    } catch (Exception e) {
                        Log.e(this, "processRenameResult() - Exception : " + e.getMessage());
                    } finally {
                        if (cursor != null && !cursor.isClosed()) {
                            cursor.close();
                        }
                    }
                }
            } else if (nRspStatus == CloudGatewayFileTransferUtils.RENAME_STATUS_RSP_SKIP) {
                Log.i(this, "processRenameResult RENAME_STATUS_RSP_SKIP");
                c2cTask.skipTransfer = true;
            }
        }
    }

    @Override
    protected boolean onExitMultiChannel() {
        boolean isDeleteSourceFiles = false;
        String[] cloudIds = new String[completedTasks.size()];
        int count = 0, deviceId = 0;
        for (C2CTaskInfo task : completedTasks) {
            deviceId = task.sourceDevice.getId();
            File tmpFile = getCacheFile(task.mainDownload.targetFile.getName());
            if (tmpFile != null && tmpFile.exists()) {
                FileUtils.deleteQuietly(tmpFile);
            }

            if (getOptions().deleteSourceFilesWhenTransferIsComplete && !task.skipTransfer) {
                if (task.mainDownload.aspFile != null) {
                    try {
                        ASPFileProvider fileProvider = ASPFileProviderFactory.getFileProviderForDevice(this.context, task.sourceDevice);
                        String cloudId = fileProvider.getStorageGatewayFileId(task.mainDownload.aspFile);
                        cloudIds[count++] = cloudId;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (task.mainDownload.storageGatewayFileId != null) {
                    cloudIds[count++] = task.mainDownload.storageGatewayFileId;
                }
                if (task.sourceDevice == getTargetDevice()) {
                    isDeleteSourceFiles = true;
                }
            }
        }
        if (isDeleteSourceFiles) {
            return true;
        } else {
            return (getOptions().deleteSourceFilesWhenTransferIsComplete) ? CloudGatewayFileBrowserUtils.getInstance(context).mrrControlBatchCommand(deviceId, CloudGatewayFileBrowserUtils.REMOTE_FOLDER_RMDIR_BATCH, cloudIds, null) : true;
        }
    }

    @Override
    protected void doThreadSafeTransfer(int nChannelId) throws Exception {
        int nItemCnt = 0;

        for (C2CTaskInfo task : tasks) {
            if (isCanceled()) {
                Log.d(this, "doThreadSafeTransfer() canceled");
                break;
            }
            nItemCnt++;
            if (!checkMultiChannelItem(nChannelId, nItemCnt)) {
                continue;
            }

            if (task != null) {
                doThreadSafeOneItemTransfer(task, task.mainDownload);
                nSentFileNum++;
            }
            Thread.sleep(50);
        }
    }

    //added by shirley for persistent file transfer
    @Override
    public long getSourceMediaId(int index) {
        return 0;//this.tasks.get(index).mainDownload.id;
    }

    @Override
    public int getSentFileNumForMultiChannelSending() {
        return nSentFileNum;
    }

    @Override
    public void cancel() {
        super.cancel();
        DeviceSLPF deviceSLPF = null;
        if (opType == OperationType.DOWNLOAD) {
            Set<DeviceSLPF> srcSet = getSourceDevices();
            if (srcSet != null) {
                Iterator<DeviceSLPF> it = srcSet.iterator();
                if (it.hasNext()) {
                    deviceSLPF = it.next();
                }
            }
        } else {
            deviceSLPF = getTargetDevice();
        }

        if (deviceSLPF != null) {
            CloudStorageSync cloudStorageSync = deviceSLPF.getCloudStorageSync();
            cloudStorageSync.cancel(opType);
        }
    }
}

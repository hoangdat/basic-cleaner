
package platform.com.mfluent.asp.dws;

import android.content.Context;

import java.io.File;
import java.util.Collections;
import java.util.Set;

import platform.com.mfluent.asp.datamodel.DataModelSLPF;
import platform.com.mfluent.asp.datamodel.DeviceSLPF;
import platform.com.mfluent.asp.filetransfer.FileTransferSession;
import platform.com.mfluent.asp.util.FileUtils;
import platform.com.mfluent.asp.util.MFLStorageManagerSLPF;

public class SaveFilesSession implements FileTransferSession {

    private File mTmpFile;
    private final File mCacheDir;
    private final String mSessionId;
    private int mCurrentFileIndex = -1;
    private Status mStatus = Status.INIT;

    public SaveFilesSession(Context context, String sessionId) {
        mCacheDir = new File(MFLStorageManagerSLPF.getCacheDir(context), "savefiles_cache");
        mSessionId = sessionId;

        mCacheDir.mkdirs();
    }

    public void cleanup() {
        FileUtils.deleteQuietly(mTmpFile);
        mTmpFile = null;
    }

    @Override
    public int getCurrentFileIndex() {
        return mCurrentFileIndex;
    }

    @Override
    public Status getStatus() {
        return mStatus;
    }

    @Override
    public void setStatus(Status status) {
        mStatus = status;
    }

    @Override
    public String getSessionId() {
        return mSessionId;
    }


    @Override
    public int getProgress() {
//        if (this.totalBytes <= 0) {
//            return 0;
//        } else if (this.totalBytesSent > this.totalBytes) {
//            return 100;
//        }
//        return (int) (this.totalBytesSent * 100 / this.totalBytes);
        return 0;
    }

    @Override
    public boolean errorOccurred() {
        return Status.STOPPED.equals(getStatus());
    }

    @Override
    public int getNumberOfFiles() {
//        return this.numberOfTotalFiles;
        return 0;
    }

    @Override
    public Set<DeviceSLPF> getSourceDevices() {
        return Collections.emptySet();
    }

    @Override
    public DeviceSLPF getTargetDevice() {
        return DataModelSLPF.getInstance().getLocalDevice();
    }

    @Override
    public long getTotalBytesToTransfer() {
        //return this.totalBytes;
        return 0;
    }

    @Override
    public long getBytesTransferred() {
//        return this.totalBytesSent;
        return 0;
    }

    @Override
    public boolean isTransferStarted() {
        return true;
    }

    @Override
    public boolean isInProgress() {
        return Status.INIT.equals(mStatus) || Status.SENDING.equals(mStatus);
    }

    @Override
    public boolean isDownload() {
        return true;
    }
}

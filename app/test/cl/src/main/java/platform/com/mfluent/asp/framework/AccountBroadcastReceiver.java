package platform.com.mfluent.asp.framework;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.mfluent.log.Log;

import platform.com.mfluent.asp.datamodel.DataModelSLPF;
import platform.com.mfluent.asp.datamodel.DeviceSLPF;
import platform.com.mfluent.asp.ui.SignInOutUtils;
import platform.com.mfluent.asp.ui.StorageSignInOutHelper;
import platform.com.mfluent.asp.util.DeviceUtilSLPF;

public class AccountBroadcastReceiver extends BroadcastReceiver {
    private void removeAccount(final Context context_final, final String storageType) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                DeviceSLPF device = DataModelSLPF.getInstance().getDeviceByStorageType(storageType);
                if (device == null) {
                    Log.d(this, "removeAccount() - device is null " + storageType);
                    return;
                }

                String accountId = device.getWebStorageUserId();
                if (accountId == null) {
                    Log.d(this, "removeAccount() - accountId is null " + storageType);
                    return;
                }

                String accountsType = null;
                switch (storageType) {
                    case DeviceUtilSLPF.GOOGLEDRIVE_WEBSTORAGE_NAME:
                        accountsType = SignInOutUtils.AccountType_GoogleAccount;
                        break;
                    case DeviceUtilSLPF.SAMSUNGDRIVE_WEBSTORAGE_NAME:
                        accountsType = SignInOutUtils.AccountType_SamsungAccount;
                        break;
                    default:
                        Log.e(this, "removeAccount() - Wrong storage type : " + storageType);
                        return;
                }

                try {
                    AccountManager accountManager = AccountManager.get(context_final);
                    Account[] accounts = accountManager.getAccountsByType(accountsType);
                    if ((accounts != null) && (accounts.length > 0)) {
                        if (DeviceUtilSLPF.GOOGLEDRIVE_WEBSTORAGE_NAME.equals(storageType)) {
                            for (Account a : accounts) {
                                if (a.name.equals(accountId)) {
                                    Log.d(this, "removeAccount(), google accounts found");
                                    return;
                                }
                            }
                        } else { // Samsung - 1 account is allowed
                            return;
                        }
                    }

                    StorageSignInOutHelper signOutHelper = new StorageSignInOutHelper(context_final);
                    signOutHelper.signOutOfStorageService(device);
                } catch (SecurityException e) {
                    Log.e(this, "removeAccount() - SecurityException error : storageType is " + storageType + ", " + e.getMessage());
                }
            }
        }).start();
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(this, "onReceive() - Received Intent : " + intent.getAction());

        if ("android.accounts.LOGIN_ACCOUNTS_CHANGED".equals(intent.getAction())) {
            removeAccount(context, DeviceUtilSLPF.GOOGLEDRIVE_WEBSTORAGE_NAME);
            removeAccount(context, DeviceUtilSLPF.SAMSUNGDRIVE_WEBSTORAGE_NAME);
        }
    }
}

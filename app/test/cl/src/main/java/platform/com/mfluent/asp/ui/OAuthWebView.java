
package platform.com.mfluent.asp.ui;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.CookieManager;
import android.webkit.ValueCallback;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.webkit.WebViewDatabase;
import android.widget.FrameLayout;

import com.mfluent.asp.cloudstorage.api.sync.CloudStorageSync;
import com.mfluent.asp.common.datamodel.CloudDevice;
import com.mfluent.log.Log;
import com.samsung.android.slinkcloud.R;

import java.net.URISyntaxException;

import platform.com.mfluent.asp.datamodel.DataModelSLPF;
import platform.com.mfluent.asp.datamodel.DeviceSLPF;
import platform.com.mfluent.asp.framework.IASPApplication2;
import platform.com.mfluent.asp.framework.ServiceLocatorSLPF;
import platform.com.mfluent.asp.framework.StorageProviderDatabaseHelper;
import platform.com.mfluent.asp.framework.StorageProviderInfo;
import platform.com.mfluent.asp.sync.CloudStorageSyncManager;

public class OAuthWebView extends Activity {

    private static final String TAG = "OAuthWebView";

    private static final String GOOGLE_ACCOUNT_TYPE = "com.google";

    private WebView mWebView;

    private int mDeviceId;

    private static final String SAVE_INSTANCE_DEVICE_ID = "save_instance_device_id";

    private IntentFilter mCallbackFilter;

    private FrameLayout mWebViewPlaceholder;

    private boolean mPageFinishedLoading = false;

    private boolean mAbortingSignIn = false;


    public static final Intent createOAuthWebViewIntent(Context context, Intent cloudStorageIntent) {
        // Log this out only because it was logged in this method's predecessor, startOauthWebView
        Log.i(TAG, "createOAuthWebViewIntent() - startOauthWebView OAuthWebPage : " + cloudStorageIntent.getStringExtra(CloudStorageSync.OAUTH1_URI));
        Intent result = new Intent(context, OAuthWebView.class);
        result.putExtras(cloudStorageIntent);
        result.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        return result;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (ServiceLocatorSLPF.get(IASPApplication2.class).isTablet()) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.oauth_webveiw);
        showLoading();
        initUI();
    }

    private String getAccountByType(String type) {

        Log.i(this, "getAccountByType(), type = " + type);

        Account[] accounts = AccountManager.get(this).getAccountsByType(type);
        if (accounts.length < 1) {
            return null;
        }

        Log.i(this, "getAccountByType(), account = " + accounts[0].name);
        return accounts[0].name;
    }

    private void finishWithResultOK() {
        setResult(RESULT_OK);
        finish();
    }

    @SuppressWarnings("deprecation")
    @SuppressLint("SetJavaScriptEnabled")
    // We need javascript for OAuth to work
    private void initUI() {

        // Retrieve UI elements
        mWebViewPlaceholder = ((FrameLayout) findViewById(R.id.webViewPlaceholder));

        if (mWebView == null) {
            mWebView = new WebView(this);
            WebSettings webSettings = mWebView.getSettings();
            webSettings.setJavaScriptEnabled(true);

            webSettings.setLoadWithOverviewMode(true);
            webSettings.setUseWideViewPort(true);
            webSettings.setSavePassword(false);
            mWebView.setInitialScale(1);

            webSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);
            mWebView.setWebViewClient(new WebViewClient() {

                @Override
                public void onPageFinished(WebView view, String url) {

                    String javascriptFormat = null;
                    DeviceSLPF device = null;

                    try {
                        device = DataModelSLPF.getInstance().getDeviceById(getDeviceID());
                    } catch (Exception e) {
                        Log.e(this, "initUI() - onPageFinished exception : " + e.getMessage());
                    }

                    if (device != null) {
                        javascriptFormat = device.getOAuthWebViewJS();
                    }
                    String javascript = null;
                    String account = null;

                    if (url.contains("accounts.google.com")) { // In case of the Google oAuth login page
                        Log.d(this, "onPageFinished() - Google oAuth url");
                        account = getAccountByType(GOOGLE_ACCOUNT_TYPE);
                    }

                    if (javascriptFormat != null && account != null) {  //P151028-05156
                        javascript = String.format(javascriptFormat, account);

                        if (javascript != null) {
                            Log.d(this, "onPageFinished() - injected js = " + javascript);

                            view.evaluateJavascript(javascript, new ValueCallback<String>() {

                                @Override
                                public void onReceiveValue(String s) {
                                    Log.d(this, "ValueCallback::onReceiveValue(), " + s);
                                }
                            });
                        }
                    }

                    hideLoading();
                    mPageFinishedLoading = true;
                    Log.d(this, "onPageFinished() - oauth load finished.");
                }

                // TODO: we need to handle error in the browser

                @Override
                public void onPageStarted(WebView view, String url, Bitmap favicon) {
                    super.onPageStarted(view, url, favicon);

                    Log.d(this, "onPageStarted() : " + url);
                    showLoading();
                    mPageFinishedLoading = false;
                }

                @Override
                public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                    Log.e(this, "onReceivedError() oauth page load error: " + errorCode + ": " + description);
                }

                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {

                    // TODOMK line 13 in drawing
                    Log.e(this, "shouldOverrideUrlLoading() : " + url);
                    Intent intent;
                    try {
                        intent = Intent.parseUri(url, 0);
                        int match = mCallbackFilter.match(getContentResolver(), intent, true, TAG);
                        Log.e(this, "shouldOverrideUrlLoading() : match = " + match);
                        if (match >= 0 || url.startsWith("http://accesscloud.samsung.com")) {

                            if (url.contains("not_approved=true") || url.contains("error=access_denied")) {
                                //dropbox || box
                                finish();
                            } else {

                                LocalBroadcastManager localBraodcastManager = LocalBroadcastManager.getInstance(OAuthWebView.this);
                                intent = new Intent(CloudStorageSync.CLOUD_OAUTH1_RESPONSE);
                                intent.putExtra(CloudDevice.DEVICE_ID_EXTRA_KEY, mDeviceId);
                                intent.putExtra(CloudStorageSync.OAUTH1_RESPONSE_URI, url);
                                localBraodcastManager.sendBroadcast(intent);

                                DeviceSLPF device = DataModelSLPF.getInstance().getDeviceById(getDeviceID());
                                if (device != null) {
                                    StorageProviderDatabaseHelper providerDB = StorageProviderDatabaseHelper.getInstance(OAuthWebView.this);
                                    StorageProviderInfo selectedStorageService = providerDB.get(device.getWebStorageType());
                                    if (selectedStorageService != null) {
                                        selectedStorageService.setLoginStatus(true);
                                    }
                                    providerDB.saveOrUpdate(selectedStorageService);
                                    ////tskim imsi PoC dropbox sync
                                    if (device.getWebStorageType().equals("dropbox")) {
                                        CloudStorageSyncManager.bNeedPoCToken = true;
                                    }
                                }

                                // TODO - calling finish here sometimes causes problems. Sometimes onReceivedError gets called afterwards
                                finishWithResultOK();
                            }

                            return true;
                        } else if (url.contains("allshareplay.com/storage/oAuthPcLogin.do")
                                && url.contains("&oauth_token=")
                                && url.contains("&oauth_verifier=")) {
                            // This is a *HACK* until Myung can sort out how this should work.
                            // http://www.allshareplay.com/storage/oAuthPcLogin.do?callbackSpName=ndrive&target=tv&cnty=USA&oauth_token=a5PXbw3EJ8EPnQl2&oauth_verifier=XmxyRZdoHPgPoTz0RKC9rc64LI6c2x
                            LocalBroadcastManager localBraodcastManager = LocalBroadcastManager.getInstance(OAuthWebView.this);
                            intent = new Intent(CloudStorageSync.CLOUD_OAUTH1_RESPONSE);
                            intent.putExtra(CloudDevice.DEVICE_ID_EXTRA_KEY, mDeviceId);
                            intent.putExtra(CloudStorageSync.OAUTH1_RESPONSE_URI, url);
                            localBraodcastManager.sendBroadcast(intent);

                            return true;
                        } else if (url.startsWith("market://")) {
                            view.getContext().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                            return true;
                        }
                    } catch (URISyntaxException e) {
                        Log.e(this, "shouldOverrideUrlLoading() - URISyntaxException parsing url : " + e.getMessage());
                    }

                    view.loadUrl(url);
                    return true;
                }
            });

            loadUrlToWebClient();
        }

        mWebViewPlaceholder.addView(mWebView);

        if (mPageFinishedLoading) {
            hideLoading();
        } else {
            showLoading();
        }
    }

    @SuppressWarnings("deprecation")
    private void loadUrlToWebClient() {
        Intent intent = getIntent();
        mDeviceId = intent.getIntExtra(CloudDevice.DEVICE_ID_EXTRA_KEY, 0);
        String oauthUri = intent.getStringExtra(CloudStorageSync.OAUTH1_URI);
        mCallbackFilter = intent.getParcelableExtra(CloudStorageSync.OAUTH1_CALLBACK_FILTER);
        if (mCallbackFilter == null) {
            mCallbackFilter = new IntentFilter();
            mCallbackFilter.addAction(Intent.ACTION_VIEW);
            mCallbackFilter.addDataScheme(CloudStorageSync.ASP_OAUTH_CALLBACK_KEYWORD);
        }
        //tskim fix no need not to clear cache in cloudmanager ux
        if (mWebView != null) {
            try {
                mWebView.clearCache(true);
                CookieManager cookieManager = CookieManager.getInstance();
                cookieManager.removeAllCookie();
                WebViewDatabase webViewDatabase = WebViewDatabase.getInstance(this);
                webViewDatabase.clearFormData();
                webViewDatabase.clearHttpAuthUsernamePassword();
                webViewDatabase.clearUsernamePassword();
            } catch (Exception e) {
                Log.d(this, "loadUrlToWebClient() - Trouble clearing webview persistent data : " + e.getMessage());
            }

            Log.i(this, "loadUrlToWebClient() - LoadURI: " + oauthUri);
            mWebView.loadUrl(oauthUri);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
        LocalBroadcastManager.getInstance(this).registerReceiver(mDeviceListChangedReceiver, new IntentFilter(DataModelSLPF.BROADCAST_DEVICE_LIST_CHANGE));
    }

    @Override
    protected void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mDeviceListChangedReceiver);
        hideLoading();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        if (mWebView != null) {
            // Remove the WebView from the old placeholder
            mWebViewPlaceholder.removeView(mWebView);
        }

        super.onConfigurationChanged(newConfig);

        // Load the layout resource for the new configuration
        setContentView(R.layout.oauth_webveiw);

        // Reinitialize the UI
        initUI();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        // Save the state of the WebView
        mWebView.saveState(outState);

        outState.putBoolean("aborting_signin", mAbortingSignIn);
        outState.putInt(SAVE_INSTANCE_DEVICE_ID, mDeviceId);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        // Restore the state of the WebView
        mWebView.restoreState(savedInstanceState);

        savedInstanceState.getBoolean("aborting_signin");
        savedInstanceState.getInt(SAVE_INSTANCE_DEVICE_ID);
    }

    public int getDeviceID() {
        return mDeviceId;
    }

    private void showLoading() {
        findViewById(R.id.loading_layout).setVisibility(View.VISIBLE);
    }

    private void hideLoading() {
        findViewById(R.id.loading_layout).setVisibility(View.GONE);
    }

    @Override
    public void onBackPressed() {
        if (mWebView.canGoBack()) {
            mWebView.goBack();
        } else {

            //showLoading();

            DeviceSLPF device = DataModelSLPF.getInstance().getDeviceById(getDeviceID());

            if (device != null && !device.isWebStorageSignedIn()) {

                device.setPendingSetup(false);
                try {
                    mAbortingSignIn = true;
                    device.getCloudStorageSync().reset();
                } catch (Exception e) {
                    Log.e(this, "onBackPresed() - Exceptions : " + e.getMessage());
                }
                super.onBackPressed();
// -------------------------------------------------

            } else {
                super.onBackPressed();
            }
        }
    }

    private final BroadcastReceiver mDeviceListChangedReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (mAbortingSignIn) {
                mAbortingSignIn = false;
                hideLoading();
                finishWithResultOK();
            }
        }
    };
}

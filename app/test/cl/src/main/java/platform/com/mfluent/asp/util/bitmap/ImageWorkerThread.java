
package platform.com.mfluent.asp.util.bitmap;

import platform.com.mfluent.asp.util.bitmap.ImageWorker.LoadTask;

public class ImageWorkerThread extends Thread {

	private final ImageWorkerQueue mQueue;
	private LoadTask mTask;
	public final byte[] buffer;

	public ImageWorkerThread(ImageWorkerQueue queue) {
		this.mQueue = queue;
		setDaemon(true);
		this.buffer = new byte[16 * 1024];
	}

	@Override
	public void run() {
		while (true) {

			try {
				this.mTask = this.mQueue.get();

				this.mTask.run();
			} catch (InterruptedException ie) {
				Thread.interrupted();
			} catch (Throwable t) {

			}
			this.mTask = null;
		}
	}
}

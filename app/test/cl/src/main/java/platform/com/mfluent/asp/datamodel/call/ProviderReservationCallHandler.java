package platform.com.mfluent.asp.datamodel.call;

import android.content.Context;
import android.os.Bundle;

/**
 * Created by sec on 2015-08-28.
 */
public class ProviderReservationCallHandler implements CallHandler {
    @Override
    public Bundle call(Context context, String method, String arg, Bundle extras) {
        //do nothing...only for provider onCreate
        Bundle bundle = new Bundle();
        bundle.putBoolean("result", true);
        return bundle;
    }
}

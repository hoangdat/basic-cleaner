/**
 *
 */

package platform.com.mfluent.asp.util;

import com.mfluent.log.Log;

import java.lang.reflect.Method;
import java.util.ArrayList;


/**
 * @author Ilan Klinghofer
 */
public class MFLEnvironmentSLPF {

    private static final String TAG = "MFLEnvironment";
    private static final ArrayList<String> CHINA_SALES_CODES;

    static {
        CHINA_SALES_CODES = new ArrayList<String>();
        CHINA_SALES_CODES.add("CHN");
        CHINA_SALES_CODES.add("CHU");
        CHINA_SALES_CODES.add("CHM");
        CHINA_SALES_CODES.add("CHZ");
        CHINA_SALES_CODES.add("CTC");
        CHINA_SALES_CODES.add("CHC");
    }

    public static String getSalesCode() {
        Log.i(TAG, "getSalesCode() called");
        Class<?> c = null;

        try {
            c = Class.forName("android.os.SystemProperties");
        } catch (ClassNotFoundException e) {
            Log.e(TAG, "getSalesCode() - ClassNotFoundException : " + e.getMessage());
            return null;
        }

        Method m = null;

        try {
            m = c.getMethod("get", String.class);
        } catch (NoSuchMethodException e) {
            Log.e(TAG, "getSalesCode() - NoSuchMethodException : " + e.getMessage());
            return null;
        }

        String salesCode = null;

        try {
            salesCode = (String) m.invoke(c, "ro.csc.sales_code");
        } catch (Exception e) {
            Log.w(TAG, "getSalesCode() - Exception : " + e.getMessage());
            return null;
        }

        Log.i(TAG, "getSalesCode() returns " + salesCode);
        return salesCode;
    }

    public static boolean isRunningOnChinaProduct() {
        Log.i(TAG, "isRunningOnChinaProduct() called");
        String salesCode = getSalesCode();
        return salesCode != null ? CHINA_SALES_CODES.contains(salesCode) : false;
    }
}

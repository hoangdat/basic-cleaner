package platform.com.mfluent.asp.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;
import android.util.Base64;

import com.mfluent.log.Log;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;

/**
 * Created by seongsu.yoon on 2017-03-22.
 */

public class CryptoManager {

    private static final String KEY_ALIAS = "com.samsung.android.slinkcloud_key";
    private static final String KEY_ALGORITHM = "AES";
    private static final String KEY_PROVIDER = "AndroidKeyStore";
    private static final String CIPHER_TRANSFORMATION = "AES/CBC/PKCS7PADDING";
    private static final String CHARSET_UTF8 = "UTF-8";
    private static final String SHAREDPREF_KEY_IV = "IV";

    private byte[] mIV = null;

    private Context mContext;

    public CryptoManager(Context context) {
        mContext = context;
    }

    private void setIV(byte[] IV, String prefFileName) {
        if (IV == null) {
            Log.e(this, "setIV() - IV is null");
            return;
        }

        mIV = IV;
        saveIVtoPreferences(IV, prefFileName);
    }

    private byte[] getIV(String prefFileName) {
        if (mIV == null) {
            String savedIV = getIVfromPreferences(prefFileName);
            if (savedIV != null) {
                mIV = Base64.decode(savedIV, Base64.NO_WRAP);
            }
        }
        return mIV;
    }

    public void makeKeyFromKeyStore() {
        try {
            KeyGenParameterSpec.Builder builder = new KeyGenParameterSpec.Builder(KEY_ALIAS, KeyProperties.PURPOSE_ENCRYPT | KeyProperties.PURPOSE_DECRYPT);
            KeyGenParameterSpec keySpec = builder.setKeySize(256)
                    .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                    .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_PKCS7)
                    .build();
            KeyGenerator kg = KeyGenerator.getInstance(KEY_ALGORITHM, KEY_PROVIDER);
            kg.init(keySpec);
            kg.generateKey();
        } catch (NoSuchAlgorithmException e) {
            Log.e(this, "makeKeyFromKeyStore() - NoSuchAlgorithmException : " + e.getMessage());
            e.printStackTrace();
        } catch (NoSuchProviderException e) {
            Log.e(this, "makeKeyFromKeyStore() - NoSuchProviderException : " + e.getMessage());
            e.printStackTrace();
        } catch (InvalidAlgorithmParameterException e) {
            Log.e(this, "makeKeyFromKeyStore() - InvalidAlgorithmParameterException : " + e.getMessage());
            e.printStackTrace();
        }
    }

    public SecretKey getKeyFromKeyStore() {
        SecretKey key = null;
        try {
            KeyStore ks = KeyStore.getInstance(KEY_PROVIDER);
            ks.load(null);

            KeyStore.SecretKeyEntry entry = (KeyStore.SecretKeyEntry)ks.getEntry(KEY_ALIAS, null);
            key = entry.getSecretKey();
        } catch (KeyStoreException e) {
            Log.e(this, "getKeyFromKeyStore() - KeyStoreException : " + e.getMessage());
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            Log.e(this, "getKeyFromKeyStore() - NoSuchAlgorithmException : " + e.getMessage());
            e.printStackTrace();
        } catch (CertificateException e) {
            Log.e(this, "getKeyFromKeyStore() - CertificateException : " + e.getMessage());
        } catch (IOException e) {
            Log.e(this, "getKeyFromKeyStore() - IOException : " + e.getMessage());
        } catch (UnrecoverableEntryException e) {
            Log.e(this, "getKeyFromKeyStore() - UnrecoverableEntryException : " + e.getMessage());
        } finally {
            return key;
        }
    }

    public String encryptString(String strPlain, String prefFileName) {
        String strEncrypted = null;
        SecretKey key = getKeyFromKeyStore();
        try {
            if (key != null && strPlain != null) {
                Cipher cipher = Cipher.getInstance(CIPHER_TRANSFORMATION);
                cipher.init(Cipher.ENCRYPT_MODE, key);

                setIV(cipher.getIV(), prefFileName);

                byte[] encrypted = cipher.doFinal(strPlain.getBytes(CHARSET_UTF8));
                strEncrypted = new String(Base64.encode(encrypted, Base64.DEFAULT));
            }
        } catch (NoSuchAlgorithmException e) {
            Log.e(this, "encryptString() - NoSuchAlgorithmException : " + e.getMessage());
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            Log.e(this, "encryptString() - NoSuchPaddingException : " + e.getMessage());
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            Log.e(this, "encryptString() - InvalidKeyException : " + e.getMessage());
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            Log.e(this, "encryptString() - UnsupportedEncodingException : " + e.getMessage());
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            Log.e(this, "encryptString() - IllegalBlockSizeException : " + e.getMessage());
            e.printStackTrace();
        } catch (BadPaddingException e) {
            Log.e(this, "encryptString() - BadPaddingException : " + e.getMessage());
            e.printStackTrace();
        } finally {
            return strEncrypted;
        }
    }

    public String decryptString(String strEncrypted, String prefFileName) {
        String strPlain = null;
        SecretKey key = getKeyFromKeyStore();
        try {
            if (key != null) {
                Cipher cipher = Cipher.getInstance(CIPHER_TRANSFORMATION);
                cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(getIV(prefFileName)));

                byte[] byteStr = Base64.decode(strEncrypted.getBytes(CHARSET_UTF8), Base64.DEFAULT);
                strPlain = new String(cipher.doFinal(byteStr), CHARSET_UTF8);
            }
        } catch (NoSuchAlgorithmException e) {
            Log.e(this, "decryptString() - NoSuchAlgorithmException : " + e.getMessage());
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            Log.e(this, "decryptString() - NoSuchPaddingException : " + e.getMessage());
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            Log.e(this, "decryptString() - InvalidKeyException : " + e.getMessage());
            e.printStackTrace();
        } catch (InvalidAlgorithmParameterException e) {
            Log.e(this, "decryptString() - InvalidAlgorithmParameterException : " + e.getMessage());
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            Log.e(this, "decryptString() - UnsupportedEncodingException : " + e.getMessage());
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            Log.e(this, "decryptString() - IllegalBlockSizeException : " + e.getMessage());
            e.printStackTrace();
        } catch (BadPaddingException e) {
            Log.e(this, "decryptString() - BadPaddingException : " + e.getMessage());
            e.printStackTrace();
        } finally {
            return strPlain;
        }
    }

    public void deleteKeyFromKeyStore() {
        try {
            KeyStore ks = KeyStore.getInstance(KEY_PROVIDER);
            ks.load(null);
            ks.deleteEntry(KEY_ALIAS);
        } catch (KeyStoreException e) {
            Log.e(this, "deleteKeyFromKeyStore() - KeyStoreException : " + e.getMessage());
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            Log.e(this, "deleteKeyFromKeyStore() - NoSuchAlgorithmException : " + e.getMessage());
            e.printStackTrace();
        } catch (CertificateException e) {
            Log.e(this, "deleteKeyFromKeyStore() - CertificateException : " + e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            Log.e(this, "deleteKeyFromKeyStore() - IOException : " + e.getMessage());
        }
    }

    public boolean isKeyExist() {
        boolean bRet = false;
        try {
            KeyStore ks = KeyStore.getInstance(KEY_PROVIDER);
            ks.load(null);
            bRet = ks.containsAlias(KEY_ALIAS);
        } catch (KeyStoreException e) {
            Log.e(this, "isKeyExist() - KeyStoreException : " + e.getMessage());
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            Log.e(this, "isKeyExist() - NoSuchAlgorithmException : " + e.getMessage());
            e.printStackTrace();
        } catch (CertificateException e) {
            Log.e(this, "isKeyExist() - CertificateException : " + e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            Log.e(this, "isKeyExist() - IOException : " + e.getMessage());
        }

        return bRet;
    }

    private void saveIVtoPreferences(byte[] IV, String prefFileName) {
        SharedPreferences.Editor editor = getSharedPreferences(prefFileName).edit();
        editor.putString(SHAREDPREF_KEY_IV, new String(Base64.encode(IV, Base64.DEFAULT)));
        editor.apply();
    }

    private String getIVfromPreferences(String prefFileName) {
        return getSharedPreferences(prefFileName).getString(SHAREDPREF_KEY_IV, null);
    }

    private SharedPreferences getSharedPreferences(String prefFileName) {
        return mContext.getSharedPreferences(prefFileName, Context.MODE_PRIVATE);
    }
}

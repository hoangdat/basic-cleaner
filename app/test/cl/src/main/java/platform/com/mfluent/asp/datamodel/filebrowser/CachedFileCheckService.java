package platform.com.mfluent.asp.datamodel.filebrowser;

import android.content.Context;
import android.os.PowerManager;

import com.mfluent.log.Log;
import com.samsung.android.sdk.slinkcloud.CloudGatewayDeviceTransportType;

import java.util.List;

import platform.com.mfluent.asp.datamodel.DataModelSLPF;
import platform.com.mfluent.asp.datamodel.DeviceSLPF;
import platform.com.mfluent.asp.framework.IASPApplication2;
import platform.com.mfluent.asp.framework.ServiceLocatorSLPF;
import platform.com.mfluent.asp.sync.CloudStorageSyncManager;
import platform.com.mfluent.asp.util.NetworkUtilSLPF;
import platform.com.samsung.android.slinkcloud.HeatManager;
import uicommon.com.mfluent.asp.util.ScheduledHandlerBasedTaskWithoutIdleThread;

/**
 * Created by sec on 2015-09-04.
 */
public class CachedFileCheckService {
    private static String TAG = "CachedFileCheckService";
    static volatile boolean mbDuringService = false;
    static volatile boolean mbFileBrowserCheckTriggered = false;
    static final int CLOUD_SYNC_POLLING_INTERVAL_MSEC = 301300; //5 minute

    static ScheduledHandlerBasedTaskWithoutIdleThread sScheduler = new ScheduledHandlerBasedTaskWithoutIdleThread();

    public static void startScheduler() {
        sScheduler.setRunnable(new Runnable() {
            @Override
            public void run() {
                if (NetworkUtilSLPF.isNetworkAvailable()) {
                    runCheck();
                }
//                Log.i("INFO", "CachedFileCheckService service launch");
//                IASPApplication2 asp2 = ServiceLocatorSLPF.get(IASPApplication2.class);
//                if(asp2 != null) {
//                    Log.i("INFO", "CachedFileCheckService service launch2");
//                    asp2.startService(new Intent(asp2, CachedFileCheckService.class));
//                }
            }
        });
        sScheduler.setRepeatTimer(20000, CLOUD_SYNC_POLLING_INTERVAL_MSEC);
    }

    public static void stopScheduler() {
        sScheduler.cancelRepeatTimer();
    }

    public static void triggerScan(long nDelay) {
        Log.d(TAG, "triggerScan() - delay = " + nDelay);
        mbFileBrowserCheckTriggered = true;
        sScheduler.triggerEvent(nDelay);
    }

    public static void runCheck() {
        if (mbDuringService) {
            return;
        }
        mbDuringService = true;
        try {
            if (mbFileBrowserCheckTriggered) {
                Log.d(TAG, "runCheck() - triggerFileBrowser");
                fileBrowserCheck();
                mbFileBrowserCheckTriggered = false;
            } else {
                cloudSyncCheck();
            }

        } finally {
            mbDuringService = false;
        }
    }

    public static void cloudSyncCheck() {
        IASPApplication2 asp2 = ServiceLocatorSLPF.get(IASPApplication2.class);
        if (asp2 != null) {
            Context context = asp2.getApplicationContext();
            boolean isScreenOn = true;
            try {
                PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
                isScreenOn = pm.isScreenOn();
            } catch (Exception e) {
                e.printStackTrace();
            }

            HeatManager heatManager = HeatManager.getInstance(context);
            long nCurTime = System.currentTimeMillis();

            if (nCurTime < CloudStorageSyncManager.mLastSyncDoneTime + 60000) {
                Log.i(TAG, "cloudSyncCheck - disable timer cloud sync due to sync frequency too much");
            } else if (isScreenOn == false) {
                Log.i(TAG, "cloudSyncCheck - disable timer cloud sync during screen off");
            } else if (heatManager.isDeviceOverheated()) {
                Log.i(TAG, "cloudSyncCheck - disable timer cloud sync because device is overheated");
            } else {
                List<DeviceSLPF> tmpList = DataModelSLPF.getInstance().getDevicesByType(CloudGatewayDeviceTransportType.WEB_STORAGE);
                if (tmpList != null) {
                    int nSize = tmpList.size();
                    DeviceSLPF tmpDev;
                    for (int nCnt = 0; nCnt < nSize; nCnt++) {
                        tmpDev = tmpList.get(nCnt);
                        if ((tmpDev != null) && tmpDev.isWebStorageSignedIn() && tmpDev.isSyncing() == false) {
                            Log.i(TAG, "send timer cloud sync msg for " + tmpDev.getWebStorageType());
                            tmpDev.broadcastDeviceRefresh();
                        }
                    }
                }
                //Intent brintent = new Intent(CloudStorageSyncManager.strCloudRefreshEvent);
                //LocalBroadcastManager.getInstance(context).sendBroadcast(brintent);
            }
        }
    }

    public static void fileBrowserCheck() {
        Log.i(TAG, "CachedFileCheckService started...");
        try {
            CachedFileBrowser.checkAllInstances();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.i(TAG, "CachedFileCheckService ended...");
    }
}

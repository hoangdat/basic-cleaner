
package platform.com.mfluent.asp.filetransfer;

import java.util.Set;

import platform.com.mfluent.asp.datamodel.DeviceSLPF;

public interface FileTransferSession {

    enum Status {
        INIT,
        SENDING,
        STOPPED,
        COMPLETED
    }

    String getSessionId();

    int getProgress();

    boolean errorOccurred();

    int getNumberOfFiles();

    Set<DeviceSLPF> getSourceDevices();

    DeviceSLPF getTargetDevice();

    long getTotalBytesToTransfer();

    long getBytesTransferred();

    boolean isTransferStarted();

    boolean isInProgress();

    boolean isDownload();

    int getCurrentFileIndex();

    Status getStatus();

    ////auto retry file transfer, to cancel from remote device
    void setStatus(Status status);

}

package platform.com.mfluent.asp.filetransfer;

/**
 * Created by sec on 2015-08-12.
 */
public class RenameDataItem {
    public String strRenameToken = "";
    public String strSessionID = "";
    public String strOldName = "";
    public String strNewName = "";
    public long   nCheckTime = 0;
    public boolean bIsDirectory = false;
    public int		nStatus = 0;
    public boolean bApplyAll = false;

    public interface OnReceiveRSPRenameStatus {
        void onReceiveRenameStatus(RenameDataItem dataItem, String strRenamed);
    }
}
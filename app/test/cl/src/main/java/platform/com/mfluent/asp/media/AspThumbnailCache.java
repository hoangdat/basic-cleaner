/**
 *
 */

package platform.com.mfluent.asp.media;

import android.content.Context;
import android.graphics.BitmapFactory;

import com.mfluent.asp.common.media.thumbnails.ImageInfo;
import com.mfluent.log.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;

import platform.com.mfluent.asp.util.FileUtils;
import platform.com.mfluent.asp.util.IOUtils;
import uicommon.com.mfluent.asp.util.FileImageInfo;

/**
 * @author michaelgierach
 */
public class AspThumbnailCache {

    private static final String DATA_FILE_SUFFIX = ".jpg";
    private static final String INFO_FILE_SUFFIX = ".info";
    private static final String THUMB_CACHE_DIR = "thumb_cache";

    private static AspThumbnailCache sInstance = null;

    private File mBaseDir;
    private final HashMap<String, ThumbnailInfo> mInfoCache;
    private final ReentrantLock mMapLock;
    public int mMaxSize = 1000000000; //200000000;
    public int mNormalSize = mMaxSize;
    public static final long INTERVAL_CACHESIZE_CHECK = 15 * 60 * 1000;    //cache size check per 15 minutes
    private volatile long mLastTimeCacheCheck = 0;
    private static final int BUFF_SIZE_FAST = 128 * 1024;

    private static class ThumbnailInfo {

        public FileImageInfo mObjInfo;
        public ImageInfo mObjPrefetchImg;

        public ThumbnailInfo(ImageInfo info) {
            mObjInfo = null;
            mObjPrefetchImg = info;
        }

        public void updateFile(File file) {
            mObjInfo = new FileImageInfo(mObjPrefetchImg, file);
            mObjPrefetchImg = null;
        }

        public ThumbnailInfo(FileImageInfo fileImgInfo) {
            mObjInfo = fileImgInfo;
            mObjPrefetchImg = null;
        }
    }

    public static synchronized AspThumbnailCache getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new AspThumbnailCache(200 * 1024 * 1024);
            sInstance.init(context);
        }
        return sInstance;
    }

    /**
     * @param maxSize
     */
    private AspThumbnailCache(int maxSize) {
        mMaxSize = maxSize;
        mNormalSize = maxSize * 2 / 3;
        mMapLock = new ReentrantLock();
        mInfoCache = new HashMap<String, ThumbnailInfo>(1000);
    }

    public boolean checkExternalCloudDirectory() {
        return true;
    }

    public void init(Context context) {
        Log.i(this, "init() - cache thumbnail init");

        mBaseDir = context.getDir(THUMB_CACHE_DIR, Context.MODE_PRIVATE);

        Log.i(this, "init() - cache thumbnail init end...");
    }

    public File getCacheDir() {
        return mBaseDir;
    }

    private ThumbnailInfo getThumbInfo(String key) {
        return mInfoCache.get(key); //P151119-02266
    }

    ////instead of getting all thumbfiles to hashmap, putting item on demand
    private FileImageInfo lckGetFileImageInfo(String key) {
        FileImageInfo objRet = null;
        mMapLock.lock();
        try {
            ThumbnailInfo thumbnailInfo = getThumbInfo(key);
            if (thumbnailInfo != null) {
                objRet = thumbnailInfo.mObjInfo;
                if (objRet != null && (objRet.getFile() == null || objRet.getFile().exists() == false)) {
                    ////remove item in some zombie state
                    Log.e(this, "lckGetFileImageInfo() - no jpg file => delete item " + key);
                    mInfoCache.remove(key);
                    objRet = null;
                }
            } else {
                ////check whether there is xxx.info file in the cache directory
                File dataFile = getDataFile(key);
                File infoFile = getInfoFile(key);
                if (dataFile.exists() && infoFile.exists()) {
                    FileImageInfo fileImageInfo = new FileImageInfo(infoFile, dataFile);
                    ThumbnailInfo thumInfo = new ThumbnailInfo(fileImageInfo);
                    mInfoCache.put(key, thumInfo);
                    objRet = fileImageInfo;
                }
            }
        } finally {
            mMapLock.unlock();
        }
        return objRet;
    }

    private boolean lckCheckItemDeleted(String key) {
        mMapLock.lock();
        try {
            ThumbnailInfo thumbInfo = getThumbInfo(key);
            if (thumbInfo == null) {
                return true;
            } else {
                return false;
            }
        } finally {
            mMapLock.unlock();
        }
    }

    ////if other task is getting thumbnail before this task, this task wait for 5 sec for the prv work to be done
    private boolean lckReserveThumbnailWork(String key, ImageInfo imgInfo) {
        mMapLock.lock();
        try {
            ThumbnailInfo thumbnailInfo = getThumbInfo(key);
            if (thumbnailInfo != null) {
                return false;
            }
            ThumbnailInfo thumnailInfo = new ThumbnailInfo(imgInfo);
            mInfoCache.put(key, thumnailInfo);
            return true;
        } finally {
            mMapLock.unlock();
        }

    }

    private FileImageInfo lckCheckPreviousWorkDone(String key) {
        mMapLock.lock();
        try {
            ThumbnailInfo thumbnailInfo = getThumbInfo(key);
            if (thumbnailInfo != null && thumbnailInfo.mObjInfo != null) {
                return thumbnailInfo.mObjInfo;
            }
            return null;
        } finally {
            mMapLock.unlock();
        }
    }

    ////called after putFile
    private FileImageInfo lckPostUpdateThumbInfo(String key, ImageInfo imgInfo, File file) {
        mMapLock.lock();
        try {
            ThumbnailInfo thumbnailInfo = getThumbInfo(key);
            if (thumbnailInfo != null) {
                thumbnailInfo.updateFile(file);
            } else {
                thumbnailInfo = new ThumbnailInfo(imgInfo);
                thumbnailInfo.updateFile(file);
                mInfoCache.put(key, thumbnailInfo);
            }
            return thumbnailInfo.mObjInfo;
        } finally {
            mMapLock.unlock();
        }
    }

    private void lckClearAllItemsFromMap() {
        mMapLock.lock();
        try {
            mInfoCache.clear();
        } finally {
            mMapLock.unlock();
        }

    }

    private void lckDeleteItemFromMap(String key) {
        mMapLock.lock();
        try {
            mInfoCache.remove(key);
        } finally {
            mMapLock.unlock();
        }

    }

    private void deleteRelatedFile(String key) {
        File infoFile = getInfoFile(key);
        if (infoFile != null && infoFile.exists()) {
            File dataFile = getDataFile(key);
            try {
                infoFile.delete();
                dataFile.delete();
            } catch (Exception e) {
                Log.e(this, "deleteRelatedFile() - key : " + key + ", Exceptions : " + e.getMessage());
            }
        }
    }

    public void setDevicePrefetchList(int deviceId, ArrayList<ImageInfo> sortedImageInfos) {
        ////do nothing here
    }

    public void clearDevicePrefetchList(int deviceId) {
        if (mInfoCache == null || mInfoCache.isEmpty()) {
            return;
        }
        mMapLock.lock();
        try {
            Iterator it = mInfoCache.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry<String, ThumbnailInfo> pair = (Map.Entry) it.next();
                String strKey = pair.getKey();
                ThumbnailInfo thumnailInfo = pair.getValue();
                if (thumnailInfo != null && thumnailInfo.mObjInfo != null) {
                    ImageInfo objImg = thumnailInfo.mObjInfo.getImageInfo();
                    if (objImg != null && objImg.getDeviceId() == deviceId) {
                        deleteRelatedFile(strKey);
                        it.remove();
                    }
                }
            }
        } finally {
            mMapLock.unlock();
        }
    }

    public FileImageInfo getFile(ImageInfo imageInfo) {
        String key = generateKey(imageInfo);

        return getFileFromKey(key);
    }

    public FileImageInfo getCachedOnly(long contentId, int mediaType, long deviceId) {
        String key = generateKey(contentId, mediaType, deviceId);
        FileImageInfo result = lckGetFileImageInfo(key);
        if (result == null || result.getImageInfo() == null || result.getFile() == null || !result.getFile().exists()) {
            return null;
        }
        return result;
    }

    public FileImageInfo getFile(long contentId, int mediaType, long deviceId) {
        String key = generateKey(contentId, mediaType, deviceId);

        return getFileFromKey(key);
    }

    private FileImageInfo getFileFromKey(String key) {
        FileImageInfo result = lckGetFileImageInfo(key);

        if (result == null || result.getImageInfo() == null || result.getFile() == null || !result.getFile().exists()) {
            return null;
        }

        return result;
    }

    public FileImageInfo putFile(ImageInfo imageInfo, File tmpFile) {
        String key = generateKey(imageInfo);

        checkOversizeCacheInterval();
        boolean bNewTask = lckReserveThumbnailWork(key, imageInfo);
        if (!bNewTask) {
            int nCnt = 0;
            FileImageInfo previousInfo;
            Log.i(this, "putFile() - putFile wait for previous work on " + key);
            for (nCnt = 0; nCnt < 10; nCnt++) {
                previousInfo = lckCheckPreviousWorkDone(key);
                if (previousInfo != null) {
                    Log.i(this, "putFile() - putFile done previous work on " + key);
                    return previousInfo;
                }
                if (lckCheckItemDeleted(key)) {
                    bNewTask = lckReserveThumbnailWork(key, imageInfo);
                    if (bNewTask == false) {
                        Log.i(this, "putFile() - putFile previous work deleted but can't redownload " + key);
                        return null;
                    } else {
                        Log.i(this, "putFile() - putFile previous work deleted and redownload " + key);
                        break;
                    }
                }
                try {
                    Thread.sleep(500);
                } catch (Exception e) {
                    return null;
                }
            }
        }

        File file = getDataFile(key);
        boolean error = (tmpFile == null);
        FileOutputStream fos = null;
        BitmapFactory.Options options = null;

        if (!error) {
            imageInfo = new ImageInfo(imageInfo);
            try {
                options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;

                BitmapFactory.decodeFile(tmpFile.getAbsolutePath(), options);
                if (options.outWidth <= 0 || options.outHeight <= 0) {
                    throw new IOException("Unable to decode bounds from newly downloaded image.");
                }
                imageInfo.setThumbWidth(options.outWidth);
                imageInfo.setThumbHeight(options.outHeight);
            } catch (Exception e) {
                error = true;
                Log.e(this, "putFile() - putFile Error for " + imageInfo + " : " + e.getMessage());
            }
        }

        if (!error) {
            if (!file.exists() || file.delete()) {
                try {
                    FileUtils.moveFile(tmpFile, file);
                } catch (IOException e) {
                    Log.e(this, "putFile() - putFile Error moving temp file for " + imageInfo + " : " + e.getMessage());
                    error = true;
                }
            } else {
                error = true;
            }
        }
        if (error) {
            if (tmpFile != null && tmpFile.exists()) {
                tmpFile.delete();
            }
            if (!file.exists()) {
                lckDeleteItemFromMap(key);
            }
            return null;
        }

        ObjectOutputStream objectOutputStream = null;
        try {
            fos = new FileOutputStream(getInfoFile(key));
            objectOutputStream = new ObjectOutputStream(fos);
            objectOutputStream.writeObject(imageInfo);
        } catch (Exception e) {
            Log.e(this, "putFile() - Exception : " + e.getMessage());
        } finally {
            IOUtils.closeQuietly(objectOutputStream);
            IOUtils.closeQuietly(fos);
        }

        FileImageInfo result = null;
        if (file != null) {
            Log.v(this, "putFile() - Adding to fileCache for " + imageInfo);
            result = lckPostUpdateThumbInfo(key, imageInfo, file);
        }

        return result;
    }

    /**
     * @param key
     * @return
     */
    private File getInfoFile(String key) {
        return new File(mBaseDir, "." + key + INFO_FILE_SUFFIX);
    }

    /**
     * @param key
     * @return
     */
    private File getDataFile(String key) {
        return new File(mBaseDir, key + DATA_FILE_SUFFIX);
    }

    public void remove(ImageInfo imageInfo) {
        String strKey = generateKey(imageInfo);
        deleteRelatedFile(strKey);
        lckDeleteItemFromMap(strKey);
    }

    /**
     *
     */
    public void clearCache() {
        try {
            lckClearAllItemsFromMap();
            FileUtils.cleanDirectory(getCacheDir());
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private String generateKey(ImageInfo imageInfo) {
        return generateKey(imageInfo.getContentId(), imageInfo.getMediaType(), imageInfo.getDeviceId());
    }

    private String generateKey(long contentId, int mediaType, long device_id) {
        return mediaType + "_" + contentId + "_" + device_id;
    }

    ////calculate directory size
    private long getThumbnailCacheDataSize() {
        long nTotalSize = 0;
        try {
            File file = new File(mBaseDir.getAbsolutePath());
            File[] childFileList = file.listFiles();
            if (childFileList == null) {
                return 0;
            }
            for (File childFile : childFileList) {
                if (childFile.isDirectory() == false) {
                    nTotalSize += childFile.length();
                }
            }
        } catch (Exception e) {
            Log.e(this, "getThumbnailCacheDataSize() - Exception error during getThumbnailCacheDataSize e=" + e);
        }
        return nTotalSize;
    }

    ////return true if over 15 minute
    public boolean checkOversizeCacheInterval() {
        long nCurTime = 0;
        nCurTime = System.currentTimeMillis();
        if (mLastTimeCacheCheck + INTERVAL_CACHESIZE_CHECK < nCurTime) {
            cleanOversizeCache();
            return true;
        } else {
            return false;
        }
    }

    ////reduce cache files if over 150MB, until 100MB watermark with LRU
    ////previous mfluent thumbnail cache logic calculate the directory, deleting everytime putFile
    ////->it makes many side effects->always failure loadImageFromCursor if over the MAX size
    public synchronized boolean cleanOversizeCache() {
        long nTotalSize = 0;
        int nLoopCount = 0;
        File file = null;
        long nStartTime = 0;
        long nCurTime = 0;
        String strKey = "";
        String strFileName;
        int nIndex;

        nCurTime = System.currentTimeMillis();
        mLastTimeCacheCheck = nCurTime;
        nTotalSize = getThumbnailCacheDataSize();
        if (nTotalSize < mMaxSize) {
            Log.i(this, "cleanOversizeCache() - no need to delete cache files nTotalSize=" + nTotalSize + ", mMaxSize=" + mMaxSize);
            return true;
        }
        Log.i(this, "cleanOversizeCache() - start job nTotalSize=" + nTotalSize + ", normalSize=" + mNormalSize);
        nStartTime = nCurTime;
        ////find victim and delete some
        try {
            file = new File(mBaseDir.getAbsolutePath());
            File[] childFileList = file.listFiles();
            if (childFileList == null) {
                Log.i(this, "cleanOversizeCache() - no thumbnail cache files return without clean");
                return true;
            }
            ////LRU list via filelist sorting
            Arrays.sort(childFileList, new Comparator<File>() {

                @Override
                public int compare(File f1, File f2) {
                    return Long.valueOf(f1.lastModified()).compareTo(f2.lastModified());
                }
            });
            for (File childFile : childFileList) {
                if (childFile.isDirectory() == false) {
                    strFileName = childFile.getName();
                    nTotalSize -= childFile.length();
                    childFile.delete();
                    nIndex = strFileName.indexOf(INFO_FILE_SUFFIX);
                    if (nIndex > 0) {
                        strKey = strFileName.substring(0, nIndex);
                        lckDeleteItemFromMap(strKey);
                    }
                }
                nLoopCount++;
                nCurTime = System.currentTimeMillis();
                if (nTotalSize <= mNormalSize || nCurTime > nStartTime + 10000) {
                    break;
                }
            }
        } catch (Exception e) {
            Log.e(this, "cleanOversizeCache() - Exception error during CheckCacheBackgroudJob e=" + e);
        }
        Log.i(this, "cleanOversizeCache() - reducing job done nLoopCount=" + nLoopCount + ", Size=" + nTotalSize);
        return true;
    }

}

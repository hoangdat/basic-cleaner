package platform.com.mfluent.asp.filetransfer;

/**
 * Created by sev_user on 2/25/2017.
 */
public class FileTransferAction {

    public static final String UPLOAD = "upload";

    public static final String DOWNLOAD = "download";

    public static final String CREATE_DIR = "create_dir";

    public static final String RESTORE = "restore";
}


package platform.com.mfluent.asp.framework;

import android.app.Activity;
import android.app.Application;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.content.LocalBroadcastManager;

import com.mfluent.asp.common.datamodel.CloudDevice;
import com.mfluent.log.Log;
import com.samsung.android.sdk.slinkcloud.CloudGatewayMediaStore;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

import platform.com.mfluent.asp.datamodel.filebrowser.CachedFileCheckService;
import platform.com.mfluent.asp.filetransfer.FileTransferManagerSingleton;
import platform.com.mfluent.asp.sync.CloudStorageSyncManager;
import platform.com.mfluent.asp.sync.MflNotificationManager;
import platform.com.mfluent.asp.sync.SyncManager;
import platform.com.mfluent.asp.util.BroadcastReceiverManager;
import platform.com.mfluent.asp.util.MFLStorageManagerSLPF;
import platform.com.mfluent.asp.util.NetworkUtilSLPF;
import platform.com.samsung.android.slinkcloud.HeatManager;
import uicommon.com.mfluent.asp.ServiceLocator;
import uicommon.com.mfluent.asp.util.CachedExecutorService;

/**
 * The main application.
 *
 * @author Ilan Klinghofer
 */
public class IASPApplication2 extends Application implements Application.ActivityLifecycleCallbacks {
    private static final String TAG = "IASPApplication2";

    // TODO: MUST FLIP THIS TO true FOR PRODUCTION BUILD
    public static final boolean PRODUCTION_BUILD = false;
    public static final String PREFERENCES_NAME = "slpf_pref_20";
    private static final String DONT_MANAGE_RECEIVER = "com.mfluent.asp.ASPApplication.DONT_MANAGE_RECEIVER";

    // for GoogleDrive Auth
    public static boolean isInGoogleDriveUserRecovering = false;

    public static final String BROADCAST_MASTER_RESET = IASPApplication2.class.getName() + "_BROADCAST_MASTER_RESET_CLOUDS";
    public static final String BROADCAST_MASTER_RESET_COMPLETE = IASPApplication2.class.getName() + "_BROADCAST_MASTER_RESET_COMPLETE_CLOUDS";
    public static final String BROADCAST_GO_HOME = IASPApplication2.class.getName() + "_BROADCAST_GO_HOME_CLOUDS";

    public static final String Name_bShouldCloudSelectionPopupDisplayedAutomatically = "name_bShouldCloudSelectionPopupDisplayedAutomatically";
    public static final String Key_bShouldCloudSelectionPopupDisplayedAutomatically = "key_bShouldCloudSelectionPopupDisplayedAutomatically";

    // Display name that is used by About page
    // This value will be set from VERSION_FILENAME in assets folder
    private final String version = "0";

    /**
     * This boolean value will act as a gateway in the initSyncStart method so that it is only executed
     * once per running of the application. It will be cleared on master reset.
     */
    private volatile boolean mInit1Ok = false;
    private volatile boolean mInitialize2Ready = false;

    public static boolean isAppInitDone = false;

    public static boolean sKeepAliveServiceFromNativeApp = false;
    public static boolean sInstanceExist = false;
    public static final String SERVICE_ON_CREATE_INTENT = IASPApplication2.class.getName() + "_SERVICE_ON_CREATE_INTENT";

    private static final int MATCH_DISABLED_COMPONENTS = 0x00000200; // PackageManager.GET_DISABLED_COMPONENTS - This constant was deprecated in API level 24. replaced with MATCH_DISABLED_COMPONENTS

    public static void traceStack() {
        try {
            throw new Exception("trace stack (no bug, just logging...)");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isTablet() {
        return (getResources().getConfiguration().smallestScreenWidthDp >= 580);
    }

    public IASPApplication2() {
        ServiceLocatorSLPF.bind(this, IASPApplication2.class);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        ServiceLocator.bind(this, IASPApplication2.class);
    }

    private void startNullDeviceCheck() {
        Handler some = new Handler(getMainLooper());
        some.postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    CachedExecutorService.getInstance().submit(new Runnable() {
                        @Override
                        public void run() {
                            CloudStorageSyncManager.checkNoDeviceStatus();
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, 4567);
    }

    public String getVersion() {
        return this.version;
    }

    public boolean initialize1() {
        if (mInit1Ok)
            return true;
        mInit1Ok = true;
        Log.i(this, "Platform ASPApplication2 ::initialize1 start");
        long startTimeMs = System.currentTimeMillis();
        if (wasInitSyncStart()) {
            enableApplicationReceivers();
        } else {
            resetApplicationReceivers();
        }
        Context aspUiCommonContext = ServiceLocator.getWithClassName(IASPApplication2.class, "ASPApplication");
        if (aspUiCommonContext == null) {
            ////when two-apk mode, need to register this class as ASPApplication.class due to ASPUiCommon
            ServiceLocator.bind(this, IASPApplication2.class);
        }
        ServiceLocatorSLPF.bind(this, IASPApplication2.class);

        // Get the HeatManager so that it starts listening
        HeatManager.getInstance(this);
        getExternalFilesDir(null);

        //This dummy call will cause our ASPMediaStoreProvider to be instantiated and initialized. This, in turn causes
        //the DataModel class to be initialized.
//		if (true == CloudGatewaySignInUtils.getInstance(this).isPlatformEnabled()) {
//			this.getContentResolver().call(ASPMediaStore.CallMethods.CONTENT_URI, "", null, null);
//			Log.e(TAG, "this is enabled");
//		} else {
//			Log.w(TAG, "this platform is not enabled, ignore dummy call");
//		}

        ////PLM issues related to when platform is disabled, dummy call make FC
        ////it has a problem in DataModel.getInstance()...
        try {
            ServiceLocatorSLPF.bind(new MflNotificationManager(), MflNotificationManager.class);
            ServiceLocatorSLPF.bind(new MFLStorageManagerSLPF(getApplicationContext()), MFLStorageManagerSLPF.class);

            FileTransferManagerSingleton.getInstance(this).cleanupCacheFiles();

            startService(new Intent(this, CloudManagerMainService.class));

            registerActivityLifecycleCallbacks(this);
        } catch (Exception e) {
            e.printStackTrace();
            return true;
        }
        Log.i(this, "initialize1: ASPApplication initialize1() DONE: " + (System.currentTimeMillis() - startTimeMs) + "ms");
        isAppInitDone = true;
        CachedFileCheckService.startScheduler();
        startNullDeviceCheck();
        return true;
    }

    /**
     * initSyncStart() must be called on the main thread or it will throw an IllegalStateException
     */
    public void initSyncStart() {
        if (this.mInitialize2Ready == false) {
            try {
                if (Looper.getMainLooper() != Looper.myLooper()) {
                    throw new IllegalStateException("Only call initSyncStart() on the main thread.s");
                }
            } catch (NoSuchMethodError e) {
                Log.e(this, "ASPApplication exception : " + e.getMessage());
            }
            this.mInitialize2Ready = true;

            BroadcastReceiverManager manager = BroadcastReceiverManager.getInstance(this);
            try {
                manager.initTelephonyManager(this);
            } catch (Exception e) {
                e.printStackTrace();
            }
            registerReceiver(manager.getBroadcastReceiver(), manager.getNetworkIntentFilter());

            enableApplicationReceivers();
            Log.d(this, "start up  SyncManager...");
            SyncManager.getInstance(this).start(new Intent(SERVICE_ON_CREATE_INTENT));
            Log.d(this, "end up  SyncManager...");

            // Send the GO HOME broadcast that will start up the delayed app components
            Intent goHomeIntent = new Intent(IASPApplication2.BROADCAST_GO_HOME);
            goHomeIntent.putExtra(CloudDevice.REFRESH_FROM_KEY, CloudDevice.REFRESH_FROM_LAUNCH);
            LocalBroadcastManager.getInstance(this).sendBroadcast(goHomeIntent);
        }
    }

    public boolean wasInitSyncStart() {
        return mInitialize2Ready;
    }

    public SharedPreferences getSharedPreferences() {
        return getSharedPreferences(IASPApplication2.PREFERENCES_NAME, Context.MODE_PRIVATE);
    }

    private void enableApplicationReceivers() {
        setApplicationReceiversEnabledState(PackageManager.COMPONENT_ENABLED_STATE_ENABLED);
    }

    private void resetApplicationReceivers() {
        setApplicationReceiversEnabledState(PackageManager.COMPONENT_ENABLED_STATE_DEFAULT);
    }

    private void setApplicationReceiversEnabledState(final int receiverState) {
        Log.i(this, "setApplicationReceiversEnabledState: entered with state = " + receiverState);

        // Get all the receivers in the AndroidManifest
        PackageManager pm = getPackageManager();
        PackageInfo packageInfo = null;
        try {
            packageInfo = pm.getPackageInfo(getPackageName(), PackageManager.GET_RECEIVERS
                    | MATCH_DISABLED_COMPONENTS
                    | PackageManager.GET_META_DATA);
        } catch (NameNotFoundException e) {
            // This should never happen!!!
            Log.e(this, "Failed to find our app's package: " + getPackageName());
        }

        if (packageInfo == null || packageInfo.receivers == null) {
            // This shouldn't happen unless all the receivers have been removed from the app (which is unlikely)
            Log.e(this, "setApplicationReceiversEnabledState: Quitting without setting enabled state. No receivers found.");
            return;
        }

        for (ActivityInfo info : packageInfo.receivers) {
            if (shouldManageReceiverEnabledState(info)) {
                ComponentName componentName = new ComponentName(this, info.name);
                pm.setComponentEnabledSetting(componentName, receiverState, PackageManager.DONT_KILL_APP);
            }
        }
    }

    /**
     * @param info The receiver's activity info (with metadata retrieved)
     * @return
     */
    private boolean shouldManageReceiverEnabledState(ActivityInfo info) {
        // Right now there's only one exception, com.samsung.android.sdk.slinkcloud.uploader.UploaderReceiver.
        return info.metaData == null || !info.metaData.containsKey(DONT_MANAGE_RECEIVER);
    }

    public String getContentOfFileInAsset(String filename) {
        String fileContent = null;
        try {
            Log.v(this, "::getFileContentFromAsset: filename : " + filename);
            InputStream is = getAssets().open(filename);

            int bufSize = 1024;
            byte[] buf = new byte[bufSize];
            ByteArrayOutputStream outString = new ByteArrayOutputStream();

            int readCnt = 0;
            while ((readCnt = is.read(buf)) != -1) {
                outString.write(buf, 0, readCnt);
            }

            fileContent = new String(outString.toByteArray());
            Log.v(this, "::getFileContentFromAsset:" + "[file content] : \n" + fileContent);
            is.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return fileContent;
    }

    public static boolean checkNeedWifiOnlyBlock() {
        SharedPreferences prefs = ServiceLocatorSLPF.get(IASPApplication2.class).getSharedPreferences(IASPApplication2.PREFERENCES_NAME, Activity.MODE_PRIVATE);
        boolean bRet = prefs.getBoolean(CloudGatewayMediaStore.CallMethods.SetWifiOnly.RET_ENABLE_BOOLEAN, true);
        if (bRet) {
            bRet = NetworkUtilSLPF.isDataNetworkAvailable() == true && NetworkUtilSLPF.isWiFiConnected() == false;
        }
        Log.i(TAG, "checkNeedWifiOnlyBlock " + bRet);
        return bRet;
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle bundle) {

    }

    @Override
    public void onActivityStarted(Activity activity) {

    }

    @Override
    public void onActivityResumed(Activity activity) {

    }

    @Override
    public void onActivityPaused(Activity activity) {

    }

    @Override
    public void onActivityStopped(Activity activity) {

    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {

    }
}


package platform.com.mfluent.asp.framework.AuthDelegate;

import platform.com.mfluent.asp.datamodel.DataModelSLPF;
import platform.com.mfluent.asp.datamodel.DeviceSLPF;
import platform.com.mfluent.asp.framework.IASPApplication2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;

import com.mfluent.asp.common.datamodel.CloudDevice;
import com.mfluent.log.Log;

public class GoogleDriveAuthActivity extends Activity {

    public static final String MSG_IN_BUNDLE = "message";
    public static final String INTENT_IN_BUNDEL = "intent";
    public static final String BUNDLE_IN_INTENT_EXTRA = "bundle";

    static final int REQUEST_AUTHORIZATION = 1000;
    private int mDeviceId = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        Log.i(this, "onCreate(), I got the intent [ " + intent + " ]");
        Bundle bundle = intent.getBundleExtra(BUNDLE_IN_INTENT_EXTRA);
        if (bundle != null) {
            String message = bundle.getString(MSG_IN_BUNDLE);
            mDeviceId = bundle.getInt(CloudDevice.DEVICE_ID_EXTRA_KEY);
            intent = (Intent) bundle.getParcelable(INTENT_IN_BUNDEL);
            Log.i(this, "onCreate(), device id : " + mDeviceId);

            if (message != null) {
                if (message.equals("UserRecoverableAuthIOException")) {
                    startActivityForResult(intent, REQUEST_AUTHORIZATION);
                }
            }
        }

        return;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_AUTHORIZATION) {
            Log.i(this, "onActivityResult() REQUEST_AUTHORIZATION, resultCode = " + resultCode);
            IASPApplication2.isInGoogleDriveUserRecovering = false;
            if (resultCode == 0) {  // user pressed cancel button on the permission request pop-up
                DeviceSLPF device = DataModelSLPF.getInstance().getDeviceById(mDeviceId);
                if (device != null) {
                    device.delete();
                }
            } else {
                Intent intent = new Intent(DeviceSLPF.BROADCAST_DEVICE_STATE_CHANGE);
                intent.putExtra(CloudDevice.DEVICE_ID_EXTRA_KEY, mDeviceId);
                LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
                mDeviceId = 0;
            }
            finish();
        }
    }
}

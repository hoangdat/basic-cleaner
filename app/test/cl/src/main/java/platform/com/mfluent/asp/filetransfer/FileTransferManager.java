/**
 *
 */

package platform.com.mfluent.asp.filetransfer;

import com.samsung.android.sdk.slinkcloud.CloudGatewayFileTransferUtils.TransferOptions;
import com.samsung.android.sdk.slinkcloud.CloudGatewayMediaSet;

import java.util.Collection;

import platform.com.mfluent.asp.datamodel.DeviceSLPF;
import platform.com.mfluent.asp.dws.SaveFilesSession;

/**
 * @author Ilan Klinghofer
 */
public interface FileTransferManager {

    String CANCEL = "com.mfluent.asp.filetransfer.FileTransferManager.CANCELED";
    String RETRY = "com.mfluent.asp.filetransfer.FileTransferManager.RETRY";

    String TRANSFER_COMPLETED = "com.mfluent.asp.filetransfer.FileTransferManager.TRANSFER_COMPLETED";
    String TRANSFER_COMPLETE_DEVICE_ID = "com.mfluent.asp.filetransfer.FileTransferManager.TRANSFER_COMPLETE_DEVICE_ID";

    void transfer(CloudGatewayMediaSet mediaSet, DeviceSLPF targetDevice, TransferOptions options, String strPremadeSession);

    void cancel(String sessionId, boolean isDeleteSessionAfterCancel);

    void cancelAll(boolean isDeleteSessionAfterCancel);

    void cancelAllForDevice(DeviceSLPF device);

    void cancelAllForAutoUpload();

    boolean isTransferInProgress();

    void cleanupCacheFiles();

    FileTransferTask retry(String sessionId);

    Collection<SaveFilesSession> getSaveFilesSessions();

    int getActiveSaveFilesSessionsCount();

    boolean checkCancelable(String sessionId);
}

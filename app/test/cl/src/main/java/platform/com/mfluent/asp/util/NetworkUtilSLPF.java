
package platform.com.mfluent.asp.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;

import com.mfluent.log.Log;

import platform.com.mfluent.asp.framework.IASPApplication2;
import platform.com.mfluent.asp.framework.ServiceLocatorSLPF;

public class NetworkUtilSLPF {

    private static final String TAG = "NetworkUtil";

    public static boolean isWiFiConnected() {
        Context context = ServiceLocatorSLPF.get(IASPApplication2.class);
        WifiManager myWifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        if (myWifiManager.isWifiEnabled()) {
            WifiInfo myWifiInfo = myWifiManager.getConnectionInfo();
            Log.d(TAG, "isWiFiConnected: myWifiInfo: " + myWifiInfo + ", isWifiEnabled:" + myWifiManager.isWifiEnabled());
            if (myWifiInfo != null && myWifiInfo.getNetworkId() >= 0) {
                return true;
            } else {
                Log.d(TAG, "isWiFiConnected: no network id, wifi not connected.");
            }
        } else {
            Log.d(TAG, "isWiFiConnected: wifi is not enabled.");
        }
        return false;
    }

    /**
     * detect if any data connection is available
     * NOTE: does not detect low signal strength, etc. To know for sure, try to connect somewhere.
     */
    public static boolean isNetworkAvailable() {
        Context context = ServiceLocatorSLPF.get(IASPApplication2.class);
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = connectivityManager.getActiveNetworkInfo();
        return (netInfo != null && netInfo.isConnectedOrConnecting());
    }

    /**
     * Indicates whether network connectivity is possible. A network is unavailable when a persistent or semi-persistent condition prevents the possibility of
     * connecting to that network.
     */
    public static boolean isDataNetworkAvailable() {
        Context context = ServiceLocatorSLPF.get(IASPApplication2.class);
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = connectivityManager.getActiveNetworkInfo();
        return (netInfo != null && netInfo.isAvailable() && netInfo.getType() == ConnectivityManager.TYPE_MOBILE);
    }
}

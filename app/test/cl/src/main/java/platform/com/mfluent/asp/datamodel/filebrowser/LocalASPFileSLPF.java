/**
 *
 */

package platform.com.mfluent.asp.datamodel.filebrowser;

import java.io.File;

/**
 * @author Ilan Klinghofer
 */
public class LocalASPFileSLPF extends ASPFileWithSpecialType {

    private final File file;

    public LocalASPFileSLPF(File file) {
        this.file = file;
    }

    public File getFile() {
        return this.file;
    }

    /*
     * (non-Javadoc)
     * @see com.mfluent.asp.datamodel.ASPFile#getName()
     */
    @Override
    public String getName() {
        return this.file.getName();
    }

    /*
     * (non-Javadoc)
     * @see com.mfluent.asp.datamodel.ASPFile#isDirectory()
     */
    @Override
    public boolean isDirectory() {
        return this.file.isDirectory();
    }

    /*
     * (non-Javadoc)
     * @see com.mfluent.asp.datamodel.ASPFile#length()
     */
    @Override
    public long length() {
        return this.file.length();
    }

    /*
     * (non-Javadoc)
     * @see com.mfluent.asp.datamodel.ASPFile#lastModified()
     */
    @Override
    public long lastModified() {
        return this.file.lastModified();
    }

    @Override
    public String toString() {
        return "LocalASPFile: " + (isDirectory() ? "DIR: " + getName() : "FILE: " + getName() + ", size: " + length());
    }

}

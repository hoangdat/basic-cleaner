
package platform.com.mfluent.asp.util.bitmap;

import java.util.LinkedList;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import android.util.Pair;
import com.mfluent.log.Log;

import platform.com.mfluent.asp.util.bitmap.ImageWorker.LoadTask;

/**
 * The Class ImageWorkerQueue.
 *
 * @author MFluent
 * @version 1.5
 */
public class ImageWorkerQueue {

    /**
     * The Class PriorityQueue.
     *
     * @author MFluent
     * @version 1.5
     */
    @SuppressWarnings("serial")
    private static class PriorityQueue extends LinkedList<LoadTask> {

    }

    private final PriorityQueue[] mQueues;
    private final ReentrantLock mLock;
    private final Condition mCondition;

    /**
     * Instantiates a new image worker queue.
     *
     * @param numPriorities   the num priorities
     */
    public ImageWorkerQueue(int numPriorities) {
        this.mQueues = new PriorityQueue[numPriorities];
        for (int i = 0; i < numPriorities; ++i) {
            this.mQueues[i] = new PriorityQueue();
        }
        this.mLock = new ReentrantLock();
        this.mCondition = this.mLock.newCondition();
    }

    /**
     * Gets the.
     *
     * @return the load task
     * @throws InterruptedException the interrupted exception
     */
    public LoadTask get() throws InterruptedException {
        LoadTask result = null;

        this.mLock.lockInterruptibly();
        try {
            while (result == null) {
                for (int i = 0; i < this.mQueues.length; ++i) {
                    if (this.mQueues[i].size() > 0) {
                        result = this.mQueues[i].removeFirst();
                        break;
                    }
                }

                if (result == null) {
                    this.mCondition.await();
                }
            }
        } finally {
            this.mLock.unlock();
        }

        return result;
    }

    /**
     * Put.
     *
     * @param task     the task
     * @param priority the priority
     */
    public void put(LoadTask task, int priority) {
        this.mLock.lock();
        try {
            Pair<Integer, Integer> found = find(task);

            if ((found.first < 0) || (found.second < 0) || (found.first != priority)) {
                boolean insertLast = true;
                if (found.first >= 0) {

                    if (found.first < priority) {
                        insertLast = false;
                    }
                    LoadTask oldTask = this.mQueues[found.first].get(found.second);
                    this.mQueues[found.first].remove(found.second);
                    Log.v(this, "Put with different priority. Removed:" + oldTask + " oldPriority:" + found.first);
                }

                if (insertLast) {
                    this.mQueues[priority].addLast(task);
                } else {
                    this.mQueues[priority].addFirst(task);
                }

                task.setPriority(priority);
                Log.v(this, "Put task: " + task + " priority:" + priority + ", queue size: " + this.mQueues[priority].size());
                this.mCondition.signal();
            } else {
                LoadTask oldTask = this.mQueues[found.first].set(found.second, task);
                Log.v(this, "Replaced same priority task: " + task + " oldTask: " + oldTask + " priority:" + priority);
                task.setPriority(priority);
            }
        } finally {
            this.mLock.unlock();
        }
    }

    /**
     * Find.
     *
     * @param task the task
     * @return the pair
     */
    private Pair<Integer, Integer> find(LoadTask task) {
        int foundQueue = -1;
        int foundPos = -1;

        for (int i = 0; i < this.mQueues.length; ++i) {
            int lookup = this.mQueues[i].indexOf(task);
            if (lookup >= 0) {
                foundQueue = i;
                foundPos = lookup;
                break;
            }
        }

        return new Pair<Integer, Integer>(foundQueue, foundPos);
    }

    /**
     * Removes the.
     *
     * @param task the task
     * @return true, if successful
     */
    public boolean remove(LoadTask task) {
        boolean result = false;

        this.mLock.lock();
        try {
            for (int i = 0; i < this.mQueues.length; ++i) {
                if (this.mQueues[i].remove(task)) {
                    result = true;
                    break;
                }
            }
        } finally {
            this.mLock.unlock();
        }

        return result;
    }
}


package platform.com.mfluent.asp.util;

import android.os.Looper;

import com.mfluent.log.Log;

import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class AspFileUtilsSLPF {
    private static String TAG = "AspFileUtilsSLPF";

    private static final char EXTENSION_SEPARATOR = '.';
    private static final char UNIX_SEPARATOR = '/';
    private static final char WINDOWS_SEPARATOR = '\\';

    public static int indexOfLastSeparator(String filename) {
        if (filename == null) {
            return -1;
        }
        int lastUnixPos = filename.lastIndexOf(UNIX_SEPARATOR);
        int lastWindowsPos = filename.lastIndexOf(WINDOWS_SEPARATOR);
        return Math.max(lastUnixPos, lastWindowsPos);
    }

    public static int indexOfExtension(String filename) {
        if (filename == null) {
            return -1;
        }
        int extensionPos = filename.lastIndexOf(EXTENSION_SEPARATOR);
        int lastSeparator = indexOfLastSeparator(filename);
        return lastSeparator > extensionPos ? -1 : extensionPos;
    }

    public static String getExtension(String filename) {
        if (filename == null) {
            return null;
        }
        int index = indexOfExtension(filename);
        if (index == -1) {
            return "";
        } else {
            return filename.substring(index + 1);
        }
    }

    public static String removeExtension(String filename) {
        if (filename == null) {
            return null;
        }
        int index = indexOfExtension(filename);
        if (index == -1) {
            return filename;
        } else {
            return filename.substring(0, index);
        }
    }

    public static String getName(String filename) {
        if (filename == null) {
            return null;
        }
        int index = indexOfLastSeparator(filename);
        return filename.substring(index + 1);
    }

    public static String getBaseName(String filename) {
        return removeExtension(getName(filename));
    }


    private static boolean isSeparator(char ch) {
        return ch == UNIX_SEPARATOR || ch == WINDOWS_SEPARATOR;
    }

    public static int getPrefixLength(String filename) {
        if (filename == null) {
            return -1;
        }
        int len = filename.length();
        if (len == 0) {
            return 0;
        }
        char ch0 = filename.charAt(0);
        if (ch0 == ':') {
            return -1;
        }
        if (len == 1) {
            if (ch0 == '~') {
                return 2;  // return a length greater than the input
            }
            return isSeparator(ch0) ? 1 : 0;
        } else {
            if (ch0 == '~') {
                int posUnix = filename.indexOf(UNIX_SEPARATOR, 1);
                int posWin = filename.indexOf(WINDOWS_SEPARATOR, 1);
                if (posUnix == -1 && posWin == -1) {
                    return len + 1;  // return a length greater than the input
                }
                posUnix = posUnix == -1 ? posWin : posUnix;
                posWin = posWin == -1 ? posUnix : posWin;
                return Math.min(posUnix, posWin) + 1;
            }
            char ch1 = filename.charAt(1);
            if (ch1 == ':') {
                ch0 = Character.toUpperCase(ch0);
                if (ch0 >= 'A' && ch0 <= 'Z') {
                    if (len == 2 || isSeparator(filename.charAt(2)) == false) {
                        return 2;
                    }
                    return 3;
                }
                return -1;

            } else if (isSeparator(ch0) && isSeparator(ch1)) {
                int posUnix = filename.indexOf(UNIX_SEPARATOR, 2);
                int posWin = filename.indexOf(WINDOWS_SEPARATOR, 2);
                if (posUnix == -1 && posWin == -1 || posUnix == 2 || posWin == 2) {
                    return -1;
                }
                posUnix = posUnix == -1 ? posWin : posUnix;
                posWin = posWin == -1 ? posUnix : posWin;
                return Math.min(posUnix, posWin) + 1;
            } else {
                return isSeparator(ch0) ? 1 : 0;
            }
        }
    }

    public static String getPrefix(String filename) {
        if (filename == null) {
            return null;
        }
        int len = getPrefixLength(filename);
        if (len < 0) {
            return null;
        }
        if (len > filename.length()) {
            return filename + UNIX_SEPARATOR;  // we know this only happens for unix
        }
        return filename.substring(0, len);
    }

    private static String doGetFullPath(String filename, boolean includeSeparator) {
        if (filename == null) {
            return null;
        }
        int prefix = getPrefixLength(filename);
        if (prefix < 0) {
            return null;
        }
        if (prefix >= filename.length()) {
            if (includeSeparator) {
                return getPrefix(filename);  // add end slash if necessary
            } else {
                return filename;
            }
        }
        int index = indexOfLastSeparator(filename);
        if (index < 0) {
            return filename.substring(0, prefix);
        }
        int end = index + (includeSeparator ? 1 : 0);
        if (end == 0) {
            end++;
        }
        return filename.substring(0, end);
    }

    public static String getFullPath(String filename) {
        return doGetFullPath(filename, true);
    }

    public static File moveFile(File tmpFile, File targetFile, boolean needRename) throws IOException {
        if (targetFile.exists() && needRename) {
            String filename = targetFile.getAbsolutePath();
            String baseName = getBaseName(filename);
            String extension = getExtension(filename);
            String targetDirectory = getFullPath(filename);
            String suffix;
            if (StringUtils.isEmpty(extension)) {
                suffix = "";
            } else {
                suffix = "." + extension;
            }

            // NOTE: ASP PC client saves duplicates as "name(x)" where x starts from 1
            int copyNumber = 1;
            while (targetFile.exists()) {
                targetFile = new File(targetDirectory, baseName + "(" + copyNumber + ")" + suffix);
                copyNumber++;
            }
        }

        Log.d(TAG, "Moving " + tmpFile.getAbsolutePath() + " to " + targetFile.getAbsolutePath());

        //Doing this instead of calling FileUtils.moveFile since on Android that method fails for files larger than MAX_INT (roughly 2 GB)
        boolean rename = tmpFile.renameTo(targetFile);
        if (!rename) {
            AspFileUtilsSLPF.copyFile(tmpFile, targetFile);
            if (!tmpFile.delete()) {
                FileUtils.deleteQuietly(targetFile);
                throw new IOException("Failed to delete original file '" + tmpFile + "' after copy to '" + targetFile + "'");
            }
        }

        return targetFile;
    }

    /**
     * Copies a file to a new location preserving the file date.
     * <p/>
     * This method copies the contents of the specified source file to the specified destination file. The directory holding the destination file is created if
     * it does not exist. If the destination file exists, then this method will overwrite it.
     * <p/>
     * <strong>Note:</strong> This method tries to preserve the file's last modified date/times using {@link File#setLastModified(long)}, however it is not
     * guaranteed that the operation will succeed. If the modification operation fails, no indication is provided.
     *
     * @param srcFile  an existing file to copy, must not be {@code null}
     * @param destFile the new file, must not be {@code null}
     * @throws NullPointerException if source or destination is {@code null}
     * @throws IOException          if source or destination is invalid
     * @throws IOException          if an IO error occurs during copying
     */
    public static void copyFile(File srcFile, File destFile) throws IOException {
        Log.d(TAG, "Copying " + srcFile.getAbsolutePath() + " to " + destFile.getAbsolutePath());

        FileInputStream fis = null;
        FileOutputStream fos = null;
        try {

            String parentName = destFile.getParent();
            File parentDir = new File(parentName);

            Log.d(TAG, "parentName " + parentName + ", parentDir is exists? " + parentDir.exists());

            if (!parentDir.exists()) {
                parentDir.mkdirs();
            }

            ////auto retry file transfer
            fis = AspFileUtilsSLPF.openFileInputStreamWithRetry(srcFile);
            fos = AspFileUtilsSLPF.openFileOutputStreamWithRetry(destFile, false);
            //fis = new FileInputStream(srcFile);
            //fos = new FileOutputStream(destFile);

            IOUtils.copy(fis, fos);
        } finally {
            IOUtils.closeQuietly(fos);
            IOUtils.closeQuietly(fis);
        }

        if (srcFile.length() != destFile.length()) {
            throw new IOException("Failed to copy full contents from '" + srcFile + "' to '" + destFile + "'");
        }
        destFile.setLastModified(srcFile.lastModified());
    }

    public static void deleteRecursive(File fileOrDirectory) {
        AspFileUtilsSLPF.deleteChildrenRecursive(fileOrDirectory);
        fileOrDirectory.delete();
    }

    public static void deleteChildrenRecursive(File fileOrDirectory) {
        if (fileOrDirectory.isDirectory()) {
            File[] fileArr = fileOrDirectory.listFiles();
            if (fileArr != null) {
                for (File child : fileArr) {
                    AspFileUtilsSLPF.deleteRecursive(child);
                }
            }
        }
    }

    ////auto retry file transfer
    public static FileInputStream openFileInputStreamWithRetry(File objFile) throws FileNotFoundException {
        FileInputStream inputFile = null;
        int nRetryCnt;
        if (objFile == null) {
            throw new FileNotFoundException();
        }
        for (nRetryCnt = 0; nRetryCnt < 3; nRetryCnt++) {
            try {
                inputFile = new FileInputStream(objFile);
                if (inputFile != null) {
                    break;
                }
            } catch (FileNotFoundException e) {
                if (Looper.myLooper() != Looper.getMainLooper()) {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e1) {
                        break;
                    }
                }
            }
        }
        if (nRetryCnt >= 3) {
            throw new FileNotFoundException();
        }
        return inputFile;
    }

    ////auto retry file transfer
    public static FileOutputStream openFileOutputStreamWithRetry(File objFile, boolean bAppend) throws FileNotFoundException {
        FileOutputStream outputFile = null;
        int nRetryCnt;
        if (objFile == null) {
            throw new FileNotFoundException();
        }
        for (nRetryCnt = 0; nRetryCnt < 3; nRetryCnt++) {
            try {
                outputFile = new FileOutputStream(objFile, bAppend);
                if (outputFile != null) {
                    break;
                }
            } catch (FileNotFoundException e) {
                if (Looper.myLooper() != Looper.getMainLooper()) {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e1) {
                        break;
                    }
                }
            }
        }
        if (nRetryCnt >= 3) {
            throw new FileNotFoundException();
        }
        return outputFile;
    }

    ////auto retry file transfer
    public static long getFileSizeWithRetry(File objFile) {
        if (objFile == null) {
            return 0;
        }
        long nLength = objFile.length();
        if (nLength <= 0) {
            for (int nRetryCnt = 0; nRetryCnt < 3; nRetryCnt++) {
                try {
                    nLength = objFile.length();
                    if (nLength > 0) {
                        break;
                    }
                } catch (Exception e) {
                    if (Looper.myLooper() != Looper.getMainLooper()) {
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e1) {
                            break;
                        }
                    }
                }
            }
        }
        return nLength;
    }

    ////auto retry file transfer
    public static boolean checkDuplicateFile(File tmpFile, File targetFile) {
        boolean bRet = false;
        if (targetFile.exists()) {
            if (tmpFile.length() == targetFile.length()) {
                bRet = true;
            }
        }
        return bRet;
    }
}

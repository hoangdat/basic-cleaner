
package platform.com.mfluent.asp.sync;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.concurrent.locks.ReentrantLock;

import platform.com.mfluent.asp.datamodel.DataModelSLPF;
import platform.com.mfluent.asp.datamodel.DeviceSLPF;
import platform.com.mfluent.asp.framework.IASPApplication2;
import platform.com.mfluent.asp.framework.ServiceLocatorSLPF;
import platform.com.mfluent.asp.media.AspThumbnailCache;
import platform.com.mfluent.asp.util.bitmap.ImageWorker;
import platform.com.mfluent.asp.util.bitmap.ImageWorker.ImageWorkerClient;
import uicommon.com.mfluent.asp.util.bitmap.BitmapInfo;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Point;
import android.os.Looper;
import android.util.DisplayMetrics;

import com.mfluent.asp.common.datamodel.ASPMediaStore;
import com.mfluent.asp.common.datamodel.ASPMediaStore.Files.FileColumns;
import com.mfluent.asp.common.datasource.AspMediaId;
import com.mfluent.asp.common.media.thumbnails.ImageInfo;
import com.mfluent.asp.common.util.CursorUtils;
import com.mfluent.log.Log;

public class ASPThumbnailPrefetcherService extends IntentService {

    public static final String ACTION_PREFETCH_DEVICE = "com.mfluent.asp.sync.ASPThumbnailPrefetcherService.ACTION_PREFETCH_DEVICE";

    public static final String EXTRA_DEVICE_ID = "DEVICE_ID";
    public static final String EXTRA_TRIGGER_BY_MULTIGETTING_API = "TRIGGER_BY_MULTIGETTING_API";
    public static final String EXTRA_CONTENTID_LIST = "CONTENTID_LIST";

    ////test for PoC
    private static int NUM_PREFETCH_PER_DEVICE = 30;

    private static class DummyImageWorkerClient implements ImageWorkerClient {

        private Object mTag;
        private final ImageInfo mImageInfo;
        boolean mbNeedBroadcastResult;
        Context context = null;

        public DummyImageWorkerClient(ImageInfo imageInfo, boolean bNeedBroadcastReport) {
            mImageInfo = imageInfo;
            mbNeedBroadcastResult = bNeedBroadcastReport;
        }

        @Override
        public void setImageAndOrientation(BitmapInfo bitmapInfo, int orientation, ImageInfo request) {
            removeFromPendingRequests();
            //Log.i("ATP", "setImageAndOrientation = " + bitmapInfo.getImageInfo().getContentId());
            if (mbNeedBroadcastResult)
                sendBroadcastResult(true);
        }

        private void sendBroadcastResult(boolean bSuccess) {
            if (context == null) {
                IASPApplication2 app = ServiceLocatorSLPF.get(IASPApplication2.class);
                if (app != null) {
                    context = app.getApplicationContext();
                }
            }
            if (context != null) {
                Intent brResult = new Intent("com.samsung.android.slinkcloud.multigetting");
                brResult.putExtra("RESULT", bSuccess);
                brResult.putExtra("NAME", "" + mImageInfo.getMediaType() + "_" + mImageInfo.getContentId());
                Log.i(this, "send report id = " + mImageInfo.getContentId() + ",result=" + bSuccess);
                context.sendBroadcast(brResult);
            }
        }

        private void removeFromPendingRequests() {
            ASPThumbnailPrefetcherService.mLock.lock();
            try {
                if (ASPThumbnailPrefetcherService.mPendingRequests.get(mImageInfo) == this) {
                    ASPThumbnailPrefetcherService.mPendingRequests.remove(mImageInfo);
                }
            } finally {
                ASPThumbnailPrefetcherService.mLock.unlock();
            }
        }

        @Override
        public void setTag(int key, Object object) {
            mTag = object;
        }

        @Override
        public Object getTag(int key) {
            return mTag;
        }

        @Override
        public void onImageFailedToLoad() {
            removeFromPendingRequests();
            if (mbNeedBroadcastResult)
                sendBroadcastResult(false);
        }

        @Override
        public Point getMaximumBitmapSize() {
            return null;
        }

    }

    private ImageWorker mImageWorker;
    private static final ReentrantLock mLock = new ReentrantLock();
    private static final HashMap<ImageInfo, DummyImageWorkerClient> mPendingRequests = new HashMap<ImageInfo, DummyImageWorkerClient>(NUM_PREFETCH_PER_DEVICE);
    private boolean mDestroyed = false;
    private int mDownloadDimension;

    public ASPThumbnailPrefetcherService() {
        super("ASPThumbnailPrefetcher");
    }

    @Override
    public void onCreate() {
        super.onCreate();

        this.mImageWorker = new ImageWorker(this, Looper.myLooper());
        this.mDestroyed = false;

        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        this.mDownloadDimension = Math.max(displayMetrics.heightPixels, displayMetrics.widthPixels);
        this.mDownloadDimension = Math.min(1280, this.mDownloadDimension);

    }

    @Override
    public void onDestroy() {
        mDestroyed = true;
        super.onDestroy();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        boolean bTriggerByMultiGetting = false;
        String[] idList = null;
        if(intent == null)
            return;
        if (!ACTION_PREFETCH_DEVICE.equals(intent.getAction())) {
            Log.w(this, "Unknown intent action: " + intent.getAction());
            return;
        }
        DeviceSLPF device = DataModelSLPF.getInstance().getDeviceById(intent.getIntExtra(EXTRA_DEVICE_ID, 0));
        if (device == null) {
            Log.w(this, "Device was not found. Device id: " + intent.getIntExtra(EXTRA_DEVICE_ID, 0));
            return;
        }
        try {
            int nOwner = intent.getIntExtra(EXTRA_TRIGGER_BY_MULTIGETTING_API, 0);
            if (nOwner == 1) {
                bTriggerByMultiGetting = true;
                String strContentIdList = intent.getStringExtra(EXTRA_CONTENTID_LIST);
                if (strContentIdList == null) {
                    Log.w(this, "Triggered by multigetting api but no content list");
                    return;
                }
                idList = strContentIdList.split(",");
                //Log.i("ATP","multigetting api idlist len="+idList.length);
            }
        } catch (Exception e) {
            Log.e(this, "onHandleIntent() - Exception : " + e.getMessage());
        }

        DeviceSLPF localDevice = DataModelSLPF.getInstance().getLocalDevice();
        if (device == localDevice && bTriggerByMultiGetting == false) {
            return;
        }

        ////Poc only, please use it after PoC
        //		if ((device.getDeviceNetworkMode() != CloudGatewayNetworkMode.WIFI && device.getDeviceTransportType() != CloudGatewayDeviceTransportType.WEB_STORAGE)
        //				|| localDevice.getDeviceNetworkMode() != CloudGatewayNetworkMode.WIFI) {
        //			sLogger.info("Device {} is not on WIFI or local device is not on WIFI. Giving up.", device.getDisplayName());
        //			if (bTriggerBySFinder == false) {
        //				return;
        //			}
        //			sLogger.info("triggered by SFINDER=>prefetch more...");
        //		}
        if (IASPApplication2.checkNeedWifiOnlyBlock()) {
            Log.i(this, "cloud storage sync is not enabled due to wifi only mode in mobile networks");
            return;
        }
        ArrayList<ImageInfo> sortedImageInfos = new ArrayList<ImageInfo>(NUM_PREFETCH_PER_DEVICE);
        AspThumbnailCache cache = AspThumbnailCache.getInstance(this);
        final String[] projection = {
                FileColumns._ID,
                FileColumns.DEVICE_ID,
                FileColumns.SOURCE_MEDIA_ID,
                FileColumns.MEDIA_TYPE,
                FileColumns.FULL_URI,
                FileColumns.DATA,
                FileColumns.DISPLAY_NAME,
                FileColumns.ORIENTATION,
                FileColumns.THUMBNAIL_URI,
                FileColumns.WIDTH,
                FileColumns.HEIGHT};
        if (bTriggerByMultiGetting == false) {
            try(Cursor cursor = getContentResolver().query(
                    ASPMediaStore.Files.CONTENT_URI,
                    projection,
                    FileColumns.DEVICE_ID + "=? AND (" + FileColumns.MEDIA_TYPE + "=? OR " + FileColumns.MEDIA_TYPE + "=?)",
                    new String[]{Integer.toString(device.getId()),Integer.toString(AspMediaId.MEDIA_TYPE_IMAGE),Integer.toString(AspMediaId.MEDIA_TYPE_VIDEO)},
                    FileColumns.DATE_ADDED + " DESC")) {
                if (cursor != null) {
                    cursor.moveToFirst();
                    while (!mDestroyed && cursor.moveToNext() && sortedImageInfos.size() < NUM_PREFETCH_PER_DEVICE) {
                        ////check thumbnail exist
                        int nContId = CursorUtils.getInt(cursor, FileColumns._ID);
                        int nMediaType = CursorUtils.getInt(cursor, FileColumns.MEDIA_TYPE);
                        if (cache.getCachedOnly(nContId, nMediaType, device.getId()) != null) {
                            //Log.i("INFO", "skip thumbnail prefetch due to exist id=" + nContId);
                            continue;
                        }
                        ImageInfo temp = ImageInfo.fromCursor(cursor);
                        temp.setDesiredWidth(mDownloadDimension);
                        temp.setDesiredHeight(mDownloadDimension);
                        temp.setThumbnailSize((temp.getMediaType() == AspMediaId.MEDIA_TYPE_IMAGE) ? ImageInfo.ThumbnailSize.FULL_SCREEN : ImageInfo.ThumbnailSize.MINI);
                        sortedImageInfos.add(temp);
                    }
                }
            }
        } else {
            for (int nCnt = 0; nCnt < idList.length; nCnt++) {
                try(Cursor cursor = getContentResolver().query(
                        ASPMediaStore.Files.CONTENT_URI,
                        projection,
                        FileColumns._ID + "=?",
                        new String[]{idList[nCnt]},
                        FileColumns.DATE_ADDED)){
                    if (cursor != null && cursor.getCount() > 0) {
                        cursor.moveToFirst();
                        ImageInfo temp = ImageInfo.fromCursor(cursor);
                        temp.setDesiredWidth(mDownloadDimension);
                        temp.setDesiredHeight(mDownloadDimension);
                        //Log.i("ATP", "multigetting api check cursor contentid=" + temp.getContentId());
                        temp.setThumbnailSize((temp.getMediaType() == AspMediaId.MEDIA_TYPE_IMAGE) ? ImageInfo.ThumbnailSize.FULL_SCREEN : ImageInfo.ThumbnailSize.MINI);
                        sortedImageInfos.add(temp);
                    }
                }
            }
        }

        if (mDestroyed) {
            return;
        }

        cache.setDevicePrefetchList(device.getId(), sortedImageInfos);

        HashMap<ImageInfo, DummyImageWorkerClient> requestsToCancel;

        mLock.lock();
        try {
            requestsToCancel = new HashMap<ImageInfo, DummyImageWorkerClient>(mPendingRequests.size());
            if (bTriggerByMultiGetting == false) {
                for (Entry<ImageInfo, DummyImageWorkerClient> entry : mPendingRequests.entrySet()) {
                    if (entry.getKey().getDeviceId() == device.getId()) {
                        requestsToCancel.put(entry.getKey(), entry.getValue());
                    }
                }
            }
        } finally {
            mLock.unlock();
        }

        for (int i = 0; i < sortedImageInfos.size() && !mDestroyed; ++i) {
            ImageInfo imageInfo = sortedImageInfos.get(i);
            DummyImageWorkerClient client = null;
            mLock.lock();
            try {
                if (!mPendingRequests.containsKey(imageInfo)) {
                    Log.d(this, "Prefetching thumbnail: " + imageInfo);
                    client = new DummyImageWorkerClient(imageInfo, bTriggerByMultiGetting);
                    mPendingRequests.put(imageInfo, client);
                    ImageWorker.MemoryCheck tmpCheck = ImageWorker.MemoryCheck.PRE_FETCH;
                    if (bTriggerByMultiGetting)
                        tmpCheck = ImageWorker.MemoryCheck.DEFAULT;
                    mImageWorker.loadImage(imageInfo, client, tmpCheck, ImageWorker.PRIORITY_PREFETCH_RECENT);
                } else {
                    Log.d(this, "Already prefetching thumbnail: " + imageInfo);
                }
                if (bTriggerByMultiGetting == false) {
                    requestsToCancel.remove(imageInfo);
                }
            } finally {
                mLock.unlock();
            }
        }
        if (bTriggerByMultiGetting == false) {
            for (Entry<ImageInfo, DummyImageWorkerClient> entry : requestsToCancel.entrySet()) {
                mLock.lock();
                try {
                    if (mPendingRequests.remove(entry.getKey()) != null) {
                        Log.d(this, "Cancelling thumbnail prefetch for: " + entry.getKey());
                        mImageWorker.cancelCurrentTask(entry.getValue());
                    }
                } finally {
                    mLock.unlock();
                }
            }
        }
    }
}


package platform.com.mfluent.asp.util;

import android.telephony.TelephonyManager;

import com.mfluent.log.Log;
import com.samsung.android.sdk.slinkcloud.CloudGatewayNetworkMode;

public class TypeManagerSLPF {
    private static String TAG = "TypeManagerSLPF";

    //	public static <T extends Enum<T>> T getEnumFromString(Class<T> enumType, String key)
    //	{
    //	    if( enumType != null && key != null )
    //	    {
    //	        try
    //	        {
    //	            return Enum.valueOf(enumType, key.trim().toUpperCase());
    //	        }
    //	        catch(IllegalArgumentException ex)
    //	        {
    //	        }
    //	    }
    //	    return null;
    //	}

    public static CloudGatewayNetworkMode checkMobileType(int mobileType) {
        Log.i(TAG, "mobile type: " + mobileType);

        switch (mobileType) {
            case TelephonyManager.NETWORK_TYPE_LTE:
                return CloudGatewayNetworkMode.MOBILE_LTE;

            case TelephonyManager.NETWORK_TYPE_UMTS:
            case TelephonyManager.NETWORK_TYPE_HSDPA:
            case TelephonyManager.NETWORK_TYPE_HSPA:
            case TelephonyManager.NETWORK_TYPE_HSPAP://4G
            case TelephonyManager.NETWORK_TYPE_HSUPA:
            case TelephonyManager.NETWORK_TYPE_EVDO_0:
            case TelephonyManager.NETWORK_TYPE_EVDO_A:
            case TelephonyManager.NETWORK_TYPE_EVDO_B:
            case TelephonyManager.NETWORK_TYPE_EHRPD:
                return CloudGatewayNetworkMode.MOBILE_3G;

            default:
                return CloudGatewayNetworkMode.MOBILE_2G;
        }
    }
}

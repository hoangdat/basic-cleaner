/**
 *
 */

package platform.com.mfluent.asp.filetransfer;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import android.util.Base64;

import com.mfluent.asp.common.datamodel.ASPFile;
import com.mfluent.asp.common.datamodel.ASPFileBrowser;
import com.mfluent.asp.common.datamodel.ASPFileProvider;
import com.mfluent.asp.common.datamodel.ASPFileSortType;
import com.mfluent.asp.common.io.util.StreamProgressListener;
import com.mfluent.asp.common.util.CloudStorageError;
import com.mfluent.log.Log;
import com.samsung.android.sdk.slinkcloud.CloudGatewayDeviceTransportType;
import com.samsung.android.sdk.slinkcloud.CloudGatewayFileBrowserUtils;
import com.samsung.android.sdk.slinkcloud.CloudGatewayFileTransferUtils;
import com.samsung.android.sdk.slinkcloud.CloudGatewayFileTransferUtils.TransferOptions;
import com.samsung.android.slinkcloud.Manifest;

import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.locks.ReentrantLock;
import java.util.regex.Pattern;

import platform.com.mfluent.asp.datamodel.DeviceSLPF;
import platform.com.mfluent.asp.datamodel.filebrowser.ASPFileBrowserManager;
import platform.com.mfluent.asp.datamodel.filebrowser.ASPFileProviderFactory;
import platform.com.mfluent.asp.datamodel.filebrowser.CachedFileProvider;
import platform.com.mfluent.asp.filetransfer.UploadTask.StorageUploadFileTooLargeException;
import platform.com.mfluent.asp.framework.IASPApplication2;
import platform.com.mfluent.asp.util.ProcessorManager;
import uicommon.com.mfluent.asp.util.CachedExecutorService;

public abstract class FileTransferTask implements Runnable, StreamProgressListener, FileTransferSession {
    public static final Pattern PATERN_SPECIAL_CHARS = Pattern.compile("[?:\"*|/\\<>]");
    public interface UploadCompleteListener {

        void onUploadComplete(UploadTask.FileUploadInfo fileUploadInfo);
    }

    private static final String TAG = "FileTransferTask";

    private final Set<DeviceSLPF> sourceDevices = new HashSet<DeviceSLPF>();

    private final HashMap<Integer, String> mMapSrcParentIdForDevice = new HashMap<Integer, String>();

    private final DeviceSLPF mTargetDevice;

    private final Context mContext;

    private final FileTransferTaskListener mListener;

    private final TransferOptions mOptions;

    private long mTotalBytesToTransfer = 0;

    private long bytesTransferred = 0;

    private boolean mErrorOccurred;

    private boolean mInProgress;

    // true if transfer is actually starting (what inProgress should have been used for IMO)
    private boolean mIsTransferStarted;

    private final String mSessionId;

    private boolean mHasWrittenComplete;

    private boolean mHasWrittenInProgress; //added by shirley,kim for persistent file transfer

    private boolean mHasNotifiedServiceOfStart;
    private boolean mFlagCanceled = false;

    private Future<?> mFuture;

    private int mQuotaErrorType;

    private ProcessorManager.ProcessorLock processorLock = null;
    private final static boolean USE_CPULOCK_FOR_FILETRANSFER = false;
    private int nFirstSourceId = 0;

    private int mInterpolationPercent = 0;
    public boolean mIsFileList = false;

    private Handler mDismissHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message msg) {
            int what = msg.what;
            switch (what) {
                case COMPLETED_OP_CMD:
                    mLevelWait = LEVEL_NONE;
                    sendStatusBroadcastForNativeApplication(CloudGatewayFileTransferUtils.SESSION_STATUS_COMPLETED);
                    break;
                default:
                    break;
            }
        }
    };
    private static final int COMPLETED_OP_CMD = 2301;
    private BroadcastReceiver mSyncReceiver;
    private int mLevelWait = LEVEL_NONE;
    private static final int LEVEL_NONE = 0;
    private static final int LEVEL_PREPARE = 1;

    public FileTransferTask(Context context, DeviceSLPF targetDevice, FileTransferTaskListener listener, String sessionId, TransferOptions options) {
        mContext = context.getApplicationContext();
        mTargetDevice = targetDevice;
        mListener = listener;
        mFlagCanceled = false;
        mQuotaErrorType = CloudGatewayFileBrowserUtils.ERROR_NONE;

        renameApiCheckStarted(context);
        if (options == null) {
            options = new TransferOptions();
        }
        mOptions = options;

        if (StringUtils.isEmpty(sessionId)) {
            mSessionId = createSessionId();
        } else {
            mSessionId = sessionId;
        }

        if (targetDevice.getDeviceTransportType() == CloudGatewayDeviceTransportType.WEB_STORAGE) {
            nMultiChannelTasks = targetDevice.getMaxNumTxConnection();
        }
    }

    private String createSessionId() {
//        String peerId = DataModelSLPF.getInstance().getLocalDevice().getPeerId();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss", Locale.US);
        String strDate = sdf.format(new Date());
        StringBuilder sb = new StringBuilder();
        //		sb.append(peerId.replaceAll("-", "_")).append("-").append(strDate);
        sb/*.append(peerId).append("-")*/.append(strDate);
        Log.d(TAG, "createSessionId() : " + sb.toString());

        return sb.toString();
    }

    @Override
    public Set<DeviceSLPF> getSourceDevices() {
        return Collections.unmodifiableSet(sourceDevices);
    }

    public void addMapSrcParentForDevice(int deviceId, String parentId) {
        mMapSrcParentIdForDevice.put(deviceId, parentId);
    }

    public String getSrcParentForDevice(int deviceId) {
        String result = null;

        if (mMapSrcParentIdForDevice != null && mMapSrcParentIdForDevice.size() > 0) {
            result = mMapSrcParentIdForDevice.get(deviceId);
        }

        return result;
    }

    protected void addSourceDevice(DeviceSLPF device) {
        if (nFirstSourceId == 0)
            nFirstSourceId = device.getId();
        sourceDevices.add(device);
    }

    @Override
    public DeviceSLPF getTargetDevice() {
        return mTargetDevice;
    }

    @Override
    public int getProgress() {
        if (mTotalBytesToTransfer <= 0) {
            return 0;
        } else if (bytesTransferred > mTotalBytesToTransfer) {
            return 100;
        }
        return (int) (bytesTransferred * 100 / mTotalBytesToTransfer);
    }

    @Override
    public long getTotalBytesToTransfer() {
        return mTotalBytesToTransfer;
    }

    public void setTotalBytesToTransfer(long totalBytesToTransfer) {
        mTotalBytesToTransfer = totalBytesToTransfer;
    }

    @Override
    public abstract int getNumberOfFiles();

    @Override
    public boolean errorOccurred() {
        return mErrorOccurred;
    }

    @Override
    public boolean isInProgress() {
        return mInProgress;
    }

    public void setInProgress(boolean inProgress) {
        mInProgress = inProgress;
        if (inProgress) {
            acquireProcessorLock();
        } else {
            releaseProcessorLock();
        }
    }

    public synchronized void acquireProcessorLock() {
        if (USE_CPULOCK_FOR_FILETRANSFER && processorLock == null) {
            processorLock = ProcessorManager.getInstance(mContext).createLock(ProcessorManager.LOCK_TYPE_CPU | ProcessorManager.LOCK_TYPE_WIFI);
        }
    }

    public synchronized void releaseProcessorLock() {
        if (USE_CPULOCK_FOR_FILETRANSFER && processorLock != null) {
            processorLock.release();
            processorLock = null;
        }
    }

    @Override
    public boolean isTransferStarted() {
        return mIsTransferStarted;
    }

    public void setTransferStarted(boolean isTransferStarted) {
        if (isTransferStarted != mIsTransferStarted) {
            mIsTransferStarted = isTransferStarted;
            updateListener();
        }
    }

    public abstract void addTask(ASPFile file, DeviceSLPF sourceDevice);

    public abstract boolean addTask(ASPFile file, DeviceSLPF sourceDevice, String relativeTarget, Map<String, String> targetFolderCache);

    private ArrayList<String> mSourceFolderList = new ArrayList<>();
    private ArrayList<Integer> mSourceFolderDeviceList = new ArrayList<>();

    public void addSourceFolder(String strCloudID, DeviceSLPF sourceDevice) {
        mSourceFolderList.add(strCloudID);
        mSourceFolderDeviceList.add(sourceDevice.getId());
    }

    private boolean removeSourceFolder() {
        int nSize = mSourceFolderList.size();
        if (nSize == 0)
            return true;

        boolean bRet = true;
        Log.d(this, "removeSourceFolder() - " + nSize);

        try {
            int deviceId = mSourceFolderDeviceList.get(0);
            String[] arr = new String[nSize];
            mSourceFolderList.toArray(arr);
            bRet = CloudGatewayFileBrowserUtils.getInstance(mContext).mrrControlBatchCommand(deviceId, CloudGatewayFileBrowserUtils.REMOTE_FOLDER_RMDIR_PERMANENTLY_BATCH, arr, null);
        } catch (Exception e) {
            Log.e(this, "removeSourceFolder() - Exception : " + e.getMessage());
        }
        return bRet;
    }

    //added by shirley.kim for persistent file transfer
    public abstract long getSourceMediaId(int index);

    public void reset() {
        bytesTransferred = 0;
        mInProgress = true;
        acquireProcessorLock();
        mErrorOccurred = false;
    }

    public boolean checkExceptionForAutoRetry(Exception e) {
        boolean bIsCritical = true;
        String exceptionName = e.getClass().toString();
        String errorMsg = e.getMessage();
        Log.d(this, "checkExceptionForAutoRetry() - " + exceptionName + ", msg : " + errorMsg);
        e.printStackTrace();

        ////auto retry file transfer
        if (IASPApplication2.checkNeedWifiOnlyBlock()) {
            Log.i(this, "checkExceptionForAutoRetry() - it's wifi only mode return error during filetransfer...");
            bIsCritical = true;
        } else if (exceptionName.contains("IOException")) {
            if (StringUtils.contains(errorMsg, "ENOSPC") && getTargetDevice().isLocalDevice()) {
                Log.e(this, "checkExceptionForAutoRetry() - ENOSPC (Device full)");
                bIsCritical = true;
            } else if ((StringUtils.contains(errorMsg, CloudStorageError.RETRY_MAX_ERROR))
                    || (StringUtils.contains(errorMsg, CloudStorageError.NO_NEED_RETRY_ERROR))) {
                bIsCritical = true;
            } else if (!getTargetDevice().isLocalDevice() && (StringUtils.contains(errorMsg, CloudStorageError.OUT_OF_STORAGE_ERROR)
                    || StringUtils.contains(errorMsg, CloudStorageError.REACH_MAX_ITEM_ERROR))) {
                bIsCritical = true;
            } else {
                bIsCritical = false;
            }
        } else if (exceptionName.contains("ConnectException")) {
            bIsCritical = false;
        } else {
            if (errorMsg != null && errorMsg.contains("Connection timed out")) {
                bIsCritical = false;
            } else if (e.getCause() != null) {
                String causeMsg = e.getCause().getMessage();
                if ((causeMsg != null) && (causeMsg.contains("Connection timed out") || causeMsg.contains("Unable to resolve host"))) {
                    bIsCritical = false;
                }
            }
        }
        return bIsCritical;
    }

    @Override
    public void run() {
        reset();
        updateListener();

        try {
            Log.d(TAG, "run() - Starting transfer for " + this);
            doMultiChannelTransfer();
        } catch (InterruptedException e) {
            Log.e(TAG, "run() - Transfer interrupted : " + e.getMessage());
            mErrorOccurred = true;
            Thread.currentThread().interrupt();
        } catch (StorageUploadFileTooLargeException e) {
            Log.e(TAG, "run() - Trouble transferring; file too large : " + e.getMessage());
            mErrorOccurred = true;
        } catch (Exception e) {
            Log.e(TAG, "run() - Trouble transferring " + this + " : " + e.getMessage());
            mErrorOccurred = true;
        } finally {
            mInProgress = false;
            releaseProcessorLock();
            updateListener();
        }
    }

    @Override
    public long getBytesTransferred() {
        return bytesTransferred;
    }

    /*
     * (non-Javadoc)
     * @see com.mfluent.asp.io.util.StreamProgressListener#bytesTransferred(int)
     */
    @Override
    public void bytesTransferred(long numberOfBytes) {
        int oldProgress = getProgress();

        bytesTransferred += numberOfBytes;
        mListener.bytesTransferred(numberOfBytes);
        if (getProgress() != oldProgress) {
            updateListener();
        }
    }

    public void updateListener() {
        mListener.stateChanged(this);
    }

    public boolean isCanceled() {
        return mFlagCanceled;
    }

    public void cancel() {
        // prevent infinite loop
        sendStatusBroadcastForNativeApplication(CloudGatewayFileTransferUtils.SESSION_STATUS_CANCELED);
        if (mFlagCanceled) {
            return;
        }
        mFlagCanceled = true;

        mErrorOccurred = true;
        mInProgress = false;

        if (mFuture != null) {
            mFuture.cancel(true);
        }
        releaseProcessorLock();

        // By Minsung Ku - sometimes, the transfer maintain in status of canceling and no longer in progress.
        // I think that it is happen when the task be cancelled before actual transfer begin.
        // So In this case, should be update current status and continue to next progress.
        //
        // Tested by JinHa Kim.

        // If Send-to, prevent infinite loop (java.lang.StackOverflowError)
        //if (isAutoUpload()) { // infinite loop prevention moved to this function start above
        updateListener();
        //}
        ////multi channel codes
        cancelMultiChannel();
//        tmpLoadingMultiLaneThumbnailCancel(getSessionId());
    }

    public Context getContext() {
        return mContext;
    }

    /**
     * @return the errorOccurred
     */
    public boolean isErrorOccurred() {
        return mErrorOccurred;
    }

    /**
     * @param errorOccurred the errorOccurred to set
     */
    public void setErrorOccurred(boolean errorOccurred) {
        mErrorOccurred = errorOccurred;
    }

    @Override
    public String getSessionId() {
        return mSessionId;
    }

    public void destroy() {
        if (mSyncReceiver != null) {
            mContext.unregisterReceiver(mSyncReceiver);
            mSyncReceiver = null;
        }
        mDismissHandler = null;
    }

    public boolean getHasWrittenComplete() {
        return mHasWrittenComplete;
    }

    public void setHasWrittenComplete(boolean hasWrittenComplete) {
        mHasWrittenComplete = hasWrittenComplete;
    }

    //added by shirley.kim for persistent file transfer
    public boolean getHasWrittenInProgress() {
        return mHasWrittenInProgress;
    }

    public void setHasWrittenInProgress(boolean hasWrittenInProgress) {
        mHasWrittenInProgress = hasWrittenInProgress;
    }

    public void setFuture(Future<?> future) {
        mFuture = future;
    }

    @Override
    public int getCurrentFileIndex() {
        return 0;
    }

    public int getSentFileNumForMultiChannelSending() {
        return 0;
    }

    @Override
    public Status getStatus() {
        if (!mIsTransferStarted) {
            return Status.INIT;
        }

        if (mInProgress) {
            return Status.SENDING;
        }

        if (mErrorOccurred) {
            return Status.STOPPED;
        }

        return Status.COMPLETED;
    }

    public boolean getHasNotifiedServiceOfStart() {
        return mHasNotifiedServiceOfStart;
    }

    public void setHasNotifiedServiceOfStart(boolean hasNotifiedServiceOfStart) {
        mHasNotifiedServiceOfStart = hasNotifiedServiceOfStart;
    }

    public TransferOptions getOptions() {
        return mOptions;
    }

    @Override
    public void setStatus(Status status) {
        if (status == Status.COMPLETED) {
            mErrorOccurred = false;
        }
    }

    private int mOldTotalNumForBR = 0;
    private int mOldPercent = 0;

    public int sendStatusBroadcastForNativeApplication(int nStatusCode) {
        int nPrePercent = 0;

        try {
            Intent intent = new Intent(CloudGatewayFileTransferUtils.FILE_TRANSFER_SESSION_ID_BROADCAST_ACTION);
            intent.putExtra(CloudGatewayFileTransferUtils.EXTRA_SESSION_STATUS_CODE, nStatusCode);
            intent.putExtra(CloudGatewayFileTransferUtils.EXTRA_SESSION_ID, getSessionId());
            nPrePercent = getProgress();

            int nTotalFiles = getNumberOfFiles();
            if (mOldTotalNumForBR > nTotalFiles)
                nTotalFiles = mOldTotalNumForBR;

            mOldTotalNumForBR = nTotalFiles;
            int nCurSent = getSentFileNumForMultiChannelSending();

            Log.i(this, "sendStatusBroadcastForNativeApplication start nStatusCode=" + nStatusCode + " prePercent=" + nPrePercent + " nTotalFiles=" + nTotalFiles
                    + " mOldTotalNumForBR=" + mOldTotalNumForBR + " nCurSent=" + nCurSent + " mInterpolationPercent=" + mInterpolationPercent);

            if (nPrePercent < 0)
                nPrePercent = 0;

            if ((nPrePercent > 100) || (nStatusCode == CloudGatewayFileTransferUtils.SESSION_STATUS_COMPLETED))
                nPrePercent = 100;

            if ((nCurSent > nTotalFiles) || (nPrePercent == 100))
                nCurSent = nTotalFiles;

            if ((nPrePercent == 0) && (nCurSent > 0)) {
                nPrePercent = nCurSent * 100 / nTotalFiles;
            }
            if (mInterpolationPercent < nPrePercent) {
                mInterpolationPercent = nPrePercent;
            }
            if (nPrePercent < 100)
                nPrePercent = mInterpolationPercent;
            if (nPrePercent == 0 && nCurSent == nTotalFiles && nCurSent > 0) {
                nCurSent = 0;
            }
//            if(nPrePercent>=85 && nTotalFiles==1 && strFirstFileName.indexOf(".mp4")>0) {
//				Log.i(this,"85 nPrePercent="+nPrePercent);
//                nPrePercent = 100;
//            }else
            if (nPrePercent >= 100 && nStatusCode == CloudGatewayFileTransferUtils.SESSION_STATUS_INPROGRESS) {
                nPrePercent = 99;
            }

            if (mOldPercent > nPrePercent) {
                nPrePercent = mOldPercent;
            } else {
                mOldPercent = nPrePercent;
            }
            intent.putExtra(CloudGatewayFileTransferUtils.EXTRA_SESSION_STATUS_PERCENTAGE, nPrePercent);
            intent.putExtra(CloudGatewayFileTransferUtils.EXTRA_SESSION_STATUS_FILENUM, nTotalFiles);
            intent.putExtra(CloudGatewayFileTransferUtils.EXTRA_SESSION_STATUS_SENTFILENUM, nCurSent);
            intent.putExtra(CloudGatewayFileTransferUtils.EXTRA_SESSION_STATUS_ERROR_TYPE, mQuotaErrorType);
            if ((nPrePercent == 100) || (nStatusCode == CloudGatewayFileTransferUtils.SESSION_STATUS_CANCELED) || (nStatusCode == CloudGatewayFileTransferUtils.SESSION_STATUS_ERROR)) {
                String strSourceFileList = getTransferredFileList();
                if (strSourceFileList != null)
                    intent.putExtra(CloudGatewayFileTransferUtils.EXTRA_SESSION_STATUS_FILELIST, strSourceFileList);
            }
            mContext.sendBroadcast(intent);
            Log.i(this, "sendStatusBroadcastForNativeApplication end");
        } catch (Exception e) {
            Log.e(this, "sendStatusBroadcastForNativeApplication() - Exception : " + e.getMessage());
            e.printStackTrace();
        }
        return nPrePercent;
    }

    //multi channel codes
    public static final int MAX_CHANNEL_NUM = 8;
    private int nMultiChannelTasks = MAX_CHANNEL_NUM;

    private ArrayList<MultiChannelTransferJob> mMultiChannelJobList = null;
    private ArrayList<Future<Void>> mMultiChannelFutureList = null;
    protected ReentrantLock mMultiChannelLock = null;

    private final HashMap<Integer, Integer> mapDoneJob = new HashMap<Integer, Integer>();
    private final HashMap<Integer, Integer> mapChannelCurrentItem = new HashMap<Integer, Integer>();
    private int nJobCounter = 0;

    public static boolean bDuringMultiChannelTransfer = false;

    public void setMultiChannelNum(int num) {
        if (num < MAX_CHANNEL_NUM) {
            nMultiChannelTasks = num;
        }
    }

    public void _setMultiChannelTransfer() {
        setMultiChannelNum(getNumberOfFiles());
    }

    private void doMultiChannelTransfer() throws Exception {

        try {
            if (IASPApplication2.checkNeedWifiOnlyBlock()) {
                cancel();
                return;
            }
            prepareMultiChannel();
            sendStatusBroadcastForNativeApplication(CloudGatewayFileTransferUtils.SESSION_STATUS_STARTED);

            _setMultiChannelTransfer();
            Log.d(this, "doMultiChannelTransfer() - Channel num : " + nMultiChannelTasks);

            bDuringMultiChannelTransfer = true;
            mInterpolationPercent = 0;
            nJobCounter = nMultiChannelTasks;
            mapDoneJob.clear();
            mapChannelCurrentItem.clear();

            mMultiChannelLock = new ReentrantLock();
            if (mMultiChannelJobList == null) {
                mMultiChannelJobList = new ArrayList<>();
                for (int nCnt = 0; nCnt < nMultiChannelTasks; nCnt++) {
                    MultiChannelTransferJob tmpJob = new MultiChannelTransferJob(nCnt);
                    mMultiChannelJobList.add(tmpJob);
                }
                mMultiChannelFutureList = new ArrayList<>();
            }

            if (mFlagCanceled || (mbRenameApplyedAll && m_nRenameApplyedAllStatus == CloudGatewayFileTransferUtils.RENAME_STATUS_RSP_CANCEL)) {
                cancel();
                return;
            }

            ////start threads
            for (int nCnt = 0; nCnt < nMultiChannelTasks; nCnt++) {
                mMultiChannelJobList.get(nCnt).refresh();
                Future<Void> future = CachedExecutorService.getInstance().submit(mMultiChannelJobList.get(nCnt));
                mMultiChannelFutureList.add(future);
            }

            mContext.registerReceiver(getSyncBroadcastReceiver(), new IntentFilter(CloudGatewayFileBrowserUtils.SYNC_FINISHED), Manifest.permission.PUBLIC_ACCESS, null);

            ////monitor threads
            int nErrorCount, nBroadcastCount = 0;
            boolean bDone, isOver60 = false;
            while (true) {
                bDone = true;
                nErrorCount = 0;
                for (int nCnt = 0; nCnt < nMultiChannelTasks; nCnt++) {
                    if (mMultiChannelJobList.get(nCnt).isRunning == true) {
                        bDone = false;
                        break;
                    } else if (mMultiChannelJobList.get(nCnt).bCriticalError) {
                        nErrorCount++;
                    }
                    mQuotaErrorType = mMultiChannelJobList.get(nCnt).bStorageError;
                    if (mQuotaErrorType != CloudGatewayFileBrowserUtils.ERROR_NONE) {
                        Log.i(this, "doMultiChannelTransfer() - quotaError = " + mQuotaErrorType);
                        cancelMultiChannel();
                        bDone = true;
                        break;
                    }
                }
                if (bDone) {
                    Log.i(this, "doMultiChannelTransfer() - all done multichannel jobs");
                    break;
                }
                Thread.sleep(200);
                nBroadcastCount++;
                if (isOver60 || nBroadcastCount > 3) {
                    nBroadcastCount = 0;
                    //Send Broadcast for myfiles, gallery, music app
                    if (sendStatusBroadcastForNativeApplication(CloudGatewayFileTransferUtils.SESSION_STATUS_INPROGRESS) > 90)
                        isOver60 = true;
                }
            }
            boolean success = onExitMultiChannel();
            if (mFlagCanceled) {
                sendStatusBroadcastForNativeApplication(CloudGatewayFileTransferUtils.SESSION_STATUS_CANCELED);
                return;
            }

            if ((nErrorCount > 0) || !success) {
                Log.i(this, "doMultiChannelTransfer() - error happen during file transfer nErrorCount=" + nErrorCount + "-quota error = " + mQuotaErrorType);
                setErrorOccurred(true);
                getOptions().skipIfDuplicate = true;
                getOptions().waitForRename = false;
                sendStatusBroadcastForNativeApplication(CloudGatewayFileTransferUtils.SESSION_STATUS_ERROR);
            } else {
                success = (getOptions().deleteSourceFilesWhenTransferIsComplete) ? removeSourceFolder() : true;
                sendStatusBroadcastForNativeApplication(success ? CloudGatewayFileTransferUtils.SESSION_STATUS_COMPLETED : CloudGatewayFileTransferUtils.SESSION_STATUS_ERROR);
            }
        } catch (Exception e) {
            Log.i(this, "doMultiChannelTransfer() - multichannel exception");
            throw e;
        } finally {
            bDuringMultiChannelTransfer = false;
            Log.d(this, "doMultiChannelTransfer() - finally bDuringMultiChannelTransfer");
        }
    }

    protected void lockMultiChannel() {
        mMultiChannelLock.lock();
    }

    protected void unlockMultiChannel() {
        mMultiChannelLock.unlock();
    }

    public void errorMultiChannelJobCounter(int nChannelId) {
        synchronized (mapDoneJob) {
            if (!mapChannelCurrentItem.isEmpty()) {
                int nItemCnt = mapChannelCurrentItem.get(nChannelId);
                if (nItemCnt >= 0) {
                    mapDoneJob.remove(Integer.valueOf(nItemCnt));
                }
            }
        }
    }

    public boolean checkMultiChannelItem(int nChannelId, int nItemCnt) {
        boolean bRet = false;
        synchronized (mapDoneJob) {
            Integer obj = mapDoneJob.get(nItemCnt);
            if (obj == null) {
                mapDoneJob.put(nItemCnt, nChannelId);
                mapChannelCurrentItem.remove(nChannelId);
                mapChannelCurrentItem.put(nChannelId, nItemCnt);
                bRet = true;
            }
        }
        return bRet;
    }

    protected void cancelMultiChannel() {
        if (mMultiChannelJobList != null) {
            for (int nCnt = 0; nCnt < nMultiChannelTasks; nCnt++) {
                try {
                    if (mMultiChannelJobList.size() > nCnt) {
                        mMultiChannelJobList.get(nCnt).needExit = true;
                        mMultiChannelFutureList.get(nCnt).cancel(true);
                        Log.i(this, "cancelMultiChannel:task id=" + nCnt);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    break;
                }
            }
        }
    }

    ////should be called as super in children classes
    protected void prepareMultiChannel() throws Exception {
        setTransferStarted(true);
    }

    protected boolean onExitMultiChannel() {
        Log.w(this, "onExitMultiChannel() - no implementation (override this function)");
        return true;
    }

    protected void doThreadSafeTransfer(int nChannelId) throws Exception {
        Log.w(this, "doTransferMultiChannel() - no implementation (override this function)");
    }

    private class MultiChannelTransferJob implements Callable<Void> {
        public int nChannelId = 0;
        public volatile boolean isRunning = false;
        public volatile boolean needExit = false;
        public volatile boolean bCriticalError = false;
        public volatile int bStorageError = CloudGatewayFileBrowserUtils.ERROR_NONE;

        MultiChannelTransferJob(int nId) {
            nChannelId = nId;
        }

        public void refresh() {
            bCriticalError = false;
        }

        @Override
        public Void call() {
            boolean bError;
            isRunning = true;

            if (needExit) {
                Log.i(this, "MultiChannelTransferJob: immediate return cause needExit true");
                return null;
            }
            while (needExit == false) {
                bError = false;
                try {
                    doThreadSafeTransfer(nChannelId);
                } catch (Exception e) {
                    errorMultiChannelJobCounter(nChannelId);
                    bStorageError = CloudStorageError.getExceptionError(e);
                    bCriticalError = checkExceptionForAutoRetry(e);
                    Log.i(this, "MultiChannelTransferJob/call() - Exception : " + e.getMessage() + ", bCriticalError : " + bCriticalError);
                    bError = true;
                }

                if (bCriticalError || needExit) {
                    break;
                }
                if (!bError) {
                    if (nJobCounter == 1) {
                        nJobCounter = 0;
                        Log.i(this, "check remaining files in the last thread");
                        continue;
                    } else {
                        break;
                    }
                }
            }
            isRunning = false;
            synchronized (mapDoneJob) {
                if (nJobCounter > 0)
                    nJobCounter--;
            }
            return null;
        }
    }

    public boolean mbRenameApplyedAll = false;
    public int m_nRenameApplyedAllStatus = 0;
    public static final String mRenameSkipResult = "SKIP^ANYWAY";
    private static int mRenameCounter = 0;
    private static boolean mRenameReceiverInit = false;
    private static final Object mRenameReceiverObject = new Object();

    private static final HashMap<String, RenameDataItem> mRenameDataRepo = new HashMap<String, RenameDataItem>();

    private static final BroadcastReceiver sFileTransferRenameResultBroadcastReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                String strToken = intent.getStringExtra(CloudGatewayFileTransferUtils.EXTRA_RENAME_TOKEN);
                String strNewName = intent.getStringExtra(CloudGatewayFileTransferUtils.EXTRA_RENAME_NEWNAME);
                int nStatus = intent.getIntExtra(CloudGatewayFileTransferUtils.EXTRA_RENAME_STATUS, 0);
                boolean bApplyAll = intent.getBooleanExtra(CloudGatewayFileTransferUtils.EXTRA_RENAME_APPLYALL, false);
                if (strToken != null)
                    renameApiProcessRenameResult(strToken, strNewName, nStatus, bApplyAll);
            }
        }
    };

    public static void renameApiCheckStarted(Context context) {
        synchronized (mRenameReceiverObject) {
            if (mRenameReceiverInit == false) {
                context.registerReceiver(sFileTransferRenameResultBroadcastReceiver, new IntentFilter(CloudGatewayFileTransferUtils.ACTION_RENAME_RSP), Manifest.permission.PUBLIC_ACCESS, null);
                mRenameReceiverInit = true;
            }
        }
    }

    public static String renameApiCheckSpecialCharacterCondition(String strTargetDirectoryCloudID, String strFileName, boolean isDirectoryProcessing) {
        String strRet = strFileName;
        if (TextUtils.isEmpty(strTargetDirectoryCloudID) || TextUtils.isEmpty(strFileName))
            return strRet;
        //*/:?"<>|
//		Log.i("special","1 prev="+strFileName);
        strRet = PATERN_SPECIAL_CHARS.matcher(strRet).replaceAll("-");
        String strRet2 = strRet.replace("\u20A9", "-");
        if (isDirectoryProcessing) {
            if (strRet2.equals(".")) {
                strRet2 = "-";
            } else if (strRet2.equals("..")) {
                strRet2 = "--";
            }
        }
        Log.i("special", "renameApiCheckSpecialCharacterCondition() - post=" + strRet2);
        return strRet2;
    }

    public static String renameApiCheckDuplicationCondition(Context context, DeviceSLPF targetDevice, String strTargetDirectoryCloudID, String strFileName) {
        String strRet = strFileName;
        int nCnt = 0;
        int nStrLen;
        int nParenthesisCount;
        ASPFile file;
        if (TextUtils.isEmpty(strTargetDirectoryCloudID) || TextUtils.isEmpty(strFileName)) {
            return strRet;
        }
        try {
            ASPFileProvider fileProvider = ASPFileProviderFactory.getFileProviderForDevice(context, targetDevice);
            if (fileProvider instanceof CachedFileProvider) {
                CachedFileProvider newProvider = (CachedFileProvider) fileProvider;
                newProvider.setNonBlockingBrowser();
            }
            ASPFileBrowserManager.ASPFileBrowserReference tmpBrowserRef = ASPFileBrowserManager.getInstance().getLoadedFileBrowser2(fileProvider, strTargetDirectoryCloudID, null, null);
            if (tmpBrowserRef != null) {
                ASPFileBrowser<?> tmpBrowser = tmpBrowserRef.getFileBrowser();
                while (true) {
                    file = findFileInFileBrowser(tmpBrowser, strRet, false);
                    if (file != null) {
                        String strExt = "";
                        String strPrefix = strRet;
                        int nLastIndex = strRet.lastIndexOf(".");
                        //if it is a file..
                        if (nLastIndex > 0) {
                            strPrefix = strRet.substring(0, nLastIndex);
                            strExt = strRet.substring(nLastIndex);
                        }//end if
                        nParenthesisCount = -1;
                        nStrLen = strPrefix.length();
                        if (nStrLen > 3 && strPrefix.charAt(nStrLen - 1) == ')' && strPrefix.indexOf('(') > 0) {
                            Log.i(TAG, "renameApiCheckDuplicationCondition() - rename ( detected");
                            int nStartPTH = strPrefix.indexOf('(');
                            String strNewSub = strPrefix.substring(nStartPTH + 1, nStrLen - 1);
                            if (strNewSub != null) {
                                Log.i(TAG, "renameApiCheckDuplicationCondition() - rename strNewSub is not null");
                                try {
                                    nParenthesisCount = Integer.parseInt(strNewSub);
                                } catch (Exception e) {
                                    nParenthesisCount = -1;
                                }
                            }
                            if (nParenthesisCount >= 0) {
                                Log.i(TAG, "renameApiCheckDuplicationCondition() - rename nParenthesisCount=" + nParenthesisCount);
                                String strNewPrefix = strPrefix.substring(0, nStartPTH);
                                String strNoSpacePrefix = strNewPrefix.trim();
                                strRet = strNoSpacePrefix + " (" + (nParenthesisCount + 1) + ")" + strExt;
                            }
                        }
                        if (nParenthesisCount < 0) {
                            Log.i(TAG, "renameApiCheckDuplicationCondition() - rename minus nParenthesisCount=" + nParenthesisCount);
                            strRet = strPrefix + " (" + nCnt + ")" + strExt;
                        }
                        Log.i(TAG, "renameApiCheckDuplicationCondition() - rename api new name=" + strRet);
                        nCnt++;
                    } else
                        break;
                }
            }
        } catch (RuntimeException runE) {
            Log.e(TAG, "renameApiCheckDuplicationCondition() - RuntimeException : " + runE.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "renameApiCheckDuplicationCondition() - Exception : " + e.getMessage());
        }
        return strRet;
    }

    public static RenameDataItem renameApiRequestRenameToApplication(Context context, String strSessionID, String strOldName, String strNewName, boolean isDirectory, boolean isSpecicalCharacter) {
        RenameDataItem objData = new RenameDataItem();
        try {
            String strToken = strSessionID + "-" + "strOldName" + "-" + (mRenameCounter++);
            objData.bIsDirectory = isDirectory;
            objData.nCheckTime = System.currentTimeMillis();
            objData.nStatus = CloudGatewayFileTransferUtils.RENAME_STATUS_REQ_INUSE;
            objData.strNewName = strNewName;
            objData.strOldName = strOldName;
            objData.strSessionID = strSessionID;
            objData.strRenameToken = strToken;
            synchronized (mRenameDataRepo) {
                mRenameDataRepo.put(strToken, objData);
            }

            Intent intent = new Intent(CloudGatewayFileTransferUtils.ACTION_RENAME_REQ);
            intent.putExtra(CloudGatewayFileTransferUtils.EXTRA_RENAME_STATUS,
                    isSpecicalCharacter ? CloudGatewayFileTransferUtils.RENAME_STATUS_REQ_STRANGE_CHARACTER : CloudGatewayFileTransferUtils.RENAME_STATUS_REQ_INUSE);
            intent.putExtra(CloudGatewayFileTransferUtils.EXTRA_SESSION_ID, strSessionID);
            intent.putExtra(CloudGatewayFileTransferUtils.EXTRA_RENAME_OLDNAME, strOldName);
            intent.putExtra(CloudGatewayFileTransferUtils.EXTRA_RENAME_NEWNAME, strNewName);
            intent.putExtra(CloudGatewayFileTransferUtils.EXTRA_RENAME_ISDIRECTORY, isDirectory);
            intent.putExtra(CloudGatewayFileTransferUtils.EXTRA_RENAME_TOKEN, strToken);
            context.sendBroadcast(intent);

            int nCnt = 0;
            int nStatus = 0;
            while (true) {
                synchronized (mRenameDataRepo) {
                    objData = mRenameDataRepo.get(strToken);
                    if (objData != null) {
                        nStatus = objData.nStatus;
                    }
                }
                if ((nStatus >= CloudGatewayFileTransferUtils.RENAME_STATUS_RSP_RENAME) && (nStatus <= CloudGatewayFileTransferUtils.RENAME_STATUS_RSP_CANCEL)) {
                    break;
                }
                nCnt++;
                if (nStatus < CloudGatewayFileTransferUtils.RENAME_STATUS_RSP_START && nCnt > 100) {
                    Log.i(TAG, "renameApiRequestRenameToApplication() - time out for rename rsp");
                    break;
                }
                Thread.sleep(200);
            }
            synchronized (mRenameDataRepo) {
                mRenameDataRepo.remove(strToken);
            }
        } catch (RuntimeException runE) {
            Log.e(TAG, "renameApiRequestRenameToApplication() - RuntimeException : " + runE.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "renameApiRequestRenameToApplication() - Exception : " + e.getMessage());
        }
        return objData;
    }


    public void renameApiRequestRenameToApplication(Context context, String strSessionID, String strOldName, String strNewName, boolean isDirectory, boolean isSpecialCharacter, RenameDataItem.OnReceiveRSPRenameStatus callback) {
        RenameDataItem objData = new RenameDataItem();
        try {
            String strToken = strSessionID + "-" + "strOldName" + "-" + (mRenameCounter++);
            objData.bIsDirectory = isDirectory;
            objData.nCheckTime = System.currentTimeMillis();
            objData.nStatus = CloudGatewayFileTransferUtils.RENAME_STATUS_REQ_INUSE;
            objData.strNewName = strNewName;
            objData.strOldName = strOldName;
            objData.strSessionID = strSessionID;
            objData.strRenameToken = strToken;
            if (mbRenameApplyedAll) {
                Log.d(this, "renameApiRequestRenameToApplication - apply all");
                objData.nStatus = m_nRenameApplyedAllStatus;
                callback.onReceiveRenameStatus(objData, strNewName);
                return;
            }

            synchronized (mRenameDataRepo) {
                mRenameDataRepo.put(strToken, objData);
            }

            Intent intent = new Intent(CloudGatewayFileTransferUtils.ACTION_RENAME_REQ);
            intent.putExtra(CloudGatewayFileTransferUtils.EXTRA_RENAME_STATUS,
                    isSpecialCharacter ? CloudGatewayFileTransferUtils.RENAME_STATUS_REQ_STRANGE_CHARACTER : CloudGatewayFileTransferUtils.RENAME_STATUS_REQ_INUSE);
            intent.putExtra(CloudGatewayFileTransferUtils.EXTRA_SESSION_ID, strSessionID);
            intent.putExtra(CloudGatewayFileTransferUtils.EXTRA_RENAME_OLDNAME, strOldName);
            intent.putExtra(CloudGatewayFileTransferUtils.EXTRA_RENAME_NEWNAME, strNewName);
            intent.putExtra(CloudGatewayFileTransferUtils.EXTRA_RENAME_ISDIRECTORY, isDirectory);
            intent.putExtra(CloudGatewayFileTransferUtils.EXTRA_RENAME_TOKEN, strToken);
            context.sendBroadcast(intent);

            int nCnt = 0;
            int nStatus = 0;
            while (true) {
                synchronized (mRenameDataRepo) {
                    objData = mRenameDataRepo.get(strToken);
                    if (objData != null) {
                        Log.d(this, "status = " + objData.nStatus);
                        nStatus = objData.nStatus;
                    }
                }
                if ((nStatus >= CloudGatewayFileTransferUtils.RENAME_STATUS_RSP_RENAME) && (nStatus <= CloudGatewayFileTransferUtils.RENAME_STATUS_RSP_CANCEL)) {
                    break;
                }
                nCnt++;
                if (nStatus < CloudGatewayFileTransferUtils.RENAME_STATUS_RSP_START && nCnt > 100) {
                    Log.i(TAG, "renameApiRequestRenameToApplication() - time out for rename rsp");
                    break;
                }
                Thread.sleep(200);
            }
            synchronized (mRenameDataRepo) {
                mRenameDataRepo.remove(strToken);
            }
        } catch (RuntimeException runE) {
            Log.e(TAG, "renameApiRequestRenameToApplication() - RuntimeException : " + runE.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "renameApiRequestRenameToApplication() - Exception : " + e.getMessage());
        }

        callback.onReceiveRenameStatus(objData, strNewName);
    }

    public static void renameApiProcessRenameResult(String strToken, String strNewName, int nStatus, boolean bApplyAll) {
        synchronized (mRenameDataRepo) {
            if (mRenameDataRepo.isEmpty() == false) {
                try {
                    RenameDataItem objRet = mRenameDataRepo.get(strToken);
                    if (objRet != null) {
                        objRet.nCheckTime = System.currentTimeMillis();
                        objRet.nStatus = nStatus;
                        if (TextUtils.isEmpty(strNewName) == false)
                            objRet.strNewName = strNewName;
                        objRet.bApplyAll = bApplyAll;
                    }
                } catch (Exception e) {
                    Log.e(TAG, "renameApiProcessRenameResult() - Exception : " + e.getMessage());
                }
            }
        }
    }


    public static ASPFile findFileInFileBrowser(ASPFileBrowser<?> tmpBrowser, String strFileName, boolean bDirectoryOnly) {
        ASPFile retFile = null;
        ASPFile file;
        int nSize = tmpBrowser.getCount();
        try {
            for (int nCnt = 0; nCnt < nSize; nCnt++) {
                file = tmpBrowser.getFile(nCnt);
                if (file != null && (bDirectoryOnly == false || file.isDirectory()) && strFileName.equals(file.getName())) {
                    retFile = file;
                    break;
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "findFileInFileBrowser() - Exception : " + e.getMessage());
        }
        return retFile;
    }

    private static String makeTargetFolderPathUsingSplitArray(String[] splitedArray, int nDepth, String relativeTarget) {
        boolean isStartWithSlash = false;
        if (relativeTarget.charAt(0) == '/')
            isStartWithSlash = true;
        StringBuffer assembledPath = new StringBuffer();
        if (isStartWithSlash)
            assembledPath.append("/");
        for (int nInnerCnt = 0; nInnerCnt <= nDepth; nInnerCnt++) {
            assembledPath.append(splitedArray[nInnerCnt]);
            if (nInnerCnt != nDepth)
                assembledPath.append("/");
        }
        return assembledPath.toString();
    }

    private String checkTargetCreateFolder(Context context, DeviceSLPF devTarget, String strTargetDirectory, String strNewFolder) throws Exception {
        String strRet = "";
        int nCnt;
        int nMaxRetry = 10;
        for (nCnt = 0; nCnt < nMaxRetry; nCnt++) {
            strRet = CloudGatewayFileBrowserUtils.getInstance(context).mrrControlCommandWithStringReturn(devTarget.getId(), CloudGatewayFileBrowserUtils.REMOTE_FOLDER_MKDIR, strTargetDirectory, null, strNewFolder, null);
            if (strRet == null || strRet.isEmpty()) {
                Log.i(this, "uploadFile failure when create directory=" + strNewFolder + ", retry again cnt=" + nCnt);
                Thread.sleep(1000);
            } else {
                break;
            }
        }
        if (nCnt == nMaxRetry) {
            throw new IOException(CloudStorageError.RETRY_MAX_ERROR);
        }
        return strRet;
    }

    public static String findFolderIDfromRelativePath(String relativeTarget, Map<String, String> folderIdMap) {
        if (folderIdMap != null && folderIdMap.isEmpty() == false) {
            String cachedFolderID = folderIdMap.get(relativeTarget);
            if (cachedFolderID != null) {
                Log.i(TAG, "cachedFolderID=" + cachedFolderID);
                return cachedFolderID;
            }
        }
        return null;
    }

    private boolean isNeedToMakeDir(ASPFileProvider fileProvider, StringBuffer strTargetOld, StringBuffer strTargetDirectory, String[] splitedArray, int nDepth, String relativeTarget) throws Exception {
        try {
            ASPFileBrowserManager.ASPFileBrowserReference tmpBrowserRef = ASPFileBrowserManager.getInstance().getLoadedFileBrowser2(fileProvider, strTargetDirectory.toString(), null, null);
            if (tmpBrowserRef != null) {
                ASPFileBrowser<?> tmpBrowser = tmpBrowserRef.getFileBrowser();
                String strDirName = splitedArray[nDepth];
                ASPFile file = findFileInFileBrowser(tmpBrowser, strDirName, true);
                if (file == null) {
                    boolean bFileListFound = false;
                    if (cacheMapForFolderAnalysis != null && cacheMapForFolderAnalysis.isEmpty() == false) {
                        if (splitedArray.length >= 2) {
                            String assembledPath = makeTargetFolderPathUsingSplitArray(splitedArray, nDepth, relativeTarget);
                            String cachedFolderID = cacheMapForFolderAnalysis.get(assembledPath);
                            if (cachedFolderID != null) {
                                bFileListFound = true;
                                Log.i(this, "isNeedToMakeDir() - cachedFolderID=" + cachedFolderID + "for path=" + assembledPath);
                                strTargetOld.delete(0, strTargetOld.length());
                                strTargetOld.append(strTargetDirectory.toString());
                                strTargetDirectory.delete(0, strTargetDirectory.length());
                                strTargetDirectory.append(cachedFolderID);
                            }
                        }
                    }
                    if (bFileListFound == false) {
                        strTargetOld.delete(0, strTargetOld.length());
                        strTargetOld.append(strTargetDirectory.toString());
                        if (nDepth < splitedArray.length - 1) {
                            if (!(strTargetDirectory.toString()).equals("")) {
                                String assembledPath = makeTargetFolderPathUsingSplitArray(splitedArray, nDepth, relativeTarget);
                                if (cacheMapForFolderAnalysis != null) {
                                    Log.d(this, "isNeedToMakeDir: put cached: key = " + assembledPath);
                                    cacheMapForFolderAnalysis.put(assembledPath, strTargetDirectory.toString());
                                }
                            }

                        } else {
                            return true;
                        }
                    }
                } else {
                    strTargetOld.delete(0, strTargetOld.length());
                    strTargetOld.append(strTargetDirectory.toString());
                    strTargetDirectory.delete(0, strTargetDirectory.length());
                    strTargetDirectory.append(fileProvider.getStorageGatewayFileId(file));
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return false;
    }

    public String checkTargetRelativePathAndReturnNewRelativePath(ASPFile targetAspFile, Context context, final String relativeTarget, boolean bNeedToCheckRename) throws Exception {
        Log.d(this, "checkTargetRelativePathAndReturnNewRelativePath: task = " + this.getClass().getSimpleName() + "-relTarget = " + relativeTarget);
        DeviceSLPF devTarget = getTargetDevice();
        String targetDirectoryPath = getOptions().targetDirectoryPath;

        StringBuffer strTargetDirectory = null;
        final StringBuffer strNewRelatedTarget = new StringBuffer(relativeTarget);

        if (targetDirectoryPath != null) {
            if (devTarget.isLocalDevice()) {
                strTargetDirectory = new StringBuffer(new String(Base64.encode(targetDirectoryPath.getBytes(), Base64.NO_WRAP | Base64.URL_SAFE)));
            } else {
                strTargetDirectory = new StringBuffer(targetDirectoryPath);
            }
            Log.i(this, "checkTargetRelativePathAndReturnNewRelativePath() - targetDirectory2=" + targetDirectoryPath);
        }
        //check cache first
        if (cacheMapForFolderAnalysis != null && cacheMapForFolderAnalysis.isEmpty() == false && TextUtils.isEmpty(relativeTarget) == false) {
            String cachedFolderID = cacheMapForFolderAnalysis.get(relativeTarget);
            if (cachedFolderID != null) {
                Log.i(this, "checkTargetRelativePathAndReturnNewRelativePath() - cachedFolderID=" + cachedFolderID);
                return relativeTarget;
            }
        }
        ASPFileProvider fileProvider = ASPFileProviderFactory.getFileProviderForDevice(context, devTarget);
        if (fileProvider instanceof CachedFileProvider) {
            CachedFileProvider newProvider = (CachedFileProvider) fileProvider;
            newProvider.setNonBlockingBrowser();
        }

        String [] splitedArray = null;
        String child = targetAspFile.getName();
        if (relativeTarget.equals(child)) {
            splitedArray = new String[]{child};
        } else {
            splitedArray = new String[]{relativeTarget.substring(0, relativeTarget.lastIndexOf(child) - 1), child};
        }
        StringBuffer strTargetOld = new StringBuffer(strTargetDirectory);
        for (int nDepth = 0; nDepth < splitedArray.length; nDepth++) {
            isNeedToMakeDir(fileProvider, strTargetOld, strTargetDirectory, splitedArray, nDepth, relativeTarget);
        }

        String strDirName = splitedArray[splitedArray.length - 1];
        final StringBuffer strRenamedDir = new StringBuffer(strDirName);
        final String [] relativeTargetSplitPathArray = splitedArray;
        final boolean needAppend = (relativeTargetSplitPathArray.length > 1) ? true : false;
        if (targetAspFile.isDirectory() && this.getOptions().waitForRename) {
            requestRename(strTargetOld.toString(), strDirName, bNeedToCheckRename, new RenameDataItem.OnReceiveRSPRenameStatus() {
                @Override
                public void onReceiveRenameStatus(RenameDataItem dataItem, String strRenamed) {
                    strRenamedDir.delete(0, strRenamedDir.length());
                    strRenamedDir.append(strRenamed);
                    processRenameDirectoryResult(relativeTargetSplitPathArray, dataItem, strRenamed, strNewRelatedTarget, needAppend);
                }
            });
        }

        if (!mRenameSkipResult.equals(strNewRelatedTarget.toString())) {
            if (TextUtils.equals(strDirName, strRenamedDir.toString())) {
                Log.d(this, "checkTargetRelativePathAndReturnNewRelativePath() create folder: " + strNewRelatedTarget.toString());
                strTargetDirectory = new StringBuffer(checkTargetCreateFolder(context, devTarget, strTargetDirectory.toString(), strDirName));
            } else if (TextUtils.equals(relativeTarget, strNewRelatedTarget)) {
                Log.d(this, "checkTargetRelativePathAndReturnNewRelativePath() replace folder: " + strTargetDirectory.toString());
                strTargetDirectory = new StringBuffer(strTargetDirectory.toString());
            } else {
                Log.d(this, "checkTargetRelativePathAndReturnNewRelativePath() create folder: " + strTargetOld.toString());
                strTargetDirectory = new StringBuffer(checkTargetCreateFolder(context, devTarget, strTargetOld.toString(), strRenamedDir.toString()));
            }
        }

        if (cacheMapForFolderAnalysis != null) {
            Log.d(this, "checkTargetRelativePathAndReturnNewRelativePath: put cached: key = " + strNewRelatedTarget.toString() + "-value: " + strTargetDirectory.toString());
            cacheMapForFolderAnalysis.put(strNewRelatedTarget.toString(), strTargetDirectory.toString());
        }
        return strNewRelatedTarget.toString();
    }

    protected void addDirectoryTask(ASPFile curDirectoryAspFile, String directoryId, String curPath, DeviceSLPF sourceDevice,
                                    ASPFileProvider fileProvider, Map<String, String> cacheFolderID, boolean bNeedToRenameCheck) throws Exception {

        ASPFileSortType sortType = ASPFileSortType.getDefaultSortType();

        Queue<String> dirList = new LinkedList<>();
        Queue<String> curPathList = new LinkedList<>();
        Queue<ASPFile> dirAspFileList = new LinkedList<>();
        dirList.add(directoryId);
        curPathList.add(curPath);
        dirAspFileList.add(curDirectoryAspFile);

        try {
            while (!dirList.isEmpty()) {
                String dirId = dirList.remove();
                String path = curPathList.remove();
                ASPFile curAspFile = dirAspFileList.remove();

                if (mFlagCanceled) {
                    Log.i(this, "addDirectoryTask canceled");
                    return;
                }

                ASPFileBrowserManager.ASPFileBrowserReference tmpBrowserRef = ASPFileBrowserManager.getInstance().getLoadedFileBrowser2(fileProvider, dirId, sortType, null);
                if (tmpBrowserRef != null) {
                    ASPFileBrowser<?> tmpBrowser = tmpBrowserRef.getFileBrowser();

                    int nSize = tmpBrowser.getCount();
                    //add directory it self
                    //task.nFolderNumber++;
                    String strNewPath = checkTargetRelativePathAndReturnNewRelativePath(curAspFile, mContext, path, bNeedToRenameCheck);
                    if (strNewPath != null && strNewPath.equals(path) == false) {
                        bNeedToRenameCheck = false;
                        path = strNewPath;
                    }

                    if (mRenameSkipResult.equals(path)) {
                        Log.i(this, "addDirectoryTask() - RenameAPI: this folder is skipped, directoryId=" + dirId);
                        return;
                    }
                    addSourceFolder(dirId, sourceDevice);
                    if (m_nRenameApplyedAllStatus == CloudGatewayFileTransferUtils.RENAME_STATUS_RSP_CANCEL) {
                        Log.i(this, "addDirectoryTask() - RenameAPI: session is canceled.");
                        return;
                    }

                    Log.i(this, "addDirectoryTask curPath =" + path + " size = " + nSize);
                    for (int nCnt = 0; nCnt < nSize; nCnt++) {
                        ASPFile file = tmpBrowser.getFile(nCnt);
                        if (!file.isDirectory()) {
                            addTask(file, sourceDevice, path, cacheFolderID);
                            Log.i(this, "addDirectoryTask() - file=" + file);
                        } else {
                            String subDirID = fileProvider.getStorageGatewayFileId(file);
                            String newPath = path + "/" + file.getName();

                            dirList.add(subDirID);
                            curPathList.add(newPath);
                            dirAspFileList.add(file);

                            if (this.mFlagCanceled)
                                return;
                            Log.i(this, "addDirectoryTask() - newPath=" + newPath);
                        }
                    }
                }
            }
        } catch (Exception e) {
            throw e;
        }
    }

    protected HashMap<String, String> cacheMapForFolderAnalysis = null;
    protected DeviceSLPF sourceDeviceForFolderAnalysis = null;

    protected ASPFileProvider beginFolderSourceAnalysisBeforeSending() {
        ASPFileProvider fileProvider = null;
        Set<DeviceSLPF> srcSet = getSourceDevices();
        if (srcSet != null) {
            Iterator<DeviceSLPF> it = srcSet.iterator();
            sourceDeviceForFolderAnalysis = null;
            if (it.hasNext()) {
                sourceDeviceForFolderAnalysis = it.next();
            }

            if (mIsFileList && srcSet.size() == 1) {
                fileProvider = ASPFileProviderFactory.getFileProviderForDevice(mContext, sourceDeviceForFolderAnalysis);
                if (fileProvider instanceof CachedFileProvider) {
                    //TBD check google drive error
                    if (sourceDeviceForFolderAnalysis.getWebStorageType() != null) {
                        CachedFileProvider newProvider = (CachedFileProvider) fileProvider;
                        newProvider.setNonBlockingBrowser();
                    }
                }
                cacheMapForFolderAnalysis = new HashMap<>();
            }
        }
        return fileProvider;
    }

    protected void checkSourceFolderBeforeSending(ASPFileProvider fileProvider, String strCloudId, ASPFile srcDirFile, String strFolderName) throws Exception {
        Log.d(this, "checkSourceFolderBeforeSending(): " + fileProvider.getClass().getSimpleName() + "-strCloudID = " + strCloudId
                + "---srcDirFile: " + srcDirFile.getName() + "---folderName: " + strFolderName);
        if (strCloudId != null) {
            addDirectoryTask(srcDirFile, strCloudId, strFolderName, sourceDeviceForFolderAnalysis, fileProvider, cacheMapForFolderAnalysis, true);
        }
    }

    protected void endFolderSourceAnalysisBeforeSending() {
        cacheMapForFolderAnalysis = null;
        sourceDeviceForFolderAnalysis = null;
    }

    protected String getTransferredFileList() {
        return null;
    }

    protected void requestRename(String strTargetDirectoryCloudID, String strFileName, boolean bNeedToCheckRename, RenameDataItem.OnReceiveRSPRenameStatus callback) {
        boolean bSpecialCase = false;
        String strSpecial = renameApiCheckSpecialCharacterCondition(strTargetDirectoryCloudID, strFileName, true);
        if (strSpecial != null && strSpecial.equals(strFileName) == false) {
            bSpecialCase = true;
        }
        String strNewFolder = null;
        if (bSpecialCase) {
            strNewFolder = strSpecial;
        }
        if (bNeedToCheckRename) {
            strNewFolder = renameApiCheckDuplicationCondition(getContext(), getTargetDevice(), strTargetDirectoryCloudID, strSpecial);
        }
        if (bSpecialCase || (bNeedToCheckRename && strNewFolder != null && strNewFolder.equals(strSpecial) == false)) {
            Log.d(this, "requestRename newName = " + strNewFolder + "---strSpecial = " + strSpecial);
            renameApiRequestRenameToApplication(getContext(), getSessionId(), strFileName, strNewFolder, true, bSpecialCase, callback);
        }
    }

    protected void requestRename(String strTargetDirectoryCloudID, String strFileName, RenameDataItem.OnReceiveRSPRenameStatus callback) {
        Log.d(this, "requestRename start");
        boolean bSpecialCase = false;
        String strSpecial = renameApiCheckSpecialCharacterCondition(strTargetDirectoryCloudID, strFileName, false);
        if (strSpecial != null && strSpecial.equals(strFileName) == false)
            bSpecialCase = true;
        String strRenamed = renameApiCheckDuplicationCondition(getContext(), getTargetDevice(), strTargetDirectoryCloudID, strSpecial);

        if (strRenamed != null && strRenamed.equals(strFileName) == false) {
            Log.d(this, "requestRename newName = " + strRenamed + "---strSpecial = " + strSpecial);
            renameApiRequestRenameToApplication(getContext(), getSessionId(), strFileName, strRenamed, false, bSpecialCase, callback);
        }
    }

    protected void processRenameResult(TransferTaskInfo taskInfo, RenameDataItem dataItem, String strNewName) {
    }

    protected void processRenameResult(TransferTaskInfo taskInfo, RenameDataItem dataItem, String strNewName, String targetDirId) {
    }

    private void processRenameDirectoryResult(String [] relativeTargetSplitPathArray, RenameDataItem dataItem, String strNewFolder, StringBuffer strNewRelatedTarget, boolean needAppend) {
        int nRspStatus = 0;
        if (dataItem != null) {
            nRspStatus = dataItem.nStatus;
            if (dataItem.bApplyAll) {
                mbRenameApplyedAll = true;
                m_nRenameApplyedAllStatus = nRspStatus;
            }
        }
        switch (nRspStatus) {
            case CloudGatewayFileTransferUtils.RENAME_STATUS_RSP_RENAME:
                strNewRelatedTarget.delete(0, strNewRelatedTarget.length());
                if (needAppend) {
                    strNewRelatedTarget.append(relativeTargetSplitPathArray[0]);
                    strNewRelatedTarget.append("/" + strNewFolder);
                } else {
                    strNewRelatedTarget.append(strNewFolder);
                }
                break;
            case CloudGatewayFileTransferUtils.RENAME_STATUS_RSP_REPLACE:
                break;
            case CloudGatewayFileTransferUtils.RENAME_STATUS_RSP_SKIP:
                strNewRelatedTarget.delete(0, strNewRelatedTarget.length());
                strNewRelatedTarget.append(mRenameSkipResult);
                break;
            default:
                break;
        }
    }

    public static class TransferTaskInfo {
    }

    public static class TransferItemInfo {
    }

    private BroadcastReceiver getSyncBroadcastReceiver() {
        if (mSyncReceiver == null) {
            mSyncReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    if (intent.getAction().equals(CloudGatewayFileBrowserUtils.SYNC_FINISHED)) {
                        CloudGatewayFileBrowserUtils.SyncResult reason =
                                (CloudGatewayFileBrowserUtils.SyncResult) intent.getSerializableExtra(CloudGatewayFileBrowserUtils.SYNC_FINISHED);
                        if (reason == CloudGatewayFileBrowserUtils.SyncResult.SYNC_SUCCESS && mLevelWait == LEVEL_PREPARE) {
                            mDismissHandler.removeMessages(COMPLETED_OP_CMD);
                            mDismissHandler.sendMessage(mDismissHandler.obtainMessage(COMPLETED_OP_CMD));
                        }
                    }
                }
            };
        }

        return mSyncReceiver;
    }
}


package platform.com.mfluent.asp.filetransfer;

import android.content.Context;
import android.database.Cursor;
import android.text.TextUtils;

import com.mfluent.asp.cloudstorage.api.sync.CloudStorageSync;
import com.mfluent.asp.common.datamodel.ASPFile;
import com.mfluent.asp.common.datamodel.ASPFileProvider;
import com.mfluent.asp.common.datamodel.ASPMediaStore;
import com.mfluent.log.Log;
import com.samsung.android.sdk.slinkcloud.CloudGatewayConstants.OperationType;
import com.samsung.android.sdk.slinkcloud.CloudGatewayFileBrowserUtils;
import com.samsung.android.sdk.slinkcloud.CloudGatewayFileTransferUtils;
import com.samsung.android.sdk.slinkcloud.CloudGatewayFileTransferUtils.TransferOptions;
import com.samsung.android.sdk.slinkcloud.CloudGatewayMediaStore;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import platform.com.mfluent.asp.datamodel.DeviceSLPF;
import platform.com.mfluent.asp.datamodel.filebrowser.CachedCloudFile;
import platform.com.mfluent.asp.datamodel.filebrowser.CachedFileBrowser;
import platform.com.mfluent.asp.util.FileUtils;
import platform.com.mfluent.asp.util.UiUtilsSLPF;
import platform.com.samsung.android.slinkcloud.NetResourceCacheManager;

public class CloudInternalTransferTask extends FileTransferTask {

    private static class CloudInnerItemInfo {
        public String sourceMediaId;
        public ASPFile aspFile = null;
        public ASPFile aspDirectory = null;
        public String storageGatewayFileId = null;
        public String fileName;
        public String mimeType;
        public File targetFile;
        public long length;
        public boolean isDirectory = false;
    }

    private static class CloudInnerTaskInfo extends TransferTaskInfo {
        DeviceSLPF sourceDevice;
        CloudInnerItemInfo mainDownload;
        File targetDirectory;
        String targetFolderID = null;
        boolean skipTransfer = false;
        File tmpFile;
        long totalBytesToTransfer;
        int nTaskId = 0;
        boolean bNeedReplace = false;

        public CloudInnerTaskInfo(int nNewTaskId) {
            nTaskId = nNewTaskId;
        }
    }

    private final ArrayList<CloudInnerTaskInfo> tasks = new ArrayList<>();
    private final Set<CloudInnerTaskInfo> skippedTasks = new HashSet<>();

    private Context context = null;

    private boolean isDownloading = false;

    private boolean bTransferResult = false;

    private int nTaskCount = 0;
    private int nSentFileNum = 0;

    private int mMaxNumTxConnection = 1;

    private OperationType opType = OperationType.NONE;

    public CloudInternalTransferTask(
            Context context,
            DeviceSLPF targetDevice,
            FileTransferTaskListener listener,
            TransferOptions options,
            String strPremadeSession) {
        super(context, targetDevice, listener, strPremadeSession, options);

        this.context = context;
        this.nTaskCount = 0;

        setMultiChannelNum(mMaxNumTxConnection);
    }

    private File getTargetPrivateDirectory() {
        Log.d(this, "getTargetPrivateDirectory() called");

        File tmpF = new File(getContext().getExternalFilesDir(null), ".tmp");
        if (!tmpF.mkdirs()) {
            Log.d(this, "getTargetPrivateDirectory() - Failed make dir tmpF");
        }

        File noMediaF = new File(tmpF, ".nomedia");
        if (!noMediaF.exists()) {
            try {
                noMediaF.createNewFile();
            } catch (IOException e) {
                Log.e(this, "getTargetPrivateDirectory() - IOException : " + e.getMessage());
            }
        }
        return tmpF;
    }

    @Override
    public void addTask(ASPFile file, DeviceSLPF sourceDevice) {
        addTask(file, sourceDevice, null, null);
    }

    @Override
    public boolean addTask(ASPFile file, DeviceSLPF sourceDevice, String relativeTarget, Map<String, String> targetFolderCache) {
        boolean bRet = true;
        Log.d(this, "addTask() - ASPFile : " + file + ", sourceDevice : " + sourceDevice);

        opType = (getOptions().deleteSourceFilesWhenTransferIsComplete) ? OperationType.MOVE : OperationType.COPY;
        CloudInnerTaskInfo taskInfo = new CloudInnerTaskInfo(nTaskCount++);

        taskInfo.mainDownload = new CloudInnerItemInfo();
        if (file instanceof CachedCloudFile) {
            CachedCloudFile cachedFile = (CachedCloudFile) file;
            taskInfo.mainDownload.aspFile = null;
            taskInfo.mainDownload.storageGatewayFileId = cachedFile.getCloudId();
            Log.i(this, "CloudInternalTransfer addFile CachedCloudFile storageGatewayFileId=" + taskInfo.mainDownload.storageGatewayFileId);
        } else {
            taskInfo.mainDownload.aspFile = file;
        }

        if (file.isDirectory()) {
            taskInfo.mainDownload.isDirectory = true;
            taskInfo.mainDownload.aspDirectory = file;
            taskInfo.skipTransfer = (opType == OperationType.COPY);
        }
        taskInfo.mainDownload.fileName = file.getName();
        taskInfo.mainDownload.sourceMediaId = null;
        taskInfo.sourceDevice = sourceDevice;

        String convertedFileName = taskInfo.mainDownload.fileName;
        // convert filename to ASP Media Type
        taskInfo.mainDownload.mimeType = UiUtilsSLPF.getMimeFromFilename(convertedFileName);
        taskInfo.targetDirectory = getTargetPrivateDirectory();
        taskInfo.mainDownload.targetFile = new File(taskInfo.targetDirectory, convertedFileName);
        if (taskInfo.mainDownload.isDirectory) {
            taskInfo.mainDownload.length = ((opType == OperationType.MOVE) ? 1 : 0);
            taskInfo.totalBytesToTransfer = ((opType == OperationType.MOVE) ? 1 : 0);
        } else {
            taskInfo.mainDownload.length = file.length();
            taskInfo.totalBytesToTransfer = file.length();
        }
        if ((opType == OperationType.COPY) && !TextUtils.isEmpty(relativeTarget)) {
            taskInfo.targetFolderID = findFolderIDfromRelativePath(relativeTarget, targetFolderCache);
            if (mRenameSkipResult.equals(taskInfo.targetFolderID))
                bRet = false;
        } else {
            taskInfo.targetFolderID = getOptions().targetDirectoryPath;
        }
        Log.i(this, "taskInfo.targetFolderID = " + taskInfo.targetFolderID);

        setTotalBytesToTransfer(getTotalBytesToTransfer() + 1);

        tasks.add(taskInfo);
        addSourceDevice(sourceDevice);

        return bRet;
    }

    @Override
    public boolean isDownload() {
        return isDownloading;
    }

    @Override
    public int getNumberOfFiles() {
        return tasks.size();
    }

    @Override
    public void _setMultiChannelTransfer() {
        setMultiChannelNum(mMaxNumTxConnection);
    }

    @Override
    public void destroy() {
        for (CloudInnerTaskInfo task : tasks) {
            FileUtils.deleteQuietly(task.tmpFile);
        }
    }

    @Override
    public int getCurrentFileIndex() {
        long totalBytesTransferred = getBytesTransferred();
        long sum = 0;
        int currentFileIndex = 0;
        for (CloudInnerTaskInfo downloadTaskInfo : tasks) {
            sum += downloadTaskInfo.mainDownload.length;
            if (totalBytesTransferred <= sum) {
                break;
            }

            currentFileIndex++;
        }

        return currentFileIndex;
    }

    //multi channel codes
    @Override
    protected void prepareMultiChannel() throws Exception {
        super.prepareMultiChannel();


        if (opType == OperationType.COPY) {
            //check folder to file list to send
            ASPFileProvider fileProvider = beginFolderSourceAnalysisBeforeSending();
            if (fileProvider != null) {
                String strCloudId = null;
                int nSize = tasks.size();
                for (int nCnt = 0; nCnt < nSize; nCnt++) {
                    CloudInnerTaskInfo taskInfo = tasks.get(nCnt);
                    if (taskInfo.mainDownload.isDirectory) {
                        try {
                            if (taskInfo.mainDownload.storageGatewayFileId != null) {
                                strCloudId = taskInfo.mainDownload.storageGatewayFileId;
                            } else if (taskInfo.mainDownload.aspFile != null) {
                                strCloudId = fileProvider.getStorageGatewayFileId(taskInfo.mainDownload.aspFile);
                            }
                        } catch (Exception e) {
                            Log.e(this, "prepareMultiChannel() - Exception : " + e.getMessage());
                            strCloudId = null;
                        }

                        checkSourceFolderBeforeSending(fileProvider, strCloudId, taskInfo.mainDownload.aspDirectory, taskInfo.mainDownload.fileName);

                    }
                }
                endFolderSourceAnalysisBeforeSending();
            }
        }

        //check rename condition

        if (getOptions().waitForRename) {
            for (final CloudInnerTaskInfo taskInfo : tasks) {
                if (isCanceled()) {
                    Log.i(this, "prepareMultiChannel() - canceled");
                    break;
                }
                if ((opType == OperationType.COPY) && taskInfo.mainDownload.isDirectory) {
                    Log.i(this, "skip checkRename " + taskInfo.mainDownload.fileName + " due to dirprecheck done");
                    continue;
                }
                String targetDirId = null;
                if (taskInfo.targetFolderID != null) {
                    targetDirId = taskInfo.targetFolderID;
                } else if (getOptions().targetDirectoryPath != null) {
                    targetDirId = getOptions().targetDirectoryPath;
                }

                final String finalTargetDirId = targetDirId;
                requestRename(targetDirId, taskInfo.mainDownload.fileName, new RenameDataItem.OnReceiveRSPRenameStatus() {
                    @Override
                    public void onReceiveRenameStatus(RenameDataItem dataItem, String strRenamed) {
                        Log.d(this, "onReceiveRenameStatus - strRenamed: " + strRenamed);
                        processRenameResult(taskInfo, dataItem, strRenamed, finalTargetDirId);
                    }
                });
            }
        }
    }

    @Override
    protected void processRenameResult(TransferTaskInfo taskInfo, RenameDataItem dataItem, String strNewName, String targetDirId) {
        int nRspStatus = 0;
        if (dataItem != null) {
            nRspStatus = dataItem.nStatus;
            if (dataItem.bApplyAll) {
                mbRenameApplyedAll = true;
                m_nRenameApplyedAllStatus = nRspStatus;
            }
        }
        if (taskInfo instanceof CloudInnerTaskInfo) {
            CloudInnerTaskInfo task = (CloudInnerTaskInfo) taskInfo;
            if (nRspStatus == CloudGatewayFileTransferUtils.RENAME_STATUS_RSP_RENAME) {
                task.mainDownload.fileName = strNewName;
            } else if (nRspStatus == CloudGatewayFileTransferUtils.RENAME_STATUS_RSP_REPLACE) {
                task.bNeedReplace = true;
                if (!task.mainDownload.isDirectory) {
                    String selection = "_display_name=? and parent_cloud_id=?";
                    String[] selectionArgs = new String[]{task.mainDownload.fileName, targetDirId};
                    Cursor cursor = null;
                    try {
                        String parentId = null;
                        cursor = context.getContentResolver().query(CloudGatewayMediaStore.CloudFiles.getCloudFileUri(task.sourceDevice.getId()), new String[]{ASPMediaStore.Files.FileColumns.PARENT_CLOUD_ID}, ASPMediaStore.Files.FileColumns.SOURCE_MEDIA_ID + "=?", new String[]{task.mainDownload.storageGatewayFileId}, null);
                        if ((cursor != null) && (cursor.getCount() != 0) && cursor.moveToFirst()) {
                            parentId = cursor.getString(cursor.getColumnIndex(ASPMediaStore.Files.FileColumns.PARENT_CLOUD_ID));
                            cursor.close();
                        }
                        if (parentId != null && !parentId.equals(targetDirId)) {
                            cursor = context.getContentResolver().query(CloudGatewayMediaStore.CloudFiles.getCloudFileUri(getTargetDevice().getId()), new String[]{ASPMediaStore.Files.FileColumns.SOURCE_MEDIA_ID}, selection, selectionArgs, null);
                            if ((cursor != null) && (cursor.getCount() != 0) && cursor.moveToFirst()) {
                                String[] cloudIds = new String[cursor.getCount()];
                                int count = 0;
                                do {
                                    cloudIds[count++] = cursor.getString(cursor.getColumnIndex(ASPMediaStore.Files.FileColumns.SOURCE_MEDIA_ID));
                                } while (cursor.moveToNext());
                                CloudGatewayFileBrowserUtils.getInstance(context).mrrControlBatchCommand(getTargetDevice().getId(),CloudGatewayFileBrowserUtils.REMOTE_FOLDER_RMDIR_BATCH, cloudIds, null);
                                cursor.close();
                            }
                        } else {
                            task.skipTransfer = true;
                        }
                    } catch (Exception e) {
                        Log.e(this, "processRenameResult() - Exception : " + e.getMessage());
                    } finally {
                        if (cursor != null && !cursor.isClosed()) {
                            cursor.close();
                        }
                    }
                }
            } else if (nRspStatus == CloudGatewayFileTransferUtils.RENAME_STATUS_RSP_SKIP) {
                Log.i(this, "processRenameResult RENAME_STATUS_RSP_SKIP");
                task.skipTransfer = true;
            }
        }
    }

    @Override
    protected boolean onExitMultiChannel() {
        return bTransferResult;
    }

    @Override
    protected void doThreadSafeTransfer(int nChannelId) throws Exception {
        if (!checkMultiChannelItem(nChannelId, 0)) {
            return;
        }

        ArrayList<String[]> taskList = new ArrayList<>();
        for (CloudInnerTaskInfo task : tasks) {
            if (task != null) {
                String[] fileInfo = new String[3];
                if (task.skipTransfer) {
                    lockMultiChannel();
                    skippedTasks.add(task);
                    unlockMultiChannel();

                    nSentFileNum++;
                } else {
                    fileInfo[0] = task.mainDownload.storageGatewayFileId;
                    fileInfo[1] = task.mainDownload.fileName;
                    fileInfo[2] = task.targetFolderID;
                    taskList.add(fileInfo);
                    Log.d(this, "doThreadSafeTransfer() - taskList.add(" + fileInfo[0] + ", " + fileInfo[1] + ", " + fileInfo[2] + ")");
                }
            }
        }

        if (isCanceled()) {
            Log.d(this, "doThreadSafeTransfer() canceled");
            return;
        }

        CloudStorageSync.Result result = null;
        CloudStorageSync cloudStorageSync = tasks.get(0).sourceDevice.getCloudStorageSync();
        if (getOptions().deleteSourceFilesWhenTransferIsComplete) {
            //Move action
            Log.i(this, "doThreadSafeTransfer() - Move called");
            opType = OperationType.MOVE;
            result = cloudStorageSync.moveFileBatch(taskList, this);

            String parentSrcId = getSrcParentForDevice(tasks.get(0).sourceDevice.getId());
            if (parentSrcId != null) {
                NetResourceCacheManager.getInstance(context).deleteCache(CachedFileBrowser.URI_MATCHER + parentSrcId, null, tasks.get(0).sourceDevice.getId());
            }
        } else {
            //Copy action
            Log.i(this, "doThreadSafeTransfer() - Copy called");
            opType = OperationType.COPY;
            result = cloudStorageSync.copyFileBatch(taskList, this);
        }

        if (result != null) {
            int size = result.mFileIdList.size();
            for (int i = 0; i < size; ++i) {
                CachedFileBrowser.deltaFileLayerAdd(getTargetDevice(), context, result.mFileNameList.get(i), result.mFileDstIdList.get(i), result.mFileIdList.get(i), false);
                NetResourceCacheManager.getInstance(context).deleteCache(CachedFileBrowser.URI_MATCHER + result.mFileDstIdList.get(i), null, tasks.get(0).sourceDevice.getId());
            }
            bTransferResult = result.mRet;
        } else {
            Log.e(this, "doThreadSafeTransfer() - result==null");
            bTransferResult = false;
        }
    }

    //added by shirley for persistent file transfer
    @Override
    public long getSourceMediaId(int index) {
        return 0;//this.tasks.get(index).mainDownload.id;
    }

    @Override
    public int getSentFileNumForMultiChannelSending() {
        return nSentFileNum;
    }

    @Override
    public void cancel() {
        super.cancel();

        DeviceSLPF targetDevice = getTargetDevice();
        if (targetDevice != null) {
            CloudStorageSync cloudStorageSync = targetDevice.getCloudStorageSync();
            cloudStorageSync.cancel(opType);
        }
    }
}

/*
 * Copyright (C) 2012 The Android Open Source Project
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package platform.com.mfluent.asp.util.bitmap;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;

import com.mfluent.asp.common.media.thumbnails.ImageInfo;
import com.mfluent.asp.common.media.thumbnails.ImageInfo.ThumbnailSize;
import com.mfluent.log.Log;

import java.io.FileInputStream;
import java.io.InputStream;


/**
 * A simple subclass of {@link ImageWorker} that resizes images from resources
 * given a target width
 * and height. Useful for when the input images might be too large to simply
 * load directly into
 * memory.
 * 
 * @author MFluent
 * @version 1.5
 */
public class ImageResizerSLPF {

	private static final String TAG = "ImageResizer";

	//    /**
	//     * Decode and sample down a bitmap from resources to the requested width and height.
	//     *
	//     * @param res The resources object containing the image data
	//     * @param resId The resource id of the image data
	//     * @param reqWidth The requested width of the resulting bitmap
	//     * @param reqHeight The requested height of the resulting bitmap
	//     * @return A bitmap sampled down from the original with the same aspect ratio and dimensions
	//     *         that are equal to or greater than the requested width and height
	//     */
	//    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
	//            int reqWidth, int reqHeight) {
	//
	//        // First decode with inJustDecodeBounds=true to check dimensions
	//        final BitmapFactory.Options options = new BitmapFactory.Options();
	//        options.inJustDecodeBounds = true;
	//        BitmapFactory.decodeResource(res, resId, options);
	//
	//        // Calculate inSampleSize
	//        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
	//
	//        // Decode bitmap with inSampleSize set
	//        options.inJustDecodeBounds = false;
	//        return BitmapFactory.decodeResource(res, resId, options);
	//    }

	/**
	 * Decode and sample down a bitmap from a file to the requested width and
	 * height.
	 * 
	 * @param filename
	 *            The full path of the file to decode
	 * @param imageInfo
	 *            the image info
	 * @param options
	 *            the options
	 * @return A bitmap sampled down from the original with the same aspect
	 *         ratio
	 *         and dimensions
	 *         that are equal to or greater than the requested width and height
	 */
	public static Bitmap decodeSampledBitmapFromFile(String filename, ImageInfo imageInfo, BitmapFactory.Options options, Point maxSize)
			throws OutOfMemoryError {
		Log.v(TAG, "decodeSampledBitmapFromFile() : " + "decoding bitmap from file " + filename + " for imageInfo " + imageInfo);

		int reqHeight = imageInfo.getDesiredBitmapHeight();
		int reqWidth = imageInfo.getDesiredBitmapWidth();

		Bitmap bitmap = null;
		InputStream is = null;
		int maxWidth = 0;
		int maxHeight = 0;
		try {
			is = new FileInputStream(filename);

			// First decode with inJustDecodeBounds=true to check dimensions
			options.inJustDecodeBounds = true;

			BitmapFactory.decodeStream(is, null, options);

			is.close();

			// rotation will affect scaling.. need to adjust requested W/H as well
			// NOTE: actual image rotation happens later
			//            int rotation = imageInfo.isRotated() ? 0 : imageInfo.getOrientation();
			//            if (rotation == 90 || rotation == 270)
			//            {
			//                // swap both requested and actual height & width values
			//                int tempH = reqHeight;
			//                reqHeight = reqWidth;
			//                reqWidth = tempH;
			//                tempH = options.outHeight;
			//                options.outHeight = options.outWidth;
			//                options.outWidth = tempH;
			//            }
			if (maxSize != null) {
				maxWidth = maxSize.x;
				maxHeight = maxSize.y;
			}

			// Calculate inSampleSize
			options.inSampleSize = ImageResizerSLPF.calculateInSampleSize(options, reqWidth, reqHeight, maxWidth, maxHeight);

			// Decode bitmap with inSampleSize set
			options.inJustDecodeBounds = false;

			// TODO (JP) allow system to cleanup memory better when images are no longer in use (http://voices.yahoo.com/android-virtual-machine-vm-out-memory-error-7342266.html)
			//options.inTempStorage = new byte[16*1024];
			//options.inPurgeable = true;

			is = new FileInputStream(filename);

			try {
				bitmap = BitmapFactory.decodeStream(is, null, options);
			} catch (OutOfMemoryError e) {
				Log.e(TAG, "OutOfMemoryError: file: "
						+ filename
						+ "; reqWidth: "
						+ reqWidth
						+ "; reqHeight: "
						+ reqHeight
						+ ", sample: "
						+ options.inSampleSize);

				// this is unfortunately a common scenario loading large local images.. better to scale smaller than return null
				is.close();
				is = new FileInputStream(filename);
				int newSample = options.inSampleSize * 2;
				Log.i(TAG, "OutOfMemoryError: retry sample: " + options.inSampleSize + "->" + newSample + ", file: " + filename);
				options.inSampleSize = newSample;
				try {
					bitmap = BitmapFactory.decodeStream(is, null, options);
				} catch (OutOfMemoryError e2) {
					Log.e(TAG, "OutOfMemoryError: file2: "
							+ filename
							+ "; reqWidth: "
							+ reqWidth
							+ "; reqHeight: "
							+ reqHeight
							+ ", sample: "
							+ options.inSampleSize);
					throw e2;
				}
			}
		} catch (Exception e) {
			Log.e(TAG, "Error when decoding bitmap : " + e.getMessage());
			return null;
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (Exception e) {
				}
			}
		}

		// down-sampling an image to lower quality can retain it's original size, which will cause the following when > maxSize
		// >> OpenGLRenderer: Bitmap too large to be uploaded into a texture (5000x5000, max=4096x4096)
		// NOTE: only necessary for FULL_SCREEN images - others will be resized in ImageWorker
		if (imageInfo.getThumbnailSize() == ThumbnailSize.FULL_SCREEN
				&& bitmap != null
				&& (maxWidth > 0 && maxHeight > 0)
				&& (bitmap.getWidth() > maxWidth || bitmap.getHeight() > maxHeight)) {
			Log.d(TAG, "bitmap too large! img: " + bitmap.getWidth() + "x" + bitmap.getHeight() + ", max: " + maxWidth + "x" + maxHeight);
			ImageInfo info = new ImageInfo(imageInfo);
			info.setDesiredBitmapWidth(maxWidth);
			info.setDesiredBitmapHeight(maxHeight);
			bitmap = ImageResizerSLPF.resampleBitmap(bitmap, info);
		}

		return bitmap;
	}

	/**
	 * Calculate an inSampleSize for use in a {@link BitmapFactory.Options} object when decoding
	 * bitmaps using the decode* methods from {@link BitmapFactory}. This
	 * implementation calculates
	 * the closest inSampleSize that will result in the final decoded bitmap
	 * having a width and
	 * height equal to or larger than the requested width and height. This
	 * implementation does not
	 * ensure a power of 2 is returned for inSampleSize which can be faster when
	 * decoding but
	 * results in a larger bitmap which isn't as useful for caching purposes.
	 * 
	 * @param options
	 *            An options object with out* params already populated (run
	 *            through a decode*
	 *            method with inJustDecodeBounds==true
	 * @param reqWidth
	 *            The requested width of the resulting bitmap
	 * @param reqHeight
	 *            The requested height of the resulting bitmap
	 * @return The value to be used for inSampleSize
	 */
	private static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight, int maxWidth, int maxHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {
			if (width > height) {
				inSampleSize = Math.round((float) height / (float) reqHeight);
			} else {
				inSampleSize = Math.round((float) width / (float) reqWidth);
			}

			// This offers some additional logic in case the image has a strange
			// aspect ratio. For example, a panorama may have a much larger
			// width than height. In these cases the total pixels might still
			// end up being too large to fit comfortably in memory, so we should
			// be more aggressive with sample down the image (=larger
			// inSampleSize).

			final float totalPixels = width * height;

			// Anything more than 2x the requested pixels we'll sample down
			// further.
			final float totalReqPixelsCap = reqWidth * reqHeight * 2;

			while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
				inSampleSize++;
			}

			/*
			 * There is a maximum texture width and height supported by android's OpenGL renderer
			 * If we know the max width and height we will further limit the size so that we can
			 * display the rendered image.
			 */
			if ((maxWidth > 0) && (maxHeight > 0)) {
				int widthSampleMax = (int) Math.ceil((double) width / (double) maxWidth);
				int heightSampleMax = (int) Math.ceil((double) height / (double) maxHeight);

				int maxSampleSize = Math.max(widthSampleMax, heightSampleMax);

				if (maxSampleSize > inSampleSize) {
					//We are going to make maxSampleSize a power of two to make sure it scales.
					//According to the documentation, BitmapFactory may not honor a scale factor
					//this is not a power of two. It seems that if it does not honor it, it will 
					//pick the power of two that is lower than what was requested, so we will
					//pick a power of two that is equal or higher.
					int power = 1;
					while (power < maxSampleSize) {
						power *= 2;
					}
					inSampleSize = power;
				}
			}
		}
		return inSampleSize;
	}

	public static Bitmap resampleBitmap(Bitmap bitmap, ImageInfo request) {
		int largestRequestDim = Math.max(request.getDesiredBitmapWidth(), request.getDesiredBitmapHeight());
		int largestActualDim = Math.max(bitmap.getWidth(), bitmap.getHeight());

		Bitmap result = bitmap;
		if (largestRequestDim < largestActualDim) {
			float ratio = ((float) largestRequestDim) / ((float) largestActualDim);

			int newWidth;
			int newHeight;
			if (largestActualDim == bitmap.getWidth()) {
				newWidth = largestRequestDim;
				newHeight = (int) (bitmap.getHeight() * ratio);
			} else {
				newWidth = (int) (bitmap.getWidth() * ratio);
				newHeight = largestRequestDim;
			}

			if (newWidth <= 0 || newHeight <= 0) {
				// if request doesn't have valid height/width set, just return original image
				Log.d(TAG, "resampleBitmap() : desired height/width not set! " + request.getDesiredBitmapWidth() + "x" + request.getDesiredBitmapHeight());
				return bitmap;
			}

			try {
				result = Bitmap.createScaledBitmap(bitmap, newWidth, newHeight, false);
				if (result != null) {
					bitmap.recycle();
					bitmap = null;
				} else {
					result = bitmap;
				}
			} catch (OutOfMemoryError oome) {
				Log.d(TAG, "resampleBitmap() : OutOfMemoryError request:" + request + " - " + oome.getMessage());
			}
		}

		return result;
	}
}


package platform.com.mfluent.asp.datamodel.filebrowser;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.provider.MediaStore;

import com.mfluent.asp.common.datamodel.ASPFile;
import com.mfluent.asp.common.datamodel.ASPFileBrowser;
import com.mfluent.asp.common.datamodel.ASPFileProvider;
import com.mfluent.asp.common.datamodel.ASPFileSortType;
import com.mfluent.asp.common.datamodel.CloudDevice;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import platform.com.mfluent.asp.datamodel.DeviceSLPF;

public class LocalFileProvider implements ASPFileProvider {

    private final Context mContext;
    private final DeviceSLPF mDevice;

    public LocalFileProvider(Context context, DeviceSLPF device) {
        this.mContext = context;
        this.mDevice = device;
    }

    @Override
    public Context getApplicationContext() {
        return this.mContext;
    }

    @Override
    public CloudDevice getCloudDevice() {
        return this.mDevice;
    }

    @Override
    public String getStorageGatewayFileId(ASPFile file) throws FileNotFoundException {
        if (!(file instanceof LocalASPFileSLPF)) {
            throw new IllegalArgumentException("Parameter must be of type " + LocalASPFileSLPF.class.getName());
        }
        return LocalFileBrowser.getStorageGatewayId((LocalASPFileSLPF) file);
    }

    @Override
    public ASPFileBrowser<?> getCloudStorageFileBrowser(String storageGatewayID, ASPFileSortType sort, boolean forceReload, boolean isBrowserForView)
            throws InterruptedException, IOException {
        LocalASPFileSLPF directory = LocalFileBrowser.getFileFromStorageGatewayId(storageGatewayID);
        if (directory != null) {
            File file = directory.getFile();
            if (!file.exists()) {
                throw new FileNotFoundException("File does not exist: " + file.getAbsolutePath());
            }
            if (!file.isDirectory()) {
                throw new IllegalArgumentException("File is not a directory: " + file.getAbsolutePath());
            }
        }
        LocalFileBrowser localFileBrowser = new LocalFileBrowser();

        localFileBrowser.init(directory, sort, forceReload);

        return localFileBrowser;
    }

    @Override
    public int deleteFiles(String directoryStorageGatewayId, ASPFileSortType sortType, String... storageGatewayFileIdsToDelete) {
        int deleteCount = 0;
        boolean result = false;

        ContentResolver cr = this.mContext.getContentResolver();
        String[] projection = new String[]{MediaStore.MediaColumns._ID, MediaStore.MediaColumns.DATA};
        String selection = MediaStore.MediaColumns.DISPLAY_NAME + "=?";
        String[] selectionArgs = new String[1];

        LocalASPFileSLPF aspDirectory = LocalFileBrowser.getFileFromStorageGatewayId(directoryStorageGatewayId);
        if (aspDirectory != null) {
            File directory = aspDirectory.getFile();

            if (!directory.exists()) {
                result = true;
                deleteCount = storageGatewayFileIdsToDelete.length;
            } else {
                ArrayList<File> toDelete = new ArrayList<File>(storageGatewayFileIdsToDelete.length);
                for (String storageGatewayFileId : storageGatewayFileIdsToDelete) {
                    LocalASPFileSLPF aspFile = LocalFileBrowser.getFileFromStorageGatewayId(storageGatewayFileId);
                    if (aspFile != null) {
                        File file = aspFile.getFile();
                        if (!ObjectUtils.equals(file.getParentFile(), directory)) {
                            throw new IllegalArgumentException("File to delete: "
                                    + file.getAbsolutePath()
                                    + " is not a child of directory:"
                                    + directory.getAbsolutePath());
                        }
                        if (file.exists()) {
                            toDelete.add(file);
                        } else {
                            deleteCount++;
                        }
                    }
                }

                result = true;
                for (File file : toDelete) {
                    Cursor mediaCursor = null;
                    try {
                        String canonicalPath = file.getCanonicalPath();
                        selectionArgs[0] = file.getName();
                        mediaCursor = cr.query(MediaStore.Files.getContentUri("external"), projection, selection, selectionArgs, null);
                        int deleteId = -1;
                        if (mediaCursor != null) {
                            while (mediaCursor.moveToNext()) {
                                String path = mediaCursor.getString(1);
                                if (StringUtils.isEmpty(path)) {
                                    continue;
                                }
                                try {
                                    String mediaCanonicalPath = new File(path).getCanonicalPath();
                                    if (canonicalPath.equals(mediaCanonicalPath)) {
                                        deleteId = mediaCursor.getInt(0);
                                        break;
                                    }
                                } catch (IOException ioe) {

                                }
                            }
                            if (mediaCursor.getCount() == 1) {
                                mediaCursor.moveToFirst();
                                deleteId = mediaCursor.getInt(0);
                            }
                        }

                        if (deleteId > 0) {
                            deleteCount += cr.delete(MediaStore.Files.getContentUri("external", deleteId), null, null);
                        }
                    } catch (IOException ioe) {

                    } finally {
                        if (mediaCursor != null) {
                            mediaCursor.close();
                        }
                    }
                }
                for (File file : toDelete) {
                    result = file.delete();
                    if (!result) {
                        break;
                    } else {
                        deleteCount++;
                    }
                }
            }
        }

        return deleteCount;
    }

    //jsub12_151006
    @Override
    public int getCountOfChild(ASPFile file_param) {
        return 0;
    }

    //jsub12_151006
    @Override
    public int getCountOfChildDir(ASPFile file_param) {
        return 0;
    }

    @Override
    public int getCountOfDescendants(ASPFile var1) {
        return 0;
    }

    @Override
    public int getCountOfDescendantDir(ASPFile var1) {
        return 0;
    }

    @Override
    public String getTrashProcessingDir(ASPFile file) {
        return null;
    }
}

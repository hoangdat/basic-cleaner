/**
 * 
 */

package platform.com.mfluent.asp.filetransfer;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

/**
 * @author Ilan Klinghofer
 */
public class FileTransferInfo {

	private final int mProgress;

	private final boolean mIsDownload;

	public FileTransferInfo(
			int progress,
			boolean isDownload) {

		mProgress = progress;
		mIsDownload = isDownload;
	}

	public int getProgress() {
		return mProgress;
	}

	public boolean isDownload() {
		return mIsDownload;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}
}

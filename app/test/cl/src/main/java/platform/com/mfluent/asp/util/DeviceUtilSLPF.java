/**
 * Copyright (c) 2009 Samsung Electronics, Inc.
 * All right reserved.
 * This software is the confidential and proprietary information of Samsung
 * Electronics, Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with Samsung Electronics.
 * Revision History
 * Author Date Description
 * ------------------ -------------- ------------------
 * Min-Cheol Kang 2011.04.07 First Draft.
 */

package platform.com.mfluent.asp.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.TelephonyManager;

import com.mfluent.log.Log;
import com.samsung.android.sdk.slinkcloud.CloudGatewayNetworkMode;

/**
 * Device ID(IMEI) & Network Status Util
 *
 * @author Min-Cheol Kang
 * @version 1.0
 */
public class DeviceUtilSLPF {
    private static final String TAG = "DeviceUtil";

    public static final String SAMSUNGDRIVE_ALIAS_NAME = "Samsung Drive";

    public static final String SAMSUNGDRIVE_WEBSTORAGE_NAME = "samsungdrive";
    public static final String GOOGLEDRIVE_WEBSTORAGE_NAME = "googledrive";

    public static CloudGatewayNetworkMode getNetworkType(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetInfo = connectivityManager.getActiveNetworkInfo();

        CloudGatewayNetworkMode netType = CloudGatewayNetworkMode.OFF;

        if (activeNetInfo != null) {
            if (activeNetInfo.getState() == NetworkInfo.State.CONNECTED) {
                switch (activeNetInfo.getType()) {
                    case ConnectivityManager.TYPE_MOBILE:
                        Log.d(TAG, "Network - MOBILE connected");
                        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
                        netType = TypeManagerSLPF.checkMobileType(telephonyManager.getNetworkType());
                        break;

                    case ConnectivityManager.TYPE_WIFI:
                        Log.d(TAG, "Network - wifi connected");
                        netType = CloudGatewayNetworkMode.WIFI;
                        break;

                    case ConnectivityManager.TYPE_BLUETOOTH:
                        Log.d(TAG, "Network - bluetooth connected");
                        netType = CloudGatewayNetworkMode.MOBILE_2G;
                        break;

                    default:
                        Log.e(TAG, "Unknown Network Type! type-" + activeNetInfo.getTypeName());
                        netType = CloudGatewayNetworkMode.MOBILE_2G;
                        break;
                }
            }
        }
        return netType;
    }
}

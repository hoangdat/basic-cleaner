
package platform.com.mfluent.asp.datamodel.filebrowser;

import org.apache.commons.lang3.StringUtils;

import android.os.Bundle;

import com.mfluent.asp.common.datamodel.ASPFile;

public class CachedCloudFile extends ASPFileWithSpecialType {

    private String name = StringUtils.EMPTY;
    private boolean isDirectory;
    private long length;
    private long lastModified;

    private String m_strCloudId = "";
    private Bundle m_objEtcData = null;
    private ASPFile m_orgCloudFile = null;
    private int nDeviceId = 0;

    public CachedCloudFile() {
        super();
    }

    public CachedCloudFile(CachedCloudFile copyFile) {
        super();
        m_orgCloudFile = copyFile.m_orgCloudFile;
        isDirectory = copyFile.isDirectory();
        length = copyFile.length();
        lastModified = copyFile.lastModified();
        name = copyFile.getName();
        m_objEtcData = (Bundle) copyFile.getEtcDataObject();
        m_strCloudId = copyFile.getCloudId();
    }

    public CachedCloudFile(ASPFile rawFile) {
        super();
        m_orgCloudFile = rawFile;
        isDirectory = rawFile.isDirectory();
        length = rawFile.length();
        lastModified = rawFile.lastModified();
        name = rawFile.getName();
    }

    /*
         * (non-Javadoc)
         * @see com.mfluent.asp.datamodel.ASPFile#getName()
         */
    @Override
    public String getName() {
        return this.name;
    }

    /*
     * (non-Javadoc)
     * @see com.mfluent.asp.datamodel.ASPFile#isDirectory()
     */
    @Override
    public boolean isDirectory() {
        return this.isDirectory;
    }

    /*
     * (non-Javadoc)
     * @see com.mfluent.asp.datamodel.ASPFile#length()
     */
    @Override
    public long length() {
        return this.length;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = StringUtils.defaultString(name);
    }

    /**
     * @param isDirectory the isDirectory to set
     */
    public void setDirectory(boolean isDirectory) {
        this.isDirectory = isDirectory;
    }

    /**
     * @param length the length to set
     */
    public void setLength(long length) {
        this.length = length;
    }

    /*
     * (non-Javadoc)
     * @see com.mfluent.asp.datamodel.ASPFile#lastModified()
     */
    @Override
    public long lastModified() {
        return this.lastModified;
    }

    /**
     * @param lastModified the lastModified to set
     */
    public void setLastModified(long lastModified) {
        this.lastModified = lastModified;
    }

    @Override
    public String toString() {
        if (isDirectory()) {
            return "\"" + getName() + "\"";
        } else {
            return "file: " + getName() + ", len: " + length();
        }
    }

    public void setCloudId(String nCloudId) {
        this.m_strCloudId = nCloudId;
    }

    public String getCloudId() {
        return this.m_strCloudId;
    }

    public void setEtcDataObject(Object objEtcData) {
        this.m_objEtcData = (Bundle) objEtcData;
    }

    public Object getEtcDataObject() {
        return this.m_objEtcData;
    }

    public void setDeviceId(int nDevId) {
        nDeviceId = nDevId;
    }

    public int getDeviceId() {
        return nDeviceId;
    }

}

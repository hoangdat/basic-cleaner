/**
 *
 */

package platform.com.mfluent.asp.filetransfer;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.media.MediaScannerConnection.OnScanCompletedListener;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.LruCache;

import com.mfluent.asp.common.datamodel.ASPFile;
import com.mfluent.asp.common.datamodel.ASPFileBrowser;
import com.mfluent.asp.common.datamodel.ASPFileProvider;
import com.mfluent.asp.common.datamodel.ASPFileSortType;
import com.mfluent.log.Log;
import com.samsung.android.sdk.slinkcloud.CloudGatewayFileTransferUtils;
import com.samsung.android.sdk.slinkcloud.CloudGatewayFileTransferUtils.TransferOptions;
import com.samsung.android.sdk.slinkcloud.CloudGatewayMediaSet;
import com.samsung.android.sdk.slinkcloud.CloudGatewayMediaStore;

import org.json.JSONArray;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import platform.com.mfluent.asp.datamodel.DataModelSLPF;
import platform.com.mfluent.asp.datamodel.DeviceSLPF;
import platform.com.mfluent.asp.datamodel.filebrowser.ASPFileBrowserManager;
import platform.com.mfluent.asp.datamodel.filebrowser.ASPFileBrowserManager.ASPFileBrowserReference;
import platform.com.mfluent.asp.datamodel.filebrowser.ASPFileProviderFactory;
import platform.com.mfluent.asp.datamodel.filebrowser.CachedFileProvider;
import platform.com.mfluent.asp.datamodel.filebrowser.LocalASPFileSLPF;
import platform.com.mfluent.asp.dws.SaveFilesSession;
import platform.com.mfluent.asp.framework.IASPApplication2;
import platform.com.mfluent.asp.framework.ServiceLocatorSLPF;
import platform.com.mfluent.asp.util.CloudGatewayMediaSetHelper;
import platform.com.mfluent.asp.util.FileUtils;
import platform.com.mfluent.asp.util.MFLStorageManagerSLPF;
import uicommon.com.mfluent.asp.util.CachedExecutorService;

public class FileTransferManagerImpl implements FileTransferManager, FileTransferTaskListener {

    private static final String FILE_TRANSFER_IN_PROGRESS = "fileTransferInProgress";

    private static final int PREVIOUS_TRANSFER_FAILED_NOTIFICATION_ID = 1;

    private final Context mContext;

    private final ExecutorService executorService = Executors.newSingleThreadExecutor();

    private final ExecutorService executorMultiService = Executors.newCachedThreadPool();

    private final Lock tasksLock = new ReentrantLock();

    private BroadcastReceiver cancelReceiver;

    private BroadcastReceiver retryReceiver;

    private final LruCache<String, FileTransferTask> retryCache = new LruCache<String, FileTransferTask>(10) {

        @Override
        protected void entryRemoved(boolean evicted, String key, FileTransferTask oldValue, FileTransferTask newValue) {
            if (evicted) {
                oldValue.destroy();
            }
        }
    };

    private final LruCache<String, SaveFilesSession> saveFileSessions = new LruCache<String, SaveFilesSession>(20) {

        @Override
        protected void entryRemoved(boolean evicted, String key, SaveFilesSession oldValue, SaveFilesSession newValue) {
            oldValue.cleanup();
        }
    };

    // save errorOccurredCache in auto-upload
    private final LruCache<String, FileTransferTask> errorOccurredCache = new LruCache<String, FileTransferTask>(10);

    private final Set<FileTransferTask> activeTasks = new HashSet<FileTransferTask>();

    private static final String FILE_TRANSFER_INFO_ARRAY = "fileTransferInfoArray";

    private final JSONArray fileTransferInfoArray = new JSONArray();

    private final File cacheDir;

    public FileTransferManagerImpl(Context context, boolean registerBroadcastReceiver) {
        mContext = context;
        cacheDir = new File(MFLStorageManagerSLPF.getCacheDir(mContext), "filetransfer_cache");
        cacheDir.mkdirs();

        if (registerBroadcastReceiver) {
            IntentFilter cancelIntentFilter = new IntentFilter();
            cancelIntentFilter.addAction(FileTransferManager.CANCEL);

            cancelReceiver = new BroadcastReceiver() {

                @Override
                public void onReceive(Context context, Intent intent) {
                    Bundle extras = intent.getExtras();
                    if (extras != null) {
                        String sessionId = extras.getString("sessionId");
                        boolean isDeleteSessionAfterCancel = extras.getBoolean("isDeleteSessionAfterCancel", false);
                        cancel(sessionId, isDeleteSessionAfterCancel);
                    }
                }
            };
            LocalBroadcastManager.getInstance(context).registerReceiver(cancelReceiver, cancelIntentFilter);

            IntentFilter retryIntentFilter = new IntentFilter();
            retryIntentFilter.addAction(FileTransferManager.RETRY);

            retryReceiver = new BroadcastReceiver() {

                @Override
                public void onReceive(Context context, Intent intent) {
                    Bundle extras = intent.getExtras();
                    if (extras != null) {
                        String sessionId = extras.getString("sessionId");
                        boolean isAutoUpload = extras.getBoolean("isAutoUpload", false);

                        if (!isAutoUpload) {
                            retry(sessionId);
                        } else {
                            retryAutoUpload(sessionId);
                        }
                    }
                }
            };
            LocalBroadcastManager.getInstance(context).registerReceiver(retryReceiver, retryIntentFilter);
        }
    }

    @Override
    public void cleanupCacheFiles() {
        try {
            FileUtils.cleanDirectory(cacheDir);
            ////tskim clear gara thumbnail
            CachedExecutorService.getInstance().execute(new Runnable() {

                @Override
                public void run() {
                    try {
                        Thread.sleep(5000);
                        IASPApplication2 objApp = ServiceLocatorSLPF.get(IASPApplication2.class);
                        if (objApp != null) {
                            objApp.getApplicationContext().getContentResolver().delete(CloudGatewayMediaStore.Files.CONTENT_URI, "is_loading >= ?", new String[]{"1"});
                            final ContentValues values = new ContentValues();
                            values.put("is_loading", 0);
                            objApp.getApplicationContext().getContentResolver().update(CloudGatewayMediaStore.Files.CONTENT_URI, values, "is_loading < ?", new String[]{"0"});
                        }
                    } catch (Exception e) {
                        Log.e(this, "cleanupCacheFile() - Exception : " + e.getMessage());
                    }
                }
            });

        } catch (IOException e) {
            Log.e(this, "cleanupCacheFiles() - Trouble cleaning up cache directory " + cacheDir + ", IOException : " + e);
        } catch (IllegalArgumentException e) {
            Log.e(this, "cleanupCacheFiles() - Trouble cleaning up cache directory " + cacheDir + ", IllegalArgumentException : " + e);
        }
    }

    /*
     * (non-Javadoc)
     * @see com.mfluent.asp.filetransfer.FileTransferManager#cancelAll()
     */
    @Override
    public void cancelAll(boolean isDeleteSessionAfterCancel) {
        tasksLock.lock();
        try {
            HashSet<FileTransferTask> activeTasksSnapshot = new HashSet<FileTransferTask>(activeTasks);
            for (FileTransferTask task : activeTasksSnapshot) {
                task.cancel();
                if (isDeleteSessionAfterCancel) {
                    activeTasks.remove(task);
                }
            }

            for (SaveFilesSession saveFilesSession : saveFileSessions.snapshot().values()) {
                Intent cancelIntent = new Intent(FileTransferManager.CANCEL);
                cancelIntent.putExtra("sessionId", saveFilesSession.getSessionId());
                cancelIntent.putExtra("isDeleteSessionAfterCancel", isDeleteSessionAfterCancel);
                LocalBroadcastManager.getInstance(mContext).sendBroadcast(cancelIntent);
            }
        } finally {
            tasksLock.unlock();
        }
    }

    /*
     * (non-Javadoc)
     * @see com.mfluent.asp.filetransfer.FileTransferManager#cancelAllForDevice(com.mfluent.asp.datamodel.Device)
     */
    @Override
    public void cancelAllForDevice(DeviceSLPF device) {
        if (device != null) {
            if (device.isLocalDevice()) {
                cancelAll(false);
            } else {
                tasksLock.lock();
                try {
                    HashSet<FileTransferTask> activeTasksSnapshot = new HashSet<FileTransferTask>(activeTasks);
                    for (FileTransferTask task : activeTasksSnapshot) {
                        Boolean checkDevice;
                        if (task.isDownload()) {
                            checkDevice = task.getSourceDevices().contains(device);
                        } else {
                            checkDevice = device.equals(task.getTargetDevice());
                        }
                        if (checkDevice) {
                            cancel(task.getSessionId(), false);
                        }
                    }
                } finally {
                    tasksLock.unlock();
                }
            }
        }
    }

    public boolean checkCancelable(String sessionId) {
        boolean bRet = false;
        tasksLock.lock();
        try {
            Log.d(this, "checkCancelable() - size : " + activeTasks.size());

            for (FileTransferTask task : activeTasks) {
                if (task.getSessionId().equals(sessionId)) {
                    bRet = !((task.getNumberOfFiles() <= 2) && (task.getProgress() > 90));
                    break;
                }
            }
        } catch (Exception e) {
            Log.e(this, "checkCancelable() - Exception : " + e.getMessage());
        } finally {
            tasksLock.unlock();
        }
        return bRet;
    }

    @Override
    public void cancel(String sessionId, boolean isDeleteSessionAfterCancel) {
        Log.i(this, "cancel task is 1, sessionId=" + sessionId);
        tasksLock.lock();
        try {
            FileTransferTask taskToCancel = null;
            for (FileTransferTask task : activeTasks) {
                if (task.getSessionId().equals(sessionId)) {
                    taskToCancel = task;
                    break;
                }
            }
            Log.i(this, "cancel task is 2, sessionId=" + sessionId);
            if (taskToCancel != null) {
                Log.i(this, "isDeleteSessionAfterCancel sessionId=" + sessionId);
                taskToCancel.cancel();
                //Pending session should be written in DB
                if (!taskToCancel.isInProgress() && !taskToCancel.isTransferStarted()) {
                    stateChanged(taskToCancel);
                }
            } else {
                for (SaveFilesSession saveFilesSession : saveFileSessions.snapshot().values()) {
                    if (saveFilesSession.getSessionId().equals(sessionId)) {
                        Log.i(this, "cancelling savesession sessionId=" + sessionId);
                        Intent cancelIntent = new Intent(FileTransferManager.CANCEL);
                        cancelIntent.putExtra("sessionId", saveFilesSession.getSessionId());
                        cancelIntent.putExtra("isDeleteSessionAfterCancel", isDeleteSessionAfterCancel);
                        LocalBroadcastManager.getInstance(mContext).sendBroadcast(cancelIntent);
                    }
                }
            }
        } finally {
            tasksLock.unlock();
        }
        Log.i(this, "cancel task is 3, sessionId=" + sessionId);
    }

    @Override
    public void cancelAllForAutoUpload() {
        tasksLock.lock();
        try {
            removeAllErrorOccurredCache();
        } finally {
            tasksLock.unlock();
        }
    }

    @Override
    public FileTransferTask retry(String sessionId) {
        tasksLock.lock();
        try {
            FileTransferTask retryTask = retryCache.remove(sessionId);
            if (retryTask != null) {
                retryTask.reset();
                Log.d(this, "retry: " + sessionId + ", transferId: " + retryTask + ", retryCache.size: " + retryCache.size());
                schedule(retryTask);
                return retryTask;
            } else {
                Log.w(this, "retry unknown transferId: " + sessionId + ", retryCache.size: " + retryCache.size());
            }
        } finally {
            tasksLock.unlock();
        }

        return null;
    }

    /*
     * (non-Javadoc)
     * @see com.mfluent.asp.filetransfer.FileTransferTaskListener#stateChanged(com.mfluent.asp.filetransfer.FileTransferTask)
     */
    @Override
    public void stateChanged(FileTransferTask task) {
        if (!task.isInProgress()) {
            tasksLock.lock();
            try {
                activeTasks.remove(task);

                if (task.errorOccurred()) {
                    retryCache.put(task.getSessionId(), task);
                } else if (!task.getHasWrittenComplete()) {
                    task.setHasWrittenComplete(true);
                }
                updateInProgressPref();
            } finally {
                tasksLock.unlock();
            }

            ////auto retry file transfer
            Intent broadcastIntent = new Intent(TRANSFER_COMPLETED);
            broadcastIntent.putExtra(TRANSFER_COMPLETE_DEVICE_ID, task.getTargetDevice().getId());
            LocalBroadcastManager mgr = LocalBroadcastManager.getInstance(mContext);
            mgr.sendBroadcast(broadcastIntent);
        } else {
            if (!task.getHasNotifiedServiceOfStart()) {
                task.setHasNotifiedServiceOfStart(true);// autoupload do not use ongoing
            }
        }
    }

    /*
     * (non-Javadoc)
     * @see com.mfluent.asp.io.util.StreamProgressListener#bytesTransferred(int)
     */
    @Override
    public void bytesTransferred(long numberOfBytes) {
    }

    private void schedule(FileTransferTask task) {
        NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(FileTransferManagerImpl.class.getName(), PREVIOUS_TRANSFER_FAILED_NOTIFICATION_ID);
        tasksLock.lock();
        try {
            task.setInProgress(true);
            stateChanged(task);

            Future<?> future;
            if (task.getOptions().transferImmediately) {
                future = executorMultiService.submit(task);
            } else {
                future = executorService.submit(task);
            }

            task.setFuture(future);
            activeTasks.add(task);

            //Send Broadcast
            task.sendStatusBroadcastForNativeApplication(CloudGatewayFileTransferUtils.SESSION_STATUS_PREPARE);

            updateInProgressPref();
            task.setHasNotifiedServiceOfStart(true);
//                notifyService(task.isAutoUpload(), task.getOptions().deleteSourceFilesWhenTransferIsComplete);
        } finally {
            tasksLock.unlock();
        }
    }

    ////autoretry file transfer
    private FileTransferTask buildFileTransferTask(
            CloudGatewayMediaSet mediaSet,
            DeviceSLPF targetDevice,
            OnScanCompletedListener onScanCompletedListener,
            TransferOptions options,
            String strPremadeSession) {

        Log.d(this, "buildFileTransferTask(mediaSet: " + mediaSet
                + ", targetDevice: " + targetDevice
                + ", onScanCompletedListener, options)");

        FileTransferTask task;
        if (targetDevice.isLocalDevice()) {
            task = new DownloadTask(mContext, this, onScanCompletedListener, cacheDir, options, strPremadeSession);
        } else {
            //either upload or 3box
            DeviceSLPF sourceDevice = new CloudGatewayMediaSetHelper().getSourceDevice(mContext.getContentResolver(), mediaSet, DataModelSLPF.getInstance());
            Log.d(this, "buildFileTransferTask() : sourceDevice: " + sourceDevice);

            if (sourceDevice == null) {
                throw new IllegalArgumentException("can't upload or 3box transfer from multiple source devices");
            } else if (sourceDevice.isLocalDevice()) {
                task = new UploadTask(mContext, targetDevice, this, strPremadeSession, options);
            } else {
                if (targetDevice.getWebStorageType() != null && targetDevice.getWebStorageType().equals(sourceDevice.getWebStorageType())) {
                    task = new CloudInternalTransferTask(mContext, targetDevice, this, options, strPremadeSession);
                } else {
                    task = new C2CTransferTask(mContext, targetDevice, this, cacheDir, options, strPremadeSession);
                }
            }
        }

        return task;
    }

    @Override
    public void transfer(CloudGatewayMediaSet mediaSet, DeviceSLPF targetDevice, TransferOptions options, String strPremadeSession) {
        Log.d(this, "transfer(mediaSet, targetDevice: " + targetDevice + ")");
        try {
            FileTransferTask task = buildFileTransferTask(mediaSet, targetDevice, null, options, strPremadeSession);
            transfer(task, mediaSet);
        } catch (IllegalArgumentException e) {
            Log.e(this, "transfer() - IllegalArgumentException : " + e);
        }
    }

    private void transfer(final FileTransferTask task, CloudGatewayMediaSet mediaSet) {
        Log.d(this, "transfer(task, mediaSet).....>>");
        //P151123-03345 ~
        DataModelSLPF datamodel = DataModelSLPF.getInstance();

        if (mediaSet.isLocalFilePathsMediaSet()) {
            //mediaSet = transformFromLocalFilePathToFileBrowserSet(mediaSet);

            Log.d(this, "Enter FileTransferManagerImpl::transfer(task, mediaSet), localFilePathsMediaSet");
            DeviceSLPF sourceDevice = datamodel.getDeviceById(1);
            for (String filePath : mediaSet.getLocalFilePaths()) {
                Log.d(this, "transfer(task, mediaSet), local file path = " + filePath);
                ASPFile file = new LocalASPFileSLPF(new File(filePath));
                if (!file.isDirectory()) {
                    task.addTask(file, sourceDevice);
                } else {
                    task.addTask(file, sourceDevice, file.getName(), null);
                }
            }
        } else if (CloudGatewayMediaStore.FileBrowser.FileList.isFileListUri(mediaSet.getUri())) {
//			DataModelSLPF datamodel = DataModelSLPF.getInstance();
            DeviceSLPF sourceDevice = datamodel.getDeviceById(CloudGatewayMediaStore.FileBrowser.FileList.getDeviceIdFromUri(mediaSet.getUri()));

            Log.d(this, "transfer() - sourceDevice: " + sourceDevice);

            String storageDirectoryId = CloudGatewayMediaStore.FileBrowser.FileList.getDirectoryIdFromUri(mediaSet.getUri());
            if (sourceDevice != null && storageDirectoryId != null) {
                task.addMapSrcParentForDevice(sourceDevice.getId(), storageDirectoryId);
            }

            ASPFileSortType sortType = ASPFileSortType.getSortTypeFromCursorSortOrder(mediaSet.getSortOrder());
            if (sortType == null) {
                sortType = ASPFileSortType.getDefaultSortType();
            }

            ASPFileBrowserReference fileBrowserRef = null;

            Log.i(this, "transfer using filebrowser mediaset!");
            try {
                ASPFileProvider fileProvider = ASPFileProviderFactory.getFileProviderForDevice(mContext, sourceDevice);
                if (fileProvider instanceof CachedFileProvider) {
                    //TBD check google drive error
                    if (sourceDevice != null && sourceDevice.getWebStorageType() != null) {
                        CachedFileProvider newProvider = (CachedFileProvider) fileProvider;
                        newProvider.setNonBlockingBrowser();
                    }
                }
                fileBrowserRef = ASPFileBrowserManager.getInstance().getLoadedFileBrowser2(fileProvider, storageDirectoryId, sortType, null);

                if (fileBrowserRef != null) {
                    int nCheckFileCount = 0;
                    String[] sortedIds = mediaSet.getIds().clone();
                    Arrays.sort(sortedIds);
                    ASPFileBrowser<?> fileBrowser = fileBrowserRef.getFileBrowser();
                    int count = fileBrowser.getCount();
                    for (int i = 0; i < count; ++i) {
                        ASPFile file = fileBrowser.getFile(i);
                        if (!file.isDirectory()) {
                            String fileId = fileProvider.getStorageGatewayFileId(file);
                            Log.i(this, "file id=" + fileId);
                            if ((Arrays.binarySearch(sortedIds, fileId) >= 0) == mediaSet.isInclude()) {
                                task.addTask(file, sourceDevice);
                                nCheckFileCount++;
                                Log.i(this, "file:nCheckFileCount=" + nCheckFileCount);
                            }
                        } else {
                            String directoryId = fileProvider.getStorageGatewayFileId(file);
                            Log.i(this, "dir id=" + directoryId);
                            if ((Arrays.binarySearch(sortedIds, directoryId) >= 0) == mediaSet.isInclude()) {
                                task.addTask(file, sourceDevice, file.getName(), null);
                                nCheckFileCount++;
                            }
                        }
                        if (nCheckFileCount >= sortedIds.length) {
                            break;
                        }
                    }
                }
            } catch (RuntimeException runE) {
                Log.e(this, "transfer() - RuntimeException : " + runE.getMessage());
            } catch (Exception e) {
                Log.e(this, "Unable to initiate FileBrowser based transfer for:" + mediaSet + ", Exception : " + e);
            } finally {
                if (fileBrowserRef != null && sourceDevice != null) {
                    Log.d(this, "releaseLoadedFileBrowser : " + fileBrowserRef + " ,deviceId : " + sourceDevice.getId() + " ,dirID : " + storageDirectoryId + " ,sortType : " + sortType);
                    ASPFileBrowserManager.getInstance().releaseLoadedFileBrowser(fileBrowserRef, sourceDevice.getId(), storageDirectoryId, sortType, null);
                }
                try {
                    if (task.getSourceDevices().size() == 0) {
                        task.addSourceDevice(sourceDevice);
                    }
                } catch (Exception e) {
                    Log.e(this, "transfer() - Exception : " + e.getMessage());
                }
            }
        }

        if (task != null) {
            task.mIsFileList = true;
            schedule(task);
        } else {
            Log.e(this, "transfer() - task is null");
        }
    }

    /*
     * (non-Javadoc)
     * @see com.mfluent.asp.filetransfer.FileTransferManager#isTransferInProgress()
     */
    @Override
    public boolean isTransferInProgress() {
        boolean bRet = false;

        tasksLock.lock();
        try {
            bRet = !activeTasks.isEmpty() || getActiveSaveFilesSessionsCount() > 0;
        } catch (Exception e) {
            Log.e(this, "isTransferInProgress() - Exception : " + e.getMessage());
        } finally {
            tasksLock.unlock();
        }

        return bRet;
    }

    private void updateInProgressPref() {
        SharedPreferences preferences = mContext.getSharedPreferences(IASPApplication2.PREFERENCES_NAME, Activity.MODE_PRIVATE);
        boolean pref = preferences.getBoolean(FILE_TRANSFER_IN_PROGRESS, false);
        boolean inProgress = isTransferInProgress();

        if (pref != inProgress) {
            Editor editor = preferences.edit();
            editor.putBoolean(FILE_TRANSFER_IN_PROGRESS, inProgress);
            editor.apply();
        }
    }

    @Override
    public Collection<SaveFilesSession> getSaveFilesSessions() {
        return saveFileSessions.snapshot().values();
    }

    @Override
    public int getActiveSaveFilesSessionsCount() {
        int activeSaveSessions = 0;
        for (SaveFilesSession saveFilesSession : getSaveFilesSessions()) {
            if (saveFilesSession.isInProgress()) {
                activeSaveSessions++;
            }
        }

        return activeSaveSessions;
    }

    public void removeAllErrorOccurredCache() {
        errorOccurredCache.evictAll();
    }

    public void retryAutoUpload(String sessionId) {
        errorOccurredCache.remove(sessionId);

        Intent intent = new Intent(CloudGatewayFileTransferUtils.BROADCAST_AUTO_UPLOAD_STATE_CHANGED);
        intent.putExtra("pressedRetryButton", true);

        ////reducing BRdcast
        LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
    }
}

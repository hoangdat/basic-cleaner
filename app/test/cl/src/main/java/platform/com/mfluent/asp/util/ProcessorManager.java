
package platform.com.mfluent.asp.util;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.os.Build;
import android.os.PowerManager;
import android.os.SystemClock;

import com.mfluent.asp.common.util.IntVector;
import com.mfluent.log.Log;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.concurrent.atomic.AtomicInteger;

import platform.com.mfluent.asp.framework.IASPApplication2;
import platform.com.mfluent.asp.framework.ServiceLocatorSLPF;

public class ProcessorManager {

    private static ProcessorManager sInstance = null;
    public static boolean DEBUG_ENABLED = false;

    public static final int LOCK_TYPE_CPU = 1;
    public static final int LOCK_TYPE_WIFI = 2;

    public static final int MIN_CPULOCK_INTERVAL = 1;
    public static final int MIN_ALARM_INTERVAL = 23000;
    public static final int MIN_BATTERY_PERCENTAGE = 70;


    public static synchronized ProcessorManager getInstance(Context context) {
        if (context == null) {
            throw new NullPointerException();
        }

        if (sInstance == null) {
            sInstance = new ProcessorManager(context);
        }

        return sInstance;
    }

    public static class ProcessorLock {

        private static final AtomicInteger sNextId = new AtomicInteger(0);

        private int mLockCount;
        private final ProcessorManager mManager;
        private final String mDebugStack;
        protected final int mFlags;
        protected final int mId;

        private ProcessorLock(ProcessorManager manager, String debugStack, int flags) {
            this.mLockCount = 1;
            this.mManager = manager;
            this.mId = sNextId.incrementAndGet();
            this.mDebugStack = debugStack;
            this.mFlags = flags;
        }

        public synchronized void release() {
            if (USE_PERMISSION_FOR_WAKELOCK == false)
                return;
            if (this.mLockCount > 0) {
                this.mLockCount--;
                if (this.mLockCount == 0) {
                    this.mManager.releaseProcessorLock(this);
                }
            }
        }

        @Override
        public void finalize() throws Throwable {
            super.finalize();
            if (USE_PERMISSION_FOR_WAKELOCK == false)
                return;
            if (this.mLockCount > 0) {
                if (this.mDebugStack != null) {
                    Log.w(this, "Finalizing ProcessorLock that was not released! : " + this.mDebugStack);
                }
                AsyncTask.THREAD_POOL_EXECUTOR.execute(new FinalizeProcessorLockRunnable(this.mManager, this.mId, this.mFlags));
                this.mLockCount = 0;
            }
        }
    }

    private static class FinalizeProcessorLockRunnable implements Runnable {

        private final int mId;
        private final int mFlags;
        private final ProcessorManager mManager;

        public FinalizeProcessorLockRunnable(ProcessorManager manager, int id, int flags) {
            this.mId = id;
            this.mFlags = flags;
            this.mManager = manager;
        }

        @Override
        public void run() {
            this.mManager.releaseProcessorLock(this.mId, this.mFlags);
        }

    }

    private static final String INTENT_ALARM_ACTION = "com.mfluent.asp.util.ProcessorManager.INTENT_ALARM_ACTION";

    private final Context mContext;
    private PowerManager.WakeLock mWakeLock = null;
    private WifiManager.WifiLock mWifiLock = null;
    private PendingIntent mAlarmIntent = null;
    private final IntVector mChanceToRunTokens = new IntVector(5, 5, true, true);
    private final IntVector mProcessorLocks = new IntVector(20, 20, true, true);
    private final IntVector mWifiLocks = new IntVector(20, 20, true, true);
    private boolean mTimerStarted = false;

    ////check performance
    private long m_nTotalTick = 0;
    private long m_nLockStart = 0;
    private int m_nCPULock = 0;
    private final static boolean CHECK_CPULOCK_PERFORMANCE = false;
    public final static boolean USE_PERMISSION_FOR_WAKELOCK = false;

    private void checkCPUPerformance(boolean bRelease) {
        if (CHECK_CPULOCK_PERFORMANCE == false) {
            return;
        }
        if (bRelease) {
            if (this.m_nCPULock == 1) {
                this.m_nCPULock = 0;
                long lockTime = System.currentTimeMillis() - this.m_nLockStart;
                if (lockTime > 0) {
                    this.m_nTotalTick += lockTime;
                    Log.i(this, "::SysDBGTest-lock time=" + this.m_nTotalTick + "ms");
                }
            } else if (this.m_nCPULock > 0) {
                this.m_nCPULock--;
            }
        } else {
            if (this.m_nCPULock == 0) {
                this.m_nLockStart = System.currentTimeMillis();
                Log.i(this, "SysDBGTest-Start CPU lock (" + this.m_nTotalTick + "ms)");
            }
            this.m_nCPULock++;
        }
    }

    private ProcessorManager(Context context) {
        this.mContext = context.getApplicationContext();
        if (USE_PERMISSION_FOR_WAKELOCK) {
            PowerManager powerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
            this.mWakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "com.mfluent.asp.util.ProcessorManager");

            WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            this.mWifiLock = wifiManager.createWifiLock(WifiManager.WIFI_MODE_FULL_HIGH_PERF, "com.mfluent.asp.util.ProcessorManager");

            Intent intent = new Intent();
            intent.setAction(INTENT_ALARM_ACTION);
            intent.setClass(context, ProcessorManagerAlarmReceiver.class);
            this.mAlarmIntent = PendingIntent.getBroadcast(this.mContext, 1, intent, PendingIntent.FLAG_CANCEL_CURRENT);

            AlarmManager alarmManager = (AlarmManager) this.mContext.getSystemService(Context.ALARM_SERVICE);
            alarmManager.cancel(this.mAlarmIntent);
        }
    }

    public synchronized ProcessorLock createLock(final int lockTypeFlags) {
        String debugStackTrace = null;
        if (DEBUG_ENABLED) {
            Exception e = new Exception();
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            try {

                e.printStackTrace(pw);
                debugStackTrace = sw.toString();
            } finally {
                if (pw != null) {
                    pw.close();
                }
            }
        }

        ProcessorLock result = new ProcessorLock(this, debugStackTrace, lockTypeFlags);
        if (USE_PERMISSION_FOR_WAKELOCK == false)
            return result;
        if ((lockTypeFlags & LOCK_TYPE_CPU) != 0) {
            this.mWakeLock.acquire();
            checkCPUPerformance(false);
            this.mProcessorLocks.insert(result.mId);
            Log.d(this, "createLock id: " + result.mId + ", cpu locks held: " + this.mProcessorLocks.size());

            checkToStopTimer();
        }

        if ((lockTypeFlags & LOCK_TYPE_WIFI) != 0) {
            this.mWifiLock.acquire();
            this.mWifiLocks.insert(result.mId);
            Log.d(this, "createLock id: " + result.mId + ", wifi locks held: " + this.mWifiLocks.size());
        }

        return result;
    }

    private void releaseProcessorLock(ProcessorLock lock) {
        releaseProcessorLock(lock.mId, lock.mFlags);
    }

    private synchronized void releaseProcessorLock(int lockId, int lockFlags) {
        int foundPos = -1;
        if (USE_PERMISSION_FOR_WAKELOCK == false)
            return;

        if ((lockFlags & LOCK_TYPE_CPU) != 0) {
            foundPos = this.mProcessorLocks.find(lockId);
            if (foundPos >= 0) {
                this.mProcessorLocks.removeElementAt(foundPos);

                Log.d(this, "releaseProcessorLock id: " + lockId + ", cpu locks left: " + this.mProcessorLocks.size());

                this.mWakeLock.release();
                checkCPUPerformance(true);

                checkToStartTimer();
            } else {
                Log.w(this, "releaseProcessorLock cpu lock not found! id: " + lockId);
            }
        }

        if ((lockFlags & LOCK_TYPE_WIFI) != 0) {
            foundPos = this.mWifiLocks.find(lockId);
            if (foundPos >= 0) {
                this.mWifiLocks.removeElementAt(foundPos);

                Log.d(this, "releaseProcessorLock id: " + lockId + ", wifi locks left: " + this.mWifiLocks.size());

                this.mWifiLock.release();
            } else {
                Log.w(this, "releaseProcessorLock wifi lock not found! id: " + lockId);
            }
        }
    }

    private boolean isBatteryStatusGood() {
        boolean bRet = true;
        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        IASPApplication2 objApp = ServiceLocatorSLPF.get(IASPApplication2.class);
        if (objApp == null) {
            return true;
        }
        try {
            ////temporal test only until Zero PRA
            if (Build.MODEL.contains("920V") || Build.MODEL.contains("925V")) {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Intent batteryStatus = objApp.registerReceiver(null, ifilter);
        if (batteryStatus != null) {
            int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
            int maxLevel = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
            int plugged = batteryStatus.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
            if (level >= 0 && maxLevel > 0) {
                int nPercentage = level * 100 / maxLevel;
                if (nPercentage < MIN_BATTERY_PERCENTAGE) {
                    Log.w(this, "isBatteryStatusGood can't start wakelock due to battery level = " + nPercentage + "%%");
                    bRet = false;
                }
            }
        }
        return bRet;
    }

    //Only to be called from within synchronized blocks.
    private void checkToStartTimer() {
        if (USE_PERMISSION_FOR_WAKELOCK == false)
            return;
        if (!this.mTimerStarted && this.mProcessorLocks.size() == 0 && this.mChanceToRunTokens.size() > 0 && isBatteryStatusGood()) {
            Log.d(this, "Beginning timer. CPU locks held: " + mProcessorLocks.size() + ", Tokens held: " + mChanceToRunTokens.size());
            AlarmManager alarmManager = (AlarmManager) this.mContext.getSystemService(Context.ALARM_SERVICE);
            alarmManager.setRepeating(
                    AlarmManager.ELAPSED_REALTIME_WAKEUP,
                    SystemClock.elapsedRealtime() + MIN_ALARM_INTERVAL,
                    MIN_ALARM_INTERVAL,
                    this.mAlarmIntent);
            this.mTimerStarted = true;
        }
    }

    //Only to be called from within synchronized blocks.
    private void checkToStopTimer() {
        if (USE_PERMISSION_FOR_WAKELOCK == false)
            return;
        if (this.mTimerStarted && (this.mProcessorLocks.size() > 0 || this.mChanceToRunTokens.size() == 0)) {
            Log.d(this, "Ending timer. CPU locks held: " + mProcessorLocks.size() + ", Tokens held: " + mChanceToRunTokens.size());
            AlarmManager alarmManager = (AlarmManager) this.mContext.getSystemService(Context.ALARM_SERVICE);
            alarmManager.cancel(this.mAlarmIntent);
            this.mTimerStarted = false;
        }
    }

    synchronized void onTimer() {
        if (this.mWakeLock != null)
            this.mWakeLock.acquire();

        checkCPUPerformance(false);

        new Thread("com.mfluent.asp.util.ProcessorManager.AlarmReceiver") {

            @Override
            public void run() {
                try {
                    onTimerRun();
                } catch (InterruptedException e) {

                }
            }
        }.start();
    }

    private synchronized void onTimerRun() throws InterruptedException {
        try {
            if (this.mChanceToRunTokens.size() > 0) {
                Log.d(this, "Giving it a chance to run for " + MIN_CPULOCK_INTERVAL + " ms.");
                Thread.sleep(MIN_CPULOCK_INTERVAL);
            } else {
                //we shouldn't be running the alarm at this time
                this.mTimerStarted = true;
                checkToStopTimer();
            }
        } finally {
            if (this.mWakeLock != null)
                this.mWakeLock.release();

            checkCPUPerformance(true);
        }
    }
}

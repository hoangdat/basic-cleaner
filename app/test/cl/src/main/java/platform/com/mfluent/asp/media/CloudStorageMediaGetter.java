/**
 *
 */

package platform.com.mfluent.asp.media;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import android.content.ContentProvider.PipeDataWriter;
import android.net.Uri;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;

import com.mfluent.asp.cloudstorage.api.sync.CloudStorageSync;
import com.mfluent.asp.cloudstorage.api.sync.CloudStreamInfo;

import platform.com.mfluent.asp.datamodel.ASPMediaStoreProvider;
import platform.com.mfluent.asp.util.IOUtils;

/**
 * @author michaelgierach
 */
public class CloudStorageMediaGetter extends BaseRemoteMediaGetter implements PipeDataWriter<AspMediaInfo> {

    /**
     * @param mediaInfo
     * @param provider
     */
    public CloudStorageMediaGetter(AspMediaInfo mediaInfo, ASPMediaStoreProvider provider) {
        super(mediaInfo, provider);
    }

    @Override
    public void writeDataToPipe(ParcelFileDescriptor output, Uri uri, String mimeType, Bundle opts, AspMediaInfo args) {

        InputStream content = null;
        FileOutputStream dest = null;
        byte[] buffer = new byte[32 * 1024];

        try {
            AspMediaInfo mediaInfo = getMediaInfo();
            CloudStorageSync cloudStorageSync = mediaInfo.getDevice().getCloudStorageSync();
            if (cloudStorageSync == null) {
                throw new FileNotFoundException("CloudStorageSync instance not found.");
            }

            CloudStreamInfo streamInfo = cloudStorageSync.getFile(mediaInfo.getSourceUri(), mediaInfo.getDeviceMediaId(), null);
            if (streamInfo == null) {
                throw new FileNotFoundException("Null streamInfo returned.");
            }

            content = streamInfo.getInputStream();
            if (content == null) {
                throw new FileNotFoundException("Null InputStream returned.");
            }
        } catch (IOException ioe) {
            //Log.w(TAG, "Failed to open InputStream", ioe);

            try {
                setState(STATE_FAILED);
            } catch (InterruptedException ie) {

            }
            return;
        } catch (Exception e) {
            e.printStackTrace();
            try {
                setState(STATE_FAILED);
            } catch (InterruptedException ie) {

            }
            return;
        }

        try {
            dest = new FileOutputStream(output.getFileDescriptor());

            int readResult = 0;
            while (readResult >= 0) {
                readResult = content.read(buffer);
                if (readResult > 0) {
                    try {
                        setState(STATE_HAS_SOME_DATA);
                    } catch (InterruptedException ie) {

                    }
                    dest.write(buffer, 0, readResult);
                }
            }
        } catch (IOException ioe) {
            //Log.w(TAG, "Failure during writeToPipe", ioe);
        } finally {
            IOUtils.closeQuietly(content);
            IOUtils.closeQuietly(dest);
        }
    }
}

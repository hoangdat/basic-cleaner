/*
 * Copyright (C) 2012 The Android Open Source Project
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package platform.com.mfluent.asp.util.bitmap;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.Handler;
import android.os.Looper;

import com.mfluent.asp.common.media.thumbnails.ImageInfo;
import com.mfluent.asp.common.media.thumbnails.ImageInfo.ThumbnailSize;
import com.mfluent.log.Log;
import com.samsung.android.slinkcloud.R;

import java.io.FileNotFoundException;
import java.lang.ref.WeakReference;

import platform.com.mfluent.asp.datamodel.DataModelSLPF;
import platform.com.mfluent.asp.datamodel.DeviceSLPF;
import platform.com.mfluent.asp.media.AspThumbnailCache;
import platform.com.mfluent.asp.media.CloudStorageThumbnailGetter;
import uicommon.com.mfluent.asp.util.FileImageInfo;
import uicommon.com.mfluent.asp.util.bitmap.BitmapInfo;

/**
 * This class wraps up completing some arbitrary long running work when loading
 * a bitmap to an
 * ImageWorkerClient. It handles things like using a memory and disk cache,
 * running the work in a background
 * thread and setting a placeholder image.
 *
 * @author MFluent
 * @version 1.5
 */
public class ImageWorker {

    /**
     * The Interface ImageWorkerClient.
     *
     * @author MFluent
     * @version 1.5
     */
    public interface ImageWorkerClient {

        /**
         * Sets the image and orientation.
         *
         * @param bitmapInfo  the image
         * @param orientation the orientation
         * @param request     TODO
         */
        void setImageAndOrientation(BitmapInfo bitmapInfo, int orientation, ImageInfo request);

        /**
         * Sets the tag.
         *
         * @param key    the key
         * @param object the object
         */
        void setTag(int key, Object object);

        /**
         * Gets the tag.
         *
         * @param key the key
         * @return the tag
         */
        Object getTag(int key);

        /**
         * This is called when the image failed to load
         */
        void onImageFailedToLoad();

        Point getMaximumBitmapSize();
    }

    private static Point sMaximumBitmapSize = null;

    private final Context mContext;

    public static final int[] PRIORITY_ADJACENT_PLAYERS;
    public static final int PRIORITY_PREFETCH_RECENT;
    private static final int NUM_PRIORITIES;

    private static final ImageWorkerThread[] LOAD_FROM_FILE_THREAD_POOL;
    private static final ImageWorkerQueue LOAD_FROM_FILE_QUEUE;
    private static final int LOAD_FROM_FILE_THREAD_POOL_SIZE = 2;

    private static final ImageWorkerThread[] LOAD_FROM_REMOTE_THREAD_POOL;
    private static final ImageWorkerQueue LOAD_FROM_REMOTE_QUEUE;
    private static final int LOAD_FROM_REMOTE_THREAD_POOL_SIZE = 4;
    private static final int OFFSCREEN_PAGE_LIMIT = 4;

    static {
        PRIORITY_ADJACENT_PLAYERS = new int[OFFSCREEN_PAGE_LIMIT];
        for (int i = 0; i < OFFSCREEN_PAGE_LIMIT; ++i) {
            PRIORITY_ADJACENT_PLAYERS[i] = i + 1;
        }
        PRIORITY_PREFETCH_RECENT = OFFSCREEN_PAGE_LIMIT + 2;
        NUM_PRIORITIES = OFFSCREEN_PAGE_LIMIT + 3;

        LOAD_FROM_FILE_QUEUE = new ImageWorkerQueue(NUM_PRIORITIES);
        LOAD_FROM_FILE_THREAD_POOL = new ImageWorkerThread[LOAD_FROM_FILE_THREAD_POOL_SIZE];
        for (int i = 0; i < LOAD_FROM_FILE_THREAD_POOL_SIZE; ++i) {
            LOAD_FROM_FILE_THREAD_POOL[i] = new ImageWorkerThread(LOAD_FROM_FILE_QUEUE);
            LOAD_FROM_FILE_THREAD_POOL[i].start();
        }

        LOAD_FROM_REMOTE_QUEUE = new ImageWorkerQueue(NUM_PRIORITIES);
        LOAD_FROM_REMOTE_THREAD_POOL = new ImageWorkerThread[LOAD_FROM_REMOTE_THREAD_POOL_SIZE];
        for (int i = 0; i < LOAD_FROM_REMOTE_THREAD_POOL_SIZE; ++i) {
            LOAD_FROM_REMOTE_THREAD_POOL[i] = new ImageWorkerThread(LOAD_FROM_REMOTE_QUEUE);
            LOAD_FROM_REMOTE_THREAD_POOL[i].start();
        }
    }

    private synchronized static Point getMaximumBitmapSize(ImageWorkerClient client) {
        if ((sMaximumBitmapSize == null) && (client != null)) {
            Point temp = client.getMaximumBitmapSize();
            if (temp != null && temp.x > 0 && temp.y > 0) {
                sMaximumBitmapSize = new Point(temp);
            }
        }

        return sMaximumBitmapSize;
    }

    private final int localDeviceId;

    private final Handler mHandler;

    /**
     * Instantiates a new image worker.
     */
    public ImageWorker(Context context, Looper looper) {
        //        mContext = context;
        this.mContext = context.getApplicationContext();

        this.mHandler = new Handler(looper);

        DeviceSLPF localDevice = DataModelSLPF.getInstance().getLocalDevice();
        if (localDevice == null) {
            this.localDeviceId = -1;
        } else {
            this.localDeviceId = localDevice.getId();
        }
    }

    /**
     * The Enum MemoryCheck.
     *
     * @author MFluent
     * @version 1.5
     */
    public enum MemoryCheck {
        DEFAULT, // check memory cache first - then fall back to loading image from source
        MEMORY_ONLY, // only check memory cache
        SKIP_MEMORY, // skip memory check - always load image from source
        PRE_FETCH // skips memory check, will check for file existence and not render the bitmap
    }

    /**
     * Load image.
     *
     * @param imageInfo   the image info
     * @param client      the client
     * @param memoryCheck the memory check
     * @param priority    the priority
     */
    public void loadImage(ImageInfo imageInfo, ImageWorkerClient client, MemoryCheck memoryCheck, int priority) {

        if (client == null) {
            Log.v(this, "loadImage - ImageWorkerClient is null");
            return;
        }

        Log.v(this, "loadImage - memoryCheck : " + memoryCheck + ", priority : " + priority);

        switch (memoryCheck) {
            case MEMORY_ONLY: {
                //Bitmap memory cache has been removed from platform
                break;
            }
            case SKIP_MEMORY:
            case PRE_FETCH:
            default: {
                startLoadImageTask(imageInfo, client, priority, memoryCheck);
                break;
            }
        }
    }

    /**
     * Checks if is result image info too small.
     *
     * @param request the request
     * @param result  the result
     * @return true, if is result image info too small
     */
    private boolean isResultImageInfoTooSmall(ImageInfo request, ImageInfo result) {
        if (result == null) {
            return true;
        }

        return request.getDesiredBitmapWidth() > result.getDesiredBitmapWidth()
                || request.getDesiredBitmapHeight() > result.getDesiredBitmapHeight()
                || request.getThumbnailSize().compareTo(result.getThumbnailSize()) > 0;
    }

    /**
     * Start load image task.
     *
     * @param imageInfo   the image info
     * @param client      the client
     * @param priority    TODO
     * @param memoryCheck the memory check
     */
    private void startLoadImageTask(ImageInfo imageInfo, ImageWorkerClient client, int priority, MemoryCheck memoryCheck) {
        LoadTask task = new LoadFromFileCacheTask(client, imageInfo, this.mHandler, memoryCheck);
        client.setTag(R.id.platform_com_mfluent_asp_util_bitmap_ImageWorker_tag, new WeakReference<LoadTask>(task));

        LOAD_FROM_FILE_QUEUE.put(task, priority);
    }

    /**
     * Start remote fetch task.
     *
     * @param imageInfo   the image info
     * @param client      the client
     * @param priority    the priority
     * @param memoryCheck the memory check
     */
    private void startRemoteFetchTask(ImageInfo imageInfo, ImageWorkerClient client, int priority, MemoryCheck memoryCheck) {
        LoadRemoteFileTask task = new LoadRemoteFileTask(client, imageInfo, this.mHandler, memoryCheck);

        client.setTag(R.id.platform_com_mfluent_asp_util_bitmap_ImageWorker_tag, new WeakReference<LoadTask>(task));

        LOAD_FROM_REMOTE_QUEUE.put(task, priority);
    }

    /**
     * Cancel current task.
     *
     * @param client the client
     */
    public void cancelCurrentTask(ImageWorkerClient client) {

        if (client == null) {
            return;
        }
        @SuppressWarnings("unchecked")
        WeakReference<LoadTask> taskRef = (WeakReference<LoadTask>) client.getTag(R.id.platform_com_mfluent_asp_util_bitmap_ImageWorker_tag);
        if (taskRef != null) {
            LoadTask loadTask = taskRef.get();
            if (loadTask != null) {
                loadTask.cancel();
                boolean removed = LOAD_FROM_FILE_QUEUE.remove(loadTask);
                if (!removed) {
                    removed = LOAD_FROM_REMOTE_QUEUE.remove(loadTask);
                    if (removed) {
                        Log.v(this, "Task removed from REMOTE QUEUE: " + loadTask);
                    }
                } else {
                    Log.v(this, "Task removed from FILE QUEUE: " + loadTask);
                }
            }
        }
    }

    @SuppressWarnings("unused")
    private void recycleBitmapIfNotInCache(BitmapInfo bitmapInfo) {
        if (bitmapInfo != null) {
            Bitmap bitmap = bitmapInfo.getBitmap();
            if (bitmap != null && !hasBitmapInMemoryCache(bitmapInfo.getImageInfo(), bitmap)) {
                Log.v(this, "Recycling bitmap produced but not used for ImageInfo:" + bitmapInfo.getImageInfo() + " bitmap size: " + bitmap.getByteCount());
                bitmap.recycle();
            }
        }
    }

    public boolean hasBitmapInMemoryCache(ImageInfo imageInfo, Bitmap bitmap) {
        return false;
    }


    //    /**
    //     * Load an image specified from a set adapter into an ImageWorkerClient (override
    //     * {@link ImageWorker#processBitmap(Object)} to define the processing logic). A memory and disk
    //     * cache will be used if an {@link ImageCache} has been set using
    //     * {@link ImageWorker#setImageCache(ImageCache)}. If the image is found in the memory cache, it
    //     * is set immediately, otherwise an {@link AsyncTask} will be created to asynchronously load the
    //     * bitmap. {@link ImageWorker#setAdapter(ImageWorkerAdapter)} must be called before using this
    //     * method.
    //     *
    //     * @param data The URL of the image to download.
    //     * @param client The ImageWorkerClient to bind the downloaded image to.
    //     */
    //    public void loadImage(int num, ImageWorkerClient client) {
    //        if (mImageWorkerAdapter != null) {
    //            loadImage(mImageWorkerAdapter.getItem(num), client);
    //        } else {
    //            throw new NullPointerException("Data not set, must call setAdapter() first.");
    //        }
    //    }

    /**
     * The Class LoadTask.
     *
     * @author MFluent
     * @version 1.5
     */
    abstract class LoadTask implements Runnable {

        protected final WeakReference<ImageWorkerClient> clientReference;
        protected final ImageInfo imageInfo;
        protected final Handler mHandler;
        protected final MemoryCheck mMemoryCheck;
        private BitmapInfo mResult;
        private boolean mCancelled = false;
        private int mPriority;

        /**
         * Instantiates a new load task.
         *
         * @param client      the client
         * @param imageInfo   the image info
         * @param handler     the handler
         * @param memoryCheck the memory check
         */
        public LoadTask(ImageWorkerClient client, ImageInfo imageInfo, Handler handler, MemoryCheck memoryCheck) {
            this.imageInfo = new ImageInfo(imageInfo);
            this.clientReference = new WeakReference<ImageWorkerClient>(client);
            this.mHandler = handler;
            this.mMemoryCheck = memoryCheck;
        }

        /**
         * Cancel.
         */
        public synchronized void cancel() {
            if (!this.mCancelled) {
                this.clientReference.clear();
                this.mCancelled = true;
            }
        }

        /**
         * Checks if is cancelled.
         *
         * @return true, if is cancelled
         */
        public synchronized boolean isCancelled() {
            return this.mCancelled;
        }

        /**
         * Sets the priority.
         *
         * @param priority the new priority
         */
        public final void setPriority(int priority) {
            this.mPriority = priority;
        }

        /**
         * Gets the priority.
         *
         * @return the priority
         */
        public final int getPriority() {
            return this.mPriority;
        }

        /**
         * Gets the image worker.
         *
         * @return the image worker
         */
        public final ImageWorker getImageWorker() {
            return ImageWorker.this;
        }

        /**
         * @see java.lang.Runnable#run()
         */
        @Override
        public void run() {
            this.mResult = doInBackground();
            Runnable postExecRunnable = new Runnable() {

                @Override
                public void run() {
                    onPostExecute(LoadTask.this.mResult);
                }
            };

            this.mHandler.post(postExecRunnable);
        }

        @Override
        public String toString() {
            return this.getClass().getSimpleName() + " imageInfo:" + this.imageInfo;
        }

        /**
         * Do in background.
         *
         * @return the bitmap info
         */
        protected abstract BitmapInfo doInBackground();

        /**
         * On post execute.
         *
         * @param bitmapInfo the bitmap info
         */
        protected void onPostExecute(BitmapInfo bitmapInfo) {
            ImageWorkerClient client = this.clientReference.get();
            if (bitmapInfo != null) {
                bitmapInfo.retain();
            }
            try {
                if (client == null) {
                    Log.v(this, "::onPostExecute client reference has gone away! imageInfo:" + this.imageInfo);
                    //recycleBitmapIfNotInCache(bitmapInfo);
                    return;
                }

                if (isCancelled()) {
                    Log.v(this, "::onPostExecute I have been cancelled! imageInfo:" + this.imageInfo);
                    //recycleBitmapIfNotInCache(bitmapInfo);
                    return;
                }

                WeakReference<?> taskRef = (WeakReference<?>) client.getTag(R.id.platform_com_mfluent_asp_util_bitmap_ImageWorker_tag);
                if (taskRef == null || taskRef.get() != this) {
                    Log.v(this, "onPostExecute client's tag does not match me! imageInfo:" + this.imageInfo);
                    //recycleBitmapIfNotInCache(bitmapInfo);
                    return;
                }

                if (bitmapInfo == null) {
                    if (this.mMemoryCheck != MemoryCheck.PRE_FETCH) {
                        Log.v(this, "onPostExecute failed to get Bitmap. imageInfo:" + this.imageInfo);
                    }
                    //				if (!client.hasDrawable()) {
                    loadDefaultBitmap(client);
                    //				}
                    return;
                }

				/*
                 * if (bitmapInfo.getImageInfo().getThumbnailSize() != ThumbnailSize.FULL_SCREEN) {
				 * ImageWorker.this.mImageCache.addBitmapToCache(bitmapInfo);
				 * } else {
				 * BitmapInfo inCache = ImageWorker.this.mImageCache.getBitmapFromMemCache(this.imageInfo);
				 * if (inCache != null) {
				 * int desiredArea = inCache.getImageInfo().getDesiredBitmapHeight() * inCache.getImageInfo().getDesiredBitmapWidth();
				 * int cachedArea = inCache.getBitmap().getWidth() * inCache.getBitmap().getHeight();
				 * if (desiredArea > cachedArea * 2) {
				 * ImageWorker.this.mImageCache.removeBitmapFromMemCache(this.imageInfo);
				 * }
				 * }
				 * }
				 */

                setImageBitmap(client, bitmapInfo);
            } finally {
                if (bitmapInfo != null) {
                    bitmapInfo.release();
                }
            }
        }

        protected BitmapInfo getGoodEnoughImageFromMemCache() {
            //Bitmap memory cache has been removed from platform.
            return null;
        }

        /**
         * Load default bitmap.
         *
         * @param client the client
         */
        protected void loadDefaultBitmap(ImageWorkerClient client) {
            //			Bitmap defaultBitmap = DEFAULT_BITMAPS0.get(this.imageInfo.getMediaType());
            //			if (defaultBitmap != null) {
            //				client.setImageAndOrientation(defaultBitmap, 0);
            //			} else {
            client.onImageFailedToLoad();
            //			}
        }
    }

    /**
     * The Class LoadFromFileCacheTask.
     *
     * @author MFluent
     * @version 1.5
     */
    private class LoadFromFileCacheTask extends LoadTask {

        private boolean doRemoteFetch;

        /**
         * Instantiates a new load from file cache task.
         *
         * @param client      the client
         * @param imageInfo   the image info
         * @param handler     the handler
         * @param memoryCheck the memory check
         */
        public LoadFromFileCacheTask(ImageWorkerClient client, ImageInfo imageInfo, Handler handler, MemoryCheck memoryCheck) {
            super(client, imageInfo, handler, memoryCheck);
        }

		/*
         * (non-Javadoc)
		 * @see android.os.AsyncTask#doInBackground(Params[])
		 */

        @Override
        protected BitmapInfo doInBackground() {
            AspThumbnailCache fileCache = AspThumbnailCache.getInstance(ImageWorker.this.mContext);
            FileImageInfo fileImageInfo = fileCache.getFile(imageInfo);
            if (fileImageInfo == null) {
                doRemoteFetch = true;
                return null;
            }

            //First check to see if there was actually an image put in the memory cache since this task
            //was queued up. If it is good enough, just return that image.
            BitmapInfo goodEnough = getGoodEnoughImageFromMemCache();
            if (goodEnough != null) {
                return goodEnough;
            }

            if (isFileCacheImageTooSmall(imageInfo, fileImageInfo)) {
                Log.d(this, "file cache hit too small for " + imageInfo);
                doRemoteFetch = true;
            }

            BitmapInfo bitmapInfo = null;
            if (mMemoryCheck != MemoryCheck.PRE_FETCH) {
                try {
                    bitmapInfo = decodeBitmapFromFile(fileImageInfo, imageInfo, ImageWorker.getMaximumBitmapSize(clientReference.get()));
                    if (bitmapInfo == null) {
                        Log.d(this, "doInBackground:" + "Failed to decode bitmap from file cache hit - removing from file cache " + imageInfo);

                        fileCache.remove(imageInfo);
                        doRemoteFetch = true;
                    }
                } catch (OutOfMemoryError oome) {
                    Log.d(this, "::doInBackground:" + "OutOfMemoryError " + imageInfo + " - " + oome.getMessage());
                }
            } else {
                Log.d(this, "pre-fetch file in cache. Doing remote fetch:" + doRemoteFetch);
            }

            return bitmapInfo;
        }

		/*
         * (non-Javadoc)
		 * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
		 */

        @Override
        protected void onPostExecute(BitmapInfo bitmapInfo) {
            super.onPostExecute(bitmapInfo);

            ImageWorkerClient client = this.clientReference.get();
            if (client == null) {
                return;
            }

            if (isCancelled()) {
                return;
            }

            if (this.doRemoteFetch) {
                startRemoteFetchTask(this.imageInfo, client, getPriority(), this.mMemoryCheck);
            }
        }

        /*
         * (non-Javadoc)
         * @see
         * com.mfluent.asp.util.bitmap.ImageWorker.LoadTask#loadDefaultBitmap
         * (android.widget.ImageView)
         */
        @Override
        protected void loadDefaultBitmap(ImageWorkerClient client) {
            if (!this.doRemoteFetch) {
                //don't load the default bitmap if we still need to do a remote fetch
                super.loadDefaultBitmap(client);
            }
        }

        /**
         * Checks if is file cache image too small.
         *
         * @param request the request
         * @param result  the result
         * @return true, if is file cache image too small
         */
        private boolean isFileCacheImageTooSmall(ImageInfo request, FileImageInfo result) {
            if (result == null) {
                return true;
            }

            return request.getDesiredWidth() > result.getImageInfo().getDesiredWidth() || request.getDesiredHeight() > result.getImageInfo().getDesiredHeight();
        }
    }

    /**
     * The Class LoadRemoteFileTask.
     *
     * @author MFluent
     * @version 1.5
     */
    private class LoadRemoteFileTask extends LoadTask {

        /**
         * Instantiates a new load remote file task.
         *
         * @param client      the client
         * @param imageInfo   the image info
         * @param handler     the handler
         * @param memoryCheck the memory check
         */
        public LoadRemoteFileTask(ImageWorkerClient client, ImageInfo imageInfo, Handler handler, MemoryCheck memoryCheck) {
            super(client, imageInfo, handler, memoryCheck);
        }

        /*
         * (non-Javadoc)
         * @see android.os.AsyncTask#doInBackground(Params[])
         */
        @Override
        protected BitmapInfo doInBackground() {
            try {
                return doGetImage();
            } catch (FileNotFoundException fne) {
                Log.d(this, "::doInBackground:" + "Trouble getting remote thumbnail: FileNotFoundException: " + fne.getMessage());
            } catch (Exception e) {
                Log.d(this, "::doInBackground:" + "Trouble getting remote thumbnail" + e.getMessage());
            }
            return null;
        }

        /**
         * Do get image.
         *
         * @return the bitmap info
         * @throws Exception the exception
         */
        private BitmapInfo doGetImage() throws Exception {
            DeviceSLPF device = getDevice(this.imageInfo);
            if (device == null) {
                Log.i(this, "device is null, thumbnail loading failed!");
                return null;
            }

            if (this.isCancelled()) {
                Log.i(this, "canceled, thumbnail worker return");
                return null;
            }

            //First check to see if there was actually an image put in the memory cache since this task
            //was queued up. If it is good enough, just return that image.
            BitmapInfo goodEnough = this.getGoodEnoughImageFromMemCache();
            if (goodEnough != null) {
                return goodEnough;
            }

            AspThumbnailCache fileCache = AspThumbnailCache.getInstance(ImageWorker.this.mContext);
            FileImageInfo fileImageInfo = fileCache.getFile(this.imageInfo);
            if (fileImageInfo != null && isResultImageInfoTooSmall(this.imageInfo, fileImageInfo.getImageInfo())) {
                fileImageInfo = null;
            }
            if (fileImageInfo == null) {
                switch (device.getDeviceTransportType()) {
                    case WEB_STORAGE: {
                        fileImageInfo = new CloudStorageThumbnailGetter(this.imageInfo, fileCache).openThumbnail();
                        break;
                    }
                    default:
                        return null;
                }
            }

            if (this.mMemoryCheck != MemoryCheck.PRE_FETCH && !this.isCancelled()) {
                try {
                    return decodeBitmapFromFile(fileImageInfo, this.imageInfo, ImageWorker.getMaximumBitmapSize(this.clientReference.get()));
                } catch (OutOfMemoryError oome) {
                    Log.e(this, "doGetImage:" + "OutOfMemoryError " + this.imageInfo + " - " + oome.getMessage());
                }
            } else if (this.mMemoryCheck == MemoryCheck.PRE_FETCH) {
                Log.d(this, "doGetImage: pre-fetch " + (fileImageInfo != null ? "SUCCESS " : "FAILED ") + this.imageInfo);
            }

            return null;
        }
    }

    /**
     * Decode bitmap from file.
     *
     * @param fileImageInfo the file image info
     * @param request       the request
     * @return the bitmap info
     */
    private BitmapInfo decodeBitmapFromFile(FileImageInfo fileImageInfo, ImageInfo request, Point maxSize) {
        //        acquireSemaphoreIfNecessary();

        BitmapFactory.Options factoryOptions = getBitmapFactoryOptions(request);

        Bitmap bitmap = ImageResizerSLPF.decodeSampledBitmapFromFile(fileImageInfo.getFile().getAbsolutePath(), request, factoryOptions, maxSize);
        if (bitmap == null) {
            return null;
        }

        if (request.getThumbnailSize() != ThumbnailSize.FULL_SCREEN) {
            bitmap = ImageResizerSLPF.resampleBitmap(bitmap, request);
        }

        //        // if loading local image from file fails (typical OutOfMemoryError situation), fall back on thumbnail
        //        if (bitmap == null && device.isDeviceTransportType(CloudGatewayDeviceTransportType.LOCAL) &&
        //                request.getMediaType() == AspMediaId.MEDIA_TYPE_IMAGE)
        //        {
        //            bitmap = MediaStore.Images.Thumbnails.getThumbnail(context.getContentResolver(), Integer.parseInt(imageInfo.getSourceMediaId()), MediaStore.Images.Thumbnails.MINI_KIND, factoryOptions);
        //        }

        BitmapInfo bitmapInfo = new BitmapInfo(request, bitmap);
        return bitmapInfo;
    }

    /**
     * Gets the bitmap factory options.
     *
     * @param request the request
     * @return the bitmap factory options
     */
    private BitmapFactory.Options getBitmapFactoryOptions(ImageInfo request) {
        BitmapFactory.Options factoryOptions = new BitmapFactory.Options();
        if (request.getThumbnailSize() != ThumbnailSize.FULL_SCREEN) {
            factoryOptions.inPreferredConfig = Bitmap.Config.RGB_565;
        }
        if (Thread.currentThread() instanceof ImageWorkerThread) {
            factoryOptions.inTempStorage = ((ImageWorkerThread) Thread.currentThread()).buffer;
        }
        return factoryOptions;
    }

    //    /**
    //     * The actual AsyncTask that will asynchronously process the image.
    //     */
    //    private class BitmapWorkerTask extends AsyncTask<ImageInfo, Void, BitmapInfo> {
    //        private ImageInfo imageInfo;
    //        private final WeakReference<ImageWorkerClient> clientReference;
    //        boolean skipFileCache;
    //        private ImageFetcher imageFetcher;
    //
    //        public BitmapWorkerTask(ImageWorkerClient client) {
    //            clientReference = new WeakReference<ImageWorkerClient>(client);
    //        }
    //
    //        /**
    //         * Background processing.
    //         */
    //        @Override
    //        protected BitmapInfo doInBackground(ImageInfo... params) {
    //            imageInfo = params[0];
    //            BitmapInfo bitmapInfo = null;
    //
    //            try
    //            {
    //                // If the bitmap was not found in the cache and this task has not been cancelled by
    //                // another thread and the ImageWorkerClient that was originally bound to this task is still
    //                // bound back to this task and our "exit early" flag is not set, then call the main
    //                // process method (as implemented by a subclass)
    //                if (!isCancelled() && getAttachedImageWorkerClient() != null
    //                        && !mExitTasksEarly) {
    //                    long startTime = System.currentTimeMillis();
    //                    imageFetcher = new ImageFetcher(mContext, imageInfo, skipFileCache, bitmapInMemorySemaphore);
    //                    imageFetcher.fetchImage();
    //                    bitmapInfo = imageFetcher.getResult();
    //                    Log.v(TAG, "Fetch took " + (System.currentTimeMillis() - startTime));
    //                }
    //
    //                // If the bitmap was processed and the image cache is available, then add the processed
    //                // bitmap to the cache for future use. Note we don't check if the task was cancelled
    //                // here, if it was, and the thread is still running, we may as well add the processed
    //                // bitmap to our cache as it might be used again in the future
    //                if (bitmapInfo != null && bitmapInfo.getBitmap() != null && mImageCache != null && imageInfo.getThumbnailSize() != ThumbnailSize.FULL_SCREEN) {
    //                    mImageCache.addBitmapToCache(bitmapInfo);
    //                }
    //            }
    //            catch (Throwable t)
    //            {
    //                Log.e(TAG, "::doInBackground uncaught exception", t);
    //            }
    //
    //            return bitmapInfo;
    //        }
    //
    //        /**
    //         * Once the image is processed, associates it to the client
    //         */
    //        @Override
    //        protected void onPostExecute(BitmapInfo bitmapInfo) {
    //            // if cancel was called on this task or the "exit early" flag is set then we're done
    //            if (!isCancelled() && !mExitTasksEarly) {
    //                final ImageWorkerClient client = getAttachedImageWorkerClient();
    //                if (client != null) {
    //                    if (bitmapInfo != null && bitmapInfo.getBitmap() != null) {
    //                        setImageBitmap(client, bitmapInfo.getBitmap(), imageInfo.getOrientation());
    //
    //                        if (!skipFileCache && isResultImageInfoTooSmall(imageInfo, bitmapInfo)) {
    //                            Log.v(TAG, "Refetching image " + imageInfo);
    //                            startLoadImageTask(imageInfo, client, true, true);
    //                        }
    //                    } else {
    //                        //there was a problem loading the bitmap - show the default one
    //                        if (client.getDrawable() == null) {
    //                            Bitmap defaultBitmap = DEFAULT_BITMAPS.get(imageInfo.getMediaType());
    //                            if (defaultBitmap != null) {
    //                                setImageBitmap(client, defaultBitmap, 0);
    //                            }
    //                        }
    //                    }
    //                }
    //            }
    //
    //            if (imageFetcher != null) {
    //                imageFetcher.releaseSemaphoreIfNecessary();
    //            }
    //        }
    //
    //        /* (non-Javadoc)
    //         * @see android.os.AsyncTask#onCancelled(java.lang.Object)
    //         */
    //        @Override
    //        protected void onCancelled(BitmapInfo result) {
    //            super.onCancelled(result);
    //
    //            if (imageFetcher != null) {
    //                imageFetcher.releaseSemaphoreIfNecessary();
    //            }
    //        }
    //
    //        /**
    //         * Returns the ImageWorkerClient associated with this task as long as the ImageWorkerClient's task still
    //         * points to this task as well. Returns null otherwise.
    //         */
    //        private ImageWorkerClient getAttachedImageWorkerClient() {
    //            final ImageWorkerClient client = clientReference.get();
    //            if (client == null) {
    //                return null;
    //            }
    //
    //            BitmapWorkerTask bitmapWorkerTask = getBitmapWorkerTask(client);
    //
    //            if (this == bitmapWorkerTask) {
    //                return client;
    //            }
    //
    //            return null;
    //        }
    //    }

    //    /**
    //     * A custom Drawable that will be attached to the client while the work is in progress.
    //     * Contains a reference to the actual worker task, so that it can be stopped if a new binding is
    //     * required, and makes sure that only the last started worker process can bind its result,
    //     * independently of the finish order.
    //     */
    //    private static class AsyncDrawable extends BitmapDrawable {
    //        private final WeakReference<BitmapWorkerTask> bitmapWorkerTaskReference;
    //
    //        public AsyncDrawable(Resources res, Bitmap bitmap, BitmapWorkerTask bitmapWorkerTask) {
    //            super(res, bitmap);
    //
    //            bitmapWorkerTaskReference =
    //                new WeakReference<BitmapWorkerTask>(bitmapWorkerTask);
    //        }
    //
    //        public BitmapWorkerTask getBitmapWorkerTask() {
    //            return bitmapWorkerTaskReference.get();
    //        }
    //    }

    /**
     * Called when the processing is complete and the final bitmap should be set
     * on
     * the ImageWorkerClient.
     *
     * @param client     the client
     * @param bitmapInfo the bitmap info
     */
    private void setImageBitmap(ImageWorkerClient client, BitmapInfo bitmapInfo) {

        client.setImageAndOrientation(bitmapInfo, bitmapInfo.getImageInfo().getOrientation(), new ImageInfo(bitmapInfo.getImageInfo()));
    }

    /**
     * Gets the device.
     *
     * @param imageInfo the image info
     * @return the device
     */
    private DeviceSLPF getDevice(ImageInfo imageInfo) {
        DeviceSLPF device = DataModelSLPF.getInstance().getDeviceById(imageInfo.getDeviceId());
        return device;
    }

    /**
     * Checks if is local device.
     *
     * @param imageInfo the image info
     * @return true, if is local device
     */
    private boolean isLocalDevice(ImageInfo imageInfo) {
        return imageInfo.getDeviceId() == this.localDeviceId;
    }
}


package platform.com.mfluent.asp.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;

import com.mfluent.log.Log;
import com.samsung.android.sdk.slinkcloud.CloudGatewayConstants;
import com.samsung.android.sdk.slinkcloud.CloudGatewayStorageUtils;
import com.samsung.android.slinkcloud.R;

import platform.com.mfluent.asp.media.AspThumbnailCache;
import platform.com.mfluent.asp.ui.StorageSignInSettingDialogFragment.StorageSignInDialogFragListener;
import platform.com.mfluent.asp.util.AspCommonUtils;
import platform.com.mfluent.asp.util.NetworkUtilSLPF;

public class AddWebStorageActivity extends Activity implements StorageSignInDialogFragListener {

    private static final String SIGN_IN_DIALOG_FRAGMENT_TAG = "dialog";
    private static final int REQUEST_CODE_OAUTH_SIGN_IN = 2201;
    public static final int REQ_CODE_FOR_PERMISSION = 1009;

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQ_CODE_FOR_PERMISSION:
                int nSize = grantResults.length;
                boolean bRet = true;
                for (int nCnt = 0; nCnt < nSize; nCnt++) {
                    Log.i(this, "permissions[nCnt]=" + permissions[nCnt]);
                    Log.i(this, "grantResults[nCnt]=" + grantResults[nCnt]);
                    if (grantResults[nCnt] != PackageManager.PERMISSION_GRANTED) {
                        bRet = false;
                        break;
                    }
                }
                if (bRet && AspCommonUtils.checkFileOpertionPermissioin() == false) {
                    Log.i(this, "permission check result fail:file op");
                    bRet = false;
                } else if (bRet && AspCommonUtils.checkAccountOperation(this) == false) {
                    Log.i(this, "permission check result fail:account op");
                    bRet = false;
                }
                if (bRet) {
                    AspThumbnailCache.getInstance(this).checkExternalCloudDirectory();
                    doCreateAfterPermissionCheck();
                } else {
                    AspCommonUtils.popupPermissionSetting(this);
                }
        }
    }


    private void doCreateAfterPermissionCheck() {

        if (!NetworkUtilSLPF.isNetworkAvailable()) {
            AlertDialog.Builder errorDialog = new AlertDialog.Builder(this);
            errorDialog.setTitle(R.string.common_popup_notification);
            errorDialog.setMessage(R.string.failed_to_connect);
            errorDialog.setNegativeButton(R.string.common_popup_confirm, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
            errorDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    finish();
                }
            });
            errorDialog.show();
            return;
        }

        String storageType = getIntent().getStringExtra(CloudGatewayStorageUtils.EXTRA_STORAGE_TYPE);

        boolean isUiAppTheme = getIntent().getBooleanExtra(CloudGatewayConstants.SLINK_UI_APP_THEME, false);

        String signInAccount = getIntent().getStringExtra(CloudGatewayConstants.SIGN_IN_ACCOUNT);

        if (storageType != null) {
            StorageSignInSettingDialogFragment.newInstance(storageType, isUiAppTheme, signInAccount).show(getFragmentManager(), SIGN_IN_DIALOG_FRAGMENT_TAG);
        } else {
            boolean bAdditionalCloud = getIntent().getBooleanExtra("additional_cloud_selection", false);

            if (bAdditionalCloud) {
                StorageSignInSettingDialogFragment.newInstanceForAdditionalClouds(isUiAppTheme).show(getFragmentManager(), SIGN_IN_DIALOG_FRAGMENT_TAG);
            } else {
                StorageSignInSettingDialogFragment.newInstance(isUiAppTheme).show(getFragmentManager(), SIGN_IN_DIALOG_FRAGMENT_TAG);
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            return;
        }
        boolean bCheckPermission = AspCommonUtils.checkDangerousPermission(this, this.getApplicationContext(), REQ_CODE_FOR_PERMISSION);

        if (bCheckPermission == true)
            return;

        doCreateAfterPermissionCheck();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQUEST_CODE_OAUTH_SIGN_IN:
                if (resultCode == RESULT_OK) {
                    setResult(RESULT_OK);
                }
                finish();
                break;
        }
    }

    @Override
    public void storageSignInDialogDismissed(boolean deviceRemoved) {
        finish();
    }

    @Override
    public void storageDeviceWasAddedInSignInDialogFrag() {
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void startOAuthSignInActivity(Intent intent) {
        startActivityForResult(intent, REQUEST_CODE_OAUTH_SIGN_IN);
    }

    @Override
    public void signInFailed() {
        finish();
    }
}

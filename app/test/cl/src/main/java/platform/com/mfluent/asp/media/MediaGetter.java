/**
 *
 */

package platform.com.mfluent.asp.media;

import android.os.ParcelFileDescriptor;

import com.mfluent.log.Log;

import java.io.FileNotFoundException;
import java.util.HashMap;

import platform.com.mfluent.asp.datamodel.ASPMediaStoreProvider;

/**
 * @author michaelgierach
 */
public abstract class MediaGetter {
    private static HashMap<AspMediaInfo, AspGetMediaTask> mCurrTasks = new HashMap<AspMediaInfo, AspGetMediaTask>();

    private final AspMediaInfo mMediaInfo;
    private final ASPMediaStoreProvider mProvider;

    public MediaGetter(AspMediaInfo mediaInfo, ASPMediaStoreProvider provider) {
        mMediaInfo = mediaInfo;
        mProvider = provider;
    }

    public abstract void cancel();

    protected abstract ParcelFileDescriptor getMedia() throws FileNotFoundException, InterruptedException;

    public final ParcelFileDescriptor openMedia() throws FileNotFoundException, InterruptedException {
        ParcelFileDescriptor result = null;

        AspGetMediaTask task = null;
        boolean found = true;

		/*
         * First we will check to make sure that this thread is the only
		 * one that is loading this thumbnail. If it is not, then we must
		 * wait until our turn.
		 */
        while (found) {

            synchronized (mCurrTasks) {
                task = mCurrTasks.get(mMediaInfo);
                if (task == null) {
                    task = new AspGetMediaTask(mMediaInfo);
                    mCurrTasks.put(mMediaInfo, task);
                    found = false;
                }
            }
            if (found) {
                synchronized (task) {
                    try {
                        task.wait();
                    } catch (InterruptedException ie) {
                        Log.d(this, "openMedia() - InterruptedException : " + ie.getMessage());
                    }
                }
            }
        }

		/*
         * It is now our turn to execute and get the thumbnail.
		 */
        try {
            if (result == null) {
                task.setMediaGetter(this);
                result = getMedia();
            }
        } finally {
            synchronized (mCurrTasks) {
                mCurrTasks.remove(mMediaInfo);
            }
            task.finish();
        }

        return result;
    }

    public final AspMediaInfo getMediaInfo() {
        return mMediaInfo;
    }

    public final ASPMediaStoreProvider getProvider() {
        return mProvider;
    }
}

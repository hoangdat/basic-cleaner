/**
 *
 */

package platform.com.mfluent.asp.filetransfer;

import android.content.Context;
import android.media.MediaScannerConnection;
import android.media.MediaScannerConnection.OnScanCompletedListener;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Base64;

import com.mfluent.asp.cloudstorage.api.sync.CloudStorageSync;
import com.mfluent.asp.common.datamodel.ASPFile;
import com.mfluent.asp.common.datamodel.ASPFileProvider;
import com.mfluent.log.Log;
import com.samsung.android.sdk.slinkcloud.CloudGatewayConstants;
import com.samsung.android.sdk.slinkcloud.CloudGatewayFileBrowserUtils;
import com.samsung.android.sdk.slinkcloud.CloudGatewayFileTransferUtils;
import com.samsung.android.sdk.slinkcloud.CloudGatewayFileTransferUtils.TransferOptions;

import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import platform.com.mfluent.asp.datamodel.DataModelSLPF;
import platform.com.mfluent.asp.datamodel.DeviceSLPF;
import platform.com.mfluent.asp.datamodel.filebrowser.ASPFileProviderFactory;
import platform.com.mfluent.asp.datamodel.filebrowser.CachedCloudFile;
import platform.com.mfluent.asp.util.FileUtils;
import platform.com.mfluent.asp.util.MFLStorageManagerSLPF;
import platform.com.mfluent.asp.util.UiUtilsSLPF;

public class DownloadTask extends FileTransferTask {

    public static class DownloadItemInfo extends TransferItemInfo {

        public String fullUri;
        public String sourceMediaId;
        public ASPFile aspFile;
        public String storageGatewayFileId;
        public String fileName;
        public String mimeType;
        public File targetFile;
        public long length;
        public boolean isDirectory = false;
        ASPFile aspDirectory = null;
    }

    public static class DownloadTaskInfo extends TransferTaskInfo {
        DeviceSLPF sourceDevice;
        DownloadItemInfo mainDownload;
        ArrayList<DownloadItemInfo> pairedFiles = new ArrayList<DownloadItemInfo>(2);
        public File targetDirectory;
        String targetRelativePath;
        boolean skipDownload = false;
        public File tmpFile;
        long totalBytesToTransfer;
    }

    private final List<DownloadTaskInfo> tasks = new ArrayList<DownloadTask.DownloadTaskInfo>();
    private final Set<DownloadTaskInfo> completedTasks = new HashSet<DownloadTask.DownloadTaskInfo>();
    private final Set<DownloadTaskInfo> skippedTasks = new HashSet<>();

    private final OnScanCompletedListener mOnScanCompletedListener;

    private final File mCacheDir;

    private Context mContext = null;
    private int nSentFileNum = 0;

    public DownloadTask(
            Context context,
            FileTransferTaskListener listener,
            OnScanCompletedListener onScanCompletedListener,
            File cacheDir,
            TransferOptions options,
            String strPremadeSession) {
        super(context, DataModelSLPF.getInstance().getLocalDevice(), listener, strPremadeSession, options);

        mOnScanCompletedListener = onScanCompletedListener;
        mCacheDir = cacheDir;
        mContext = context;
    }

    private File getTargetDirectory() {
        Log.d(this, "getTargetDirectory() ");
        return (getOptions().targetDirectoryPath != null) ? new File(getOptions().targetDirectoryPath) : MFLStorageManagerSLPF.getStorageFilesDirectory(false);
    }

    private File getTargetDirectoryConsideringOptions(DownloadTaskInfo taskInfo) {
        File ret;
        if (TextUtils.isEmpty(getOptions().targetDirectoryPath)) {
            ret = getTargetDirectory();
        } else {
            if (TextUtils.isEmpty(taskInfo.targetRelativePath)) {
                ret = new File(getOptions().targetDirectoryPath);
            } else {
                ret = new File(getOptions().targetDirectoryPath + "/" + taskInfo.targetRelativePath);
            }
        }
        return ret;
    }

    @Override
    public void addTask(ASPFile file, DeviceSLPF sourceDevice) {

        Log.d(this, "addTask() ASPFile : " + file + ", sourceDevice : " + sourceDevice);
        DownloadTaskInfo taskInfo = new DownloadTaskInfo();

        taskInfo.mainDownload = new DownloadItemInfo();
        if (file instanceof CachedCloudFile) {
            CachedCloudFile cachedFile = (CachedCloudFile) file;
            taskInfo.mainDownload.aspFile = null;
            taskInfo.mainDownload.storageGatewayFileId = cachedFile.getCloudId();
            Log.i(this, "addFile CachedCloudFile storageGatewayFileId=" + taskInfo.mainDownload.storageGatewayFileId);
        } else {
            taskInfo.mainDownload.aspFile = file;
        }

        taskInfo.mainDownload.fileName = file.getName();
        taskInfo.skipDownload = false;
        taskInfo.mainDownload.fullUri = null;
        taskInfo.mainDownload.sourceMediaId = null;
        taskInfo.sourceDevice = sourceDevice;

        String convertedFileName = taskInfo.mainDownload.fileName;

        // convert filename to ASP Media Type
        taskInfo.mainDownload.mimeType = UiUtilsSLPF.getMimeFromFilename(convertedFileName);
        taskInfo.targetDirectory = getTargetDirectory();//always save to Files directory
        taskInfo.targetRelativePath = null;
        if (getOptions().targetDirectoryPath != null) {
            taskInfo.mainDownload.targetFile = new File(getOptions().targetDirectoryPath, convertedFileName);
        } else {
            taskInfo.mainDownload.targetFile = new File(taskInfo.targetDirectory, convertedFileName);
        }
        taskInfo.mainDownload.length = file.length();
        taskInfo.totalBytesToTransfer = file.length();
        setTotalBytesToTransfer(getTotalBytesToTransfer() + taskInfo.totalBytesToTransfer);
        tasks.add(taskInfo);
        addSourceDevice(sourceDevice);
    }

    @Override
    public boolean addTask(ASPFile file, DeviceSLPF sourceDevice, String relativeTarget, Map<String, String> targetFolderCache) {
        Log.d(this, "addTask() ASPFile : " + file + ", sourceDevice : " + sourceDevice);
        boolean bRet = true;
        DownloadTaskInfo taskInfo = new DownloadTaskInfo();

        taskInfo.mainDownload = new DownloadItemInfo();
        if (file instanceof CachedCloudFile) {
            CachedCloudFile cachedFile = (CachedCloudFile) file;
            taskInfo.mainDownload.aspFile = null;
            taskInfo.mainDownload.storageGatewayFileId = cachedFile.getCloudId();
            Log.i(this, "addTask() - CachedCloudFile storageGatewayFileId=" + taskInfo.mainDownload.storageGatewayFileId);
        } else {
            taskInfo.mainDownload.aspFile = file;
        }
        if (file.isDirectory()) {
            taskInfo.mainDownload.aspDirectory = file;
            taskInfo.mainDownload.isDirectory = true;
        }
        taskInfo.mainDownload.fileName = file.getName();
        taskInfo.skipDownload = file.isDirectory();
        taskInfo.mainDownload.fullUri = null;
        taskInfo.mainDownload.sourceMediaId = null;
        taskInfo.sourceDevice = sourceDevice;

        String convertedFileName = taskInfo.mainDownload.fileName;

        // convert filename to ASP Media Type
        taskInfo.mainDownload.mimeType = UiUtilsSLPF.getMimeFromFilename(convertedFileName);

        taskInfo.targetDirectory = getTargetDirectory();
        taskInfo.targetRelativePath = relativeTarget;
        if (getOptions().targetDirectoryPath != null) {
            String strTargetPath = getOptions().targetDirectoryPath + "/" + relativeTarget;
            taskInfo.mainDownload.targetFile = new File(strTargetPath, convertedFileName);
            Log.i(this, "addTask() - targetFile=" + taskInfo.mainDownload.targetFile.getAbsolutePath());
        } else {
            taskInfo.mainDownload.targetFile = new File(taskInfo.targetDirectory, convertedFileName);
        }
        taskInfo.mainDownload.length = file.length();
        taskInfo.totalBytesToTransfer = file.length();
        setTotalBytesToTransfer(getTotalBytesToTransfer() + taskInfo.totalBytesToTransfer);
        tasks.add(taskInfo);
        addSourceDevice(sourceDevice);
        return bRet;
    }

    @Override
    public boolean isDownload() {
        return true;
    }

    @Override
    public int getNumberOfFiles() {
        return tasks.size();
    }

    private void doItemTransfer(DownloadTaskInfo task, DownloadItemInfo downloadItem) throws Exception {
        Log.d(this, "doItemTransfer() called");

        DeviceSLPF sourceDevice = task.sourceDevice;
        if (task.mainDownload.targetFile != null) {
            sourceDevice.getCloudStorageSync().downloadFile(this, downloadItem.storageGatewayFileId,
                    task.mainDownload.targetFile.getParent(), task.mainDownload.targetFile.getName(), getOptions().waitForRename);
        }
    }

    @Override
    public void destroy() {
        for (DownloadTaskInfo task : tasks) {
            FileUtils.deleteQuietly(task.tmpFile);
        }
    }

    private static class MetadataFixingScanCompleteListener implements OnScanCompletedListener {

        private static OnScanCompletedListener mBaseOnScanCompletedListener;

        public MetadataFixingScanCompleteListener(OnScanCompletedListener baseOnScanCompletedListener) {
            mBaseOnScanCompletedListener = baseOnScanCompletedListener;
        }

        @Override
        public void onScanCompleted(String path, Uri uri) {

            Log.d(this, "Enter MetadataFixingScanCompleteListener::onScanCompleted() - path : " + path + ", uri : " + uri);
            if (mBaseOnScanCompletedListener != null) {
                mBaseOnScanCompletedListener.onScanCompleted(path, uri);
            }
        }
    }

    @Override
    public int getSentFileNumForMultiChannelSending() {
        return nSentFileNum;
    }

    @Override
    public int getCurrentFileIndex() {
        long totalBytesTransferred = getBytesTransferred();
        long sum = 0;
        int currentFileIndex = 0;
        for (DownloadTaskInfo downloadTaskInfo : tasks) {
            sum += downloadTaskInfo.mainDownload.length;
            if (totalBytesTransferred <= sum) {
                break;
            }

            currentFileIndex++;
        }

        return currentFileIndex;
    }

    //multi channel codes
    @Override
    protected void prepareMultiChannel() throws Exception {
        super.prepareMultiChannel();
        //check folder to file list to send
        ASPFileProvider fileProvider = beginFolderSourceAnalysisBeforeSending();
        if (fileProvider != null) {
            String strCloudId = null;

            int nSize = tasks.size();
            for (int nCnt = 0; nCnt < nSize; nCnt++) {
                DownloadTaskInfo taskInfo = tasks.get(nCnt);
                //for (DownloadTaskInfo taskInfo : this.tasks) {
                if (taskInfo.mainDownload.isDirectory) {
                    try {
                        if (taskInfo.mainDownload.storageGatewayFileId != null)
                            strCloudId = taskInfo.mainDownload.storageGatewayFileId;
                        else if (taskInfo.mainDownload.aspFile != null)
                            strCloudId = fileProvider.getStorageGatewayFileId(taskInfo.mainDownload.aspFile);
                    } catch (Exception e) {
                        Log.e(this, "prepareMultiChannel() - Exception : " + e.getMessage());
                        strCloudId = null;
                    }
                    checkSourceFolderBeforeSending(fileProvider, strCloudId, taskInfo.mainDownload.aspDirectory, taskInfo.mainDownload.fileName);
                }
            }
            endFolderSourceAnalysisBeforeSending();
        }
        //check rename condition
        if (getOptions().waitForRename && getOptions().targetDirectoryPath != null) {
            for (final DownloadTaskInfo taskInfo : tasks) {
                if (isCanceled()) {
                    Log.i(this, "prepareMultiChannel() - canceled");
                    break;
                }

                if (taskInfo.mainDownload.isDirectory) {
                    Log.i(this, "skip checkRename " + taskInfo.mainDownload.fileName + " due to dirprecheck done");
                    continue;
                }
                String targetDirId = null;
                if (taskInfo.targetRelativePath != null) {
                    targetDirId = getOptions().targetDirectoryPath + "/" + taskInfo.targetRelativePath;
                } else if (getOptions().targetDirectoryPath != null) {
                    targetDirId = getOptions().targetDirectoryPath;
                }
                String localTargetStorageGateway = null;
                try {
                    localTargetStorageGateway = new String(Base64.encode(targetDirId.getBytes(), Base64.NO_WRAP | Base64.URL_SAFE));
                } catch (Exception e) {
                    Log.e(this, "prepareMultiChannel() - Exception : " + e.getMessage());
                }

                requestRename(localTargetStorageGateway, taskInfo.mainDownload.fileName, new RenameDataItem.OnReceiveRSPRenameStatus() {
                    @Override
                    public void onReceiveRenameStatus(RenameDataItem dataItem, String strRenamed) {
                        Log.d(this, "onReceiveRenameStatus - newName: " + strRenamed);
                        processRenameResult(taskInfo, dataItem, strRenamed);
                    }
                });
            }
        }
    }

    @Override
    protected void processRenameResult(TransferTaskInfo taskInfo, RenameDataItem dataItem, String strNewName) {
        int nRspStatus = 0;
        if (dataItem != null) {
            nRspStatus = dataItem.nStatus;
            if (dataItem.bApplyAll) {
                mbRenameApplyedAll = true;
                m_nRenameApplyedAllStatus = nRspStatus;
            }
        }
        if (taskInfo instanceof DownloadTaskInfo) {
            DownloadTaskInfo downTask = (DownloadTaskInfo) taskInfo;
            if (nRspStatus == CloudGatewayFileTransferUtils.RENAME_STATUS_RSP_RENAME) {
                Log.i(this, "processRenameResult RENAME_STATUS_RSP_RENAME");
                downTask.targetDirectory = getTargetDirectoryConsideringOptions(downTask);
                downTask.mainDownload.targetFile = new File(downTask.targetDirectory, strNewName);
            } else if (nRspStatus == CloudGatewayFileTransferUtils.RENAME_STATUS_RSP_REPLACE) {
                if (!downTask.mainDownload.isDirectory) {
                    String[] fileIds = new String[]{downTask.mainDownload.targetFile.getAbsolutePath()};
                    CloudGatewayFileBrowserUtils.getInstance(mContext).mrrControlBatchCommand(getTargetDevice().getId(), CloudGatewayFileBrowserUtils.REMOTE_FOLDER_RMDIR_BATCH, fileIds, null);
                }
            } else if (nRspStatus == CloudGatewayFileTransferUtils.RENAME_STATUS_RSP_SKIP) {
                Log.i(this, "processRenameResult RENAME_STATUS_RSP_SKIP");
                downTask.skipDownload = true;
            }
        }
    }

    @Override
    protected boolean onExitMultiChannel() {
        if (getOptions().deleteSourceFilesWhenTransferIsComplete) {
            String[] cloudIds = new String[completedTasks.size()];
            int count = 0, deviceId = 0;

            for (DownloadTaskInfo task : completedTasks) {
                deviceId = task.sourceDevice.getId();
                if (task.skipDownload) {
                    Log.d(this, "onExitMultiChannel - task.skipDownload : " + task.targetRelativePath);
                    continue;
                }

                if (task.mainDownload.aspFile != null) {
                    try {
                        ASPFileProvider fileProvider = ASPFileProviderFactory.getFileProviderForDevice(mContext, task.sourceDevice);
                        String cloudId = fileProvider.getStorageGatewayFileId(task.mainDownload.aspFile);
                        cloudIds[count++] = cloudId;
                    } catch (Exception e) {
                        Log.e(this, "onExitMultiChannel() - Exception : " + e.getMessage());
                    }
                } else if (task.mainDownload.storageGatewayFileId != null) {
                    cloudIds[count++] = task.mainDownload.storageGatewayFileId;
                }
            }
            return CloudGatewayFileBrowserUtils.getInstance(mContext).mrrControlBatchCommand(deviceId, CloudGatewayFileBrowserUtils.REMOTE_FOLDER_RMDIR_PERMANENTLY_BATCH, cloudIds, null);
        }
        return true;
    }

    @Override
    protected void doThreadSafeTransfer(int nChannelId) throws Exception {
        Log.d(this, "doMultiChannelTransferItem id=" + nChannelId);
        int nItemCnt = 0;

        for (DownloadTaskInfo task : tasks) {
            if (isCanceled()) {
                Log.d(this, "doThreadSafeTransfer() canceled");
                break;
            }
            nItemCnt++;
            if (!checkMultiChannelItem(nChannelId, nItemCnt)) {
                continue;
            }
            lockMultiChannel();
            if (completedTasks.contains(task)) {
                unlockMultiChannel();
                //this task already completed
                bytesTransferred(task.totalBytesToTransfer);
                // nSentFileNum++;
                continue;
            } else {
                unlockMultiChannel();
            }

            if (getOptions().skipIfDuplicate) {
                if ((task.mainDownload.aspFile != null) && (task.mainDownload.targetFile.exists())) {
                    if ((task.mainDownload.aspFile.length() <= 0) || (task.mainDownload.targetFile.length() == task.mainDownload.aspFile.length())) {
                        task.skipDownload = true;
                        Log.d(this, "doMultiChannelTransferItem: duplicate file: " + task.mainDownload.targetFile);
                    }
                }
            }

            if (task.skipDownload) {
                bytesTransferred(task.totalBytesToTransfer);
                if (mOnScanCompletedListener != null) {
                    MediaScannerConnection.scanFile(
                            getContext(),
                            new String[]{task.mainDownload.targetFile.getAbsolutePath()},
                            StringUtils.isEmpty(task.mainDownload.mimeType) ? null : new String[]{task.mainDownload.mimeType},
                            new MetadataFixingScanCompleteListener(mOnScanCompletedListener));
                }
                lockMultiChannel();
                completedTasks.add(task);
                skippedTasks.add(task);
                nSentFileNum++;
                Log.i(this, "nSentFileNum increased by skipDownload");
                unlockMultiChannel();
                //process the rest of the tasks
                continue;
            }
            setTransferStarted(true);

            //download the caption files first so that when the video file is scanned, the caption information will be available
            for (DownloadItemInfo pairedItem : task.pairedFiles) {
                doItemTransfer(task, pairedItem);
            }

            doItemTransfer(task, task.mainDownload);
            lockMultiChannel();
            completedTasks.add(task);
            Log.i(this, "nSentFileNum increased by doItemTransfer");
            nSentFileNum++;
            unlockMultiChannel();
            Thread.sleep(50);
        }
    }

    //added by shirley for persistent file transfer
    @Override
    public long getSourceMediaId(int index) {
        return 0;
    }

    @Override
    public void cancel() {
        super.cancel();

        DeviceSLPF targetDevice = getTargetDevice();
        if (targetDevice != null) {
            CloudStorageSync cloudStorageSync = targetDevice.getCloudStorageSync();
            if (cloudStorageSync != null) {
                cloudStorageSync.cancel(CloudGatewayConstants.OperationType.DOWNLOAD);
            }
        }
    }
}

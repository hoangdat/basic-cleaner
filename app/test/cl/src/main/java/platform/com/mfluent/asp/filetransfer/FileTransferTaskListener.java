/**
 * 
 */

package platform.com.mfluent.asp.filetransfer;

import com.mfluent.asp.common.io.util.StreamProgressListener;

/**
 * @author Ilan Klinghofer
 */
public interface FileTransferTaskListener extends StreamProgressListener {

	void stateChanged(FileTransferTask task);
}

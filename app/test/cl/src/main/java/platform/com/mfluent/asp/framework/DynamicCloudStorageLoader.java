
package platform.com.mfluent.asp.framework;

import android.content.Context;

import com.mfluent.asp.cloudstorage.api.sync.CloudStorageSync;
import com.mfluent.log.Log;

/**
 * List of available storage types:
 * http://d3j2aiqzxb5fab.cloudfront.net/jar_collection/cloudPlugins.lst
 * TODO: move point this at a Samsung hosted server.
 *
 * @author chris
 */
public class DynamicCloudStorageLoader {

    private static final String TAG = "DynamicCloudStorageLoader";

    public static synchronized CloudStorageSync newInstanceOf(StorageProviderInfo cloudStorageType, Context context) {
        if (cloudStorageType == null) {
            return null;
        }

        CloudStorageSync result = DynamicCloudStorageLoader.loadFromClassPath(cloudStorageType);
        if (result != null) {
            result.setApplicationContext(context.getApplicationContext());
        }
        return result;
    }

    /**
     * This is primarily for debugging. This also allows client to be shipped with a default version of a plugin
     * but server version will take precedence over this version.
     *
     * @param cloudStorageType
     * @return
     */
    private static CloudStorageSync loadFromClassPath(StorageProviderInfo cloudStorageType) {
        CloudStorageSync cloudStorageSync = null;
        try {
            Class<?> cloudStorageClass = Class.forName(cloudStorageType.getMain());
            cloudStorageSync = (CloudStorageSync) cloudStorageClass.newInstance();

            Log.d(TAG, "loadFromClassPath() - Loaded [" + cloudStorageType.getMain() + "] from classpath.");
        } catch (Exception e) {
            Log.e(TAG, "loadFromClassPath() - Failed to load \"" + cloudStorageType.getMain() + "] from class path. + " + e.getCause());
        }
        return cloudStorageSync;
    }

    private DynamicCloudStorageLoader() {
    }
}

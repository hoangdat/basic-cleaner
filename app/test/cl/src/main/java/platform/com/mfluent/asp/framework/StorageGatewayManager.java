
package platform.com.mfluent.asp.framework;

import android.content.Context;

import com.mfluent.asp.cloudstorage.api.sync.CloudStorageSync;
import com.mfluent.log.Log;
import com.samsung.android.sdk.slinkcloud.CloudGatewayDeviceTransportType;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.ConnectException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import platform.com.mfluent.asp.datamodel.DataModelSLPF;
import platform.com.mfluent.asp.datamodel.DeviceSLPF;
import platform.com.mfluent.asp.util.DeviceUtilSLPF;

public class StorageGatewayManager {
    public static final String PROVIDER_LIST_INITIALIZED = "ProviderListInitialized";

    //jsub12_150916
    private static final ArrayList<String> mPossibleSpList = new ArrayList<String>();

    private static StorageGatewayManager sInstance;

    private Context mContext;

    public static synchronized StorageGatewayManager getInstance(Context context) {
        if (context == null) {
            throw new NullPointerException();
        }
        if (sInstance == null) {
            sInstance = new StorageGatewayManager(context);
        }
        return sInstance;
    }

    private StorageGatewayManager(Context context) {
        mContext = context;
        try {
            mPossibleSpList.clear();
            mPossibleSpList.add(DeviceUtilSLPF.GOOGLEDRIVE_WEBSTORAGE_NAME);
            mPossibleSpList.add(DeviceUtilSLPF.SAMSUNGDRIVE_WEBSTORAGE_NAME);
        } catch (Exception e) {
            Log.e(this, "StorageGatewayManager() - Exception : " + e.getMessage());
        }
    }

    /**
     * @return
     */
    public void getStorageProviderList() {
        Log.v(this, "getStorageProviderList() [START] ##");

        //read the service providers list from asset folder
        try {
            IASPApplication2 aspApp = ServiceLocatorSLPF.get(IASPApplication2.class);
            parseStorageProviderResponse(aspApp.getContentOfFileInAsset("clouds/overrideStorageProvider.json"));
            return;
        } catch (Exception e) {
            Log.e(this, "getStorageProviderList() - Exception : " + e.getMessage());
        }

    }

    private void parseStorageProviderResponse(String response) throws JSONException {
        StorageProviderDatabaseHelper databaseHelper = StorageProviderDatabaseHelper.getInstance(mContext);

        JSONObject jsonObject = new JSONObject(response);
        JSONObject spList = jsonObject.optJSONObject("Response");

        if (spList != null) {
            // get the all
            ConcurrentHashMap<String, StorageProviderInfo> spNameToProviderMap = new ConcurrentHashMap<>();
            List<StorageProviderInfo> allProviders = databaseHelper.findAllStorageProviders();
            for (Iterator<StorageProviderInfo> iterator = allProviders.iterator(); iterator.hasNext(); ) {
                StorageProviderInfo storageProviderInfo = iterator.next();
                spNameToProviderMap.put(storageProviderInfo.getSpName(), storageProviderInfo);
            }

            JSONArray serviceProviders = spList.optJSONArray("ServiceProvider");
            if (serviceProviders != null) {
                // go thru JSONArray
                for (int i = 0; i < serviceProviders.length(); i++) {
                    JSONObject serviceProviderJSON = serviceProviders.getJSONObject(i);
//jsub12_150916
                    if (!mPossibleSpList.contains(serviceProviderJSON.getString("SpName"))) {
                        Log.i(this, "Skip " + serviceProviderJSON.getString("SpName"));
                        continue;
                    }

                    StorageProviderInfo provider = parseServiceProvider(serviceProviderJSON);

                    // save the display order (sort key)
                    provider.setSortKey(i);

                    // remove from existing providers lookup table
                    spNameToProviderMap.remove(provider.getSpName());

                    // save the order
                    databaseHelper.saveOrUpdate(provider);

                    Log.v(this, "parseStorageProviderResponse() - adding sp array item : " + provider.getName());
                }

            } else {
                // only 1 provider
                JSONObject serviceProviderJSON = spList.optJSONObject("ServiceProvider");
//jsub12_150916
                if (serviceProviderJSON != null
                        && mPossibleSpList.contains(serviceProviderJSON.getString("SpName"))) {

                    StorageProviderInfo provider = parseServiceProvider(serviceProviderJSON);

                    // set the display order (sort key) to 0
                    provider.setSortKey(0);

                    // remove from existing providers lookup table
                    spNameToProviderMap.remove(provider.getSpName());

                    databaseHelper.saveOrUpdate(provider);

                    Log.v(this, "parseStorageProviderResponse() - adding sp item 1 : " + provider.getName());
                }
            }

            // anything left in the map needs to be removed from allowed storage
            for (StorageProviderInfo provider : spNameToProviderMap.values()) {
                provider.setLoginStatus(false);
                provider.setForbidden(true);
                databaseHelper.saveOrUpdate(provider);
            }
        }

        databaseHelper.saveOrUpdateMetaData(PROVIDER_LIST_INITIALIZED, Boolean.toString(true));
    }

    private StorageProviderInfo parseServiceProvider(JSONObject serviceProvider) throws JSONException {

        StorageProviderDatabaseHelper databaseHelper = StorageProviderDatabaseHelper.getInstance(mContext);
        String name = serviceProvider.getString("SpName");

        StorageProviderInfo storageProviderInfo = databaseHelper.get(name);

        if (storageProviderInfo == null) {
            storageProviderInfo = new StorageProviderInfo();
            storageProviderInfo.setSpName(name);
        }

        // required
        storageProviderInfo.setName(serviceProvider.getString("Name"));

        if (StringUtils.isNotEmpty(serviceProvider.optString("IsOAuth"))) {
            boolean isOauth = serviceProvider.optBoolean("IsOAuth");
            storageProviderInfo.setOAuth(isOauth);
        }

        if (StringUtils.isNotEmpty(serviceProvider.optString("Type"))) {
            storageProviderInfo.setType(serviceProvider.optString("Type"));
        }

        String applicableValue = serviceProvider.optString("Applicable");
        if (StringUtils.isNotEmpty(applicableValue)) {
            if ("false".equalsIgnoreCase(applicableValue)) {
                storageProviderInfo.setForbidden(true);
            } else {
                storageProviderInfo.setForbidden(false);
            }
        } else {
            storageProviderInfo.setForbidden(false);
        }

        String accountID = serviceProvider.optString("AccountID");
        if (StringUtils.isNotEmpty(accountID)) {

            DataModelSLPF dataModel = DataModelSLPF.getInstance();

            List<DeviceSLPF> storageDevicesInDataModel = dataModel.getDevicesByType(CloudGatewayDeviceTransportType.WEB_STORAGE);

            for (DeviceSLPF device : storageDevicesInDataModel) {

                if (name.equalsIgnoreCase(device.getWebStorageType())) {
                    if (device.getWebStorageUserId() != null && !accountID.equalsIgnoreCase(device.getWebStorageUserId())) {

                        try {
                            CloudStorageSync cloudStorageSync = device.getCloudStorageSync();
                            if (cloudStorageSync != null) {
                                Log.e(this, "parseServiceProvider() - cloudStorageSync.reset()");
                                cloudStorageSync.reset();
                            }
                        } catch (Exception e) {
                            Log.e(this, "parseServiceProvider() - Exception : " + e.getMessage());
                        }

                        device.setWebStorageUserId(null);
                        device.setWebStoragePw(null);

                        try {
                            Log.e(this, "parseServiceProvider() - device.delete()");
                            // delete device
                            device.delete();
                        } catch (Exception e) {
                            Log.e(this, "parseServiceProvider() - Exception : " + e.getMessage());
                        }

                    }
                    break;
                }
            }
        }

        return storageProviderInfo;
    }

    public String logout(String provider) throws ConnectException {
//		String logoutStatus = null;
//		String result = null;
//
//		String uri = "ssg/websvc/" + provider + "/auth/logout.json";

        IASPApplication2.traceStack();

        Log.v(this, "logout() - no auth info, no storagegateway");
        StorageProviderDatabaseHelper providerDB = StorageProviderDatabaseHelper.getInstance(this.mContext);
        StorageProviderInfo providerInfo = providerDB.get(provider);
        if (providerInfo != null) {
            providerInfo.setLoginStatus(false);
            providerDB.saveOrUpdate(providerInfo);
        }
        return "false";
    }
}

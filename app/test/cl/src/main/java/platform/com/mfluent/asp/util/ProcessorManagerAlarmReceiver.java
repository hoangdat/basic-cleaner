
package platform.com.mfluent.asp.util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class ProcessorManagerAlarmReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		ProcessorManager processorManager = ProcessorManager.getInstance(context);

		processorManager.onTimer();
	}

}


package platform.com.mfluent.asp.datamodel;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.os.ParcelFileDescriptor;

import com.mfluent.asp.common.datamodel.ASPMediaStore;
import com.mfluent.asp.common.media.thumbnails.ImageInfo;
import com.mfluent.log.Log;
import com.samsung.android.sdk.slinkcloud.CloudGatewayMediaStore;
import com.samsung.android.sdk.slinkcloud.CloudGatewayMediaStore.BaseSamsungLinkColumns;
import com.samsung.android.sdk.slinkcloud.CloudGatewayMediaStore.ThumbnailColumns;

import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.FileNotFoundException;
import java.util.concurrent.ConcurrentHashMap;

import platform.com.mfluent.asp.media.AspThumbnailCache;
import platform.com.mfluent.asp.media.CloudStorageThumbnailGetter;
import platform.com.mfluent.asp.media.ThumbnailGetter;
import uicommon.com.mfluent.asp.util.FileImageInfo;
import uicommon.com.mfluent.asp.util.FrameworkReflector;

public abstract class ThumbnailMediaInfo extends MediaInfo {

    private static class ThumbnailKey {

        public final long mGroupId;
        public final long mEntryId;
        public final int mType;
        public final long mDeviceId;

        public ThumbnailKey(ImageInfo imageInfo, long groupId) {
            mGroupId = groupId;
            mEntryId = imageInfo.getContentId();
            mType = imageInfo.getMediaType();
            mDeviceId = imageInfo.getDeviceId();
        }

        public ThumbnailKey(long entryId, int type, long groupId, long deviceId) {
            mEntryId = entryId;
            mType = type;
            mGroupId = groupId;
            mDeviceId = deviceId;
        }

        @Override
        public int hashCode() {
            return new HashCodeBuilder().append(mEntryId).append(mEntryId).append(mGroupId).append(mDeviceId).toHashCode();
        }

        @Override
        public boolean equals(Object o) {
            if (o == null) {
                return false;
            }

            if (o == this) {
                return true;
            }
            ThumbnailKey other = (ThumbnailKey) o;
            return (other.mEntryId == mEntryId && other.mGroupId == mGroupId && other.mType == mType && other.mDeviceId == mDeviceId);
        }
    }

    private static final String[] QUERY_FIELD_PROJECTION = {
            CloudGatewayMediaStore.ThumbnailColumns._ID,
            CloudGatewayMediaStore.ThumbnailColumns.SIZE,
            CloudGatewayMediaStore.ThumbnailColumns.WIDTH,
            CloudGatewayMediaStore.ThumbnailColumns.HEIGHT,
            CloudGatewayMediaStore.ThumbnailColumns.FULL_WIDTH,
            CloudGatewayMediaStore.ThumbnailColumns.FULL_HEIGHT,
            CloudGatewayMediaStore.ThumbnailColumns.THUMB_WIDTH,
            CloudGatewayMediaStore.ThumbnailColumns.THUMB_HEIGHT,
            CloudGatewayMediaStore.ThumbnailColumns.ORIENTATION};

    private static ConcurrentHashMap<ThumbnailKey, ThumbnailGetter> sRunningGetters = new ConcurrentHashMap<ThumbnailKey, ThumbnailGetter>();

    @Override
    public Uri getEntryUri(long rowId) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Uri getContentUri() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Uri getContentUriForDevice(long deviceId) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String[] getStreamContentType(MediaInfoContext mediaInfoContext) {
        return new String[]{"image/jpeg"};
    }

    @Override
    public final int handleDelete(MediaInfoContext mediaInfoContext, String where, String[] whereArgs, boolean cleanupOrphans) {

        return 0;
    }

    @Override
    public int handleUpdate(MediaInfoContext mediaInfoContext, ContentValues values, String where, String[] whereArgs, long recordId, ContentValues origValues) {
        return 0;
    }

    @Override
    public long handleInsert(MediaInfoContext mediaInfoContext, ContentValues values, ContentValues origValues) {
        return 0;
    }

    protected abstract int getMediaType();

    @Override
    public Cursor handleQuery(
            MediaInfoContext mediaInfoContext,
            String[] projection,
            String where,
            String[] whereArgs,
            String sortBy,
            String groupBy,
            String having,
            String limit,
            long entryId) {

        if (mediaInfoContext.patternId != ASPMediaStoreProvider.ASP_ENTRY_PATTERN && mediaInfoContext.patternId != ASPMediaStoreProvider.ASP_DEVICE_ENTRY_PATTERN) {
            throw new IllegalArgumentException("Only single entries may be queried. Please use an entry URI.");
        }
        ImageInfo mediaInfo = getImageInfo(mediaInfoContext, entryId);
        boolean cancel = getQueryStringBoolean(mediaInfoContext.uri, CloudGatewayMediaStore.ThumbnailColumns.QUERY_STR_CANCEL, false);
        if (cancel) {
            long groupId = getQueryStringLong(mediaInfoContext.uri, CloudGatewayMediaStore.ThumbnailColumns.QUERY_STR_GROUP_ID, 0);

            if (mediaInfo != null) {
                ThumbnailKey key = new ThumbnailKey(entryId, getMediaType(), groupId, mediaInfo.getDeviceId());
                ThumbnailGetter thumbGetter = sRunningGetters.get(key);
                if (thumbGetter != null) {
                    sRunningGetters.remove(key);
                    thumbGetter.cancel();
                }
            }
            return null;
        }

        //validate projection that was passed in
        if (projection != null) {
            for (String field : projection) {
                //if (!ArrayUtils.contains(QUERY_FIELD_PROJECTION, field)) {
                if (!FrameworkReflector.ArrayUtils_contains(null, QUERY_FIELD_PROJECTION, field)) {
                    throw new IllegalArgumentException("Illegal Field: " + field);
                }
            }
        } else {
            projection = QUERY_FIELD_PROJECTION;
        }

        MatrixCursor result = new MatrixCursor(projection);
        AspThumbnailCache fileCache = AspThumbnailCache.getInstance(mediaInfoContext.provider.getContext());

        if (mediaInfo != null) {
            FileImageInfo fileInfo = fileCache.getFile(entryId, getMediaType(), mediaInfo.getDeviceId());

            if (fileInfo != null) {
                ImageInfo imageInfo = fileInfo.getImageInfo();
                if (imageInfo != null) {
                    Object[] row = new Object[projection.length];
                    for (int i = 0; i < projection.length; ++i) {
                        if (ThumbnailColumns._ID.equals(projection[i])) {
                            row[i] = Long.valueOf(entryId);
                        } else if (ThumbnailColumns.SIZE.equals(projection[i])) {
                            if (fileInfo.getFile() != null) {
                                row[i] = Long.valueOf(fileInfo.getFile().length());
                            }
                        } else if (ThumbnailColumns.WIDTH.equals(projection[i])) {
                            row[i] = Integer.valueOf(imageInfo.getDesiredWidth());
                        } else if (ThumbnailColumns.HEIGHT.equals(projection[i])) {
                            row[i] = Integer.valueOf(imageInfo.getDesiredHeight());
                        } else if (ThumbnailColumns.FULL_WIDTH.equals(projection[i])) {
                            row[i] = Integer.valueOf(imageInfo.getFullWidth());
                        } else if (ThumbnailColumns.FULL_HEIGHT.equals(projection[i])) {
                            row[i] = Integer.valueOf(imageInfo.getFullHeight());
                        } else if (ThumbnailColumns.ORIENTATION.equals(projection[i])) {
                            row[i] = Integer.valueOf(imageInfo.getOrientation());
                        } else if (ThumbnailColumns.THUMB_WIDTH.equals(projection[i])) {
                            row[i] = Integer.valueOf(imageInfo.getThumbWidth());
                        } else if (ThumbnailColumns.THUMB_HEIGHT.equals(projection[i])) {
                            row[i] = Integer.valueOf(imageInfo.getThumbHeight());
                        } else if (ThumbnailColumns.DEVICE_ID.equals(projection[i])) {
                            row[i] = Integer.valueOf(imageInfo.getDeviceId());
                        }
                    }

                    result.addRow(row);
                }
            }
        }


        return result;
    }

    protected abstract String[] getQueryTableProjection();

    protected ImageInfo getImageInfo(MediaInfoContext mediaInfoContext, long entryId) {

        Long lockToken = mediaInfoContext.dbLock.readLock();

        String tableName;
        if (mediaInfoContext.patternId == ASPMediaStoreProvider.ASP_DEVICE_ENTRY_PATTERN) {
            tableName = CloudGatewayMediaStore.CloudFiles.PATH;
        } else {
            tableName = (mediaInfoContext.uri.toString().contains(":")) ? FilesMediaInfo.TABLE_NAME : CloudGatewayMediaStore.Files.CONTENT_URI.toString();
        }

        String[] projection = getQueryTableProjection();
        ImageInfo result = null;
        try(Cursor cursor = mediaInfoContext.db.query(
                tableName,
                projection,
                BaseSamsungLinkColumns._ID + "=?",
                new String[]{Long.toString(entryId)},
                null,
                null,
                null)) {

            if (cursor != null && cursor.moveToFirst()) {
                result = ImageInfo.fromCursor(cursor);
            }
        } catch (Exception e) {
            Log.d(this, "getImageInfo() - Exception : " + e.getMessage());
        } finally {
            mediaInfoContext.dbLock.readUnlock(lockToken);
            return result;
        }
    }

    @Override
    public ParcelFileDescriptor openFile(MediaInfoContext mediaInfoContext, long entryId) throws FileNotFoundException, InterruptedException {
        ImageInfo imageInfo = getImageInfo(mediaInfoContext, entryId);
        if (imageInfo == null) {
            throw new FileNotFoundException("Record not found for Uri: " + mediaInfoContext.uri);
        }
        //IASPApplication2.traceStack();

        boolean skipCacheGet = getQueryStringBoolean(mediaInfoContext.uri, CloudGatewayMediaStore.ThumbnailColumns.QUERY_STR_SKIP_CACHE_GET, true);
        boolean skipCachePut = getQueryStringBoolean(mediaInfoContext.uri, CloudGatewayMediaStore.ThumbnailColumns.QUERY_STR_SKIP_CACHE_PUT, true);
        boolean readCacheOnly = getQueryStringBoolean(mediaInfoContext.uri, CloudGatewayMediaStore.ThumbnailColumns.QUERY_STR_CACHE_ONLY, false);
        long groupId = getQueryStringLong(mediaInfoContext.uri, CloudGatewayMediaStore.ThumbnailColumns.QUERY_STR_GROUP_ID, 0);
        int width = getQueryStringInt(
                mediaInfoContext.uri,
                CloudGatewayMediaStore.ThumbnailColumns.QUERY_STR_WIDTH,
                Math.max(ASPMediaStore.DEFAULT_THUMBNAIL_WIDTH, ASPMediaStore.DEFAULT_THUMBNAIL_HEIGHT));
        int height = getQueryStringInt(
                mediaInfoContext.uri,
                CloudGatewayMediaStore.ThumbnailColumns.QUERY_STR_HEIGHT,
                Math.max(ASPMediaStore.DEFAULT_THUMBNAIL_WIDTH, ASPMediaStore.DEFAULT_THUMBNAIL_HEIGHT));

        AspThumbnailCache fileCache = AspThumbnailCache.getInstance(mediaInfoContext.provider.getContext());

        imageInfo.setDesiredWidth(width);
        imageInfo.setDesiredHeight(height);

        ThumbnailGetter thumbGetter = new CloudStorageThumbnailGetter(imageInfo, fileCache);

        ThumbnailKey key = new ThumbnailKey(imageInfo, groupId);
        sRunningGetters.put(key, thumbGetter);
        try {
            return thumbGetter.openThumbnailFileDescriptor(mediaInfoContext.provider, !skipCacheGet, !skipCachePut, readCacheOnly, groupId);
        } finally {
            sRunningGetters.remove(key);
        }

    }

    private boolean getQueryStringBoolean(Uri uri, String key, boolean defaultValue) {
        boolean result = defaultValue;
        String strValue = uri.getQueryParameter(key);
        if (strValue != null) {
            result = Boolean.parseBoolean(strValue);
        }

        return result;
    }

    private long getQueryStringLong(Uri uri, String key, long defaultValue) {
        long result = defaultValue;
        String strValue = uri.getQueryParameter(key);
        if (strValue != null) {
            try {
                result = Long.parseLong(strValue);
            } catch (NumberFormatException nfe) {

            }
        }

        return result;
    }

    private int getQueryStringInt(Uri uri, String key, int defaultValue) {
        int result = defaultValue;
        String strValue = uri.getQueryParameter(key);
        if (strValue != null) {
            try {
                result = Integer.parseInt(strValue);
            } catch (NumberFormatException nfe) {

            }
        }

        return result;
    }
}

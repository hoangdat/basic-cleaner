
package platform.com.mfluent.asp.util;

import java.util.HashMap;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.support.v4.content.LocalBroadcastManager;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;

import platform.com.mfluent.asp.datamodel.DataModelSLPF;
import platform.com.mfluent.asp.datamodel.DeviceSLPF;
import platform.com.mfluent.asp.framework.IASPApplication2;
import platform.com.mfluent.asp.framework.ServiceLocatorSLPF;

import com.mfluent.log.Log;
import com.samsung.android.sdk.slinkcloud.CloudGatewayNetworkMode;

public class BroadcastReceiverManager {

    public static final String BROADCAST_NETWORK_STATUS_CHANGED = BroadcastReceiverManager.class.getName() + "_BROADCAST_NETWORK_STATUS_CHANGED";

    @SuppressWarnings("unused")
    private final HashMap<String, IntentFilter> mIntentFilterMap = new HashMap<String, IntentFilter>();
    private CloudGatewayNetworkMode mNetworkStatus;

    private static TelephonyManager mTelephonyManager;

    private static BroadcastReceiverManager sInstance = null;

    private BroadcastReceiverManager(Context context) {
        super();
        this.mNetworkStatus = DeviceUtilSLPF.getNetworkType(context);
        //sendNetworkChange(mNetworkStatus);
    }

    public static synchronized BroadcastReceiverManager getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new BroadcastReceiverManager(context);
        }

        return sInstance;
    }

    private final BroadcastReceiver mEventReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.i(this, "mEventReceiver/onReceive() - action : " + intent.getAction());

            String action = intent.getAction();
            if (Intent.ACTION_SCREEN_OFF.equals(action)) {
                Intent keyIntent = new Intent("TO_APPLICATION_FROM_PLATFORM_API");
                keyIntent.putExtra("COMMAND", "KeyManager.setByPass.false");
                LocalBroadcastManager.getInstance(context).sendBroadcast(keyIntent);
            }
        }
    };

    public void updateNetworkStatus(Context context, CloudGatewayNetworkMode aspNetworkStatus) {
        if (aspNetworkStatus == mNetworkStatus) {
            Log.i(this, "updateNetworkStatus() - network status unchanged. Will suppress update.");
            return;
        }

        Log.i(this, "updateNetworkStatus() - new type=" + aspNetworkStatus + " old type=" + mNetworkStatus);
        mNetworkStatus = aspNetworkStatus;

        // force set local device network mode
        DataModelSLPF model = DataModelSLPF.getInstance();
        DeviceSLPF device = model.getLocalDevice();
        device.setDeviceNetworkMode(mNetworkStatus);
        model.updateDevice(device);

        // refresh my own peerlist
        LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent(BROADCAST_NETWORK_STATUS_CHANGED));
    }

    public void initTelephonyManager(Context context) {
        // PhoneStateListener 관련
        mTelephonyManager = (TelephonyManager) context.getSystemService(Activity.TELEPHONY_SERVICE);
        mTelephonyManager.listen(mPhoneStateListener, PhoneStateListener.LISTEN_DATA_CONNECTION_STATE);
    }

    // PhoneStateListener
    private final PhoneStateListener mPhoneStateListener = new PhoneStateListener() {

        @Override
        public void onDataConnectionStateChanged(int dataState, int networkType) {
            super.onDataConnectionStateChanged(dataState, networkType);
            CloudGatewayNetworkMode aspNetworkStatus = TypeManagerSLPF.checkMobileType(networkType);

            Log.d(this, "mPhoneStateListener/onDataConnectionStateChanged() - dataState:" + dataState + " networkType:" + networkType);

            IASPApplication2 context = ServiceLocatorSLPF.get(IASPApplication2.class);
            if (context == null) {
                Log.e(this, "mPhoneStateListener/onDataConnectionStateChanged() - context is null in mPhoneStateListener");
                return;
            }

            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetInfo = null;

            try {
                if (connectivityManager != null) {
                    activeNetInfo = connectivityManager.getActiveNetworkInfo();
                }
            } catch (Exception exc) {
                activeNetInfo = null;
                Log.e(this, "mPhoneStateListener/onDataConnectionStateChanged() - Exception : ConnectivityManager throws exception " + exc);
            }

            if (dataState == TelephonyManager.DATA_CONNECTED) {

                if (activeNetInfo != null && activeNetInfo.getType() == ConnectivityManager.TYPE_MOBILE) {
                    updateNetworkStatus(context, aspNetworkStatus);
                }
            }
        }
    };

    public IntentFilter getNetworkIntentFilter() {
        IntentFilter networkIntentFilter = new IntentFilter();
        networkIntentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        networkIntentFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
        networkIntentFilter.addAction(Intent.ACTION_SCREEN_OFF);
        return networkIntentFilter;
    }

    public BroadcastReceiver getBroadcastReceiver() {

        Log.i(this, "getBroadcastReceiver() - " + mEventReceiver.toString());
        return mEventReceiver;
    }

    public CloudGatewayNetworkMode getNetworkStatus() {
        return mNetworkStatus;
    }
}

package platform.com.mfluent.asp.util;

/**
 * Created by sec on 2016-01-08.
 */
public abstract class Predicate {

    abstract public boolean evaluate(Object arg);
}

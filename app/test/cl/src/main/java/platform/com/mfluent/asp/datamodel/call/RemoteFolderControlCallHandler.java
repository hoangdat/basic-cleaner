package platform.com.mfluent.asp.datamodel.call;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Base64;

import com.mfluent.asp.cloudstorage.api.sync.CloudStorageSync;
import com.mfluent.asp.common.datamodel.ASPMediaStore;
import com.mfluent.asp.common.util.CloudStorageError;
import com.mfluent.log.Log;
import com.samsung.android.sdk.slinkcloud.CloudGatewayDeviceTransportType;
import com.samsung.android.sdk.slinkcloud.CloudGatewayFileBrowserUtils;
import com.samsung.android.sdk.slinkcloud.CloudGatewayMediaStore;
import com.samsung.android.sdk.slinkcloud.CloudGatewayMediaStore.CallMethods.MRRControl;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import platform.com.mfluent.asp.datamodel.DataModelSLPF;
import platform.com.mfluent.asp.datamodel.DeviceSLPF;
import platform.com.mfluent.asp.datamodel.filebrowser.CachedFileBrowser;
import platform.com.mfluent.asp.framework.IASPApplication2;
import platform.com.mfluent.asp.util.DeviceUtilSLPF;
import platform.com.mfluent.asp.util.NetworkUtilSLPF;
import platform.com.samsung.android.slinkcloud.NetResourceCacheManager;

public class RemoteFolderControlCallHandler implements CallHandler {
    public boolean removeLocalFile(String sourceMediaID) {
        boolean bSuccess = false;
        String decodedPath;
        try {
            decodedPath = new String(Base64.decode(sourceMediaID, Base64.NO_WRAP | Base64.URL_SAFE));
        } catch (Exception e) {
            decodedPath = sourceMediaID;
        }

        if(decodedPath != null && !decodedPath.startsWith("/")){
            decodedPath = sourceMediaID;
        }

        File fDelete = new File(decodedPath);
        if ((fDelete != null) && fDelete.exists()) {
            bSuccess = fDelete.delete();
            if (!bSuccess) {
                Log.e(this, "removeLocalFile()::file delete failed: local file id = " + sourceMediaID);
            }
        }
        return bSuccess;
    }

    public boolean renameLocalFile(String sourceMediaID, String strNewName) {
        boolean bSuccess = false;
        String decodedPath;
        try {
            decodedPath = new String(Base64.decode(sourceMediaID, Base64.NO_WRAP | Base64.URL_SAFE));
        } catch (Exception e) {
            decodedPath = sourceMediaID;
        }

        File fRename = new File(decodedPath);
        if ((fRename != null) && fRename.exists()) {
            File newPath = new File(fRename.getParent() + "/" + strNewName);
            bSuccess = fRename.renameTo(newPath);
            if (!bSuccess) {
                Log.e(this, "renameLocalFile()::file rename failed: local file id = " + sourceMediaID);
            }
        }

        return bSuccess;
    }

    private Bundle callForLocalStorage(Bundle result, Context context, final int nCommand, final long deviceId, String parentDirectoryId, String sourceId, String newItem, DeviceSLPF device) {
        boolean bRet = false;
        String retStr = null;
        try {
            switch (nCommand) {
                case CloudGatewayFileBrowserUtils.REMOTE_FOLDER_MKDIR:
                    String newDirectoryName = newItem;
                    Log.i(this, "newDirectoryName=" + newDirectoryName + ",id=" + parentDirectoryId);
                    try {
                        String decodedPath = new String(Base64.decode(parentDirectoryId, Base64.NO_WRAP | Base64.URL_SAFE));
                        File newFile = new File(decodedPath, newDirectoryName);
                        if (newFile.exists() == false) {
                            newFile.mkdir();
                        }
                        String tmpPath = newFile.getAbsolutePath();
                        retStr = new String(Base64.encode(tmpPath.getBytes(), Base64.NO_WRAP | Base64.URL_SAFE));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    NetResourceCacheManager.getInstance(context).deleteCache(CachedFileBrowser.URI_MATCHER + parentDirectoryId, null, (int) deviceId);
                    device.broadcastDeviceRefresh(0);
                    break;
                case CloudGatewayFileBrowserUtils.REMOTE_FOLDER_RMDIR:
                    if (sourceId != null && sourceId.isEmpty() == false) {
                        bRet = removeLocalFile(sourceId);
                    }
                    break;
                case CloudGatewayFileBrowserUtils.REMOTE_FOLDER_RENAME:
                    String newName = newItem;
                    bRet = renameLocalFile(sourceId, newName);
                    NetResourceCacheManager.getInstance(context).deleteCache(CachedFileBrowser.URI_MATCHER + parentDirectoryId, null, (int) deviceId);
                    device.broadcastDeviceRefresh(0);
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        result.putBoolean(CloudGatewayMediaStore.CallMethods.KEY_RESULT, bRet);
        result.putString(CloudGatewayMediaStore.CallMethods.KEY_RESULT_STR, retStr);
        return result;
    }

    private Bundle callForWebStorage(Bundle result, Context context, final int nCommand, final long deviceId, String parentDirectoryId, String sourceId, String newItem, DeviceSLPF device) {
        boolean bRet = false;
        String retStr = null;
        try {
            CloudStorageSync cloudStorageSync = device.getCloudStorageSync();
            if (cloudStorageSync != null) {
                switch (nCommand) {
                    case CloudGatewayFileBrowserUtils.REMOTE_FOLDER_MKDIR:
                        ////check first tmp file cache
                        String newDirectoryName = newItem;
                        retStr = CachedFileBrowser.deltaFileLayerFind((int) deviceId, newDirectoryName, parentDirectoryId);
                        if (retStr != null) {
                            Log.i(this, "cached cloud id is detected! name=" + newDirectoryName + ", parentid=" + parentDirectoryId);
                            bRet = true;
                        } else {
                            Log.i(this, "newDirectoryName=" + newDirectoryName + ", id=" + parentDirectoryId);
                            retStr = cloudStorageSync.createDirectory(parentDirectoryId, newDirectoryName); //jsublee_150730
                            if (retStr != null) {
                                Log.i(this, "returned FileID=" + retStr);
                                bRet = true;
                                CachedFileBrowser.deltaFileLayerAdd(device, context, newDirectoryName, parentDirectoryId, retStr, true);
                            }
                        }
                        break;
                    case CloudGatewayFileBrowserUtils.REMOTE_FOLDER_RMDIR:
                        bRet = cloudStorageSync.deleteFile(sourceId, sourceId);
                        if (bRet) {
                            CachedFileBrowser.deltaFileLayerRemove(device.getId(), sourceId);
                        }
                        NetResourceCacheManager.getInstance(context).deleteCache(CachedFileBrowser.URI_MATCHER + CloudGatewayMediaStore.Trash.Trash_ID, null, device.getId());
                        break;
                    case CloudGatewayFileBrowserUtils.REMOTE_FOLDER_RENAME:
                        bRet = cloudStorageSync.renameFile(parentDirectoryId, sourceId, newItem);
                        if (bRet) {
                            CachedFileBrowser.deltaFileLayerRename(device, context, parentDirectoryId, sourceId, newItem, false);
                        }
                        break;
                    case CloudGatewayFileBrowserUtils.REMOTE_FOLDER_MOVE:
                        bRet = cloudStorageSync.moveFile(parentDirectoryId, sourceId, newItem);
                        if (bRet) {
                            NetResourceCacheManager.getInstance(context).deleteCache(CachedFileBrowser.URI_MATCHER + parentDirectoryId, null, (int) deviceId);
                        }
                        break;
                    case CloudGatewayFileBrowserUtils.REMOTE_FOLDER_COPY:
                        bRet = cloudStorageSync.copyFile(parentDirectoryId, sourceId, newItem, null);
                        if (bRet) {
                            NetResourceCacheManager.getInstance(context).deleteCache(CachedFileBrowser.URI_MATCHER + parentDirectoryId, null, (int) deviceId);
                        }
                        break;
                    case CloudGatewayFileBrowserUtils.REMOTE_FOLDER_SYNC:
                        device.broadcastDeviceRefresh(0);
                        bRet = true;
                        break;
                    case CloudGatewayFileBrowserUtils.REMOTE_TRASH_EMPTY:
                        ArrayList<String> sourceIds = new ArrayList<String>();
                        if (DeviceUtilSLPF.SAMSUNGDRIVE_WEBSTORAGE_NAME.equalsIgnoreCase(device.getWebStorageType())) {
                            SQLiteDatabase db = cloudStorageSync.getReadableCloudDatabase();
                            String selection = ASPMediaStore.TrashColumns.TRASHED + " = 1 and "
                                    + ASPMediaStore.TrashColumns.LIST_SHOWN + " = 1";
                            Cursor cursor = db.query(com.mfluent.cloud.samsungdrive.common.DatabaseHelper.FILE_TABLE,
                                    new String[]{ASPMediaStore.MediaColumns.SOURCE_MEDIA_ID}, selection, null, null, null, null);

                            if (cursor != null) {
                                cursor.moveToFirst();
                                while (!cursor.isAfterLast()) {
                                    String cloudId = cursor.getString(cursor.getColumnIndex(ASPMediaStore.MediaColumns.SOURCE_MEDIA_ID));
                                    if (cloudId != null) {
                                        sourceIds.add(cloudId);
                                    }
                                    cursor.moveToNext();
                                }
                                cursor.close();
                            }
                        }
                        String[] sourceIdArr = new String[sourceIds.size()];
                        sourceIdArr = sourceIds.toArray(sourceIdArr);
                        if (sourceIds != null && sourceIds.size() > 0) {
                            bRet = cloudStorageSync.deleteTrashBatch(sourceIdArr);
                            if (bRet) {
                                for (int i = 0; i < sourceIds.size(); i++) {
                                    Log.d(this, "deleteTrashBatch REMOTE_TRASH_EMPTY deltaFileLayerRemove file = " + sourceIds.get(i));
                                    CachedFileBrowser.deltaFileLayerRemoveTrash((int) deviceId, sourceIds.get(i), false);
                                }
                            }
                        }
                        NetResourceCacheManager.getInstance(context).deleteCache(CachedFileBrowser.URI_MATCHER + CloudGatewayMediaStore.Trash.Trash_ID, null, (int) deviceId);
                        break;
                    case CloudGatewayFileBrowserUtils.REMOTE_TRASH_DELETE:
                        bRet = cloudStorageSync.deleteTrash(sourceId);
                        if (bRet) {
                            CachedFileBrowser.deltaFileLayerRemoveTrash((int) deviceId, sourceId, false);
                        }
                        NetResourceCacheManager.getInstance(context).deleteCache(CachedFileBrowser.URI_MATCHER + CloudGatewayMediaStore.Trash.Trash_ID, null, (int) deviceId);
                        break;
                    case CloudGatewayFileBrowserUtils.REMOTE_TRASH_RESTORE:
                        String parentId = cloudStorageSync.restoreTrash(sourceId);
                        if (parentId != null) {
                            bRet = true;
                            CachedFileBrowser.deltaFileLayerRemoveTrash((int) deviceId, sourceId, true);
                            NetResourceCacheManager.getInstance(context).deleteCache(CachedFileBrowser.URI_MATCHER + CloudGatewayMediaStore.Trash.Trash_ID, null, (int) deviceId);
                            NetResourceCacheManager.getInstance(context).deleteCache(CachedFileBrowser.URI_MATCHER + parentId, null, (int) deviceId);
                        }
                        break;
                }
            }
        } catch (IOException ioe) {
            result.putInt(CloudGatewayMediaStore.CallMethods.KEY_RESULT_QUOTA_ERROR, CloudStorageError.getExceptionError(ioe));
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            result.putBoolean(CloudGatewayMediaStore.CallMethods.KEY_RESULT, bRet);
            result.putString(CloudGatewayMediaStore.CallMethods.KEY_RESULT_STR, retStr);
            return result;
        }
    }

    @Override
    public Bundle call(Context context, String method, String arg, Bundle extras) {
        Bundle result = new Bundle();

        final long deviceId = extras.getLong(MRRControl.INTENT_ARG_DEVICEID);
        final int nCommand = extras.getInt(MRRControl.INTENT_ARG_CMD);

        String parentDirectoryId = extras.getString(MRRControl.INTENT_ARG_PARENTDIRECTORYID);
        String sourceId = extras.getString(MRRControl.INTENT_ARG_SOURCEID);
        String newItem = extras.getString(MRRControl.INTENT_ARG_NEWITEM);

        DataModelSLPF dataModel = DataModelSLPF.getInstance();
        DeviceSLPF device = dataModel.getDeviceById((int) deviceId);

        result.putBoolean(CloudGatewayMediaStore.CallMethods.KEY_RESULT, false);
        if (device == null) {
            Log.i(this, "device is null for device id=" + deviceId + ",cmd=" + nCommand);
            return result;
        }

        if (!NetworkUtilSLPF.isNetworkAvailable() || IASPApplication2.checkNeedWifiOnlyBlock()) {
            Log.e(this, "Network is blocked");
            return result;
        }

        Log.i(this, "RemoteFolderControlCallHandler called cmd=" + nCommand);

        CloudGatewayDeviceTransportType transportType = device.getDeviceTransportType();
        switch (transportType) {
            case WEB_STORAGE:
                return callForWebStorage(result, context, nCommand, deviceId, parentDirectoryId, sourceId, newItem, device);
            default:
                return callForLocalStorage(result, context, nCommand, deviceId, parentDirectoryId, sourceId, newItem, device);
        }
    }
}

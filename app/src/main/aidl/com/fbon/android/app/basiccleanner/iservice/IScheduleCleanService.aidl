// IScheduleCleanService.aidl
package com.fbon.android.app.basiccleanner.iservice;

// Declare any non-default types here with import statements

interface IScheduleCleanService {
    boolean isServiceScanning();
    void stopScanning();
}

package com.fbon.android.app.basiccleaner.fragment;


import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.fbon.android.app.basiccleaner.R;
import com.fbon.android.app.basiccleaner.adapter.GroupItem;
import com.fbon.android.app.basiccleaner.adapter.StorageAdapterEx;
import com.fbon.android.app.basiccleaner.feature.CleanLdrManager;
import com.fbon.android.app.basiccleaner.feature.ClearListType;
import com.fbon.android.app.basiccleaner.feature.ClearMgr;
import com.fbon.android.app.basiccleaner.model.StorageInfoDataModel;
import com.fbon.android.app.basiccleaner.uicore.Event;
import com.fbon.android.app.basiccleaner.util.AppFeature;
import com.fbon.android.app.basiccleaner.util.Constant;
import com.fbon.android.app.basiccleaner.util.Log;
import com.fbon.android.app.basiccleaner.util.UiUtils;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class MemoryFragment extends ItemFragment implements StorageAdapterEx.OnItemCheckChangedListener, AdapterView.OnItemClickListener {
    private static final String TAG = "MemoryFragment";

    private static final int JUNK_TYPE = (!AppFeature.isSupportListMem()) ? CleanLdrManager.CACHE_JUNK_TYPE : CleanLdrManager.MEM_JUNK_TYPE;
    private static final int FLAG_SCAN_DONE = AppFeature.isSupportListMem() ? (Constant.FLAG_LOAD_APP_MEM) : (Constant.FLAG_LOAD_CACHE);

    private ArrayList<StorageInfoDataModel> mLstProcess;
    private View mViewMore;
    private ListView mLstViewMore;
    private TextView mTxtViewMore;
    private boolean mExpandMore = false;
    private StorageAdapterEx mAdapterMem;
    private GroupItem mGroupMem;

    private AtomicInteger mSizeUpdateRequestId = new AtomicInteger(0);
    private SizeMemUpdateHandler mSizeUpdateHandler;
    private final Object SIZE_UPDATE_LOCK = new Object();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSizeUpdateHandler = new SizeMemUpdateHandler(this);
    }

    @Override
    public int getFixNowEvent() {
        return Event.MEMORY_CLEAR;
    }

    @Override
    public String getUsedText() {
        return super.getUsedText();
    }

    @Override
    public String getAvailableText() {
        return super.getAvailableText();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        Log.d(TAG, "onViewCreated()");
        super.onViewCreated(view, savedInstanceState);
        mLstProcess = new ArrayList<>();
        mGroupMem = new GroupItem(null, mLstProcess, JUNK_TYPE);
        mAdapterMem = new StorageAdapterEx(getActivity(), R.layout.item_layout, Arrays.asList(mGroupMem));
        mAdapterMem.setAllSelected();
        mLstViewMore.setOnItemClickListener(this);
        mAdapterMem.registerItemCheckChange(this);
        mLstViewMore.setAdapter(mAdapterMem);
    }

    @Override
    protected int getRequestInfo() {
        return CleanLdrManager.MSG_START_LOAD_MEM_INFO;
    }

    @Override
    public boolean sendScanRequest() {
        Log.d(TAG, "sendScanRequest()");
        return CleanLdrManager.getInstance(getActivity()).startLoadMem(firstScanType(), this);
    }

    @Override
    public int getViewStubId() {
        return R.id.memory_stub;
    }

    @Override
    public void initViewFormStub(View stubView) {
        mViewMore = stubView.findViewById(R.id.memory_view_more);
        mViewMore.setOnClickListener(this);
        mTxtViewMore = (TextView) stubView.findViewById(R.id.txt_view_more);
        mLstViewMore = (ListView) stubView.findViewById(R.id.memory_more_list);
    }

    private void updateViewMore(boolean enable) {
        if (mViewMore != null) {
            mViewMore.setVisibility(enable ? View.VISIBLE : View.GONE);
        }
    }

    private void showListMore(boolean show) {
        Log.d(TAG, "showListMore() - show = " + show);
        mExpandMore = show;
        mTxtViewMore.setText(show? R.string.view_less : R.string.view_more);
        if (mLstViewMore != null) {
            if (show) {
                mLstViewMore.setVisibility(View.VISIBLE);
            } else {
                mLstViewMore.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void _onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.fix_now:
                if (mAdapterMem != null) {
                    HashMap<Integer, List<StorageInfoDataModel>> map = mAdapterMem.getSelectedList();
                    if (map != null) {
                        List<StorageInfoDataModel> lst = map.get(JUNK_TYPE);
                        if (lst != null) {
                            ClearListType clear = new ClearListType(JUNK_TYPE, new ArrayList<>(lst));
                            ArrayList<ClearListType> lstClear = new ArrayList<>();
                            lstClear.add(clear);
                            ClearMgr.getInstance(getActivity()).setLstClear(FLAG_SCAN_DONE, lstClear, false);
                        }
                    }
                }
                break;
            case R.id.memory_view_more:
                showListMore(!mExpandMore);
                break;
            default:
                break;
        }
        super._onClick(v);
    }

    @Override
    protected long getUsed(ArrayList<Long> usedTotal) {
        long ret = 0;
        if (usedTotal != null) {
            ret = usedTotal.get(2);
        }
        return ret;
    }

    @Override
    protected long getTotal(ArrayList<Long> usedTotal) {
        long ret = 0;
        if (usedTotal != null) {
            ret = usedTotal.get(3);
        }
        return ret;
    }

    @Override
    public void updateResultView(int scanType, long prevScore, boolean isPercent) {
        if (scanType == Constant.FLAG_NONE) {
            updateViewMore(false);
            mExpandMore = false;
        } else if (scanType == firstScanType()) {
            updateViewMore(true);
            mExpandMore = false;
        }
        super.updateResultView(scanType, prevScore, isPercent);
    }

    @Override
    protected void _onJunkLoaderProgress(int scanType, int junkType, int progress, StorageInfoDataModel model) {
        if (model == null) {
            return;
        }
        if (junkType == JUNK_TYPE) {
            mTxtProgress.setText(model.getName());
        }
    }

    @Override
    public int firstScanType() {
        return FLAG_SCAN_DONE;
    }

    @Override
    protected long getScanTotal() {
        long ret = UiUtils.getStoreSize(getActivity(), firstScanType(), JUNK_TYPE);
        if (mAdapterMem != null) {
            HashMap<Integer, Long> mapSize = mAdapterMem.getSelectedSizeGroup();
            if (mapSize != null) {
                Long objSz = mapSize.get(JUNK_TYPE);
                if (objSz != null) {
                    Log.d(TAG, "getScanTotal() - get selected");
                    ret = objSz.longValue();
                }
            }
        }
        return ret;
    }

    @Override
    protected void setAddInfo() {
        Resources res = getActivity().getResources();
        mAvailableAddInfo.setText(String.format(res.getString(R.string.available_mem),
                UiUtils.makeFileSizeString(getActivity(), getAvailable(mUsedTotal))));

        mUsedAddInfo.setText(String.format(res.getString(R.string.used_mem),
                UiUtils.makeFileSizeString(getActivity(), getUsed(mUsedTotal))));
    }

    private long getAvailable(ArrayList<Long> usedTotal) {
        long ret = 0;
        if (usedTotal != null) {
            ret = usedTotal.get(1);
        }
        return ret;
    }

    @Override
    public void onItemCheckChanged() {
        clearSizeUpdateEvent();
        Message msg = mSizeUpdateHandler.obtainMessage(Constant.MSG_START_CALCULATE_SELECTED_FILE_SIZE);
        msg.arg1 = mSizeUpdateRequestId.incrementAndGet();
        mSizeUpdateHandler.sendMessage(msg);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Log.d(TAG, "onItemClick - pos = " + position);
        mAdapterMem.setSelected(position);
    }

    private void clearSizeUpdateEvent() {
        mSizeUpdateHandler.removeMessages(Constant.MSG_START_CALCULATE_SELECTED_FILE_SIZE);
        mSizeUpdateHandler.removeMessages(Constant.MSG_DISPLAY_RESULT_SIZE);
    }

    private static class SizeMemUpdateHandler extends Handler {
        private WeakReference<MemoryFragment> mParent;

        public SizeMemUpdateHandler(MemoryFragment fragment) {
            mParent = new WeakReference<>(fragment);
        }

        @Override
        public void handleMessage(Message msg) {
            MemoryFragment fragment = mParent.get();
            if (fragment != null) {
                switch (msg.what) {
                    case Constant.MSG_START_CALCULATE_SELECTED_FILE_SIZE:
                        Log.d(TAG, "handleMessage() - MSG_START_CALCULATE_SELECTED_FILE_SIZE");
                        new SizeCalculateThread(fragment, msg.arg1).start();
                        break;
                    case Constant.MSG_DISPLAY_RESULT_SIZE:
                        Log.d(TAG, "handleMessage() - MSG_DISPLAY_RESULT_SIZE");
                        fragment.setSelectedFileSize((HashMap<Integer, Long>) msg.obj);
                        break;
                }
            }
        }
    }

    private static class SizeCalculateThread extends Thread {
        private WeakReference<MemoryFragment> mParent;
        private int mRequestId;

        public SizeCalculateThread(MemoryFragment parent, int requestId) {
            mParent = new WeakReference<MemoryFragment>(parent);
            mRequestId = requestId;
        }

        @Override
        public void run() {
            HashMap<Integer, Long> mapSize = null;
            if (keepGoing()) {
                MemoryFragment fragment = mParent.get();
                if (fragment != null) {
                    mapSize = fragment.mAdapterMem.getSelectedSizeGroup();
                    synchronized (fragment.SIZE_UPDATE_LOCK) {
                        Message msg = fragment.mSizeUpdateHandler.obtainMessage(Constant.MSG_DISPLAY_RESULT_SIZE);
                        msg.obj = mapSize;
                        Log.d(TAG, "run() sendMsg = " + mapSize);
                        fragment.mSizeUpdateHandler.sendMessage(msg);
                    }
                }
            }
        }

        private boolean keepGoing() {
            boolean ret = false;
            MemoryFragment fragment = mParent.get();
            if (fragment != null) {
                ret = fragment.mSizeUpdateRequestId.get() == mRequestId;
            }
            return ret;
        }
    }

    private void setSelectedFileSize(HashMap<Integer, Long> mapSize) {
        if (mapSize ==  null) {
            return;
        }
        boolean keepGoing = false;

        if (CleanLdrManager.getInstance(getActivity()).getScanType() == firstScanType()) {
            Log.d(TAG, "setSelectedFileSize() - state = " + CleanLdrManager.getInstance(getActivity()).getState());
            if (CleanLdrManager.getInstance(getActivity()).getState() == CleanLdrManager.LOADER_STATE_SCAN_DONE) {
                keepGoing = true;
            }
        }

        if (!keepGoing)
            return;

        Long oSz = mapSize.get(JUNK_TYPE);
        Log.d(TAG, "setSelectedFileSize() - oSz = " + oSz);
        if (oSz != null) {
            String sz = UiUtils.makeFileSizeString(getActivity(), oSz);
            mTotalSize = oSz;
            updateFixNowButton(true, R.string.clean_now, sz);
        }
    }

    @Override
    protected void updateData(int scanType) {
        if (scanType == firstScanType()) {
            mLstProcess = CleanLdrManager.getInstance(getActivity()).getStore(firstScanType(), JUNK_TYPE);
            Log.d(TAG, "showListMore() - item = " + mLstProcess.size());
            mGroupMem.setChilds(mLstProcess);
            mAdapterMem.notifyDataSetChanged();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}

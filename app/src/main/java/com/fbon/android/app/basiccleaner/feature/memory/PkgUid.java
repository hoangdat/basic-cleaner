package com.fbon.android.app.basiccleaner.feature.memory;


import java.util.Comparator;

public class PkgUid implements Comparator<PkgUid>{
    private static final String TOKEN = ",";

    private String mPkgName;
    private int mUid;
    private String mPkgNameUid;

    public PkgUid(String pkgName, int uid) {
        mPkgName = pkgName;
        mUid = uid;
        mPkgNameUid = pkgName + TOKEN + uid;
    }

    public String getPkgName() {
        return mPkgName;
    }

    public int getUid() {
        return mUid;
    }

    private String getPkgNameUid() {
        return mPkgNameUid;
    }


    @Override
    public int compare(PkgUid o1, PkgUid o2) {
        return o1.getPkgNameUid().compareToIgnoreCase(o2.getPkgNameUid());
    }

    @Override
    public boolean equals(Object obj) {
        return (obj instanceof PkgUid) && mPkgNameUid.equals(((PkgUid) obj).getPkgNameUid());
    }

    @Override
    public int hashCode() {
        return mPkgNameUid.hashCode();
    }
}

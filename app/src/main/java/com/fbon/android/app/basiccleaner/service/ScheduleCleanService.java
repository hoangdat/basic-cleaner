package com.fbon.android.app.basiccleaner.service;

import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;

import com.fbon.android.app.basiccleaner.util.Log;
import com.fbon.android.app.basiccleanner.iservice.IScheduleCleanService;

import java.lang.ref.WeakReference;
import java.util.Enumeration;
import java.util.Hashtable;



public class ScheduleCleanService extends Service {
    private static final String TAG = "ScheduleSrv";
    private IScheduleCleanService.Stub mBinder = null;

    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "onBind");
        return mBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.d(TAG, "onUnbind");
        if (Helper.connectionCount() == 0) {
            Log.i(TAG, "onUnbind connectionCount is zero");
            stopSelf();
        }

        return super.onUnbind(intent);
    }

    @Override
    public void onDestroy() {

        super.onDestroy();
    }

    private static class IScheduleCleanServiceStub extends IScheduleCleanService.Stub {
        private final WeakReference<ScheduleCleanService> mScheduleService;

        public IScheduleCleanServiceStub(ScheduleCleanService service) {
            mScheduleService = new WeakReference<ScheduleCleanService>(service);
        }

        @Override
        public boolean isServiceScanning() throws RemoteException {
            return false;
        }

        @Override
        public void stopScanning() throws RemoteException {

        }
    }

    public static class Helper {
        private static final String TAG = "Helper";
        private static Hashtable<Context, ServiceBinder> mConnectionMap = new Hashtable<>();

        public static boolean bindToService(Context context, ServiceConnection callBack) {
            Log.d(TAG, "bindToService()");
            if (context.startService(new Intent(context, ScheduleCleanService.class)) != null) {
                ServiceBinder sb = new ServiceBinder(callBack);

                mConnectionMap.put(context, sb);
                Log.v(TAG, "bindToService - connection size : " + mConnectionMap.size());
                return context.bindService(new Intent().setClass(context, ScheduleCleanService.class), sb, 0);
            } else {
                return false;
            }
        }

        public static void unbindFromService(Context context) {
            Log.d(TAG, "unbindFromService()");
            ServiceBinder sb = mConnectionMap.get(context);
            if (null == sb) {
                return;
            }
            context.unbindService(sb);
        }
        public static int connectionCount() {
            if (mConnectionMap == null) {
                Log.v(TAG, "connectionCount - connectionMap is null");
                return 0;
            }
            Log.v(TAG, "connectionCount - connectionMap size : " + mConnectionMap.size());
            return mConnectionMap.size();
        }

        public static boolean isConnectedContext(Context context) {
            Enumeration enumeration = mConnectionMap.keys();
            Context item;
            while (enumeration.hasMoreElements()) {
                item = (Context) enumeration.nextElement();
                if (item.getClass().getSimpleName().equals(context.getClass().getSimpleName())) {
                    return true;
                }
            }

            return false;
        }


        private static class ServiceBinder implements ServiceConnection {
            ServiceConnection mServiceConnection;

            ServiceBinder(ServiceConnection serviceConnection) {
                mServiceConnection = serviceConnection;
            }

            @Override
            public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
                if (null != mServiceConnection) {
                    mServiceConnection.onServiceConnected(componentName, iBinder);
                }
            }

            @Override
            public void onServiceDisconnected(ComponentName className) {
                if (null != mServiceConnection) {
                    mServiceConnection.onServiceDisconnected(className);
                }
            }
        }
    }
}

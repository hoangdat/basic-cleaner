package com.fbon.android.app.basiccleaner.feature.memory;


import android.app.ActivityManager;
import android.content.Context;
import android.os.Debug;

import java.util.ArrayList;

public class MemoryWorker {
    private MemoryWorker(Context context) {
        mAm = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        mMemInfo = new ActivityManager.MemoryInfo();
    }

    private ActivityManager mAm;
    private ActivityManager.MemoryInfo mMemInfo;

    private long mPrevTotalMem;
    private long mPrevAvailMem;
    private boolean mPrevLowMemory;

    private static MemoryWorker mInstance;
    public static MemoryWorker getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new MemoryWorker(context);
        }
        return mInstance;
    }
    public long getMemOfPids(ArrayList<Integer> pidArr) {
        long pss = 0;
        if (pidArr != null && pidArr.size() > 0) {
            int pids[] = new int[pidArr.size()];
            for (int i = 0; i < pidArr.size(); i++) {
                pids[i] = pidArr.get(i);
            }
            Debug.MemoryInfo[] debugMemInfos = mAm.getProcessMemoryInfo(pids);
            for (Debug.MemoryInfo debugMemInfo : debugMemInfos) {
                pss += debugMemInfo.getTotalPss();
            }
        }
        return pss;
    }

    public boolean updateMemInfo() {
        mAm.getMemoryInfo(mMemInfo);
        boolean bChanged = (mMemInfo.availMem != mPrevAvailMem //
                || mMemInfo.totalMem != mPrevTotalMem || mMemInfo.lowMemory != mPrevLowMemory);
        mPrevAvailMem = mMemInfo.availMem;
        mPrevTotalMem = mMemInfo.totalMem;
        mPrevLowMemory = mMemInfo.lowMemory;
        return bChanged;
    }

    public long getAvailMem() {
        return mMemInfo.availMem;
    }

    public long getTotalMemory() {
        return mMemInfo.totalMem;
    }
}

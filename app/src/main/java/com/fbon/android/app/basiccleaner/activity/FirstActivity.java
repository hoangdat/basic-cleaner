package com.fbon.android.app.basiccleaner.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.fbon.android.app.basiccleaner.R;
import com.fbon.android.app.basiccleaner.actionbar.MainActionBar;
import com.fbon.android.app.basiccleaner.dialog.PermissionDialog;
import com.fbon.android.app.basiccleaner.fragment.FragmentController;
import com.fbon.android.app.basiccleaner.uicore.BasicCleanerObservable;
import com.fbon.android.app.basiccleaner.uicore.Event;
import com.fbon.android.app.basiccleaner.util.Constant;
import com.fbon.android.app.basiccleaner.util.Log;
import com.fbon.android.app.basiccleaner.util.PreferenceUtils;
import com.fbon.android.app.basiccleaner.util.ShortcutUtils;
import com.startapp.android.publish.adsCommon.StartAppAd;
import com.startapp.android.publish.adsCommon.StartAppSDK;

import java.util.Observable;
import java.util.Observer;



public class FirstActivity extends Activity implements FragmentController.OnSceneChangeListener, Observer{
    private StartAppAd startAppAd = new StartAppAd(this);
    private static final String TAG = "FirstActivity";
    private FragmentController mFragmentController;
    private MainActionBar mActionBar = null;
    private int mScene = FragmentController.Scene.Overall;
    private BasicCleanerObservable mObservable = null;
    private boolean mAlwaysDenied;
    long cur = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate()");
        StartAppSDK.init(this, Constant.AdvertisementConstant.AppId, false);
        StartAppAd.disableSplash();
        setContentView(R.layout.activity_first);
        cur = System.currentTimeMillis();

        mFragmentController = FragmentController.getInstance();
        mFragmentController.registerListener(this);
        mFragmentController.setContext(this);

        mActionBar = new MainActionBar(this, mScene);

        mObservable = BasicCleanerObservable.getInstance();
        mObservable.addObserver(this);
        handleStart();
    }

    private void handleStart() {
        if (isPermissionValid()) {
            Log.i(TAG, "handleStart() - permission valid");
            handleScene();
        } else {
            Log.i(TAG, "handleStart() - time to request permission = " + (System.currentTimeMillis() - cur));
            String[] permissions = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE};
            if (mAlwaysDenied) {
                runApplicationSettings(permissions);
            } else {
                ActivityCompat.requestPermissions(this, permissions, Constant.PERMISSION_REQUEST_CODE);
            }
        }
    }

    private void runApplicationSettings(String[] permission) {
        PermissionDialog dialog = new PermissionDialog();
        dialog.setArguments(permission);
        dialog.show(getFragmentManager(), "Permission_" + permission);
    }

    private boolean isPermissionValid() {
        boolean ret = true;
        mAlwaysDenied = false;
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (!PreferenceUtils.isFirstEntry(this) && !ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                Log.d(TAG, "isPermissionValid() ] should show");
                mAlwaysDenied = true;
            }
            ret = false;
        }
        PreferenceUtils.setFirstEntry(this, false);
        Log.d(TAG, "isPermissionValid() ] return " + ret);
        return ret;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        boolean granted = true;
        Log.i(TAG, "onRequestPermissionsResult() ] requestCode = " + requestCode);
        if (requestCode == Constant.PERMISSION_REQUEST_CODE) {
            if (permissions.length == 0 || grantResults.length == 0) {
                Log.d(TAG, "onRequestPermissionsResult() ] Abnormal case.");
                finishAffinity();
                return;
            }

            for (int grantResult : grantResults) {
                if (grantResult == PackageManager.PERMISSION_DENIED) {
                    granted = false;
                    break;
                }
            }
            if (granted) {
                handleScene();
            } else {
                Log.d(TAG, "onRequestPermissionsResult() ] The permission is denied. So finish");
                finish();
            }

        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        Log.i(TAG, "onPrepareOptionsMenu");
        if (mActionBar != null) {
            mActionBar.onPrepareOptionMenu(menu, mScene, this);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        Log.i(TAG, "onOptionsItemSelected: " + id);
        switch (id) {
            case android.R.id.home:
                if (mFragmentController == null || !mFragmentController.onBackPress()) {
                    super.onBackPressed();
                }
                break;
            default:
                if (mActionBar != null) {
                    mActionBar.onOptionsItemSelected(item, this);
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSceneChange(int scene) {
        mScene = scene;
    }

    public void changeScene() {
        changeScene(mScene);
    }

    public void handleScene() {
        Intent intent = getIntent();
        String action = intent.getAction();
        if (action == Constant.Action.ACTION_SCAN_OVERALL) {
            mScene = intent.getIntExtra(Constant.Extra.EXTRA_LAUNCH_SCENE, FragmentController.Scene.Overall);
            changeSceneClearPrev(mScene);
        } else {
            changeScene(mScene);
        }
        if (!PreferenceUtils.getInstalledShortcut(this)) {
            addShortcutToHome();
            Toast.makeText(this, R.string.install_shortcut_message, Toast.LENGTH_LONG).show();
        }
    }

    public void changeSceneClearPrev(int scene) {
        if (mFragmentController != null) {
            mFragmentController.changeSceneWithClearPrev(scene);
        }
    }

    public void changeScene(int scene) {
        if (mFragmentController != null) {
            mFragmentController.changeScene(scene);
        }
    }

    public void changeSceneWithoutBackStack(int scene) {
        if (mFragmentController != null) {
            mFragmentController.changeSceneWithoutBackStack(scene);
        }
    }

    @Override
    public void update(Observable o, Object arg) {
        int event = (int) arg;
        Log.i(TAG, "update() event = " + event);
        switch (event) {
            case Event.MEMORY_GO:
                changeScene(FragmentController.Scene.Memory);
                break;
            case Event.MEMORY_CLEAR:
                changeSceneWithoutBackStack(FragmentController.Scene.Clean_memory);
                break;
            case Event.STORAGE_GO:
                changeScene(FragmentController.Scene.Storage);
                break;
            case Event.STORAGE_CLEAR:
                changeSceneWithoutBackStack(FragmentController.Scene.Clean_storage);
                break;
            case Event.STORAGE_MORE_SPACE:
                changeScene(FragmentController.Scene.More_space);
                break;
            case Event.STORAGE_CLEAR_MORE_SPACE:
                changeSceneWithoutBackStack(FragmentController.Scene.Clean_more_space);
                break;
            case Event.OVERALL_CLEAN:
                changeScene(FragmentController.Scene.CleanAll);
                break;
            default:
                break;
        }
    }

    private void addShortcutToHome() {
        Intent addShortcutIntent = ShortcutUtils.getIntentForShortcut(this);
        sendBroadcast(addShortcutIntent);
        PreferenceUtils.setShorcutInstalled(this, true);
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        Log.d(TAG, "onPause()");
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        Log.i(TAG, "onBackPressed");
        if (isDestroyed() || isFinishing()) {
            return;
        }
        if (mScene == FragmentController.Scene.Clean_quick) {
            int count = PreferenceUtils.getCountClean(this);
            Log.d(TAG, "onBackPressed count = " + count);
            if (count >= Constant.AdvertisementConstant.MAX_COUNT_CLEAN) {
                startAppAd.showAd();
                PreferenceUtils.setCountClean(this, 0);
                return;
            }
        }
        if (mFragmentController != null && mFragmentController.onBackPress()) {
            return;
        }
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        Log.i(TAG, "onDestroy()");
        if (mActionBar != null) {
            mActionBar.onDestroy();
            mActionBar = null;
        }

        if (mFragmentController != null) {
            mFragmentController.unregisterSceneChangeListener(this);
        }

        if (mObservable != null) {
            mObservable.deleteObserver(this);
        }
        super.onDestroy();
    }
}

package com.fbon.android.app.basiccleaner.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fbon.android.app.basiccleaner.R;
import com.fbon.android.app.basiccleaner.feature.CleanLdrManager;
import com.fbon.android.app.basiccleaner.util.Constant;
import com.fbon.android.app.basiccleaner.util.Log;
import com.fbon.android.app.basiccleaner.util.UiUtils;

import java.util.ArrayList;


public class ItemFragment extends BasicCleanFragment implements View.OnClickListener, CleanLdrManager.OnInfoListener{
    private static final String TAG = "ItemFrg";
    protected TextView mTxtProgress;
    protected ArrayList<Long> mUsedTotal;
    protected TextView mUsedAddInfo, mAvailableAddInfo;
    protected LinearLayout mLinearAddInfo;
    protected final static int FLAG_WAIT_INFO           = 0x01;
    protected final static int FLAG_DONE_INFO           = 0x10;
    protected int mFlagState = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutId(), container, false);
        initCommonView(view);
        initView(view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViewFormStub(ensureDetailsView(view));
    }

    @Override
    public void scan() {
        Log.d(TAG, "scan");
        mFlagState = 0;
        mTotalSize = 0;
        mUsedTotal = new ArrayList<>();
        updateResultView(Constant.FLAG_NONE, 0, false);
        if (!sendScanRequest()) {
            mFlagState |= FLAG_WAIT_INFO;
            CleanLdrManager.getInstance(getActivity()).startInfo(this, getRequestInfo());
        }
    }

    protected int getRequestInfo() {
        return 0;
    }

    @Override
    public int getLayoutId() {
        return R.layout.item_fragment;
    }

    @Override
    public void initView(View view) {
        mBtnFixNow = (Button) view.findViewById(R.id.fix_now);
        mBtnFixNow.setOnClickListener(this);

        mLinearAddInfo = (LinearLayout) view.findViewById(R.id.add_on_info_container);
        mUsedAddInfo = (TextView) view.findViewById(R.id.add_info_used);
        mAvailableAddInfo = (TextView) view.findViewById(R.id.add_info_available);

        mTxtProgress = (TextView) view.findViewById(R.id.txt_progress_scan);
    }

    public String getAvailableText() {
        return null;
    }

    public String getUsedText() {
        return null;
    }

    public int getViewStubId() {
        return -1;
    }

    public View ensureDetailsView(View parentView) {
        View ret;
        ViewStub stub = null;
        if (getViewStubId() > 0) {
            stub = (ViewStub) parentView.findViewById(getViewStubId());
        }

        if (stub != null) {
            ret = stub.inflate();
        } else {
            ret = parentView;
        }

        return ret;
    }

    public void initViewFormStub(View stubView) {
    }

    @Override
    public void updateFixNowButton(boolean isEnable, int resId, String addInfo) {
        if (isEnable && mTotalSize == 0) {
            isEnable = false;
        }
        super.updateFixNowButton(isEnable, resId, addInfo);
    }

    @Override
    public void updateResultView(int scanType, long prevScore, boolean isPercent) {
        Log.d(TAG, "updateResultView() - state = " + scanType);
        if (scanType == Constant.FLAG_NONE) {
            mTxtUsed.setVisibility(View.GONE);
            mTxtTotal.setVisibility(View.GONE);
            mTxtProgress.setText(R.string.checking);
            updateFixNowButton(false, R.string.clean_now, null);
            setScanningEnable(true, 0);
            mAvailableAddInfo.setVisibility(View.GONE);
            mUsedAddInfo.setVisibility(View.GONE);
            mLinearAddInfo.setVisibility(View.GONE);
        } else if (scanType == firstScanType()) {
            long used = getUsed(mUsedTotal);
            long total = getTotal(mUsedTotal);
            mTxtUsed.setVisibility(View.VISIBLE);
            mTxtTotal.setVisibility(View.VISIBLE);
            mTxtUsed.setText(UiUtils.makeFileSizeString(getActivity(), used));
            mTxtTotal.setText(UiUtils.makeFileSizeString(getActivity(), total));

            setAddInfo();
            animateAddInfo();

            double ratio = used *1f / total;
            setScanningEnable(false, (int)(ratio * 100));
            String totalSize = null;
            mTotalSize = getScanTotal();
            if (mTotalSize != 0) {
                totalSize = UiUtils.makeFileSizeString(getActivity(), mTotalSize);
            }
            updateFixNowButton(true, R.string.clean_now, totalSize);
            mTxtProgress.setText(null);
        }
    }

    protected long getUsed(ArrayList<Long> usedTotal) {
        return 0;
    }

    protected long getTotal(ArrayList<Long> usedTotal) {
        return 0;
    }

    protected long getScanTotal() {
        return 0;
    }

    @Override
    public void onInfo(int scanType, int infoType, ArrayList<Long> usedTotal) {
        Log.d(TAG, "onInfo() - scanType = " + scanType + "-inf= " + infoType);
        if (scanType == getScanType() && infoType == getRequestInfo()) {
            mUsedTotal = usedTotal;
            mFlagState |= FLAG_DONE_INFO;
            Log.d(TAG, "onInfo() - scanType = " + scanType + "-inf= " + infoType + "-fl = " + mFlagState);
        }

        if (mFlagState == (FLAG_DONE_INFO | FLAG_WAIT_INFO)) {
            int scanTypeTmp = firstScanType();
            updateData(scanTypeTmp);
            updateResultView(scanTypeTmp, 0, false);
        }
    }

    @Override
    protected void _onScanFinished(int scanType) {
        Log.d(TAG, "_onScanFinished type = " + scanType);
        if (scanType == firstScanType()) {
            mFlagState |= FLAG_WAIT_INFO;
            CleanLdrManager.getInstance(getActivity()).startInfo(this, getRequestInfo());
        }
    }

    protected void animateAddInfo() {
        Animation slideDown = AnimationUtils.makeInAnimation(getActivity(), true);
        slideDown.setDuration(1600);
        mLinearAddInfo.setVisibility(View.VISIBLE);
        mAvailableAddInfo.setVisibility(View.VISIBLE);
        mUsedAddInfo.setVisibility(View.VISIBLE);
        mLinearAddInfo.startAnimation(slideDown);
        mAvailableAddInfo.startAnimation(slideDown);
        mUsedAddInfo.startAnimation(slideDown);
    }

    protected void setAddInfo() {
    }

    protected void updateData(int scanType) {
    }
}

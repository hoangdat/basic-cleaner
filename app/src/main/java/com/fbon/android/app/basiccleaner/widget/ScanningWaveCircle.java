package com.fbon.android.app.basiccleaner.widget;


import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.PathInterpolator;

import com.fbon.android.app.basiccleaner.R;
import com.fbon.android.app.basiccleaner.util.Constant;

public class ScanningWaveCircle extends View{
    private static final int CIRCLE_SCORE = 360;
    private static final float CEIL_THRESHOLD = 0.9f;
    private static final float FLOOR_THRESHOLD = 0.1f;
    private Paint mInnerPaint, mProgressPaint;
    private Paint mPaint;
    private RectF mArcElement;
    private int mThickness;
    private int mInnerColor;
    private ValueAnimator mInnerAnimation;
    private float mValue;
    private int mState = 0;
    private int mStartProgress;
    private float mCondition = 0.98f;

    public ScanningWaveCircle(Context context) {
        super(context);
        initView(context);
    }

    public ScanningWaveCircle(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public ScanningWaveCircle(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private void initView(Context context) {
        Resources resources = context.getResources();
        mThickness = resources.getDimensionPixelSize(R.dimen.circle_ring_thickness);

        mInnerColor = resources.getColor(R.color.white_little_little);
        mInnerPaint = new Paint();
        mInnerPaint.setStyle(Paint.Style.STROKE);
        mInnerPaint.setAntiAlias(true);
        mInnerPaint.setStrokeWidth(mThickness);
        mInnerPaint.setColor(mInnerColor);

        mPaint = new Paint();
        mPaint.set(mInnerPaint);

        mProgressPaint = new Paint();
        mProgressPaint.set(mInnerPaint);
        mProgressPaint.setColor(getResources().getColor(R.color.white));
//        mInnerPaint.setShader(new LinearGradient())
        mArcElement = new RectF();

        mStartProgress = -90;

        setUpAnimation();
    }

    private void setUpAnimation() {
        mInnerAnimation = ValueAnimator.ofFloat(0f, 1f);
//        mInnerAnimation.setInterpolator(new PathInterpolator(0.33f, 0f, 0.40f, 1.0f));
//        mInnerAnimation.setInterpolator(new LinearInterpolator());
        mInnerAnimation.setInterpolator(new PathInterpolator(0f, 0f, 0.34f, 0.93f));
        mInnerAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float value = (float) animation.getAnimatedValue();
//                Log.d("datph", "value --11111 = " + value);
                mValue = value;
                invalidate();
            }
        });

        mInnerAnimation.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {

            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {
                mStartProgress = mStartProgress % CIRCLE_SCORE;
            }
        });
        mInnerAnimation.setDuration(Constant.SCANNING_WAVE_DURATION);
        mInnerAnimation.setRepeatCount(ValueAnimator.INFINITE);
        mInnerAnimation.setRepeatMode(ValueAnimator.REVERSE);
    }

    @Override
    protected void onDraw(Canvas canvas) {
//        Log.d("datph", "onDraw() - mVal = " + mValue);
        float halfRingThickness = mThickness;

        int addPadding;
        int progress;

        switch (mState) {
            case 0:
                if (mValue >= mCondition) {
                    mState += 1;
                    mCondition = FLOOR_THRESHOLD;
                }
                break;
            case 1:
                if (mValue < mCondition) {
                    mState += 1;
                }
                break;
            default:
                break;
        }

        float[] position = new float[4];
        position[0] = halfRingThickness;
        position[1] = halfRingThickness;
        position[2] = canvas.getWidth()  - halfRingThickness;
        position[3] = canvas.getHeight() - halfRingThickness;


        if (mState > 0) {
            mArcElement.set(position[0], position[1], position[2], position[3]);
            progress = (int)((1 - mValue) * CIRCLE_SCORE);
            if (360 - progress < 5) {
                progress = 360;
            }
            mStartProgress += 2;
//            Log.d("datph", "onDraw() - mVal = " + mValue + "-pro = " + progress + "-st = " + mStartProgress);
            canvas.drawArc(mArcElement, mStartProgress, progress, false, mProgressPaint);
            mValue = mValue - 0.07f;
        }

        addPadding = (int)((1 - mValue) * (canvas.getWidth() /2));
        position[0] = halfRingThickness + addPadding;
        position[1] = halfRingThickness + addPadding;
        position[2] = canvas.getWidth()  - halfRingThickness - addPadding;
        position[3] = canvas.getHeight() - halfRingThickness - addPadding;

        mArcElement.set(position[0], position[1], position[2], position[3]);
        canvas.drawArc(mArcElement, 0, CIRCLE_SCORE, false, mInnerPaint);
    }

    public void start() {
        mCondition= CEIL_THRESHOLD;
        mState = 0;
        mInnerAnimation.start();
    }

    public void stop() {
        mInnerAnimation.setRepeatCount(0);
        mInnerAnimation.end();
        mInnerAnimation.cancel();
    }
}

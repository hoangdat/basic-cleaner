package com.fbon.android.app.basiccleaner.service;


import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ScheduleExecutor {
    private static final ExecutorService sSchedulerExecutor = Executors.newCachedThreadPool();

    public synchronized static ExecutorService getInstance() {
        return sSchedulerExecutor;
    }
}

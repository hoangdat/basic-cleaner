package com.fbon.android.app.basiccleaner.util;

import java.util.Arrays;
import java.util.List;

public class MediaFile {
    public static List<String> AUDIO = Arrays.asList("MP3", "MPGA", "M4A", "M4A", "WAV","AMR","AWB",
            "WMA", "OGG", "OGA", "AAC", "_3GA", "FLAC", "PYA", "MP4_A", "MP4_AUDIO", "_3GP_AUDIO",
            "_3G2_AUDIO", "ASF_AUDIO", "_3GPP_AUDIO", "QCP", "MKA");
    public static List<String> VIDEO = Arrays.asList("MP4", "M4V", "_3GP", "_3GPP", "_3GPP2", "_3G2",
            "WMV", "MPG", "MPEG", "ASF", "AVI", "DIVX", "FLV", "MKV", "MOV", "PYV", "SKM", "K3G", "AK3G",
            "WEBM", "RM", "RMVB", "SDP", "TS", "MTS", "M2TS", "M2T", "TRP", "TP");
    public static List<String> IMAGE = Arrays.asList("JPEG", "JPG", "MY5", "GIF", "PNG", "BMP", "XBMP",
            "WBMP", "MPO", "GOLF", "WEBP", "RAF", "ORF", "ERF", "FFF", "CRW", "CR2", "CR3", "DNG", "MEF",
            "MOS", "PXN", "SRW", "PTX", "PEF", "RW2", "BAY", "TIF", "K25", "KDC", "DCS", "DCR", "DRF",
            "ARW", "SRF", "SR2", "CAP", "IIQ", "MRW", "X3F", "R3D", "NEF", "NRW");


    static {
//        sExtensionToFileTypeMap.put("APK", FileType.APK);
//        sExtensionToFileTypeMap.put("JPG", FileType.JPG);
//        sExtensionToFileTypeMap.put("GIF", FileType.GIF);
//        sExtensionToFileTypeMap.put("JPEG", FileType.JPEG);
//        sExtensionToFileTypeMap.put("MP3", FileType.MP3);
    }

    public static class FileType {
        public static final int UNKNOWN = 0;

        // Image Files
        private static final int FIRST_IMAGE_FILE_TYPE = 10;
        private static int LAST_IMAGE_FILE_TYPE = FIRST_IMAGE_FILE_TYPE;
        public static final int JPEG = LAST_IMAGE_FILE_TYPE++;
        public static final int JPG = LAST_IMAGE_FILE_TYPE++;
        public static final int MY5 = LAST_IMAGE_FILE_TYPE++;
        public static final int GIF = LAST_IMAGE_FILE_TYPE++;
        public static final int PNG = LAST_IMAGE_FILE_TYPE++;
        public static final int BMP = LAST_IMAGE_FILE_TYPE++;
        public static final int XBMP = LAST_IMAGE_FILE_TYPE++;
        public static final int WBMP = LAST_IMAGE_FILE_TYPE++;
        public static final int MPO = LAST_IMAGE_FILE_TYPE++;
        public static final int GOLF = LAST_IMAGE_FILE_TYPE++;
        public static final int WEBP = LAST_IMAGE_FILE_TYPE++;

        public static final int RAF = LAST_IMAGE_FILE_TYPE++; //FujiFilm
        public static final int ORF = LAST_IMAGE_FILE_TYPE++; //Olympus
        public static final int ERF = LAST_IMAGE_FILE_TYPE++; //Epson
        public static final int FFF = LAST_IMAGE_FILE_TYPE++; //Imacon
        public static final int CRW = LAST_IMAGE_FILE_TYPE++; //Cannon
        public static final int CR2 = LAST_IMAGE_FILE_TYPE++;
        public static final int CR3 = LAST_IMAGE_FILE_TYPE++;
        public static final int DNG = LAST_IMAGE_FILE_TYPE++; //Adobe
        public static final int MEF = LAST_IMAGE_FILE_TYPE++; //Mamiya
        public static final int MOS = LAST_IMAGE_FILE_TYPE++;
        public static final int PXN = LAST_IMAGE_FILE_TYPE++; //Logitech
        public static final int SRW = LAST_IMAGE_FILE_TYPE++; //Samsung
        public static final int PTX = LAST_IMAGE_FILE_TYPE++; //Pentax
        public static final int PEF = LAST_IMAGE_FILE_TYPE++;
        public static final int RW2 = LAST_IMAGE_FILE_TYPE++; //Panasonic
        public static final int BAY = LAST_IMAGE_FILE_TYPE++; //Casio
        public static final int TIF = LAST_IMAGE_FILE_TYPE++; //Kodak
        public static final int K25 = LAST_IMAGE_FILE_TYPE++;
        public static final int KDC = LAST_IMAGE_FILE_TYPE++;
        public static final int DCS = LAST_IMAGE_FILE_TYPE++;
        public static final int DCR = LAST_IMAGE_FILE_TYPE++;
        public static final int DRF = LAST_IMAGE_FILE_TYPE++;
        public static final int ARW = LAST_IMAGE_FILE_TYPE++; //Sony
        public static final int SRF = LAST_IMAGE_FILE_TYPE++;
        public static final int SR2 = LAST_IMAGE_FILE_TYPE++;
        public static final int CAP = LAST_IMAGE_FILE_TYPE++; //FaceOne
        public static final int IIQ = LAST_IMAGE_FILE_TYPE++;
        public static final int MRW = LAST_IMAGE_FILE_TYPE++; //Minolta
        public static final int X3F = LAST_IMAGE_FILE_TYPE++; //Sigma
        public static final int R3D = LAST_IMAGE_FILE_TYPE++; //Red
        public static final int NEF = LAST_IMAGE_FILE_TYPE++; //Nikon
        public static final int NRW = LAST_IMAGE_FILE_TYPE++;

        //audio
        private static final int FIRST_AUDIO_FILE_TYPE = 100;
        private static int LAST_AUDIO_FILE_TYPE = FIRST_AUDIO_FILE_TYPE;
        public static final int MP3 = LAST_AUDIO_FILE_TYPE++;
        public static final int MPGA = LAST_AUDIO_FILE_TYPE++;
        public static final int M4A = LAST_AUDIO_FILE_TYPE++;
        public static final int WAV = LAST_AUDIO_FILE_TYPE++;
        public static final int AMR = LAST_AUDIO_FILE_TYPE++;
        public static final int AWB = LAST_AUDIO_FILE_TYPE++;
        public static final int WMA = LAST_AUDIO_FILE_TYPE++;
        public static final int OGG = LAST_AUDIO_FILE_TYPE++;
        public static final int OGA = LAST_AUDIO_FILE_TYPE++;
        public static final int AAC = LAST_AUDIO_FILE_TYPE++;
        public static final int _3GA = LAST_AUDIO_FILE_TYPE++;
        public static final int FLAC = LAST_AUDIO_FILE_TYPE++;
        public static final int PYA = LAST_AUDIO_FILE_TYPE++;
        public static final int MP4_A = LAST_AUDIO_FILE_TYPE++;
        public static final int MP4_AUDIO = LAST_AUDIO_FILE_TYPE++;
        public static final int _3GP_AUDIO = LAST_AUDIO_FILE_TYPE++;
        public static final int _3G2_AUDIO = LAST_AUDIO_FILE_TYPE++;
        public static final int ASF_AUDIO = LAST_AUDIO_FILE_TYPE++;
        public static final int _3GPP_AUDIO = LAST_AUDIO_FILE_TYPE++;
        public static final int QCP = LAST_AUDIO_FILE_TYPE++;
        public static final int MKA = LAST_AUDIO_FILE_TYPE++;

        //video
        private static final int FIRST_VIDEO_FILE_TYPE = 200;
        private static int LAST_VIDEO_FILE_TYPE = FIRST_VIDEO_FILE_TYPE;
        public static final int MP4 = LAST_VIDEO_FILE_TYPE++;
        public static final int M4V = LAST_VIDEO_FILE_TYPE++;
        public static final int _3GP = LAST_VIDEO_FILE_TYPE++;
        public static final int _3GPP = LAST_VIDEO_FILE_TYPE++;
        public static final int _3GPP2 = LAST_VIDEO_FILE_TYPE++;
        public static final int _3G2 = LAST_VIDEO_FILE_TYPE++;
        public static final int WMV = LAST_VIDEO_FILE_TYPE++;
        public static final int MPG = LAST_VIDEO_FILE_TYPE++;
        public static final int MPEG = LAST_VIDEO_FILE_TYPE++;
        public static final int ASF = LAST_VIDEO_FILE_TYPE++;
        public static final int AVI = LAST_VIDEO_FILE_TYPE++;
        public static final int DIVX = LAST_VIDEO_FILE_TYPE++;
        public static final int FLV = LAST_VIDEO_FILE_TYPE++;
        public static final int MKV = LAST_VIDEO_FILE_TYPE++;
        public static final int MOV = LAST_VIDEO_FILE_TYPE++;
        public static final int PYV = LAST_VIDEO_FILE_TYPE++;
        public static final int SKM = LAST_VIDEO_FILE_TYPE++;
        public static final int K3G = LAST_VIDEO_FILE_TYPE++;
        public static final int AK3G = LAST_VIDEO_FILE_TYPE++;
        public static final int WEBM = LAST_VIDEO_FILE_TYPE++;
        public static final int RM = LAST_VIDEO_FILE_TYPE++;
        public static final int RMVB = LAST_VIDEO_FILE_TYPE++;
        public static final int SDP = LAST_VIDEO_FILE_TYPE++;
        public static final int TS = LAST_VIDEO_FILE_TYPE++;
        public static final int MTS = LAST_VIDEO_FILE_TYPE++;
        public static final int M2TS = LAST_VIDEO_FILE_TYPE++;
        public static final int M2T = LAST_VIDEO_FILE_TYPE++;
        public static final int TRP = LAST_VIDEO_FILE_TYPE++;
        public static final int TP = LAST_VIDEO_FILE_TYPE++;

        private static final int FIRST_INSTALL_FILE_TYPE = 400;
        private static int LAST_INSTALL_FILE_TYPE = FIRST_INSTALL_FILE_TYPE;
        public static final int APK = LAST_INSTALL_FILE_TYPE++;
        public static final int WGT = LAST_INSTALL_FILE_TYPE++;

        public static boolean isAudioFileType(int fileType) {
            return ((fileType >= FIRST_AUDIO_FILE_TYPE && fileType <= LAST_AUDIO_FILE_TYPE));
        }

        public static boolean isVideoFileType(int fileType) {
            return (fileType >= FIRST_VIDEO_FILE_TYPE && fileType <= LAST_VIDEO_FILE_TYPE);
        }

        public static boolean isImageFileType(int fileType) {
            return (fileType >= FIRST_IMAGE_FILE_TYPE && fileType <= LAST_IMAGE_FILE_TYPE);
        }

        public static boolean isApkFileType(int fileType) {
            return (fileType == APK);
        }
    }
}

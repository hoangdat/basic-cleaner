package com.fbon.android.app.basiccleaner.widget;


import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.PathInterpolator;

import com.fbon.android.app.basiccleaner.R;


public class CircleView extends View {
    public static final String TAG  = "CircleView";

    private static final int CIRCLE_PROGRESS_FACTOR = 360;
    public static final int SCANNING_DURATION = 1667;

    private Paint mBgPaint;
    private Paint mRingPaint;

    private int mRingThickness;
    private int mRingColor;

    private int mScore = 0;

    private boolean mIsScanning = false;
    private float mStartAngle;

    private float mProgress;

    private RectF mArcElement;

    private ValueAnimator mScoreAnimator, mScanAnimator, mDeltaAnimator;
    private OnFinishedSetScore mFinishedScore;

    public CircleView(Context context) {
        super(context);
        initView(context);
    }

    public CircleView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public CircleView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    public CircleView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initView(context);
    }

    public interface OnFinishedSetScore {
        void onFinishedScore(Animator animator);
    }

    private void initView(Context context) {
        Resources resources = context.getResources();

        mRingThickness = resources.getDimensionPixelSize(R.dimen.circle_ring_thickness);

        mRingColor = resources.getColor(R.color.white);

        mBgPaint = new Paint();
        mBgPaint.setStyle(Paint.Style.STROKE);
        mBgPaint.setAntiAlias(true);
        mBgPaint.setColor(resources.getColor(R.color.white_little_little));
        mBgPaint.setStrokeWidth(mRingThickness);

        mRingPaint = new Paint();
        mRingPaint.setStyle(Paint.Style.STROKE);
        mRingPaint.setAntiAlias(true);
        mRingPaint.setStrokeCap(Paint.Cap.ROUND);
        mRingPaint.setColor(mRingColor);
        mRingPaint.setStrokeWidth(mRingThickness);

        mArcElement = new RectF();

        setUpAnimation();
    }

    public void setFinishedScore(OnFinishedSetScore mFinishedScore) {
        this.mFinishedScore = mFinishedScore;
    }

    private void setUpAnimation() {
        mScoreAnimator = ValueAnimator.ofFloat(0f, 1f);
        mScoreAnimator.setInterpolator(new PathInterpolator(0.33f, 0f, 0.40f, 1.0f));
        mScoreAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float value = (float) animation.getAnimatedValue();
                setProgress(value * mScore);
            }
        });
        mScoreAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (mFinishedScore != null) {
                    mFinishedScore.onFinishedScore(animation);
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        mDeltaAnimator = ValueAnimator.ofFloat(0f, 1f);
        mDeltaAnimator.setInterpolator(new PathInterpolator(.8f, 0f, .2f, 1f));
        mDeltaAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float value = (float) animation.getAnimatedValue();
                setProgress(value);
            }
        });
        mDeltaAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {

            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        mScanAnimator = ValueAnimator.ofFloat(0f, 180f);
        mScanAnimator.setInterpolator(new AccelerateInterpolator());
        mScanAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                mStartAngle = (float) animation.getAnimatedValue();
                invalidate();
            }
        });
    }

    @Override
    protected void onDraw(Canvas canvas) {
        float halfRingThickness = mRingThickness / 2;
        float arcX0 = halfRingThickness;
        float arcY0 = halfRingThickness;
        float arcX = canvas.getWidth()  - halfRingThickness;
        float arcY = canvas.getHeight() - halfRingThickness;

        mArcElement.set(arcX0, arcY0, arcX, arcY);

        canvas.drawArc(mArcElement, 0, 360, false, mBgPaint);
        if (mIsScanning) {
            canvas.drawArc(mArcElement, -90 + mStartAngle, 90, false, mRingPaint);
            canvas.drawArc(mArcElement, mStartAngle, 90, false, mRingPaint);
        } else {
            canvas.drawArc(mArcElement, -90, mProgress, false, mRingPaint);
        }
    }

    public void setProgress(float progress) {
        this.mProgress = CIRCLE_PROGRESS_FACTOR * progress / 100;
        invalidate();
    }

    public void setScore(int score, int colorCode, boolean animate, int duration) {
        if (score < 0 || score > 100) {
            score = 0;
        }

        mIsScanning = false;
        mScore = score;
        if(colorCode != -1)
            mRingPaint.setColor(colorCode);

        if (animate) {
            if(duration > 0)
                mScoreAnimator.setDuration(duration).start();
            else // default duration
                mScoreAnimator.setDuration(1700 * mScore / 100)
                        .start();
        } else {
            setProgress(mScore);
        }
    }

    /**
     *
     * @param score target value score % or score / 100
     * @param colorCode colorCode
     * @param duration
     */
    public void setDeltaScore(int score, int colorCode, int duration) {
        if (score < 0 || score > 100) {
            score = 0;
        }

        mIsScanning = false;
        float deltaStart = mScore;
        mScore = score;

        if (!mDeltaAnimator.isRunning()) {
            if (colorCode != -1)
                mRingPaint.setColor(colorCode);
            mDeltaAnimator.setFloatValues(deltaStart, mScore);
            mDeltaAnimator.setDuration(duration).start();
        }
    }

    public int getScore() {
        return mScore;
    }

    public void setScan(int colorCode) {
        mIsScanning = true;
        mRingPaint.setColor(colorCode);
        mScanAnimator.setRepeatCount(ValueAnimator.INFINITE);
        mScanAnimator.setRepeatMode(ValueAnimator.REVERSE);
        mScanAnimator.setDuration(1667).start();
    }

    public void cancelAnimation() {
        mDeltaAnimator.cancel();
        invalidate();
    }
}

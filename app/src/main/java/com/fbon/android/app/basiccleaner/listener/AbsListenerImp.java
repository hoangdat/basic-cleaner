package com.fbon.android.app.basiccleaner.listener;



public abstract class AbsListenerImp {
    protected ListenerMgr.LifeCycle mRegisterTime;
    protected ListenerMgr.LifeCycle mUnregisterTime;


    public AbsListenerImp(ListenerMgr.LifeCycle registerTime, ListenerMgr.LifeCycle unregisterTime) {
        mRegisterTime = registerTime;
        mUnregisterTime = unregisterTime;
    }

    public ListenerMgr.LifeCycle getRegisterTime() {
        return mRegisterTime;
    }

    public ListenerMgr.LifeCycle getUnregisterTime() {
        return mUnregisterTime;
    }

    public abstract void registerListener();
    public abstract void unregisterListener();
}

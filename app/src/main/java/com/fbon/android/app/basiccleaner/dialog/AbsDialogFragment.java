package com.fbon.android.app.basiccleaner.dialog;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;

import com.fbon.android.app.basiccleaner.fragment.FragmentController;


public abstract class AbsDialogFragment extends DialogFragment implements FragmentController.OnSceneChangeListener{
    private FragmentController mFragmentController;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mFragmentController = FragmentController.getInstance();
        mFragmentController.registerListener(this);
        return _createDialog();
    }

    protected abstract Dialog _createDialog();

    @Override
    public void onSceneChange(int scene) {
        dismissAllowingStateLoss();
    }

    @Override
    public void onDestroy() {
//        dismissAllowingStateLoss();
        super.onDestroy();
    }
}

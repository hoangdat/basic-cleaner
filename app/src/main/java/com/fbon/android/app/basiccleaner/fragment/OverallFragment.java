package com.fbon.android.app.basiccleaner.fragment;

import android.animation.Animator;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.transition.TransitionManager;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fbon.android.app.basiccleaner.R;
import com.fbon.android.app.basiccleaner.feature.CleanLdrManager;
import com.fbon.android.app.basiccleaner.feature.ClearMgr;
import com.fbon.android.app.basiccleaner.feature.memory.MemoryWorker;
import com.fbon.android.app.basiccleaner.model.StorageInfoDataModel;
import com.fbon.android.app.basiccleaner.uicore.Event;
import com.fbon.android.app.basiccleaner.util.AppFeature;
import com.fbon.android.app.basiccleaner.util.Constant;
import com.fbon.android.app.basiccleaner.util.Log;
import com.fbon.android.app.basiccleaner.util.PreferenceUtils;
import com.fbon.android.app.basiccleaner.util.StorageInfo;
import com.fbon.android.app.basiccleaner.util.UiUtils;

import java.util.ArrayList;
import java.util.HashMap;

public class OverallFragment extends BasicCleanFragment implements View.OnClickListener, CleanLdrManager.OnInfoListener {
    private static final String TAG = "overallF";
    private ImageButton mBtnStorage, mBtnMemory;
    private Button mBtnFixNow;
    private TextView mTxtProgressStt;
    private int mProgressScanDone = 100;
    private ViewGroup mContainerRoot;
    private FrameLayout mCircleContainer;
    private LinearLayout mAddInfoContainer;
    private TextView mTxtStorageAddInfo, mTxtMemAddInfo;
    private HashMap<Integer, ArrayList<Long>> mMapUsedTotal;
    private SparseArray<Integer> mInfos;
    private boolean mShowAddInfo;

    private static final int FLAG_SCAN_TYPE = AppFeature.isSupportListMem() ?
            (Constant.FLAG_LOAD_CACHE | Constant.FLAG_LOAD_UN | Constant.FLAG_LOAD_APP_MEM) :
            (Constant.FLAG_LOAD_CACHE | Constant.FLAG_LOAD_UN);

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutId(), container, false);
        initCommonView(view);
        initView(view);
        mShowAddInfo = false;

        return view;
    }

    @Override
    public void initView(View view) {
        mBtnFixNow = (Button) view.findViewById(R.id.fix_now);
        mBtnFixNow.setOnClickListener(this);

        mTxtProgressStt = (TextView) view.findViewById(R.id.overall_progress_status);

        mBtnMemory = (ImageButton) view.findViewById(R.id.btn_memory_item);
        mBtnMemory.setColorFilter(getActivity().getResources().getColor(R.color.black_little));
        mBtnMemory.setOnClickListener(this);

        mBtnStorage = (ImageButton) view.findViewById(R.id.btn_storage_item);
        mBtnStorage.setColorFilter(getActivity().getResources().getColor(R.color.black_little));
        mBtnStorage.setOnClickListener(this);

        mContainerRoot = (ViewGroup) view.findViewById(R.id.overall_frame_container);
        mCircleContainer = (FrameLayout) view.findViewById(R.id.overall_circle_container);
        mAddInfoContainer = (LinearLayout) view.findViewById(R.id.overall_add_on_info_container);

        mTxtStorageAddInfo = (TextView) view.findViewById(R.id.overall_add_info_storage);
        mTxtMemAddInfo = (TextView) view.findViewById(R.id.overall_add_info_mem);
    }

    @Override
    public int getLayoutId() {
        return R.layout.overall_fragment;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public boolean sendScanRequest() {
        return CleanLdrManager.getInstance(getActivity()).startLoadOverall(firstScanType(), this);
    }

    @Override
    public void setScan(boolean enable) {
        Log.d(TAG, "setScan()");
        initInfo();
        long lstClear = PreferenceUtils.getLastTimeOverallClean(getActivity());
        if (System.currentTimeMillis() - lstClear > Constant.PERIOD_CLEAN_OK) {
            super.setScan(enable);
            mProgressScanDone = 0;
        } else {
            Log.d(TAG, "setScan() - only request add info");
            setScanType(firstScanType());
            mProgressScanDone = 100;
            requestAddInfo();
        }
    }

    @Override
    public void scan() {
        updateResultView(Constant.FLAG_NONE, 0, false);
        if (!sendScanRequest()) {
            requestAddInfo();
        }
    }

    @Override
    public void updateResultView(int scanType, long prevScore, boolean isPercent) {
        Log.d(TAG, "updateResultView() - state = " + scanType);
       if (scanType == Constant.FLAG_NONE) {
           mTxtUsed.setVisibility(View.GONE);
           updateFixNowButton(false, R.string.clean_now, null);
       } else if (scanType == firstScanType()) {
           if (prevScore == 100) {
               mProgressScanDone = 100;
           } else {
               mProgressScanDone = calculateProgress();
           }
           String strScore;
           if (mProgressScanDone < 100) {
               strScore = String.format("%d%%", mProgressScanDone);
           } else {
               strScore = String.format("%d!", mProgressScanDone);
           }
           mTxtUsed.setText(strScore);
           mTxtUsed.setVisibility(View.VISIBLE);
           mTxtProgressStt.setText((mProgressScanDone < 100) ? R.string.loaded_recommend : R.string.cleared_sttus);
           updateFixNowButton(true, R.string.clean_now, null);
           setScanningEnable(false, mProgressScanDone);
        }
    }

    @Override
    public void _onClick(View v) {
        Log.d(TAG, "_onClick id = " + v.getId());
        int id = v.getId();
        switch (id) {
            case R.id.btn_memory_item:
                postEvent(Event.MEMORY_GO);
                break;
            case R.id.btn_storage_item:
                postEvent(Event.STORAGE_GO);
                break;
            case R.id.fix_now:
                ClearMgr.getInstance(getActivity()).setLstClear(firstScanType(), null, true);
            default:
                super._onClick(v);
                break;
        }
    }

    private int calculateProgress() {
        double ret = 1;
        double percent1, percent2 = 0;
        double w1 = 1;
        long freeSpace = StorageInfo.getStorageFreeSpace(getActivity(), StorageInfo.iStorageType.INTERNAL);
        long item = UiUtils.getStoreSize(getActivity(), firstScanType(), CleanLdrManager.CACHE_JUNK_TYPE);
        item += UiUtils.getStoreSize(getActivity(), firstScanType(), CleanLdrManager.UNNECESSARY_JUNK_TYPE);
        try {
            percent1 = item * 1F / freeSpace;
        } catch (Exception e) {
            percent1 = 1;
            e.printStackTrace();
        }
        Log.d(TAG, "calculateProgress() 1 = " + item + "-per = " + percent1 + "free = " + freeSpace);
        if (AppFeature.isSupportListMem()) {
            MemoryWorker.getInstance(getActivity()).updateMemInfo();
            long avail = MemoryWorker.getInstance(getActivity()).getTotalMemory();
            item = UiUtils.getStoreSize(getActivity(), firstScanType(), CleanLdrManager.MEM_JUNK_TYPE);
            try {
                w1 = 0.495;
                percent2 = item * 1F / avail;
            } catch (Exception e) {
                percent2 = 0;
                w1 = 1;
                e.printStackTrace();
            }
            Log.d(TAG, "calculateProgress() 2 = " + item + "-per = " + percent2 + "-avai = " + avail);
        }

        percent1 = percent1 * w1 + 0.505 * percent2;
        ret = ret - percent1;
        Log.d(TAG, "calculateProgress() 3 = " + item + "-per = " + percent1 + "-ret = " + ret);
        if (ret < Constant.THRESHOLD_SCORE) {
            ret = Math.floor(ret * 100);
        } else {
            ret = Math.ceil(ret * 100);
        }
        return (int)(ret);
    }

    @Override
    public void updateFixNowButton(boolean isEnable, int resId, String addInfo) {
        if (isEnable && mProgressScanDone == 100) {
            isEnable = false;
        }
        super.updateFixNowButton(isEnable, resId, addInfo);
    }

    @Override
    protected void _onJunkLoaderProgress(int scanType, int junkType, int progress, StorageInfoDataModel model) {
//        Log.d(TAG, "onJunkLoaderProgress type = " + junkType + "-" + model);
        if (junkType == CleanLdrManager.CACHE_JUNK_TYPE || junkType == CleanLdrManager.MEM_JUNK_TYPE) {
            mTxtProgressStt.setText(model.getPackageName());
        }
    }

    @Override
    protected void _onScanFinished(int scanType) {
        if (scanType == getScanType()) {
            if(scanType == firstScanType()) {
                requestAddInfo();
            }
        }
    }

    @Override
    public int firstScanType() {
        return FLAG_SCAN_TYPE;
    }

    @Override
    public int getFixNowEvent() {
        return Event.OVERALL_CLEAN;
    }

    @Override
    protected void _onFinishedScore(Animator animator) {
        if (getActivity() != null) {
            Resources res = getActivity().getResources();

            TransitionManager.beginDelayedTransition(mContainerRoot);
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) mCircleContainer.getLayoutParams();
            int width = res.getDimensionPixelSize(R.dimen.overall_circle_width_small);
            params.width = width;
            params.height = width;
            int margin = res.getDimensionPixelSize(R.dimen.overall_circle_margin_top_after_scan);
            params.setMarginStart(margin);
            params.addRule(RelativeLayout.ALIGN_PARENT_START);
            params.addRule(RelativeLayout.CENTER_VERTICAL);

            RelativeLayout.LayoutParams paramsInfo = (RelativeLayout.LayoutParams) mAddInfoContainer.getLayoutParams();
            paramsInfo.addRule(RelativeLayout.CENTER_VERTICAL);
            paramsInfo.addRule(RelativeLayout.ALIGN_PARENT_END);
            mTxtStorageAddInfo.setText(makeAddInfo(CleanLdrManager.MSG_START_LOAD_STORAGE_INFO));
            mTxtMemAddInfo.setText(makeAddInfo(CleanLdrManager.MSG_START_LOAD_MEM_INFO));

            Animation slideRight = AnimationUtils.makeInAnimation(getActivity(), false);
            slideRight.setDuration(900);
            mAddInfoContainer.setVisibility(View.VISIBLE);
            if (!mShowAddInfo) {
                mShowAddInfo = true;
                mAddInfoContainer.startAnimation(slideRight);
            }
            mCircleContainer.setLayoutParams(params);
            mAddInfoContainer.setLayoutParams(paramsInfo);
        }
    }

    @Override
    public void onInfo(int scanType, int infoType, ArrayList<Long> usedTotal) {
        Log.d(TAG, "onInfo() infoType = " + infoType + "-us = " + usedTotal);

        int tmp = mInfos.get(infoType, 0);
        if (tmp == 1) {
            Log.d(TAG, "onInfo() infoType = " + infoType);
            mMapUsedTotal.put(infoType, usedTotal);
            Log.d(TAG, "onInfo() mMap = " + mMapUsedTotal);
            mInfos.remove(infoType);
        }

        if (mInfos.size() == 0) {
            updateResultView(firstScanType(), mProgressScanDone, true);
        }

    }

    private void initInfo() {
        mMapUsedTotal = new HashMap<>();
        mInfos = new SparseArray<>();
        mInfos.put(CleanLdrManager.MSG_START_LOAD_STORAGE_INFO, 1);
        mInfos.put(CleanLdrManager.MSG_START_LOAD_MEM_INFO, 1);
    }

    private void requestAddInfo() {
        CleanLdrManager.getInstance(getActivity()).startInfo(this, CleanLdrManager.MSG_START_LOAD_ALL_INFO);
    }

    private String makeAddInfo(int typeInfo) {
        Log.d(TAG, "makeAddInfo() infoType = " + typeInfo);
        String ret;
        StringBuilder builder = new StringBuilder();
        ArrayList<Long> usedTotal = mMapUsedTotal.get(typeInfo);
        Log.d(TAG, "makeAddInfo() infoType = " + typeInfo + "-us = " + usedTotal);
        long used = 0;
        long total = 0;
        if (usedTotal != null) {
            int size = usedTotal.size();
            if (typeInfo == CleanLdrManager.MSG_START_LOAD_STORAGE_INFO && size == 2) {
                used = usedTotal.get(0);
                total = usedTotal.get(1);
            } else if (typeInfo == CleanLdrManager.MSG_START_LOAD_MEM_INFO && size == 4) {
                used = usedTotal.get(2);
                total = usedTotal.get(3);
            }
        }
        builder.append(UiUtils.makeFileSizeString(getActivity(), used)).append(" / ").append(UiUtils.makeFileSizeString(getActivity(), total));
        ret = builder.toString();

        return ret;
    }
}

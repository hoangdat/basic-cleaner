package com.fbon.android.app.basiccleaner.feature;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.IPackageStatsObserver;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageStats;
import android.database.Cursor;
import android.mtp.MtpConstants;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.provider.MediaStore;
import android.util.SparseArray;

import com.fbon.android.app.basiccleaner.adapter.JUnitHandler;
import com.fbon.android.app.basiccleaner.feature.memory.MemoryWorker;
import com.fbon.android.app.basiccleaner.model.AppInfoDataModel;
import com.fbon.android.app.basiccleaner.model.FileInfoDataModel;
import com.fbon.android.app.basiccleaner.model.StorageInfoDataModel;
import com.fbon.android.app.basiccleaner.util.Constant;
import com.fbon.android.app.basiccleaner.util.Log;
import com.fbon.android.app.basiccleaner.util.StorageInfo;
import com.fbon.android.app.basiccleaner.util.UiUtils;
import com.jaredrummler.android.processes.AndroidProcesses;
import com.jaredrummler.android.processes.models.AndroidAppProcess;
import com.jaredrummler.android.processes.models.Statm;

import java.io.IOException;
import java.io.Serializable;
import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;


public class CleanLdrManager {
    private static final String TAG = "CleanLdrMgr";
    public static final int DEFAULT_LARGE_SIZE = 25;
    public static final String[] REDUNDANT_FILE_EXTENSION = {
            ".log",
            ".tmp",
            ".temp"
    };

    private static HashMap<Integer, Integer> mMapScanTypeFlag = new HashMap<>();
    private SparseArray<Boolean> mMapRunning = new SparseArray<>();

    public static final Uri MEDIA_PROVIDER_URI = MediaStore.Files.getContentUri("external");
    private Handler mHandlerUI;
    // this is handler thread has a looper to run with MgrLoaderHandler
    private HandlerThread mAppHandlerThread, mApkHandlerThread, mUnnecessaryHandlerThread,
            mLargeHandlerThread, mCacheHandlerThread, mStorageHandlerThread;
    private MgrLoaderHandler mAppLoaderHandler, mApkLoaderHandler, mUnnecessaryLoaderHandler,
            mLargeLoaderHandler, mCacheLoaderHandler, mStorageLoaderHandler;
    private Context mContext;
    private PackageManager mPackageMgr;

    private OnInfoListener mInfoListener;
    private final ArrayList<WeakReference<OnJunkLoaderListener>> mJunkListener = new ArrayList<>();

    private int mScanType;
    private int mState;
    private int mFlagFinished;

    public static final int MSG_START_LOAD_APP_MEM              = 1;
    public static final int MSG_START_LOAD_APP_USAGE            = 2;


    public static final int MSG_START_LOAD_STORAGE              = 5;
    public static final int MSG_START_LOAD_CACHE                = 6;
    public static final int MSG_START_LOAD_JUNK                 = 7;
    public static final int MSG_START_LOAD_APK                  = 8;
    public static final int MSG_START_LOAD_UNNES                = 9;
    public static final int MSG_START_LOAD_LARGE                = 10;
    public static final int MSG_UI_LOAD_JUNK_PROGRESS           = 11;
    public static final int MSG_UI_LOAD_JUNK_FINISHED           = 12;
    public static final int MSG_START_LOAD_STORAGE_INFO         = 13;
//    public static final int MSG_UI_STORAGE_INFO_DONE            = 14;
    public static final int MSG_START_LOAD_CACHE_AND_UN         = 15;
//    public static final int MSG_UI_MEM_INFO_DONE                = 16;
    public static final int MSG_START_LOAD_MEM_INFO             = 17;
    public static final int MSG_START_LOAD_ALL_INFO             = 18;

    public static final int APK_JUNK_TYPE                       = 100;
    public static final int UNNECESSARY_JUNK_TYPE               = 101;
    public static final int LARGE_JUNK_TYPE                     = 102;
    public static final int EMPTY_FOLDER                        = 103;
    public static final int CACHE_JUNK_TYPE                     = 104;
    public static final int MEM_JUNK_TYPE                       = 105;

    public static final int LOADER_STATE_NONE                   = 2000;
    public static final int LOADER_STATE_SCAN                   = 2001;
    public static final int LOADER_STATE_SCAN_DONE              = 2002;

    static {
        mMapScanTypeFlag.put(APK_JUNK_TYPE, Constant.FLAG_LOAD_APK);
        mMapScanTypeFlag.put(UNNECESSARY_JUNK_TYPE, Constant.FLAG_LOAD_UN);
        mMapScanTypeFlag.put(LARGE_JUNK_TYPE, Constant.FLAG_LOAD_LARGE);
        mMapScanTypeFlag.put(CACHE_JUNK_TYPE, Constant.FLAG_LOAD_CACHE);
        mMapScanTypeFlag.put(MEM_JUNK_TYPE, Constant.FLAG_LOAD_APP_MEM);
    }

    private HashMap<Integer, ArrayList<StorageInfoDataModel>> mMapStore = new HashMap<>();


    public interface OnInfoListener {
        void onInfo(int scanType, int infoType, ArrayList<Long> usedTotal);
    }

    public interface OnJunkLoaderListener {
        void onJunkLoaderProgress(int scanType, int junkType, int progress, StorageInfoDataModel model);
        void onScanFinished(int scanType);
    }

    private static CleanLdrManager mInstance;

    public static CleanLdrManager getInstance(Context context) {
        if (mInstance == null) {
            Log.d(TAG, "getInstance");
            mInstance = new CleanLdrManager(context);
        }
        return mInstance;
    }

    private CleanLdrManager(Context context) {
        Log.d(TAG, "CleanLdrManager");
        mContext = context;
        mPackageMgr = context.getPackageManager();
        setScanType(0);
        setState(LOADER_STATE_NONE);
        mHandlerUI = new UIHandler();
    }

    public static HashMap<Integer, Integer> getMapTypeFlag() {
        return mMapScanTypeFlag;
    }

    public int getScanType() {
        return mScanType;
    }

    private void setScanType(int scanType) {
        this.mScanType = scanType;
    }

    public int getState() {
        return mState;
    }

    private void setState(int state) {
        Log.d(TAG, "setState() - state = " + state);
        this.mState = state;
    }

    public void resetLoader() {
        setScanType(Constant.FLAG_NONE);
        setState(LOADER_STATE_NONE);
        setRunningStore(MEM_JUNK_TYPE, false);
        setRunningStore(APK_JUNK_TYPE, false);
        setRunningStore(LARGE_JUNK_TYPE, false);
        setRunningStore(UNNECESSARY_JUNK_TYPE, false);
        setRunningStore(CACHE_JUNK_TYPE, false);
    }

    public boolean setFlagFinished(int flag) {
        boolean ret = false;
        flag = mMapScanTypeFlag.get(flag);
        Log.d(TAG, "setFlagFinished() flag = " + flag + "-mFlagFi = " + mFlagFinished);
        int tmpScan = getScanType() | flag;
        if (tmpScan == getScanType()) {
            mFlagFinished |= flag;
            ret = true;
        }
        return ret;
    }

    public void registerListener(OnJunkLoaderListener listener) {
        if (listener == null || containsListener(listener)) {
            return;
        }
        synchronized (mJunkListener) {
            mJunkListener.add(new WeakReference<OnJunkLoaderListener>(listener));
        }
    }

    public final void unregisterListener(OnJunkLoaderListener listener) {
        if (listener != null && containsListener(listener)) {
            removeListener(listener);
        }
    }

    private void removeListener(OnJunkLoaderListener listener) {
        synchronized (mJunkListener) {
            if (mJunkListener == null || listener == null) {
                return;
            }

            int size = mJunkListener.size();
            WeakReference<OnJunkLoaderListener> weakListener;
            // do not use foreach
            for (int i = size - 1; i >= 0; i--) {
                weakListener = mJunkListener.get(i);
                if (weakListener.get() == null || weakListener.get().equals(listener)) {
                    mJunkListener.remove(weakListener);
                }
            }
        }
    }

    private void notifyProgressObservers(int scanType, int junkType, int progress, StorageInfoDataModel data) {
        synchronized (mJunkListener) {
            int size = mJunkListener.size();
            WeakReference<OnJunkLoaderListener> weakListener;
            // do not use foreach
            for (int i = size - 1; i >= 0; i--) {
                try {
                    weakListener = mJunkListener.get(i);
                    if (weakListener == null) {
                        return;
                    }
                    if (weakListener.get() == null) {
                        mJunkListener.remove(weakListener);
                        continue;
                    }
                weakListener.get().onJunkLoaderProgress(scanType, junkType, progress, data);
                } catch (IndexOutOfBoundsException e) {
                    Log.e(TAG, "IndexOutOfBoundsException !", e);
                } catch (NullPointerException e) {
                    Log.e(TAG, "NullPointerException !", e);
                }
            }
        }
    }

    private void notifyFinishedObservers(int scanType) {
        synchronized (mJunkListener) {
            int size = mJunkListener.size();
            WeakReference<OnJunkLoaderListener> weakListener;
            // do not use foreach
            for (int i = size - 1; i >= 0; i--) {
                try {
                    weakListener = mJunkListener.get(i);
                    if (weakListener == null) {
                        return;
                    }
                    if (weakListener.get() == null) {
                        mJunkListener.remove(weakListener);
                        continue;
                    }
                weakListener.get().onScanFinished(scanType);
                } catch (IndexOutOfBoundsException e) {
                    Log.e(TAG, "IndexOutOfBoundsException !", e);
                } catch (NullPointerException e) {
                    Log.e(TAG, "NullPointerException !", e);
                }
            }
        }
    }

    private void unregisterAllListener() {
        synchronized (mJunkListener) {
            mJunkListener.clear();
        }
    }

    private boolean containsListener(OnJunkLoaderListener listener) {
        synchronized (mJunkListener) {
            if (mJunkListener == null || listener == null) {
                return false;
            }

            for (WeakReference<OnJunkLoaderListener> weakListener : mJunkListener) {
                if (listener.equals(weakListener.get())) { // listener is not null, but weakListener.get() can be null
                    return true;
                }
            }
        }
        return false;
    }

    private final class MgrLoaderHandler extends JUnitHandler {
        public MgrLoaderHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_START_LOAD_APP_MEM:
                    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                        loadCache();
                    } else {
                        loadApp();
                    }
                    break;
                case MSG_START_LOAD_APP_USAGE:
                    break;
                case MSG_START_LOAD_APK:
                    loadRedundantFile(APK_JUNK_TYPE);
                    break;
                case MSG_START_LOAD_CACHE:
                    loadCache();
                    break;
                case MSG_START_LOAD_UNNES:
                    loadRedundantFile(UNNECESSARY_JUNK_TYPE);
                    break;
                case MSG_START_LOAD_LARGE:
                    loadRedundantFile(LARGE_JUNK_TYPE);
                    break;
                case MSG_START_LOAD_STORAGE_INFO:
                    startStorageInfo(StorageInfo.iStorageType.INTERNAL);
                    break;
                case MSG_START_LOAD_MEM_INFO:
                    startMemInfo();
                    break;
                case MSG_START_LOAD_ALL_INFO:
                    startLoadAllInfo();
                    break;
                default:
                    break;
            }
        }
    }

    public static class AppsSizeComparator implements Serializable, Comparator<AppInfoDataModel> {

        @Override
        public int compare(AppInfoDataModel o1, AppInfoDataModel o2) {
            if (o1 != null && o2 != null) {
                return Long.compare(o1.getSize(), o2.getSize());
            }
            return 0;
        }
    }

    public static class StorageSizeComparator implements Serializable, Comparator<StorageInfoDataModel> {

        @Override
        public int compare(StorageInfoDataModel o1, StorageInfoDataModel o2) {
            if (o1 != null && o2 != null) {
                return Long.compare(o2.getSize(), o1.getSize());
            }
            return 0;
        }
    }

    /***
     * this handler to update to UI,
     * all work with UI need through this handler
     */
    private class UIHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            if (msg != null) {
                switch (msg.what) {
                    case MSG_UI_LOAD_JUNK_PROGRESS:
                        StorageInfoDataModel storageInfo = (StorageInfoDataModel) msg.obj;
                        if (msg.arg1 != -1) {
                            if (storageInfo != null && storageInfo.getSize() > 0) {
                                addToStore(msg.arg1, storageInfo);
                            }
                            notifyProgressObservers(getScanType(), msg.arg1, msg.arg2, storageInfo);
                        }
                        break;
                    case MSG_START_LOAD_STORAGE_INFO:
                        ArrayList<Long> usedTotal = (ArrayList<Long>) msg.obj;
                        if (mInfoListener != null && usedTotal != null) {
                            mInfoListener.onInfo(getScanType(), MSG_START_LOAD_STORAGE_INFO, usedTotal);
                        }
                        break;
                    case MSG_START_LOAD_MEM_INFO:
                        ArrayList<Long> memInfo = (ArrayList<Long>) msg.obj;
                        if (mInfoListener != null && memInfo != null) {
                            mInfoListener.onInfo(getScanType(), MSG_START_LOAD_MEM_INFO, memInfo);
                        }
                        break;
                    case MSG_UI_LOAD_JUNK_FINISHED:
                        if (msg.arg1 != -1) {
                            Log.d(TAG, "datph finished = " + msg.arg1 + "-fi = " + mFlagFinished);
                            if (setFlagFinished(msg.arg1)) {
                                checkDone();
                            }
                        }
                        break;
                    case MSG_START_LOAD_ALL_INFO:
                        HashMap<Integer, ArrayList<Long>> allInfo = (HashMap<Integer, ArrayList<Long>>) msg.obj;
                        if (mInfoListener != null && allInfo != null) {
                            for (Integer key : allInfo.keySet()) {
                                ArrayList<Long> lstUsedTotal = allInfo.get(key);
                                mInfoListener.onInfo(getScanType(), key, lstUsedTotal);
                            }
                        }
                        break;
                    default:
                        break;
                }
            }
        }
    }

    private void checkDone() {
        Log.d(TAG, "checkDone() - mFlag = " + mFlagFinished + "-get " + getScanType());
        if (mFlagFinished == getScanType()) {
            setState(LOADER_STATE_SCAN_DONE);
            notifyFinishedObservers(getScanType());
        }
    }

    public void startLoadAppMem(int scanType) {
        Log.d(TAG, "startLoadAppMem() - scanType = " + scanType);
        int junkType = 0;
        if (scanType == Constant.FLAG_LOAD_APP_MEM) {
            junkType = MEM_JUNK_TYPE;
        } else if (scanType == Constant.FLAG_LOAD_CACHE) {
            junkType = CACHE_JUNK_TYPE;
        } else {
            return;
        }
        if (getRunningStore(junkType)) {
            Log.d(TAG, "startLoadAppMem() - return");
            return;
        }
        clearStore(junkType);
        setRunningStore(junkType, true);
        if (mAppHandlerThread != null) {
            mAppHandlerThread.quit();
        }
        mAppHandlerThread = new HandlerThread(TAG + "-mem");
        mAppHandlerThread.start();

        if (mAppLoaderHandler != null) {
            mAppLoaderHandler.removeMessages(MSG_START_LOAD_APP_MEM);
        }

        mAppLoaderHandler = new MgrLoaderHandler(mAppHandlerThread.getLooper());

        if (mAppLoaderHandler != null && mAppHandlerThread.isAlive()) {
            mAppLoaderHandler.sendMessage(mAppLoaderHandler.obtainMessage(MSG_START_LOAD_APP_MEM));
        }
    }

    private void initScan(int scanType) {
        mFlagFinished = 0;
        setScanType(scanType);
        setState(LOADER_STATE_SCAN);
    }

//    private void resetStore() {
//        synchronized (mMapStore) {
//            mMapStore.clear();
//        }
//    }

    private void clearStore(int junkType) {
        synchronized (mMapStore) {
            ArrayList<StorageInfoDataModel> tmpList = mMapStore.get(junkType);
            if (tmpList == null) {
                tmpList = new ArrayList<>();
            }
            tmpList.clear();
        }
    }

    private void addToStore(int junkType, StorageInfoDataModel data) {
        synchronized (mMapStore) {
            ArrayList<StorageInfoDataModel> tmpList = mMapStore.get(junkType);
            if (tmpList == null) {
                tmpList = new ArrayList<>();
            }
            tmpList.add(data);
            mMapStore.put(junkType, tmpList);
        }
    }

    public boolean startLoadOverall(int scanType, OnJunkLoaderListener listener) {
        Log.d(TAG, "startLoadOverall() - scanType = " + scanType);
        registerListener(listener);
        if (scanType == getScanType()) {
            int state = getState();
            if (state == LOADER_STATE_SCAN_DONE) {
                return false;
            } else if (state == LOADER_STATE_SCAN) {
                return true;
            }
        }
        initScan(scanType);
        startLoadCache();
        startLoadUn();
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
            startLoadAppMem(Constant.FLAG_LOAD_APP_MEM);
        }
        return true;
    }

    public boolean startLoadStorage(int scanType, OnJunkLoaderListener listener) {
        Log.d(TAG, "startLoadStorage");
        registerListener(listener);
        if (scanType == getScanType()) {
            int state = getState();
            if (state == LOADER_STATE_SCAN_DONE) {
                return false;
            } else if (state == LOADER_STATE_SCAN) {
                return true;
            }
        }
        initScan(scanType);
        startLoadCache();
        startLoadUn();
        startLoadLarge();
        startLoadApk();
        return true;
    }

    public boolean startLoadMem(int scanType, OnJunkLoaderListener listener) {
        Log.d(TAG, "startLoadMem - scanType = " + scanType);
        registerListener(listener);
        if (scanType == getScanType()) {
            int state = getState();
            if (state == LOADER_STATE_SCAN_DONE) {
                return false;
            } else if (state == LOADER_STATE_SCAN) {
                return true;
            }
        }
        initScan(scanType);
        startLoadAppMem(scanType);
        return true;
    }

    public boolean startLoadLargeApk(int scanType, OnJunkLoaderListener listener) {
        Log.d(TAG, "startLoadLargeApk");
        registerListener(listener);
        if (scanType == getScanType()) {
            int state = getState();
            if (state == LOADER_STATE_SCAN_DONE) {
                return false;
            } else if (state == LOADER_STATE_SCAN) {
                return true;
            }
        }
        initScan(scanType);
        startLoadLarge();
        startLoadApk();
        return true;
    }

    public boolean startLoadCacheAndUn(int scanType, OnJunkLoaderListener listener) {
        Log.d(TAG, "startLoadCacheAndUn");
        registerListener(listener);
        if (scanType == getScanType()) {
            int state = getState();
            if (state == LOADER_STATE_SCAN_DONE) {
                return false;
            } else if (state == LOADER_STATE_SCAN) {
                return true;
            }
        }
        initScan(scanType);
        startLoadCache();
        startLoadUn();
        return true;
    }

    private void setInfoListener(OnInfoListener listener) {
        mInfoListener = listener;
    }

    public void startInfo(OnInfoListener listener, int what) {
        Log.d(TAG, "startInfo");
        setInfoListener(listener);
        if (mStorageHandlerThread != null) {
            mStorageHandlerThread.quit();
        }
        mStorageHandlerThread = new HandlerThread(TAG + "sm");
        mStorageHandlerThread.start();
        mStorageLoaderHandler = new MgrLoaderHandler(mStorageHandlerThread.getLooper());

        if (mStorageLoaderHandler != null && mStorageHandlerThread.isAlive()) {
            mStorageLoaderHandler.sendMessage(mStorageLoaderHandler.obtainMessage(what));
        }
    }

    private void startLoadUn() {
        if (getRunningStore(UNNECESSARY_JUNK_TYPE)) {
            return;
        }
        clearStore(UNNECESSARY_JUNK_TYPE);
        setRunningStore(UNNECESSARY_JUNK_TYPE, true);
        if (mUnnecessaryHandlerThread != null) {
            mUnnecessaryHandlerThread.quit();
        }
        mUnnecessaryHandlerThread = new HandlerThread(TAG + "-Un");
        mUnnecessaryHandlerThread.start();
        mUnnecessaryLoaderHandler = new MgrLoaderHandler(mUnnecessaryHandlerThread.getLooper());

        if (mUnnecessaryLoaderHandler != null && mUnnecessaryHandlerThread.isAlive()) {
            mUnnecessaryLoaderHandler.sendMessage(mUnnecessaryLoaderHandler.obtainMessage(MSG_START_LOAD_UNNES));
        }
    }

    private void startLoadLarge() {
        if (getRunningStore(LARGE_JUNK_TYPE)) {
            return;
        }
        clearStore(LARGE_JUNK_TYPE);
        setRunningStore(LARGE_JUNK_TYPE, true);
        if (mLargeHandlerThread != null) {
            mLargeHandlerThread.quit();
        }
        mLargeHandlerThread = new HandlerThread(TAG + "-Lgr");
        mLargeHandlerThread.start();
        mLargeLoaderHandler = new MgrLoaderHandler(mLargeHandlerThread.getLooper());

        if (mLargeLoaderHandler != null && mLargeHandlerThread.isAlive()) {
            mLargeLoaderHandler.sendMessage(mLargeLoaderHandler.obtainMessage(MSG_START_LOAD_LARGE));
        }
    }

    private void startLoadApk() {
        if (getRunningStore(APK_JUNK_TYPE)) {
            return;
        }
        clearStore(APK_JUNK_TYPE);
        setRunningStore(APK_JUNK_TYPE, true);
        if (mApkHandlerThread != null) {
            mApkHandlerThread.quit();
        }
        mApkHandlerThread = new HandlerThread(TAG + "-Apk");
        mApkHandlerThread.start();
        mApkLoaderHandler = new MgrLoaderHandler(mApkHandlerThread.getLooper());

        if (mApkLoaderHandler != null && mApkHandlerThread.isAlive()) {
            mApkLoaderHandler.sendMessage(mApkLoaderHandler.obtainMessage(MSG_START_LOAD_APK));
        }
    }

//    public boolean startLoadJunk(OnJunkLoaderListener listener, int what) {
//        Log.d(TAG, "startLoadJunk() - " + what);
//
//        if (what == MSG_START_LOAD_STORAGE || what == MSG_START_LOAD_JUNK) {
//            setJunkLoaderListener(listener);
//
//            mUnnecessaryHandlerThread = new HandlerThread(TAG + "-Un");
//            mUnnecessaryHandlerThread.start();
//            mUnnecessaryLoaderHandler = new MgrLoaderHandler(mUnnecessaryHandlerThread.getLooper());
//
//            mApkHandlerThread = new HandlerThread(TAG + "-Apk");
//            mApkHandlerThread.start();
//            mApkLoaderHandler = new MgrLoaderHandler(mApkHandlerThread.getLooper());
//
//            mLargeHandlerThread = new HandlerThread(TAG + "-Lgr");
//            mLargeHandlerThread.start();
//            mLargeLoaderHandler = new MgrLoaderHandler(mLargeHandlerThread.getLooper());
//
//            if (mUnnecessaryLoaderHandler != null && mUnnecessaryHandlerThread.isAlive()) {
//                mUnnecessaryLoaderHandler.sendMessage(mUnnecessaryLoaderHandler.obtainMessage(MSG_START_LOAD_UNNES));
//            }
//
//            if (mApkLoaderHandler != null && mApkHandlerThread.isAlive()) {
//                mApkLoaderHandler.sendMessage(mApkLoaderHandler.obtainMessage(MSG_START_LOAD_APK));
//            }
//
//            if (mLargeLoaderHandler != null && mLargeHandlerThread.isAlive()) {
//                mLargeLoaderHandler.sendMessage(mLargeLoaderHandler.obtainMessage(MSG_START_LOAD_LARGE));
//            }
//        }
//        return true;
//    }

    private void startLoadCache() {
        if (getRunningStore(CACHE_JUNK_TYPE)) {
            return;
        }
        clearStore(CACHE_JUNK_TYPE);
        setRunningStore(CACHE_JUNK_TYPE, true);
        if (mCacheHandlerThread != null) {
            mCacheHandlerThread.quit();
        }

        if (mCacheLoaderHandler != null) {
            mCacheLoaderHandler.removeMessages(MSG_START_LOAD_CACHE);
        }

        mCacheHandlerThread = new HandlerThread(TAG + "-cache");
        mCacheHandlerThread.start();

        mCacheLoaderHandler = new MgrLoaderHandler(mCacheHandlerThread.getLooper());

        if (mCacheLoaderHandler != null && mCacheHandlerThread.isAlive()) {
            mCacheLoaderHandler.sendMessage(mCacheLoaderHandler.obtainMessage(MSG_START_LOAD_CACHE));
        }
    }

//    public void loadAppUpperM() {
//        Log.d(TAG, "loadAppUpperM()");
//        UsageStatsManager usage = (UsageStatsManager) mContext.getSystemService(Context.USAGE_STATS_SERVICE);
//        long time = System.currentTimeMillis();
//        List<UsageStats> appList = usage.queryUsageStats(UsageStatsManager.INTERVAL_DAILY, time - 1000 * 1000, time);
//        Log.d(TAG, "loadAppUpperM() - appList = " + appList);
//        if (appList != null && !appList.isEmpty()) {
//            for (UsageStats usageStats : appList) {
//                Log.d(TAG, "loadAppUpperM - " + usageStats.getPackageName());
//            }
//        }
//    }

    public ArrayList<StorageInfoDataModel> getStore(int scanType, int junkType) {
        Log.d(TAG, "getStore() - scanType = " + scanType + "-junk = " + junkType);
        ArrayList<StorageInfoDataModel> ret = null;
        synchronized (mMapStore) {
            if (scanType == getScanType()) {
//                Log.d(TAG, "getStore() - state = " + getState());
                if (getState() == LOADER_STATE_SCAN_DONE) {
                    ret = mMapStore.get(junkType);
                }
            }
        }
        return ret;
    }

    public boolean getRunningStore(int junkType) {
        return mMapRunning.get(junkType, false);
    }

    public void setRunningStore(int junkType, boolean isRunning) {
        mMapRunning.put(junkType, isRunning);
    }

    public void loadApp(int bgType) {
        /*ActivityManager am = (ActivityManager) mContext.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> runningAppProcessInfos = am.getRunningAppProcesses();
        HashMap<PkgUid, AppInfoDataModel> pkgInfoMap = new HashMap<>();
        int uid;
        PkgUid pkgUid;
        AppInfoDataModel pkgInfo;
        if (runningAppProcessInfos != null && runningAppProcessInfos.size() > 0) {
            HashSet<String> importantPkgSet = new HashSet<>();
            for (ActivityManager.RunningAppProcessInfo procInfo : runningAppProcessInfos) {
                if (procInfo.importance < ActivityManager.RunningAppProcessInfo.IMPORTANCE_SERVICE
                        && procInfo.pkgList != null && procInfo.pkgList.length > 0) {
                    Log.d(TAG, "loadApp() - list = " + procInfo.pkgList + ",size = " + procInfo.pkgList.length);
                    importantPkgSet.addAll(Arrays.asList(procInfo.pkgList));
                }
            }

            for (ActivityManager.RunningAppProcessInfo procInfo : runningAppProcessInfos) {
                if (procInfo.importance < ActivityManager.RunningAppProcessInfo.IMPORTANCE_SERVICE
                        && procInfo.pkgList != null && procInfo.pkgList.length > 0
//                        && UserHandle.semGetUserId(procInfo.uid) == UserHandle.semGetMyUserId()
                        ) {
                    for (String pkgName : procInfo.pkgList) {
                        Log.d(TAG, "loadApp() - pkgName = " + pkgName);
                        if (importantPkgSet.contains(pkgName)) {
                            continue; //add them white list
                        }
                        uid = UserHandle.semGetUserId(procInfo.uid);
                        pkgUid = new PkgUid(pkgName, uid);
                        pkgInfo = pkgInfoMap.get(pkgUid);
                        if (pkgInfo == null) {
                            pkgInfo = new AppInfoDataModel(pkgName);
                            pkgInfo.setPackageName(pkgName);
                            String label = getApplicationLabel(pkgName);
                            if (label != null) {
                                pkgInfo.setName(label);
                            } else {
                                continue;
                            }
                        }
                        pkgInfo.addProcessName(procInfo.processName);
                        pkgInfo.addPid(procInfo.pid);
                        if (!pkgInfoMap.containsKey(pkgUid)) {
                            pkgInfoMap.put(pkgUid, pkgInfo);
                        }
                    }
                }
            }

            Iterator<Map.Entry<PkgUid, AppInfoDataModel>> it = pkgInfoMap.entrySet().iterator();
            AppInfoDataModel model;
            long memSize;
            while (it.hasNext()) {
                model = it.next().getValue();
                memSize = MemoryWorker.getInstance(mContext).getMemOfPids(model.getPidList());
                model.setSize(memSize);
                if (mHandlerUI != null) {
                    mHandlerUI.sendMessage(mHandlerUI.obtainMessage(MSG_UI_LOAD_JUNK_PROGRESS, MEM_JUNK_TYPE, -1, model));
                }
            }
            setRunningStore(MEM_JUNK_TYPE, false);
            if (mHandlerUI != null) {
                mHandlerUI.sendMessage(mHandlerUI.obtainMessage(MSG_UI_LOAD_JUNK_FINISHED, MEM_JUNK_TYPE, -1, null));
            }
        }*/
    }

//    private String getApplicationLabel(String pkgName) {
//        // Retrieving app name is long term task.
//        // Get app name only if mShouldIncludeDetailAppInfo
//        try {
//            ApplicationInfo appInfo = mContext.getPackageManager().getApplicationInfo(pkgName, 0);
//            return appInfo.loadLabel(mContext.getPackageManager()).toString();
//        } catch (Exception e) {
//            // Do not print error logs
//        }
//        return null;
//    }

    private static ArrayList<String> mWhiteListMem = new ArrayList<>();
    static {
        mWhiteListMem.add("com.fbon.android.app.basiccleaner");
    }
    /***
     * only work for Android < N
     */
    public void loadApp() {
        List<AndroidAppProcess> processes = AndroidProcesses.getRunningAppProcesses();
        Log.d(TAG, "loadApp size " + processes.size());

        for (AndroidAppProcess process : processes) {
            String packageName = process.getPackageName();
            if (!mWhiteListMem.contains(packageName)) {
                AppInfoDataModel model = new AppInfoDataModel(process.getPackageName());

                try {
                    Statm statm = process.statm();
                    model.setSize(statm.getResidentSetSize());
                } catch (IOException ioe) {
                    Log.e(TAG, "loadApp() - IOException: " + ioe);
                    continue;
                }

                try {
                    PackageInfo pkgInfo = process.getPackageInfo(mContext, 0);
                    model.setName(pkgInfo.applicationInfo.loadLabel(mPackageMgr).toString());
                } catch (PackageManager.NameNotFoundException e) {
                    Log.e(TAG, "loadApp() - e = " + e.getMessage());
                    continue;
                }
//            Log.d(TAG, "loadApp() - model = " + model.getPackageName() + ",sz = " + model.getSize());

                if (mHandlerUI != null) {
                    mHandlerUI.sendMessage(mHandlerUI.obtainMessage(MSG_UI_LOAD_JUNK_PROGRESS, MEM_JUNK_TYPE, -1, model));
                }
            }
        }

//        Log.d(TAG, "loadApp() - finished");
        setRunningStore(MEM_JUNK_TYPE, false);
        if (mHandlerUI != null) {
//            Log.d(TAG, "loadApp() - finished - 2");
            mHandlerUI.sendMessage(mHandlerUI.obtainMessage(MSG_UI_LOAD_JUNK_FINISHED, MEM_JUNK_TYPE, -1, null));
        }
    }

    private static ArrayList<String> mWhiteListCache = new ArrayList<>();
    static {
        mWhiteListCache.add("com.fbon.android.app.basiccleaner");
    }

    public void loadCache() {
        Log.d(TAG, "loadCache()");
        List<ApplicationInfo> installedPackages = mPackageMgr.getInstalledApplications(PackageManager.GET_GIDS);
        HashMap<String, String> mAppName = new HashMap<>();
        int nTotal = installedPackages.size() - mWhiteListCache.size();
        IPackageStatsObserver.Stub observer = new PackageStatsObserver(mAppName, nTotal);

        int count = 0;
        for (int i = 0; i < nTotal; i++) {
            ApplicationInfo info = installedPackages.get(i);
            String packageName = info.packageName;
            if (!mWhiteListCache.contains(packageName)) {
                count = count + 1;
                Log.d(TAG, "loadCache() add = " + count);
                mAppName.put(info.packageName, mPackageMgr.getApplicationLabel(info).toString());
                getPackageInfo(info.packageName, observer);
            }
        }
        ((PackageStatsObserver) observer).setTotalSize(count);
    }

    public void getPackageInfo(String packageName, IPackageStatsObserver.Stub observer) {
        try {
            Method getPackageSizeInfo = mPackageMgr.getClass()
                    .getMethod("getPackageSizeInfo", String.class, IPackageStatsObserver.class);

            getPackageSizeInfo.invoke(mPackageMgr, packageName, observer);
        } catch (NoSuchMethodException e ) {
            Log.e(TAG, "loadCache() error = " + e.getMessage());
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            Log.e(TAG, "loadCache() error = " + e.getMessage());
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            Log.e(TAG, "loadCache() error = " + e.getMessage());
            e.printStackTrace();
        }
    }

    private class PackageStatsObserver extends IPackageStatsObserver.Stub {
        HashMap<String, String> mAppNameMap;
        ArrayList<StorageInfoDataModel> mScannedList;
        volatile int mTotalSize;
        int nProgress;

        PackageStatsObserver (HashMap<String, String> map, int totalSize) {
            mAppNameMap = map;
            mScannedList = new ArrayList<>();
            mTotalSize = totalSize;
            nProgress = 0;
        }

        public void setTotalSize(int totalSize) {
            this.mTotalSize = totalSize;
        }

        @Override
        public void onGetStatsCompleted(PackageStats pStats, boolean succeeded) throws RemoteException {
            Log.d(TAG, "onGetStatsCompleted() start stat = " + pStats + "-su = " + succeeded);
            nProgress++;

            if (pStats != null && succeeded) {
                StorageInfoDataModel storage = new StorageInfoDataModel(CACHE_JUNK_TYPE);
                storage.setPackageName(pStats.packageName);
                if (mAppNameMap != null) {
                    storage.setName(mAppNameMap.get(pStats.packageName) != null ? mAppNameMap.get(pStats.packageName) : pStats.packageName);
                }
                Log.d(TAG, "onGetStatsCompleted() pk = " + pStats.packageName + ": cache sz = " + pStats.cacheSize + "-exCache sz = " + pStats.externalCacheSize);
                long size = pStats.cacheSize + pStats.externalCacheSize;
                if (size > 0) {
                    storage.setSize(size);

                    mScannedList.add(storage);
                    if (mHandlerUI != null) {
                        mHandlerUI.sendMessage(mHandlerUI.obtainMessage(MSG_UI_LOAD_JUNK_PROGRESS, CACHE_JUNK_TYPE, nProgress / mTotalSize, storage));
                    }
                }
            }

            if (nProgress == mTotalSize) {
                setRunningStore(CACHE_JUNK_TYPE, false);
                if (mHandlerUI != null) {
                    mHandlerUI.sendMessage(mHandlerUI.obtainMessage(MSG_UI_LOAD_JUNK_FINISHED, CACHE_JUNK_TYPE, -1, mScannedList));
                }
            }
        }
    }

    public void loadRedundantFile(int junkType) {
        if (mContext != null) {
            ArrayList<StorageInfoDataModel> listStorage = new ArrayList<>();
            Log.d(TAG, "loadRedundantFile() - junkType = " + junkType + " - selection = " + createSelection(junkType));
            try (Cursor c = mContext.getContentResolver().query(MEDIA_PROVIDER_URI, null, createSelection(junkType),
                    getSelectionArgs(junkType), null)) {
                FileInfoDataModel storage;
                if (c != null && c.moveToFirst()) {
                    int nTotalCount = c.getCount();
                    int nCount = 0;
                    do {
                        nCount++;
                        storage = new FileInfoDataModel(junkType);
                        String name = c.getString(c.getColumnIndex(MediaStore.Files.FileColumns.DISPLAY_NAME));
                        String path = c.getString(c.getColumnIndex(MediaStore.Files.FileColumns.DATA));
                        Log.d(TAG, "loadRedundantFile() - junkType = " + junkType + " - model = " + path);
                        long size = c.getLong(c.getColumnIndex(MediaStore.Files.FileColumns.SIZE));
                        int id = c.getInt(c.getColumnIndex(MediaStore.Files.FileColumns._ID));
                        storage.setName(name);
                        storage.setPath(path);
                        storage.setSize(size);
                        storage.setId(id);

                        listStorage.add(storage);
                        if (mHandlerUI != null) {
                            mHandlerUI.sendMessage(mHandlerUI.obtainMessage(MSG_UI_LOAD_JUNK_PROGRESS, junkType, nCount / nTotalCount, storage));
                        }
                    } while (c.moveToNext());
                }
            } finally {
                setRunningStore(junkType, false);
                if (mHandlerUI != null) {
                    mHandlerUI.sendMessage(mHandlerUI.obtainMessage(MSG_UI_LOAD_JUNK_FINISHED, junkType, -1, listStorage));
                }
            }
        }
    }

    private String createSelection(int junkType) {
        String ret = null;
        StringBuilder sb = new StringBuilder();
        sb.append("((").append("format").append("!=").append(MtpConstants.FORMAT_ASSOCIATION).append(")")
                .append(" AND (").append(MediaStore.Files.FileColumns.DATA).append(" LIKE '/storage/emulated%')");

        switch (junkType) {
            case APK_JUNK_TYPE:
                sb.append(" AND (").append(MediaStore.Files.FileColumns.DATA).append(" LIKE ?))");
                break;
            case LARGE_JUNK_TYPE:
                sb.append(" AND (").append(MediaStore.Files.FileColumns.DATA).append(" NOT LIKE ? AND ");
                for (int i = 0; i < REDUNDANT_FILE_EXTENSION.length; i++) {
                    if (i != REDUNDANT_FILE_EXTENSION.length - 1) {
                        sb.append(MediaStore.Files.FileColumns.DATA).append(" NOT LIKE ? AND ");
                    } else {
                        sb.append(MediaStore.Files.FileColumns.DATA).append(" NOT LIKE ?)");
                    }
                }
                sb.append(" AND (").append(MediaStore.Files.FileColumns.SIZE).append("> ?))");
                break;
            case UNNECESSARY_JUNK_TYPE:
                sb.append(" AND (").append(MediaStore.Files.FileColumns.SIZE).append("=0 OR ");
                for (int i = 0; i < REDUNDANT_FILE_EXTENSION.length; i++) {
                    if (i != REDUNDANT_FILE_EXTENSION.length - 1) {
                        sb.append(MediaStore.Files.FileColumns.DATA).append(" LIKE ? OR ");
                    } else {
                        sb.append(MediaStore.Files.FileColumns.DATA).append(" LIKE ?))");
                    }
                }
                break;
            default:
                break;
        }

        ret = sb.toString();
        return ret;
    }

    private String[] getSelectionArgs(int junkType) {
        String [] args = {};
        switch (junkType) {
            case APK_JUNK_TYPE:
                args = new String[] {"%.apk"};
                break;
            case LARGE_JUNK_TYPE:
                args = new String[2 + REDUNDANT_FILE_EXTENSION.length];
                int index = 0;
                args[index++] = "%.apk";
                for (int i = 0; i < REDUNDANT_FILE_EXTENSION.length; i++) {
                    args[index++] = '%' + REDUNDANT_FILE_EXTENSION[i];
                }
                args[index] = String.valueOf(DEFAULT_LARGE_SIZE * Constant.MEGA_BYTES);
                break;
            case UNNECESSARY_JUNK_TYPE:
                args = new String[REDUNDANT_FILE_EXTENSION.length];
                for (int i = 0; i < REDUNDANT_FILE_EXTENSION.length; i++) {
                    args[i] = '%' + REDUNDANT_FILE_EXTENSION[i];
                }
                break;
        }
        return args;
    }

    private void startLoadAllInfo() {
        HashMap<Integer, ArrayList<Long>> mapUsedTotal = new HashMap<>();
        long totalSize = StorageInfo.getStorageSize(mContext, StorageInfo.iStorageType.INTERNAL);
        long freeSpace = StorageInfo.getStorageFreeSpace(mContext, StorageInfo.iStorageType.INTERNAL);
        long used;

        ArrayList<Long> usedTotal = new ArrayList<>();
        if (totalSize != 0 && freeSpace != -1) {
            used = totalSize - freeSpace;
            Log.d(TAG, "startLoadAllInfo() used = " + used);
            usedTotal.add(used);
            usedTotal.add(totalSize);
            mapUsedTotal.put(MSG_START_LOAD_STORAGE_INFO, usedTotal);
        }

        MemoryWorker.getInstance(mContext).updateMemInfo();
        freeSpace = MemoryWorker.getInstance(mContext).getAvailMem();
        totalSize = MemoryWorker.getInstance(mContext).getTotalMemory();
        used = totalSize - freeSpace;

        long totalHWRamSize = UiUtils.getHWMemSize(mContext);
        long reserved = totalHWRamSize - totalSize;

        usedTotal = new ArrayList<>();
        usedTotal.add(reserved);
        usedTotal.add(freeSpace);
        usedTotal.add(used);
        usedTotal.add(totalHWRamSize);
        mapUsedTotal.put(MSG_START_LOAD_MEM_INFO, usedTotal);

        if (mHandlerUI != null)
            mHandlerUI.sendMessage(mHandlerUI.obtainMessage(MSG_START_LOAD_ALL_INFO, mapUsedTotal));
    }

    private void startStorageInfo(int storageType) {
        ArrayList<Long> ret = new ArrayList<>();
        long totalSize = StorageInfo.getStorageSize(mContext, storageType);
        long freeSpace = StorageInfo.getStorageFreeSpace(mContext, storageType);
        long used;


        if (totalSize != 0 && freeSpace != -1) {
            used = totalSize - freeSpace;
            Log.d(TAG, "startStorageInfo() used = " + used);
            ret.add(used);
            ret.add(totalSize);
        }

        if (mHandlerUI != null)
            mHandlerUI.sendMessage(mHandlerUI.obtainMessage(MSG_START_LOAD_STORAGE_INFO, ret));
    }

    private void startMemInfo() {
        ArrayList<Long> ret = new ArrayList<>();
        MemoryWorker.getInstance(mContext).updateMemInfo();
        long avail = MemoryWorker.getInstance(mContext).getAvailMem();
        long total = MemoryWorker.getInstance(mContext).getTotalMemory();
        long used = total - avail;

        long totalHWRamSize = UiUtils.getHWMemSize(mContext);
        long reserved = totalHWRamSize - total;

        ret.add(reserved);
        ret.add(avail);
        ret.add(used);
        ret.add(totalHWRamSize);

        if (mHandlerUI != null)
            mHandlerUI.sendMessage(mHandlerUI.obtainMessage(MSG_START_LOAD_MEM_INFO, ret));
    }

    public void destroy() {
        if (mApkHandlerThread != null) {
            mApkHandlerThread.quit();
        }

        if (mCacheHandlerThread != null) {
            mCacheHandlerThread.quit();
        }

        if (mUnnecessaryHandlerThread != null) {
            mUnnecessaryHandlerThread.quit();
        }

        if (mLargeHandlerThread != null) {
            mLargeHandlerThread.quit();
        }

        if (mAppHandlerThread != null) {
            mAppHandlerThread.quit();
        }

        if (mInfoListener != null) {
            mInfoListener = null;
        }

        unregisterAllListener();

        if (mApkLoaderHandler != null) {
            mApkLoaderHandler = null;
        }

        if (mCacheLoaderHandler != null) {
            mCacheLoaderHandler = null;
        }

        if (mHandlerUI != null) {
            mHandlerUI = null;
        }
    }
}

package com.fbon.android.app.basiccleaner.adapter;

public class SelectionMgr {
    private static SelectionMgr mInstance;
    private SelectionMgr() {

    }

    public static SelectionMgr getInstance() {
        if (mInstance == null) {
            mInstance = new SelectionMgr();
        }
        return mInstance;
    }
}

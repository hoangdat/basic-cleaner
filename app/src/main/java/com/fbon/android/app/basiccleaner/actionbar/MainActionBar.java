package com.fbon.android.app.basiccleaner.actionbar;

import android.app.ActionBar;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.fbon.android.app.basiccleaner.R;
import com.fbon.android.app.basiccleaner.activity.SettingsActivity;
import com.fbon.android.app.basiccleaner.fragment.FragmentController;
import com.fbon.android.app.basiccleaner.util.Log;
import com.fbon.android.app.basiccleaner.util.UiUtils;

/**
 * control action bar of this app
 */
public class MainActionBar implements FragmentController.OnSceneChangeListener {
    private static final String TAG = "MainActionBar";
    private Activity mActivity = null;
    private ActionBar mActionbar = null;
    private int mScene;

    public MainActionBar(Activity activity, int scene) {
        mActivity = activity;
        mActionbar = mActivity.getActionBar();
        onSceneChange(scene);

        FragmentController.getInstance().registerListener(this);
    }

    public void onPrepareOptionMenu(Menu menu, int scene, Activity activity) {
        switch (scene) {
            case FragmentController.Scene.Overall:
                addOverallMenu(menu, activity);
                break;
            case FragmentController.Scene.Memory:
            case FragmentController.Scene.Storage:
            case FragmentController.Scene.More_space:
                addItemMenu(menu, activity);
                break;
            default:
                menu.clear();
                break;
        }
    }

    public void onOptionsItemSelected(MenuItem menuItem, Activity activity) {
        int id = menuItem.getItemId();
        Log.i(TAG, "selectOption - command : " + id);
        switch (id) {
            case R.id.action_settings:
                Intent intent = new Intent(activity, SettingsActivity.class);
                activity.startActivity(intent);
                break;
            case R.id.action_rate_us:
                rateUs(activity);
                break;
            default:
                break;
        }
    }

    private void addOverallMenu(Menu menu, Activity activity) {
        Log.i(TAG, "addOverallMenu()");
        clearMenu(menu);

        activity.getMenuInflater().inflate(R.menu.menu_first, menu);
    }

    private void addItemMenu(Menu menu, Activity activity) {
        Log.i(TAG, "addItemMenu()");
        clearMenu(menu);
    }

    @Override
    public void onSceneChange(int scene) {
        Log.i(TAG, "onSceneChange - scene: " + scene);
        if (mActivity == null) {
            return;
        }
        mScene = scene;
        switch (scene) {
            case FragmentController.Scene.Overall:
                showOverall();
                break;
            case FragmentController.Scene.Memory:
            case FragmentController.Scene.Storage:
                showItem();
                break;
            case FragmentController.Scene.Clean_battery:
            case FragmentController.Scene.Clean_memory:
            case FragmentController.Scene.Clean_storage:
            case FragmentController.Scene.CleanAll:
            case FragmentController.Scene.Clean_more_space:
            case FragmentController.Scene.Clean_quick:
                showClean();
                break;
            case FragmentController.Scene.More_space:
                showMoreSpace();
                break;
            default:
                hide();
                break;
        }
        mActivity.invalidateOptionsMenu();
    }

    private void showOverall() {
        Log.d(TAG, "showOverall");
        if (mActionbar != null && mActivity != null) {
            mActionbar.setDisplayShowHomeEnabled(false);
            mActionbar.setDisplayHomeAsUpEnabled(false);
            mActionbar.setDisplayUseLogoEnabled(false);

            mActionbar.setTitle(UiUtils.getTitle(mActivity, mScene));
            mActivity.invalidateOptionsMenu();
        }
    }

    private void showItem() {
        Log.d(TAG, "showItem");
        if (mActivity != null && mActionbar != null) {
            mActionbar.setDisplayShowHomeEnabled(false);
            mActionbar.setDisplayHomeAsUpEnabled(true);
            mActionbar.setDisplayUseLogoEnabled(false);

            mActionbar.setTitle(UiUtils.getTitle(mActivity, mScene));
            mActivity.invalidateOptionsMenu();
        }
    }

    private void showMoreSpace() {
        Log.d(TAG, "showMoreSpace");
        if (mActivity != null && mActionbar != null) {
            mActionbar.setDisplayShowHomeEnabled(false);
            mActionbar.setDisplayHomeAsUpEnabled(true);
            mActionbar.setDisplayUseLogoEnabled(false);

            mActionbar.setTitle(UiUtils.getTitle(mActivity, mScene));
            mActivity.invalidateOptionsMenu();
        }
    }

    private void clearMenu(Menu menu) {
        Log.i(TAG, "clearMenu");
        menu.clear();
    }

    private void showClean() {
        Log.d(TAG, "showClean");
        if (mActivity != null && mActionbar != null) {
            mActionbar.setDisplayShowHomeEnabled(false);
            mActionbar.setDisplayHomeAsUpEnabled(true);
            mActionbar.setDisplayUseLogoEnabled(false);

            mActionbar.setTitle(UiUtils.getTitle(mActivity, mScene));
            mActivity.invalidateOptionsMenu();
        }
    }

    private void hide() {
        mActionbar.hide();
    }

    public void onDestroy() {
    }

    private boolean startActivity(Intent aIntent, Activity activity) {
        try {
            activity.startActivity(aIntent);
            return true;
        }
        catch (ActivityNotFoundException e) {
            return false;
        }
    }


    public void rateUs(Activity activity) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        //Try Google play
        intent.setData(Uri.parse("market://details?id=com.fbon.android.app.basiccleaner"));
        if (!startActivity(intent, activity)) {
            //Market (Google play) app seems not installed, let's try to open a webbrowser
            intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.fbon.android.app.basiccleaner"));
            if (!startActivity(intent, activity)) {
                Toast.makeText(activity, "Could not open Android market, please install the market app.", Toast.LENGTH_SHORT).show();
            }
        }
    }
}

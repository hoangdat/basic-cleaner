package com.fbon.android.app.basiccleaner.uicore;


import com.fbon.android.app.basiccleaner.util.Log;

import java.util.Observable;


public class BasicCleanerObservable extends Observable {
    private static final String TAG = "BasicCleanerObservable";
    private static BasicCleanerObservable mInstance = null;

    public static BasicCleanerObservable getInstance() {
        if (mInstance == null) {
            mInstance = new BasicCleanerObservable();
        }
        return mInstance;
    }

    @Override
    public void notifyObservers(Object arg) {
        Log.d(TAG, "notifyObservers - data: " + (int)arg);
        setChanged();
        super.notifyObservers(arg);
    }
}

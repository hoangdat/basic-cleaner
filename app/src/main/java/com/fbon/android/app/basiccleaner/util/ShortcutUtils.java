package com.fbon.android.app.basiccleaner.util;


import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;

import com.fbon.android.app.basiccleaner.R;
import com.fbon.android.app.basiccleaner.fragment.FragmentController;
import com.fbon.android.app.basiccleaner.thumbnail.ThumbnailMgr;

public class ShortcutUtils {

    private static final String ACTION_INSTALL_SHORTCUT_TO_HOME = "com.android.launcher.action.INSTALL_SHORTCUT";

    public static Intent getIntentForShortcut(Context context) {
        Intent intent = null;
        Resources res = context.getResources();
        intent = new Intent(ACTION_INSTALL_SHORTCUT_TO_HOME);
        Intent pendingIntent = getPendingIntentForShortcut();
        int thumbnailSize = res.getDimensionPixelSize(R.dimen.shortcut_icon_thumbnail_size);
        Bitmap shortcutIconBm = ThumbnailMgr.getInstance(context).getHomeShortcutBitmap(context, thumbnailSize);

        intent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, pendingIntent);
        intent.putExtra(Intent.EXTRA_SHORTCUT_NAME, res.getString(R.string.quick_boost));
        intent.putExtra(Intent.EXTRA_SHORTCUT_ICON, shortcutIconBm);

        return intent;
    }

    public static Intent getPendingIntentForShortcut() {
        Intent pendingIntent;
        pendingIntent = new Intent(Constant.Action.ACTION_SCAN_OVERALL);
        pendingIntent.putExtra(Constant.Extra.EXTRA_LAUNCH_SCENE, FragmentController.Scene.Clean_quick);
        pendingIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        pendingIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        return pendingIntent;
    }
}

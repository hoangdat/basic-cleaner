package com.fbon.android.app.basiccleaner.feature.storage;

import android.content.ContentResolver;
import android.content.Context;
import android.os.SystemClock;

import com.fbon.android.app.basiccleaner.feature.CleanLdrManager;
import com.fbon.android.app.basiccleaner.feature.ClearImp;
import com.fbon.android.app.basiccleaner.feature.ClearListType;
import com.fbon.android.app.basiccleaner.feature.ClearMgr;
import com.fbon.android.app.basiccleaner.model.FileInfoDataModel;
import com.fbon.android.app.basiccleaner.model.StorageInfoDataModel;
import com.fbon.android.app.basiccleaner.util.Constant;
import com.fbon.android.app.basiccleaner.util.Log;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.util.ArrayList;


public class ClearJunkImp extends ClearImp {
    public ClearJunkImp(Context context) {
        super(context);
    }

    @Override
    protected void _handleClear(ClearMgr.OnFinishedClearListener listener, ClearListType type) {
        long size = 0;
        if (isSupport(type)) {
            ArrayList<StorageInfoDataModel> lst = type.getList();
            Log.d(TAG, "_handleClear() lst = " + lst);
            if (lst != null) {
                ContentResolver resolver = mContext.getContentResolver();
                StringBuilder sb = new StringBuilder();
                boolean isFirst = true;
                int nCnt = 0;
                for (StorageInfoDataModel model : lst) {
                    if (model != null) {
                        nCnt++;
                        size += model.getSize();
                        Log.d(TAG, "_handleClear() model = " + model);
                        String path = model.getPath();
                        if (path == null) {
                            break;
                        }
                        Log.d(TAG, "_handleClear() path = " + path);
                        File delFile = new File(path);
                        Log.d(TAG, "_handleClear() file = " + delFile);
                        if (delFile != null) {
                            if (!delFile.exists() || delFile.isDirectory()) {
                                Log.d(TAG, "_handleClear() exist = " + delFile.exists());
                                continue;
                            }
                            Log.d(TAG, "_handleClear() delete file");
                            boolean c = FileUtils.deleteQuietly(delFile);
                            Log.d(TAG, "_handleClear() path = " + path + ": clear = " + c);

                            if (c && (model instanceof FileInfoDataModel)) {
                                FileInfoDataModel fileModel = (FileInfoDataModel) model;
                                int id = fileModel.getId();
                                if (id > 0) {
                                    if (!isFirst) {
                                        sb.append(',');
                                    }
                                    isFirst = false;
                                    sb.append(id);
                                }

                                if (nCnt == BULK_CNT) {
                                    //thuc hien o day
                                    Log.d(TAG, "_handleClear() sb = " + sb.toString());
                                    int row = resolver.delete(MEDIA_PROVIDER_URI, "_id in ( " + sb.toString() + ")", null);
                                    Log.e(TAG, "_handleClear() delete media Db cnt" + row);
                                    //reset
                                    sb.delete(0, sb.length() - 1);
                                    isFirst = true;
                                    nCnt = 0;
                                }
                            }
                        }
                    }
                }
                if (nCnt > 0) {
                    Log.d(TAG, "_handleClear() 2 sb = " + sb.toString());
                    int row = resolver.delete(MEDIA_PROVIDER_URI, "_id in ( " + sb.toString() + ")", null);
                    Log.e(TAG, "_handleClear() 2 delete media Db cnt" + row);
                }

                SystemClock.sleep(17);
            }
        }
        synchronized (listener) {
            listener.onFinishedClearJunk(getFlagType(type.getType()), size);
        }
    }

    private int getFlagType(int type) {
        int ret = -1;
        switch (type) {
            case CleanLdrManager.APK_JUNK_TYPE:
                ret = Constant.FLAG_CLEAR_APK;
                break;
            case CleanLdrManager.LARGE_JUNK_TYPE:
                ret = Constant.FLAG_CLEAR_LARGE;
                break;
            case CleanLdrManager.UNNECESSARY_JUNK_TYPE:
                ret = Constant.FLAG_CLEAR_UN;
                break;
        }
        return ret;
    }

    @Override
    protected boolean isSupport(ClearListType type) {
        int curType = type.getType();
        Log.d(TAG, "isSupport() - curType = " + curType);
        if (CleanLdrManager.LARGE_JUNK_TYPE == curType
         || CleanLdrManager.APK_JUNK_TYPE == curType
         || CleanLdrManager.UNNECESSARY_JUNK_TYPE == curType) {
            return true;
        }
        return false;
    }

    @Override
    protected void _clear(ClearMgr.OnFinishedClearListener listener, ClearListType type) {
        Log.d(TAG, "_clear() JUNK");
        createHandler(listener, type);
    }

    @Override
    protected String getName() {
        return "junk";
    }

    @Override
    protected int getMsgType() {
        return MSG_START_CLEAR_JUNK;
    }
}

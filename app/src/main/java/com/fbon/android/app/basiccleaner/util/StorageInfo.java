package com.fbon.android.app.basiccleaner.util;


import android.content.Context;
import android.os.Environment;
import android.os.StatFs;

import java.io.File;

public class StorageInfo {
    private static final String TAG = "StorageInfo";
    private static final long SYSTEM_MEMORY_UNIT_GB = 1L * 1024L * 1024L * 1024L;

    public interface iStorageType {
        int UNKNOWN = -1;
        int INTERNAL = 0;
        int EXTERNAL_SD = 1;
        int EXTERNAL_PRIVATE_MODE = 2;
        int EXTERNAL_USB_DRIVE_A = 3;
        int EXTERNAL_USB_DRIVE_B = 4;
        int EXTERNAL_USB_DRIVE_C = 5;
        int EXTERNAL_USB_DRIVE_D = 6;
        int EXTERNAL_USB_DRIVE_E = 7;
        int EXTERNAL_USB_DRIVE_F = 8;
    }

    private String ROOT = "/storage";

    // below is usually hided to user.
    private String EMULATED_ENTRY_FOLDER = "/storage/emulated";
    public static String INTERNAL_ROOT = Environment.getExternalStorageDirectory().getAbsolutePath();

    public static String getStoragePath(Context context, int storageType) {
        String path = null;
        switch (storageType) {
            case iStorageType.INTERNAL:
                path = INTERNAL_ROOT;
                break;
            default:
                break;
        }
        return path;
    }

    public static long getStorageSize(Context context, int storageType) {
        long capacity = 0;
        String path = getStoragePath(context, storageType);
        if (path != null) {
            File file = new File(path);
            capacity = file.getTotalSpace();
        }

        if (storageType == iStorageType.INTERNAL) {
            return correctionStorageSize(capacity);
        }
        return capacity;
    }

    private static long correctionStorageSize(long totalSize) {
        int power = 2; // The power for the minimum total space. This value can be different as per the system.
        long tempTotalSize = 0;
        //long mRealTotalSize = Settings.System.getLong(mContext.getContentResolver(), "storage_mmc_size", SYSTEM_MEMORY_REAL_SIZE);
        StatFs statFs = new StatFs(Environment.getRootDirectory().getPath());
        long realTotalSize = totalSize + statFs.getTotalBytes();

        while (true) {
            tempTotalSize = SYSTEM_MEMORY_UNIT_GB * (long) Math.pow(2, power);
            if (realTotalSize <= tempTotalSize) {
                break;
            } else {
                power++;
            }
        }
        return tempTotalSize;
    }

    public static long getStorageFreeSpace(Context context, int storageType) {
        long capacity = 0;
        if (storageType == iStorageType.INTERNAL) {
            capacity = getDeviceStorageFreeSpace();
        } else {
            String path = getStoragePath(context, storageType);
            StatFs stat = getStatFS(path);
            if (stat != null) {
                capacity = stat.getAvailableBytes();
            } else {
                capacity = -1L;
            }
        }
        return capacity;
    }

    public static long getDeviceStorageFreeSpace() {
        long capacity = 0;
        // "/data" path is real partition of "/storage/emulated/0".
        // "/data" is larger than "/storage/emulated/0" about 20MB because
        // memory full issue.
        String path = "/data";
        StatFs stat = getStatFS(path);
        if (stat != null) {
            capacity = stat.getAvailableBytes();
        } else {
            capacity = -1L;
        }
        return capacity;
    }

    private static StatFs getStatFS(String path) {
        StatFs stat = null;
        if (path != null)
            try {
                stat = new StatFs(path);
            } catch (Exception e) {
                Log.e(TAG, "path " + path);
            }
        return stat;
    }
}

package com.fbon.android.app.basiccleaner.fragment;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.transition.Slide;
import android.view.Gravity;

import com.fbon.android.app.basiccleaner.R;
import com.fbon.android.app.basiccleaner.util.Log;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Stack;



public class FragmentController {
    public static final String TAG = "FragmentCtrl";

    private static FragmentController mInstance = null;
    private Activity mActivity = null;

    private Stack<Integer> mSceneStack;
    private int mCurrentScene;

    public static AbsBasicCleanFragment createFragment(String tag) {
        Log.v(TAG, "createFragment: " + tag);
        AbsBasicCleanFragment fragment = null;

        switch (tag) {
            case OVERALL:
                fragment = new OverallFragment();
                break;
            case STORAGE:
                fragment = new StorageFragment();
                break;
            case MEMORY:
                fragment = new MemoryFragment();
                break;
            case CLEAN_ALL:
            case CLEAN_BATTERY:
            case CLEAN_MEMORY:
            case CLEAN_STORAGE:
            case CLEAN_MORE_SPACE:
                fragment = new CleanFragment();
                break;
            case CLEAN_QUICK:
                fragment = new CleanQuickFragment();
                break;
            case MORE_SPACE:
                fragment = new MoreSpaceFragment();
                Slide slideTransition = new Slide(Gravity.BOTTOM);
                slideTransition.setDuration(500);
                fragment.setEnterTransition(slideTransition);
//                fragment.setExitTransition(slideTransition);
                break;
            default:
                break;
        }

        return fragment;
    }

    public static final String OVERALL              = "overall";
    public static final String STORAGE              = "storage";
    public static final String MEMORY               = "memory";
    public static final String BATTERY              = "battery";
    public static final String CLEAN_ALL            = "clean_all";
    public static final String CLEAN_STORAGE        = "clean_storage";
    public static final String CLEAN_MEMORY         = "clean_memory";
    public static final String CLEAN_BATTERY        = "clean_battery";
    public static final String MORE_SPACE           = "more_space";
    public static final String CLEAN_MORE_SPACE     = "clean_more_space";
    public static final String CLEAN_QUICK          = "clean_quick";

    public static class Scene {
        public static final int Empty               = 0;
        public static final int Overall             = 1;
        public static final int Storage             = 2;
        public static final int Memory              = 3;
        public static final int CleanAll            = 5;
        public static final int Battery             = 4;
        public static final int Clean_storage       = 6;
        public static final int Clean_memory        = 7;
        public static final int Clean_battery       = 8;
        public static final int More_space          = 9;
        public static final int Clean_more_space    = 10;
        public static final int Clean_quick         = 11;
    }

    public interface OnSceneChangeListener {
        void onSceneChange(int scene);
    }

    private final ArrayList<WeakReference<OnSceneChangeListener>> mListener = new ArrayList<>();
    private static final HashMap<Integer, String> TABLE_TAG_SCENE = new HashMap<>();

    public static synchronized FragmentController getInstance() {
        if (mInstance == null) {
            mInstance = new FragmentController();
        }

        return mInstance;
}

    public FragmentController() {
        mCurrentScene = Scene.Empty;
        mSceneStack = new Stack<>();
        TABLE_TAG_SCENE.put(Scene.Overall, OVERALL);
        TABLE_TAG_SCENE.put(Scene.Memory, MEMORY);
        TABLE_TAG_SCENE.put(Scene.Storage, STORAGE);
        TABLE_TAG_SCENE.put(Scene.CleanAll, CLEAN_ALL);
        TABLE_TAG_SCENE.put(Scene.Battery, BATTERY);
        TABLE_TAG_SCENE.put(Scene.Clean_battery, CLEAN_BATTERY);
        TABLE_TAG_SCENE.put(Scene.Clean_memory, CLEAN_MEMORY);
        TABLE_TAG_SCENE.put(Scene.Clean_storage, CLEAN_STORAGE);
        TABLE_TAG_SCENE.put(Scene.More_space, MORE_SPACE);
        TABLE_TAG_SCENE.put(Scene.Clean_more_space, CLEAN_MORE_SPACE);
        TABLE_TAG_SCENE.put(Scene.Clean_quick, CLEAN_QUICK);
    }

    public void setContext(Activity activity) {
        mActivity = activity;
    }

    public void changeSceneWithClearPrev(int scene) {
        if (mSceneStack == null) {
            mSceneStack = new Stack<>();
        }
        mSceneStack.clear();
        changeScene(scene);
    }

    public void changeScene(int scene) {
        Log.d(TAG, "changeScene(): scene = " + scene + "-stack = " + mSceneStack);
        if (mSceneStack == null)
            mSceneStack = new Stack<>();

        int top = mSceneStack.size() > 0 ? mSceneStack.peek() : -1;
        Log.d(TAG, "changeScene(): top = " + top);
        if (top == Scene.Clean_quick) {
            mSceneStack.pop();
        }
        if (scene != top && top != Scene.CleanAll) {
            String tag = TABLE_TAG_SCENE.get(scene);
            Log.d(TAG, "changeScene(): tag = " + tag);
            if (tag != null) {
                Log.d(TAG, "changeScene(): tag = " + tag);
                AbsBasicCleanFragment fragment = createFragment(tag);
                FragmentManager fm = mActivity.getFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.all_container, fragment, tag);
                ft.commitAllowingStateLoss();

                mCurrentScene = scene;
                mSceneStack.add(scene);
                notifyObservers(scene);
            }
        }
    }

    public void changeSceneWithoutBackStack(int scene) {
        Log.d(TAG, "changeSceneWithoutBackStack(): scene = " + scene + "-stack = " + mSceneStack);
        if (mSceneStack == null)
            mSceneStack = new Stack<>();

        int top = mSceneStack.size() > 0 ? (mSceneStack.pop()) : -1;


        if (scene != top && top != Scene.CleanAll) {
            String tag = TABLE_TAG_SCENE.get(scene);
            if (tag != null) {
                AbsBasicCleanFragment fragment = createFragment(tag);
                FragmentManager fm = mActivity.getFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.all_container, fragment, tag);
                ft.commitAllowingStateLoss();

                mCurrentScene = scene;
                mSceneStack.add(scene);
                Log.d(TAG, "changeSceneWithoutBackStack(): noti stack = " + mSceneStack);
                notifyObservers(scene);
            }
        }
    }

    public int getCurrentScene() {
        return mCurrentScene;
    }

    public void registerListener(OnSceneChangeListener listener) {
        if (listener == null && containListener(listener)) {
            return;
        }

        mListener.add(new WeakReference<>(listener));
    }

    private boolean containListener(OnSceneChangeListener listener) {
        boolean ret = false;
        if (mListener == null || listener == null) {
            return ret;
        }

        for (WeakReference<OnSceneChangeListener> weakReference : mListener) {
            if (weakReference != null && weakReference.get() != null && weakReference.get().equals(listener)) {
                ret = true;
            }
        }

        return ret;
    }

    private void removeListener(OnSceneChangeListener listener) {
        if (mListener == null || listener == null) {
            return;
        }

        synchronized (mListener) {
            int size = mListener.size();
            WeakReference<OnSceneChangeListener> weakListener;
            // do not use foreach
            for (int i = size - 1; i >= 0; i--) {
                weakListener = mListener.get(i);
                if (weakListener.get() == null || weakListener.get().equals(listener)) {
                    mListener.remove(weakListener);
                }
            }
        }
    }

    public final void unregisterSceneChangeListener(OnSceneChangeListener listener) {
        if (listener != null && containListener(listener)) {
            removeListener(listener);
        }
    }

    private void unregisterAllSceneChangeListener() {
        synchronized (mListener) {
            mListener.clear();
        }
    }

    private void notifyObservers(int scene) {
        Log.d(TAG, "notifyObservers: " + scene);

        int size = mListener.size();
        WeakReference<OnSceneChangeListener> weakListener;

        for (int i = size - 1; i >= 0 ; i--) {
            weakListener = mListener.get(i);
            if (weakListener.get() == null) {
                mListener.remove(weakListener);
                continue;
            }
            Log.d(TAG, "notifyObservers: " + scene + "-of: " + weakListener.get());
            weakListener.get().onSceneChange(scene);
        }
    }

    public boolean onBackPress() {
        Log.d(TAG, "onBackPress()");
        boolean bRet = false;

        if (!isEmpty()) {
            int top = mSceneStack.peek();
            if (top != Scene.Overall && top != Scene.Clean_quick) {
                mSceneStack.pop();
                if (!mSceneStack.isEmpty()) {
                    mCurrentScene = mSceneStack.pop();
                    changeScene(mCurrentScene);
                    bRet = true;
                }
            } else {
                mSceneStack.clear();
            }
        }

        return bRet;
    }

    private boolean isEmpty() {
        if (mSceneStack != null) {
            return mSceneStack.isEmpty();
        }
        return false;
    }
}

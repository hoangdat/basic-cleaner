package com.fbon.android.app.basiccleaner.adapter;



public class ViewHolder {
    public static int index = 0;
//    public static final int GROUP_NAME = index++;
//    public static final int GROUP_SIZE = index++;
//    public static final int GROUP_PROGRESS = index++;
//    public static final int GROUP_SELECT = index++;
    public static final int NAME                             = index++;
    public static final int SIZE = index++;
    public static final int ICON = index++;
    public static final int CHECK_BOX_STUB = index++;
    public static final int CHECK_BOX = index++;
    public static final int GROUP_HEADER_STUB = index++;
    public static final int GROUP_HEADER = index++;
    public static final int GROUP_NAME = index++;
    public static final int GROUP_SIZE = index++;
    public static final int CONTENT_CONTAINER = index++;
    public static final int MAX = index;
}

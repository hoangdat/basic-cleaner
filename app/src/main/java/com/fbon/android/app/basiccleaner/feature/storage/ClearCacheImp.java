package com.fbon.android.app.basiccleaner.feature.storage;

import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.IPackageDataObserver;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.RemoteException;
import android.os.SystemClock;
import android.provider.MediaStore;

import com.fbon.android.app.basiccleaner.feature.CleanLdrManager;
import com.fbon.android.app.basiccleaner.feature.ClearImp;
import com.fbon.android.app.basiccleaner.feature.ClearListType;
import com.fbon.android.app.basiccleaner.feature.ClearMgr;
import com.fbon.android.app.basiccleaner.model.StorageInfoDataModel;
import com.fbon.android.app.basiccleaner.util.Constant;
import com.fbon.android.app.basiccleaner.util.Log;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;


public class ClearCacheImp extends ClearImp {

    public ClearCacheImp(Context context) {
        super(context);
    }

    @Override
    protected void _handleClear(ClearMgr.OnFinishedClearListener listener, ClearListType type) {
        Log.d(TAG, "_handleClear() CACHE");
        final boolean[] isDone = {false};
        long size = 0;
        if (isSupport(type)) {
            File externalCache = mContext.getExternalCacheDir();
//            Log.d(TAG, "cache dir = " + externalCache);
            if (externalCache == null) {
                return;
            }
            String myPackage = mContext.getPackageName();
//            String SD_CACHE_PATH = "/Android/data/" + myPackage + "/cache/";
//            String cache = Environment.getExternalStorageDirectory() + SD_CACHE_PATH;
//            Log.d(TAG, "cache dir 2 = " + externalCache);
            ArrayList<StorageInfoDataModel> lst = type.getList();
            if (lst != null) {
                ContentResolver resolver = mContext.getContentResolver();
                String where = MediaStore.Files.FileColumns.DATA + "=?";
                Queue<File> dir = new LinkedList<>();
                Stack<File> stackDir = new Stack<>();
                Log.d(TAG, "_handleClear() CACHE ls = " + lst);
                for (StorageInfoDataModel model : lst) {
                    if (model != null && model.getPackageName() != null) {
                        Log.d(TAG, "_handleClear() CACHE model = " + model);
                        size += model.getSize();
                        String packageDir = externalCache.getAbsolutePath().replace(myPackage, model.getPackageName());
                        File path = new File(packageDir);
                        ArrayList<Integer> lstId = new ArrayList<>();

                        if (path.exists() && path.isDirectory()) {
                            File tmpFile;
                            dir.add(path);
                            while (!dir.isEmpty() && ((tmpFile = dir.remove()) != null) && tmpFile.exists()) {
                                //get id
                                String tmpPath = tmpFile.getAbsolutePath();
//                                /storage/emulated/0/Android/data/com.fbon.android.app.basiccleaner/cache
//                                /storage/emulated/0/Android/data/com.google.android.googlequicksearchbox/cache
//                                "/storage/emulated/0/Android/data/com.sec.android.gallery3d/cache"
                                Log.d(TAG, "_handleClear() CACHE path = " + tmpPath);
//                                StringBuilder whereCondition = new StringBuilder(where);
//                                whereCondition.append(tmpPath).append("\"");
//                                Log.d(TAG, "_handleClear() CACHE whereCon = " + whereCondition.toString());
                                try (Cursor cursor = resolver.query(MEDIA_PROVIDER_URI, null, where, new String[] {tmpPath}, null)) {
                                    Log.d(TAG, "_handleClear() CACHE cursor = " + cursor);
                                    if (cursor.moveToFirst()) {
                                        int id = cursor.getInt(cursor.getColumnIndex(MediaStore.Files.FileColumns._ID));
                                        Log.d(TAG, "_handleClear() CACHE id = " + id + "-addId");
                                        lstId.add(id);
                                    } else {
                                        Log.d(TAG, "_handleClear() CACHE not move to first ");
                                    }
                                }

                                //trace this
                                File[] lstChild = tmpFile.listFiles();
                                Log.d(TAG, "_handleClear() CACHE tmp childs = " + lstChild);
                                for (int i = 0; i < lstChild.length; i++) {
                                    File tmpChild = lstChild[i];
                                    Log.d(TAG, "_handleClear() CACHE tmp child = " + tmpChild.getPath());
                                    if (tmpChild != null && tmpChild.exists()) {
                                        if (tmpChild.isDirectory()) {
                                            Log.d(TAG, "_handleClear() CACHE tmp child dir");
                                            dir.add(tmpChild);
                                        } else {
                                            Log.d(TAG, "_handleClear() CACHE tmp child file");
                                            try (Cursor cursor = resolver.query(MEDIA_PROVIDER_URI, null, where, new String[] { tmpChild.getPath()}, null)) {
                                                if (cursor.moveToFirst()) {
                                                    int id = cursor.getInt(cursor.getColumnIndex(MediaStore.Files.FileColumns._ID));
                                                    Log.d(TAG, "_handleClear() CACHE tmp child id = " + id + "-addId");
                                                    lstId.add(id);
                                                }
                                            }
                                            tmpChild.delete();
                                        }
                                    }
                                }
                                Log.d(TAG, "_handleClear() CACHE push stack tmpFile " + tmpFile);
                                stackDir.push(tmpFile);
                            }

                            //delete file trong stack
                            while (!stackDir.isEmpty() && (tmpFile = stackDir.pop()) != null) {
                                FileUtils.deleteQuietly(tmpFile);
                            }

                            //delete file trong db
                            int start = 0;
                            int offset = BULK_CNT;
                            int idCnt = lstId.size();
                            if (offset > idCnt) {
                                offset = idCnt;
                            }
                            int remain = idCnt - offset;
                            boolean isFirst = true;
                            StringBuilder sb = new StringBuilder();
                            while (remain >= 0 && idCnt != 0) {
//                                Log.d(TAG, "_handleClear() CACHE - start offset  = " + start + ", " + offset);
                                for (int i = start; i < offset; i++) {
                                    if (!isFirst) {
                                        sb.append(',');
                                    }
                                    isFirst = false;
                                    sb.append(lstId.get(i));
                                }
//                                Log.d(TAG, "_handleClear() CACHE - delete sb = " + sb.toString());
                                int row = resolver.delete(MEDIA_PROVIDER_URI, "_id in ( " + sb.toString() + ")", null);
//                                Log.d(TAG, "_handleClear() CACHE - delete row = " + row);

                                start = offset;
                                offset = offset + BULK_CNT;
                                if (offset > idCnt) {
                                    offset = idCnt;
                                }
                                remain = idCnt - offset;
                                if (sb.length() > 1) {
                                    sb.delete(0, sb.length() - 1);
                                }
                                isFirst = true;
                            }
                            SystemClock.sleep(17);
                        }
                    }
                }
            }

            try {
                PackageManager pm = mContext.getPackageManager();
                Method freeStorageAndNotify = pm.getClass()
                        .getMethod("freeStorageAndNotify", long.class, IPackageDataObserver.class);
                long freeStorageSize = Long.MAX_VALUE;

                freeStorageAndNotify.invoke(pm, freeStorageSize, new IPackageDataObserver.Stub() {
                    @Override
                    public void onRemoveCompleted(String packageName, boolean succeeded) throws RemoteException {
                        Log.d(TAG, "_handleClear() CACHE - set isDone");
                        isDone[0] = true;
                    }
                });
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        } else {
            isDone[0] = true;
        }
        Log.d(TAG, "_handleClear() CACHE - isDone = " + isDone[0]);
        int nCnt = 0;
        while (!isDone[0] && nCnt < 10) {
            try {
                Thread.sleep(200);
                nCnt++;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        synchronized (listener) {
            listener.onFinishedClearJunk(Constant.FLAG_CLEAR_CACHE, size);
        }
    }

    @Override
    protected boolean isSupport(ClearListType type) {
        if (CleanLdrManager.CACHE_JUNK_TYPE == type.getType()) {
            return true;
        }
        return false;
    }

    @Override
    protected void _clear(ClearMgr.OnFinishedClearListener listener, ClearListType type) {
        Log.d(TAG, "_clear() CACHE");
        createHandler(listener, type);
    }

    @Override
    protected String getName() {
        return "CCache";
    }

    @Override
    protected int getMsgType() {
        return MSG_START_CLEAR_CACHE;
    }
}

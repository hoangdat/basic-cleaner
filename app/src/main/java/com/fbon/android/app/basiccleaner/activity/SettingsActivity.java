package com.fbon.android.app.basiccleaner.activity;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewStub;
import android.widget.TextView;

import com.fbon.android.app.basiccleaner.R;
import com.fbon.android.app.basiccleaner.dialog.SettingsLargeFile;
import com.fbon.android.app.basiccleaner.listener.ListenerMgr;
import com.fbon.android.app.basiccleaner.listener.SettingsRefreshReceiver;
import com.fbon.android.app.basiccleaner.util.Constant;
import com.fbon.android.app.basiccleaner.util.Log;
import com.fbon.android.app.basiccleaner.util.PreferenceUtils;
import com.fbon.android.app.basiccleaner.util.UiUtils;
import com.startapp.android.publish.adsCommon.StartAppAd;
import com.startapp.android.publish.adsCommon.StartAppSDK;

import java.util.ArrayList;

public class SettingsActivity extends Activity {
    private static final String TAG = "setting";
    private ArrayList<Integer> mMapMoreSpaceDetail;
    private ListenerMgr mListenerMgr;
    private StartAppAd startAppAd = new StartAppAd(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StartAppSDK.init(this, Constant.AdvertisementConstant.AppId, false);
        StartAppAd.disableSplash();
        if (getActionBar() != null) {
            getActionBar().setDisplayOptions(ActionBar.DISPLAY_HOME_AS_UP, ActionBar.DISPLAY_HOME_AS_UP);
            getActionBar().setDisplayHomeAsUpEnabled(true);
            getActionBar().show();
        }
        mListenerMgr = new ListenerMgr();
        mListenerMgr.addListener(new SettingsRefreshReceiver(this, ListenerMgr.LifeCycle.CREATE, ListenerMgr.LifeCycle.DESTROY));
        mListenerMgr.notifyCreate();
        setContentView(R.layout.activity_settings);
        initDetailViewMSSettings(R.id.settings_large_files);
    }

    @Override
    protected void onResume() {
        Log.d(TAG, "onResume()");
        super.onResume();
        startAppAd.onResume();
        setAboutPage();
        ensureMoreSpaceView();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        Log.i(TAG, "onOptionsItemSelected: " + id);
        switch (id) {
            case android.R.id.home:
                this.finish();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void ensureMoreSpaceView() {
        Log.d(TAG, "ensureMoreSpaceView()");
        View moreSpaceView;
        ViewStub moreSpaceStub = (ViewStub) findViewById(R.id.more_space_settings_stub);

        if (moreSpaceStub != null) {
            moreSpaceView = moreSpaceStub.inflate();
        } else {
            moreSpaceView = findViewById(R.id.more_space_settings);
        }
        int msSettingCnt = mMapMoreSpaceDetail.size();
        Resources res = getResources();
        String size;
        for (int i = 0; i < msSettingCnt; i++) {
            int id = mMapMoreSpaceDetail.get(i);
            switch (id) {
                case R.id.settings_large_files:
                    size = UiUtils.makeFileSizeString(this, PreferenceUtils.getLargeFilesSize(this));
                    setMSSettingsDetails(moreSpaceView, id, res.getString(R.string.large_files_is_more_than), size, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            showMSSettings();
                        }
                    });
                    break;
                default:
                    break;
            }
        }
    }

    private void setMSSettingsDetails(View root, int detailView, String firstContent, String secondContent, View.OnClickListener clickListener) {
        Log.d(TAG, "setMSSettingsDetails() - detailV = " + detailView);
        View view = root.findViewById(detailView);
        if (view != null) {
            view.setOnClickListener(clickListener);
            TextView txt1 = (TextView) view.findViewById(R.id.more_space_settings_list_item_text);
            if (txt1 != null) {
                txt1.setText(firstContent);
            }

            TextView txt2 = (TextView) view.findViewById(R.id.more_space_settings_list_item_text_second);
            if (txt2 != null) {
                txt2.setText(secondContent);
            }
        }
    }

    private void initDetailViewMSSettings(int detailViewID) {
        if (mMapMoreSpaceDetail == null) {
            mMapMoreSpaceDetail = new ArrayList<>();
        }

        if (detailViewID > 0) {
            mMapMoreSpaceDetail.add(detailViewID);
        }
    }

    private void setAboutPage() {
        View aboutContainer = findViewById(R.id.about_container);
        aboutContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "enterAboutPage()");
                enterAbout();
            }
        });
    }

    private void enterAbout() {
        Intent intent = new Intent(this, AboutActivity.class);
        startActivity(intent);
    }

    private void showMSSettings() {
        SettingsLargeFile dialog = new SettingsLargeFile();
        FragmentManager fragMan = getFragmentManager();
        if (fragMan != null) {
            FragmentTransaction ft = fragMan.beginTransaction();
            Fragment prev = fragMan.findFragmentByTag(SettingsLargeFile.NAME);
            if (prev != null) {
                ft.remove(prev);
            }
            if (ft != null) {
                ft.add(dialog, SettingsLargeFile.NAME);
                ft.commitAllowingStateLoss();
            }
        }
    }

    @Override
    protected void onDestroy() {
        mListenerMgr.notifyDestroy();
        super.onDestroy();
    }
}

package com.fbon.android.app.basiccleaner.model;

import com.fbon.android.app.basiccleaner.feature.CleanLdrManager;

import java.util.ArrayList;

public class AppInfoDataModel extends StorageInfoDataModel{
    public AppInfoDataModel(String packageName) {
        super(CleanLdrManager.APK_JUNK_TYPE);
        setPackageName(packageName);
    }

    private int mUid;
    private double mBatteryUsage;
    private float mCpuUsage;
    private long mMemUsage;
    private ArrayList<String> mProcessNameList;
    private ArrayList<Integer> mPidList;

    public double getBatteryUsage() {
        return mBatteryUsage;
    }

    public void setBatteryUsage(double mBatteryUsage) {
        this.mBatteryUsage = mBatteryUsage;
    }

    public float getCpuUsage() {
        return mCpuUsage;
    }

    public void setCpuUsage(float mCpuUsage) {
        this.mCpuUsage = mCpuUsage;
    }

    public long getMemUsage() {
        return mMemUsage;
    }

    public void setMemUsage(long mMemUsage) {
        this.mMemUsage = mMemUsage;
    }

    public ArrayList<String> getProcessNameList() {
        return mProcessNameList;
    }

    public void addProcessName(String processName) {
        if (mProcessNameList == null) {
            mProcessNameList = new ArrayList<>();
        }
        if (!mProcessNameList.contains(processName)) {
            mProcessNameList.add(processName);
        }
    }

    public ArrayList<Integer> getPidList() {
        return mPidList;
    }

    public void addPid(int pid) {
        if (mPidList == null) {
            mPidList = new ArrayList<>();
        }
        if (!mPidList.contains(pid)) {
            mPidList.add(pid);
        }
    }

    public void setPidList(ArrayList<Integer> mPidList) {
        this.mPidList = mPidList;
    }

    public int getUid() {

        return mUid;
    }

    public void setUid(int mUid) {
        this.mUid = mUid;
    }
}

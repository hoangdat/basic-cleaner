package com.fbon.android.app.basiccleaner.activity;

import android.app.ActionBar;
import android.app.Activity;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.fbon.android.app.basiccleaner.R;
import com.fbon.android.app.basiccleaner.util.Constant;
import com.fbon.android.app.basiccleaner.util.Log;
import com.startapp.android.publish.adsCommon.StartAppAd;
import com.startapp.android.publish.adsCommon.StartAppSDK;

public class AboutActivity extends Activity {
    private static final String TAG = "AboutAct";
    private TextView mTxtVersion;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StartAppSDK.init(this, Constant.AdvertisementConstant.AppId, false);
        StartAppAd.disableSplash();
        ActionBar actionbar = getActionBar();
        if (actionbar != null) {
            actionbar.setDisplayOptions(ActionBar.DISPLAY_HOME_AS_UP, ActionBar.DISPLAY_HOME_AS_UP);
            actionbar.setDisplayHomeAsUpEnabled(true);
            actionbar.show();
        }
        setContentView(R.layout.activity_about);
        initView();
    }

    private void initView() {
        mTxtVersion = (TextView) findViewById(R.id.about_app_version);
        String version = getApkVersionName();
        if (version != null) {
            mTxtVersion.setVisibility(View.VISIBLE);
            mTxtVersion.setText(String.format(getString(R.string.version), version));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                this.finish();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    //To do: add more information, advertisement, more app link
    private String getApkVersionName() {
        try {
            PackageInfo localPackageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            Log.d(TAG, "versionName : " + localPackageInfo.versionName);
            return localPackageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(TAG, "PackageManager.NameNotFoundException : " + e);
        }
        return null;
    }
}

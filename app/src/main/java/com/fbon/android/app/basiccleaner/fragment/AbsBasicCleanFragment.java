package com.fbon.android.app.basiccleaner.fragment;


import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;

import com.fbon.android.app.basiccleaner.R;
import com.fbon.android.app.basiccleaner.uicore.BasicCleanerObservable;
import com.fbon.android.app.basiccleaner.util.Log;


public abstract class AbsBasicCleanFragment extends Fragment {
    public static final String TAG = "rootFrag";
    private static BasicCleanerObservable mObservable = BasicCleanerObservable.getInstance();

    public abstract int getLayoutId();
    public abstract void initView(View view);

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Context context = getActivity();
        context.setTheme(R.style.BasicClean_Default);
    }

    protected void postEventDelayed(int data, long delayedTime) {
        Log.i(TAG, "postEventDelayed : data = " + data + ", delayedTime = " + delayedTime);
        mEventHandler.sendEmptyMessageDelayed(data, delayedTime);
    }

    protected void postEvent(int data) {
        Log.i(TAG, "postEvent : data = " + data);
        mEventHandler.sendEmptyMessage(data);
    }

    protected boolean hasPostEvent(int data) {
        Log.i(TAG, "hasPostEvent : data = " + data);
        return mEventHandler.hasMessages(data);
    }

    private static Handler mEventHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            Log.i(TAG, "handleMessage: " + msg.what);
            mObservable.notifyObservers(msg.what);
            return false;
        }
    });

    public abstract void scan();
    public abstract void updateResultView(int scanType, long prevScore, boolean isPercent);

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}

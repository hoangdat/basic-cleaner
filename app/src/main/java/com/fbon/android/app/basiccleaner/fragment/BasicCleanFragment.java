package com.fbon.android.app.basiccleaner.fragment;

import android.animation.Animator;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.fbon.android.app.basiccleaner.R;
import com.fbon.android.app.basiccleaner.feature.CleanLdrManager;
import com.fbon.android.app.basiccleaner.model.StorageInfoDataModel;
import com.fbon.android.app.basiccleaner.util.Constant;
import com.fbon.android.app.basiccleaner.util.Log;
import com.fbon.android.app.basiccleaner.widget.CircleView;
import com.fbon.android.app.basiccleaner.widget.ScanningWaveCircle;


public class BasicCleanFragment extends AbsBasicCleanFragment implements View.OnClickListener, CleanLdrManager.OnJunkLoaderListener, CircleView.OnFinishedSetScore{
    private static final String TAG = "basicFrg";
    protected TextView mTxtUsed, mTxtTotal;
    protected CircleView mStatusCircle;
    protected ScanningWaveCircle mScanningCircle;
    protected long mTotalSize;
    private int mScanType;
    protected Button mBtnFixNow;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CleanLdrManager.getInstance(getActivity()).registerListener(this);
    }

    @Override
    public int getLayoutId() {
        return 0;
    }

    @Override
    public void initView(View view) {

    }

    @Override
    public void onStart() {
        Log.d(TAG, "onStart");
        super.onStart();
        setScan(true);
    }

    @Override
    public void scan() {
        if (sendScanRequest()) {
            updateResultView(Constant.FLAG_NONE, 0, false);
        } else {
            updateResultView(firstScanType(), 0, true);
        }
    }

    public boolean sendScanRequest() {
        return false;
    }

    @Override
    public void updateResultView(int scanType, long prevScore, boolean isPercent) {
    }

    public void initCommonView(View view) {
        mScanningCircle = (ScanningWaveCircle) view.findViewById(R.id.circle_scan);
        mStatusCircle = (CircleView) view.findViewById(R.id.circle_status);
        mStatusCircle.setFinishedScore(this);

        mTxtUsed      = (TextView) view.findViewById(R.id.used_text);
        mTxtTotal     = (TextView) view.findViewById(R.id.total_text);

        mBtnFixNow = (Button) view.findViewById(R.id.fix_now);
        mBtnFixNow.setOnClickListener(this);
    }

    public void setScan(boolean enable) {
        setScanType(firstScanType());
        setScanningEnable(enable, 0);
        scan();
    }

    public void setScanType(int scanType) {
        mScanType = scanType;
    }

    public int getScanType() {
        return mScanType;
    }

    public int firstScanType() {
        return 0;
    }

    public void setScanningEnable(boolean isScanning, int score) {
        mStatusCircle.setVisibility(isScanning ? View.GONE : View.VISIBLE);
        mScanningCircle.setVisibility(isScanning ? View.VISIBLE : View.GONE);
        if (isScanning) {
            mScanningCircle.start();
        } else {
            mScanningCircle.stop();
            mStatusCircle.setScore(score, getActivity().getResources().getColor(R.color.white),
                    true, CircleView.SCANNING_DURATION);
        }
    }


    @Override
    public void onClick(View v) {
        Log.d(TAG, "onClick() v = " + v);
        _onClick(v);
    }

    public void _onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.fix_now:
                Log.d(TAG, "fix_now + " + getFixNowEvent());
                postEvent(getFixNowEvent());
                break;
            case R.id.btn_usage:
                postEvent(getShowUsageEvent());
                break;
            default:
                break;
        }
    }

    public int getShowUsageEvent() {
        return 0;
    }

    public int getFixNowEvent() {
        return 0;
    }

    public void updateFixNowButton(boolean isEnable, int resId, String addInfo) {
        if (mBtnFixNow != null) {
            if (addInfo != null) {
                mBtnFixNow.setText(getActivity().getString(resId) + " (+" + addInfo + ")");
            } else {
                mBtnFixNow.setText(resId);
            }
            mBtnFixNow.setAlpha(isEnable ? 1f : 0.4f);
            mBtnFixNow.setEnabled(isEnable);
        }
    }

    @Override
    public void onJunkLoaderProgress(int scanType, int junkType, int progress, StorageInfoDataModel model) {
        _onJunkLoaderProgress(scanType, junkType, progress, model);
    }

    @Override
    public void onScanFinished(int scanType) {
        _onScanFinished(scanType);
    }

    protected void _onJunkLoaderProgress(int scanType, int junkType, int progress, StorageInfoDataModel model) {
    }

    protected void _onScanFinished(int scanType) {
    }

    @Override
    public void onDestroy() {
        CleanLdrManager.getInstance(getActivity()).unregisterListener(this);
        super.onDestroy();
    }

    @Override
    public void onFinishedScore(Animator animator) {
        _onFinishedScore(animator);
    }

    protected void _onFinishedScore(Animator animator) {
    }
}

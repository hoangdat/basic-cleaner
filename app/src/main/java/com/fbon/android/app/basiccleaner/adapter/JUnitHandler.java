package com.fbon.android.app.basiccleaner.adapter;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

public class JUnitHandler<T> extends Handler {
    static boolean mIsJUnitMode = false;

    public JUnitHandler() {
        super();
    }

    public JUnitHandler(Looper looper) {
        super(looper);
    }

    public static void setJUnit() {
        mIsJUnitMode = true;
    }
    public static void unsetJUnit() {
        mIsJUnitMode = false;
    }

    @Override
    public boolean sendMessageAtTime(Message msg, long uptimeMillis) {
        boolean bRet = true;
        if (mIsJUnitMode) {
            handleMessage(msg);
        } else {
            bRet = super.sendMessageAtTime(msg, uptimeMillis);
        }
        return bRet;
    }

    @SuppressWarnings("unchecked")
    protected T getMsgValue(Object obj) {
        T ret = (T) obj;
        return ret;
    }
}

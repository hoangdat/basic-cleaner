package com.fbon.android.app.basiccleaner.feature;

import android.content.Context;
import android.net.Uri;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.provider.MediaStore;

import com.fbon.android.app.basiccleaner.adapter.JUnitHandler;
import com.fbon.android.app.basiccleaner.util.Chain;
import com.fbon.android.app.basiccleaner.util.Log;


public abstract class ClearImp implements Chain<ClearImp> {
    public static final String TAG = "ClearImp";
    public static final int BULK_CNT = 128;
    public static final Uri MEDIA_PROVIDER_URI = MediaStore.Files.getContentUri("external");
    protected Context mContext;
    private ClearImp mNext;
    protected HandlerThread mHandlerThread;
    protected static Handler mUIHandler = new UIClearUpdateHandler();
    protected ClearHandler mClearHandler;

    public static final int MSG_START_CLEAR_CACHE                = 1;
    public static final int MSG_START_CLEAR_JUNK                 = 2;
    public static final int MSG_START_CLEAR_APP_MEM              = 3;

    public ClearImp(Context context) {
        mContext = context;
        mHandlerThread = new HandlerThread("Thread-" + getName());
    }

    @Override
    public void setNext(ClearImp next) {
        mNext = next;
    }

    public void clear(ClearMgr.OnFinishedClearListener listener, ClearListType type) {
        if (type != null) {
            if (isSupport(type)) {
                _clear(listener, type);
            } else if (mNext != null) {
                mNext.clear(listener, type);
            }
        }
    }

    public void handleClear(ClearMgr.OnFinishedClearListener listener, ClearListType type) {
        Log.d(TAG, "handleClear()");
        if (type != null) {
            _handleClear(listener, type);
        }
    }

    protected abstract void _handleClear(ClearMgr.OnFinishedClearListener listener, ClearListType type);

    protected abstract boolean isSupport(ClearListType type);

    protected abstract void _clear(ClearMgr.OnFinishedClearListener listener, ClearListType type);

    protected abstract String getName();

    protected abstract int getMsgType();

    public void createHandler(ClearMgr.OnFinishedClearListener listener, ClearListType type) {
        if (mHandlerThread != null && !mHandlerThread.isAlive()) {
            mHandlerThread.start();
        }
        mClearHandler = new ClearHandler(mHandlerThread.getLooper());

        ClearReqInfo info = new ClearReqInfo(listener, type);
        int msgType = getMsgType();

        mClearHandler.removeMessages(msgType);
        mClearHandler.sendMessage(mClearHandler.obtainMessage(msgType, info));
    }

    protected final class ClearHandler extends JUnitHandler<ClearReqInfo> {
        public ClearHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            ClearReqInfo info = getMsgValue(msg.obj);
            handleClear(info.mListener, info.mType);
        }
    }

    private static class UIClearUpdateHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
        }
    }

    public static class ClearReqInfo {
        ClearMgr.OnFinishedClearListener mListener;
        ClearListType mType;

        public ClearReqInfo(ClearMgr.OnFinishedClearListener listener, ClearListType type) {
            mListener = listener;
            mType = type;
        }
    }
}

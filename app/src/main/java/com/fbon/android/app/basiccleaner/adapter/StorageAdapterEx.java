package com.fbon.android.app.basiccleaner.adapter;

import android.content.Context;
import android.util.SparseArray;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.fbon.android.app.basiccleaner.R;
import com.fbon.android.app.basiccleaner.model.StorageInfoDataModel;
import com.fbon.android.app.basiccleaner.thumbnail.ThumbnailMgr;
import com.fbon.android.app.basiccleaner.util.Log;
import com.fbon.android.app.basiccleaner.util.UiUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;



public class StorageAdapterEx extends BaseAdapter {
    private static final String TAG = "SAdapterEx";

    private static final int TYPE_GROUP = 0;
    private static final int TYPE_ITEM = 1;
    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private List<GroupItem> mGroupItems;
    private SparseArray<Integer> mGroupSet = new SparseArray<>();
    protected boolean mCheckMode;
    private boolean mSelectedAll;
    private ArrayList<Integer> mSelectedList = new ArrayList<>();
    private int mLayoutId;
    private OnItemCheckChangedListener mOnItemCheckChangedListener;
    private HashMap<Integer, List<StorageInfoDataModel>> mMapSelectedModels;

    public StorageAdapterEx(Context context, int layoutId, List<GroupItem> lstGroup) {
        super();
        mContext = context;
        mLayoutInflater = LayoutInflater.from(mContext);
        mGroupItems = lstGroup;
        mLayoutId = layoutId;
        mMapSelectedModels = new HashMap<>();
        updateGroupSet();
    }

    public interface OnItemCheckChangedListener {
        void onItemCheckChanged();
    }

    public void setAllSelected() {
        setCheckMode(true);
        mSelectedAll = true;
    }

    private void setCheckedListForAll() {
        Log.d(TAG, "setCheckedListForAll() - selectAl = " + mSelectedAll);
        if (mSelectedAll == true) {
            int itemCount = getCount();
            Log.d(TAG, "setCheckedListForAll() - itemCount = " + itemCount);
            for (int i = 0; i < itemCount; i++) {
                if (isTwoType() && isGroupHeader(i)) {
                    Log.d(TAG, "setCheckedListForAll() - isGroup + i = " + i);
                    continue;
                }
                Log.d(TAG, "setCheckedListForAll() - addList i = " + i);
                mSelectedList.add(i);
            }
            getSelectedSizeGroup();
        }
    }

    public void registerItemCheckChange(OnItemCheckChangedListener listener) {
        mOnItemCheckChangedListener = listener;
    }

    public void notifyItemCheckChange() {
        if (mOnItemCheckChangedListener != null) {
            mOnItemCheckChangedListener.onItemCheckChanged();
        }
    }

    private boolean isSelectedAll() {
        return mSelectedAll;
    }

    public void setSelectAll(boolean isAll) {
        mSelectedAll = isAll;
        notifyItemCheckChange();
    }

    public void setSelected(int position) {
        Log.d(TAG, "setSelected() - pos = " + position);
        if (isTwoType() && isGroupHeader(position)) {
            return;
        }
        Log.d(TAG, "setSelected() - not Group header");
        if (isSelected(position)) {
            Log.d(TAG, "setSelected() - isSelect need remove = " + position);
            mSelectedList.remove(new Integer(position));
            mSelectedAll = false;
        } else {
            mSelectedList.add(position);
        }
        notifyDataSetChanged();
        notifyItemCheckChange();
    }

    private boolean isSelected(int position) {
        boolean ret = false;
        if (mSelectedList != null && mSelectedList.contains(position)) {
            ret = true;
        }
        return ret;
    }

    public void setCheckMode(boolean checkMode) {
        this.mCheckMode = checkMode;
    }

    public boolean isCheckMode() {
        return mCheckMode;
    }

    @Override
    public int getItemViewType(int position) {
        if (isTwoType() && isGroupHeader(position)) {
                return TYPE_GROUP;
        }
        return TYPE_ITEM;
    }

    @Override
    public int getViewTypeCount() {
        if (isTwoType()) {
            return 2;
        }
        return 1;
    }

    @Override
    public int getCount() {
        int total = 0;
        int addGroupHeader = 0;
        if (isTwoType()) {
            addGroupHeader = 1;
        }

        if (mGroupItems != null) {
            for (GroupItem item : mGroupItems) {
                total += ((item != null && item.getChildList() != null)? item.getChildList().size() : 0) + addGroupHeader;
            }
        }

        return total;
    }

    @Override
    public Object getItem(int position) {
        Object ret = null;
        if (isTwoType()) {
            int groupIndex = getGroupIndex(position);
            if (isGroupHeader(position)) {
                ret = mGroupItems.get(groupIndex);
            }

            int groupPos = getGroupPosInList(groupIndex);
            if (groupPos != -1 || position < (groupPos + 1)) {
                ret =  mGroupItems.get(groupIndex).getChildList().get(position - groupPos - 1);
            }
        } else {
            if (mGroupItems != null && mGroupItems.get(0) != null) {
                if (mGroupItems.get(0).getChildList() != null) {
                    ret = mGroupItems.get(0).getChildList().get(position);
                }
            }
        }
        return ret;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Log.d(TAG, "getView pos = " + position);
        View ret = null;
        if (isTwoType()) {
//            Log.d(TAG, "getView pos = " + position + "-2 type");
            int groupIndex = getGroupIndex(position);
            if (isGroupHeader(position)) {
//                Log.d(TAG, "getView pos = " + position + "-group header");
                return getHeaderView(groupIndex, convertView, parent);
            }

            int groupPos = getGroupPosInList(groupIndex);
//            Log.d(TAG, "getView pos = " + position + "-show group pos = " + groupPos);
            if (groupPos != -1 || position < (groupPos + 1)) {
//                Log.d(TAG, "getView pos = " + position + "-get childView");
                ret =  getChildView(position, groupPos, groupIndex, convertView, parent);
            }
            if (ret != null) {
                showGroupHeader((ListViewHolder) ret.getTag(), false);
            }
        } else {
            ret = getChildView(position, 0, 0 , convertView, parent);
        }
        return ret;
    }

    private View newView(View view, ViewGroup parent) {
        ListViewHolder holder;
        view = mLayoutInflater.inflate(mLayoutId, parent, false);
        holder = createViewHolder(view);
        view.setTag(holder);

        return view;
    }

    private View getHeaderView(int groupIndex, View convertView, ViewGroup parent) {
        View group = convertView;
        if (group == null) {
            group = newView(group, parent);
        }

        ListViewHolder holder = (ListViewHolder) group.getTag();
        View groupHeaderContainer = holder.getView(ViewHolder.GROUP_HEADER, View.class);
        if (groupHeaderContainer == null) {
            groupHeaderContainer = ensureViewFromStub(holder, group, ViewHolder.GROUP_HEADER_STUB,
                    ViewHolder.GROUP_HEADER, R.id.group_header_container);
        }
        showGroupHeader(holder, true);

        if (groupHeaderContainer != null) {
            _newGroupHeaderView(holder, groupHeaderContainer);
            _bindGroupView(holder, groupHeaderContainer, groupIndex);
        }

        return group;
    }

    private void _newGroupHeaderView(ListViewHolder vh, View view) {
        vh.addView(ViewHolder.GROUP_NAME, view.findViewById(R.id.item_name));
        vh.addView(ViewHolder.GROUP_SIZE, view.findViewById(R.id.item_size));
    }

    private void showGroupHeader(ListViewHolder holder, boolean isShow) {
        if (isTwoType() && holder != null) {
            View contentContainer = holder.getView(ViewHolder.CONTENT_CONTAINER);
            View groupContainer = holder.getView(ViewHolder.GROUP_HEADER);
            if (contentContainer != null && groupContainer != null) {
                if (isShow) {
                    contentContainer.setVisibility(View.GONE);
                    groupContainer.setVisibility(View.VISIBLE);
                } else {
                    contentContainer.setVisibility(View.VISIBLE);
                    groupContainer.setVisibility(View.GONE);
                }
            }
        }
    }


    private View getChildView(int position, int groupPosition, int groupIndex, View convertView, ViewGroup parent) {
        View child = convertView;
        if (child == null) {
            child = newView(child, parent);
        }

        ListViewHolder holder = (ListViewHolder) child.getTag();
        _bindChildView(holder, child, groupIndex, groupPosition, position);
        return child;
    }

    private void _bindGroupView(ListViewHolder holder, View view, int groupIndex) {
        GroupItem groupItem = mGroupItems.get(groupIndex);
        if (groupItem == null) {
            return;
        }

        TextView name = holder.getView(ViewHolder.GROUP_NAME, TextView.class);
        name.setText(groupItem.getName());

        TextView size = holder.getView(ViewHolder.GROUP_SIZE, TextView.class);
        size.setText(UiUtils.makeFileSizeString(mContext, groupItem.getSize()));
    }

    private void _bindChildView(ListViewHolder holder, View child, int groupIndex, int groupPosition, int positionInList) {
        GroupItem group = mGroupItems.get(groupIndex);
        if (group == null || group.getChildList() == null) {
            return;
        }
        Log.d(TAG, "_bindChildView() pos = " + positionInList + "-groupPos = " + groupPosition + "-grIndex = " + groupIndex);
        StorageInfoDataModel childItem = group.getChildList().get(isTwoType() ? (positionInList - groupPosition - 1) : positionInList);
        if (childItem != null) {
            ImageView icon = holder.getView(ViewHolder.ICON, ImageView.class);
            ImageView view = ThumbnailMgr.getInstance(mContext).loadThumbnail(childItem.getPath(), childItem.getPackageName(), icon);
            if (view == null) {
                icon.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                icon.setBackground(mContext.getDrawable(R.drawable.item_button_bg));
                icon.setImageDrawable(UiUtils.getFileTypeDrawable(mContext, UiUtils.getExtensionAsUpperCase(childItem.getPath())));
                icon.setColorFilter(mContext.getResources().getColor(R.color.fix_now_good_bg_color));
            } else {
                icon.setBackground(null);
                icon.setColorFilter(null);
            }

            TextView name = holder.getView(ViewHolder.NAME, TextView.class);
            name.setText(childItem.getName());

            TextView size = holder.getView(ViewHolder.SIZE, TextView.class);
            size.setText(UiUtils.makeFileSizeString(mContext, childItem.getSize()));

            _bindCheckView(holder, child, positionInList);
        }
    }

    private void _bindCheckView(ListViewHolder holder, View child, int positionInList) {
        if (holder.getView(ViewHolder.CHECK_BOX) != null) {
            Log.d(TAG, "_bindCheckView() - pos = " + positionInList + "- isSelect = " + isSelected(positionInList) + "-selectedAll = " + isSelectedAll());
            boolean isSelect = isSelected(positionInList) || isSelectedAll();
            Log.d(TAG, "_bindCheckView() - isSelect = " + isSelect);
            showCheckBox(holder, child, mCheckMode, isSelect);
        }
    }

    private void showCheckBox(ListViewHolder holder, View child, boolean show, boolean isSelected) {
        CheckBox box = holder.getView(ViewHolder.CHECK_BOX, CheckBox.class);

        if (box != null) {
            box.setVisibility(show ? View.VISIBLE : View.GONE);
            if (show) {
                box.setChecked(isSelected);
            }
        }
    }

    private ListViewHolder createViewHolder(View v) {
        ListViewHolder ret = ListViewHolder.createHolder(ViewHolder.MAX);

        ret.addView(ViewHolder.CONTENT_CONTAINER, v.findViewById(R.id.content_container));
        ret.addView(ViewHolder.NAME, v.findViewById(R.id.item_name));
        ret.addView(ViewHolder.SIZE, v.findViewById(R.id.item_size));
        ret.addView(ViewHolder.CHECK_BOX_STUB, getCheckBoxView(v));
        ret.addView(ViewHolder.CHECK_BOX, v.findViewById(R.id.item_check));
        ret.addView(ViewHolder.GROUP_HEADER_STUB, v.findViewById(R.id.group_header_container_stub));
        ret.addView(ViewHolder.GROUP_HEADER, v.findViewById(R.id.group_header_container));
        ret.addView(ViewHolder.ICON, v.findViewById(R.id.item_icon));

        return ret;
    }

    protected View ensureViewFromStub(ListViewHolder vh, View v,
                                      int stubIdInViewHolder, int viewIdInViewHolder, int targetViewId) {
        View ret;
        ViewStub stub = vh.getView(stubIdInViewHolder, ViewStub.class);
        if (stub != null) {
            ret = stub.inflate();
        } else {
            ret = v.findViewById(targetViewId);
        }

        if (ret != null) {
            vh.addView(viewIdInViewHolder, ret);
        }
        return ret;
    }

    private View getCheckBoxView(View v) {
        View ret = null;
        ViewStub stub  = (ViewStub) v.findViewById(R.id.check_stub);
        if (stub != null) {
            try {
                ret = stub.inflate();
            } catch (InflateException e) {
                Log.e(TAG, "getCheckBoxView() - InflateException: " + e.getMessage());
            }
        }

        return ret;
    }

    @Override
    public void notifyDataSetChanged() {
        updateGroupSet();
        super.notifyDataSetChanged();
        setCheckedListForAll();
    }

    private void updateGroupSet() {
        Log.d(TAG, "datph updateGroupSet");
        if (mGroupItems != null) {
            mGroupSet.clear();
            int nGroupPos = 0;
            int nGroupSz = mGroupItems.size();
            int prevSize = 0;
            for (int i = 0; i < nGroupSz; i++) {
                GroupItem item = mGroupItems.get(i);
                if (item == null) {
                    return;
                }
                if (i != 0) {
                    nGroupPos = nGroupPos + 1 + prevSize;
                }
                prevSize = item.getChildList() != null ? item.getChildList().size() : 0;

                Log.d(TAG, "datph updateGroupSet i = " + i + "-pos = " + nGroupPos);
                mGroupSet.put(i, nGroupPos);
            }
        }
    }

    private boolean isGroupHeader(int position) {
        boolean ret = false;
        if (mGroupItems != null) {
            int size = mGroupItems.size();
            int index = 0;
            do {
                GroupItem item  = mGroupItems.get(index++);
                if (position == 0) {
                    ret = true;
                }
                position = position - item.getChildListSize() - 1; // 1 is group header.
            } while (position >= 0 && index < size);
        }
        return ret;
    }

    private int getGroupIndex(int position) {
        int ret = -1;

        if (mGroupItems != null) {
            int index = 0;
            int groupCnt = mGroupItems.size();
            Log.d(TAG, "datph getGroupIndex groupCnt = " + groupCnt);
            while (index < groupCnt) {
                Log.d(TAG, "datph getGroupIndex position before = " + position + "-index = " + index);
                position = position - (1 + mGroupItems.get(index).getChildListSize());
                if (position < 0) {
                    Log.d(TAG, "datph getGroupIndex (set ret) position after = " + position);
                    ret = index;
                    break;
                }
                index++;
            }
        }

        return ret;
    }

    private boolean isTwoType() {
        boolean ret = false;
        if (mGroupItems != null && mGroupItems.size() > 1) {
            ret = true;
        }
        return ret;
    }

    private int getGroupPosInList(int index) {
        if (index < 0 || mGroupSet == null) {
            return -1;
        }

        return mGroupSet.get(index, -1);
    }

    //To do: need to update selected List
    public HashMap<Integer, Long> getSelectedSizeGroup() {
        Log.d(TAG, "getSelectedSizeGroup()");
        HashMap<Integer, Long> retSizeMap = new HashMap<>();
        mMapSelectedModels.clear();
        int junkType;
        long size;
        synchronized (mSelectedList) {
            if (isCheckMode()) {
                if (isSelectedAll()) {
                    synchronized (mGroupItems) {
                        Log.d(TAG, "getSelectedSizeGroup() = grSz = " + mGroupItems.size());
                        for (GroupItem itemGroup : mGroupItems) {
                            junkType = itemGroup.getJunkType();
                            size = itemGroup.getSize();
                            Log.d(TAG, "getSelectedSizeGroup() - child sz = " + size);
                            retSizeMap.put(junkType, size);
                            mMapSelectedModels.put(junkType, itemGroup.getChildList());
                        }
                    }
                } else {
                    int selectedSz = mSelectedList.size();
                    int position;
                    GroupItem groupTmp;
                    List<StorageInfoDataModel> childList;
                    if (isTwoType()) {
                        //(positionInList - groupPosition - 1) : positionInList
                        List<StorageInfoDataModel> tmpLst;
                        for (int i = 0; i < selectedSz; i++) {
                            position = mSelectedList.get(i);
                            int groupIndex = getGroupIndex(position);
                            if (!isGroupHeader(position)) {
                                int groupPos = getGroupPosInList(groupIndex);
                                groupTmp = mGroupItems.get(groupIndex);
                                if (groupTmp != null) {
                                    childList = groupTmp.getChildList();
                                    if (childList != null) {
                                        StorageInfoDataModel tmpModel = childList.get(position - groupPos - 1);
                                        if (tmpModel != null) {
                                            junkType = groupTmp.getJunkType();
                                            if (retSizeMap.containsKey(junkType)) {
                                                size = retSizeMap.get(junkType);
                                            } else {
                                                size = 0;
                                            }
                                            size += tmpModel.getSize();
                                            retSizeMap.put(junkType, size);

                                            tmpLst = mMapSelectedModels.get(junkType);
                                            if (tmpLst == null) {
                                                tmpLst = new ArrayList<>();
                                            }
                                            tmpLst.add(tmpModel);
                                            mMapSelectedModels.put(junkType, tmpLst);
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        if (mGroupItems != null && mGroupItems.get(0) != null) {
                            groupTmp = mGroupItems.get(0);
                            junkType = groupTmp.getJunkType();
                            size = 0;
                            int childSz = groupTmp.getChildListSize();
                            childList = groupTmp.getChildList();
                            List<StorageInfoDataModel> lstSelected = new ArrayList<>();
                            for (int i = 0; i < selectedSz; i++) {
                                position = mSelectedList.get(i);
                                if (childSz > position) {
                                    StorageInfoDataModel tmpModel = childList.get(position);
                                    if (tmpModel != null) {
                                        lstSelected.add(tmpModel);
                                        size += tmpModel.getSize();
                                    }
                                }
                            }
                            retSizeMap.put(junkType, size);
                            mMapSelectedModels.put(junkType, lstSelected);
                        }
                    }
                }
            }
        }
        Log.d(TAG, "getSelectedSizeGroup() - size = " + retSizeMap);
        return retSizeMap;
    }

    public HashMap<Integer, List<StorageInfoDataModel>> getSelectedList() {
        Log.d(TAG, "getSelectedList() - " + mMapSelectedModels);
        return mMapSelectedModels;
    }
}

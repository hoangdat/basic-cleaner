package com.fbon.android.app.basiccleaner.util;


public class Constant {
    public static final String APP_TAG = "BaC-PH";

    public static final String PACKAGE_ID = "com.fbon.android.app.basiccleaner";
    public static final long KILO_BYTES         = 1024L;
    public static final long MEGA_BYTES         = KILO_BYTES * 1024;

    public static final int FLAG_CLEAR_CACHE    = 0b0000000001;
    public static final int FLAG_CLEAR_UN       = 0b0000000010;
    public static final int FLAG_CLEAR_LARGE    = 0b0000000100;
    public static final int FLAG_CLEAR_APK      = 0b0000001000;
    public static final int FLAG_CLEAR_APP_MEM  = 0b0000010000;

    public static final int FLAG_LOAD_CACHE             = 0b000000100000;
    public static final int FLAG_LOAD_UN                = 0b000001000000;
    public static final int FLAG_LOAD_LARGE             = 0b000010000000;
    public static final int FLAG_LOAD_APK               = 0b000100000000;
    public static final int FLAG_LOAD_APP_MEM           = 0b001000000000;
    public static final int FLAG_LOAD_MEM_INFO          = 0b010000000000;
    public static final int FLAG_LOAD_STORAGE_INFO      = 0b100000000000;

    public static final int FLAG_NONE                   = -1;

//    public static final int STATE_NONE          = 0;
//    public static final int STATE_SCAN_DONE     = 1;
//    public static final int STATE_CLEAR_DONE    = 2;

    public static final double THRESHOLD_SCORE  = 0.99;

    public static final int UI_FINISH_UPDATE = 10001;

    public static final int MSG_START_CALCULATE_SELECTED_FILE_SIZE = 1000;
    public static final int MSG_DISPLAY_RESULT_SIZE = 1001;

    public static final int SCANNING_WAVE_DURATION = 1766;

    public static final int PERIOD_CLEAN_OK = 5 * 1000 * 60;
    public static final int PERMISSION_REQUEST_CODE = 101;

    public static final String SETTINGS_REFRESH_BROADCAST = "settings_refresh";
    public static final String EXTRA_IS_SETTINGS_LARGE_FILE = "is_large_file";

    public interface Extra {
        String EXTRA_LAUNCH_SCENE = "launch_scene";
    }

    public interface Action {
        String ACTION_SCAN_OVERALL = "fbon.basiccleaner.intent.action.SCAN_OVERALL";
    }

    public interface AdvertisementConstant {
        String AppId = "204204844";
        int MAX_COUNT_CLEAN = 16;
    }
}

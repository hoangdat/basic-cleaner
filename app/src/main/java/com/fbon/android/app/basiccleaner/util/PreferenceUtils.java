package com.fbon.android.app.basiccleaner.util;

import android.content.Context;
import android.content.SharedPreferences;

import static android.preference.PreferenceManager.getDefaultSharedPreferences;

public class PreferenceUtils {

//    public static final String KEY_LAST_STORAGE_SCAN      = "last_storage_scan";
//    public static final String KEY_LAST_RESULT_STORAGE    = "last_storage_result";
//    public static final String KEY_LAST_RESULT_MEMORY     = "last_memory_result";
//    public static final String KEY_LAST_MEM_SCAN          = "last_mem_scan";
    public static final String KEY_LAST_OVERALL_CLEAN = "last_overall_clean";
    public static final String IS_FIRST_ENTRY = "is_first_entry";
//    public static final String KEY_LAST_RESULT_OVERALL    = "last_overall_result";
//    public static final String KEY_STATE_OVERALL          = "state_overall";
//    public static final String KEY_STATE_STORAGE          = "state_storage";
//    public static final String KEY_STATE_MEMORY           = "state_memory";
    public static final String KEY_MORE_SPACE_LARGE_THRESHOLD = "more_space_large_threshold";
    public static final String KEY_INSTALLED_SHORCUT = "installed_shortcut";
    public static final String KEY_COUNT_CLEAN = "count_clean";


//    public static long getLastTimeStorageScan(Context context) {
//        return getDefaultSharedPreferences(context).getLong(KEY_LAST_STORAGE_SCAN, -1);
//    }
//
//    public static void setLastTimeStorageScan(Context context, long time) {
//        SharedPreferences.Editor editor = getDefaultSharedPreferences(context).edit();
//        editor.putLong(KEY_LAST_STORAGE_SCAN, time);
//        editor.apply();
//    }
//
//    public static long getLastStorageScan(Context context) {
//        return getDefaultSharedPreferences(context).getLong(KEY_LAST_RESULT_STORAGE, -1);
//    }
//
//    public static void setLastStorageScan(Context context, long size) {
//        SharedPreferences.Editor editor = getDefaultSharedPreferences(context).edit();
//        editor.putLong(KEY_LAST_RESULT_STORAGE, size);
//        editor.apply();
//    }
//
////    public static int getStorageState(Context context) {
////        return getDefaultSharedPreferences(context).getInt(KEY_STATE_STORAGE, Constant.STATE_NONE);
////    }
//
//    public static void setStorageState(Context context, int state) {
//        SharedPreferences.Editor editor = getDefaultSharedPreferences(context).edit();
//        editor.putInt(KEY_STATE_STORAGE, state);
//        editor.apply();
//    }
//
//    public static void setLastTimeMemoryScan(Context context, long time) {
//        SharedPreferences.Editor editor = getDefaultSharedPreferences(context).edit();
//        editor.putLong(KEY_LAST_MEM_SCAN, time);
//        editor.apply();
//    }
//
//    public static long getLastTimeMemScan(Context context) {
//        return getDefaultSharedPreferences(context).getLong(KEY_LAST_MEM_SCAN, -1);
//    }
//
    public static void setLastTimeOverallClean(Context context, long time) {
        SharedPreferences.Editor editor = getDefaultSharedPreferences(context).edit();
        editor.putLong(KEY_LAST_OVERALL_CLEAN, time);
        editor.apply();
    }

    public static long getLastTimeOverallClean(Context context) {
        return getDefaultSharedPreferences(context).getLong(KEY_LAST_OVERALL_CLEAN, -1);
    }

    public static void setFirstEntry(Context context, boolean value) {
        SharedPreferences.Editor e = getDefaultSharedPreferences(context).edit();
        e.putBoolean(IS_FIRST_ENTRY, value);
        e.apply();
    }

    public static boolean isFirstEntry(Context context) {
        return getDefaultSharedPreferences(context).getBoolean(IS_FIRST_ENTRY, true);
    }

//    public static void setLastResultOverallScan(Context context, int progress) {
//        SharedPreferences.Editor editor = getDefaultSharedPreferences(context).edit();
//        editor.putInt(KEY_LAST_RESULT_OVERALL, progress);
//        editor.apply();
//    }
//
//    public static int getLastResultOverallScan(Context context) {
//        return getDefaultSharedPreferences(context).getInt(KEY_LAST_RESULT_OVERALL, -1);
//    }
//
////    public static int getOverallState(Context context) {
////        return getDefaultSharedPreferences(context).getInt(KEY_STATE_OVERALL, Constant.STATE_NONE);
////    }
//
//    public static void setOverallState(Context context, int state) {
//        SharedPreferences.Editor editor = getDefaultSharedPreferences(context).edit();
//        editor.putInt(KEY_STATE_OVERALL, state);
//        editor.apply();
//    }
//
////    public static int getMemoryState(Context context) {
////        return getDefaultSharedPreferences(context).getInt(KEY_STATE_MEMORY, Constant.STATE_NONE);
////    }
//
//    public static void setMemoryState(Context context, int state) {
//        SharedPreferences.Editor editor = getDefaultSharedPreferences(context).edit();
//        editor.putInt(KEY_STATE_MEMORY, state);
//        editor.apply();
//    }
//
//    public static void setLastResultMemoryScan(Context context, long progress) {
//        SharedPreferences.Editor editor = getDefaultSharedPreferences(context).edit();
//        editor.putLong(KEY_LAST_RESULT_MEMORY, progress);
//        editor.apply();
//    }
//
//    public static long getLastResultMemoryScan(Context context) {
//        return getDefaultSharedPreferences(context).getLong(KEY_LAST_RESULT_MEMORY, -1);
//    }

    public static long getLargeFilesSize(Context context) {
        return getDefaultSharedPreferences(context).getLong(KEY_MORE_SPACE_LARGE_THRESHOLD, 25 * Constant.MEGA_BYTES);
    }

    public static void setLargeFilesSize(Context context, Long size) {
        SharedPreferences.Editor e = getDefaultSharedPreferences(context).edit();
        e.putLong(KEY_MORE_SPACE_LARGE_THRESHOLD, size);
        e.apply();
    }

    public static void setShorcutInstalled(Context context, Boolean isInstall) {
        SharedPreferences.Editor e = getDefaultSharedPreferences(context).edit();
        e.putBoolean(KEY_INSTALLED_SHORCUT, isInstall);
        e.apply();
    }

    public static boolean getInstalledShortcut(Context context) {
        return getDefaultSharedPreferences(context).getBoolean(KEY_INSTALLED_SHORCUT, false);
    }

    public static int getCountClean(Context context) {
        if (context != null) {
            return getDefaultSharedPreferences(context).getInt(KEY_COUNT_CLEAN, 0);
        }
        return 0;
    }

    public static void setCountClean(Context context, int count) {
        if (context != null) {
            SharedPreferences.Editor e = getDefaultSharedPreferences(context).edit();
            e.putInt(KEY_COUNT_CLEAN, count);
            e.apply();
        }
    }
}

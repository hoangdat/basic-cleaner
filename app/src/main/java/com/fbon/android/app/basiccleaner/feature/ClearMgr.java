package com.fbon.android.app.basiccleaner.feature;


import android.content.Context;

import com.fbon.android.app.basiccleaner.feature.memory.ClearAppMemImp;
import com.fbon.android.app.basiccleaner.feature.storage.ClearCacheImp;
import com.fbon.android.app.basiccleaner.feature.storage.ClearJunkImp;
import com.fbon.android.app.basiccleaner.util.AppFeature;
import com.fbon.android.app.basiccleaner.util.ChainBuilder;
import com.fbon.android.app.basiccleaner.util.Constant;
import com.fbon.android.app.basiccleaner.util.Log;

import java.util.ArrayList;
import java.util.HashMap;


public class ClearMgr {
    public static final String TAG = "ClearMgr";

    static ClearMgr mInstance = null;
    private ArrayList<ClearListType> mLstClear;
    private int mScanType;
    private ClearImp mClearImp;
    private boolean mIsCleanDefault = true;

    private static HashMap<Integer, Integer> mMapTypeFlagClear = new HashMap<>();
    static {
        mMapTypeFlagClear.put(CleanLdrManager.APK_JUNK_TYPE, Constant.FLAG_CLEAR_APK);
        mMapTypeFlagClear.put(CleanLdrManager.CACHE_JUNK_TYPE, Constant.FLAG_CLEAR_CACHE);
        mMapTypeFlagClear.put(CleanLdrManager.UNNECESSARY_JUNK_TYPE, Constant.FLAG_CLEAR_UN);
        mMapTypeFlagClear.put(CleanLdrManager.LARGE_JUNK_TYPE, Constant.FLAG_CLEAR_LARGE);
        mMapTypeFlagClear.put(CleanLdrManager.MEM_JUNK_TYPE, Constant.FLAG_CLEAR_APP_MEM);
    }

    private static final int FLAG_CLEAR_MEM_STATE = (!AppFeature.isSupportListMem()) ?
            Constant.FLAG_CLEAR_CACHE : Constant.FLAG_CLEAR_APP_MEM;

    private ClearMgr(Context context) {
        ChainBuilder<ClearImp> builder = new ChainBuilder<ClearImp>()
                .append(new ClearCacheImp(context))
                .append(new ClearAppMemImp(context))
                .append(new ClearJunkImp(context));

        mClearImp = builder.getInstance();
    }

    public static ClearMgr getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new ClearMgr(context);
        }
        return mInstance;
    }

    public void setCleanDefault(boolean cleanDefault) {
        this.mIsCleanDefault = cleanDefault;
    }

    public boolean isValidListClear() {
        Log.d(TAG, "isValidListClear()");
        boolean ret = true;
        if (mLstClear == null || mIsCleanDefault == true) {
            ret = false;
        }
        Log.d(TAG, "isValidListClear() - ret = " + ret);
        return ret;
    }

    public void setLstClear(int scanType, ArrayList<ClearListType> mLstClear, boolean cleanDefault) {
        Log.d(TAG, "setLstClear()");
        this.mScanType = scanType;
        this.mLstClear = mLstClear;
        setCleanDefault(cleanDefault);
    }

    public interface OnFinishedClearListener {
        void onFinishedClearJunk(int type, long cleanedSz);
    }

    public void startClear(OnFinishedClearListener listener) {
        Log.d(TAG, "startClear() start");
        if (mLstClear == null) {
            if (listener != null) {
                listener.onFinishedClearJunk(-1, -1);
            }
            return;
        }

        for (ClearListType type : mLstClear) {
            mClearImp.clear(listener, type);
        }

        mLstClear = null;
        setCleanDefault(true);
    }

    public int getScanType() {
        return mScanType;
    }

    public static HashMap<Integer, Integer> getClearMapFlag() {
        return mMapTypeFlagClear;
    }

    public int getFlagCleared() {
        int ret = 0;

        if (mLstClear != null) {
            for (ClearListType type : mLstClear) {
                int junk = type.getType();
                int clearFlag = mMapTypeFlagClear.get(junk);
                ret = ret | clearFlag;
            }
        }

        return ret;
    }

//    public int getFlagCleared() {
//        int ret = 0;
//        int curScene = FragmentController.getInstance().getCurrentScene();
//        switch (curScene) {
//            case FragmentController.Scene.Clean_storage:
//                ret = Constant.FLAG_CLEAR_CACHE | Constant.FLAG_CLEAR_UN;
//                break;
//            case FragmentController.Scene.Clean_memory:
//                ret = FLAG_CLEAR_MEM_STATE;
//                break;
//            case FragmentController.Scene.CleanAll:
//                ret = AppFeature.isSupportListMem() ?
//                        (Constant.FLAG_CLEAR_CACHE | Constant.FLAG_CLEAR_UN | Constant.FLAG_CLEAR_APP_MEM) :
//                        (Constant.FLAG_CLEAR_CACHE | Constant.FLAG_CLEAR_UN);
//                break;
////            case FragmentController.Scene.Clean_more_space:
////                ret = Constant.FLAG_CLEAR_APK | Constant.FLAG_CLEAR_LARGE;
//            default:
//                break;
//        }
//        return ret;
//    }
}

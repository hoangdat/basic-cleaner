package com.fbon.android.app.basiccleaner.adapter;

import android.view.View;

public class ListViewHolder {
    public static ListViewHolder createHolder(int max) {
        return new ListViewHolder(max);
    }

    private int mMaxSize;
    private View mView[];

    private ListViewHolder(int max) {
        mView = new View[max];
        mMaxSize = max;
    }

    public void addView(int key, View view) {
        if (key < mMaxSize && mView != null) {
            mView[key] = view;
        }
    }

    public View getView(int key) {
        View ret = null;
        if (key < mMaxSize && mView != null) {
            ret = mView[key];
        }
        return ret;
    }

    public <T extends View> T getView(int key, Class<T> type) {
        T ret = null;
        if (key < mMaxSize && mView != null) {
            ret = type.cast(mView[key]);
        }
        return ret;
    }
}

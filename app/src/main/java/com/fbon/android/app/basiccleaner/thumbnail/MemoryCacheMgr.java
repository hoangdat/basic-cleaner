package com.fbon.android.app.basiccleaner.thumbnail;

import android.graphics.Bitmap;
import android.util.LruCache;

public class MemoryCacheMgr {
    private static LruCache<Object, Bitmap> sCache;
    private static MemoryCacheMgr mInstance = null;

    private static int cacheCnt = 64;

    private MemoryCacheMgr() {
        sCache = new LruCache<Object, Bitmap>(cacheCnt);
    }

    public static synchronized MemoryCacheMgr getInstance() {
        if (mInstance == null) {
            mInstance = new MemoryCacheMgr();
        }
        return mInstance;
    }

    public void addCache(String fullPath, String packageName, Bitmap bmp) {
        String key = fullPath != null  ? fullPath : packageName;
        if (key != null) {
            sCache.put(key, bmp);
        }
    }

    public void clearCache() {
        sCache.evictAll();
    }

    public Bitmap getCache(String fullPath, String packageName) {
        String key = fullPath != null ? fullPath : packageName;
        if (key != null) {
            return sCache.get(key);
        }
        return null;
    }
}

package com.fbon.android.app.basiccleaner.listener;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;

import com.fbon.android.app.basiccleaner.activity.SettingsActivity;
import com.fbon.android.app.basiccleaner.util.Constant;


public class SettingsRefreshReceiver extends AbsBroadcastReceiverImp {
    private SettingsActivity mActivity;

    public SettingsRefreshReceiver(SettingsActivity activity, ListenerMgr.LifeCycle registerTime, ListenerMgr.LifeCycle unregisterTime) {
        super(activity, registerTime, unregisterTime);
        mActivity = activity;
    }

    @Override
    IntentFilter getIntentFilter() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(Constant.SETTINGS_REFRESH_BROADCAST);
        return filter;
    }

    @Override
    BroadcastReceiver getBroadcastReceiver() {
        return new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                boolean isLargeFile = intent.getBooleanExtra(Constant.EXTRA_IS_SETTINGS_LARGE_FILE, false);
                if (isLargeFile) {
                    mActivity.ensureMoreSpaceView();
                }
            }
        };
    }

    @Override
    public void unregisterListener() {
        LocalBroadcastManager.getInstance(mContext).unregisterReceiver(mReceiver);
        mContext = null;
        mActivity = null;
    }
}

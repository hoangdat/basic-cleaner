package com.fbon.android.app.basiccleaner.thumbnail;


import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.util.LruCache;
import android.widget.ImageView;

import com.fbon.android.app.basiccleaner.R;
import com.fbon.android.app.basiccleaner.adapter.JUnitHandler;
import com.fbon.android.app.basiccleaner.util.ChainBuilder;
import com.fbon.android.app.basiccleaner.util.Log;
import com.fbon.android.app.basiccleaner.util.UiUtils;

public class ThumbnailMgr {
    private static final String TAG = "ThumbnailMgr";
    private static final int MAX_THREAD = Runtime.getRuntime().availableProcessors() / 2;

    private Context mContext;

    private static ThumbnailMgr sInstance;
    private HandlerThread[] mThumbnailThread;
    private ThumbnailHandler[] mThumbnailHandler;
    private int mCurThreadIndex = 0;
    private MemoryCacheMgr mMemoryCacheMgr = null;

    private static final String THUMBNAIL_THREAD_NAME = "thumbnail_thread";
    private static final int MAX_FAIL_LIST = 1024;
    private LruCache<Integer, String> mFailedCache = new LruCache<>(MAX_FAIL_LIST);
    private ThumbnailImp mThumbnailImp;

    private Handler mUiUpdateHandler = new WeakRefHandler();

    public static ThumbnailMgr getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new ThumbnailMgr(context.getApplicationContext());
        }

        return sInstance;
    }

    private static ImageView getThumbnailView(String path, String packagename, ImageView iconView) {
        ImageView ret = null;
        if (path != null || packagename != null) {
            if (UiUtils.isApk(UiUtils.getExtensionAsUpperCase(path)) || packagename != null) {
                ret = iconView;
            }
        }

        return ret;
    }

    public ImageView loadThumbnail(String path, String packageName, ImageView iconView) {
        Log.d(TAG, "loadThumbnail path = " + path + "-package = " + packageName);
        if (iconView == null) {
            return null;
        }

        ImageView ret = null;

        String key = path != null ? path : packageName;
        iconView.setTag(key);

        if (!isFailed(path, packageName)) {
            ImageView imageView = getThumbnailView(path, packageName, iconView);

            Bitmap bmp = mMemoryCacheMgr.getCache(path, packageName);

            if (imageView != null) {
                if (bmp != null) {
                    imageView.setImageBitmap(bmp);
                    ret = imageView;
                } else {
                    imageView.setImageBitmap(null);

                    if (UiUtils.isApk(UiUtils.getExtensionAsUpperCase(path)) || packageName != null) {
                        ret = imageView;
                    }

                    ThumbnailReqInfo info = new ThumbnailReqInfo(path, packageName, iconView);
                    mThumbnailHandler[mCurThreadIndex].sendMessageAtFrontOfQueue(mThumbnailHandler[mCurThreadIndex].obtainMessage(0, info));

                    mCurThreadIndex++;
                    if (mCurThreadIndex >= MAX_THREAD) {
                        mCurThreadIndex = 0;
                    }
                }
            }
        } else {
            iconView.setImageBitmap(null);
        }

        return ret;
    }

    private ThumbnailMgr(Context context) {
        mContext = context;
        mMemoryCacheMgr = MemoryCacheMgr.getInstance();

        mThumbnailThread = new HandlerThread[MAX_THREAD];
        mThumbnailHandler = new ThumbnailHandler[MAX_THREAD];

        ChainBuilder<ThumbnailImp> builder = new ChainBuilder<>().
                append(new ApkThumbnailImp(mContext));
        mThumbnailImp = builder.getInstance();

        for (int i = 0; i < MAX_THREAD; i++) {
            mThumbnailThread[i] = new HandlerThread(THUMBNAIL_THREAD_NAME + i);
            mThumbnailThread[i].start();

            Looper looper = mThumbnailThread[i].getLooper();

            if (looper != null) {
                mThumbnailHandler[i] = new ThumbnailHandler(looper);
            }
        }
    }

    private static class WeakRefHandler extends Handler {
        public void handleMessage(Message msg) {
            if (msg != null) {
                ThumbnailReqInfo reqInfo = (ThumbnailReqInfo) msg.obj;
                if (reqInfo != null) {
                    ImageView imageView = reqInfo.getThumbnailView();
                    if (imageView != null) {
                        String tag = (String) imageView.getTag();
                        if (tag != null && (tag.equals(reqInfo.mPath) || tag.equals(reqInfo.mPackageName))) {
                            UiUtils.setImageBitmap(imageView, reqInfo.mBmp);
                        }
                    }
                }
            }
        }
    }

    private final class ThumbnailHandler extends JUnitHandler<ThumbnailReqInfo> {
        public ThumbnailHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            ThumbnailReqInfo reqInfo = getMsgValue(msg.obj);

            if (reqInfo != null) {
                if (reqInfo.mBmp == null) {
                    reqInfo.mBmp = mThumbnailImp.createThumbnail(reqInfo.mPath, reqInfo.mPackageName);
                }

                if (reqInfo.mBmp != null) {
                    mUiUpdateHandler.sendMessageAtFrontOfQueue(mUiUpdateHandler.obtainMessage(0, reqInfo));
                    mMemoryCacheMgr.addCache(reqInfo.mPath, reqInfo.mPackageName, reqInfo.mBmp);
                } else {
                    addToFailed(reqInfo.mPath, reqInfo.mPackageName);
                }
            }

        }
    }

    private boolean isFailed(String path, String packageName) {
        boolean bRet = false;
        String key = path != null ? path : packageName;
        if (key != null) {
            String keyValue = mFailedCache.get(key.hashCode());
            if (keyValue != null) {
                bRet = keyValue.equals(key);
            }
        }
        return bRet;
    }

    private void addToFailed(String path, String packageName) {
        String key = path != null ? path : packageName;
        if (key != null) {
            mFailedCache.put(key.hashCode(), key);
        }
    }

    private static class ThumbnailReqInfo {
        String mPath;
        String mPackageName;
        ImageView mIconView;

        Bitmap mBmp;

        public ThumbnailReqInfo(String path, String packageName, ImageView iconView) {
            mPath = path;
            mIconView = iconView;
            mPackageName = packageName;
        }

        public ImageView getThumbnailView() {
            return ThumbnailMgr.getThumbnailView(mPath, mPackageName, mIconView);
        }
    }

    public Bitmap getHomeShortcutBitmap(Context context, int width) {
        Bitmap thumbnail;
        int originWidth;
        int originHeight;
        int longerLineLength;

        Resources r = context.getResources();

        thumbnail = BitmapFactory.decodeResource(r, R.mipmap.ic_quickboost);

        if (thumbnail != null) {
            originWidth = thumbnail.getWidth();
            originHeight = thumbnail.getHeight();
            longerLineLength = Math.max(originWidth, originHeight);
            float scaleRatio = (float) width / (float) longerLineLength;

            if (width != longerLineLength && scaleRatio > 0.0f && originWidth > 0 && originHeight > 0) {
                thumbnail = Bitmap.createScaledBitmap(thumbnail, (int) (originWidth * scaleRatio), (int) (originHeight * scaleRatio), true);
            }
        }

        return thumbnail;
    }
}

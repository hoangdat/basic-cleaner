package com.fbon.android.app.basiccleaner.feature.memory;

import android.app.ActivityManager;
import android.content.Context;

import com.fbon.android.app.basiccleaner.feature.CleanLdrManager;
import com.fbon.android.app.basiccleaner.feature.ClearImp;
import com.fbon.android.app.basiccleaner.feature.ClearListType;
import com.fbon.android.app.basiccleaner.feature.ClearMgr;
import com.fbon.android.app.basiccleaner.model.StorageInfoDataModel;
import com.fbon.android.app.basiccleaner.util.Constant;
import com.fbon.android.app.basiccleaner.util.Log;

import java.util.ArrayList;


public class ClearAppMemImp extends ClearImp {

    public ClearAppMemImp(Context context) {
        super(context);
    }

    @Override
    protected void _handleClear(ClearMgr.OnFinishedClearListener listener, ClearListType type) {
        long size = 0;
        if (isSupport(type)) {
            ActivityManager am = (ActivityManager) mContext.getSystemService(Context.ACTIVITY_SERVICE);
            ArrayList<StorageInfoDataModel> lst = type.getList();
            if (lst != null) {
                for (StorageInfoDataModel model : lst) {
                    if (model != null && model.getPackageName() != null) {
                        size += model.getSize();
                        Log.d(TAG, "_handleClear() - mem = " + model.getPackageName());
                        try {
                            am.killBackgroundProcesses(model.getPackageName());
                        } catch (Exception e) {
                            Log.d(TAG, "_handleClear() - exception e = " + e);
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
        synchronized (listener) {
            listener.onFinishedClearJunk(Constant.FLAG_CLEAR_APP_MEM, size);
        }
    }

    @Override
    protected boolean isSupport(ClearListType type) {
        if (CleanLdrManager.MEM_JUNK_TYPE == type.getType()) {
            return true;
        }
        return false;
    }

    @Override
    protected void _clear(ClearMgr.OnFinishedClearListener listener, ClearListType type) {
        Log.d(TAG, "_clear() MEM");
        createHandler(listener, type);
    }

    @Override
    protected String getName() {
        return "memBoost";
    }

    @Override
    protected int getMsgType() {
        return MSG_START_CLEAR_APP_MEM;
    }
}

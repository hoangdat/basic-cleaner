package com.fbon.android.app.basiccleaner.util;


import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.text.format.Formatter;
import android.widget.ImageView;

import com.fbon.android.app.basiccleaner.R;
import com.fbon.android.app.basiccleaner.feature.CleanLdrManager;
import com.fbon.android.app.basiccleaner.fragment.FragmentController;
import com.fbon.android.app.basiccleaner.model.StorageInfoDataModel;

import java.util.ArrayList;
import java.util.Locale;

public class UiUtils {
    public static final String TAG = "UiUtils";
    public static final int THUMBNAIL_SIZE = 320;
    public static final int PERIOD_SCAN_STORAGE_TIME = 3 * 60 * 1000; //3 minutes
    public static final int PERIOD_MEM_CLEAR_TIME    = 60 * 1000;
    public static final int PERIOD_SCAN_OVERALL_TIME = 3 * 60 * 1000;

    public static String getTitle(Context context, int scene) {
        Resources res = context.getResources();
        int resId = -1;
        switch (scene) {
            case FragmentController.Scene.Clean_memory:
            case FragmentController.Scene.Memory:
                resId = R.string.memory;
                break;
            case FragmentController.Scene.Clean_storage:
            case FragmentController.Scene.Storage:
                resId = R.string.storage;
                break;
            case FragmentController.Scene.CleanAll:
            case FragmentController.Scene.Overall:
            case FragmentController.Scene.Clean_quick:
                resId = R.string.title_activity_first;
                break;
            case FragmentController.Scene.Clean_more_space:
            case FragmentController.Scene.More_space:
                resId = R.string.more_space;
                break;
            default:
                break;
        }

        return res.getString(resId);
    }

    public static Drawable getApkIcon(Context context, String filePath) {
        Drawable ret = null;
        PackageManager pkgManager = context.getPackageManager();
        if (pkgManager != null && filePath != null) {
            PackageInfo pkgInfo = pkgManager.getPackageArchiveInfo(filePath, PackageManager.GET_ACTIVITIES);
            if (pkgInfo != null) {
                try {
                    ApplicationInfo appInfo = pkgInfo.applicationInfo;
                    appInfo.sourceDir = filePath;
                    appInfo.publicSourceDir = filePath;
                    if (appInfo.icon != 0) {
                        ret = appInfo.loadIcon(context.getPackageManager());
                        Log.e(TAG, "getApkIcon() - ret :" + ret);
                    }
                } catch (OutOfMemoryError e) {
                    Log.e(TAG, "getApkIcon() - OutOfMemoryError:" + e.toString());
                }
            }
        }

        if (ret == null) {
            Log.e(TAG, "getApkIcon() - ret  = null");
            ret = UiUtils.getFileTypeDrawable(context, UiUtils.getExtensionAsUpperCase(filePath));
        }

        return ret;
    }

    public static void setImageBitmap(ImageView view, Bitmap bmp) {
        view.setImageBitmap(bmp);
    }

    public static String makeFileSizeString(Context context, long fileSize) {
        String ret = null;

        if (ret == null && context != null) {
                ret = Formatter.formatFileSize(context, fileSize);
        }

        return ret;
    }

    public static String getExtensionAsUpperCase(String path) {
        String ext = getExt(path);
        return ext.toUpperCase(Locale.ENGLISH);
    }

    public static String getExt(String path) {
        String ret = "";
        if (path != null) {
            int lastDot = path.lastIndexOf('.');
            if (lastDot >= 0) {
                ret = path.substring(lastDot + 1);
            }
        }

        return ret;
    }

    public static boolean isApk(String ext) {
        boolean bRet = false;
        if (TextUtils.equals("APK", ext)) {
            bRet = true;
        }
        return bRet;
    }

    public static Drawable getFileTypeDrawable(Context context, String ext) {
        Drawable icon = null;
        if (MediaFile.AUDIO.contains(ext)) {
            icon = context.getDrawable(R.drawable.ic_audiotrack_24px);
        } else if (MediaFile.IMAGE.contains(ext)) {
            icon = context.getDrawable(R.drawable.ic_photo);
        } else if (MediaFile.VIDEO.contains(ext)) {
            icon = context.getDrawable(R.drawable.ic_video);
        } else if (ext.equalsIgnoreCase("apk")) {
            icon = context.getDrawable(R.drawable.ic_android);
        } else {
            icon = context.getDrawable(R.drawable.ic_file_empty);
        }

        return icon;
    }

    public static Bitmap getBitmapFromDrawable(Drawable drawable) {
        Bitmap ret = null;
        if (drawable instanceof BitmapDrawable) {
            ret = ((BitmapDrawable) drawable).getBitmap();
        } else {
            /*
             * 3rd Party App icon might have other drawable types.
             * For example, Facebook App icon is LayerDrawable object.
             * So, below these codes are needed.
            * */
            int width = drawable.getIntrinsicWidth();
            int height = drawable.getIntrinsicHeight();
            Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);
            drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
            drawable.draw(canvas);
            ret = bitmap;
        }
        return ret;
    }

//    public static boolean canScanStorage(Context context) {
//        boolean ret = false;
//        long currentTime = System.currentTimeMillis();
//        long lstScan = PreferenceUtils.getLastTimeStorageScan(context);
//        if (lstScan == -1 || currentTime - lstScan > PERIOD_SCAN_STORAGE_TIME) {
//            ret = true;
//        }
//        return ret;
//    }
//
//    public static boolean canScanMem(Context context) {
//        boolean ret = false;
//        long currentTime = System.currentTimeMillis();
//        long lstScan = PreferenceUtils.getLastTimeMemScan(context);
//        if (lstScan == -1 || currentTime - lstScan > PERIOD_MEM_CLEAR_TIME) {
//            ret = true;
//        }
//        return ret;
//    }
//
//    public static boolean canScanOverall(Context context) {
//        boolean ret = false;
//        long currentTime = System.currentTimeMillis();
//        long lstScan = PreferenceUtils.getLastTimeOverallScan(context);
//        if (lstScan == -1 || currentTime - lstScan > PERIOD_SCAN_OVERALL_TIME) {
//            ret = true;
//        }
//        return ret;
//    }

    public static long getHWMemSize(Context context) {
        final long GB_BY_BYTE = 1024L * 1024 * 1024;
        long resData;
        long total;
        ActivityManager actManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        ActivityManager.MemoryInfo memInfo = new ActivityManager.MemoryInfo();
        actManager.getMemoryInfo(memInfo);
        total = memInfo.totalMem;
        total = total / (1024 * 1024);

        if (total > 7168) {
            resData = 8L * GB_BY_BYTE;
        } else if (total > 6144) {
            resData = 7L * GB_BY_BYTE;
        } else if (total > 5120) {
            resData = 6L * GB_BY_BYTE;
        } else if (total > 4096) {
            resData = 5L * GB_BY_BYTE;
        } else if (total > 3072) {
            resData = 4L * GB_BY_BYTE;
        } else if (total > 2048) {
            resData = 3L * GB_BY_BYTE;
        } else if (total > 1024 + 512) {
            resData = 2L * GB_BY_BYTE;
        } else if (total > 1024) {
            resData = (15L * GB_BY_BYTE) / 10;
        } else if (total > 768) {
            resData = 1L * GB_BY_BYTE;
        } else if (total > 512) {
            resData = (7L * GB_BY_BYTE) / 10;
        } else {
            resData = (5L * GB_BY_BYTE) / 10;
        }
        return resData;
    }

    public static long getStoreSize(Context context, int scanType, int junkType) {
        long ret = 0;
        ArrayList<StorageInfoDataModel> store = CleanLdrManager.getInstance(context).getStore(scanType, junkType);
        Log.d(TAG, "getStoreSize() store = " + store);
        if (store != null) {
            for (StorageInfoDataModel data : store) {
                if (data != null) {
                    ret += data.getSize();
//                    Log.d(TAG, "getStoreSize() size item = " + data.getSize() + "-ret = " + ret);
                }
            }
        }
        return ret;
    }

    public static boolean isNetworkOn(Context context) {
        if (context == null) {
            return false;
        }
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager != null) {
            NetworkInfo netInfo = connectivityManager.getActiveNetworkInfo();
            if (netInfo != null) {
                Log.d(TAG, "netInfo.getType() = " + netInfo.getType());
                Log.d(TAG, "netInfo.getSubtype() = " + netInfo.getSubtype());
            } else {
                Log.d(TAG, "netInfo == null");
            }

            if (netInfo != null
                    && NetworkInfo.DetailedState.CONNECTED.equals(netInfo.getDetailedState())
                    && (netInfo.getType() == ConnectivityManager.TYPE_WIFI || netInfo.getType() == ConnectivityManager.TYPE_BLUETOOTH
                    || (netInfo.getType() == ConnectivityManager.TYPE_ETHERNET)
                    || (netInfo.getType() == ConnectivityManager.TYPE_MOBILE && (netInfo.getSubtype() == TelephonyManager.NETWORK_TYPE_UMTS
                    || netInfo.getSubtype() == TelephonyManager.NETWORK_TYPE_LTE
                    || netInfo.getSubtype() == TelephonyManager.NETWORK_TYPE_HSPAP
                    || netInfo.getSubtype() == TelephonyManager.NETWORK_TYPE_HSDPA
                    || netInfo.getSubtype() == TelephonyManager.NETWORK_TYPE_HSUPA
                    || netInfo.getSubtype() == TelephonyManager.NETWORK_TYPE_HSPA
                    || netInfo.getSubtype() == TelephonyManager.NETWORK_TYPE_CDMA
                    || netInfo.getSubtype() == TelephonyManager.NETWORK_TYPE_1xRTT
                    || netInfo.getSubtype() == TelephonyManager.NETWORK_TYPE_EVDO_0
                    || netInfo.getSubtype() == TelephonyManager.NETWORK_TYPE_EVDO_A
                    || netInfo.getSubtype() == TelephonyManager.NETWORK_TYPE_EVDO_B
                    || netInfo.getSubtype() == TelephonyManager.NETWORK_TYPE_EHRPD
                    || netInfo.getSubtype() == TelephonyManager.NETWORK_TYPE_GPRS || netInfo.getSubtype() == TelephonyManager.NETWORK_TYPE_EDGE)) // Add
                    // 2G
                    // type;
            )) {
                Log.w(TAG, " if (netInfo != null) netInfo.getType() = " + netInfo.getType());
                return true;
            }
        }
        return false;
    }

    public static boolean isWifiOn(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager != null) {
            Network networks[] = connectivityManager.getAllNetworks();
            if (networks != null && networks.length > 0) {
                for (Network network : networks) {
                    if (network != null) {
                        NetworkInfo netInfo = connectivityManager.getNetworkInfo(network);
                        if (netInfo != null && isWifiNetwork(netInfo) && NetworkInfo.DetailedState.CONNECTED.equals(netInfo.getDetailedState())) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    private static boolean isWifiNetwork(NetworkInfo netInfo) {
        int type = netInfo.getType();
        boolean ret = false;
        if (type == ConnectivityManager.TYPE_WIFI
                || type == ConnectivityManager.TYPE_BLUETOOTH
                || type == ConnectivityManager.TYPE_ETHERNET) {
            ret = true;
        }
        return ret;
    }
}

package com.fbon.android.app.basiccleaner.util;


public interface Chain<T extends Chain> {
    void setNext(T next);
}

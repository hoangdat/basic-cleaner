package com.fbon.android.app.basiccleaner.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

import com.fbon.android.app.basiccleaner.R;
import com.fbon.android.app.basiccleaner.util.Constant;
import com.fbon.android.app.basiccleaner.util.Log;
import com.fbon.android.app.basiccleaner.util.PreferenceUtils;
import com.fbon.android.app.basiccleaner.util.UiUtils;

import java.util.Arrays;
import java.util.List;


public class SettingsLargeFile extends AbsDialogFragment {
    public static final String TAG = "SetLarge";
    public static final String NAME = "settings_large_file";
    private SeekBar mSeekLargeFile;
    private TextView mTxtValue;
    private long mCurrentSize;

    private static final List<Long> mListSize = Arrays.asList(10 * Constant.MEGA_BYTES, 25 * Constant.MEGA_BYTES, 50 * Constant.MEGA_BYTES,
            128 * Constant.MEGA_BYTES, 256 * Constant.MEGA_BYTES, 512 * Constant.MEGA_BYTES, 1024 * Constant.MEGA_BYTES);

    @Override
    protected Dialog _createDialog() {
        View view = initView();
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setTitle(R.string.large_group);
        alertDialog.setPositiveButton(R.string.set, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                PreferenceUtils.setLargeFilesSize(getActivity(), mCurrentSize);
                Intent intent = new Intent(Constant.SETTINGS_REFRESH_BROADCAST);
                intent.putExtra(Constant.EXTRA_IS_SETTINGS_LARGE_FILE, true);
                getActivity().sendBroadcast(intent);
                dismissAllowingStateLoss();
            }
        }).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dismissAllowingStateLoss();
            }
        });
        alertDialog.setView(view);
        AlertDialog dialog = alertDialog.create();
        dialog.setCanceledOnTouchOutside(true);
        return dialog;
    }

    private View initView() {
        final Context context = getActivity();
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rootView = inflater.inflate(R.layout.settings_large_file, null);

        mSeekLargeFile = (SeekBar) rootView.findViewById(R.id.large_file_seek_bar);
        mTxtValue = (TextView) rootView.findViewById(R.id.large_file_value);

        long size = PreferenceUtils.getLargeFilesSize(context);
        int index = mListSize.indexOf(size);
        Log.d(TAG, "onProgressChanged() - size = " + size + "-indez = " + index);
        if (index != -1) {
            mSeekLargeFile.setProgress(index);
        }
        String strSize = UiUtils.makeFileSizeString(context, size);
        mTxtValue.setText(strSize);
        mSeekLargeFile.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                Log.d(TAG, "onProgressChanged() - progress = " + progress);
                mCurrentSize = mListSize.get(progress);
                String tmpSize = UiUtils.makeFileSizeString(context, mCurrentSize);
                mTxtValue.setText(tmpSize);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        return rootView;
    }
}

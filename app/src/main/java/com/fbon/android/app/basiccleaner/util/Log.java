package com.fbon.android.app.basiccleaner.util;

/*
http://developer.android.com/reference/android/util/Log.html
The order in terms of verbosity, from least to most is ERROR, WARN, INFO, DEBUG, VERBOSE.
Verbose should never be compiled into an application except during development.
Debug logs are compiled in but stripped at runtime.
Error, warning and info logs are always kept.
*/

import android.os.Build;

public final class Log {
    public static boolean ENG = ("eng".equals(Build.TYPE));
    private static final String prefix = "BaC";

    public static int v(String tag, String msg) {
        if (!ENG) {
            return 0;
        }
        return android.util.Log.v(prefix + tag, msg);
    }

    public static int v(String tag, String msg, Throwable tr) {
        if (!ENG) {
            return 0;
        }
        return android.util.Log.v(prefix + tag, msg, tr);
    }

    public static int d(String tag, String msg) {
        if (!ENG) {
            return 0;
        }
        return android.util.Log.d(prefix + tag, msg);
    }

    public static int d(String tag, String msg, Throwable tr) {
        if (!ENG) {
            return 0;
        }
        return android.util.Log.d(prefix + tag, msg, tr);
    }

    public static int i(String tag, String msg) {
        return android.util.Log.i(prefix + tag, msg);
    }

    public static int i(String tag, String msg, Throwable tr) {
        return android.util.Log.i(prefix + tag, msg, tr);
    }

    public static int w(String tag, String msg) {
        return android.util.Log.w(prefix + tag, msg);
    }

    public static int w(String tag, String msg, Throwable tr) {
        return android.util.Log.w(prefix + tag, msg, tr);
    }

    public static int w(String tag, Throwable tr) {
        return android.util.Log.w(prefix + tag, tr);
    }

    public static int e(String tag, String msg) {
        return android.util.Log.e(prefix + tag, msg);
    }

    public static int e(String tag, String msg, Throwable tr) {
        return android.util.Log.e(prefix + tag, msg, tr);
    }
}

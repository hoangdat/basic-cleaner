package com.fbon.android.app.basiccleaner.thumbnail;

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;

import com.fbon.android.app.basiccleaner.R;
import com.fbon.android.app.basiccleaner.util.Log;
import com.fbon.android.app.basiccleaner.util.UiUtils;


public class ApkThumbnailImp extends ThumbnailImp {
    public static final String TAG = "apkThumbnail";
    public ApkThumbnailImp(Context context) {
        super(context);
    }

    @Override
    protected boolean isSupport(String path, String packageName) {
        Log.d(TAG, "isSupport");
        if (UiUtils.isApk(UiUtils.getExtensionAsUpperCase(path)) || packageName != null) {
            return true;
        }
        return false;
    }

    @Override
    protected Bitmap _createThumbnail(String path, String packageName, int thumbnailSize) {
        Log.d(TAG, "_createThumbnail package = " + packageName + "-apk = " + path);
        Bitmap bitmap = null;
        try {
            if (UiUtils.isApk(UiUtils.getExtensionAsUpperCase(path))) {
                bitmap = getResBitmap(UiUtils.getApkIcon(mContext, path), THUMBNAIL_SIZE);
            } else {
                if (packageName != null) {
                    Log.d(TAG, "_createThumbnail package = " + packageName);
                    PackageManager packageMgr = mContext.getPackageManager();
                    Drawable d = packageMgr.getApplicationIcon(packageName);
                    bitmap = getResBitmap(d, THUMBNAIL_SIZE);
                }
            }
        } catch (OutOfMemoryError e) {
            Log.e(TAG, "OutOfMemoryError - " + e.getMessage());
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(TAG, "NameNotFoundException - " + e.getMessage());
        }
        if (bitmap == null) {
            bitmap = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.ic_android);
        }

        return bitmap;
    }
}

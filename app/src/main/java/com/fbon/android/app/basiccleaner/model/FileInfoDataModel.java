package com.fbon.android.app.basiccleaner.model;


public class FileInfoDataModel extends StorageInfoDataModel {
    private int mId = -1;
    public FileInfoDataModel(int type) {
        super(type);
    }

    public int getId() {
        return mId;
    }

    public void setId(int mId) {
        this.mId = mId;
    }
}

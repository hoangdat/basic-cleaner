package basiccleaner.app.datph.com.basiccleaner.fragment;


import com.fbon.android.app.basiccleaner.fragment.ItemFragment;
import com.fbon.android.app.basiccleaner.uicore.Event;

public class BatteryFragment extends ItemFragment {
    @Override
    public int getFixNowEvent() {
        return Event.BATTERY_SCAN;
    }

    @Override
    public void scan() {

    }
}

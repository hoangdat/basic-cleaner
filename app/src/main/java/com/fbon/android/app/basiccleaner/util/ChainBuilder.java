package com.fbon.android.app.basiccleaner.util;


public class ChainBuilder<T extends Chain> {
    private T mRet = null;
    private T mNext = null;

    public ChainBuilder append(T chain) {
        if (chain != null) {
            if (mRet == null) {
                mRet = chain;
                mNext = chain;
            } else {
                mNext.setNext(chain);
                mNext = chain;
            }
        }
        return this;
    }

    public T getInstance() {
        return mRet;
    }
}

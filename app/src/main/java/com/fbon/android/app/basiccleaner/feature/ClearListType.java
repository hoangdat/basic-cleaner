package com.fbon.android.app.basiccleaner.feature;


import com.fbon.android.app.basiccleaner.model.StorageInfoDataModel;

import java.util.ArrayList;

public class ClearListType {
    private int mType;
    private ArrayList<StorageInfoDataModel> mLstClear;

    public ClearListType(int type, ArrayList<StorageInfoDataModel> lst) {
        mType = type;
        mLstClear = lst;
    }

    public int getType() {
        return mType;
    }

    public ArrayList<StorageInfoDataModel> getList() {
        return mLstClear;
    }
}

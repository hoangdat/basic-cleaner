package com.fbon.android.app.basiccleaner.listener;


import com.fbon.android.app.basiccleaner.util.Log;

import java.util.ArrayList;
import java.util.List;

public class ListenerMgr {
    private static final String TAG = "lstMgr";
    public enum LifeCycle {
        CREATE,
        START,
        RESUME,
        PAUSE,
        DESTROY
    }

    private List<AbsListenerImp> mListenerList;

    public ListenerMgr() {
        mListenerList = new ArrayList<>();
    }

    public void addListener(AbsListenerImp listener) {
        if (listener != null) {
            if (!mListenerList.contains(listener)) {
                mListenerList.add(listener);
            }
        } else {
            Log.e(TAG, "can't add the null listener.");
        }
    }

    private void clearListener() {
        mListenerList.clear();
    }

    public void notifyCreate() {
        for (AbsListenerImp imp : mListenerList) {
            if (imp.getRegisterTime() == LifeCycle.CREATE) {
                Log.d(TAG, "onCreate : register "+ imp.getClass().getSimpleName());
                imp.registerListener();
            }
        }
    }

    public void notifyResume() {
        for (AbsListenerImp imp : mListenerList) {
            if (imp.getRegisterTime() == LifeCycle.RESUME) {
                Log.d(TAG, "onResume : register "+ imp.getClass().getSimpleName());
                imp.registerListener();
            }
        }
    }

    public void notifyPause() {
        for (AbsListenerImp imp : mListenerList) {
            if (imp.getUnregisterTime() == LifeCycle.PAUSE) {
                Log.d(TAG, "onPause : unregister "+ imp.getClass().getSimpleName());
                imp.unregisterListener();
            }
        }
    }

    public void notifyDestroy() {
        for (AbsListenerImp imp : mListenerList) {
            if (imp.getUnregisterTime() == LifeCycle.DESTROY) {
                Log.d(TAG, "onDestory : unregister "+ imp.getClass().getSimpleName());
                imp.unregisterListener();
            }
        }
        clearListener();
    }
}

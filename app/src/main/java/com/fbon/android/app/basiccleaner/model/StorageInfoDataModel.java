package com.fbon.android.app.basiccleaner.model;


public class StorageInfoDataModel {
    private String name;
    private long mSize;
    private String mPackageName;
    private String mPath;
    private int mJunkType;

    public StorageInfoDataModel(int type) {
        mJunkType = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getSize() {
        return mSize;
    }

    public void setSize(long mSize) {
        this.mSize = mSize;
    }

    public String getPackageName() {
        return mPackageName;
    }

    public void setPackageName(String mPackageName) {
        this.mPackageName = mPackageName;
    }

    public String getPath() {
        return mPath;
    }

    public void setPath(String mPath) {
        this.mPath = mPath;
    }

    public int getJunkType() {
        return mJunkType;
    }

    public void setJunkType(int mJunkType) {
        this.mJunkType = mJunkType;
    }
}

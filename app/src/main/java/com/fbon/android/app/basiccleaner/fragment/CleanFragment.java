package com.fbon.android.app.basiccleaner.fragment;

import android.animation.Animator;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.TextAppearanceSpan;
import android.transition.TransitionManager;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.PathInterpolator;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fbon.android.app.basiccleaner.R;
import com.fbon.android.app.basiccleaner.animation.BounceInterpolator;
import com.fbon.android.app.basiccleaner.feature.CleanLdrManager;
import com.fbon.android.app.basiccleaner.feature.ClearListType;
import com.fbon.android.app.basiccleaner.feature.ClearMgr;
import com.fbon.android.app.basiccleaner.feature.memory.MemoryWorker;
import com.fbon.android.app.basiccleaner.model.StorageInfoDataModel;
import com.fbon.android.app.basiccleaner.util.AppFeature;
import com.fbon.android.app.basiccleaner.util.Constant;
import com.fbon.android.app.basiccleaner.util.Log;
import com.fbon.android.app.basiccleaner.util.PreferenceUtils;
import com.fbon.android.app.basiccleaner.util.StorageInfo;
import com.fbon.android.app.basiccleaner.util.UiUtils;
import com.fbon.android.app.basiccleaner.widget.CircleView;
import com.fbon.android.app.basiccleaner.widget.ScanningWaveCircle;

import java.util.ArrayList;

public class CleanFragment extends BasicCleanFragment implements ClearMgr.OnFinishedClearListener, Animation.AnimationListener, CircleView.OnFinishedSetScore{
    protected static final String TAG = "CleanFragment";

    protected TextAppearanceSpan mUsedTotalTextApp;

    protected static final int UPDATE_CLEAR_DONE = 2301;
    protected ImageView mImgDone;
    protected Animation mAnimation;
    protected int mFlagDone = 0;
    protected int mFlagCleared = 0;
    protected SparseArray<Long> mClearedSz;
    protected RelativeLayout mScanningContainer;

    protected SpannableStringBuilder mDisplayString;

    protected static final int SCAN_TYPE_CLEAN_NONE = 3001;
    protected static final int SCAN_TYPE_CLEAN_DONE = 3002;

    protected static int[] mJunkType = {
            CleanLdrManager.CACHE_JUNK_TYPE,
            CleanLdrManager.UNNECESSARY_JUNK_TYPE,
            CleanLdrManager.LARGE_JUNK_TYPE,
            CleanLdrManager.APK_JUNK_TYPE,
            CleanLdrManager.MEM_JUNK_TYPE,
    };

    protected Handler mUICleanHandler;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mUICleanHandler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message msg) {
                int what = msg.what;
                int cleanType = msg.arg1;
                Log.d(TAG, "handleMessage() - what = " + what + "-getS = " + mFlagDone);
                switch (what) {
                    case UPDATE_CLEAR_DONE:
                        mFlagDone = cleanType | mFlagDone;
                        if (mFlagDone == mFlagCleared) {
                            CleanLdrManager.getInstance(getActivity()).resetLoader();
                            updateResultView(SCAN_TYPE_CLEAN_DONE, 0, true);
                        }
                        break;
                    default:
                        break;
                }
                return false;
            }
        });
        mClearedSz = new SparseArray<>();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutId(), container, false);
        initCommonView(view);
        initView(view);
        mUsedTotalTextApp = new TextAppearanceSpan(getActivity(), R.style.BaC_UsedTotal);
        mDisplayString = new SpannableStringBuilder();
        return view;
    }

    @Override
    public void setScan(boolean enable) {
        Log.d(TAG, "setScan() - " + enable);
        setScanType(mapScanType(false));
        updateResultView(SCAN_TYPE_CLEAN_NONE, 0, false);
        scan();
    }

    @Override
    public int getLayoutId() {
        return R.layout.clean_fragment;
    }

    @Override
    public void initCommonView(View view) {
        mScanningCircle = (ScanningWaveCircle) view.findViewById(R.id.circle_scan);
        mStatusCircle = (CircleView) view.findViewById(R.id.circle_status);
        mStatusCircle.setFinishedScore(this);
    }

    @Override
    public void initView(View view) {
        mScanningContainer = (RelativeLayout) view.findViewById(R.id.clean_scanning_container);
        mImgDone = (ImageView) view.findViewById(R.id.clean_done_image);
        mImgDone.setColorFilter(getActivity().getResources().getColor(R.color.white));
        mAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.bounce);
        mAnimation.setInterpolator(new BounceInterpolator(0.56, 8));
//        mAnimation.setInterpolator(new PathInterpolator(0.33f, 0f, 0.40f, 1.0f));
        mAnimation.setDuration(300);
        mAnimation.setAnimationListener(this);
    }

    @Override
    public void scan() {
        Log.d(TAG, "scan() - getScanType() = " + getScanType());
        doClear();
    }

    protected void doClear() {
        Log.d(TAG, "doClear() - getScanType() = " + getScanType());
        mFlagDone = 0;
        int scene = FragmentController.getInstance().getCurrentScene();
        if (scene == FragmentController.Scene.CleanAll || scene == FragmentController.Scene.Clean_quick) {
            PreferenceUtils.setLastTimeOverallClean(getActivity(), System.currentTimeMillis());
        }
        if (ClearMgr.getInstance(getActivity()).getScanType() == getScanType()) {
            if (ClearMgr.getInstance(getActivity()).isValidListClear()) {
                Log.d(TAG, "scan() - validList - clear");
                mFlagCleared = ClearMgr.getInstance(getActivity()).getFlagCleared();
                ClearMgr.getInstance(getActivity()).startClear(this);
            } else {
                int curScanType = mapScanType(true);
                ArrayList<ClearListType> listType = new ArrayList<>();
                for (int i = 0; i < mJunkType.length; i++) {
                    int tmpJunk = mJunkType[i];
                    int scanType = CleanLdrManager.getMapTypeFlag().get(tmpJunk);
                    Log.d(TAG, "scan() - not validList - scanType = " + scanType + "-tmpJunk = " + tmpJunk);
                    if ((scanType | curScanType) == curScanType) {
                        ArrayList<StorageInfoDataModel> tmpData = CleanLdrManager.getInstance(
                                getActivity()).getStore(mapScanType(false), tmpJunk);
                        listType.add(new ClearListType(tmpJunk, tmpData));
                    }
                }
                Log.d(TAG, "scan() - not validList - listType  = " + listType);
                ClearMgr.getInstance(getActivity()).setLstClear(mapScanType(false), listType, false);
                mFlagCleared = ClearMgr.getInstance(getActivity()).getFlagCleared();
                ClearMgr.getInstance(getActivity()).startClear(this);
            }
        }
    }

    @Override
    public void onFinishedClearJunk(int type, long cleanedSz) {
        Log.d(TAG, "onFinishedClearJunk() type = " + type + "-sz = " + cleanedSz);
        mClearedSz.put(type, cleanedSz);
        sendUIMessage(UPDATE_CLEAR_DONE, type, 4 * Constant.SCANNING_WAVE_DURATION);
    }

    protected int mapScanType(boolean isClean) {
        int ret = 0;
        int curScene = FragmentController.getInstance().getCurrentScene();
        switch (curScene) {
            case FragmentController.Scene.Clean_storage:
                if (isClean) {
                    ret = Constant.FLAG_LOAD_CACHE | Constant.FLAG_LOAD_UN;
                } else {
                    ret = Constant.FLAG_LOAD_CACHE | Constant.FLAG_LOAD_UN | Constant.FLAG_LOAD_LARGE | Constant.FLAG_LOAD_APK;
                }
                break;
            case FragmentController.Scene.Clean_memory:
                ret = AppFeature.isSupportListMem() ? Constant.FLAG_LOAD_APP_MEM : Constant.FLAG_LOAD_CACHE;
                break;
            case FragmentController.Scene.Clean_quick:
            case FragmentController.Scene.CleanAll:
                ret = AppFeature.isSupportListMem() ?
                        (Constant.FLAG_LOAD_CACHE | Constant.FLAG_LOAD_UN | Constant.FLAG_LOAD_APP_MEM) :
                        (Constant.FLAG_LOAD_CACHE | Constant.FLAG_LOAD_UN);
                break;
            case FragmentController.Scene.Clean_more_space:
                if (isClean) {
                    ret = Constant.FLAG_LOAD_LARGE | Constant.FLAG_LOAD_APK;
                } else {
                    ret = Constant.FLAG_LOAD_CACHE | Constant.FLAG_LOAD_UN | Constant.FLAG_LOAD_LARGE | Constant.FLAG_LOAD_APK;
                }
                break;
            default:
                break;
        }
        Log.d(TAG, "mapScanType() - ret = " + ret);
        return ret;
    }

    public void sendUIMessage(int what, int clearType, long delay) {
        Log.d(TAG, "sendUIMessage() - clearType = " + clearType);
        if (mUICleanHandler != null) {
            mUICleanHandler.sendMessageDelayed(mUICleanHandler.obtainMessage(what, clearType, 0), delay);
        }
    }

    @Override
    public void updateResultView(int scanType, long prevScore, boolean isPercent) {
        Log.d(TAG, "updateResultView() - scanType = " + scanType);
        switch (scanType) {
            case SCAN_TYPE_CLEAN_NONE:
                setScanningEnable(true, 0);
                mImgDone.setVisibility(View.GONE);
                break;
            case SCAN_TYPE_CLEAN_DONE:
                setScanningEnable(false, 100);
                break;
        }
    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {
        try {
            Thread.sleep(550);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        TransitionManager.beginDelayedTransition(mScanningContainer);
        Animation animationOut = AnimationUtils.makeOutAnimation(getActivity(), true);
        animationOut.setDuration(350);
        animationOut.setInterpolator(new PathInterpolator(0.33f, 0f, 0.40f, 1.0f));
        animationOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mScanningContainer.setVisibility(View.GONE);
                enableStubView(getView());
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        mScanningContainer.startAnimation(animationOut);
    }

    @Override
    public void onAnimationRepeat(Animation animation) {
    }

    @Override
    public void onFinishedScore(Animator animator) {
        mImgDone.setVisibility(View.VISIBLE);
        mImgDone.startAnimation(mAnimation);
    }

    protected void enableStubView(final View parentView) {
        View ret;
        ViewStub viewStub;
        viewStub = (ViewStub) parentView.findViewById(R.id.done_container_stub);
        int curScene = FragmentController.getInstance().getCurrentScene();
        int color = getActivity().getResources().getColor(R.color.white);
        if (viewStub != null) {
            ret = viewStub.inflate();
            Animation animation = AnimationUtils.makeInChildBottomAnimation(getActivity());
            animation.setDuration(656);
            animation.setInterpolator(new PathInterpolator(0.33f, 0f, 0.40f, 1.0f));
            ret.startAnimation(animation);
        } else {
            ret = parentView;
        }

        final TextView txtOp = (TextView) ret.findViewById(R.id.clean_done_operation);
        final TextView txtResult = (TextView) ret.findViewById(R.id.clean_done_operation_result);
        final ImageButton shareBtn = (ImageButton) ret.findViewById(R.id.clean_done_like);
        final RelativeLayout relUsedTotal = (RelativeLayout) ret.findViewById(R.id.clean_done_used_total_container);
        final TextView txtUsedResult = (TextView) ret.findViewById(R.id.clean_done_used_total);
        boolean isSceneCleanAll = false;
        if (curScene != FragmentController.Scene.CleanAll && curScene != FragmentController.Scene.Clean_quick) {
            isSceneCleanAll = true;
            mDisplayString.clearSpans();
            mDisplayString.clear();
            long used = getUsed();
            long total = getTotal();
            StringBuilder usedTotal = new StringBuilder(getResources().getString(R.string.used));
            usedTotal.append(": ").append(UiUtils.makeFileSizeString(getActivity(), used));
            usedTotal.append("/").append(UiUtils.makeFileSizeString(getActivity(), total));
            mDisplayString.append(usedTotal);
            int startIndex = usedTotal.indexOf(":") + 2;
            int lastIndex = usedTotal.indexOf("/", startIndex);
            mDisplayString.setSpan(mUsedTotalTextApp, startIndex, lastIndex, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            txtUsedResult.setText(mDisplayString);
        }

        shareBtn.setColorFilter(getActivity().getResources().getColor(R.color.overall_fix_now_text_color));
        final ImageView imageDone = (ImageView) ret.findViewById(R.id.clean_done_ic_done);
        imageDone.setColorFilter(color);
        CircleView circle = (CircleView) ret.findViewById(R.id.done_circle);
        final boolean fIsScanCleanAll = isSceneCleanAll;
        circle.setFinishedScore(new CircleView.OnFinishedSetScore() {
            @Override
            public void onFinishedScore(Animator animator) {
                imageDone.setVisibility(View.VISIBLE);
                txtOp.setVisibility(View.VISIBLE);
                txtResult.setVisibility(View.VISIBLE);
                txtResult.setText(UiUtils.makeFileSizeString(getActivity(), getClearedTotal()));
                shareBtn.setVisibility(View.VISIBLE);
                relUsedTotal.setVisibility(fIsScanCleanAll ? View.VISIBLE : View.GONE);
                int count = PreferenceUtils.getCountClean(getActivity());
                PreferenceUtils.setCountClean(getActivity(), count + 1);
            }
        });
        circle.setScore(100, color, true, 1600);
    }

    protected long getClearedTotal() {
        long ret = 0;
        int curScanType = mapScanType(true);
        for (int i = 0; i < mJunkType.length; i++) {
            int tmpJunk = mJunkType[i];
            int scanType = CleanLdrManager.getMapTypeFlag().get(tmpJunk);
            if ((scanType | curScanType) == curScanType && mClearedSz != null) {
                ret += mClearedSz.get(ClearMgr.getClearMapFlag().get(tmpJunk), 0L);
            }
        }
        return ret;
    }

    @Override
    public void onStop() {
        Log.d(TAG, "onStop() - flag = " + mFlagDone + "-get = " + mFlagCleared);
        if (mFlagDone == mFlagCleared) {
            Log.d(TAG, "onStop - resetLoader");
            CleanLdrManager.getInstance(getActivity()).resetLoader();
        }
        super.onStop();
    }

    @Override
    public void onDestroy() {
        if (mUICleanHandler != null) {
            mUICleanHandler.removeMessages(UPDATE_CLEAR_DONE);
            mUICleanHandler = null;
        }
        super.onDestroy();
    }

    protected long getUsed() {
        long ret = 0;
        int curScene = FragmentController.getInstance().getCurrentScene();
        switch (curScene) {
            case FragmentController.Scene.Clean_memory:
                MemoryWorker.getInstance(getActivity()).updateMemInfo();
                long avail = MemoryWorker.getInstance(getActivity()).getAvailMem();
                long total = MemoryWorker.getInstance(getActivity()).getTotalMemory();
                ret = total - avail;
                break;
            case FragmentController.Scene.Clean_storage:
            case FragmentController.Scene.Clean_more_space:
                long totalSize = StorageInfo.getStorageSize(getActivity(), StorageInfo.iStorageType.INTERNAL);
                long freeSpace = StorageInfo.getStorageFreeSpace(getActivity(), StorageInfo.iStorageType.INTERNAL);
                ret = totalSize - freeSpace;
                break;
            default:
                break;
        }
        return ret;
    }

    protected long getTotal() {
        long ret = 0;
        int curScene = FragmentController.getInstance().getCurrentScene();
        switch (curScene) {
            case FragmentController.Scene.Clean_memory:
                ret = UiUtils.getHWMemSize(getActivity());
                break;
            case FragmentController.Scene.Clean_storage:
            case FragmentController.Scene.Clean_more_space:
                ret = StorageInfo.getStorageSize(getActivity(), StorageInfo.iStorageType.INTERNAL);
                break;
            default:
                break;
        }
        return ret;
    }

//    private void showAdvertise(View parentView) {
//        if (UiUtils.isNetworkOn(getActivity()) && parentView != null) {
//            View view;
//            ViewStub stub = (ViewStub)parentView.findViewById(R.id.done_advertisement_stub);
//            if (stub != null) {
//                view = stub.inflate();
//                Animation animation = AnimationUtils.makeInChildBottomAnimation(getActivity());
//                animation.setDuration(356);
//                animation.setInterpolator(new PathInterpolator(0.33f, 0f, 0.40f, 1.0f));
//                view.startAnimation(animation);
//            } else {
//                view = parentView;
//            }
//        }
//    }
}
package com.fbon.android.app.basiccleaner.fragment;


import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.fbon.android.app.basiccleaner.R;
import com.fbon.android.app.basiccleaner.feature.CleanLdrManager;
import com.fbon.android.app.basiccleaner.feature.ClearMgr;
import com.fbon.android.app.basiccleaner.model.StorageInfoDataModel;
import com.fbon.android.app.basiccleaner.uicore.Event;
import com.fbon.android.app.basiccleaner.util.Constant;
import com.fbon.android.app.basiccleaner.util.Log;
import com.fbon.android.app.basiccleaner.util.PreferenceUtils;
import com.fbon.android.app.basiccleaner.util.StorageInfo;
import com.fbon.android.app.basiccleaner.util.UiUtils;

import java.util.ArrayList;
public class StorageFragment extends ItemFragment implements CleanLdrManager.OnInfoListener{
    private static final String TAG = "StorageFragment";

    private Button mButtonUsage;
    private TextView mTxtSuggestMoreSpace;
    private ArrayList<DetailsViewContents> mMapDetailsContent = new ArrayList<>();

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initDetailsView(view, R.id.large_layout_details, R.string.large_group, R.string.large_content);
        initDetailsView(view, R.id.obsolete_apk_layout_detail, R.string.apk_group, R.string.apk_content);
    }

    @Override
    public int getFixNowEvent() {
        return Event.STORAGE_CLEAR;
    }

    @Override
    public int getShowUsageEvent() {
        return Event.STORAGE_MORE_SPACE;
    }

    @Override
    protected int getRequestInfo() {
        return CleanLdrManager.MSG_START_LOAD_STORAGE_INFO;
    }

    @Override
    public boolean sendScanRequest() {
        Log.d(TAG, "sendScanRequest()");
        return CleanLdrManager.getInstance(getActivity()).startLoadStorage(firstScanType(), this);
    }

    @Override
    public int getViewStubId() {
        return R.id.storage_stub;
    }

    private void initDetailsView(View root, int detailViewID, int titleResId, int contentResId) {
        View detailView = root.findViewById(detailViewID);
        if (detailView != null) {
            DetailsViewContents content = new DetailsViewContents();
            content.mViewId = detailViewID;
            content.mDetailView = detailView;
            content.mTitle = (TextView) detailView.findViewById(R.id.text_detail_item_name);
            content.mSize = (TextView) detailView.findViewById(R.id.text_detail_item_size);
            content.mContent = (TextView) detailView.findViewById(R.id.text_detail_description);

            if (content.mTitle != null) {
                content.mTitle.setText(titleResId);
            }

            if (content.mContent != null) {
                content.mContent.setText(contentResId);
            }

            if (content.mSize != null) {
                content.mContent.setText(UiUtils.makeFileSizeString(getActivity(), 0));
            }

            mMapDetailsContent.add(content);
        }
    }

    private void updateDetailsView(boolean isScan, boolean isButtonShow) {
        boolean enableButton = false;
        if (mMapDetailsContent != null) {
            for (DetailsViewContents content : mMapDetailsContent) {
                if (content != null) {
                    switch (content.mViewId) {
                        case R.id.large_layout_details:
                            long mSizeLarge = UiUtils.getStoreSize(getActivity(), getScanType(), CleanLdrManager.LARGE_JUNK_TYPE);
                            if (mSizeLarge > 0) {
                                enableButton = true;
                            }
                            String size = UiUtils.makeFileSizeString(getActivity(), PreferenceUtils.getLargeFilesSize(getActivity()));
                            String title = getActivity().getResources().getString(R.string.checking);
                            if (!isScan) {
                                title = String.format(getActivity().getResources().getString(R.string.large_content), size);
                            }
                            content.mContent.setText(title);
                            content.mSize.setText(isScan ? UiUtils.makeFileSizeString(getActivity(), 0)
                                    : UiUtils.makeFileSizeString(getActivity(), mSizeLarge));
                            if (!isScan && mSizeLarge == 0) {
                                content.mDetailView.setAlpha(0.3f);
                            } else {
                                content.mDetailView.setAlpha(1f);
                            }
                            break;
                        case R.id.obsolete_apk_layout_detail:
                            long mSizeApk = UiUtils.getStoreSize(getActivity(), getScanType(), CleanLdrManager.APK_JUNK_TYPE);
                            if (mSizeApk > 0) {
                                enableButton = true;
                            }
                            content.mContent.setText(isScan ? R.string.checking : R.string.apk_content);
                            content.mSize.setText(isScan ? UiUtils.makeFileSizeString(getActivity(), 0)
                                    : UiUtils.makeFileSizeString(getActivity(), mSizeApk));
                            if (!isScan && mSizeApk == 0) {
                                content.mDetailView.setAlpha(0.3f);
                            } else {
                                content.mDetailView.setAlpha(1f);
                            }
                            break;
                        default:
                    }
                }
            }
        }

        boolean bShow = !isScan && enableButton;
        isButtonShow = isButtonShow && bShow;
        if (mButtonUsage != null) {
            mButtonUsage.setAlpha(isButtonShow ? 1f : 0.4f);
            mButtonUsage.setEnabled(isButtonShow);
        }

        if (mTxtSuggestMoreSpace != null) {
            mTxtSuggestMoreSpace.setText(bShow ? R.string.recommend_storage
                    : R.string.checking_more_space);
        }
    }

    @Override
    public void updateResultView(int scanType, long prevScore, boolean isPercent) {
        if (scanType == Constant.FLAG_NONE) {
            updateDetailsView(true, false);
        } else if (scanType == firstScanType()) {
            updateDetailsView(false, true);
        }
        super.updateResultView(scanType, prevScore, isPercent);
    }

    private static class DetailsViewContents {
        int mViewId;
        View mDetailView;
        TextView mTitle;
        TextView mSize;
        TextView mContent;
    }

    @Override
    public void initViewFormStub(View stubView) {
        mButtonUsage = (Button) stubView.findViewById(R.id.btn_usage);
        mButtonUsage.setOnClickListener(this);

        mTxtSuggestMoreSpace = (TextView) stubView.findViewById(R.id.txt_recommend_storage);
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy");
        mMapDetailsContent.clear();
        super.onDestroy();
    }

    @Override
    protected long getUsed(ArrayList<Long> usedTotal) {
        long ret = 0;
        if (usedTotal != null && !usedTotal.isEmpty()) {
            ret = usedTotal.get(0);
        }
        return ret;
    }

    @Override
    protected long getTotal(ArrayList<Long> usedTotal) {
        long ret = 0;
        if (usedTotal != null && !usedTotal.isEmpty()) {
            ret = usedTotal.get(1);
        }
        return ret;
    }

    @Override
    public int firstScanType() {
        return Constant.FLAG_LOAD_CACHE | Constant.FLAG_LOAD_UN | Constant.FLAG_LOAD_LARGE | Constant.FLAG_LOAD_APK;
    }

    @Override
    protected void _onJunkLoaderProgress(int scanType, int junkType, int progress, StorageInfoDataModel model) {
        if (model == null) {
            return;
        }
        if (junkType == CleanLdrManager.CACHE_JUNK_TYPE) {
            mTxtProgress.setText(model.getName());
        }
    }

    @Override
    protected long getScanTotal() {
        long ret = UiUtils.getStoreSize(getActivity(), firstScanType(), CleanLdrManager.CACHE_JUNK_TYPE);
        ret += UiUtils.getStoreSize(getActivity(), firstScanType(), CleanLdrManager.UNNECESSARY_JUNK_TYPE);
        return ret;
    }

    @Override
    protected void setAddInfo() {
        Resources res = getActivity().getResources();
        mAvailableAddInfo.setText(String.format(res.getString(R.string.available_storage),
                UiUtils.makeFileSizeString(getActivity(), StorageInfo.getStorageFreeSpace(getActivity(), StorageInfo.iStorageType.INTERNAL))));

        mUsedAddInfo.setText(String.format(res.getString(R.string.used_storage),
                UiUtils.makeFileSizeString(getActivity(), getUsed(mUsedTotal))));
    }

    @Override
    public void _onClick(View v) {
        int id = v.getId();
        if (id == R.id.fix_now) {
            ClearMgr.getInstance(getActivity()).setLstClear(firstScanType(), null, true);
        }
        super._onClick(v);
    }
}

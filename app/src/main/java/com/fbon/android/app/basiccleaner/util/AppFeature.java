package com.fbon.android.app.basiccleaner.util;


import android.os.Build;

public class AppFeature {
    public static boolean isSupportListMem() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
            return false;
        }

        return true;
    }
}

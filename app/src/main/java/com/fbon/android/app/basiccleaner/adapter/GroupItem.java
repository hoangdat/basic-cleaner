package com.fbon.android.app.basiccleaner.adapter;


import com.fbon.android.app.basiccleaner.model.StorageInfoDataModel;

import java.util.List;

public class GroupItem {

    private List<StorageInfoDataModel> mListChildren;
    private String mName;
    private long mSize;
    private int mJunkType;

    public int getJunkType() {
        return mJunkType;
    }

    public GroupItem(String name, List<StorageInfoDataModel> lstChilds, int junkType) {
        mName = name;
        mListChildren = lstChilds;
        mJunkType = junkType;
    }

    public List<StorageInfoDataModel> getChildList() {
        return mListChildren;
    }


    public String getName() {
        return mName;
    }

    public long getSize() {
        long ret = 0;
        if (mListChildren != null) {
            for (StorageInfoDataModel item : mListChildren) {
                ret += item.getSize();
            }
        }
        mSize = ret;
        return mSize;
    }

    public void setSize(long size) {
        mSize = size;
    }

    public void setChilds(List<StorageInfoDataModel> lst) {
        mListChildren = lst;
    }

    public int getChildListSize() {
        int ret = 0;
        if (mListChildren != null) {
            ret  = mListChildren.size();
        }
        return ret;
    }
}

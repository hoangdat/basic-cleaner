package com.fbon.android.app.basiccleaner.uicore;


public class Event {
    private static final String TAG = "Event";

    public static final int BASE_EVENT_START = 0;

    public static final int STORAGE_CLEAR = BASE_EVENT_START + 1;
    public static final int STORAGE_GO = BASE_EVENT_START + 2;
    public static final int STORAGE_MORE_SPACE = BASE_EVENT_START + 3;
    public static final int STORAGE_CLEAR_MORE_SPACE = BASE_EVENT_START + 4;

    public static final int MEMORY_CLEAR = STORAGE_CLEAR + 999;
    public static final int MEMORY_GO = MEMORY_CLEAR + 1;

    public static final int BATTERY_SCAN = MEMORY_CLEAR + 999;
    public static final int BATTERY_GO = BATTERY_SCAN + 1;

    public static final int OVERALL_SCAN = BATTERY_SCAN + 999;
    public static final int OVERALL_CLEAN = OVERALL_SCAN + 1;
}

package com.fbon.android.app.basiccleaner.fragment;

import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.fbon.android.app.basiccleaner.R;
import com.fbon.android.app.basiccleaner.adapter.GroupItem;
import com.fbon.android.app.basiccleaner.adapter.StorageAdapterEx;
import com.fbon.android.app.basiccleaner.feature.CleanLdrManager;
import com.fbon.android.app.basiccleaner.feature.ClearListType;
import com.fbon.android.app.basiccleaner.feature.ClearMgr;
import com.fbon.android.app.basiccleaner.model.StorageInfoDataModel;
import com.fbon.android.app.basiccleaner.uicore.Event;
import com.fbon.android.app.basiccleaner.util.Constant;
import com.fbon.android.app.basiccleaner.util.Log;
import com.fbon.android.app.basiccleaner.util.UiUtils;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class MoreSpaceFragment extends AbsBasicCleanFragment implements CleanLdrManager.OnJunkLoaderListener,
        AdapterView.OnItemClickListener, StorageAdapterEx.OnItemCheckChangedListener, View.OnClickListener{
    public static final String TAG = "mrSpace";
    private TextView mTxtLargeSize;
    private TextView mTxtApkSize;
    private ListView mListFile;

    private StorageAdapterEx mAdapter;
    private GroupItem mGroupLarge, mGroupApk;
    private Button mBtnDelete;

    private AtomicInteger mSizeUpdateRequestId = new AtomicInteger(0);
    private SizeUpdateHandler mSizeUpdateHandler;
    private final Object SIZE_UPDATE_LOCK = new Object();
    private static int[] mMoreSpaceJunkType = new int[] {CleanLdrManager.APK_JUNK_TYPE, CleanLdrManager.LARGE_JUNK_TYPE};

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSizeUpdateHandler = new SizeUpdateHandler(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutId(), container, false);
        initView(view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initGroupItem();
        mAdapter = new StorageAdapterEx(getActivity(), R.layout.item_layout, Arrays.asList(mGroupLarge, mGroupApk));
        mAdapter.registerItemCheckChange(this);
        mAdapter.setAllSelected();
        mListFile.setAdapter(mAdapter);
        mListFile.setOnItemClickListener(this);
        updateResultView(0, 0, false);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public int getLayoutId() {
        return R.layout.more_space_fragment;
    }

    @Override
    public void initView(View view) {
        mTxtLargeSize = (TextView) view.findViewById(R.id.more_space_large_size);
        mTxtApkSize   = (TextView) view.findViewById(R.id.more_space_apk_size);

        mBtnDelete = (Button) view.findViewById(R.id.more_space_delete_btn);
        mBtnDelete.setOnClickListener(this);

        mListFile = (ListView) view.findViewById(R.id.more_space_list);
    }

    @Override
    public void scan() {
        //do nothing, only override
    }

    @Override
    public void updateResultView(int state, long prevScore, boolean isPercent) {
        int scanType = getScanType();
        if (CleanLdrManager.getInstance(getActivity()).getScanType() == scanType) {
            boolean enableDelete = false;
            ArrayList<StorageInfoDataModel> tmpList = CleanLdrManager.getInstance(getActivity()).getStore(scanType, CleanLdrManager.LARGE_JUNK_TYPE);
            mGroupLarge.setChilds(tmpList);
            long tmpSz = UiUtils.getStoreSize(getActivity(), scanType, CleanLdrManager.LARGE_JUNK_TYPE);
            if (tmpSz > 0) {
                enableDelete = true;
            }
            setInfoSize(CleanLdrManager.LARGE_JUNK_TYPE, tmpSz);
            mGroupLarge.setSize(tmpSz);

            tmpList = CleanLdrManager.getInstance(getActivity()).getStore(scanType, CleanLdrManager.APK_JUNK_TYPE);
            mGroupApk.setChilds(tmpList);
            tmpSz = UiUtils.getStoreSize(getActivity(), scanType, CleanLdrManager.APK_JUNK_TYPE);
            if (tmpSz > 0) {
                enableDelete = true;
            }
            setInfoSize(CleanLdrManager.APK_JUNK_TYPE, tmpSz);
            mGroupApk.setSize(tmpSz);
            mAdapter.notifyDataSetChanged();
            updateDeleteButton(enableDelete);
        }
    }

    private int getScanType() {
        return Constant.FLAG_LOAD_CACHE | Constant.FLAG_LOAD_UN | Constant.FLAG_LOAD_LARGE | Constant.FLAG_LOAD_APK;
    }

    private void initGroupItem() {
        Resources res = getActivity().getResources();
        mGroupLarge = new GroupItem(res.getString(R.string.large_group), null, CleanLdrManager.LARGE_JUNK_TYPE);
        mGroupApk = new GroupItem(res.getString(R.string.apk_group), null, CleanLdrManager.APK_JUNK_TYPE);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Log.d(TAG, "onItemClick() - pos = " + position);
        mAdapter.setSelected(position);
    }

    @Override
    public void onJunkLoaderProgress(int scanType, int junkType, int progress, StorageInfoDataModel model) {

    }

    @Override
    public void onScanFinished(int scanType) {
        //do nothing
    }

    @Override
    public void onItemCheckChanged() {
        clearSizeUpdateEvent();
        Message msg = mSizeUpdateHandler.obtainMessage(Constant.MSG_START_CALCULATE_SELECTED_FILE_SIZE);
        msg.arg1 = mSizeUpdateRequestId.incrementAndGet();
        mSizeUpdateHandler.sendMessage(msg);
    }

    private void clearSizeUpdateEvent() {
        mSizeUpdateHandler.removeMessages(Constant.MSG_START_CALCULATE_SELECTED_FILE_SIZE);
        mSizeUpdateHandler.removeMessages(Constant.MSG_DISPLAY_RESULT_SIZE);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.more_space_delete_btn) {
            ArrayList<ClearListType> lstClear = new ArrayList<>();
            if (mAdapter != null && mAdapter.isCheckMode()) {
                HashMap<Integer, List<StorageInfoDataModel>> map = mAdapter.getSelectedList();
                if (map != null) {
                    for (int i = 0; i < mMoreSpaceJunkType.length; i++) {
                        int junkType = mMoreSpaceJunkType[i];
                        List<StorageInfoDataModel> lst = map.get(junkType);
                        if (lst != null) {
                            ClearListType clear = new ClearListType(junkType, new ArrayList<>(lst));
                            lstClear.add(clear);
                        }
                    }
                }
            }
            ClearMgr.getInstance(getActivity()).setLstClear(getScanType(), lstClear, false);
            postEvent(Event.STORAGE_CLEAR_MORE_SPACE);
        }
    }

    private static class SizeUpdateHandler extends Handler {
        private WeakReference<MoreSpaceFragment> mParent;

        public SizeUpdateHandler(MoreSpaceFragment fragment) {
            mParent = new WeakReference<>(fragment);
        }

        @Override
        public void handleMessage(Message msg) {
            MoreSpaceFragment fragment = mParent.get();
            if (fragment != null) {
                switch (msg.what) {
                    case Constant.MSG_START_CALCULATE_SELECTED_FILE_SIZE:
                        Log.d(TAG, "handleMessage() - MSG_START_CALCULATE_SELECTED_FILE_SIZE");
                        new SizeCalculateThread(fragment, msg.arg1).start();
                        break;
                    case Constant.MSG_DISPLAY_RESULT_SIZE:
                        Log.d(TAG, "handleMessage() - MSG_DISPLAY_RESULT_SIZE");
                        fragment.setSelectedFileSize((HashMap<Integer, Long>) msg.obj);
                        break;
                }
            }
        }
    }

    private static class SizeCalculateThread extends Thread {
        private  WeakReference<MoreSpaceFragment> mParent;
        private int mRequestId;

        public SizeCalculateThread(MoreSpaceFragment parent, int requestId) {
            mParent = new WeakReference<MoreSpaceFragment>(parent);
            mRequestId = requestId;
        }

        @Override
        public void run() {
            HashMap<Integer, Long> mapSize = null;
            if (keepGoing()) {
                MoreSpaceFragment fragment = mParent.get();
                if (fragment != null) {
                    mapSize = fragment.mAdapter.getSelectedSizeGroup();
                    synchronized (fragment.SIZE_UPDATE_LOCK) {
                        Message msg = fragment.mSizeUpdateHandler.obtainMessage(Constant.MSG_DISPLAY_RESULT_SIZE);
                        msg.obj = mapSize;
                        Log.d(TAG, "run() sendMsg = " + mapSize);
                        fragment.mSizeUpdateHandler.sendMessage(msg);
                    }
                }
            }
        }

        private boolean keepGoing() {
            boolean ret = false;
            MoreSpaceFragment fragment = mParent.get();
            if (fragment != null) {
                ret = fragment.mSizeUpdateRequestId.get() == mRequestId;
            }
            return ret;
        }
    }

    private void setInfoSize(int junkType, long size) {
        switch (junkType) {
            case CleanLdrManager.APK_JUNK_TYPE:
                mTxtApkSize.setText(UiUtils.makeFileSizeString(getActivity(), size));
                break;
            case CleanLdrManager.LARGE_JUNK_TYPE:
                mTxtLargeSize.setText(UiUtils.makeFileSizeString(getActivity(), size));
                break;
            default:
                break;
        }
    }

    private void setSelectedFileSize(HashMap<Integer, Long> mapSize) {
        boolean enableDelete = false;
        if (mapSize == null) {
            mTxtLargeSize.setText("0.00 B");
            mTxtApkSize.setText("0.00 B");
            updateDeleteButton(enableDelete);
            return;
        }
        Log.d(TAG, "setSelectedFileSize()");
        TextView[] txtType = new TextView[] {mTxtApkSize, mTxtLargeSize};
        for (int i = 0; i < mMoreSpaceJunkType.length; i++) {
            int junkType = mMoreSpaceJunkType[i];
            Long oSz = mapSize.get(junkType);
            if (oSz != null) {
                long lSz = oSz.longValue();
                if (lSz > 0) {
                    enableDelete = true;
                }
                String sz = UiUtils.makeFileSizeString(getActivity(), oSz);
                Log.d(TAG, "setSelectedFileSize() not null sz = " + sz);
                txtType[i].setText(sz);
            } else {
                Log.d(TAG, "setSelectedFileSize() null");
                txtType[i].setText("0.00 B");
            }
        }
        updateDeleteButton(enableDelete);
    }

    private void updateDeleteButton(boolean enable) {
        if (mBtnDelete != null) {
            mBtnDelete.setEnabled(enable);
        }
    }
}

package com.fbon.android.app.basiccleaner.fragment;


import com.fbon.android.app.basiccleaner.feature.CleanLdrManager;
import com.fbon.android.app.basiccleaner.feature.ClearMgr;
import com.fbon.android.app.basiccleaner.util.AppFeature;
import com.fbon.android.app.basiccleaner.util.Constant;

public class CleanQuickFragment extends CleanFragment {
    private static final int FLAG_SCAN_TYPE = AppFeature.isSupportListMem() ?
            (Constant.FLAG_LOAD_CACHE | Constant.FLAG_LOAD_UN | Constant.FLAG_LOAD_APP_MEM) :
            (Constant.FLAG_LOAD_CACHE | Constant.FLAG_LOAD_UN);

    @Override
    public void setScan(boolean enable) {
        setScanType(mapScanType(false));
        updateResultView(SCAN_TYPE_CLEAN_NONE, 0, false);
        scan();
    }

    @Override
    public void scan() {
        if (!sendScanRequest()) {
            ClearMgr.getInstance(getActivity()).setLstClear(firstScanType(), null, true);
            doClear();
        }
    }

    @Override
    public boolean sendScanRequest() {
        return CleanLdrManager.getInstance(getActivity()).startLoadOverall(firstScanType(), this);
    }

    @Override
    protected void _onScanFinished(int scanType) {
        if (scanType == getScanType()) {
            ClearMgr.getInstance(getActivity()).setLstClear(firstScanType(), null, true);
            doClear();
        }
    }

    @Override
    public int firstScanType() {
        return FLAG_SCAN_TYPE;
    }
}
